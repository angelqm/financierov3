/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  01/04/2015 - 13:26:01
 */

package ar.com.redlink.sam.financieroservices.dao.search.filter;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter;
import ar.com.redlink.sam.financieroservices.entity.GrupoPrefijoOp;

/**
 * Filtro para obtener {@link GrupoPrefijoOp} por ID de PerfilPrefijoGrupo con
 * un ID de operacion si es necesario.
 * 
 * @author aguerrea
 * 
 */
public class GrupoPrefijoOpByPerfilPrefijoGrupoIdAndOpIdSearchFilter implements
		RedLinkSearchFilter {

	private Long perfilPrefijoGrupoId;
	private Long prefijoOpId;

	/**
	 * Constructor default.
	 * 
	 * @param perfilPrefijoGrupoId
	 * @param prefijoOpId
	 */
	public GrupoPrefijoOpByPerfilPrefijoGrupoIdAndOpIdSearchFilter(
			Long perfilPrefijoGrupoId, String prefijoOpId) {
		this.perfilPrefijoGrupoId = perfilPrefijoGrupoId;

		if (null != prefijoOpId) {
			this.prefijoOpId = Long.valueOf(prefijoOpId);
		}
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyFilters(org.hibernate.Criteria)
	 */
	@Override
	public void applyFilters(Criteria criteria) {
		criteria.add(Restrictions.eq("perfilPrefijoGrupo.perfilPrefijoGrupoId",
				getPerfilPrefijoGrupoId()));

		if (null != getPrefijoOpId()) {
			/*AQM
			 * Si es getPrefijoOpId=2 -Cuentas Relacionadas-
			 * Se debe de mostrar TODOAS las operaciones 
			 * asignada al perfilPrefijoGrupo
			*/
			if (!Long.valueOf("2").equals(getPrefijoOpId())) 
				{	
					Criteria nestedCriteria = criteria
						.createCriteria("prefijoOpEntidad");
				nestedCriteria.add(Restrictions.eq("prefijoOp.prefijoOpId",
						getPrefijoOpId()));
				}
		}
		
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyOrder(org.hibernate.Criteria)
	 */
	@Override
	public void applyOrder(Criteria criteria) {
		// No se requiere un orden.
	}

	/**
	 * @return the perfilPrefijoGrupoId
	 */
	public Long getPerfilPrefijoGrupoId() {
		return perfilPrefijoGrupoId;
	}

	/**
	 * @param perfilPrefijoGrupoId
	 *            the perfilPrefijoGrupoId to set
	 */
	public void setPerfilPrefijoGrupoId(Long perfilPrefijoGrupoId) {
		this.perfilPrefijoGrupoId = perfilPrefijoGrupoId;
	}

	/**
	 * @return the prefijoOpId
	 */
	public Long getPrefijoOpId() {
		return prefijoOpId;
	}

	/**
	 * @param prefijoOpId
	 *            the prefijoOpId to set
	 */
	public void setPrefijoOpId(Long prefijoOpId) {
		this.prefijoOpId = prefijoOpId;
	}

}
