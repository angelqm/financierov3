/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  01/04/2015 - 18:20:51
 */

package ar.com.redlink.sam.financieroservices.dao.search.filter;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter;

/**
 * Filtro para la busqueda de un PerfilPrefijo a partir de un ID de
 * PerfilFinanciero.
 * 
 * @author aguerrea
 * 
 */
public class PerfilPrefijoByPerfilFinancieroIdSearchFilter implements
		RedLinkSearchFilter {

	private Long perfilFinancieroId;

	/**
	 * Constructor default.
	 * 
	 * @param perfilFinancieroId
	 */
	public PerfilPrefijoByPerfilFinancieroIdSearchFilter(Long perfilFinancieroId) {
		this.perfilFinancieroId = perfilFinancieroId;
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyOrder(org.hibernate.Criteria)
	 */
	@Override
	public void applyOrder(Criteria criteria) {

	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyFilters(org.hibernate.Criteria)
	 */
	@Override
	public void applyFilters(Criteria criteria) {
		criteria.createAlias("perfilesFinanciero", "pf");
		criteria.add(Restrictions.eq("pf.perfilFinancieroId",
				getPerfilFinancieroId()));
	}

	/**
	 * @return the perfilFinancieroId
	 */
	public Long getPerfilFinancieroId() {
		return perfilFinancieroId;
	}

	/**
	 * @param perfilFinancieroId
	 *            the perfilFinancieroId to set
	 */
	public void setPerfilFinancieroId(Long perfilFinancieroId) {
		this.perfilFinancieroId = perfilFinancieroId;
	}

}
