/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  28/03/2015 - 13:05:35
 */

package ar.com.redlink.sam.financieroservices.dao.search.filter;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter;

/**
 * Filtro para busqueda de entidad por codigo.
 * 
 * @author aguerrea
 * 
 */
public class EntidadByCodSearchFilter implements RedLinkSearchFilter {

	private String codEntidad;

	/**
	 * @param parseInt
	 */
	public EntidadByCodSearchFilter(String codEntidad) {
		this.codEntidad = codEntidad;
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyFilters(org.hibernate.Criteria)
	 */
	@Override
	public void applyFilters(Criteria criteria) {
		criteria.add(Restrictions.eq("codEntidad", codEntidad));
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyOrder(org.hibernate.Criteria)
	 */
	@Override
	public void applyOrder(Criteria criteria) {
		// No es necesario un orden especifico.
	}

	/**
	 * @return the entidadCod
	 */
	public String getCodEntidad() {
		return codEntidad;
	}

	/**
	 * @param entidadCod
	 *            the entidadCod to set
	 */
	public void setCodEntidad(String codEntidad) {
		this.codEntidad = codEntidad;
	}

}
