/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  27/03/2015 - 22:50:18
 */

package ar.com.redlink.sam.financieroservices.dao.search.filter;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter;

/**
 * Filtro para la busqueda de un Agente por ID de Agente y ID de Entidad.
 * 
 * @author aguerrea
 * 
 */
public class AgenteByAgenteIdAndEntidadIdSearchFilter implements
		RedLinkSearchFilter {

	private Long agenteId;
	private Integer entidadId;

	/**
	 * Constructor default.
	 * 
	 * @param agenteId
	 * @param entidadId
	 */
	public AgenteByAgenteIdAndEntidadIdSearchFilter(Long agenteId,
			Integer entidadId) {
		this.agenteId = agenteId;
		this.setEntidadId(entidadId);
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyFilters(org.hibernate.Criteria)
	 */
	@Override
	public void applyFilters(Criteria criteria) {
		criteria.add(Restrictions.eq("agenteId", agenteId));
		criteria.add(Restrictions.eq("entidad.entidadId", getEntidadId()));
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyOrder(org.hibernate.Criteria)
	 */
	@Override
	public void applyOrder(Criteria criteria) {
		// No se requiere ordenar.
	}

	/**
	 * @return the agenteId
	 */
	public Long getAgenteId() {
		return agenteId;
	}

	/**
	 * @param agenteId
	 *            the agenteId to set
	 */
	public void setAgenteId(Long agenteId) {
		this.agenteId = agenteId;
	}

	/**
	 * @return the entidadId
	 */
	public Integer getEntidadId() {
		return entidadId;
	}

	/**
	 * @param entidadId
	 *            the entidadId to set
	 */
	public void setEntidadId(Integer entidadId) {
		this.entidadId = entidadId;
	}
}
