/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  12/04/2015 - 00:13:59
 */

package ar.com.redlink.sam.financieroservices.dao;

import ar.com.redlink.framework.persistence.dao.RedLinkDAO;

/**
 * Extension del CRUD generico de DAO para {@link EnrolamientoSam}.
 * 
 * @author aguerrea
 * 
 */
public interface EnrolamientoSamDAO extends RedLinkDAO {

	/**
	 * Enrola el dispositivo POS en base a la informacion provista.
	 * 
	 * @param terminalSamId
	 * @param identificadorTerm
	 * @param identTerm
	 * @return Resultado del query.
	 * @throws Exception
	 */
	public int enrollDevice(String terminalSamId, String identificadorTerm) throws Exception;
}
