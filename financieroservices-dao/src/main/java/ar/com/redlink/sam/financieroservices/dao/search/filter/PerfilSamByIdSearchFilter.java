/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  01/04/2015 - 13:10:05
 */

package ar.com.redlink.sam.financieroservices.dao.search.filter;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter;
import ar.com.redlink.sam.financieroservices.entity.PerfilSam;

/**
 * Filtro para obtener un {@link PerfilSam} por ID.
 * 
 * @author aguerrea
 * 
 */
public class PerfilSamByIdSearchFilter implements RedLinkSearchFilter {

	private Long perfilSamId;

	/**
	 * Constructor default.
	 * 
	 * @param perfilSamId
	 */
	public PerfilSamByIdSearchFilter(Long perfilSamId) {
		this.perfilSamId = perfilSamId;
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyFilters(org.hibernate.Criteria)
	 */
	@Override
	public void applyFilters(Criteria criteria) {
		criteria.add(Restrictions.eq("perfilSamId", perfilSamId));
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyOrder(org.hibernate.Criteria)
	 */
	@Override
	public void applyOrder(Criteria criteria) {
		// No se requiere un orden.
	}

	/**
	 * @return the perfilSamId
	 */
	public Long getPerfilSamId() {
		return perfilSamId;
	}

	/**
	 * @param perfilSamId
	 *            the perfilSamId to set
	 */
	public void setPerfilSamId(Long perfilSamId) {
		this.perfilSamId = perfilSamId;
	}

}
