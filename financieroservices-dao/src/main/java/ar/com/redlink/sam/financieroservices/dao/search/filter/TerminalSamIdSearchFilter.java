/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  29/03/2015 - 12:19:57
 */

package ar.com.redlink.sam.financieroservices.dao.search.filter;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter;

/**
 * Filtro para obtener una terminal SAM por ID.
 * 
 * @author aguerrea
 * 
 */
public class TerminalSamIdSearchFilter implements RedLinkSearchFilter {

	private Long terminalId;

	/**
	 * Constructor default.
	 * 
	 * @param terminalId
	 */
	public TerminalSamIdSearchFilter(Long terminalId) {
		this.terminalId = terminalId;
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyFilters(org.hibernate.Criteria)
	 */
	@Override
	public void applyFilters(Criteria criteria) {
		criteria.add(Restrictions.eq("terminalSam.terminalSamId",
				this.getTerminalId()));
		criteria.add(Restrictions.eq("estadoRegistro.estadoRegistroId",
				(short) 0));
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyOrder(org.hibernate.Criteria)
	 */
	@Override
	public void applyOrder(Criteria criteria) {
		// No hay un orden requerido.
	}

	/**
	 * @return the terminalId
	 */
	public Long getTerminalId() {
		return terminalId;
	}

	/**
	 * @param terminalId
	 *            the terminalId to set
	 */
	public void setTerminalId(Long terminalId) {
		this.terminalId = terminalId;
	}

}
