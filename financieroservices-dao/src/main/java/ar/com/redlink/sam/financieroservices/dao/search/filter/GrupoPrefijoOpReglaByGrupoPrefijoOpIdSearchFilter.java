/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  01/04/2015 - 13:33:58
 */

package ar.com.redlink.sam.financieroservices.dao.search.filter;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter;
import ar.com.redlink.sam.financieroservices.entity.GrupoPrefijoOpRegla;

/**
 * Filtro para {@link GrupoPrefijoOpRegla} por Id de GrupoPrefijoOp.
 * 
 * @author aguerrea
 * 
 */
public class GrupoPrefijoOpReglaByGrupoPrefijoOpIdSearchFilter implements
		RedLinkSearchFilter {

	private Long grupoPrefijoOpId;

	/**
	 * Constructor default.
	 * 
	 * @param grupoPrefijoOpId
	 */
	public GrupoPrefijoOpReglaByGrupoPrefijoOpIdSearchFilter(
			Long grupoPrefijoOpId) {
		this.grupoPrefijoOpId = grupoPrefijoOpId;
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyFilters(org.hibernate.Criteria)
	 */
	@Override
	public void applyFilters(Criteria criteria) {
		criteria.add(Restrictions.eq("grupoPrefijoOp.grupoPrefijoOpId",
				getGrupoPrefijoOpId()));
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyOrder(org.hibernate.Criteria)
	 */
	@Override
	public void applyOrder(Criteria criteria) {
		// No se requiere un orden.

	}

	/**
	 * @return the grupoPrefijoOpId
	 */
	public Long getGrupoPrefijoOpId() {
		return grupoPrefijoOpId;
	}

	/**
	 * @param grupoPrefijoOpId
	 *            the grupoPrefijoOpId to set
	 */
	public void setGrupoPrefijoOpId(Long grupoPrefijoOpId) {
		this.grupoPrefijoOpId = grupoPrefijoOpId;
	}

}
