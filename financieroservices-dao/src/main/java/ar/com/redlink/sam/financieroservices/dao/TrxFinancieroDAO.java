/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  07/04/2015 - 21:51:39
 */

package ar.com.redlink.sam.financieroservices.dao;

import java.util.Date;
import java.util.List;

import ar.com.redlink.framework.persistence.dao.crud.RedLinkGenericDAO;
import ar.com.redlink.sam.financieroservices.entity.Intencion;
import ar.com.redlink.sam.financieroservices.entity.Pago;
import ar.com.redlink.sam.financieroservices.entity.SimpleTrxFinanciero;
import ar.com.redlink.sam.financieroservices.entity.TrxFinanciero;
import ar.com.redlink.sam.financieroservices.entity.TrxFinancieroValidation;

/**
 * Extension de DAO para {@link TrxFinanciero}.
 * 
 * @author aguerrea
 * 
 */
public interface TrxFinancieroDAO extends RedLinkGenericDAO<TrxFinanciero> {

	/**
	 * Metodo para realizar un insert explicito de un registro en
	 * TRX_FINANCIERO.
	 * 
	 * @param trxFinancieroDTO
	 * @return Resultado de la operacion.
	 * @throws Exception
	 */
	@Deprecated
	public Integer insert(SimpleTrxFinanciero trxFinancieroDTO)	throws Exception;

	/**
	 * Updatea un registro en TRX_Financiero al estado provisto acorde al ID de
	 * requerimiento original.
	 * 
	 * @param idRequerimientoOrig
	 * @param idRequerimientoBaja
	 * @param estadoTrx
	 * 
	 * @return Resultado del query.
	 * @throws Exception
	 */
	@Deprecated
	public Integer updateEntry(Long trxFinancieroId,String idRequerimientoBaja, String estadoTrx) throws Exception;
	
	
	/**
	 * @param trxFinancieroDTO
	 * @return Resultado de la operacion.
	 * @throws Exception
	 */
	public void registerWorkingKey(SimpleTrxFinanciero trxFinancieroDTO, Intencion in) throws Exception;
	
	public void validarTarjeta(SimpleTrxFinanciero trxFinancieroDTO, Intencion in) throws Exception;
	
	public void getBalance(SimpleTrxFinanciero trxFinancieroDTO, Intencion in) throws Exception;
	
	public void doDeposit(SimpleTrxFinanciero trxFinancieroDTO, Intencion in) throws Exception;
	
	public void doDebitPayment(SimpleTrxFinanciero trxFinancieroDTO, Intencion in) throws Exception;
	
	public void registerConsultaPEITrx(SimpleTrxFinanciero trxFinancieroDTO, Intencion in) throws Exception;
	
	public void doDebit(SimpleTrxFinanciero trxFinancieroDTO, Intencion in) throws Exception;
	
	public void doTransfer(SimpleTrxFinanciero trxFinancieroDTO, Intencion in) throws Exception;
	
	public Pago doPagoPei(SimpleTrxFinanciero trxFinancieroDTO, Intencion in) throws Exception;
	
	public Pago doDevolucionPagoPei(SimpleTrxFinanciero trxFinancieroDTO, Intencion in, boolean isParcial) throws Exception;
	
	public void doWithdraw(SimpleTrxFinanciero trxFinancieroDTO, Intencion in) throws Exception;
	
	public void updatePagoPei(Pago pago) throws Exception;
	
	public void updateDevolucionPagoPei(Pago pago, String idRequerimientoOrig, boolean isParcial) throws Exception;
	
	public void reverso(Long trxFinancieroId,String idRequerimientoBaja, String estadoTrx) throws Exception;
	
	public void anullDebit(Long trxFinancieroId,String idRequerimientoBaja, String estadoTrx, Intencion in) throws Exception;
	
	public void rollbackWithdraw(Long trxFinancieroId,String idRequerimientoBaja, String estadoTrx) throws Exception;
	
	public void cancelExtraction(Long trxFinancieroId,String idRequerimientoBaja, String estadoTrx, Intencion in) throws Exception;
	
	public void rollbackDeposit(Long trxFinancieroId,String idRequerimientoBaja, String estadoTrx, Intencion in) throws Exception;
	
	public void rollBackDebitPay(Long trxFinancieroId,String idRequerimientoBaja, String estadoTrx, Intencion in) throws Exception;
	
	public Intencion registrarInicioTransaccion(SimpleTrxFinanciero trxFinanciero) throws Exception;
	
	public void updateIntencion(Intencion in);
	
	/**
	 * Devuelve la suma del campo importe por el criterio definido.
	 * 
	 * @param filterField
	 * @param filterId
	 * @param dateFrom
	 * @param dateTo
	 * @param tokenizado
	 * @param prefijoOpId
	 * 
	 * @return importe actual en TRX_FINANCIERO.
	 * @throws Exception
	 * 
	 */
	public Double getSum(String filterField, String filterId, Date dateFrom,
			Date dateTo, String tokenizado, Long prefijoOpId) throws Exception;

	/**
	 * Devuelve el count dado por el criterio definido.
	 * 
	 * @param filterField
	 * @param filterId
	 * @param dateFrom
	 * @param dateTo
	 * @param tokenizado
	 * @param prefijoOpId
	 * 
	 * @return count de operaciones validas.
	 * @throws Exception
	 */
	public Integer getCount(String filterField, String filterId, Date dateFrom,
			Date dateTo, String tokenizado, Long prefijoOpId) throws Exception;

	/**
	 * Obtiene el numero de secuencia acorde al indicado y filtrando por
	 * terminalSamId y id de operacion.
	 * 
	 * @param seqNum
	 * @param terminalSamId
	 * @param notReversables
	 * 
	 * @return Resultados del query.
	 * @throws Exception
	 */
	@Deprecated
	public List<Long> getReversableOperationIds(String seqNum, String terminalSamId, String notReversables) throws Exception;
	
	/**
	 * Obtiene el numero de secuencia acorde al indicado y filtrando por
	 * terminalSamId y id de operacion.
	 * 
	 * @param seqNum
	 * @param terminalSamId
	 * @param notReversables
	 * 
	 * @return Resultados del query.
	 * @throws Exception
	 */
	public Long getReversableOperationIdsAct(String seqNum, String terminalSamId, String notReversables) throws Exception;
	

	/**
	 * Busca una TRX acorde a los datos provistos, devuelve el numero de seq si
	 * la encontro, sino, 0.
	 * 
	 * @param trxFinancieroValidation
	 * 
	 * @return numero de seq.
	 * @throws Exception
	 */
	@Deprecated
	public TrxFinanciero validateTrx(TrxFinancieroValidation trxFinancieroValidation) throws Exception;
	
	public Pago validateCancel(TrxFinancieroValidation trxFinancieroValidation) throws Exception;

	/**
	 * Obtiene el valor maximo de SeqNum de B24 para la terminal indicada.
	 * 
	 * @param terminalSamId
	 * @return numero de secuencia.
	 * @throws Exception
	 */
	@Deprecated
	public String getMaxSeqNumForTerm(String terminalSamId) throws Exception;
	
	public String getMaxSeqNumForTermAct(String terminalSamId) throws Exception;

	/**
	 * Valida un ID de requerimiento, checkeando su existencia en el registro de
	 * TRX_FINANCIERO. use @checkIdRequerimientoAct
	 * 
	 * @param idRequerimiento
	 * @throws Exception
	 */
	@Deprecated
	public List<Long> checkIdRequerimiento(String idRequerimiento) throws Exception;
	
	/**
	 * Valida un ID de requerimiento, checkeando su existencia en el registro de
	 * INTENCION, actualiza el metodo @checkIdRequerimiento.
	 * 
	 * @param idRequerimiento
	 * @throws Exception
	 */
	public boolean checkIdRequerimientoAct(String idRequerimiento) throws Exception;
	
	/**
	 * 
	 * @param idRequerimientoOrig
	 * @param idRequerimientoBaja
	 * @param estadoTrx
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	Integer updateEntryPei(String idRequerimientoOrig, String idRequerimientoBaja, String estadoTrx, String prefijoOpId) 
			throws Exception;

	/**
	 * Actualiza estado de la transcacci�n y el c�digo de Operaci�n PEI
	 * 
	 * @param trxFinanciero
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	Integer updatePei(SimpleTrxFinanciero trxFinanciero) throws Exception;

	/**
	 * Devuelve las transacciones que tienen estado_trx_id=3, use @checkIdRequerimientoOperacionPeiNuloAct
	 * 
	 * @param idRequerimiento
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	List<Long> checkIdRequerimientoOperacionPeiNulo(String idRequerimiento)	throws Exception;
	
	boolean checkIdRequerimientoOperacionPeiNuloAct(String idRequerimiento)	throws Exception;
	
	/**
	 * Actualiza estado de la transcacci�n y el c�digo de Operaci�n PEI a partir del id_requerimiento_baja 
	 * 
	 * @param trxFinanciero
	 * @return
	 * @throws Exception
	 */
	Integer updatePeiBaja(SimpleTrxFinanciero trxFinanciero) throws Exception;

}
