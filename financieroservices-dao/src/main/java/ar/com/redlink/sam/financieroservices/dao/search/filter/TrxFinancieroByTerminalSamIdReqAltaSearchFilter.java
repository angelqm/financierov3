/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  15/04/2015 - 22:36:46
 */

package ar.com.redlink.sam.financieroservices.dao.search.filter;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter;

/**
 * Filtro para obtener un TrxFinanciero filtrando por ID de terminal SAM y un ID
 * de requerimiento de alta.
 * 
 * @author aguerrea
 * 
 */
public class TrxFinancieroByTerminalSamIdReqAltaSearchFilter implements
		RedLinkSearchFilter {

	private Long terminalSamId;
	private String idRequerimientoAlta;

	/**
	 * Constructor default.
	 * 
	 * @param terminalSamId
	 * @param idRequerimientoAlta
	 */
	public TrxFinancieroByTerminalSamIdReqAltaSearchFilter(Long terminalSamId,
			String idRequerimientoAlta) {
		super();
		this.terminalSamId = terminalSamId;
		this.idRequerimientoAlta = idRequerimientoAlta;
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyFilters(org.hibernate.Criteria)
	 */
	@Override
	public void applyFilters(Criteria criteria) {
		criteria.add(Restrictions.eq("terminalSam.terminalSamId", terminalSamId));
		criteria.add(Restrictions
				.eq("idRequerimientoAlta", idRequerimientoAlta));
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyOrder(org.hibernate.Criteria)
	 */
	@Override
	public void applyOrder(Criteria criteria) {
		// No se requiere un orden.
	}

	/**
	 * @return the terminalSamId
	 */
	public Long getTerminalSamId() {
		return terminalSamId;
	}

	/**
	 * @param terminalSamId
	 *            the terminalSamId to set
	 */
	public void setTerminalSamId(Long terminalSamId) {
		this.terminalSamId = terminalSamId;
	}

	/**
	 * @return the idRequerimientoAlta
	 */
	public String getIdRequerimientoAlta() {
		return idRequerimientoAlta;
	}

	/**
	 * @param idRequerimientoAlta
	 *            the idRequerimientoAlta to set
	 */
	public void setIdRequerimientoAlta(String idRequerimientoAlta) {
		this.idRequerimientoAlta = idRequerimientoAlta;
	}

}
