/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  27/03/2015 - 23:07:15
 */

package ar.com.redlink.sam.financieroservices.dao.search.filter;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter;

/**
 * Filtro para obtener una terminalSam por ID de Entidad y ID de Servidor.
 * 
 * @author aguerrea
 * 
 */
public class TerminalSamByCodAndEntidadSearchFilter implements
		RedLinkSearchFilter {

	private Integer entidadId;
	private String terminalSamCod;

	/**
	 * Constructor default.
	 * 
	 * @param entidadId
	 * @param terminalSamCod
	 */
	public TerminalSamByCodAndEntidadSearchFilter(Integer entidadId, String terminalSamCod) {
		this.entidadId = entidadId;
		this.setTerminalSamCod(terminalSamCod);
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyFilters(org.hibernate.Criteria)
	 */
	@Override
	public void applyFilters(Criteria criteria) {
		criteria.add(Restrictions.sqlRestriction("ENTIDAD_ID = " + entidadId
				+ " AND COD_TERMINAL=" + getTerminalSamCod()));
	}
	
	//POR ANALIZAR
//	public void applyFilters(Criteria criteria) {
//        /*criteria.add(Restrictions.sqlRestriction("ENTIDAD_ID = " + entidadId
//                      + " AND COD_TERMINAL=" + getTerminalSamCod()));*/
//        criteria.add(Restrictions.sqlRestriction("ENTIDAD_ID = " + entidadId));
//        criteria.add(Restrictions.eq("codTerminal", getTerminalSamCod()));
//  }
		
	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyOrder(org.hibernate.Criteria)
	 */
	@Override
	public void applyOrder(Criteria criteria) {
		criteria.addOrder(Order.asc("estadoRegistro.estadoRegistroId"));
	}

	/**
	 * @return the entidadId
	 */
	public Integer getEntidadId() {
		return entidadId;
	}

	/**
	 * @param entidadId
	 *            the entidadId to set
	 */
	public void setEntidadId(Integer entidadId) {
		this.entidadId = entidadId;
	}

	/**
	 * @return the terminalSamCod
	 */
	public String getTerminalSamCod() {
		return terminalSamCod;
	}

	/**
	 * @param terminalSamCod the terminalSamCod to set
	 */
	public void setTerminalSamCod(String terminalSamCod) {
		this.terminalSamCod = terminalSamCod;
	}
}
