/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  27/03/2015 - 23:02:59
 */

package ar.com.redlink.sam.financieroservices.dao.search.filter;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter;

/**
 * Filtro para obtener una sucursal por ID.
 * 
 * @author aguerrea
 * 
 */
public class SucursalByIdSearchFilter implements RedLinkSearchFilter {

	private Long sucursalId;
	private Long servidorId;

	/**
	 * Constructor default.
	 * 
	 * @param sucursalId
	 */
	public SucursalByIdSearchFilter(Long sucursalId, Long servidorId) {
		this.sucursalId = sucursalId;
		this.servidorId = servidorId;
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyFilters(org.hibernate.Criteria)
	 */
	@Override
	public void applyFilters(Criteria criteria) {
		criteria.add(Restrictions.eq("sucursal.sucursalId", getSucursalId()));
		//criteria.add(Restrictions.eq("sucursal.servidorId", getServidorId()));
		// TODO: CAMBIO EN LA BUSQUEDA DE SERVIDOR
		criteria.add(Restrictions.sqlRestriction("SERVIDOR_ID = "
				+ servidorId ));
		
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyOrder(org.hibernate.Criteria)
	 */
	@Override
	public void applyOrder(Criteria criteria) {
		// No se requiere un orden.

	}

	/**
	 * @return the sucursalId
	 */
	public Long getSucursalId() {
		return sucursalId;
	}

	/**
	 * @param sucursalId
	 *            the sucursalId to set
	 */
	public void setSucursalId(Long sucursalId) {
		this.sucursalId = sucursalId;
	}

	/**
	 * @return the servidorId
	 */
	public Long getServidorId() {
		return servidorId;
	}

	/**
	 * @param servidorId the servidorId to set
	 */
	public void setServidorId(Long servidorId) {
		this.servidorId = servidorId;
	}

}
