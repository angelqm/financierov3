/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  01/04/2015 - 13:17:26
 */

package ar.com.redlink.sam.financieroservices.dao.search.filter;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter;
import ar.com.redlink.sam.financieroservices.entity.PerfilPrefijoGrupo;

/**
 * Obtiene {@link PerfilPrefijoGrupo} por ID y ID de PrefijoEntidad.
 * 
 * @author aguerrea
 * 
 */
public class PerfilPrefijoGrupoByIdAndPrefijoEntidadIdSearchFilter implements
		RedLinkSearchFilter {

	private Long perfilPrefijoId;
	private Long prefijoEntidadId;

	/**
	 * Constructor default.
	 * 
	 * @param perfilPrefijoId
	 * @param prefijoEntidadId
	 */
	public PerfilPrefijoGrupoByIdAndPrefijoEntidadIdSearchFilter(
			Long perfilPrefijoId, Long prefijoEntidadId) {
		this.perfilPrefijoId = perfilPrefijoId;
		this.prefijoEntidadId = prefijoEntidadId;
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyFilters(org.hibernate.Criteria)
	 */
	@Override
	public void applyFilters(Criteria criteria) {
		criteria.add(Restrictions.eq("perfilPrefijo.perfilPrefijoId",
				perfilPrefijoId));

		Criteria nestedCriteria = criteria.createCriteria("grupoPrefijo");
		Criteria nestedSubCriteria = nestedCriteria
				.createCriteria("grupoPrefijoEntidades");
		nestedSubCriteria.add(Restrictions.eq(
				"estadoRegistro.estadoRegistroId", (short) 0));
		nestedSubCriteria.add(Restrictions.eq(
				"prefijoEntidad.prefijoEntidadId", prefijoEntidadId));
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyOrder(org.hibernate.Criteria)
	 */
	@Override
	public void applyOrder(Criteria criteria) {
		// No se requiere un orden.
	}

	/**
	 * @return the perfilPrefijoId
	 */
	public Long getPerfilPrefijoId() {
		return perfilPrefijoId;
	}

	/**
	 * @param perfilPrefijoId
	 *            the perfilPrefijoId to set
	 */
	public void setPerfilPrefijoId(Long perfilPrefijoId) {
		this.perfilPrefijoId = perfilPrefijoId;
	}

	/**
	 * @return the prefijoEntidadId
	 */
	public Long getPrefijoEntidadId() {
		return prefijoEntidadId;
	}

	/**
	 * @param prefijoEntidadId
	 *            the prefijoEntidadId to set
	 */
	public void setPrefijoEntidadId(Long prefijoEntidadId) {
		this.prefijoEntidadId = prefijoEntidadId;
	}

}
