/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  26/03/2015 - 15:08:48
 */

package ar.com.redlink.sam.financieroservices.dao;

import ar.com.redlink.framework.persistence.dao.crud.RedLinkGenericDAO;
import ar.com.redlink.sam.financieroservices.entity.Prefijo;
import ar.com.redlink.sam.financieroservices.entity.PrefijoEntidad;

/**
 * Extension de RedLinkGenericDAO con operaciones especificas para
 * {@link Prefijo}.
 * 
 * @author aguerrea
 * 
 */
public interface PrefijoEntidadDAO extends RedLinkGenericDAO<PrefijoEntidad> {

	/**
	 * Realiza una busqueda de prefijos matcheando contra un PrefijoEntidad.
	 * 
	 * @param tarjeta
	 * @param entidad
	 * @return Prefijo obtenido.
	 * @throws Exception
	 */
	public PrefijoEntidad getPrefijoByTarjeta(String tarjeta, String entidad) throws Exception;
}
