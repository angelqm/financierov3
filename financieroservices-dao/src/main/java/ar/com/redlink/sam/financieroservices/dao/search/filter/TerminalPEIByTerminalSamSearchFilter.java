
package ar.com.redlink.sam.financieroservices.dao.search.filter;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter;

public class TerminalPEIByTerminalSamSearchFilter  implements RedLinkSearchFilter {

	private long terminalSamId;
	
	public TerminalPEIByTerminalSamSearchFilter(long terminalSamId) {
		this.terminalSamId = terminalSamId;
	}


	@Override
	public void applyFilters(Criteria criteria) {
		criteria.add(Restrictions.eq("terminalSamId",
				this.getTerminalSamId()));
		
	}


	@Override
	public void applyOrder(Criteria criteria) {
		// No hay un orden requerido.
	}


	/**
	 * @return the agenteId
	 */
	private long getTerminalSamId() {
		return terminalSamId;
	}
	
	
}
