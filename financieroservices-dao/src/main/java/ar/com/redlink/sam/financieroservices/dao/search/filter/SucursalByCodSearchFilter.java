/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  27/03/2015 - 22:57:38
 */

package ar.com.redlink.sam.financieroservices.dao.search.filter;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter;

/**
 * Filtro para la busqueda de una sucursal por codigo.
 * 
 * @author aguerrea
 * 
 */
public class SucursalByCodSearchFilter implements RedLinkSearchFilter {

	private String sucursalCod;

	/**
	 * Constructor default.
	 * 
	 * @param sucursalId
	 */
	public SucursalByCodSearchFilter(String sucursalCod) {
		this.setSucursalCod(sucursalCod);
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyFilters(org.hibernate.Criteria)
	 */
	@Override
	public void applyFilters(Criteria criteria) {
		criteria.add(Restrictions.sqlRestriction("COD_SUCURSAL = '"
				+ sucursalCod + "'"));
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyOrder(org.hibernate.Criteria)
	 */
	@Override
	public void applyOrder(Criteria criteria) {
		// No se requiere orden.
	}

	/**
	 * @return the sucursalCod
	 */
	public String getSucursalCod() {
		return sucursalCod;
	}

	/**
	 * @param sucursalCod
	 *            the sucursalCod to set
	 */
	public void setSucursalCod(String sucursalCod) {
		this.sucursalCod = sucursalCod;
	}

}
