/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  26/04/2015 - 18:04:16
 */

package ar.com.redlink.sam.financieroservices.dao.search.filter;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter;

/**
 * Filtro de busqueda para obtener las ultimas N operaciones correspondientes a
 * la terminal SAM indicada.
 * 
 * @author aguerrea
 * 
 */
public class TrxFinancieroByTerminalSamIdMaxResSearchFilter implements
		RedLinkSearchFilter {

	private Long terminalSamId;
	private Integer maxResults;

	/**
	 * Constructor default.
	 * 
	 * @param terminalSamId
	 * @param maxResults
	 */
	public TrxFinancieroByTerminalSamIdMaxResSearchFilter(Long terminalSamId,
			Integer maxResults) {
		this.setTerminalSamId(terminalSamId);
		this.setMaxResults(maxResults);
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyFilters(org.hibernate.Criteria)
	 */
	@Override
	public void applyFilters(Criteria criteria) {
		criteria.add(Restrictions
				.eq("terminalSam.terminalSamId", terminalSamId));
		criteria.setMaxResults(maxResults);

	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyOrder(org.hibernate.Criteria)
	 */
	@Override
	public void applyOrder(Criteria criteria) {
		criteria.addOrder(Order.desc("trxFinancieroId"));
	}

	/**
	 * @return the terminalSamId
	 */
	public Long getTerminalSamId() {
		return terminalSamId;
	}

	/**
	 * @param terminalSamId
	 *            the terminalSamId to set
	 */
	public void setTerminalSamId(Long terminalSamId) {
		this.terminalSamId = terminalSamId;
	}

	/**
	 * @return the maxResults
	 */
	public Integer getMaxResults() {
		return maxResults;
	}

	/**
	 * @param maxResults
	 *            the maxResults to set
	 */
	public void setMaxResults(Integer maxResults) {
		this.maxResults = maxResults;
	}

}
