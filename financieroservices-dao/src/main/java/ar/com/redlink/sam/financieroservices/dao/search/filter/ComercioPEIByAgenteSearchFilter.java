package ar.com.redlink.sam.financieroservices.dao.search.filter;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter;

public class ComercioPEIByAgenteSearchFilter  implements RedLinkSearchFilter {

	private long agenteId;
	
	public ComercioPEIByAgenteSearchFilter(long agenteId) {
		this.agenteId = agenteId;
	}


	@Override
	public void applyFilters(Criteria criteria) {
		criteria.add(Restrictions.eq("agenteId",
				this.getAgenteId()));
	}


	@Override
	public void applyOrder(Criteria criteria) {
		// No hay un orden requerido.
	}


	/**
	 * @return the agenteId
	 */
	private long getAgenteId() {
		return agenteId;
	}
	
	
}
