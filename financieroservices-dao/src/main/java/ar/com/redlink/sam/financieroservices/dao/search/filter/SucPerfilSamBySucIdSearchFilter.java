/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  01/04/2015 - 12:36:35
 */

package ar.com.redlink.sam.financieroservices.dao.search.filter;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter;
import ar.com.redlink.sam.financieroservices.entity.SucursalPerfilSam;

/**
 * Filtro para la obtencion de una entidad {@link SucursalPerfilSam} por Id de
 * Sucursal.
 * 
 * @author aguerrea
 * 
 */
public class SucPerfilSamBySucIdSearchFilter implements RedLinkSearchFilter {

	private Long sucursalId;

	/**
	 * Constructor default.
	 * 
	 * @param sucursalId
	 */
	public SucPerfilSamBySucIdSearchFilter(Long sucursalId) {
		this.sucursalId = sucursalId;
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyFilters(org.hibernate.Criteria)
	 */
	@Override
	public void applyFilters(Criteria criteria) {
		criteria.add(Restrictions.eq("sucursal.sucursalId", sucursalId));
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyOrder(org.hibernate.Criteria)
	 */
	@Override
	public void applyOrder(Criteria criteria) {
		// No se requiere un orden.
	}

	/**
	 * @return the sucursalId
	 */
	public Long getSucursalId() {
		return sucursalId;
	}

	/**
	 * @param sucursalId
	 *            the sucursalId to set
	 */
	public void setSucursalId(Long sucursalId) {
		this.sucursalId = sucursalId;
	}

}
