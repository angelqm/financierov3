/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: Hernan Di Tota
 * Date:  28/06/2016 - 23:02:59
 */

package ar.com.redlink.sam.financieroservices.dao.search.filter;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter;

/**
 * Filtro para obtener una sucursal por Codigo de sucursal,
 * y por ID de entidad.
 * 
 * @author Hernan Di Tota
 * 
 */
public class SucursalByCodAndEntidadSearchFilter implements RedLinkSearchFilter {

	private String sucursalCod;
	private Integer entidadId;

	/**
	 * Constructor default.
	 * 
	 * @param sucursalId
	 */
	public SucursalByCodAndEntidadSearchFilter(String sucursalCod, Integer entidadId) {
		//TODO: VER SI ESTO ES DEFINITIVO		
		this.sucursalCod = sucursalCod;
		//this.sucursalCod = StringUtils.leftPad(sucursalCod, 8, "0");
		this.entidadId = entidadId;
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyFilters(org.hibernate.Criteria)
	 */
	@Override
	public void applyFilters(Criteria criteria) {
		//TODO: VER SI ESTO ES DEFINITIVO
		String sucursalCod4 = "";
		String sucursalCod8 = "";
		
		if (sucursalCod.length()==4) {
			sucursalCod4 = sucursalCod;
			sucursalCod8 = StringUtils.leftPad(sucursalCod, 8, "0");
		} else {
			sucursalCod4 = StringUtils.substring(sucursalCod, 4);
			sucursalCod8 = sucursalCod;
		}
		
		criteria.add(Restrictions.sqlRestriction("(COD_SUCURSAL = '" + sucursalCod4 + "' OR COD_SUCURSAL = '" + sucursalCod8 + "')"));
		
		if (this.getEntidadId() != null) {
			criteria.add(Restrictions.eq("entidad.entidadId", getEntidadId()));
		}
	}

	/**
	 * @see ar.com.redlink.framework.persistence.dao.filter.RedLinkSearchFilter#applyOrder(org.hibernate.Criteria)
	 */
	@Override
	public void applyOrder(Criteria criteria) {
		// No se requiere un orden.

	}

	/**
	 * @return the sucursalCod
	 */
	public String getSucursalCod() {
		return sucursalCod;
	}

	/**
	 * @param sucursalCod the sucursalCod to set
	 */
	public void setSucursalCod(String sucursalCod) {
		this.sucursalCod = sucursalCod;
	}

	/**
	 * @return the entidadId
	 */
	public Integer getEntidadId() {
		return entidadId;
	}

	/**
	 * @param entidadId the entidadId to set
	 */
	public void setEntidadId(Integer entidadId) {
		this.entidadId = entidadId;
	}

}
