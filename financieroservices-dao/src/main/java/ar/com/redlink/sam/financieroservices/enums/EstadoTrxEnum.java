package ar.com.redlink.sam.financieroservices.enums;

public enum EstadoTrxEnum {

	ACTIVA(0, "Activa","Operación activa"),
	ANULADA(1, "Anulada","Operación anulada"),
	REVERSADA(2, "Reversada","Operación reversada por sincronización"),
	NULO(3,"Nulo","Operación PEI Nulo");
	
	private EstadoTrxEnum(int estadoTrxId, String nombre, String descripcion) {
		this.estadoTrxId = estadoTrxId;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}
	private int estadoTrxId;
	private String nombre;
	private String descripcion;
	/**
	 * @return the estadoTrxId
	 */
	public int getEstadoTrxId() {
		return estadoTrxId;
	}
	/**
	 * @param estadoTrxId the estadoTrxId to set
	 */
	public void setEstadoTrxId(int estadoTrxId) {
		this.estadoTrxId = estadoTrxId;
	}
	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}
