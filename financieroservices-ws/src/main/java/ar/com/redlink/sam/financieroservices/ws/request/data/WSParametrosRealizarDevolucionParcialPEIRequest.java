package ar.com.redlink.sam.financieroservices.ws.request.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * Wrapper para la parametria de la operacion de transferencias PEI.
 * 
 * @author EXT-IBANEZMA
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSParametrosRealizarDevolucionParcialPEIRequest extends WSParametrosRealizarDevolucionPEIRequest {

	private String moneda;

	/**
	 * @return the moneda
	 */
	public String getMoneda() {
		return moneda;
	}

	/**
	 * @param moneda
	 *            the moneda to set
	 */
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

}
