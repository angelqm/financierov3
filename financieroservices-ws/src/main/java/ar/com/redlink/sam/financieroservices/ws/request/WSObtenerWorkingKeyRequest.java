/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosObtenerWorkingKeyRequest;

/**
 * Wrapper para los parametros del request de obtencion de working key para
 * encripcion.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSObtenerWorkingKeyRequest extends WSAbstractRequest {

	@XmlElement(name = "obtenerWorkingKeyData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosObtenerWorkingKeyRequest obtenerWorkingKeyDataRequest;

	/**
	 * @return the obtenerWorkingKeyDataRequest
	 */
	public WSParametrosObtenerWorkingKeyRequest getObtenerWorkingKeyDataRequest() {
		return obtenerWorkingKeyDataRequest;
	}

	/**
	 * @param obtenerWorkingKeyDataRequest
	 *            the obtenerWorkingKeyDataRequest to set
	 */
	public void setObtenerWorkingKeyDataRequest(
			WSParametrosObtenerWorkingKeyRequest obtenerWorkingKeyDataRequest) {
		this.obtenerWorkingKeyDataRequest = obtenerWorkingKeyDataRequest;
	}

}
