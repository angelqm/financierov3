/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.ws.request.base;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;

/**
 * AbstractRequest con definicion del header base.
 * 
 * @author David Cisneros
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class WSAbstractRequest {

	@XmlElement(name = "cabecera", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	protected WSHeaderRequest header;

	public void setHeader(WSHeaderRequest header) {
		this.header = header;
	}

	public WSHeaderRequest getHeader() {
		return header;
	}

}
