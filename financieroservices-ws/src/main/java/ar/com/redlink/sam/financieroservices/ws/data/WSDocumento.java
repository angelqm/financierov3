/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: he_e90104
 * Date:  13/05/2019 
 */

package ar.com.redlink.sam.financieroservices.ws.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Representacion de una cuenta.
 * 
 * @author he_e90104
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSDocumento {

	@XmlElement(nillable = false, required = true)
	private String tipo;

	@XmlElement(nillable = false, required = true)
	private String numero;

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

}
