/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosAnularExtraccionRequest;

/**
 * Wrapper para los parametros del request de realizacion de anulacion.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSAnularExtraccionRequest extends WSAbstractRequest {

	@XmlElement(name = "anularExtraccionData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosAnularExtraccionRequest anularExtraccionRequest;

	/**
	 * @return the anularExtraccionRequest
	 */
	public WSParametrosAnularExtraccionRequest getAnularExtraccionRequest() {
		return anularExtraccionRequest;
	}

	/**
	 * @param anularExtraccionRequest
	 *            the anularExtraccionRequest to set
	 */
	public void setAnularExtraccionRequest(
			WSParametrosAnularExtraccionRequest realizarAnulacionRequest) {
		this.anularExtraccionRequest = realizarAnulacionRequest;
	}

}
