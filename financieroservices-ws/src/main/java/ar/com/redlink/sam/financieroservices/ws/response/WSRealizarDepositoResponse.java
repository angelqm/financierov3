/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  11/03/2015 - 21:47:12
 */

package ar.com.redlink.sam.financieroservices.ws.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.response.base.WSAbstractResponse;

/**
 * Wrapper para la respuesta de operacion de deposito.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSRealizarDepositoResponse extends WSAbstractResponse {

	@XmlElement(nillable = false, required = true)
	private String secuenciaOn;

	@XmlElement(nillable = false, required = true)
	private String anticipo;

	@XmlElement(nillable = false, required = true)
	private String saldo;

	@XmlElement(nillable = false, required = true)
	private String disponible;

	@XmlElement(nillable = false, required = true)
	private int extraccionesDisp;

	@XmlElement(nillable = false, required = true)
	private String TNA;

	@XmlElement(nillable = false, required = true)
	private String TEM;

	@XmlElement(nillable = false, required = true)
	private int cantidadCuotas;

	@XmlElement(nillable = false, required = true)
	private String disponibleEfCuotas;

	@XmlElement(nillable = false, required = true)
	private String fechaOn;

	@XmlElement(nillable = false, required = true)
	private String ticket;

	/**
	 * @return the secuenciaOn
	 */
	public String getSecuenciaOn() {
		return secuenciaOn;
	}

	/**
	 * @param secuenciaOn
	 *            the secuenciaOn to set
	 */
	public void setSecuenciaOn(String secuenciaOn) {
		this.secuenciaOn = secuenciaOn;
	}

	/**
	 * @return the anticipo
	 */
	public String getAnticipo() {
		return anticipo;
	}

	/**
	 * @param anticipo
	 *            the anticipo to set
	 */
	public void setAnticipo(String anticipo) {
		this.anticipo = anticipo;
	}

	/**
	 * @return the saldo
	 */
	public String getSaldo() {
		return saldo;
	}

	/**
	 * @param saldo
	 *            the saldo to set
	 */
	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}

	/**
	 * @return the disponible
	 */
	public String getDisponible() {
		return disponible;
	}

	/**
	 * @param disponible
	 *            the disponible to set
	 */
	public void setDisponible(String disponible) {
		this.disponible = disponible;
	}

	/**
	 * @return the extraccionesDisp
	 */
	public int getExtraccionesDisp() {
		return extraccionesDisp;
	}

	/**
	 * @param extraccionesDisp
	 *            the extraccionesDisp to set
	 */
	public void setExtraccionesDisp(int extraccionesDisp) {
		this.extraccionesDisp = extraccionesDisp;
	}

	/**
	 * @return the tNA
	 */
	public String getTNA() {
		return TNA;
	}

	/**
	 * @param tNA
	 *            the tNA to set
	 */
	public void setTNA(String tNA) {
		TNA = tNA;
	}

	/**
	 * @return the tEM
	 */
	public String getTEM() {
		return TEM;
	}

	/**
	 * @param tEM
	 *            the tEM to set
	 */
	public void setTEM(String tEM) {
		TEM = tEM;
	}

	/**
	 * @return the cantidadCuotas
	 */
	public int getCantidadCuotas() {
		return cantidadCuotas;
	}

	/**
	 * @param cantidadCuotas
	 *            the cantidadCuotas to set
	 */
	public void setCantidadCuotas(int cantidadCuotas) {
		this.cantidadCuotas = cantidadCuotas;
	}

	/**
	 * @return the disponibleEfCuotas
	 */
	public String getDisponibleEfCuotas() {
		return disponibleEfCuotas;
	}

	/**
	 * @param disponibleEfCuotas
	 *            the disponibleEfCuotas to set
	 */
	public void setDisponibleEfCuotas(String disponibleEfCuotas) {
		this.disponibleEfCuotas = disponibleEfCuotas;
	}

	/**
	 * @return the ticket
	 */
	public String getTicket() {
		return ticket;
	}

	/**
	 * @param ticket
	 *            the ticket to set
	 */
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	/**
	 * @return the fechaOn
	 */
	public String getFechaOn() {
		return fechaOn;
	}

	/**
	 * @param fechaOn
	 *            the fechaOn to set
	 */
	public void setFechaOn(String fechaOn) {
		this.fechaOn = fechaOn;
	}
}
