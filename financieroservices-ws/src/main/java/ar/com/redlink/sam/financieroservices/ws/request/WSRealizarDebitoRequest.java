/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  10/03/2015 - 22:56:22
 */

package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosRealizarDebitoRequest;

/**
 * Wrapper para un request de extraccion para debito.
 * 
 * @author aguerrea
 * 
 */
public class WSRealizarDebitoRequest extends WSAbstractRequest {

	@XmlElement(name = "realizarDebitoData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosRealizarDebitoRequest realizarDebitoRequest;

	/**
	 * @return the realizarDebitoRequest
	 */
	public WSParametrosRealizarDebitoRequest getRealizarDebitoRequest() {
		return realizarDebitoRequest;
	}

	/**
	 * @param realizarDebitoRequest
	 *            the realizarDebitoRequest to set
	 */
	public void setRealizarDebitoRequest(
			WSParametrosRealizarDebitoRequest realizarDebitoRequest) {
		this.realizarDebitoRequest = realizarDebitoRequest;
	}

}
