/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.ws.request.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import ar.com.redlink.sam.financieroservices.ws.request.WSEnrolamientoRequest;

/**
 * Parametria para {@link WSEnrolamientoRequest}.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSParametrosEnrolamientoRequest {

//	protected String identTerminal;
//
//	protected String identDispositivo;
//
//	/**
//	 * @return the identTerminal
//	 */
//	public String getIdentTerminal() {
//		return identTerminal;
//	}
//
//	/**
//	 * @param identTerminal
//	 *            the identTerminal to set
//	 */
//	public void setIdentTerminal(String identTerminal) {
//		this.identTerminal = identTerminal;
//	}
//
//	/**
//	 * @return the identDispositivo
//	 */
//	public String getIdentDispositivo() {
//		return identDispositivo;
//	}
//
//	/**
//	 * @param identDispositivo
//	 *            the identDispositivo to set
//	 */
//	public void setIdentDispositivo(String identDispositivo) {
//		this.identDispositivo = identDispositivo;
//	}

}
