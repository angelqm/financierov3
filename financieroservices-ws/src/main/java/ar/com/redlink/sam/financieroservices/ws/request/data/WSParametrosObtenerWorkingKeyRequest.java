package ar.com.redlink.sam.financieroservices.ws.request.data;

import ar.com.redlink.sam.financieroservices.ws.request.WSObtenerWorkingKeyRequest;

/**
 * Parametria de {@link WSObtenerWorkingKeyRequest}.
 * 
 * @author aguerrea
 * 
 */
public class WSParametrosObtenerWorkingKeyRequest {

	private String secuenciaOn;
	
	private String tipo;

	/**
	 * @return the secuenciaOn
	 */
	public String getSecuenciaOn() {
		return secuenciaOn;
	}

	/**
	 * @param secuenciaOn the secuenciaOn to set
	 */
	public void setSecuenciaOn(String secuenciaOn) {
		this.secuenciaOn = secuenciaOn;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo
	 *            the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
