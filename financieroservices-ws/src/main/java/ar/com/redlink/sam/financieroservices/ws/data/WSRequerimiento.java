/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  26/01/2015 - 22:22:20
 */

package ar.com.redlink.sam.financieroservices.ws.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Representacion de un requerimiento.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSRequerimiento {

	@XmlElement(nillable = false, required = true)
	private String idRequerimiento;

	@XmlElement(nillable = false, required = true)
	private String codigoOperacion;

	@XmlElement(nillable = false, required = true)
	private String nombreOperacion;

	@XmlElement(nillable = false, required = true)
	private double importeOperacion;

	@XmlElement(nillable = false, required = true)
	private String secuenciaOnOperacion;

	@XmlElement(nillable = false, required = true)
	private String fechaOnOperacion;

	@XmlElement(nillable = false, required = true)
	private String fechaHoraOperacion;

	@XmlElement(nillable = false, required = true)
	private String estadoRequerimiento;

	/**
	 * @return the idRequerimiento
	 */
	public String getIdRequerimiento() {
		return idRequerimiento;
	}

	/**
	 * @param idRequerimiento
	 *            the idRequerimiento to set
	 */
	public void setIdRequerimiento(String idRequerimiento) {
		this.idRequerimiento = idRequerimiento;
	}

	/**
	 * @return the codigoOperacion
	 */
	public String getCodigoOperacion() {
		return codigoOperacion;
	}

	/**
	 * @param codigoOperacion
	 *            the codigoOperacion to set
	 */
	public void setCodigoOperacion(String codigoOperacion) {
		this.codigoOperacion = codigoOperacion;
	}

	/**
	 * @return the nombreOperacion
	 */
	public String getNombreOperacion() {
		return nombreOperacion;
	}

	/**
	 * @param nombreOperacion
	 *            the nombreOperacion to set
	 */
	public void setNombreOperacion(String nombreOperacion) {
		this.nombreOperacion = nombreOperacion;
	}

	/**
	 * @return the importeOperacion
	 */
	public double getImporteOperacion() {
		return importeOperacion;
	}

	/**
	 * @param importeOperacion
	 *            the importeOperacion to set
	 */
	public void setImporteOperacion(double importeOperacion) {
		this.importeOperacion = importeOperacion;
	}

	/**
	 * @return the secuenciaOnOperacion
	 */
	public String getSecuenciaOnOperacion() {
		return secuenciaOnOperacion;
	}

	/**
	 * @param secuenciaOnOperacion
	 *            the secuenciaOnOperacion to set
	 */
	public void setSecuenciaOnOperacion(String secuenciaOnOperacion) {
		this.secuenciaOnOperacion = secuenciaOnOperacion;
	}

	/**
	 * @return the fechaOnOperacion
	 */
	public String getFechaOnOperacion() {
		return fechaOnOperacion;
	}

	/**
	 * @param fechaOnOperacion
	 *            the fechaOnOperacion to set
	 */
	public void setFechaOnOperacion(String fechaOnOperacion) {
		this.fechaOnOperacion = fechaOnOperacion;
	}

	/**
	 * @return the fechaHoraOperacion
	 */
	public String getFechaHoraOperacion() {
		return fechaHoraOperacion;
	}

	/**
	 * @param fechaHoraOperacion
	 *            the fechaHoraOperacion to set
	 */
	public void setFechaHoraOperacion(String fechaHoraOperacion) {
		this.fechaHoraOperacion = fechaHoraOperacion;
	}

	/**
	 * @return the estadoRequerimiento
	 */
	public String getEstadoRequerimiento() {
		return estadoRequerimiento;
	}

	/**
	 * @param estadoRequerimiento
	 *            the estadoRequerimiento to set
	 */
	public void setEstadoRequerimiento(String estadoRequerimiento) {
		this.estadoRequerimiento = estadoRequerimiento;
	}

}
