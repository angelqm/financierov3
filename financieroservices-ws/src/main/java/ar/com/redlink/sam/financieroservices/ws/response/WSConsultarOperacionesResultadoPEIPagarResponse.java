package ar.com.redlink.sam.financieroservices.ws.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import ar.com.redlink.sam.financieroservices.ws.data.WSOperacionPEI;

/**
 * 
 * @author EXT-IBANEZMA
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSConsultarOperacionesResultadoPEIPagarResponse {

	@XmlElement(nillable = false, required = true)
	private String secuenciaOn;

	@XmlElement(nillable = false, required = true)
	private Integer total;

	@XmlElementWrapper(name = "operaciones", nillable = false, required = true)
	private List<WSOperacionPEI> operacion;

	/**
	 * @return the secuenciaOn
	 */
	public String getSecuenciaOn() {
		return secuenciaOn;
	}

	/**
	 * @param secuenciaOn
	 *            the secuenciaOn to set
	 */
	public void setSecuenciaOn(String secuenciaOn) {
		this.secuenciaOn = secuenciaOn;
	}

	/**
	 * @return the total
	 */
	public Integer getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(Integer total) {
		this.total = total;
	}

	/**
	 * @return the operacion
	 */
	public List<WSOperacionPEI> getOperacion() {
		return operacion;
	}

	/**
	 * @param operacion
	 *            the operacion to set
	 */
	public void setOperacion(List<WSOperacionPEI> operacion) {
		this.operacion = operacion;
	}

	// @XmlElement(nillable = false, required = true)
	// private String idoperacion;
	//
	// @XmlElement(nillable = false, required = true)
	// private String estado;
	//
	// @XmlElement(nillable = false, required = true)
	// private String fecha;

}
