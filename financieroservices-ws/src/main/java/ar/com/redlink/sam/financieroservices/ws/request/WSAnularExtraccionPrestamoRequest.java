/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosAnularExtraccionPrestamoRequest;

/**
 * Wrapper para los parametros del request de anulacion de extraccion de
 * prestamo.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSAnularExtraccionPrestamoRequest extends WSAbstractRequest {

	@XmlElement(name = "anularExtraccionPrestamoData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosAnularExtraccionPrestamoRequest anularExtraccionPrestamoRequest;

	/**
	 * @return the anularExtraccionPrestamoRequest
	 */
	public WSParametrosAnularExtraccionPrestamoRequest getAnularExtraccionPrestamoRequest() {
		return anularExtraccionPrestamoRequest;
	}

	/**
	 * @param anularExtraccionPrestamoRequest
	 *            the anularExtraccionPrestamoRequest to set
	 */
	public void setAnularExtraccionPrestamoRequest(
			WSParametrosAnularExtraccionPrestamoRequest anularExtraccionPrestamoRequest) {
		this.anularExtraccionPrestamoRequest = anularExtraccionPrestamoRequest;
	}

}
