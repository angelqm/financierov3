/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  26/01/2015 - 22:17:26
 */

package ar.com.redlink.sam.financieroservices.ws.request.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import ar.com.redlink.sam.financieroservices.ws.request.WSObtenerOperacionesRequest;

/**
 * Parametria de {@link WSObtenerOperacionesRequest}.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSParametrosObtenerOperacionesRequest {

}
