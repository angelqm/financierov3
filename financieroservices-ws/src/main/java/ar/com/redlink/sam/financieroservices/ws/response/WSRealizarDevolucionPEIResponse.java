package ar.com.redlink.sam.financieroservices.ws.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.response.base.WSAbstractResponse;

/**
 * Respuesta del metodo de realizacion de devolucion.
 * 
 * @author EXT-IBANEZMA
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSRealizarDevolucionPEIResponse extends WSAbstractResponse {

	@XmlElement(name = "resultado", nillable = false, required = false)
	private WSRealizarDevolucionResultadoPEIResponse resultado;

	/**
	 * @return the resultado
	 */
	public WSRealizarDevolucionResultadoPEIResponse getResultado() {
		return resultado;
	}

	/**
	 * @param resultado the resultado to set
	 */
	public void setResultado(WSRealizarDevolucionResultadoPEIResponse resultado) {
		this.resultado = resultado;
	}
	
	

}
