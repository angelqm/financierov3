/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.ws.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;

import ar.com.redlink.sam.financieroservices.ws.data.WSRequerimiento;
import ar.com.redlink.sam.financieroservices.ws.response.base.WSAbstractResponse;

/**
 * Wrapper para los parametros de retorno del request de obtencion de ultimas
 * operaciones.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSObtenerOperacionesResponse extends WSAbstractResponse {

	@XmlElementWrapper(name = "requerimientos", nillable = false, required = true)
	private List<WSRequerimiento> requerimientos;
	
	

	/**
	 * @return the requerimientos
	 */
	public List<WSRequerimiento> getRequerimientos() {
		return requerimientos;
	}

	/**
	 * @param requerimientos
	 *            the requerimientos to set
	 */
	public void setRequerimientos(List<WSRequerimiento> requerimientos) {
		this.requerimientos = requerimientos;
	}

}
