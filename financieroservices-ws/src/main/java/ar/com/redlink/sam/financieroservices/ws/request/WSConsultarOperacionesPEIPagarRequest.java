package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosConsultarOperacionesPEIPagarRequest;

/**
 * 
 * @author EXT-IBANEZMA
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSConsultarOperacionesPEIPagarRequest extends WSAbstractRequest {

	@XmlElement(name = "consultarOperacionesPEIPagarData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosConsultarOperacionesPEIPagarRequest consultarOperacionesPEIPagarRequest;

	/**
	 * @return the consultarOperacionesPEIPagarRequest
	 */
	public WSParametrosConsultarOperacionesPEIPagarRequest getConsultarOperacionesPEIPagarRequest() {
		return consultarOperacionesPEIPagarRequest;
	}

	/**
	 * @param consultarOperacionesPEIPagarRequest the consultarOperacionesPEIPagarRequest to set
	 */
	public void setConsultarOperacionesPEIPagarRequest(
			WSParametrosConsultarOperacionesPEIPagarRequest consultarOperacionesPEIPagarRequest) {
		this.consultarOperacionesPEIPagarRequest = consultarOperacionesPEIPagarRequest;
	}


}
