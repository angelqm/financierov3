
package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosAnularPagoDebitoRequest;

@XmlAccessorType(XmlAccessType.FIELD)
public class WSAnularPagoDebitoRequest extends WSAbstractRequest {

	@XmlElement(name = "anularPagoDebitoData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosAnularPagoDebitoRequest anularPagoDebitoRequest;

	/**
	 * @return the anularDebitoRequest
	 */
	public WSParametrosAnularPagoDebitoRequest getAnularPagoDebitoRequest() {
		return anularPagoDebitoRequest;
	}

	/**
	 * @param anularDebitoRequest
	 *            the anularDebitoRequest to set
	 */
	public void setAnularPagoDebitoRequest(WSParametrosAnularPagoDebitoRequest anularPagoDebitoRequest) {
		this.anularPagoDebitoRequest = anularPagoDebitoRequest;
	}
}
