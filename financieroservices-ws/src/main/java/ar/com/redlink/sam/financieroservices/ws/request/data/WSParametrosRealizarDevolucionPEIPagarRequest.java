package ar.com.redlink.sam.financieroservices.ws.request.data;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.data.WSCuentaRecaudadora;
import ar.com.redlink.sam.financieroservices.ws.data.WSDocumento;
import ar.com.redlink.sam.financieroservices.ws.data.WSEntidad;
import ar.com.redlink.sam.financieroservices.ws.data.WSTarjeta;

/**
 * Wrapper para la parametria de la operacion de devolucion PEI-PAGAR.
 * 
 * @author AQM
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSParametrosRealizarDevolucionPEIPagarRequest {
	
	/*ESTO es del metodo realizarDevolucionPagoPEIPagar sus propiedades*/
	
	private String canal;
	
	@XmlElement(name = "cuentaDestino")
	private WSCuentaRecaudadora cuentaDestino;
		
	@XmlElement(name = "documento")
	private WSDocumento documento;
	
	@XmlElement(name = "entidad")
	private WSEntidad entidad;
	
	private String idReferenciaOperacion;
	
	private String idReferenciaTrx;
	
	private String idReferenciaTrxOrigen;
	
	private String operacionOrigen;

	private String panEntidad;
	
	private String posEntryMode;
	
	@XmlElement(name = "tarjeta")
	private WSTarjeta tarjeta;
		
	private String secuenciaOn;
	
	private BigDecimal importe;
	
	private String moneda;
	
	//IdRequerimiento con que fue hecha el pago sirve para
	//Busrcar en USR_SRVFINANCIERO.INTENCION: ID_REQUERIMIENTO
	private String idRequerimientoOrig;

	/**
	 * @return the canal
	 */
	public String getCanal() {
		return canal;
	}

	/**
	 * @param canal the canal to set
	 */
	public void setCanal(String canal) {
		this.canal = canal;
	}

	/**
	 * @return the cuentaDestino
	 */
	public WSCuentaRecaudadora getCuentaDestino() {
		return cuentaDestino;
	}

	/**
	 * @param cuentaDestino the cuentaDestino to set
	 */
	public void setCuentaDestino(WSCuentaRecaudadora cuentaDestino) {
		this.cuentaDestino = cuentaDestino;
	}

	/**
	 * @return the documento
	 */
	public WSDocumento getDocumento() {
		return documento;
	}

	/**
	 * @param documento the documento to set
	 */
	public void setDocumento(WSDocumento documento) {
		this.documento = documento;
	}

	/**
	 * @return the entidad
	 */
	public WSEntidad getEntidad() {
		return entidad;
	}

	/**
	 * @param entidad the entidad to set
	 */
	public void setEntidad(WSEntidad entidad) {
		this.entidad = entidad;
	}

	/**
	 * @return the idReferenciaOperacion
	 */
	public String getIdReferenciaOperacion() {
		return idReferenciaOperacion;
	}

	/**
	 * @param idReferenciaOperacion the idReferenciaOperacion to set
	 */
	public void setIdReferenciaOperacion(String idReferenciaOperacion) {
		this.idReferenciaOperacion = idReferenciaOperacion;
	}

	/**
	 * @return the idReferenciaTrx
	 */
	public String getIdReferenciaTrx() {
		return idReferenciaTrx;
	}

	/**
	 * @param idReferenciaTrx the idReferenciaTrx to set
	 */
	public void setIdReferenciaTrx(String idReferenciaTrx) {
		this.idReferenciaTrx = idReferenciaTrx;
	}

	/**
	 * @return the operacionOrigen
	 */
	public String getOperacionOrigen() {
		return operacionOrigen;
	}

	/**
	 * @param operacionOrigen the operacionOrigen to set
	 */
	public void setOperacionOrigen(String operacionOrigen) {
		this.operacionOrigen = operacionOrigen;
	}

	/**
	 * @return the panEntidad
	 */
	public String getPanEntidad() {
		return panEntidad;
	}

	/**
	 * @param panEntidad the panEntidad to set
	 */
	public void setPanEntidad(String panEntidad) {
		this.panEntidad = panEntidad;
	}

	/**
	 * @return the posEntryMode
	 */
	public String getPosEntryMode() {
		return posEntryMode;
	}

	/**
	 * @param posEntryMode the posEntryMode to set
	 */
	public void setPosEntryMode(String posEntryMode) {
		this.posEntryMode = posEntryMode;
	}

	/**
	 * @return the tarjeta
	 */
	public WSTarjeta getTarjeta() {
		return tarjeta;
	}

	/**
	 * @param tarjeta the tarjeta to set
	 */
	public void setTarjeta(WSTarjeta tarjeta) {
		this.tarjeta = tarjeta;
	}

	/**
	 * @return the secuenciaOn
	 */
	public String getSecuenciaOn() {
		return secuenciaOn;
	}

	/**
	 * @param secuenciaOn the secuenciaOn to set
	 */
	public void setSecuenciaOn(String secuenciaOn) {
		this.secuenciaOn = secuenciaOn;
	}



	/**
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}

	/**
	 * @param importe the importe to set
	 */
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	/**
	 * @return the moneda
	 */
	public String getMoneda() {
		return moneda;
	}

	/**
	 * @param moneda the moneda to set
	 */
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	/**
	 * @return the idReferenciaTrxOrigen
	 */
	public String getIdReferenciaTrxOrigen() {
		return idReferenciaTrxOrigen;
	}

	/**
	 * @param idReferenciaTrxOrigen the idReferenciaTrxOrigen to set
	 */
	public void setIdReferenciaTrxOrigen(String idReferenciaTrxOrigen) {
		this.idReferenciaTrxOrigen = idReferenciaTrxOrigen;
	}

	/**
	 * @return the idRequerimientoOrig
	 */
	public String getIdRequerimientoOrig() {
		return idRequerimientoOrig;
	}

	/**
	 * @param idRequerimientoOrig the idRequerimientoOrig to set
	 */
	public void setIdRequerimientoOrig(String idRequerimientoOrig) {
		this.idRequerimientoOrig = idRequerimientoOrig;
	}

	
}
