/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  19/03/2015 - 23:05:44
 */

package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosRealizarExtraccionPrestamoRequest;

/**
 * Request para la operacion de realizar extraccion de prestamo.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSRealizarExtraccionPrestamoRequest extends WSAbstractRequest {

	@XmlElement(name = "realizarExtraccionPrestamoData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosRealizarExtraccionPrestamoRequest realizarExtraccionPrestamoRequest;

	/**
	 * @return the realizarExtraccionPrestamoRequest
	 */
	public WSParametrosRealizarExtraccionPrestamoRequest getRealizarExtraccionPrestamoRequest() {
		return realizarExtraccionPrestamoRequest;
	}

	/**
	 * @param realizarExtraccionPrestamoRequest
	 *            the realizarExtraccionPrestamoRequest to set
	 */
	public void setRealizarExtraccionPrestamoRequest(
			WSParametrosRealizarExtraccionPrestamoRequest realizarExtraccionPrestamoRequest) {
		this.realizarExtraccionPrestamoRequest = realizarExtraccionPrestamoRequest;
	}

}
