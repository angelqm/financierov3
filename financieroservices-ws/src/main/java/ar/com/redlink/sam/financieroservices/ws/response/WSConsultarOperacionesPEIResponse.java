package ar.com.redlink.sam.financieroservices.ws.response;

import ar.com.redlink.sam.financieroservices.ws.response.base.WSAbstractResponse;

/**
 * 
 * @author EXT-IBANEZMA
 *
 */
public class WSConsultarOperacionesPEIResponse extends WSAbstractResponse {	

	private WSConsultarOperacionesResultadoPEIResponse resultado;	

	/**
	 * @return the resultado
	 */
	public WSConsultarOperacionesResultadoPEIResponse getResultado() {
		return resultado;
	}

	/**
	 * @param resultado
	 *            the resultado to set
	 */
	public void setResultado(WSConsultarOperacionesResultadoPEIResponse resultado) {
		this.resultado = resultado;
	}

}
