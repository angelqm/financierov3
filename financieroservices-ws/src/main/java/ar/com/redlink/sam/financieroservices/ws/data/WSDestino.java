/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: he_e90104
 * Date:  13/05/2019
 */

package ar.com.redlink.sam.financieroservices.ws.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Representacion de una cuenta.
 * 
 * @author he_e90104
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSDestino {
	
	@XmlElement(name = "cuentaRecaudadora")
	private WSCuentaRecaudadora cuentaRecaudadora;

	@XmlElement(name = "entidad")
	private WSEntidad entidad;
	
	/**
	 * @return the cuentaRecaudadora
	 */
	public WSCuentaRecaudadora getCuentaRecaudadora() {
		return cuentaRecaudadora;
	}

	/**
	 * @param cuentaRecaudadora the cuentaRecaudadora to set
	 */
	public void setCuentaRecaudadora(WSCuentaRecaudadora cuentaRecaudadora) {
		this.cuentaRecaudadora = cuentaRecaudadora;
	}

	/**
	 * @return the entidad
	 */
	public WSEntidad getEntidad() {
		return entidad;
	}

	/**
	 * @param entidad the entidad to set
	 */
	public void setEntidad(WSEntidad entidad) {
		this.entidad = entidad;
	}

	
}
