/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.ws.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.response.base.WSAbstractResponse;

/**
 * Wrapper para los parametros de retorno del request de obtencion de working
 * key.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSObtenerWorkingKeyResponse extends WSAbstractResponse {

	@XmlElement(nillable = false, required = true)
	private String workingKey;

	@XmlElement(nillable = false, required = true)
	private String terminalB24;

	@XmlElement(nillable = false, required = true)
	private String tipoTerminalB24;

	@XmlElement(nillable = false, required = true)
	private String tipoEncripcion;

	@XmlElement(nillable = false, required = true)
	private String fechaOn;

	@XmlElement(nillable = false, required = true)
	private String secuenciaOn;

	/**
	 * @return the workingKey
	 */
	public String getWorkingKey() {
		return workingKey;
	}

	/**
	 * @param workingKey
	 *            the workingKey to set
	 */
	public void setWorkingKey(String workingKey) {
		this.workingKey = workingKey;
	}

	/**
	 * @return the tipoTerminalB24
	 */
	public String getTipoTerminalB24() {
		return tipoTerminalB24;
	}

	/**
	 * @param tipoTerminalB24
	 *            the tipoTerminalB24 to set
	 */
	public void setTipoTerminalB24(String tipoTerminalB24) {
		this.tipoTerminalB24 = tipoTerminalB24;
	}

	/**
	 * @return the tipoEncripcion
	 */
	public String getTipoEncripcion() {
		return tipoEncripcion;
	}

	/**
	 * @param tipoEncripcion
	 *            the tipoEncripcion to set
	 */
	public void setTipoEncripcion(String tipoEncripcion) {
		this.tipoEncripcion = tipoEncripcion;
	}

	/**
	 * @return the terminalB24
	 */
	public String getTerminalB24() {
		return terminalB24;
	}

	/**
	 * @param terminalB24
	 *            the terminalB24 to set
	 */
	public void setTerminalB24(String terminalB24) {
		this.terminalB24 = terminalB24;
	}

	/**
	 * @return the fechaOn
	 */
	public String getFechaOn() {
		return fechaOn;
	}

	/**
	 * @param fechaOn
	 *            the fechaOn to set
	 */
	public void setFechaOn(String fechaOn) {
		this.fechaOn = fechaOn;
	}

	/**
	 * @return the secuenciaOn
	 */
	public String getSecuenciaOn() {
		return secuenciaOn;
	}

	/**
	 * @param secuenciaOn
	 *            the secuenciaOn to set
	 */
	public void setSecuenciaOn(String secuenciaOn) {
		this.secuenciaOn = secuenciaOn;
	}

}
