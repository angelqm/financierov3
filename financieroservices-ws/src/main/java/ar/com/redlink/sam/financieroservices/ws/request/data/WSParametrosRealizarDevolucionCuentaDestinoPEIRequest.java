package ar.com.redlink.sam.financieroservices.ws.request.data;

/**
 * 
 * @author EXT-IBANEZMA
 *
 */
public class WSParametrosRealizarDevolucionCuentaDestinoPEIRequest {

	private String CBU;

	private String aliasCBU;

	/**
	 * @return the cBU
	 */
	public String getCBU() {
		return CBU;
	}

	/**
	 * @param cBU
	 *            the cBU to set
	 */
	public void setCBU(String cBU) {
		CBU = cBU;
	}

	/**
	 * @return the aliasCBU
	 */
	public String getAliasCBU() {
		return aliasCBU;
	}

	/**
	 * @param aliasCBU
	 *            the aliasCBU to set
	 */
	public void setAliasCBU(String aliasCBU) {
		this.aliasCBU = aliasCBU;
	}

}
