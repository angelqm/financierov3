package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosRealizarTransfPEIPagarRequest;

/**
 * Request para la realizacion de una transferencia PEI.
 * 
 * @author he_e90104
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSRealizarTransferenciaPEIPagarRequest extends WSAbstractRequest {

	@XmlElement(name = "realizarTransferenciaPEIPagarData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosRealizarTransfPEIPagarRequest realizarTransferenciaPEIPagarRequest;

	/**
	 * @return the realizarTransferenciaPEIPagarRequest
	 */
	public WSParametrosRealizarTransfPEIPagarRequest getRealizarTransferenciaPEIPagarRequest() {
		return realizarTransferenciaPEIPagarRequest;
	}

	/**
	 * @param realizarTransferenciaPEIPagarRequest the realizarTransferenciaPEIPagarRequest to set
	 */
	public void setRealizarTransferenciaPEIPagarRequest(
			WSParametrosRealizarTransfPEIPagarRequest realizarTransferenciaPEIPagarRequest) {
		this.realizarTransferenciaPEIPagarRequest = realizarTransferenciaPEIPagarRequest;
	}

	
}
