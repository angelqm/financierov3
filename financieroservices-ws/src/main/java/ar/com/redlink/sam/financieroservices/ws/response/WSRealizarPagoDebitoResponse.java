package ar.com.redlink.sam.financieroservices.ws.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * Respuesta del metodo de realizacion de operacion de extraccion para debito.
 * 
 * @author Diego Lucchelli
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSRealizarPagoDebitoResponse extends WSRealizarDebitoResponse {


}
