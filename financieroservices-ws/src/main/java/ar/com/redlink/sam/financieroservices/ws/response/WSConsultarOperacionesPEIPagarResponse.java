package ar.com.redlink.sam.financieroservices.ws.response;

import ar.com.redlink.sam.financieroservices.ws.response.base.WSAbstractResponse;

/**
 * 
 * @author EXT-IBANEZMA
 *
 */
public class WSConsultarOperacionesPEIPagarResponse extends WSAbstractResponse {	

	private WSConsultarOperacionesResultadoPEIPagarResponse resultado;

	/**
	 * @return the resultado
	 */
	public WSConsultarOperacionesResultadoPEIPagarResponse getResultado() {
		return resultado;
	}

	/**
	 * @param resultado the resultado to set
	 */
	public void setResultado(WSConsultarOperacionesResultadoPEIPagarResponse resultado) {
		this.resultado = resultado;
	}	


	
	
}
