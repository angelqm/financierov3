package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;


import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;

/**
 * Request para la prueba de estado del servicio.
 * 
 * @author he_e90104
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSTestServiceRequest extends WSAbstractRequest {
	
}
