package ar.com.redlink.sam.financieroservices.ws.request.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.data.WSEntidad;
import ar.com.redlink.sam.financieroservices.ws.data.WSTrack;

/**
 * Wrapper para la parametria de consultar operaciones PEI desde PAGAR
 * 
 * @author AQM
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSParametrosConsultarOperacionesPEIPagarRequest {
			
	@XmlElement(name = "entidad")
	private WSEntidad entidad;
	
	private String estado;

	private String fechadesde;

	private String fechahasta;
	
	private String idOperacion;
	
	private String idOperacionOrigen;	

	private String idReferenciaOperacion; 
	
	private String idReferenciaTrx;
	
	private String numeroReferenciaBancaria;
	
	private String canal;
	
	private String secuenciaOn;
	
	private WSTrack tracks;

	/**
	 * @return the entidad
	 */
	public WSEntidad getEntidad() {
		return entidad;
	}

	/**
	 * @param entidad the entidad to set
	 */
	public void setEntidad(WSEntidad entidad) {
		this.entidad = entidad;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the fechadesde
	 */
	public String getFechadesde() {
		return fechadesde;
	}

	/**
	 * @param fechadesde the fechadesde to set
	 */
	public void setFechadesde(String fechadesde) {
		this.fechadesde = fechadesde;
	}

	/**
	 * @return the fechahasta
	 */
	public String getFechahasta() {
		return fechahasta;
	}

	/**
	 * @param fechahasta the fechahasta to set
	 */
	public void setFechahasta(String fechahasta) {
		this.fechahasta = fechahasta;
	}

	/**
	 * @return the idOperacion
	 */
	public String getIdOperacion() {
		return idOperacion;
	}

	/**
	 * @param idOperacion the idOperacion to set
	 */
	public void setIdOperacion(String idOperacion) {
		this.idOperacion = idOperacion;
	}

	/**
	 * @return the idOperacionOrigen
	 */
	public String getIdOperacionOrigen() {
		return idOperacionOrigen;
	}

	/**
	 * @param idOperacionOrigen the idOperacionOrigen to set
	 */
	public void setIdOperacionOrigen(String idOperacionOrigen) {
		this.idOperacionOrigen = idOperacionOrigen;
	}

	/**
	 * @return the idReferenciaOperacion
	 */
	public String getIdReferenciaOperacion() {
		return idReferenciaOperacion;
	}

	/**
	 * @param idReferenciaOperacion the idReferenciaOperacion to set
	 */
	public void setIdReferenciaOperacion(String idReferenciaOperacion) {
		this.idReferenciaOperacion = idReferenciaOperacion;
	}

	/**
	 * @return the idReferenciaTrx
	 */
	public String getIdReferenciaTrx() {
		return idReferenciaTrx;
	}

	/**
	 * @param idReferenciaTrx the idReferenciaTrx to set
	 */
	public void setIdReferenciaTrx(String idReferenciaTrx) {
		this.idReferenciaTrx = idReferenciaTrx;
	}

	/**
	 * @return the numeroReferenciaBancaria
	 */
	public String getNumeroReferenciaBancaria() {
		return numeroReferenciaBancaria;
	}

	/**
	 * @param numeroReferenciaBancaria the numeroReferenciaBancaria to set
	 */
	public void setNumeroReferenciaBancaria(String numeroReferenciaBancaria) {
		this.numeroReferenciaBancaria = numeroReferenciaBancaria;
	}

	/**
	 * @return the canal
	 */
	public String getCanal() {
		return canal;
	}

	/**
	 * @param canal the canal to set
	 */
	public void setCanal(String canal) {
		this.canal = canal;
	}

	/**
	 * @return the secuenciaOn
	 */
	public String getSecuenciaOn() {
		return secuenciaOn;
	}

	/**
	 * @param secuenciaOn the secuenciaOn to set
	 */
	public void setSecuenciaOn(String secuenciaOn) {
		this.secuenciaOn = secuenciaOn;
	}

	/**
	 * @return the tracks
	 */
	public WSTrack getTracks() {
		return tracks;
	}

	/**
	 * @param tracks the tracks to set
	 */
	public void setTracks(WSTrack tracks) {
		this.tracks = tracks;
	}
		
}
