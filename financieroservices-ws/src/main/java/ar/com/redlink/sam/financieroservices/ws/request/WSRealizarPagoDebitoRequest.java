package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosRealizarPagoDebitoRequest;

/**
 * Wrapper para un request de extraccion para debito.
 * 
 * @author Diego Lucchelli
 * 
 */
public class WSRealizarPagoDebitoRequest extends WSAbstractRequest {

	@XmlElement(name = "realizarPagoDebitoData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosRealizarPagoDebitoRequest realizarPagoDebitoRequest;

	/**
	 * @return the realizarDebitoRequest
	 */
	public WSParametrosRealizarPagoDebitoRequest getRealizarPagoDebitoRequest() {
		return realizarPagoDebitoRequest;
	}

	/**
	 * @param realizarDebitoRequest
	 *            the realizarDebitoRequest to set
	 */
	public void setRealizarPagoDebitoRequest(
			WSParametrosRealizarPagoDebitoRequest realizarPagoDebitoRequest) {
		this.realizarPagoDebitoRequest = realizarPagoDebitoRequest;
	}

}
