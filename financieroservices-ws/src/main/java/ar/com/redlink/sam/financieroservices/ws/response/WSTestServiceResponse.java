/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.ws.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import ar.com.redlink.sam.financieroservices.ws.response.base.WSAbstractResponse;

/**
 * Wrapper para los parametros de retorno del request del metodo de prueba 
 * key.
 * 
 * @author he_e90104
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSTestServiceResponse extends WSAbstractResponse {

	}
