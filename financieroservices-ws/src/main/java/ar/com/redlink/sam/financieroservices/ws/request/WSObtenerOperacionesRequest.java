/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosObtenerOperacionesRequest;

/**
 * Wrapper para los parametros del request de obtencion de ultimas operaciones.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSObtenerOperacionesRequest extends WSAbstractRequest {

	@XmlElement(name = "obtenerOperacionesData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosObtenerOperacionesRequest obtenerOperacionesRequest;

	/**
	 * @return the obtenerOperacionesRequest
	 */
	public WSParametrosObtenerOperacionesRequest getObtenerOperacionesRequest() {
		return obtenerOperacionesRequest;
	}

	/**
	 * @param obtenerOperacionesRequest
	 *            the obtenerOperacionesRequest to set
	 */
	public void setObtenerOperacionesRequest(
			WSParametrosObtenerOperacionesRequest obtenerOperacionesRequest) {
		this.obtenerOperacionesRequest = obtenerOperacionesRequest;
	}

}
