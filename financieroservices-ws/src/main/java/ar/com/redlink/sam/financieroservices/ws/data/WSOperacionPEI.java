package ar.com.redlink.sam.financieroservices.ws.data;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Representa una operacion
 * 
 * @author EXT-IBANEZMA
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSOperacionPEI {

	@XmlElement(nillable = false, required = true)
	private String pan;

	@XmlElement(nillable = false, required = true)
	private String idreferenciaoperacioncomercio;

	@XmlElement(nillable = false, required = true)
	private BigDecimal importe;

	@XmlElement(nillable = false, required = true)
	private BigDecimal saldo;

	@XmlElement(nillable = false, required = false)
	private String motivorechazo;

	@XmlElement(nillable = false, required = true)
	private String idoperacion;

	@XmlElement(nillable = false, required = true)
	private String tipooperacion;

	@XmlElement(nillable = false, required = true)
	private String estado;

	@XmlElement(nillable = false, required = true)
	private String fecha;
	
	@XmlElement(nillable = false, required = true)
	private String concepto;
	
	@XmlElement(nillable = false, required = true)
	private String numeroReferenciaBancaria;
	

	/**
	 * @return the pan
	 */
	public String getPan() {
		return pan;
	}

	/**
	 * @param pan
	 *            the pan to set
	 */
	public void setPan(String pan) {
		this.pan = pan;
	}

	/**
	 * @return the idreferenciaoperacioncomercio
	 */
	public String getIdreferenciaoperacioncomercio() {
		return idreferenciaoperacioncomercio;
	}

	/**
	 * @param idreferenciaoperacioncomercio
	 *            the idreferenciaoperacioncomercio to set
	 */
	public void setIdreferenciaoperacioncomercio(String idreferenciaoperacioncomercio) {
		this.idreferenciaoperacioncomercio = idreferenciaoperacioncomercio;
	}

	/**
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}

	/**
	 * @param importe
	 *            the importe to set
	 */
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	/**
	 * @return the saldo
	 */
	public BigDecimal getSaldo() {
		return saldo;
	}

	/**
	 * @param saldo
	 *            the saldo to set
	 */
	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	/**
	 * @return the motivorechazo
	 */
	public String getMotivorechazo() {
		return motivorechazo;
	}

	/**
	 * @param motivorechazo
	 *            the motivorechazo to set
	 */
	public void setMotivorechazo(String motivorechazo) {
		this.motivorechazo = motivorechazo;
	}

	/**
	 * @return the idoperacion
	 */
	public String getIdoperacion() {
		return idoperacion;
	}

	/**
	 * @param idoperacion
	 *            the idoperacion to set
	 */
	public void setIdoperacion(String idoperacion) {
		this.idoperacion = idoperacion;
	}

	/**
	 * @return the tipooperacion
	 */
	public String getTipooperacion() {
		return tipooperacion;
	}

	/**
	 * @param tipooperacion
	 *            the tipooperacion to set
	 */
	public void setTipooperacion(String tipooperacion) {
		this.tipooperacion = tipooperacion;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado
	 *            the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha
	 *            the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}


	/**
	 * @return concepto
	 */
	public String getConcepto() {
		return concepto;
	}
	
	
	/**
	 *  @param concepto
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getNumeroReferenciaBancaria() {
		return numeroReferenciaBancaria;
	}

	public void setNumeroReferenciaBancaria(String numeroReferenciaBancaria) {
		this.numeroReferenciaBancaria = numeroReferenciaBancaria;
	}
	
	
}
