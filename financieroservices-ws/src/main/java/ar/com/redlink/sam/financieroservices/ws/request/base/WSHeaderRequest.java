/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.ws.request.base;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class WSHeaderRequest {

	@XmlElement(nillable = false, required = true)
	protected String idRequerimiento;

	@XmlElement(nillable = false, required = true)
	protected String ipCliente;

	@XmlElement(nillable = false, required = true)
	protected String timeStamp;

	@XmlElement(nillable = false, required = true)
	protected String entidad;

	@XmlElement(nillable = false, required = true)
	protected String sucursal;

	@XmlElement(nillable = false, required = true)
	protected String terminal;

	@XmlElement(nillable = false, required = true)
	private String identTerminal;

	@XmlElement(nillable = false, required = true)
	private String identDispositivo;

	/**
	 * 
	 */
	public WSHeaderRequest() {
		super();
	}

	/**
	 * @return the idRequerimiento
	 */
	public String getIdRequerimiento() {
		return idRequerimiento;
	}

	/**
	 * @param idRequerimiento
	 *            the idRequerimiento to set
	 */
	public void setIdRequerimiento(String idRequerimiento) {
		this.idRequerimiento = idRequerimiento;
	}

	/**
	 * @return the ipCliente
	 */
	public String getIpCliente() {
		return ipCliente;
	}

	/**
	 * @param ipCliente
	 *            the ipCliente to set
	 */
	public void setIpCliente(String ipCliente) {
		this.ipCliente = ipCliente;
	}

	/**
	 * @return the timeStamp
	 */
	public String getTimeStamp() {
		return timeStamp;
	}

	/**
	 * @param timeStamp
	 *            the timeStamp to set
	 */
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	/**
	 * @return the entidad
	 */
	public String getEntidad() {
		return entidad;
	}

	/**
	 * @param entidad
	 *            the entidad to set
	 */
	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}

	/**
	 * @return the canal
	 */
	public String getSucursal() {
		return sucursal;
	}

	/**
	 * @param canal
	 *            the canal to set
	 */
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	/**
	 * @return the terminal
	 */
	public String getTerminal() {
		return terminal;
	}

	/**
	 * @param terminal
	 *            the terminal to set
	 */
	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	/**
	 * @return the identTerminal
	 */
	public String getIdentTerminal() {
		return identTerminal;
	}

	/**
	 * @param identTerminal
	 *            the identTerminal to set
	 */
	public void setIdentTerminal(String identTerminal) {
		this.identTerminal = identTerminal;
	}

	/**
	 * @return the identDispositivo
	 */
	public String getIdentDispositivo() {
		return identDispositivo;
	}

	/**
	 * @param identDispositivo
	 *            the identDispositivo to set
	 */
	public void setIdentDispositivo(String identDispositivo) {
		this.identDispositivo = identDispositivo;
	}
}
