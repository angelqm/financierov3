/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  30/03/2015 - 00:18:47
 */

package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosRealizarExtraccionCuotasRequest;

/**
 * Wrapper para el request de extraccion en cuotas.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSRealizarExtraccionCuotasRequest extends WSAbstractRequest {

	@XmlElement(name = "realizarExtraccionCuotasData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosRealizarExtraccionCuotasRequest realizarExtraccionCuotasRequest;

	/**
	 * @return the realizarExtraccionCuotasRequest
	 */
	public WSParametrosRealizarExtraccionCuotasRequest getRealizarExtraccionCuotasRequest() {
		return realizarExtraccionCuotasRequest;
	}

	/**
	 * @param realizarExtraccionCuotasRequest
	 *            the realizarExtraccionCuotasRequest to set
	 */
	public void setRealizarExtraccionCuotasRequest(
			WSParametrosRealizarExtraccionCuotasRequest realizarExtraccionCuotasRequest) {
		this.realizarExtraccionCuotasRequest = realizarExtraccionCuotasRequest;
	}

}
