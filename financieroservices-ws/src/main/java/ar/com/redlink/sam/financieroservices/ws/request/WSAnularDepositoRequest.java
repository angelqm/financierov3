/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  11/03/2015 - 22:04:19
 */

package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosAnularDepositoRequest;

/**
 * Wrapper para el request de anulacion de deposito.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSAnularDepositoRequest extends WSAbstractRequest {

	@XmlElement(name = "anularDepositoData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosAnularDepositoRequest anularDepositoRequest;

	/**
	 * @return the anularDepositoRequest
	 */
	public WSParametrosAnularDepositoRequest getAnularDepositoRequest() {
		return anularDepositoRequest;
	}

	/**
	 * @param anularDepositoRequest
	 *            the anularDepositoRequest to set
	 */
	public void setAnularDepositoRequest(
			WSParametrosAnularDepositoRequest anularDepositoRequest) {
		this.anularDepositoRequest = anularDepositoRequest;
	}

}
