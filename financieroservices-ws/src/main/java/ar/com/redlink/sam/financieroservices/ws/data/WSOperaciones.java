/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  25/01/2015 - 14:44:50
 */

package ar.com.redlink.sam.financieroservices.ws.data;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Contenedor de lista de operaciones.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSOperaciones {

	@XmlElement(name = "operacion")
	private List<WSOperacion> operaciones;

	/**
	 * @return the operaciones
	 */
	public List<WSOperacion> getOperaciones() {
		return operaciones;
	}

	/**
	 * @param operaciones
	 *            the operaciones to set
	 */
	public void setOperaciones(List<WSOperacion> operaciones) {
		this.operaciones = operaciones;
	}
}
