/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  30/03/2015 - 00:30:53
 */

package ar.com.redlink.sam.financieroservices.ws.response;

import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.response.base.WSAbstractResponse;

/**
 * Response para la solicitud de anulacion de extraccion en cuotas.
 * 
 * @author aguerrea
 * 
 */
public class WSAnularExtraccionCuotasResponse extends WSAbstractResponse {

	@XmlElement(nillable = false, required = true)
	private String fechaOn;

	@XmlElement(nillable = false, required = true)
	private String secuenciaOn;

	@XmlElement(nillable = false, required = true)
	private String ticket;

	/**
	 * @return the secuenciaOn
	 */
	public String getSecuenciaOn() {
		return secuenciaOn;
	}

	/**
	 * @param secuenciaOn
	 *            the secuenciaOn to set
	 */
	public void setSecuenciaOn(String secuenciaOn) {
		this.secuenciaOn = secuenciaOn;
	}

	/**
	 * @return the ticket
	 */
	public String getTicket() {
		return ticket;
	}

	/**
	 * @param ticket
	 *            the ticket to set
	 */
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	/**
	 * @return the fechaOn
	 */
	public String getFechaOn() {
		return fechaOn;
	}

	/**
	 * @param fechaOn
	 *            the fechaOn to set
	 */
	public void setFechaOn(String fechaOn) {
		this.fechaOn = fechaOn;
	}
}
