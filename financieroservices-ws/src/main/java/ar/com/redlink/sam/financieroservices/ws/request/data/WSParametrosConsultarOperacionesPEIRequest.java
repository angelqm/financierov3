package ar.com.redlink.sam.financieroservices.ws.request.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * Wrapper para la parametria de consultar operaciones PEI.
 * 
 * @author EXT-IBANEZMA
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSParametrosConsultarOperacionesPEIRequest {

	private String secuenciaOn;

	private String track1;

	private String track2;

	private String track3;

	private String idoperacion;

	private String fechadesde;

	private String fechahasta;

	private String estado;
	
	private String idRequerimientoOrigen;
	
	private String idreferenciaoperacioncomercio;
	
	private String numeroReferenciaBancaria;

	/**
	 * @return the secuenciaOn
	 */
	public String getSecuenciaOn() {
		return secuenciaOn;
	}

	/**
	 * @param secuenciaOn
	 *            the secuenciaOn to set
	 */
	public void setSecuenciaOn(String secuenciaOn) {
		this.secuenciaOn = secuenciaOn;
	}

	/**
	 * @return the track1
	 */
	public String getTrack1() {
		return track1;
	}

	/**
	 * @param track1
	 *            the track1 to set
	 */
	public void setTrack1(String track1) {
		this.track1 = track1;
	}

	/**
	 * @return the track2
	 */
	public String getTrack2() {
		return track2;
	}

	/**
	 * @param track2
	 *            the track2 to set
	 */
	public void setTrack2(String track2) {
		this.track2 = track2;
	}

	/**
	 * @return the track3
	 */
	public String getTrack3() {
		return track3;
	}

	/**
	 * @param track3
	 *            the track3 to set
	 */
	public void setTrack3(String track3) {
		this.track3 = track3;
	}

	/**
	 * @return the idoperacion
	 */
	public String getIdoperacion() {
		return idoperacion;
	}

	/**
	 * @param idoperacion
	 *            the idoperacion to set
	 */
	public void setIdoperacion(String idoperacion) {
		this.idoperacion = idoperacion;
	}

	/**
	 * @return the fechadesde
	 */
	public String getFechadesde() {
		return fechadesde;
	}

	/**
	 * @param fechadesde
	 *            the fechadesde to set
	 */
	public void setFechadesde(String fechadesde) {
		this.fechadesde = fechadesde;
	}

	/**
	 * @return the fechahasta
	 */
	public String getFechahasta() {
		return fechahasta;
	}

	/**
	 * @param fechahasta
	 *            the fechahasta to set
	 */
	public void setFechahasta(String fechahasta) {
		this.fechahasta = fechahasta;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado
	 *            the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * 
	 * @return
	 */
	
	public String getIdRequerimientoOrigen() {
		return idRequerimientoOrigen;
	}
	
	/**
	 * 
	 * @param idRequerimientoOrigen
	 */
	public void setIdRequerimientoOrigen(String idRequerimientoOrigen) {
		this.idRequerimientoOrigen = idRequerimientoOrigen;
	}

	public String getIdreferenciaoperacioncomercio() {
		return idreferenciaoperacioncomercio;
	}

	public void setIdreferenciaoperacioncomercio(String idreferenciaoperacioncomercio) {
		this.idreferenciaoperacioncomercio = idreferenciaoperacioncomercio;
	}

	public String getNumeroReferenciaBancaria() {
		return numeroReferenciaBancaria;
	}

	public void setNumeroReferenciaBancaria(String numeroReferenciaBancaria) {
		this.numeroReferenciaBancaria = numeroReferenciaBancaria;
	}
	
}
