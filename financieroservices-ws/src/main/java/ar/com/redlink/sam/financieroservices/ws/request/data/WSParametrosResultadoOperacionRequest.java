/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  26/01/2015 - 22:07:21
 */

package ar.com.redlink.sam.financieroservices.ws.request.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import ar.com.redlink.sam.financieroservices.ws.request.WSObtenerResultOperacionRequest;

/**
 * Parametria de {@link WSObtenerResultOperacionRequest}.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSParametrosResultadoOperacionRequest {

	private String idRequerimientoOrig;

	/**
	 * @return the idRequerimientoOrig
	 */
	public String getIdRequerimientoOrig() {
		return idRequerimientoOrig;
	}

	/**
	 * @param idRequerimientoOrig
	 *            the idRequerimientoOrig to set
	 */
	public void setIdRequerimientoOrig(String idRequerimientoOrig) {
		this.idRequerimientoOrig = idRequerimientoOrig;
	}
}
