package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosConsultarOperacionesPEIRequest;

/**
 * 
 * @author EXT-IBANEZMA
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSConsultarOperacionesPEIRequest extends WSAbstractRequest {

	@XmlElement(name = "consultarOperacionesPEIData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosConsultarOperacionesPEIRequest consultarOperacionesPEIRequest;

	/**
	 * @return the consultarOperacionesPEIRequest
	 */
	public WSParametrosConsultarOperacionesPEIRequest getConsultarOperacionesPEIRequest() {
		return consultarOperacionesPEIRequest;
	}

	/**
	 * @param consultarOperacionesPEIRequest
	 *            the consultarOperacionesPEIRequest to set
	 */
	public void setConsultarOperacionesPEIRequest(
			WSParametrosConsultarOperacionesPEIRequest consultarOperacionesPEIRequest) {
		this.consultarOperacionesPEIRequest = consultarOperacionesPEIRequest;
	}

}
