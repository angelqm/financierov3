/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  25/01/2015 - 14:38:40
 */

package ar.com.redlink.sam.financieroservices.ws.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Representacion de un limite asociado a una operacion.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSLimite {

	@XmlElement(nillable = false, required = true)
	private String nombreLimite;

	@XmlElement(nillable = false, required = true)
	private String descripcionLimite;

	@XmlElement(nillable = false, required = true)
	private String valorLimite;

	@XmlElement(nillable = false, required = true)
	private int precisionValorLimite;

	/**
	 * @return the nombreLimite
	 */
	public String getNombreLimite() {
		return nombreLimite;
	}

	/**
	 * @param nombreLimite
	 *            the nombreLimite to set
	 */
	public void setNombreLimite(String nombreLimite) {
		this.nombreLimite = nombreLimite;
	}

	/**
	 * @return the descripcionLimite
	 */
	public String getDescripcionLimite() {
		return descripcionLimite;
	}

	/**
	 * @param descripcionLimite
	 *            the descripcionLimite to set
	 */
	public void setDescripcionLimite(String descripcionLimite) {
		this.descripcionLimite = descripcionLimite;
	}

	/**
	 * @return the valorLimite
	 */
	public String getValorLimite() {
		return valorLimite;
	}

	/**
	 * @param valorLimite
	 *            the valorLimite to set
	 */
	public void setValorLimite(String valorLimite) {
		this.valorLimite = valorLimite;
	}

	/**
	 * @return the precisionValorLimite
	 */
	public int getPrecisionValorLimite() {
		return precisionValorLimite;
	}

	/**
	 * @param precisionValorLimite
	 *            the precisionValorLimite to set
	 */
	public void setPrecisionValorLimite(int precisionValorLimite) {
		this.precisionValorLimite = precisionValorLimite;
	}
}
