/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  30/03/2015 - 00:27:03
 */

package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosAnularExtraccionCuotasRequest;

/**
 * Request para anular una extraccion en cuotas.
 * 
 * @author aguerrea
 * 
 */
public class WSAnularExtraccionCuotasRequest extends WSAbstractRequest {
	@XmlElement(name = "anularExtraccionCuotasData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosAnularExtraccionCuotasRequest anularExtraccionCuotasRequest;

	/**
	 * @return the anularExtraccionCuotasRequest
	 */
	public WSParametrosAnularExtraccionCuotasRequest getAnularExtraccionCuotasRequest() {
		return anularExtraccionCuotasRequest;
	}

	/**
	 * @param anularExtraccionCuotasRequest
	 *            the anularExtraccionCuotasRequest to set
	 */
	public void setAnularExtraccionCuotasRequest(
			WSParametrosAnularExtraccionCuotasRequest anularExtraccionCuotasRequest) {
		this.anularExtraccionCuotasRequest = anularExtraccionCuotasRequest;
	}

}
