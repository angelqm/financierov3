/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosRealizarExtraccionRequest;

/**
 * Wrapper para los parametros del request de realizacion de extraccion.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSRealizarExtraccionRequest extends WSAbstractRequest {

	@XmlElement(name = "realizarExtraccionData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosRealizarExtraccionRequest realizarExtraccionRequest;

	/**
	 * @return the realizarExtraccionRequest
	 */
	public WSParametrosRealizarExtraccionRequest getRealizarExtraccionRequest() {
		return realizarExtraccionRequest;
	}

	/**
	 * @param realizarExtraccionRequest
	 *            the realizarExtraccionRequest to set
	 */
	public void setRealizarExtraccionRequest(
			WSParametrosRealizarExtraccionRequest realizarExtraccionRequest) {
		this.realizarExtraccionRequest = realizarExtraccionRequest;
	}

}
