/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  11/03/2015 - 21:39:32
 */

package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosRealizarDepositoRequest;

/**
 * Wrapper para el request de la operacion de deposito.
 * 
 * @author aguerrea
 * 
 */
public class WSRealizarDepositoRequest extends WSAbstractRequest {

	@XmlElement(name = "realizarDepositoData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosRealizarDepositoRequest realizarDepositoRequest;

	/**
	 * @return the realizarDepositoRequest
	 */
	public WSParametrosRealizarDepositoRequest getRealizarDepositoRequest() {
		return realizarDepositoRequest;
	}

	/**
	 * @param realizarDepositoRequest
	 *            the realizarDepositoRequest to set
	 */
	public void setRealizarDepositoRequest(
			WSParametrosRealizarDepositoRequest realizarDepositoRequest) {
		this.realizarDepositoRequest = realizarDepositoRequest;
	}
}
