package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosRealizarDevolucionPEIPagarRequest;

/**
 * Request para la realizacion de una devolucion PEI desde PAGAR.
 * 
 * @author AQM
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSRealizarDevolucionPagoPEIPagarRequest extends WSAbstractRequest {	

	@XmlElement(name = "realizarDevolucionPagoPEIPagarData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosRealizarDevolucionPEIPagarRequest realizarDevolucionferenciaPEIPagarRequest;

	/**
	 * @return the realizarDevolucionferenciaPEIPagarRequest
	 */
	public WSParametrosRealizarDevolucionPEIPagarRequest getRealizarDevolucionferenciaPEIPagarRequest() {
		return realizarDevolucionferenciaPEIPagarRequest;
	}

	/**
	 * @param realizarDevolucionferenciaPEIPagarRequest the realizarDevolucionferenciaPEIPagarRequest to set
	 */
	public void setRealizarDevolucionferenciaPEIRequest(
			WSParametrosRealizarDevolucionPEIPagarRequest realizarDevolucionferenciaPEIPagarRequest) {
		this.realizarDevolucionferenciaPEIPagarRequest = realizarDevolucionferenciaPEIPagarRequest;
	}	
	
}
