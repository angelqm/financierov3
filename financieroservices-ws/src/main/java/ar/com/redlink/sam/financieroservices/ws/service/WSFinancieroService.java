/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.ws.service;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.ParameterStyle;

import ar.com.redlink.framework.services.RedLinkIntegrationService;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.ws.request.WSAnularDebitoRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSAnularDepositoRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSAnularExtraccionCuotasRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSAnularExtraccionPrestamoRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSAnularExtraccionRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSConsultarOperacionesPEIRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSAnularPagoDebitoRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSConsultarOperacionesPEIPagarRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSEnrolamientoRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSObtenerOperacionesRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSObtenerResultOperacionRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSObtenerSaldoCuentaRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSObtenerWorkingKeyRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarDebitoRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarDepositoRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarDevolucionPagoPEIRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarDevolucionPagoPEIPagarRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarDevolucionPagoParcialPEIRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarExtraccionCuotasRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarExtraccionPrestamoRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarExtraccionRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarPagoDebitoRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarTransferenciaPEIPagarRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarTransferenciaPEIRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarTransferenciaRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSTestServiceRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSTipoDocumentoRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSValidarTarjetaRequest;
import ar.com.redlink.sam.financieroservices.ws.response.WSAnularDebitoResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSAnularDepositoResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSAnularExtraccionCuotasResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSAnularExtraccionPrestamoResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSAnularExtraccionResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSAnularPagoDebitoResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSConsultarOperacionesPEIPagarResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSConsultarOperacionesPEIResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSEnrolamientoResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSObtenerOperacionesResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSObtenerResultOperacionResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSObtenerSaldoCuentaResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSObtenerWorkingKeyResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarDebitoResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarDepositoResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarDevolucionPEIResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarDevolucionPEIPagarResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarExtraccionCuotasResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarExtraccionPrestamoResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarExtraccionResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarPagoDebitoResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarTransferenciaPEIPagarResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarTransferenciaPEIResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarTransferenciaResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSTestServiceResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSTiposDocumentoResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSValidarTarjetaResponse;

@WebService
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = ParameterStyle.WRAPPED)
public interface WSFinancieroService extends RedLinkIntegrationService {

	/**
	 * Permite relacionar el equipamiento del cliente con la terminal logica
	 * para proveer una identificacion positiva del puesto.
	 * 
	 * @param request
	 * @return resultado del enrolamiento.
	 * @throws FinancieroValidationException
	 */
	// @Secured("ROLE_WS_FINANCIERO")
	public @WebResult(name = "enrolamientoRes")	WSEnrolamientoResponse enrolamiento(@WebParam(name = "enrolamientoReq") WSEnrolamientoRequest request)	throws FinancieroValidationException;

	/**
	 * Permite obtener una working key para la encripcion de los datos en el
	 * dispositivo.
	 * 
	 * @param request
	 * @return workingKey valida.
	 */
	public @WebResult(name = "obtenerWorkingKeyRes")
	WSObtenerWorkingKeyResponse obtenerWorkingKey(@WebParam(name = "obtenerWorkingKeyReq") WSObtenerWorkingKeyRequest request);

	/**
	 * Permite validar los tracks de tarjeta obteniendo sus cuentas asociadas y
	 * las operaciones habilitadas para una terminal.
	 * 
	 * @param request
	 * @return validacion de la tarjeta.
	 */
	public @WebResult(name = "validarTarjetaRes")
	WSValidarTarjetaResponse validarTarjeta(@WebParam(name = "validarTarjetaReq") WSValidarTarjetaRequest request);

	/**
	 * Permite realizar una operacion de extraccion.
	 * 
	 * @param request
	 * @return datos de la extraccion.
	 */
	public @WebResult(name = "realizarExtraccionRes")
	WSRealizarExtraccionResponse realizarExtraccion(@WebParam(name = "realizarExtraccionReq") WSRealizarExtraccionRequest request);

	/**
	 * Permite realizar la anulacion de una operacion de extraccion. Debe ser la
	 * ultima operacion de la terminal cursada con el servicio.
	 * 
	 * @param request
	 * @return confirmacion de la anulacion.
	 */
	public @WebResult(name = "anularExtraccionRes")
	WSAnularExtraccionResponse anularExtraccion(@WebParam(name = "anularExtraccionReq") WSAnularExtraccionRequest request);

	/**
	 * Permite obtener el saldo de una cuenta asociada a la tarjeta.
	 * 
	 * @param request
	 * @return saldo de la cuenta.
	 */
	public @WebResult(name = "obtenerSaldoRes")
	WSObtenerSaldoCuentaResponse obtenerSaldo(@WebParam(name = "obtenerSaldoReq") WSObtenerSaldoCuentaRequest request);

	/**
	 * Permite la realizacion de una extraccion de prestamo.
	 * 
	 * @param request
	 * @return resultado de la extraccion.
	 */
	public @WebResult(name = "realizarExtraccionPrestamoRes")
	WSRealizarExtraccionPrestamoResponse realizarExtraccionPrestamo(
			@WebParam(name = "realizarExtraccionPrestamoReq") WSRealizarExtraccionPrestamoRequest request);

	/**
	 * Permite realizar la anulacion de una operacion de extraccion de prestamo.
	 * 
	 * @param request
	 * @return confirmacion de la anulacion.
	 */
	public @WebResult(name = "anularExtraccionPrestamoRes")
	WSAnularExtraccionPrestamoResponse anularExtraccionPrestamo(
			@WebParam(name = "anularExtraccionPrestamoReq") WSAnularExtraccionPrestamoRequest request);

	/**
	 * Permite obtener una operacion a partir de un codigo generado de forma
	 * univoca por una aplicacion de punta de caja.
	 * 
	 * @param request
	 * @return operaciones.
	 */
	public @WebResult(name = "obtenerResultadoOpRes")
	WSObtenerResultOperacionResponse obtenerResultadoOp(@WebParam(name = "obtenerResultadoOpReq") WSObtenerResultOperacionRequest request);

	/**
	 * Permite obtener las ultimas operaciones realizadas en la terminal.
	 * 
	 * @param request
	 * @return ultimas operaciones.
	 */
	public @WebResult(name = "obtenerOperacionesRes")
	WSObtenerOperacionesResponse obtenerOperaciones(@WebParam(name = "obtenerOperacionesReq") WSObtenerOperacionesRequest request);
	
	/**
	 * Permite obtener las operaciones PEI.
	 * 
	 * @param request
	 * @return
	 */
	public @WebResult(name = "consultarOperacionesPEIRes")
	WSConsultarOperacionesPEIResponse consultarOperacionesPEI(	@WebParam(name = "consultarOperacionesPEIReq") WSConsultarOperacionesPEIRequest request);	
		
	/**
	 * Permite realizar devolución de pago.
	 * 
	 * @param request
	 * @return
	 */
	public @WebResult(name = "realizarDevolucionPagoPEIRes")
	WSRealizarDevolucionPEIResponse realizarDevolucionPagoPEI(
			@WebParam(name = "realizarDevolucionPagoPEIReq") WSRealizarDevolucionPagoPEIRequest request);	

	/*AQM*/
	/**
	 * Permite realizar devolución PEI desde PAGAR.
	 * 
	 * @param request
	 * @return
	 */
	public @WebResult(name = "realizarDevolucionPagoPEIPagarRes")
	WSRealizarDevolucionPEIPagarResponse realizarDevolucionPagoPEIPagar(
			@WebParam(name = "realizarDevolucionPagoPEIPagarReq") WSRealizarDevolucionPagoPEIPagarRequest request);	

	
	/**
	 * Permite realizar devolución de pago parcial.
	 * 
	 * @param request
	 * @return
	 */
	public @WebResult(name = "realizarDevolucionPagoParcialPEIRes")
	WSRealizarDevolucionPEIResponse realizarDevolucionPagoParcialPEI(
			@WebParam(name = "realizarDevolucionPagoParcialPEIReq") WSRealizarDevolucionPagoParcialPEIRequest request);	

	/**
	 * Permite realizar una extraccion para debito.
	 * 
	 * @param request
	 * @return resultado de la operacion.
	 */
	public @WebResult(name = "realizarDebitoRes")
	WSRealizarDebitoResponse realizarDebito(@WebParam(name = "realizarDebitoReq") WSRealizarDebitoRequest request);
	/**
	 * 
	 */
	public @WebResult(name = "realizarPagoDebitoRes")
	WSRealizarPagoDebitoResponse realizarPagoDebito(@WebParam(name = "realizarPagoDebitoReq") WSRealizarPagoDebitoRequest request);
	

	/**
	 * Permite la anulacion de una operacion de debito.
	 * 
	 * @param request
	 * @return resultado de la operacion.
	 */
	public @WebResult(name = "anularDebitoRes")
	WSAnularDebitoResponse anularDebito(@WebParam(name = "anularDebitoReq") WSAnularDebitoRequest request);

	/**
	 * Permite realizar un deposito.
	 * 
	 * @param request
	 * @return resultado de la operacion.
	 */
	public @WebResult(name = "realizarDepositoRes")
	WSRealizarDepositoResponse realizarDeposito(@WebParam(name = "realizarDepositoReq") WSRealizarDepositoRequest request);

	/**
	 * Permite anular un deposito.
	 * 
	 * @param request
	 * @return resultado de la operacion.
	 */
	public @WebResult(name = "anularDepositoRes")
	WSAnularDepositoResponse anularDeposito(@WebParam(name = "anularDepositoReq") WSAnularDepositoRequest request);

	/**
	 * Permite realizar una transferencia.
	 * 
	 * @param request
	 * @return resultado de la operacion.
	 */
	public @WebResult(name = "realizarTransferenciaRes")
	WSRealizarTransferenciaResponse realizarTransferencia(@WebParam(name = "realizarTransferenciaReq") WSRealizarTransferenciaRequest request);
	
	/**
	 * Permite realizar una transferencia PEI.
	 * 
	 * @param request
	 * @return resultado de la operacion.
	 */
	public @WebResult(name = "realizarPagoPEIRes")
	WSRealizarTransferenciaPEIResponse realizarPagoPEI(@WebParam(name = "realizarPagoPEIReq") WSRealizarTransferenciaPEIRequest request);
	
	/**
	 * Permite realizar una transferencia PEI PAGAR.
	 * 
	 * @param request
	 * @return resultado de la operacion.
	 */
	public @WebResult(name = "realizarPagoPEIPagarRes")
	WSRealizarTransferenciaPEIPagarResponse realizarPagoPEIPagar(@WebParam(name = "realizarPagoPEIPagarReq") WSRealizarTransferenciaPEIPagarRequest request);
	

	/**
	 * Permite realizar una operacion de extraccion en cuotas.
	 * 
	 * @param request
	 * @return resultado de la operacion.
	 */
	public @WebResult(name = "realizarExtraccionCuotasRes")
	WSRealizarExtraccionCuotasResponse realizarExtraccionCuotas(@WebParam(name = "realizarExtraccionCuotasReq") WSRealizarExtraccionCuotasRequest request);

	/**
	 * Permite anular una extraccion en cuotas.
	 * 
	 * @param request
	 * @return resultado de la operacion.
	 */
	public @WebResult(name = "anularExtraccionCuotasRes")
	WSAnularExtraccionCuotasResponse anularExtraccionCuotas(@WebParam(name = "anularExtraccionCuotasReq") WSAnularExtraccionCuotasRequest request);

	/**
	 * 
	 * @param request
	 * @return
	 */
	public @WebResult(name = "anularPagoDebitoRes")
	WSAnularPagoDebitoResponse anularPagoDebito(@WebParam(name = "anularPagoDebitoReq") WSAnularPagoDebitoRequest request);

	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public @WebResult(name = "pruebaServicioFinancieroRes")
	WSTestServiceResponse pruebaServicioFinanciero(@WebParam(name = "pruebaServicioFinancieroReq") WSTestServiceRequest request);
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public @WebResult(name = "tipoDocumento")
	WSTiposDocumentoResponse tipoDocumento(@WebParam(name = "tipoDocumentoReq") WSTipoDocumentoRequest request);

	/**
	 * Permite obtener las operaciones PEI desde PAGAR.
	 * AQM
	 * @param request
	 * @return
	 */
	public @WebResult(name = "consultarOperacionesPEIPagarRes")
	WSConsultarOperacionesPEIPagarResponse consultarOperacionesPEIPagar(	@WebParam(name = "consultarOperacionesPEIPagarReq") WSConsultarOperacionesPEIPagarRequest request);	
		
}
