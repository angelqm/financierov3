/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: he_e90104
 * Date:  13/05/2019
 */

package ar.com.redlink.sam.financieroservices.ws.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Representacion de una cuenta.
 * 
 * @author he_e90104
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSCuentaRecaudadora {

	@XmlElement(nillable = false, required = true)
	private String aliasCbu;

	@XmlElement(nillable = false, required = true)
	private String cbu;
	
	@XmlElement(nillable = false, required = true)
	private String fiid; 

	@XmlElement(nillable = false, required = true)
	private String numero;

	@XmlElement(nillable = false, required = true)
	private String tipo;

	/**
	 * @return the aliasCbu
	 */
	public String getAliasCbu() {
		return aliasCbu;
	}

	/**
	 * @param aliasCbu the aliasCbu to set
	 */
	public void setAliasCbu(String aliasCbu) {
		this.aliasCbu = aliasCbu;
	}

	/**
	 * @return the cbu
	 */
	public String getCbu() {
		return cbu;
	}

	/**
	 * @param cbu the cbu to set
	 */
	public void setCbu(String cbu) {
		this.cbu = cbu;
	}
	
	/**
	 * @return the fiid
	 */
	public String getFiid() {
		return fiid;
	}

	/**
	 * @param fiid the fiid to set
	 */
	public void setFiid(String fiid) {
		this.fiid = fiid;
	}

	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	
}
