package ar.com.redlink.sam.financieroservices.ws.request.data;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * Wrapper para la parametria de la operacion de transferencias PEI.
 * 
 * @author EXT-IBANEZMA
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSParametrosRealizarDevolucionPEIRequest {
	
	private String idRequerimientoOrig;
	
	private String idpago;

	private String track1;

	private String track2;

	private String track3;

	private String posentrymode;

	private String titulardocumento;

	private String codigoseguridad;

	private String numero;
	
	private BigDecimal importe;

	private String idreferenciaoperacioncomercio;

	private String secuenciaOn;
	
	private WSParametrosRealizarDevolucionCuentaDestinoPEIRequest cuentaDestino;

	/**
	 * @return the idpago
	 */
	public String getIdpago() {
		return idpago;
	}

	/**
	 * @param idpago
	 *            the idpago to set
	 */
	public void setIdpago(String idpago) {
		this.idpago = idpago;
	}

	/**
	 * @return the track1
	 */
	public String getTrack1() {
		return track1;
	}

	/**
	 * @param track1
	 *            the track1 to set
	 */
	public void setTrack1(String track1) {
		this.track1 = track1;
	}

	/**
	 * @return the track2
	 */
	public String getTrack2() {
		return track2;
	}

	/**
	 * @param track2
	 *            the track2 to set
	 */
	public void setTrack2(String track2) {
		this.track2 = track2;
	}

	/**
	 * @return the track3
	 */
	public String getTrack3() {
		return track3;
	}

	/**
	 * @param track3
	 *            the track3 to set
	 */
	public void setTrack3(String track3) {
		this.track3 = track3;
	}

	/**
	 * @return the posentrymode
	 */
	public String getPosentrymode() {
		return posentrymode;
	}

	/**
	 * @param posentrymode
	 *            the posentrymode to set
	 */
	public void setPosentrymode(String posentrymode) {
		this.posentrymode = posentrymode;
	}

	/**
	 * @return the titulardocumento
	 */
	public String getTitulardocumento() {
		return titulardocumento;
	}

	/**
	 * @param titulardocumento
	 *            the titulardocumento to set
	 */
	public void setTitulardocumento(String titulardocumento) {
		this.titulardocumento = titulardocumento;
	}

	/**
	 * @return the codigoseguridad
	 */
	public String getCodigoseguridad() {
		return codigoseguridad;
	}

	/**
	 * @param codigoseguridad
	 *            the codigoseguridad to set
	 */
	public void setCodigoseguridad(String codigoseguridad) {
		this.codigoseguridad = codigoseguridad;
	}

	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * @param numero
	 *            the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * @return the idreferenciaoperacioncomercio
	 */
	public String getIdreferenciaoperacioncomercio() {
		return idreferenciaoperacioncomercio;
	}

	/**
	 * @param idreferenciaoperacioncomercio
	 *            the idreferenciaoperacioncomercio to set
	 */
	public void setIdreferenciaoperacioncomercio(String idreferenciaoperacioncomercio) {
		this.idreferenciaoperacioncomercio = idreferenciaoperacioncomercio;
	}

	/**
	 * @return the secuenciaOn
	 */
	public String getSecuenciaOn() {
		return secuenciaOn;
	}

	/**
	 * @param secuenciaOn the secuenciaOn to set
	 */
	public void setSecuenciaOn(String secuenciaOn) {
		this.secuenciaOn = secuenciaOn;
	}

	/**
	 * @return the cuentaDestino
	 */
	public WSParametrosRealizarDevolucionCuentaDestinoPEIRequest getCuentaDestino() {
		return cuentaDestino;
	}

	/**
	 * @param cuentaDestino the cuentaDestino to set
	 */
	public void setCuentaDestino(WSParametrosRealizarDevolucionCuentaDestinoPEIRequest cuentaDestino) {
		this.cuentaDestino = cuentaDestino;
	}

	/**
	 * @return the idRequerimientoOrig
	 */
	public String getIdRequerimientoOrig() {
		return idRequerimientoOrig;
	}

	/**
	 * @param idRequerimientoOrig the idRequerimientoOrig to set
	 */
	public void setIdRequerimientoOrig(String idRequerimientoOrig) {
		this.idRequerimientoOrig = idRequerimientoOrig;
	}

	/**
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}

	/**
	 * @param importe the importe to set
	 */
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

}
