package ar.com.redlink.sam.financieroservices.ws.request.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * Wrapper para la parametria del metodo de extraccion para debito.
 * 
 * @author Diego Lucchelli
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSParametrosRealizarPagoDebitoRequest extends WSParametrosRealizarDebitoRequest {

	private String descripcionDebito;
	private String tipoTerminal;
	
	public String getDescripcionDebito() {
		return descripcionDebito;
	}
	public void setDescripcionDebito(String descripcionDebito) {
		this.descripcionDebito = descripcionDebito;
	}
	public String getTipoTerminal() {
		return tipoTerminal;
	}
	public void setTipoTerminal(String tipoTerminal) {
		this.tipoTerminal = tipoTerminal;
	}
}
