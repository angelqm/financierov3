/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosObtenerSaldoRequest;

/**
 * Wrapper para los parametros del request de obtencion de saldo de cuenta.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSObtenerSaldoCuentaRequest extends WSAbstractRequest {

	@XmlElement(name = "obtenerSaldoData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosObtenerSaldoRequest obtenerSaldoRequest;

	/**
	 * @return the obtenerSaldoRequest
	 */
	public WSParametrosObtenerSaldoRequest getObtenerSaldoRequest() {
		return obtenerSaldoRequest;
	}

	/**
	 * @param obtenerSaldoRequest
	 *            the obtenerSaldoRequest to set
	 */
	public void setObtenerSaldoRequest(
			WSParametrosObtenerSaldoRequest obtenerSaldoRequest) {
		this.obtenerSaldoRequest = obtenerSaldoRequest;
	}

}
