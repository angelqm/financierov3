/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: he_e90104
 * Date:  16/05/2019 - 14:42:22
 */

package ar.com.redlink.sam.financieroservices.ws.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Representacion de una cuenta.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSTarjeta {

	@XmlElement(nillable = false, required = true)
	private String codSeguridad;

	@XmlElement(nillable = false, required = true)
	private String numero;

	@XmlElement(name = "track")
	private WSTrack track;

	/**
	 * @return the codSeguridad
	 */
	public String getCodSeguridad() {
		return codSeguridad;
	}

	/**
	 * @param codSeguridad the codSeguridad to set
	 */
	public void setCodSeguridad(String codSeguridad) {
		this.codSeguridad = codSeguridad;
	}

	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * @return the track
	 */
	public WSTrack getTrack() {
		return track;
	}

	/**
	 * @param track the track to set
	 */
	public void setTrack(WSTrack track) {
		this.track = track;
	}
	
}
