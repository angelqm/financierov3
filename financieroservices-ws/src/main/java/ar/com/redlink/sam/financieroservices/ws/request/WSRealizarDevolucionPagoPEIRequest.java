package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosRealizarDevolucionPEIRequest;

/**
 * Request para la realizacion de una devolucion PEI.
 * 
 * @author EXT-IBANEZMA
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSRealizarDevolucionPagoPEIRequest extends WSAbstractRequest {	

	@XmlElement(name = "realizarDevolucionPagoPEIData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosRealizarDevolucionPEIRequest realizarDevolucionferenciaPEIRequest;

	/**
	 * @return the realizarDevolucionferenciaPEIRequest
	 */
	public WSParametrosRealizarDevolucionPEIRequest getRealizarDevolucionferenciaPEIRequest() {
		return realizarDevolucionferenciaPEIRequest;
	}

	/**
	 * @param realizarDevolucionferenciaPEIRequest the realizarDevolucionferenciaPEIRequest to set
	 */
	public void setRealizarDevolucionferenciaPEIRequest(
			WSParametrosRealizarDevolucionPEIRequest realizarDevolucionferenciaPEIRequest) {
		this.realizarDevolucionferenciaPEIRequest = realizarDevolucionferenciaPEIRequest;
	}	
	
}
