package ar.com.redlink.sam.financieroservices.ws.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.response.base.WSAbstractResponse;

/**
 * Respuesta del metodo de realizacion de devolucion PEI desde PAGAR.
 * 
 * @author AQM
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSRealizarDevolucionPEIPagarResponse extends WSAbstractResponse {

	@XmlElement(name = "resultado", nillable = false, required = false)
	private WSRealizarDevolucionResultadoPEIPagarResponse resultado;

	/**
	 * @return the resultado
	 */
	public WSRealizarDevolucionResultadoPEIPagarResponse getResultado() {
		return resultado;
	}

	/**
	 * @param resultado the resultado to set
	 */
	public void setResultado(WSRealizarDevolucionResultadoPEIPagarResponse resultado) {
		this.resultado = resultado;
	}

}
