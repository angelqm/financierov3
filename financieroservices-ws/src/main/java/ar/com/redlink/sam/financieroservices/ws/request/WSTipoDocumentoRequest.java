package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;


import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;

/**
 * Request para la consulta de tipo de documento.
 * 
 * @author he_e90104
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSTipoDocumentoRequest extends WSAbstractRequest {
	
}
