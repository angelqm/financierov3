package ar.com.redlink.sam.financieroservices.ws.request.data;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.data.WSDestino;
import ar.com.redlink.sam.financieroservices.ws.data.WSDocumento;
import ar.com.redlink.sam.financieroservices.ws.data.WSTarjeta;

/**
 * Wrapper para la parametria de la operacion de transferencias PEI.
 * 
 * @author he_e90104
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSParametrosRealizarTransfPEIPagarRequest {
	
	private String canal;
	
	private String concepto;
	
	@XmlElement(name = "destino")
	private WSDestino destino;
		
	@XmlElement(name = "documento")
	private WSDocumento documento;
	
	private String idReferenciaOperacion;
	
	private String idReferenciaTrx;
	
	private BigDecimal importe;
	
	private String moneda;
	
	private String posEntryMode;
		
	@XmlElement(name = "tarjeta")
	private WSTarjeta tarjeta;
		
	private String secuenciaOn;

	/**
	 * @return the canal
	 */
	public String getCanal() {
		return canal;
	}

	/**
	 * @param canal the canal to set
	 */
	public void setCanal(String canal) {
		this.canal = canal;
	}

	/**
	 * @return the concepto
	 */
	public String getConcepto() {
		return concepto;
	}

	/**
	 * @param concepto the concepto to set
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	/**
	 * @return the destino
	 */
	public WSDestino getDestino() {
		return destino;
	}

	/**
	 * @param destino the destino to set
	 */
	public void setDestino(WSDestino destino) {
		this.destino = destino;
	}

	/**
	 * @return the documento
	 */
	public WSDocumento getDocumento() {
		return documento;
	}

	/**
	 * @param documento the documento to set
	 */
	public void setDocumento(WSDocumento documento) {
		this.documento = documento;
	}

	/**
	 * @return the idReferenciaOperacion
	 */
	public String getIdReferenciaOperacion() {
		return idReferenciaOperacion;
	}

	/**
	 * @param idReferenciaOperacion the idReferenciaOperacion to set
	 */
	public void setIdReferenciaOperacion(String idReferenciaOperacion) {
		this.idReferenciaOperacion = idReferenciaOperacion;
	}

	/**
	 * @return the idReferenciaTrx
	 */
	public String getIdReferenciaTrx() {
		return idReferenciaTrx;
	}

	/**
	 * @param idReferenciaTrx the idReferenciaTrx to set
	 */
	public void setIdReferenciaTrx(String idReferenciaTrx) {
		this.idReferenciaTrx = idReferenciaTrx;
	}

	/**
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}

	/**
	 * @param importe the importe to set
	 */
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	/**
	 * @return the moneda
	 */
	public String getMoneda() {
		return moneda;
	}

	/**
	 * @param moneda the moneda to set
	 */
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	/**
	 * @return the posEntryMode
	 */
	public String getPosEntryMode() {
		return posEntryMode;
	}

	/**
	 * @param posEntryMode the posEntryMode to set
	 */
	public void setPosEntryMode(String posEntryMode) {
		this.posEntryMode = posEntryMode;
	}

	/**
	 * @return the tarjeta
	 */
	public WSTarjeta getTarjeta() {
		return tarjeta;
	}

	/**
	 * @param tarjeta the tarjeta to set
	 */
	public void setTarjeta(WSTarjeta tarjeta) {
		this.tarjeta = tarjeta;
	}

	/**
	 * @return the secuenciaOn
	 */
	public String getSecuenciaOn() {
		return secuenciaOn;
	}

	/**
	 * @param secuenciaOn the secuenciaOn to set
	 */
	public void setSecuenciaOn(String secuenciaOn) {
		this.secuenciaOn = secuenciaOn;
	}

	
}
