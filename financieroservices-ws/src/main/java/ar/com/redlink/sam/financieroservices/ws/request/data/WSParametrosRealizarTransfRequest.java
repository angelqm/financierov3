/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  11/03/2015 - 22:28:33
 */

package ar.com.redlink.sam.financieroservices.ws.request.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.data.WSCuenta;

/**
 * Wrapper para la parametria de la operacion de transferencias.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSParametrosRealizarTransfRequest {

	private String secuenciaOn;

	private String track1;

	private String track2;

	private String track3;

	private String debitCardSuffix;

	private String cardSecurityCode;

	private String pin;

	private String dni;

	private double importe;

	@XmlElement(name = "cuentaOrigen")
	private WSCuenta cuentaOrigen;

	@XmlElement(name = "cuentaDestino")
	private WSCuenta cuentaDestino;
	
	private String tokenizado;

	private byte autorizacion;

	/**
	 * @return the secuenciaOn
	 */
	public String getSecuenciaOn() {
		return secuenciaOn;
	}

	/**
	 * @param secuenciaOn
	 *            the secuenciaOn to set
	 */
	public void setSecuenciaOn(String secuenciaOn) {
		this.secuenciaOn = secuenciaOn;
	}

	/**
	 * @return the track1
	 */
	public String getTrack1() {
		return track1;
	}

	/**
	 * @param track1
	 *            the track1 to set
	 */
	public void setTrack1(String track1) {
		this.track1 = track1;
	}

	/**
	 * @return the track2
	 */
	public String getTrack2() {
		return track2;
	}

	/**
	 * @param track2
	 *            the track2 to set
	 */
	public void setTrack2(String track2) {
		this.track2 = track2;
	}

	/**
	 * @return the track3
	 */
	public String getTrack3() {
		return track3;
	}

	/**
	 * @param track3
	 *            the track3 to set
	 */
	public void setTrack3(String track3) {
		this.track3 = track3;
	}

	/**
	 * @return the debitCardSuffix
	 */
	public String getDebitCardSuffix() {
		return debitCardSuffix;
	}

	/**
	 * @param debitCardSuffix
	 *            the debitCardSuffix to set
	 */
	public void setDebitCardSuffix(String debitCardSuffix) {
		this.debitCardSuffix = debitCardSuffix;
	}

	/**
	 * @return the cardSecurityCode
	 */
	public String getCardSecurityCode() {
		return cardSecurityCode;
	}

	/**
	 * @param cardSecurityCode
	 *            the cardSecurityCode to set
	 */
	public void setCardSecurityCode(String cardSecurityCode) {
		this.cardSecurityCode = cardSecurityCode;
	}

	/**
	 * @return the pin
	 */
	public String getPin() {
		return pin;
	}

	/**
	 * @param pin
	 *            the pin to set
	 */
	public void setPin(String pin) {
		this.pin = pin;
	}

	/**
	 * @return the dni
	 */
	public String getDni() {
		return dni;
	}

	/**
	 * @param dni
	 *            the dni to set
	 */
	public void setDni(String dni) {
		this.dni = dni;
	}

	/**
	 * @return the importe
	 */
	public double getImporte() {
		return importe;
	}

	/**
	 * @param importe
	 *            the importe to set
	 */
	public void setImporte(double importe) {
		this.importe = importe;
	}

	/**
	 * @return the cuentaOrigen
	 */
	public WSCuenta getCuentaOrigen() {
		return cuentaOrigen;
	}

	/**
	 * @param cuentaOrigen
	 *            the cuentaOrigen to set
	 */
	public void setCuentaOrigen(WSCuenta cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}

	/**
	 * @return the cuentaDestino
	 */
	public WSCuenta getCuentaDestino() {
		return cuentaDestino;
	}

	/**
	 * @param cuentaDestino
	 *            the cuentaDestino to set
	 */
	public void setCuentaDestino(WSCuenta cuentaDestino) {
		this.cuentaDestino = cuentaDestino;
	}

	/**
	 * @return the autorizacion
	 */
	public byte getAutorizacion() {
		return autorizacion;
	}

	/**
	 * @param autorizacion
	 *            the autorizacion to set
	 */
	public void setAutorizacion(byte autorizacion) {
		this.autorizacion = autorizacion;
	}

	/**
	 * @return the tokenizado
	 */
	public String getTokenizado() {
		return tokenizado;
	}

	/**
	 * @param tokenizado the tokenizado to set
	 */
	public void setTokenizado(String tokenizado) {
		this.tokenizado = tokenizado;
	}

}
