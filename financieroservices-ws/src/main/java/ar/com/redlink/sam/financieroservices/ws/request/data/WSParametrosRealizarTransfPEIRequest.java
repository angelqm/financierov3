package ar.com.redlink.sam.financieroservices.ws.request.data;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * Wrapper para la parametria de la operacion de transferencias PEI.
 * 
 * @author EXT-IBANEZMA
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSParametrosRealizarTransfPEIRequest {

	private String posentrymode;

	private String track1;

	private String track2;

	private String track3;

	private String numero;

	private String titulardocumento;

	private String idreferenciaoperacioncomercio;

	private BigDecimal importe;

	private String secuenciaOn;

	private String moneda;

	private String codigoseguridad;
	
	private String concepto;

	/**
	 * @return the posentrymode
	 */
	public String getPosentrymode() {
		return posentrymode;
	}

	/**
	 * @param posentrymode
	 *            the posentrymode to set
	 */
	public void setPosentrymode(String posentrymode) {
		this.posentrymode = posentrymode;
	}

	/**
	 * @return the track1
	 */
	public String getTrack1() {
		return track1;
	}

	/**
	 * @param track1
	 *            the track1 to set
	 */
	public void setTrack1(String track1) {
		this.track1 = track1;
	}

	/**
	 * @return the track2
	 */
	public String getTrack2() {
		return track2;
	}

	/**
	 * @param track2
	 *            the track2 to set
	 */
	public void setTrack2(String track2) {
		this.track2 = track2;
	}

	/**
	 * @return the track3
	 */
	public String getTrack3() {
		return track3;
	}

	/**
	 * @param track3
	 *            the track3 to set
	 */
	public void setTrack3(String track3) {
		this.track3 = track3;
	}

	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * @param numero
	 *            the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * @return the titulardocumento
	 */
	public String getTitulardocumento() {
		return titulardocumento;
	}

	/**
	 * @param titulardocumento
	 *            the titulardocumento to set
	 */
	public void setTitulardocumento(String titulardocumento) {
		this.titulardocumento = titulardocumento;
	}

	/**
	 * @return the idreferenciaoperacioncomercio
	 */
	public String getIdreferenciaoperacioncomercio() {
		return idreferenciaoperacioncomercio;
	}

	/**
	 * @param idreferenciaoperacioncomercio
	 *            the idreferenciaoperacioncomercio to set
	 */
	public void setIdreferenciaoperacioncomercio(String idreferenciaoperacioncomercio) {
		this.idreferenciaoperacioncomercio = idreferenciaoperacioncomercio;
	}

	/**
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}

	/**
	 * @param importe
	 *            the importe to set
	 */
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}	

	/**
	 * @return the monedacuenta
	 */
	public String getMoneda() {
		return moneda;
	}

	/**
	 * @param monedacuenta
	 *            the monedacuenta to set
	 */
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	/**
	 * @return the codigoseguridad
	 */
	public String getCodigoseguridad() {
		return codigoseguridad;
	}

	/**
	 * @param codigoseguridad
	 *            the codigoseguridad to set
	 */
	public void setCodigoseguridad(String codigoseguridad) {
		this.codigoseguridad = codigoseguridad;
	}

	/**
	 * @return the secuenciaOn
	 */
	public String getSecuenciaOn() {
		return secuenciaOn;
	}

	/**
	 * @param secuenciaOn the secuenciaOn to set
	 */
	public void setSecuenciaOn(String secuenciaOn) {
		this.secuenciaOn = secuenciaOn;
	}

	/**
	 * @return the concepto
	 */
	public String getConcepto() {
		return concepto;
	}

	/**
	 * @param concepto the concepto to set
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

}
