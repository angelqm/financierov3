/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.ws.namespace;

/**
 * Namespaces para financieroservices.
 * 
 * @author David Cisneros
 * 
 */
public class SAMNamespace {

	public final static String REQUEST_NAMESPACE = "http://request.ws.financieroservices.redlink.com.ar/";
	public final static String RESPONSE_NAMESPACE = "http://response.ws.financieroservices.redlink.com.ar/";

}
