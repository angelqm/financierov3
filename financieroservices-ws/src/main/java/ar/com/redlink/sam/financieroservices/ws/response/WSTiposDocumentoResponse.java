/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.ws.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;

import ar.com.redlink.sam.financieroservices.ws.data.WSDocumento;
import ar.com.redlink.sam.financieroservices.ws.response.base.WSAbstractResponse;

/**
 * Wrapper para los parametros de retorno del request de obtencion de ultimas
 * operaciones.
 * 
 * @author HE_E90104
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSTiposDocumentoResponse extends WSAbstractResponse {

	@XmlElementWrapper(name = "documentoTipos", nillable = false, required = true)
	private List<WSDocumento> documentoTipos;

	/**
	 * @return the documentoTipos
	 */
	public List<WSDocumento> getDocumentoTipos() {
		return documentoTipos;
	}

	/**
	 * @param documentoTipos the documentoTipos to set
	 */
	public void setDocumentoTipos(List<WSDocumento> documentoTipos) {
		this.documentoTipos = documentoTipos;
	}
	
	

	

}
