package ar.com.redlink.sam.financieroservices.ws.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.response.base.WSAbstractResponse;

/**
 * Response para la operacion PEI de transferencia.
 * 
 * @author EXT-IBANEZMA
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSRealizarTransferenciaPEIResponse extends WSAbstractResponse {	
	
	@XmlElement(name = "resultado", nillable = false, required = false)
	private WSRealizarTransferenciaResultadoPEIResponse resultado;	

	/**
	 * @return the resultado
	 */
	public WSRealizarTransferenciaResultadoPEIResponse getResultado() {
		return resultado;
	}

	/**
	 * @param resultado the resultado to set
	 */
	public void setResultado(WSRealizarTransferenciaResultadoPEIResponse resultado) {
		this.resultado = resultado;
	}
	

}
