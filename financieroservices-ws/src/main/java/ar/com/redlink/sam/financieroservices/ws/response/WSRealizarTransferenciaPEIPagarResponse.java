package ar.com.redlink.sam.financieroservices.ws.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.response.base.WSAbstractResponse;

/**
 * Response para la operacion PEI desde PAGAR 
 * 
 * @author AQM
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSRealizarTransferenciaPEIPagarResponse extends WSAbstractResponse {	
	
	@XmlElement(name = "resultado", nillable = false, required = false)
	private WSRealizarTransferenciaResultadoPEIPagarResponse resultado;

	/**
	 * @return the resultado
	 */
	public WSRealizarTransferenciaResultadoPEIPagarResponse getResultado() {
		return resultado;
	}

	/**
	 * @param resultado the resultado to set
	 */
	public void setResultado(WSRealizarTransferenciaResultadoPEIPagarResponse resultado) {
		this.resultado = resultado;
	}

}
