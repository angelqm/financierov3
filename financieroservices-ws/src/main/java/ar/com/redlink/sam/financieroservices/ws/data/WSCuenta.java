/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  25/01/2015 - 14:42:22
 */

package ar.com.redlink.sam.financieroservices.ws.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Representacion de una cuenta.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSCuenta {

	@XmlElement(nillable = false, required = true)
	private String numeroCuenta;

	@XmlElement(nillable = false, required = true)
	private String tipoCuenta;

	@XmlElement(nillable = false, required = true)
	private String monedaCuenta;

	@XmlElement(nillable = false, required = true)
	private String estadoCuenta;

	/**
	 * @return the numeroCuenta
	 */
	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	/**
	 * @param numeroCuenta
	 *            the numeroCuenta to set
	 */
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	/**
	 * @return the tipoCuenta
	 */
	public String getTipoCuenta() {
		return tipoCuenta;
	}

	/**
	 * @param tipoCuenta
	 *            the tipoCuenta to set
	 */
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	/**
	 * @return the monedaCuenta
	 */
	public String getMonedaCuenta() {
		return monedaCuenta;
	}

	/**
	 * @param monedaCuenta
	 *            the monedaCuenta to set
	 */
	public void setMonedaCuenta(String monedaCuenta) {
		this.monedaCuenta = monedaCuenta;
	}

	/**
	 * @return the estadoCuenta
	 */
	public String getEstadoCuenta() {
		return estadoCuenta;
	}

	/**
	 * @param estadoCuenta
	 *            the estadoCuenta to set
	 */
	public void setEstadoCuenta(String estadoCuenta) {
		this.estadoCuenta = estadoCuenta;
	}
}
