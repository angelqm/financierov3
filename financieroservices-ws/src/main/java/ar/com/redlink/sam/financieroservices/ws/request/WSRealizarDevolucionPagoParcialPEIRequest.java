package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosRealizarDevolucionParcialPEIRequest;

/**
 * Request para la realizacion de una devolucion PEI.
 * 
 * @author EXT-IBANEZMA
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSRealizarDevolucionPagoParcialPEIRequest extends WSAbstractRequest {

	@XmlElement(name = "realizarDevolucionPagoParcialPEIData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosRealizarDevolucionParcialPEIRequest realizarDevolucionParcialPEIRequest;

	/**
	 * @return the realizarDevolucionParcialPEIRequest
	 */
	public WSParametrosRealizarDevolucionParcialPEIRequest getRealizarDevolucionParcialPEIRequest() {
		return realizarDevolucionParcialPEIRequest;
	}

	/**
	 * @param realizarDevolucionParcialPEIRequest
	 *            the realizarDevolucionParcialPEIRequest to set
	 */
	public void setRealizarDevolucionParcialPEIRequest(
			WSParametrosRealizarDevolucionParcialPEIRequest realizarDevolucionParcialPEIRequest) {
		this.realizarDevolucionParcialPEIRequest = realizarDevolucionParcialPEIRequest;
	}

}
