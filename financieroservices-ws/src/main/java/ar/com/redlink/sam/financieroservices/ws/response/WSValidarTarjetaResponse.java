/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.ws.response;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import ar.com.redlink.sam.financieroservices.ws.data.WSCuenta;
import ar.com.redlink.sam.financieroservices.ws.data.WSOperacion;
import ar.com.redlink.sam.financieroservices.ws.response.base.WSAbstractResponse;

/**
 * Wrapper para los parametros de retorno del request de validacion de tarjeta.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSValidarTarjetaResponse extends WSAbstractResponse {

	@XmlElement(nillable = false, required = true)
	private String secuenciaOn;

	@XmlElement(nillable = false, required = true)
	private String entidadEmisora;

	@XmlElement(nillable = false, required = true)
	private String tokenizado;

	@XmlElement(nillable = false, required = true)
	private String estadoTarjeta;

	@XmlElement(nillable = false, required = true)
	private String fechaOn;

	@XmlElement(name = "operacion")
	@XmlElementWrapper(name = "operaciones", nillable = false, required = true)
	private List<WSOperacion> operaciones;

	@XmlElement(name = "cuenta")
	@XmlElementWrapper(name = "cuentas", nillable = false, required = true)
	private List<WSCuenta> cuentas;

	/**
	 * @return the secuenciaOn
	 */
	public String getSecuenciaOn() {
		return secuenciaOn;
	}

	/**
	 * @param secuenciaOn
	 *            the secuenciaOn to set
	 */
	public void setSecuenciaOn(String secuenciaOn) {
		this.secuenciaOn = secuenciaOn;
	}

	/**
	 * @return the entidadEmisora
	 */
	public String getEntidadEmisora() {
		return entidadEmisora;
	}

	/**
	 * @param entidadEmisora
	 *            the entidadEmisora to set
	 */
	public void setEntidadEmisora(String entidadEmisora) {
		this.entidadEmisora = entidadEmisora;
	}

	/**
	 * @return the tokenizado
	 */
	public String getTokenizado() {
		return tokenizado;
	}

	/**
	 * @param tokenizado
	 *            the tokenizado to set
	 */
	public void setTokenizado(String tokenizado) {
		this.tokenizado = tokenizado;
	}

	/**
	 * @return the cuentas
	 */
	public List<WSCuenta> getCuentas() {
		return cuentas;
	}

	/**
	 * @param cuentas
	 *            the cuentas to set
	 */
	public void setCuentas(List<WSCuenta> cuentas) {
		this.cuentas = cuentas;
	}

	/**
	 * @return the fechaOn
	 */
	public String getFechaOn() {
		return fechaOn;
	}

	/**
	 * @param fechaOn
	 *            the fechaOn to set
	 */
	public void setFechaOn(String fechaOn) {
		this.fechaOn = fechaOn;
	}

	/**
	 * @return the estadoTarjeta
	 */
	public String getEstadoTarjeta() {
		return estadoTarjeta;
	}

	/**
	 * @param estadoTarjeta
	 *            the estadoTarjeta to set
	 */
	public void setEstadoTarjeta(String estadoTarjeta) {
		this.estadoTarjeta = estadoTarjeta;
	}

	/**
	 * @return the operaciones
	 */
	public List<WSOperacion> getOperaciones() {
		return operaciones;
	}

	/**
	 * @param operaciones
	 *            the operaciones to set
	 */
	public void setOperaciones(List<WSOperacion> operaciones) {
		this.operaciones = operaciones;
	}

}
