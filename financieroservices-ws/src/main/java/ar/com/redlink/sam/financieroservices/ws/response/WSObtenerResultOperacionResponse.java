/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.ws.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.response.base.WSAbstractResponse;

/**
 * Wrapper para los parametros de retorno del request de obtencion de resultados
 * de operacion.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSObtenerResultOperacionResponse extends WSAbstractResponse {

	@XmlElement(nillable = false, required = true)
	private String codigoOperacion;

	@XmlElement(nillable = false, required = true)
	private String nombreOperacion;

	@XmlElement(nillable = false, required = true)
	private String importeOperacion;

	@XmlElement(nillable = false, required = true)
	private String secuenciaOnOperacion;

	@XmlElement(nillable = false, required = true)
	private String estadoRequerimiento;

	@XmlElement(nillable = false, required = true)
	private String fechaOnOperacion;

	@XmlElement(nillable = false, required = true)
	private String fechaHoraOperacion;

	@XmlElement(nillable = false, required = true)
	private String ticket;

	/**
	 * @return the codigoOperacion
	 */
	public String getCodigoOperacion() {
		return codigoOperacion;
	}

	/**
	 * @param codigoOperacion
	 *            the codigoOperacion to set
	 */
	public void setCodigoOperacion(String codigoOperacion) {
		this.codigoOperacion = codigoOperacion;
	}

	/**
	 * @return the nombreOperacion
	 */
	public String getNombreOperacion() {
		return nombreOperacion;
	}

	/**
	 * @param nombreOperacion
	 *            the nombreOperacion to set
	 */
	public void setNombreOperacion(String nombreOperacion) {
		this.nombreOperacion = nombreOperacion;
	}

	/**
	 * @return the importeOperacion
	 */
	public String getImporteOperacion() {
		return importeOperacion;
	}

	/**
	 * @param importeOperacion
	 *            the importeOperacion to set
	 */
	public void setImporteOperacion(String importeOperacion) {
		this.importeOperacion = importeOperacion;
	}

	/**
	 * @return the secuenciaOnOperacion
	 */
	public String getSecuenciaOnOperacion() {
		return secuenciaOnOperacion;
	}

	/**
	 * @param secuenciaOnOperacion
	 *            the secuenciaOnOperacion to set
	 */
	public void setSecuenciaOnOperacion(String secuenciaOnOperacion) {
		this.secuenciaOnOperacion = secuenciaOnOperacion;
	}

	/**
	 * @return the estadoRequerimiento
	 */
	public String getEstadoRequerimiento() {
		return estadoRequerimiento;
	}

	/**
	 * @param estadoRequerimiento
	 *            the estadoRequerimiento to set
	 */
	public void setEstadoRequerimiento(String estadoRequerimiento) {
		this.estadoRequerimiento = estadoRequerimiento;
	}

	/**
	 * @return the ticket
	 */
	public String getTicket() {
		return ticket;
	}

	/**
	 * @param ticket
	 *            the ticket to set
	 */
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	/**
	 * @return the fechaOnOperacion
	 */
	public String getFechaOnOperacion() {
		return fechaOnOperacion;
	}

	/**
	 * @param fechaOnOperacion
	 *            the fechaOnOperacion to set
	 */
	public void setFechaOnOperacion(String fechaOnOperacion) {
		this.fechaOnOperacion = fechaOnOperacion;
	}

	/**
	 * @return the fechaHoraOperacion
	 */
	public String getFechaHoraOperacion() {
		return fechaHoraOperacion;
	}

	/**
	 * @param fechaHoraOperacion
	 *            the fechaHoraOperacion to set
	 */
	public void setFechaHoraOperacion(String fechaHoraOperacion) {
		this.fechaHoraOperacion = fechaHoraOperacion;
	}
}
