/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.ws.response.base;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;

/**
 * AbstractResponse que wrappea los atributos comunes.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSAbstractResponse {
	@XmlElement(name = "cabecera", nillable = false, required = true, namespace = SAMNamespace.RESPONSE_NAMESPACE)
	protected WSHeaderResponse header;

	/**
	 * @return the header
	 */
	public WSHeaderResponse getHeader() {
		return header;
	}

	/**
	 * @param header
	 *            the header to set
	 */
	public void setHeader(WSHeaderResponse header) {
		this.header = header;
	}

}
