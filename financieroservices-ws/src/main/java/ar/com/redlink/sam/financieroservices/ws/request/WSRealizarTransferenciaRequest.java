/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  11/03/2015 - 22:23:50
 */

package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosRealizarTransfRequest;

/**
 * Request para la realizacion de una transferencia.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSRealizarTransferenciaRequest extends WSAbstractRequest {

	@XmlElement(name = "realizarTransferenciaData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosRealizarTransfRequest realizarTransferenciaRequest;

	/**
	 * @return the realizarTransferenciaRequest
	 */
	public WSParametrosRealizarTransfRequest getRealizarTransferenciaRequest() {
		return realizarTransferenciaRequest;
	}

	/**
	 * @param realizarTransferenciaRequest
	 *            the realizarTransferenciaRequest to set
	 */
	public void setRealizarTransferenciaRequest(
			WSParametrosRealizarTransfRequest realizarTransferenciaRequest) {
		this.realizarTransferenciaRequest = realizarTransferenciaRequest;
	}
}
