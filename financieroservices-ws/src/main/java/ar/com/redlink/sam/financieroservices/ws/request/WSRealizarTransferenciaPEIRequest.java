package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosRealizarTransfPEIRequest;

/**
 * Request para la realizacion de una transferencia PEI.
 * 
 * @author EXT-IBANEZMA
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSRealizarTransferenciaPEIRequest extends WSAbstractRequest {

	@XmlElement(name = "realizarTransferenciaPEIData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosRealizarTransfPEIRequest realizarTransferenciaPEIRequest;

	/**
	 * @return the realizarTransferenciaPEIRequest
	 */
	public WSParametrosRealizarTransfPEIRequest getRealizarTransferenciaPEIRequest() {
		return realizarTransferenciaPEIRequest;
	}

	/**
	 * @param realizarTransferenciaPEIRequest the realizarTransferenciaPEIRequest to set
	 */
	public void setRealizarTransferenciaPEIRequest(WSParametrosRealizarTransfPEIRequest realizarTransferenciaPEIRequest) {
		this.realizarTransferenciaPEIRequest = realizarTransferenciaPEIRequest;
	}
	
}
