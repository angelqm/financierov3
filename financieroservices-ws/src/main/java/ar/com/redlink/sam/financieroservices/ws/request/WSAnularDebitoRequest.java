/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  10/03/2015 - 23:23:13
 */

package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosAnularDebitoRequest;

/**
 * Wrapper para el request de anulacion de operacion de debito.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSAnularDebitoRequest extends WSAbstractRequest {

	@XmlElement(name = "anularDebitoData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosAnularDebitoRequest anularDebitoRequest;

	/**
	 * @return the anularDebitoRequest
	 */
	public WSParametrosAnularDebitoRequest getAnularDebitoRequest() {
		return anularDebitoRequest;
	}

	/**
	 * @param anularDebitoRequest
	 *            the anularDebitoRequest to set
	 */
	public void setAnularDebitoRequest(
			WSParametrosAnularDebitoRequest anularDebitoRequest) {
		this.anularDebitoRequest = anularDebitoRequest;
	}
}
