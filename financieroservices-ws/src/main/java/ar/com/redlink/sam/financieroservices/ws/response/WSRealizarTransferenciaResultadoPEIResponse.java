package ar.com.redlink.sam.financieroservices.ws.response;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author EXT-IBANEZMA
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSRealizarTransferenciaResultadoPEIResponse {

	@XmlElement(nillable = false, required = true)
	private String secuenciaOn;
	
	@XmlElement(nillable = false, required = true)
	private String tipooperacion;

	@XmlElement(nillable = false, required = true)
	private String idoperacion;

	@XmlElement(nillable = false, required = true)
	private String estado;

	@XmlElement(nillable = false, required = true)
	private String fecha;
	
	@XmlElement(nillable = false, required = true)
	private String numeroReferenciaBancaria;
	

	/**
	 * @return the tipooperacion
	 */
	public String getTipooperacion() {
		return tipooperacion;
	}

	/**
	 * @param tipooperacion
	 *            the tipooperacion to set
	 */
	public void setTipooperacion(String tipooperacion) {
		this.tipooperacion = tipooperacion;
	}

	/**
	 * @return the idoperacion
	 */
	public String getIdoperacion() {
		return idoperacion;
	}

	/**
	 * @param idoperacion
	 *            the idoperacion to set
	 */
	public void setIdoperacion(String idoperacion) {
		this.idoperacion = idoperacion;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado
	 *            the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha
	 *            the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the secuenciaOn
	 */
	public String getSecuenciaOn() {
		return secuenciaOn;
	}

	/**
	 * @param secuenciaOn
	 *            the secuenciaOn to set
	 */
	public void setSecuenciaOn(String secuenciaOn) {
		this.secuenciaOn = secuenciaOn;
	}

	public String getNumeroReferenciaBancaria() {
		return numeroReferenciaBancaria;
	}

	public void setNumeroReferenciaBancaria(String numeroReferenciaBancaria) {
		this.numeroReferenciaBancaria = numeroReferenciaBancaria;
	}
	
}
