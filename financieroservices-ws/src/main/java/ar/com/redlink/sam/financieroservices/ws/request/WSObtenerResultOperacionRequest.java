/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosResultadoOperacionRequest;

/**
 * Wrapper para los parametros del request de obtencion de resultado de
 * operacion.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSObtenerResultOperacionRequest extends WSAbstractRequest {

	@XmlElement(name = "obtenerResultadoOpData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosResultadoOperacionRequest obtenerResultOperacionRequest;

	/**
	 * @return the obtenerResultOperacionRequest
	 */
	public WSParametrosResultadoOperacionRequest getObtenerResultOperacionRequest() {
		return obtenerResultOperacionRequest;
	}

	/**
	 * @param obtenerResultOperacionRequest
	 *            the obtenerResultOperacionRequest to set
	 */
	public void setObtenerResultOperacionRequest(
			WSParametrosResultadoOperacionRequest obtenerResultOperacionRequest) {
		this.obtenerResultOperacionRequest = obtenerResultOperacionRequest;
	}

}
