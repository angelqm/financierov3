/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosValidarTarjetaRequest;

/**
 * Wrapper para los parametros del request de validacion de tarjeta.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSValidarTarjetaRequest extends WSAbstractRequest {

	@XmlElement(name = "validarTarjetaData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	private WSParametrosValidarTarjetaRequest validarTarjetaRequest;

	/**
	 * @return the validarTarjetaRequest
	 */
	public WSParametrosValidarTarjetaRequest getValidarTarjetaRequest() {
		return validarTarjetaRequest;
	}

	/**
	 * @param validarTarjetaRequest
	 *            the validarTarjetaRequest to set
	 */
	public void setValidarTarjetaRequest(
			WSParametrosValidarTarjetaRequest validarTarjetaRequest) {
		this.validarTarjetaRequest = validarTarjetaRequest;
	}

}
