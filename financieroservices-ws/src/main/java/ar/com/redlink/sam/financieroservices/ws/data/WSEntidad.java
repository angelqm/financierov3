/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: he_e90104
 * Date:  13/05/2019
 */

package ar.com.redlink.sam.financieroservices.ws.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Representacion de una cuenta.
 * 
 * @author he_e90104
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSEntidad {

	@XmlElement(nillable = false, required = true)
	private String codDispositivo;
	
	@XmlElement(nillable = false, required = true)
	private String codSucursal;

	@XmlElement(nillable = false, required = true)
	private String codTerminal;

	@XmlElement(nillable = false, required = true)
	private String fiidEntidad;


	/**
	 * @return the codDispositivo
	 */
	public String getCodDispositivo() {
		return codDispositivo;
	}

	/**
	 * @param codDispositivo the codDispositivo to set
	 */
	public void setCodDispositivo(String codDispositivo) {
		this.codDispositivo = codDispositivo;
	}

	/**
	 * @return the codSucursal
	 */
	public String getCodSucursal() {
		return codSucursal;
	}

	/**
	 * @param codSucursal the codSucursal to set
	 */
	public void setCodSucursal(String codSucursal) {
		this.codSucursal = codSucursal;
	}

	/**
	 * @return the codTerminal
	 */
	public String getCodTerminal() {
		return codTerminal;
	}

	/**
	 * @param codTerminal the codTerminal to set
	 */
	public void setCodTerminal(String codTerminal) {
		this.codTerminal = codTerminal;
	}

	/**
	 * @return the fiidEntidad
	 */
	public String getFiidEntidad() {
		return fiidEntidad;
	}

	/**
	 * @param fiidEntidad the fiidEntidad to set
	 */
	public void setFiidEntidad(String fiidEntidad) {
		this.fiidEntidad = fiidEntidad;
	}
	
}
