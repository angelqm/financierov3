/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.ws.response.base;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Header base con los atributos comunes.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSHeaderResponse {

	@XmlElement(nillable = false, required = true)
	protected String timeStamp;

	@XmlElement(nillable = false, required = true)
	protected String codigoRespuesta;

	@XmlElement(nillable = false, required = true)
	protected String descripcionRespuesta;
	
	@XmlElement(nillable = false, required = true)
	protected String codigoRespuestaB24;
	
	@XmlElement(nillable = false, required = true)
	protected String descripcionRespuestaB24;

	/**
	 * Constructor default.
	 */
	public WSHeaderResponse() {

	}

	/**
	 * Constructor que instancia el objeto con un timestamp definido.
	 * 
	 * @param timeStamp
	 */
	public WSHeaderResponse(String timeStamp) {
		this.setTimeStamp(timeStamp);
	}

	/**
	 * @return the timeStamp
	 */
	public String getTimeStamp() {
		return timeStamp;
	}

	/**
	 * @param timeStamp
	 *            the timeStamp to set
	 */
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	/**
	 * @return the codigoRespuesta
	 */
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}

	/**
	 * @param codigoRespuesta
	 *            the codigoRespuesta to set
	 */
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

	/**
	 * @return the descripcionRespuesta
	 */
	public String getDescripcionRespuesta() {
		return descripcionRespuesta;
	}

	/**
	 * @param descripcionRespuesta
	 *            the descripcionRespuesta to set
	 */
	public void setDescripcionRespuesta(String descripcionRespuesta) {
		this.descripcionRespuesta = descripcionRespuesta;
	}

	/**
	 * @return the codigoRespuestaB24
	 */
	public String getCodigoRespuestaB24() {
		return codigoRespuestaB24;
	}

	/**
	 * @param codigoRespuestaB24 the codigoRespuestaB24 to set
	 */
	public void setCodigoRespuestaB24(String codigoRespuestaB24) {
		this.codigoRespuestaB24 = codigoRespuestaB24;
	}

	/**
	 * @return the descripcionRespuestaB24
	 */
	public String getDescripcionRespuestaB24() {
		return descripcionRespuestaB24;
	}

	/**
	 * @param descripcionRespuestaB24 the descripcionRespuestaB24 to set
	 */
	public void setDescripcionRespuestaB24(String descripcionRespuestaB24) {
		this.descripcionRespuestaB24 = descripcionRespuestaB24;
	}

}
