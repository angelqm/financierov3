/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.ws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import ar.com.redlink.sam.financieroservices.ws.namespace.SAMNamespace;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosEnrolamientoRequest;

/**
 * Wrapper para los parametros del request de enrolamiento de terminal.
 * 
 * @author aguerrea
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSEnrolamientoRequest extends WSAbstractRequest {

	@XmlElement(name = "enrolamientoData", nillable = false, required = true, namespace = SAMNamespace.REQUEST_NAMESPACE)
	protected WSParametrosEnrolamientoRequest enrolamientoDataRequest;

	/**
	 * @return the enrolamientoDataRequest
	 */
	public WSParametrosEnrolamientoRequest getEnrolamientoDataRequest() {
		return enrolamientoDataRequest;
	}

	/**
	 * @param enrolamientoDataRequest
	 *            the enrolamientoDataRequest to set
	 */
	public void setEnrolamientoDataRequest(
			WSParametrosEnrolamientoRequest enrolamientoDataRequest) {
		this.enrolamientoDataRequest = enrolamientoDataRequest;
	}

}
