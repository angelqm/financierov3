/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: he_e90104
 * Date:  16/05/2019
 */

package ar.com.redlink.sam.financieroservices.ws.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Representacion de una cuenta.
 * 
 * @author he_e90104
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class WSTrack {

	@XmlElement(nillable = false, required = true)
	private String track1;

	@XmlElement(nillable = false, required = true)
	private String track2;
	
	@XmlElement(nillable = false, required = true)
	private String track3;

	/**
	 * @return the track1
	 */
	public String getTrack1() {
		return track1;
	}

	/**
	 * @param track1 the track1 to set
	 */
	public void setTrack1(String track1) {
		this.track1 = track1;
	}

	/**
	 * @return the track2
	 */
	public String getTrack2() {
		return track2;
	}

	/**
	 * @param track2 the track2 to set
	 */
	public void setTrack2(String track2) {
		this.track2 = track2;
	}

	/**
	 * @return the track3
	 */
	public String getTrack3() {
		return track3;
	}

	/**
	 * @param track3 the track3 to set
	 */
	public void setTrack3(String track3) {
		this.track3 = track3;
	}
	
}
