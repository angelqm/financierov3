package ar.com.redlink.sam.financieroservices.dto.response;

import java.io.Serializable;

/**
 * 
 * @author EXT-IBANEZMA
 *
 */
public class ConsultaOperacionPEIPagarDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	
	private ConsultaOperacionResultadoPEIPagarDTO resultado;

	/**
	 * @return the resultado
	 */
	public ConsultaOperacionResultadoPEIPagarDTO getResultado() {
		return resultado;
	}

	/**
	 * @param resultado the resultado to set
	 */
	public void setResultado(ConsultaOperacionResultadoPEIPagarDTO resultado) {
		this.resultado = resultado;
	}
	

}
