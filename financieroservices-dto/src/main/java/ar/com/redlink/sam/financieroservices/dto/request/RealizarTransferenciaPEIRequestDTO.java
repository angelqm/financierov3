package ar.com.redlink.sam.financieroservices.dto.request;

import java.io.Serializable;

/**
 * DTO para la operacion de transferencia PEI.
 * 
 * @author EXT-IBANEZMA
 *
 */
public class RealizarTransferenciaPEIRequestDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private RealizarTransferenciaPEIPagoRequestDTO pago;

	/**
	 * @return the pago
	 */
	public RealizarTransferenciaPEIPagoRequestDTO getPago() {
		return pago;
	}

	/**
	 * @param pago the pago to set
	 */
	public void setPago(RealizarTransferenciaPEIPagoRequestDTO pago) {
		this.pago = pago;
	}

}
