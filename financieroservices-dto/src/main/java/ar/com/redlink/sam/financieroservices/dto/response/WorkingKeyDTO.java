/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  28/01/2015 - 14:41:38
 */

package ar.com.redlink.sam.financieroservices.dto.response;

import java.io.Serializable;

/**
 * DTO para el retorno de una solicitud de Working Key.
 * 
 * @author aguerrea
 * 
 */
public class WorkingKeyDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1648775923936226182L;

	private String workingKey;

	private String secuenciaOn;
	
	private String fechaOn;

	/**
	 * Inicializa el objeto con un valor indicado.
	 * 
	 * @param workingKey
	 */
	public WorkingKeyDTO(String workingKey) {
		this.workingKey = workingKey;
	}

	/**
	 * @return the workingKey
	 */
	public String getWorkingKey() {
		return workingKey;
	}

	/**
	 * @param workingKey
	 *            the workingKey to set
	 */
	public void setWorkingKey(String workingKey) {
		this.workingKey = workingKey;
	}

	/**
	 * @return the secuenciaOn
	 */
	public String getSecuenciaOn() {
		return secuenciaOn;
	}

	/**
	 * @param secuenciaOn
	 *            the secuenciaOn to set
	 */
	public void setSecuenciaOn(String secuenciaOn) {
		this.secuenciaOn = secuenciaOn;
	}

	/**
	 * @return the fechaOn
	 */
	public String getFechaOn() {
		return fechaOn;
	}

	/**
	 * @param fechaOn the fechaOn to set
	 */
	public void setFechaOn(String fechaOn) {
		this.fechaOn = fechaOn;
	}
}
