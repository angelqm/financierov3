/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  28/01/2015 - 14:44:30
 */

package ar.com.redlink.sam.financieroservices.dto.response;

import java.io.Serializable;

/**
 * DTO para el resultado de una consulta de estado de operacion.
 * 
 * @author aguerrea
 * 
 */
public class EstadoOperacionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5078903100246194287L;

	private String codigoOperacion;

	private String nombreOperacion;

	private String importeOperacion;

	private String secuenciaOnOperacion;

	private String estadoRequerimiento;
	
	private String fechaOn;
	
	private String fechaHoraOperacion;

	private String ticket;

	/**
	 * @return the codigoOperacion
	 */
	public String getCodigoOperacion() {
		return codigoOperacion;
	}

	/**
	 * @param codigoOperacion
	 *            the codigoOperacion to set
	 */
	public void setCodigoOperacion(String codigoOperacion) {
		this.codigoOperacion = codigoOperacion;
	}

	/**
	 * @return the nombreOperacion
	 */
	public String getNombreOperacion() {
		return nombreOperacion;
	}

	/**
	 * @param nombreOperacion
	 *            the nombreOperacion to set
	 */
	public void setNombreOperacion(String nombreOperacion) {
		this.nombreOperacion = nombreOperacion;
	}

	/**
	 * @return the importeOperacion
	 */
	public String getImporteOperacion() {
		return importeOperacion;
	}

	/**
	 * @param importeOperacion
	 *            the importeOperacion to set
	 */
	public void setImporteOperacion(String importeOperacion) {
		this.importeOperacion = importeOperacion;
	}

	/**
	 * @return the secuenciaOnOperacion
	 */
	public String getSecuenciaOnOperacion() {
		return secuenciaOnOperacion;
	}

	/**
	 * @param secuenciaOnOperacion
	 *            the secuenciaOnOperacion to set
	 */
	public void setSecuenciaOnOperacion(String secuenciaOnOperacion) {
		this.secuenciaOnOperacion = secuenciaOnOperacion;
	}

	/**
	 * @return the estadoRequerimiento
	 */
	public String getEstadoRequerimiento() {
		return estadoRequerimiento;
	}

	/**
	 * @param estadoRequerimiento
	 *            the estadoRequerimiento to set
	 */
	public void setEstadoRequerimiento(String estadoRequerimiento) {
		this.estadoRequerimiento = estadoRequerimiento;
	}

	/**
	 * @return the ticket
	 */
	public String getTicket() {
		return ticket;
	}

	/**
	 * @param ticket
	 *            the ticket to set
	 */
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	/**
	 * @return the fechaOn
	 */
	public String getFechaOn() {
		return fechaOn;
	}

	/**
	 * @param fechaOn the fechaOn to set
	 */
	public void setFechaOn(String fechaOn) {
		this.fechaOn = fechaOn;
	}

	/**
	 * @return the fechaHoraOperacion
	 */
	public String getFechaHoraOperacion() {
		return fechaHoraOperacion;
	}

	/**
	 * @param fechaHoraOperacion the fechaHoraOperacion to set
	 */
	public void setFechaHoraOperacion(String fechaHoraOperacion) {
		this.fechaHoraOperacion = fechaHoraOperacion;
	}

}
