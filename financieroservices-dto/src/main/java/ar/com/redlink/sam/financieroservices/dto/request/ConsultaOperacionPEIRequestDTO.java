package ar.com.redlink.sam.financieroservices.dto.request;

import java.io.Serializable;

/**
 * 
 * @author EXT-IBANEZMA
 *
 */
public class ConsultaOperacionPEIRequestDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ConsultaPEIRequestDTO consulta;

	private OrigenPEIRequestDTO origen;

	/**
	 * @return the consulta
	 */
	public ConsultaPEIRequestDTO getConsulta() {
		return consulta;
	}

	/**
	 * @param consulta
	 *            the consulta to set
	 */
	public void setConsulta(ConsultaPEIRequestDTO consulta) {
		this.consulta = consulta;
	}

	/**
	 * @return the origen
	 */
	public OrigenPEIRequestDTO getOrigen() {
		return origen;
	}

	/**
	 * @param origen
	 *            the origen to set
	 */
	public void setOrigen(OrigenPEIRequestDTO origen) {
		this.origen = origen;
	}

}
