/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  30/03/2015 - 00:56:06
 */

package ar.com.redlink.sam.financieroservices.dto.response;

import ar.com.redlink.sam.financieroservices.dto.response.common.OperacionCommonDTO;

/**
 * DTO para la respuesta de extraccion en cuotas.
 * 
 * @author aguerrea
 * 
 */
public class ExtraccionCuotasDTO extends OperacionCommonDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1015886290714340413L;

}
