package ar.com.redlink.sam.financieroservices.dto.request;

import ar.com.redlink.sam.financieroservices.dto.request.common.FinancieroAccountDTO;

/**
 * DTO para la operacion de extraccion para debito. Se define su representacion
 * para eventuales extensiones.
 */
public class RealizarPagoDebitoRequestDTO extends FinancieroAccountDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8215794815921240956L;

	private String descripcionDebito;
	private String tipoTerminal;

	/**
	 * 
	 * @return
	 */
	public String getDescripcionDebito() {
		return descripcionDebito;
	}

	/**
	 * 
	 * @param descripcionDebito
	 */
	public void setDescripcionDebito(String descripcionDebito) {
		this.descripcionDebito = descripcionDebito;
	}

	/**
	 * 
	 * @return
	 */
	public String getTipoTerminal() {
		return tipoTerminal;
	}

	/**
	 * 
	 * @param tipoTerminal
	 */
	public void setTipoTerminal(String tipoTerminal) {
		this.tipoTerminal = tipoTerminal;
	}

}
