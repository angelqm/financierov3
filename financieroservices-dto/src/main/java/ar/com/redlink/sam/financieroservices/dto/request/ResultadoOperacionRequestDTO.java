/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  28/01/2015 - 14:44:39
 */

package ar.com.redlink.sam.financieroservices.dto.request;

import ar.com.redlink.sam.financieroservices.dto.request.common.FinancieroCommonDTO;

/**
 * DTO para la obtencion de resultado de una operacion.
 * 
 * @author aguerrea
 * 
 */
public class ResultadoOperacionRequestDTO extends FinancieroCommonDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1606313469129747012L;
}
