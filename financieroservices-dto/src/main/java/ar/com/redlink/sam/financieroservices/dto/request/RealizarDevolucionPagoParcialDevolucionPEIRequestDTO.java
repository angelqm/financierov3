package ar.com.redlink.sam.financieroservices.dto.request;

/**
 * 
 * @author EXT-ibanezma
 *
 */
public class RealizarDevolucionPagoParcialDevolucionPEIRequestDTO	extends RealizarDevolucionPagoDevolucionPEIRequestDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer importe;

	/**
	 * @return the importe
	 */
	public Integer getImporte() {
		return importe;
	}

	/**
	 * @param importe
	 *            the importe to set
	 */
	public void setImporte(Integer importe) {
		this.importe = importe;
	}

}
