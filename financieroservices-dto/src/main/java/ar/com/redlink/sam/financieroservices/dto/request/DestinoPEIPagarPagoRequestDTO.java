package ar.com.redlink.sam.financieroservices.dto.request;

import java.io.Serializable;

public class DestinoPEIPagarPagoRequestDTO implements Serializable {

	/**
	 * AQM
	 */
	private static final long serialVersionUID = 2719570625978139944L;
	
	private String codTerminal;
	private String codSucursal;	
	private EntidadPEIRequestDTO entidad; 	
	private CuentaRecaudadoraDTO cuentaRecaudadora;
	
	/**
	 * @return the codTerminal
	 */
	public String getCodTerminal() {
		return codTerminal;
	}
	/**
	 * @param codTerminal the codTerminal to set
	 */
	public void setCodTerminal(String codTerminal) {
		this.codTerminal = codTerminal;
	}
	/**
	 * @return the codSucursal
	 */
	public String getCodSucursal() {
		return codSucursal;
	}
	/**
	 * @param codSucursal the codSucursal to set
	 */
	public void setCodSucursal(String codSucursal) {
		this.codSucursal = codSucursal;
	}
	/**
	 * @return the entidad
	 */
	public EntidadPEIRequestDTO getEntidad() {
		return entidad;
	}
	/**
	 * @param entidad the entidad to set
	 */
	public void setEntidad(EntidadPEIRequestDTO entidad) {
		this.entidad = entidad;
	}
	/**
	 * @return the cuentaRecaudadora
	 */
	public CuentaRecaudadoraDTO getCuentaRecaudadora() {
		return cuentaRecaudadora;
	}
	/**
	 * @param cuentaRecaudadora the cuentaRecaudadora to set
	 */
	public void setCuentaRecaudadora(CuentaRecaudadoraDTO cuentaRecaudadora) {
		this.cuentaRecaudadora = cuentaRecaudadora;
	}

}
