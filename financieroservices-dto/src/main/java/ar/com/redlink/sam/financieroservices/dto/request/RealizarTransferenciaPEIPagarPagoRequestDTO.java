package ar.com.redlink.sam.financieroservices.dto.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * DTO para la operacion de transferencia PEI.
 * 
 * @author he_e90104
 *
 */
public class RealizarTransferenciaPEIPagarPagoRequestDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5324385204777931444L;

/*	ESTO VA A ENLACE PAGOS*/

	private DestinoPEIPagarPagoRequestDTO destino;
	
	private RealizarTransferenciaPEITrackDataRequestDTO tracks;
	
	private String numero;
	
	private String codigoSeguridad;
	
	private TipoDocumentoDTO documento;
	
	private String idCanal;
	
	private String idReferenciaTrx;

	private String idReferenciaOperacion;

	private Integer importe;

	private String moneda;	
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String concepto;

	/**
	 * @return the destino
	 */
	public DestinoPEIPagarPagoRequestDTO getDestino() {
		return destino;
	}

	/**
	 * @param destino the destino to set
	 */
	public void setDestino(DestinoPEIPagarPagoRequestDTO destino) {
		this.destino = destino;
	}

	/**
	 * @return the tracks
	 */
	public RealizarTransferenciaPEITrackDataRequestDTO getTracks() {
		return tracks;
	}

	/**
	 * @param tracks the tracks to set
	 */
	public void setTracks(RealizarTransferenciaPEITrackDataRequestDTO tracks) {
		this.tracks = tracks;
	}

	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * @return the codigoSeguridad
	 */
	public String getCodigoSeguridad() {
		return codigoSeguridad;
	}

	/**
	 * @param codigoSeguridad the codigoSeguridad to set
	 */
	public void setCodigoSeguridad(String codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}

	/**
	 * @return the documento
	 */
	public TipoDocumentoDTO getDocumento() {
		return documento;
	}

	/**
	 * @param documento the documento to set
	 */
	public void setDocumento(TipoDocumentoDTO documento) {
		this.documento = documento;
	}

	/**
	 * @return the idCanal
	 */
	public String getIdCanal() {
		return idCanal;
	}

	/**
	 * @param idCanal the idCanal to set
	 */
	public void setIdCanal(String idCanal) {
		this.idCanal = idCanal;
	}

	/**
	 * @return the idReferenciaTrx
	 */
	public String getIdReferenciaTrx() {
		return idReferenciaTrx;
	}

	/**
	 * @param idReferenciaTrx the idReferenciaTrx to set
	 */
	public void setIdReferenciaTrx(String idReferenciaTrx) {
		this.idReferenciaTrx = idReferenciaTrx;
	}

	/**
	 * @return the idReferenciaOperacion
	 */
	public String getIdReferenciaOperacion() {
		return idReferenciaOperacion;
	}

	/**
	 * @param idReferenciaOperacion the idReferenciaOperacion to set
	 */
	public void setIdReferenciaOperacion(String idReferenciaOperacion) {
		this.idReferenciaOperacion = idReferenciaOperacion;
	}

	/**
	 * @return the importe
	 */
	public Integer getImporte() {
		return importe;
	}

	/**
	 * @param importe the importe to set
	 */
	public void setImporte(Integer importe) {
		this.importe = importe;
	}

	/**
	 * @return the moneda
	 */
	public String getMoneda() {
		return moneda;
	}

	/**
	 * @param moneda the moneda to set
	 */
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	/**
	 * @return the concepto
	 */
	public String getConcepto() {
		return concepto;
	}

	/**
	 * @param concepto the concepto to set
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
		
}
