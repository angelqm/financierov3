package ar.com.redlink.sam.financieroservices.dto.request;

import java.io.Serializable;

public class TipoDocumentoDTO implements Serializable {

	/**
	 * 
	 * @author AQM
	 *
	 */
	private static final long serialVersionUID = 2719570625978139944L;
	private String tipo;
	private String numero;
	
	
	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}
	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}
	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
}
