/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  28/01/2015 - 14:44:21
 */

package ar.com.redlink.sam.financieroservices.dto.response;

/**
 * DTO para la respuesta de consulta de saldo.
 * 
 * @author aguerrea
 * 
 */
public class EstadoCuentaDTO extends FinancieroServiceBaseResponseDTO {

	private static final long serialVersionUID = 1056697109374744472L;

	private String anticipo;

	private String saldo;

	private String disponible;

	private int extraccionesDisp;

	private String ticket;

	/**
	 * @return the anticipo
	 */
	public String getAnticipo() {
		return anticipo;
	}

	/**
	 * @param anticipo
	 *            the anticipo to set
	 */
	public void setAnticipo(String anticipo) {
		this.anticipo = anticipo;
	}

	/**
	 * @return the saldo
	 */
	public String getSaldo() {
		return saldo;
	}

	/**
	 * @param saldo
	 *            the saldo to set
	 */
	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}

	/**
	 * @return the disponible
	 */
	public String getDisponible() {
		return disponible;
	}

	/**
	 * @param disponible
	 *            the disponible to set
	 */
	public void setDisponible(String disponible) {
		this.disponible = disponible;
	}

	/**
	 * @return the extraccionesDisp
	 */
	public int getExtraccionesDisp() {
		return extraccionesDisp;
	}

	/**
	 * @param extraccionesDisp
	 *            the extraccionesDisp to set
	 */
	public void setExtraccionesDisp(int extraccionesDisp) {
		this.extraccionesDisp = extraccionesDisp;
	}

	/**
	 * @return the ticket
	 */
	public String getTicket() {
		return ticket;
	}

	/**
	 * @param ticket
	 *            the ticket to set
	 */
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

}
