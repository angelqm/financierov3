package ar.com.redlink.sam.financieroservices.dto.request;

import java.io.Serializable;


/**
 * 
 * @author AQM
 *	Objeto que contiene las propiedades de cuenta recaudadora para PEI en PAGAR
 */
public class CuentaRecaudadoraDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4190649610827528577L;
	private String tipo;
	private String numero;
	private String fiid;
	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}
	/**
	 * @param tipo the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}
	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}
	/**
	 * @return the fiid
	 */
	public String getFiid() {
		return fiid;
	}
	/**
	 * @param fiid the fiid to set
	 */
	public void setFiid(String fiid) {
		this.fiid = fiid;
	}
	
}
