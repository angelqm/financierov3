/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  08/04/2015 - 22:59:33
 */

package ar.com.redlink.sam.financieroservices.dto.request.common;

import java.math.BigDecimal;

import ar.com.redlink.sam.financieroservices.dto.CuentaDTO;

/**
 * DTO para las entidades relacionadas a las cuentas e importes.
 * 
 * @author aguerrea
 * 
 */
public class FinancieroAccountDTO extends FinancieroCommonDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4223103683994109119L;

	private BigDecimal importe;

	private CuentaDTO cuenta;

	/**
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}

	/**
	 * @param importe
	 *            the importe to set
	 */
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	/**
	 * @return the cuenta
	 */
	public CuentaDTO getCuenta() {
		return cuenta;
	}

	/**
	 * @param cuenta
	 *            the cuenta to set
	 */
	public void setCuenta(CuentaDTO cuenta) {
		this.cuenta = cuenta;
	}
}
