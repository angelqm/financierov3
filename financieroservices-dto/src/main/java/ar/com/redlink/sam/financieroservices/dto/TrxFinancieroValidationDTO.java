/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  20/04/2015 - 16:03:53
 */

package ar.com.redlink.sam.financieroservices.dto;

/**
 * DTO para la validacion de existencia de un registro en TRX_FINANCIERO.
 * 
 * @author aguerrea
 * 
 */
/**
 * @author luchelld
 *
 */
public class TrxFinancieroValidationDTO {

	private String importe;
	private String terminalSamId;
	private String idRequerimientoOrig;
	private String tokenizado;
	private String prefijoOpId;
	private boolean importeParcial;

	/**
	 * Constructor default.
	 * 
	 * @param importe
	 * @param terminalSamId
	 * @param idRequerimientoOrig
	 * @param prefijoOpId
	 * @param tokenizado
	 */
	public TrxFinancieroValidationDTO(String importe, String terminalSamId,
			String idRequerimientoOrig, String prefijoOpId, String tokenizado) {
		this.importe = importe;
		this.terminalSamId = terminalSamId;
		this.idRequerimientoOrig = idRequerimientoOrig;
		this.tokenizado = tokenizado;
		this.prefijoOpId = prefijoOpId;
	}

	/**
	 * @return the importe
	 */
	public String getImporte() {
		return importe;
	}

	/**
	 * @param importe
	 *            the importe to set
	 */
	public void setImporte(String importe) {
		this.importe = importe;
	}

	/**
	 * @return the terminalSamId
	 */
	public String getTerminalSamId() {
		return terminalSamId;
	}

	/**
	 * @param terminalSamId
	 *            the terminalSamId to set
	 */
	public void setTerminalSamId(String terminalSamId) {
		this.terminalSamId = terminalSamId;
	}

	/**
	 * @return the idRequerimientoOrig
	 */
	public String getIdRequerimientoOrig() {
		return idRequerimientoOrig;
	}

	/**
	 * @param idRequerimientoOrig
	 *            the idRequerimientoOrig to set
	 */
	public void setIdRequerimientoOrig(String idRequerimientoOrig) {
		this.idRequerimientoOrig = idRequerimientoOrig;
	}

	/**
	 * @return the tokenizado
	 */
	public String getTokenizado() {
		return tokenizado;
	}

	/**
	 * @param tokenizado
	 *            the tokenizado to set
	 */
	public void setTokenizado(String tokenizado) {
		this.tokenizado = tokenizado;
	}

	/**
	 * @return the prefijoOpId
	 */
	public String getPrefijoOpId() {
		return prefijoOpId;
	}

	/**
	 * @param prefijoOpId
	 *            the prefijoOpId to set
	 */
	public void setPrefijoOpId(String prefijoOpId) {
		this.prefijoOpId = prefijoOpId;
	}

	/**
	 * @return the importeParcial
	 */
	public boolean isImporteParcial() {
		return importeParcial;
	}

	/**
	 * @param importeParcial the importeParcial to set
	 */
	public void setImporteParcial(boolean importeParcial) {
		this.importeParcial = importeParcial;
	}
	
}