package ar.com.redlink.sam.financieroservices.dto.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * DTO para la operacion de transferencia PEI.
 * 
 * @author EXT-IBANEZMA
 *
 */
public class RealizarTransferenciaPEITrackDataRequestDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String track1;

	private String track2;

	@JsonInclude(JsonInclude.Include.ALWAYS)
	private String track3;

	/**
	 * @return the track1
	 */
	public String getTrack1() {
		return track1;
	}

	/**
	 * @param track1
	 *            the track1 to set
	 */
	public void setTrack1(String track1) {
		this.track1 = track1;
	}

	/**
	 * @return the track2
	 */
	public String getTrack2() {
		return track2;
	}

	/**
	 * @param track2
	 *            the track2 to set
	 */
	public void setTrack2(String track2) {
		this.track2 = track2;
	}

	/**
	 * @return the track3
	 */
	public String getTrack3() {
		return track3;
	}

	/**
	 * @param track3 the track3 to set
	 */
	public void setTrack3(String track3) {
		this.track3 = track3;
	}

//	/**
//	 * @return the track3
//	 */
//	public String getTrack3() {
//		return track3;
//	}
//
//	/**
//	 * @param track3
//	 *            the track3 to set
//	 */
//	public void setTrack3(String track3) {
//		this.track3 = track3;
//	}

}
