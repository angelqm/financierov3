/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  11/03/2015 - 21:51:00
 */

package ar.com.redlink.sam.financieroservices.dto.request;

import ar.com.redlink.sam.financieroservices.dto.request.common.FinancieroAccountDTO;

/**
 * DTO para la operacion de deposito. Se define su representacion para
 * eventuales extensiones.
 * 
 * @author aguerrea
 * 
 */
public class RealizarDepositoRequestDTO extends FinancieroAccountDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3336374343517037504L;

}
