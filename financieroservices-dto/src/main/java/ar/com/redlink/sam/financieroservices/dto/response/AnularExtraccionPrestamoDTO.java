/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  28/01/2015 - 14:43:31
 */

package ar.com.redlink.sam.financieroservices.dto.response;


/**
 * DTO para la respuesta de la operacion de anulacion de extraccion de prestamo.
 * 
 * @author aguerrea
 * 
 */
public class AnularExtraccionPrestamoDTO extends
		FinancieroServiceBaseResponseDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1177774497177081069L;
	private String ticket;

	/**
	 * @return the ticket
	 */
	public String getTicket() {
		return ticket;
	}

	/**
	 * @param ticket
	 *            the ticket to set
	 */
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
}
