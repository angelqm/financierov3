/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  11/03/2015 - 21:55:30
 */

package ar.com.redlink.sam.financieroservices.dto.response;

import ar.com.redlink.sam.financieroservices.dto.response.common.OperacionCommonDTO;

/**
 * DTO para la respuesta de la operacion de deposito.
 * 
 * @author aguerrea
 * 
 */
public class RealizarDepositoDTO extends OperacionCommonDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3024318111134386150L;

}
