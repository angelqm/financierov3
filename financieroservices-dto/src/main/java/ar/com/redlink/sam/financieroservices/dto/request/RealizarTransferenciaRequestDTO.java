/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  11/03/2015 - 22:37:05
 */

package ar.com.redlink.sam.financieroservices.dto.request;

import ar.com.redlink.sam.financieroservices.dto.CuentaDTO;
import ar.com.redlink.sam.financieroservices.dto.request.common.FinancieroCommonDTO;

/**
 * 
 * DTO para la operacion de transferencia.
 * 
 * @author aguerrea
 * 
 */
public class RealizarTransferenciaRequestDTO extends FinancieroCommonDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -54812591173412402L;

	private double importe;

	private CuentaDTO cuentaOrigen;

	private CuentaDTO cuentaDestino;

	/**
	 * @return the importe
	 */
	public double getImporte() {
		return importe;
	}

	/**
	 * @param importe
	 *            the importe to set
	 */
	public void setImporte(double importe) {
		this.importe = importe;
	}

	/**
	 * @return the cuentaOrigen
	 */
	public CuentaDTO getCuentaOrigen() {
		return cuentaOrigen;
	}

	/**
	 * @param cuentaOrigen
	 *            the cuentaOrigen to set
	 */
	public void setCuentaOrigen(CuentaDTO cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}

	/**
	 * @return the cuentaDestino
	 */
	public CuentaDTO getCuentaDestino() {
		return cuentaDestino;
	}

	/**
	 * @param cuentaDestino
	 *            the cuentaDestino to set
	 */
	public void setCuentaDestino(CuentaDTO cuentaDestino) {
		this.cuentaDestino = cuentaDestino;
	}

}
