/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  28/01/2015 - 14:43:40
 */

package ar.com.redlink.sam.financieroservices.dto.request;

import ar.com.redlink.sam.financieroservices.dto.request.common.AnulacionCommonDTO;

/**
 * DTO para la operacion de anulacion de extraccion.
 * 
 * @author aguerrea
 * 
 */
public class AnularExtraccionRequestDTO extends AnulacionCommonDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4977528489109295066L;
	
}
