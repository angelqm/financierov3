package ar.com.redlink.sam.financieroservices.dto.response;

import java.io.Serializable;

/**
 * 
 * @author EXT-IBANEZMA
 *
 */
public class ConsultaOperacionPEIDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	
	private ConsultaOperacionResultadoPEIDTO resultado;

	/**
	 * @return the resultado
	 */
	public ConsultaOperacionResultadoPEIDTO getResultado() {
		return resultado;
	}

	/**
	 * @param resultado the resultado to set
	 */
	public void setResultado(ConsultaOperacionResultadoPEIDTO resultado) {
		this.resultado = resultado;
	}
	
	
	

}
