/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  30/03/2015 - 00:54:49
 */

package ar.com.redlink.sam.financieroservices.dto.request;

import ar.com.redlink.sam.financieroservices.dto.request.common.AnulacionCommonDTO;

/**
 * DTO para la solicitud de anulacion de extraccion en cuotas.
 * 
 * @author aguerrea
 * 
 */
public class AnularExtraccionCuotasRequestDTO extends AnulacionCommonDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4734026051314310036L;

	private int cuotas;

	/**
	 * @return the cuotas
	 */
	public int getCuotas() {
		return cuotas;
	}

	/**
	 * @param cuotas
	 *            the cuotas to set
	 */
	public void setCuotas(int cuotas) {
		this.cuotas = cuotas;
	}
}
