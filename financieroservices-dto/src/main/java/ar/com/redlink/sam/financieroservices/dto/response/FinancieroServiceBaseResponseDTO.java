/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  17/03/2015 - 10:46:31
 */

package ar.com.redlink.sam.financieroservices.dto.response;

import java.io.Serializable;

/**
 * DTO base para los atributos comunes de una respuesta.
 * 
 * @author aguerrea
 * 
 */
public class FinancieroServiceBaseResponseDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9159240649311284543L;

	private String secuenciaOn;

	private String fechaOn;
	/*AQM HORA_ON*/
	private String horaOn;	

	private String codRta;

	private String msg;

	// Esto se agrega para facilitar el manejo de errores de validacion en un
	// flujo donde se requiera el mensaje a pesar de los mismos.
	private String codError;

	private String descripcionError;

	/**
	 * @return the secuenciaOn
	 */
	public String getSecuenciaOn() {
		return secuenciaOn;
	}

	/**
	 * @param secuenciaOn
	 *            the secuenciaOn to set
	 */
	public void setSecuenciaOn(String secuenciaOn) {
		this.secuenciaOn = secuenciaOn;
	}

	/**
	 * @return the fechaOn
	 */
	public String getFechaOn() {
		return fechaOn;
	}

	/**
	 * @param fechaOn
	 *            the fechaOn to set
	 */
	public void setFechaOn(String fechaOn) {
		this.fechaOn = fechaOn;
	}

	/**
	 * @return the horaOn
	 */
	public String getHoraOn() {
		return horaOn;
	}

	/**
	 * @param horaOn the horaOn to set
	 */
	public void setHoraOn(String horaOn) {
		this.horaOn = horaOn;
	}

	/**
	 * @return the codRta
	 */
	public String getCodRta() {
		return codRta;
	}

	/**
	 * @param codRta
	 *            the codRta to set
	 */
	public void setCodRta(String codRta) {
		this.codRta = codRta;
	}

	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @param msg
	 *            the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * @return the codError
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 * @param codError the codError to set
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 * @return the descripcionError
	 */
	public String getDescripcionError() {
		return descripcionError;
	}

	/**
	 * @param descripcionError the descripcionError to set
	 */
	public void setDescripcionError(String descripcionError) {
		this.descripcionError = descripcionError;
	}

}
