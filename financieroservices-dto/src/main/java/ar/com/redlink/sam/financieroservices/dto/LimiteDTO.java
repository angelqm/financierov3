/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  28/01/2015 - 23:12:43
 */

package ar.com.redlink.sam.financieroservices.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * DTO para un limite.
 * 
 * @author aguerrea
 * 
 */
public class LimiteDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1389404750630781416L;

	private String nombreLimite;

	private String descripcionLimite;

	private String valorLimite;

	private int precisionValorLimite;

	/**
	 * Constructor default.
	 * 
	 * @param nombre
	 * @param descripcion
	 * @param valor
	 */
	public LimiteDTO(String nombre, String descripcion, BigDecimal valor) {
		this.nombreLimite = nombre;
		this.descripcionLimite = descripcion;
		this.valorLimite = valor.toPlainString();
	}

	/**
	 * @return the nombreLimite
	 */
	public String getNombreLimite() {
		return nombreLimite;
	}

	/**
	 * @param nombreLimite
	 *            the nombreLimite to set
	 */
	public void setNombreLimite(String nombreLimite) {
		this.nombreLimite = nombreLimite;
	}

	/**
	 * @return the descripcionLimite
	 */
	public String getDescripcionLimite() {
		return descripcionLimite;
	}

	/**
	 * @param descripcionLimite
	 *            the descripcionLimite to set
	 */
	public void setDescripcionLimite(String descripcionLimite) {
		this.descripcionLimite = descripcionLimite;
	}

	/**
	 * @return the valorLimite
	 */
	public String getValorLimite() {
		return valorLimite;
	}

	/**
	 * @param valorLimite
	 *            the valorLimite to set
	 */
	public void setValorLimite(String valorLimite) {
		this.valorLimite = valorLimite;
	}

	/**
	 * @return the precisionValorLimite
	 */
	public int getPrecisionValorLimite() {
		return precisionValorLimite;
	}

	/**
	 * @param precisionValorLimite
	 *            the precisionValorLimite to set
	 */
	public void setPrecisionValorLimite(int precisionValorLimite) {
		this.precisionValorLimite = precisionValorLimite;
	}

}
