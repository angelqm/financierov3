/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  28/01/2015 - 14:42:44
 */

package ar.com.redlink.sam.financieroservices.dto.request;

import ar.com.redlink.sam.financieroservices.dto.request.common.FinancieroCommonDTO;

/**
 * DTO para la validacion de tarjeta. Se define su representacion para
 * eventuales extensiones.
 * 
 * @author aguerrea
 * 
 */
public class ValidarTarjetaRequestDTO extends FinancieroCommonDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4253903261570378602L;

}
