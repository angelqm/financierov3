package ar.com.redlink.sam.financieroservices.dto.response;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author EXT-IBANEZMA
 *
 */
public class ConsultaTipoDocumentoPEIDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	
	private List<TipoDocumentoPEIDTO> documentoTipos;

	/**
	 * @return the documentoTipos
	 */
	public List<TipoDocumentoPEIDTO> getDocumentoTipos() {
		return documentoTipos;
	}

	/**
	 * @param documentoTipos the documentoTipos to set
	 */
	public void setDocumentoTipos(List<TipoDocumentoPEIDTO> documentoTipos) {
		this.documentoTipos = documentoTipos;
	}

}
