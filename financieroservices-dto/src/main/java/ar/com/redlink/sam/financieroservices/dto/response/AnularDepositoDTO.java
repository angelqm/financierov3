/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  11/03/2015 - 22:16:01
 */

package ar.com.redlink.sam.financieroservices.dto.response;

/**
 * DTO para la respuesta de anulacion de deposito.
 * 
 * @author aguerrea
 * 
 */
public class AnularDepositoDTO extends FinancieroServiceBaseResponseDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2405533293220796999L;
	private String ticket;

	/**
	 * @return the ticket
	 */
	public String getTicket() {
		return ticket;
	}

	/**
	 * @param ticket
	 *            the ticket to set
	 */
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
}
