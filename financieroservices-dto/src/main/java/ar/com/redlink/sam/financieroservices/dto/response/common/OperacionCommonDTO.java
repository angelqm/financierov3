/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  11/04/2015 - 17:00:56
 */

package ar.com.redlink.sam.financieroservices.dto.response.common;

import ar.com.redlink.sam.financieroservices.dto.response.FinancieroServiceBaseResponseDTO;

/**
 * Componente que tiene aquellos atributos comunes a todas las operaciones de
 * extraccion (Debito, Cuotas, etcetera).
 * 
 * @author aguerrea
 * 
 */
public class OperacionCommonDTO extends FinancieroServiceBaseResponseDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1346535922084468020L;

	private String anticipo;

	private String saldo;

	private String disponible;

	private int extraccionesDisp;

	private String TNA;

	private String TEM;

	private int cantidadCuotas;

	private String disponibleEfCuotas;

	private String ticket;

	/**
	 * @return the anticipo
	 */
	public String getAnticipo() {
		return anticipo;
	}

	/**
	 * @param anticipo
	 *            the anticipo to set
	 */
	public void setAnticipo(String anticipo) {
		this.anticipo = anticipo;
	}

	/**
	 * @return the saldo
	 */
	public String getSaldo() {
		return saldo;
	}

	/**
	 * @param saldo
	 *            the saldo to set
	 */
	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}

	/**
	 * @return the disponible
	 */
	public String getDisponible() {
		return disponible;
	}

	/**
	 * @param disponible
	 *            the disponible to set
	 */
	public void setDisponible(String disponible) {
		this.disponible = disponible;
	}

	/**
	 * @return the extraccionesDisp
	 */
	public int getExtraccionesDisp() {
		return extraccionesDisp;
	}

	/**
	 * @param extraccionesDisp
	 *            the extraccionesDisp to set
	 */
	public void setExtraccionesDisp(int extraccionesDisp) {
		this.extraccionesDisp = extraccionesDisp;
	}

	/**
	 * @return the tNA
	 */
	public String getTNA() {
		return TNA;
	}

	/**
	 * @param tNA
	 *            the tNA to set
	 */
	public void setTNA(String tNA) {
		TNA = tNA;
	}

	/**
	 * @return the tEM
	 */
	public String getTEM() {
		return TEM;
	}

	/**
	 * @param tEM
	 *            the tEM to set
	 */
	public void setTEM(String tEM) {
		TEM = tEM;
	}

	/**
	 * @return the cantidadCuotas
	 */
	public int getCantidadCuotas() {
		return cantidadCuotas;
	}

	/**
	 * @param cantidadCuotas
	 *            the cantidadCuotas to set
	 */
	public void setCantidadCuotas(int cantidadCuotas) {
		this.cantidadCuotas = cantidadCuotas;
	}

	/**
	 * @return the disponibleEfCuotas
	 */
	public String getDisponibleEfCuotas() {
		return disponibleEfCuotas;
	}

	/**
	 * @param disponibleEfCuotas
	 *            the disponibleEfCuotas to set
	 */
	public void setDisponibleEfCuotas(String disponibleEfCuotas) {
		this.disponibleEfCuotas = disponibleEfCuotas;
	}

	/**
	 * @return the ticket
	 */
	public String getTicket() {
		return ticket;
	}

	/**
	 * @param ticket
	 *            the ticket to set
	 */
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

}
