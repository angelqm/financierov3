/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  26/03/2015 - 23:38:57
 */

package ar.com.redlink.sam.financieroservices.dto.validation;

import java.util.HashMap;
import java.util.Map;

/**
 * Composicion de todos los parametros que se requiere validar al ingresar
 * cualquier request. Asi mismo, tambien recolecta datos necesarios para el
 * flujo de la operacion.
 * 
 * @author aguerrea
 * 
 */
public class ValidationDTO {

	public static final String TERM_TYPE = "TermType";

	public static final String RED_ID = "RedId";

	public static final String ENTIDAD_COD = "EntidadCod";

	public static final String TERMINAL_COD = "TerminalCod";

	public static final String SUCURSAL_COD = "SucursalCod";

	public static final String IDENT_TERMINAL = "IdentTerminal";

	public static final String IDENT_DISPOSITIVO = "IdentDispositivo";

	public static final String SUCURSAL_ID = "SucursalId";

	public static final String DISPOSITIVO_POS_ID = "DispositivoPosId";

	public static final String SERVIDOR_ID = "ServidorId";

	public static final String ENTIDAD_ID = "EntidadId";

	public static final String TERMINAL_SAM_ID = "TerminalSamId";

	public static final String AGENTE_ID = "AgenteId";

	public static final String TERMINAL_B24 = "TerminalB24";

	public static final String TARJETA = "Tarjeta";

	public static final String PREFIJO_ENTIDAD_ID = "PrefijoEntidadId";

	public static final String PERFIL_SAM_ID = "PerfilSamId";

	public static final String PERFIL_FINANCIERO_ID = "PerfilFinancieroId";

	public static final String PERFIL_PREFIJO_ID = "PerfilPrefijoId";

	public static final String PERFIL_PREFIJO_GRUPO_ID = "PerfilPrefijoGrupoId";

	public static final String OPERACION_DTOS = "OperacionDTOs";

	public static final String PREFIJO_OP_ID = "PrefijoOpId";
	
	public static final String PAGO_PREFIJO_OP_ID = "PagoPrefijoOpId";

	public static final String TOKENIZADO = "Tokenizado";

	public static final String TIPO_ENCRIPCION_B24 = "TipoEncripcionB24";

	public static final String ID_REQ = "IdRequerimiento";
	
	public static final String ID_REQ_BAJA = "IdRequerimiento";

	public static final String REQUIERE_PIN = "RequierePin";

	public static final String REQUIERE_AUT = "RequiereAut";

	public static final String REQUIERE_SEG = "RequiereSeg";

	public static final String REQUIERE_SFX = "RequiereSFX";

	public static final String REQUIERE_DNI = "RequiereDNI";

	public static final String IMPORTE = "Importe";

	public static final String REVERSA = "Reversa";

	public static final String SEQ_NUM = "SeqNum";

	public static final String ID_REQ_ORIG = "IdReqOrig";

	public static final String TIMESTAMP = "Timestamp";

	public static final String IP_CLIENTE = "IpCliente";
	
	public static final String TIPO_TERMINAL_B24 = "tipoTerminalB24";
	
	public static final String TRXT_FINANCIERO_ID = "trxFinancieroId";

	private Map<String, String> inputValues;

	private Map<String, Object> outputValues;
	
	public static final String CODIGO_COMERCIO_PEI = "CodigoComercioPEI";
	
	public static final String CODIGO_TERMINAL_PEI = "codigoTerminalPEI";
	
	public static final String PRODUCTO_ID = "productoId";
	
	public static final String CODIGO_SUCURSAL_PEI = "codigoSucursalPEI";
	
	public static final String FECHA_ON = "fechaON";
	
	public static final String HORA_ON = "HoraON";
	

	/**
	 * Constructor default.
	 */
	public ValidationDTO() {
		this.inputValues = new HashMap<String, String>();
		this.outputValues = new HashMap<String, Object>();
	}

	/**
	 * @return the outputValues
	 */
	public Map<String, Object> getOutputValues() {
		return outputValues;
	}

	/**
	 * @param outputValues
	 *            the outputValues to set
	 */
	public void setOutputValues(Map<String, Object> outputValues) {
		this.outputValues = outputValues;
	}

	/**
	 * @return the inputValues
	 */
	public Map<String, String> getInputValues() {
		return inputValues;
	}

	/**
	 * @param inputValues
	 *            the inputValues to set
	 */
	public void setInputValues(Map<String, String> inputValues) {
		this.inputValues = inputValues;
	}

	/**
	 * Agrega un campo para validar y su valor.
	 * 
	 * @param key
	 * @param value
	 */
	public void addInputValue(String key, String value) {
		this.inputValues.put(key, value);
	}

	/**
	 * Devuelve el valor de input asociado a la key provista.
	 * 
	 * @param key
	 * @return el valor asociado.
	 */
	public String getInputValue(String key) {
		return this.inputValues.get(key);
	}

	/**
	 * Agrega un dato de utilidad para la posteridad del flujo.
	 * 
	 * @param key
	 * @param value
	 */
	public void addOutputValue(String key, Object value) {
		this.outputValues.put(key, value);
	}

	/**
	 * Devuelve el valor de output asociado a la key ingresada.
	 * 
	 * @param key
	 * @return el valor asociado.
	 */
	public Object getOutputValue(String key) {
		return this.outputValues.get(key);
	}
}
