package ar.com.redlink.sam.financieroservices.dto.request;

import java.io.Serializable;

/**
 * 
 * @author EXT-ibanezma
 *
 */
public class OrigenPEIRequestDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String idCanal;

	/**
	 * @return the idCanal
	 */
	public String getIdCanal() {
		return idCanal;
	}

	/**
	 * @param idCanal the idCanal to set
	 */
	public void setIdCanal(String idCanal) {
		this.idCanal = idCanal;
	}

}
