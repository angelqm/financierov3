/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  16/03/2015 - 23:37:18
 */

package ar.com.redlink.sam.financieroservices.dto.request.common;

import java.io.Serializable;

/**
 * DTO base para todas las entidades del modulo financiero.
 * 
 * @author aguerrea
 * 
 */
public class FinancieroServiceBaseRequestDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5140355509550128535L;
	protected String identTerminal;

	protected String identDispositivo;

	protected String idEntidad;

	protected String sucursal;

	protected String terminalB24;

	protected String autorizacion;

	// Los siguientes parametros son requeridos para la persistencia en
	// TRX_FINANCIERO.
	protected String agenteId;

	protected String servidorId;

	protected String terminalSamId;

	protected String prefijoOpId;

	protected String tokenizado;

	protected String estadoTrxId;

	private String redId;

	private String secuenciaOn;

	private String secuenciaOnRta;

	private String idRequerimiento;

	private String idRequerimientoOrig;

	private String fechaOn;
	/*AQM HORA_ON*/
	private String horaOn;	
	
	private String tipoTerminalB24;
	
	private String productoId;

	/**
	 * @return the idEntidad
	 */
	public String getIdEntidad() {
		return idEntidad;
	}

	/**
	 * @param idEntidad
	 *            the idEntidad to set
	 */
	public void setIdEntidad(String idEntidad) {
		this.idEntidad = idEntidad;
	}

	/**
	 * @return the sucursal
	 */
	public String getSucursal() {
		return sucursal;
	}

	/**
	 * @param sucursal
	 *            the sucursal to set
	 */
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	/**
	 * @return the identTerminal
	 */
	public String getIdentTerminal() {
		return identTerminal;
	}

	/**
	 * @param identTerminal
	 *            the identTerminal to set
	 */
	public void setIdentTerminal(String identTerminal) {
		this.identTerminal = identTerminal;
	}

	/**
	 * @return the identDispositivo
	 */
	public String getIdentDispositivo() {
		return identDispositivo;
	}

	/**
	 * @param identDispositivo
	 *            the identDispositivo to set
	 */
	public void setIdentDispositivo(String identDispositivo) {
		this.identDispositivo = identDispositivo;
	}

	/**
	 * @return the agenteId
	 */
	public String getAgenteId() {
		return agenteId;
	}

	/**
	 * @param agenteId
	 *            the agenteId to set
	 */
	public void setAgenteId(String agenteId) {
		this.agenteId = agenteId;
	}

	/**
	 * @return the servidorId
	 */
	public String getServidorId() {
		return servidorId;
	}

	/**
	 * @param servidorId
	 *            the servidorId to set
	 */
	public void setServidorId(String servidorId) {
		this.servidorId = servidorId;
	}

	/**
	 * @return the terminalSamId
	 */
	public String getTerminalSamId() {
		return terminalSamId;
	}

	/**
	 * @param terminalSamId
	 *            the terminalSamId to set
	 */
	public void setTerminalSamId(String terminalSamId) {
		this.terminalSamId = terminalSamId;
	}

	/**
	 * @return the prefijoOpId
	 */
	public String getPrefijoOpId() {
		return prefijoOpId;
	}

	/**
	 * @param prefijoOpId
	 *            the prefijoOpId to set
	 */
	public void setPrefijoOpId(String prefijoOpId) {
		this.prefijoOpId = prefijoOpId;
	}

	/**
	 * @return the tokenizado
	 */
	public String getTokenizado() {
		return tokenizado;
	}

	/**
	 * @param tokenizado
	 *            the tokenizado to set
	 */
	public void setTokenizado(String tokenizado) {
		this.tokenizado = tokenizado;
	}

	/**
	 * @return the estadoTrxId
	 */
	public String getEstadoTrxId() {
		return estadoTrxId;
	}

	/**
	 * @param estadoTrxId
	 *            the estadoTrxId to set
	 */
	public void setEstadoTrxId(String estadoTrxId) {
		this.estadoTrxId = estadoTrxId;
	}

	/**
	 * @return the redId
	 */
	public String getRedId() {
		return redId;
	}

	/**
	 * @param redId
	 *            the redId to set
	 */
	public void setRedId(String redId) {
		this.redId = redId;
	}

	/**
	 * @return the secuenciaOn
	 */
	public String getSecuenciaOn() {
		return secuenciaOn;
	}

	/**
	 * @param secuenciaOn
	 *            the secuenciaOn to set
	 */
	public void setSecuenciaOn(String secuenciaOn) {
		this.secuenciaOn = secuenciaOn;
	}

	/**
	 * @return the secuenciaOnRta
	 */
	public String getSecuenciaOnRta() {
		return secuenciaOnRta;
	}

	/**
	 * @param secuenciaOnRta
	 *            the secuenciaOnRta to set
	 */
	public void setSecuenciaOnRta(String secuenciaOnRta) {
		this.secuenciaOnRta = secuenciaOnRta;
	}

	/**
	 * @return the idRequerimiento
	 */
	public String getIdRequerimiento() {
		return idRequerimiento;
	}

	/**
	 * @param idRequerimiento
	 *            the idRequerimiento to set
	 */
	public void setIdRequerimiento(String idRequerimiento) {
		this.idRequerimiento = idRequerimiento;
	}

	/**
	 * @return the idRequerimientoOriginal
	 */
	public String getIdRequerimientoOrig() {
		return idRequerimientoOrig;
	}

	/**
	 * @param idRequerimientoOriginal
	 *            the idRequerimientoOriginal to set
	 */
	public void setIdRequerimientoOrig(String idRequerimientoOrig) {
		this.idRequerimientoOrig = idRequerimientoOrig;
	}

	/**
	 * @return the terminalB24
	 */
	public String getTerminalB24() {
		return terminalB24;
	}

	/**
	 * @param terminalB24
	 *            the terminalB24 to set
	 */
	public void setTerminalB24(String terminalB24) {
		this.terminalB24 = terminalB24;
	}

	/**
	 * @return the fechaOn
	 */
	public String getFechaOn() {
		return fechaOn;
	}

	/**
	 * @param fechaOn
	 *            the fechaOn to set
	 */
	public void setFechaOn(String fechaOn) {
		this.fechaOn = fechaOn;
	}

	
	/**
	 * @return the horaOn
	 */
	public String getHoraOn() {
		return horaOn;
	}

	/**
	 * @param horaOn the horaOn to set
	 */
	public void setHoraOn(String horaOn) {
		this.horaOn = horaOn;
	}

	/**
	 * @return the autorizacion
	 */
	public String getAutorizacion() {
		return autorizacion;
	}

	/**
	 * @param autorizacion
	 *            the autorizacion to set
	 */
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getTipoTerminalB24() {
		return tipoTerminalB24;
	}

	/**
	 * 
	 * @param tipoTerminalB24
	 */
	public void setTipoTerminalB24(String tipoTerminalB24) {
		this.tipoTerminalB24 = tipoTerminalB24;
	}

	/**
	 * 
	 * @return
	 */	
	public String getProductoId() {
		return productoId;
	}

	/**
	 * 
	 * @param productoId
	 */	
	public void setProductoId(String productoId) {
		this.productoId = productoId;
	}
	
}
