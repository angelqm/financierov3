package ar.com.redlink.sam.financieroservices.dto.response;

import java.io.Serializable;

/**
 * DTO para la respuesta de la operacion de transferencia PEI.
 * 
 * @author AQM
 *
 */
public class RealizarTransferenciaPEIPagarDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private RealizarTransferenciaResultadoPEIPagarDTO resultado;

	/**
	 * @return the resultado
	 */
	public RealizarTransferenciaResultadoPEIPagarDTO getResultado() {
		return resultado;
	}

	/**
	 * @param resultado the resultado to set
	 */
	public void setResultado(RealizarTransferenciaResultadoPEIPagarDTO resultado) {
		this.resultado = resultado;
	}
		
}
