/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  28/01/2015 - 14:42:53
 */

package ar.com.redlink.sam.financieroservices.dto.response;

import java.util.List;

import ar.com.redlink.sam.financieroservices.dto.CuentaDTO;
import ar.com.redlink.sam.financieroservices.dto.OperacionDTO;

/**
 * DTO para el resultado de la validacion de una tarjeta.
 * 
 * @author aguerrea
 * 
 */
public class ValidacionTarjetaDTO extends FinancieroServiceBaseResponseDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 974893050969456384L;

	private List<OperacionDTO> operaciones;

	private List<CuentaDTO> cuentas;

	private String tokenizado;
	
	private String estadoTarjeta;
	
	private String entidadEmisora;

	/**
	 * @return the operaciones
	 */
	public List<OperacionDTO> getOperaciones() {
		return operaciones;
	}

	/**
	 * @param operaciones
	 *            the operaciones to set
	 */
	public void setOperaciones(List<OperacionDTO> operaciones) {
		this.operaciones = operaciones;
	}

	/**
	 * @return the cuentas
	 */
	public List<CuentaDTO> getCuentas() {
		return cuentas;
	}

	/**
	 * @param cuentas
	 *            the cuentas to set
	 */
	public void setCuentas(List<CuentaDTO> cuentas) {
		this.cuentas = cuentas;
	}

	/**
	 * @return the tokenizado
	 */
	public String getTokenizado() {
		return tokenizado;
	}

	/**
	 * @param tokenizado
	 *            the tokenizado to set
	 */
	public void setTokenizado(String tokenizado) {
		this.tokenizado = tokenizado;
	}

	/**
	 * @return the estadoTarjeta
	 */
	public String getEstadoTarjeta() {
		return estadoTarjeta;
	}

	/**
	 * @param estadoTarjeta the estadoTarjeta to set
	 */
	public void setEstadoTarjeta(String estadoTarjeta) {
		this.estadoTarjeta = estadoTarjeta;
	}

	/**
	 * @return the entidadEmisora
	 */
	public String getEntidadEmisora() {
		return entidadEmisora;
	}

	/**
	 * @param entidadEmisora the entidadEmisora to set
	 */
	public void setEntidadEmisora(String entidadEmisora) {
		this.entidadEmisora = entidadEmisora;
	}

}
