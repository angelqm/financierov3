/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  10/03/2015 - 23:12:20
 */

package ar.com.redlink.sam.financieroservices.dto.request;

import ar.com.redlink.sam.financieroservices.dto.request.common.FinancieroAccountDTO;

/**
 * DTO para la operacion de extraccion para debito. Se define su representacion
 * para eventuales extensiones.
 * 
 * @author aguerrea
 * 
 */
public class RealizarDebitoRequestDTO extends FinancieroAccountDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7914631751817837985L;
}
