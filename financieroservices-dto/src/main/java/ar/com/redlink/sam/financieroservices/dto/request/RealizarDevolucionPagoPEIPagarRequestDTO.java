package ar.com.redlink.sam.financieroservices.dto.request;

import java.io.Serializable;

/**
 * DTO para la operacion de devolucion PEI desde PAGAR .
 * 
 * @author AQM
 *
 */
public class RealizarDevolucionPagoPEIPagarRequestDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private RealizarDevolucionPagoDevolucionPEIPagarRequestDTO devolucion;

	/**
	 * @return the devolucion
	 */
	public RealizarDevolucionPagoDevolucionPEIPagarRequestDTO getDevolucion() {
		return devolucion;
	}

	/**
	 * @param devolucion
	 *            the devolucion to set
	 */
	public void setDevolucion(RealizarDevolucionPagoDevolucionPEIPagarRequestDTO devolucion) {
		this.devolucion = devolucion;
	}

}
