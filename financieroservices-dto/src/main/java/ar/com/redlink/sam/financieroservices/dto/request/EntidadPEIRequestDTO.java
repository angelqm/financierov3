package ar.com.redlink.sam.financieroservices.dto.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

public class EntidadPEIRequestDTO implements Serializable {

	/**
	 * 
	 * @author AQM
	 *
	 */
	private static final long serialVersionUID = 2719570625978139944L;

	private String fiidEntidad;
	
	@JsonInclude(JsonInclude.Include.ALWAYS)
	private String codComercio;
	/**
	 * @return the fiidEntidad
	 */
	public String getFiidEntidad() {
		return fiidEntidad;
	}
	/**
	 * @param fiidEntidad the fiidEntidad to set
	 */
	public void setFiidEntidad(String fiidEntidad) {
		this.fiidEntidad = fiidEntidad;
	}
	/**
	 * @return the codComercio
	 */
	public String getCodComercio() {
		return codComercio;
	}
	/**
	 * @param codComercio the codComercio to set
	 */
	public void setCodComercio(String codComercio) {
		this.codComercio = codComercio;
	}
	
}
