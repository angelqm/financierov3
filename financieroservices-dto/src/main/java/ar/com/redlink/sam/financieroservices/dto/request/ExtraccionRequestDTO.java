/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  28/01/2015 - 14:43:12
 */

package ar.com.redlink.sam.financieroservices.dto.request;

import ar.com.redlink.sam.financieroservices.dto.request.common.FinancieroAccountDTO;

/**
 * DTO para una extraccion.
 * 
 * @author aguerrea
 * 
 */
public class ExtraccionRequestDTO extends FinancieroAccountDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6287011347410133128L;

	private String tipoExtraccion;

	private String cuotas;

	/**
	 * @return the tipoExtraccion
	 */
	public String getTipoExtraccion() {
		return tipoExtraccion;
	}

	/**
	 * @param tipoExtraccion
	 *            the tipoExtraccion to set
	 */
	public void setTipoExtraccion(String tipoExtraccion) {
		this.tipoExtraccion = tipoExtraccion;
	}

	/**
	 * @return the cuotas
	 */
	public String getCuotas() {
		return cuotas;
	}

	/**
	 * @param cuotas the cuotas to set
	 */
	public void setCuotas(String cuotas) {
		this.cuotas = cuotas;
	}
}
