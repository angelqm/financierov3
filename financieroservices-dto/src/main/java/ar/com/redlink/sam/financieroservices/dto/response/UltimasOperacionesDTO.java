/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  28/01/2015 - 14:45:07
 */

package ar.com.redlink.sam.financieroservices.dto.response;

import java.util.List;

import ar.com.redlink.sam.financieroservices.dto.RequerimientoDTO;

/**
 * DTO para las ultimas operaciones realizadas sobre un dispositivo.
 * 
 * @author aguerrea
 * 
 */
public class UltimasOperacionesDTO extends FinancieroServiceBaseResponseDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -491124469053136751L;

	private List<RequerimientoDTO> requerimientos;

	/**
	 * @return the requerimientos
	 */
	public List<RequerimientoDTO> getRequerimientos() {
		return requerimientos;
	}

	/**
	 * @param requerimientos
	 *            the requerimientos to set
	 */
	public void setRequerimientos(List<RequerimientoDTO> requerimientos) {
		this.requerimientos = requerimientos;
	}
}
