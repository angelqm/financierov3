/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  19/03/2015 - 23:21:17
 */

package ar.com.redlink.sam.financieroservices.dto.response;

import ar.com.redlink.sam.financieroservices.dto.response.common.OperacionCommonDTO;

/**
 * DTO para la respuesta de Extraccion Prestamo.
 * 
 * @author aguerrea
 * 
 */
public class ExtraccionPrestamoDTO extends OperacionCommonDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 638019921997444566L;

}
