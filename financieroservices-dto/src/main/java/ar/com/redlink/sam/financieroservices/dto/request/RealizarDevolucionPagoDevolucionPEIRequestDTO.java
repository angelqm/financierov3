package ar.com.redlink.sam.financieroservices.dto.request;

import java.io.Serializable;

/**
 * 
 * @author EXT-ibanezma
 *
 */
public class RealizarDevolucionPagoDevolucionPEIRequestDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String idReferenciaTrxComercio;
	private String idReferenciaOperacionComercio;
	private String moneda;
	private String idTerminal;
	private String idCanal;
	private String posEntryMode;
	private RealizarTransferenciaPEITrackDataRequestDTO tracks;
	private String numero;
	private String titularDocumento;
	private String codigoSeguridad;
	private String idPago;
	private CuentaDestinoDTO cuentaDestino;

	/**
	 * @return the idReferenciaTrxComercio
	 */
	public String getIdReferenciaTrxComercio() {
		return idReferenciaTrxComercio;
	}

	/**
	 * @param idReferenciaTrxComercio
	 *            the idReferenciaTrxComercio to set
	 */
	public void setIdReferenciaTrxComercio(String idReferenciaTrxComercio) {
		this.idReferenciaTrxComercio = idReferenciaTrxComercio;
	}

	/**
	 * @return the idReferenciaOperacionComercio
	 */
	public String getIdReferenciaOperacionComercio() {
		return idReferenciaOperacionComercio;
	}

	/**
	 * @param idReferenciaOperacionComercio
	 *            the idReferenciaOperacionComercio to set
	 */
	public void setIdReferenciaOperacionComercio(String idReferenciaOperacionComercio) {
		this.idReferenciaOperacionComercio = idReferenciaOperacionComercio;
	}

	/**
	 * @return the moneda
	 */
	public String getMoneda() {
		return moneda;
	}

	/**
	 * @param moneda
	 *            the moneda to set
	 */
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	/**
	 * @return the idTerminal
	 */
	public String getIdTerminal() {
		return idTerminal;
	}

	/**
	 * @param idTerminal
	 *            the idTerminal to set
	 */
	public void setIdTerminal(String idTerminal) {
		this.idTerminal = idTerminal;
	}

	/**
	 * @return the idCanal
	 */
	public String getIdCanal() {
		return idCanal;
	}

	/**
	 * @param idCanal
	 *            the idCanal to set
	 */
	public void setIdCanal(String idCanal) {
		this.idCanal = idCanal;
	}

	/**
	 * @return the posEntryMode
	 */
	public String getPosEntryMode() {
		return posEntryMode;
	}

	/**
	 * @param posEntryMode
	 *            the posEntryMode to set
	 */
	public void setPosEntryMode(String posEntryMode) {
		this.posEntryMode = posEntryMode;
	}

	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * @param numero
	 *            the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * @return the titularDocumento
	 */
	public String getTitularDocumento() {
		return titularDocumento;
	}

	/**
	 * @param titularDocumento
	 *            the titularDocumento to set
	 */
	public void setTitularDocumento(String titularDocumento) {
		this.titularDocumento = titularDocumento;
	}

	/**
	 * @return the codigoSeguridad
	 */
	public String getCodigoSeguridad() {
		return codigoSeguridad;
	}

	/**
	 * @param codigoSeguridad
	 *            the codigoSeguridad to set
	 */
	public void setCodigoSeguridad(String codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}

	/**
	 * @return the idPago
	 */
	public String getIdPago() {
		return idPago;
	}

	/**
	 * @param idPago
	 *            the idPago to set
	 */
	public void setIdPago(String idPago) {
		this.idPago = idPago;
	}

	/**
	 * @return the tracks
	 */
	public RealizarTransferenciaPEITrackDataRequestDTO getTracks() {
		return tracks;
	}

	/**
	 * @param tracks
	 *            the tracks to set
	 */
	public void setTracks(RealizarTransferenciaPEITrackDataRequestDTO tracks) {
		this.tracks = tracks;
	}

	/**
	 * @return the cuentaDestino
	 */
	public CuentaDestinoDTO getCuentaDestino() {
		return cuentaDestino;
	}

	/**
	 * @param cuentaDestino the cuentaDestino to set
	 */
	public void setCuentaDestino(CuentaDestinoDTO cuentaDestino) {
		this.cuentaDestino = cuentaDestino;
	}


}
