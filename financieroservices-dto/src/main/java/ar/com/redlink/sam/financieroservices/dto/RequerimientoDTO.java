/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  28/01/2015 - 23:23:55
 */

package ar.com.redlink.sam.financieroservices.dto;

import ar.com.redlink.sam.financieroservices.dto.response.EstadoOperacionDTO;

/**
 * DTO para un requerimiento.
 * 
 * @author aguerrea
 * 
 */
public class RequerimientoDTO extends EstadoOperacionDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3497163914163946952L;

	private String idRequerimiento;

	/**
	 * @return the idRequerimiento
	 */
	public String getIdRequerimiento() {
		return idRequerimiento;
	}

	/**
	 * @param idRequerimiento
	 *            the idRequerimiento to set
	 */
	public void setIdRequerimiento(String idRequerimiento) {
		this.idRequerimiento = idRequerimiento;
	}
}
