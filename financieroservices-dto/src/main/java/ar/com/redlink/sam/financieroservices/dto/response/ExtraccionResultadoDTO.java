/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  28/01/2015 - 14:43:21
 */

package ar.com.redlink.sam.financieroservices.dto.response;

import ar.com.redlink.sam.financieroservices.dto.response.common.OperacionCommonDTO;

/**
 * DTO para el resultado de una extraccion.
 * 
 * @author aguerrea
 * 
 */
public class ExtraccionResultadoDTO extends OperacionCommonDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2333205715585736740L;

}
