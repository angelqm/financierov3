package ar.com.redlink.sam.financieroservices.dto.response;

import java.io.Serializable;

/**
 * 
 * @author EXT-IBANEZMA
 *
 */
public class RealizarTransferenciaResultadoPEIDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String secuenciaOn;
	private String idoperacion;
	private String tipooperacion;
	private String estado;
	private String fecha;
	private String numeroReferenciaBancaria;
	/**
	 * @return the secuenciaOn
	 */
	public String getSecuenciaOn() {
		return secuenciaOn;
	}
	/**
	 * @param secuenciaOn the secuenciaOn to set
	 */
	public void setSecuenciaOn(String secuenciaOn) {
		this.secuenciaOn = secuenciaOn;
	}
	/**
	 * @return the idoperacion
	 */
	public String getIdoperacion() {
		return idoperacion;
	}
	/**
	 * @param idoperacion the idoperacion to set
	 */
	public void setIdoperacion(String idoperacion) {
		this.idoperacion = idoperacion;
	}
	/**
	 * @return the tipooperacion
	 */
	public String getTipooperacion() {
		return tipooperacion;
	}
	/**
	 * @param tipooperacion the tipooperacion to set
	 */
	public void setTipooperacion(String tipooperacion) {
		this.tipooperacion = tipooperacion;
	}
	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}
	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	/**
	 * @return the numeroReferenciaBancaria
	 */
	public String getNumeroReferenciaBancaria() {
		return numeroReferenciaBancaria;
	}
	/**
	 * @param numeroReferenciaBancaria the numeroReferenciaBancaria to set
	 */
	public void setNumeroReferenciaBancaria(String numeroReferenciaBancaria) {
		this.numeroReferenciaBancaria = numeroReferenciaBancaria;
	}

	
}
