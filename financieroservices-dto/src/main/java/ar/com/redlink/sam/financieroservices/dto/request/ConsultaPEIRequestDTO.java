package ar.com.redlink.sam.financieroservices.dto.request;

import java.io.Serializable;

/**
 * 
 * @author EXT-IBANEZMA
 *
 */
public class ConsultaPEIRequestDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private RealizarTransferenciaPEITrackDataRequestDTO tracks;
	private String estado;
	private String idSucursal;
	private String idterminal;
	private String idReferenciaOperacionComercio;
	private String idOperacion;
	private String fechaDesde;
	private String fechaHasta;
	private String idCanal;
	private String idReferenciaTrxComercio;
	private String numeroReferenciaBancaria;

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado
	 *            the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the idSucursal
	 */
	public String getIdSucursal() {
		return idSucursal;
	}

	/**
	 * @param idSucursal
	 *            the idSucursal to set
	 */
	public void setIdSucursal(String idSucursal) {
		this.idSucursal = idSucursal;
	}

	/**
	 * @return the idterminal
	 */
	public String getIdterminal() {
		return idterminal;
	}

	/**
	 * @param idterminal
	 *            the idterminal to set
	 */
	public void setIdterminal(String idterminal) {
		this.idterminal = idterminal;
	}

	/**
	 * @return the idReferenciaOperacionComercio
	 */
	public String getIdReferenciaOperacionComercio() {
		return idReferenciaOperacionComercio;
	}

	/**
	 * @param idReferenciaOperacionComercio
	 *            the idReferenciaOperacionComercio to set
	 */
	public void setIdReferenciaOperacionComercio(String idReferenciaOperacionComercio) {
		this.idReferenciaOperacionComercio = idReferenciaOperacionComercio;
	}
	

	/**
	 * @return the idOperacion
	 */
	public String getIdOperacion() {
		return idOperacion;
	}

	/**
	 * @param idOperacion
	 *            the idOperacion to set
	 */
	public void setIdOperacion(String idOperacion) {
		this.idOperacion = idOperacion;
	}

	/**
	 * @return the fechaDesde
	 */
	public String getFechaDesde() {
		return fechaDesde;
	}

	/**
	 * @param fechaDesde
	 *            the fechaDesde to set
	 */
	public void setFechaDesde(String fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	/**
	 * @return the fechaHasta
	 */
	public String getFechaHasta() {
		return fechaHasta;
	}

	/**
	 * @param fechaHasta
	 *            the fechaHasta to set
	 */
	public void setFechaHasta(String fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	/**
	 * @return the idCanal
	 */
	public String getIdCanal() {
		return idCanal;
	}

	/**
	 * @param idCanal
	 *            the idCanal to set
	 */
	public void setIdCanal(String idCanal) {
		this.idCanal = idCanal;
	}

	/**
	 * @return the tracks
	 */
	public RealizarTransferenciaPEITrackDataRequestDTO getTracks() {
		return tracks;
	}

	/**
	 * @param tracks
	 *            the tracks to set
	 */
	public void setTracks(RealizarTransferenciaPEITrackDataRequestDTO tracks) {
		this.tracks = tracks;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getIdReferenciaTrxComercio() {
		return idReferenciaTrxComercio;
	}
	

	/**
	 * 
	 * @param idReferenciaTrxComercio
	 */
	public void setIdReferenciaTrxComercio(String idReferenciaTrxComercio) {
		this.idReferenciaTrxComercio = idReferenciaTrxComercio;
	}

	public String getNumeroReferenciaBancaria() {
		return numeroReferenciaBancaria;
	}

	public void setNumeroReferenciaBancaria(String numeroReferenciaBancaria) {
		this.numeroReferenciaBancaria = numeroReferenciaBancaria;
	}
	
}
