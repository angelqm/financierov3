/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  28/01/2015 - 14:43:48
 */

package ar.com.redlink.sam.financieroservices.dto.request;

import ar.com.redlink.sam.financieroservices.dto.CuentaDTO;
import ar.com.redlink.sam.financieroservices.dto.request.common.FinancieroCommonDTO;

/**
 * DTO para una consulta de saldo.
 * 
 * @author aguerrea
 * 
 */
public class BalanceRequestDTO extends FinancieroCommonDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3763493429742184694L;

	private CuentaDTO cuenta;

	/**
	 * @return the cuenta
	 */
	public CuentaDTO getCuenta() {
		return cuenta;
	}

	/**
	 * @param cuenta
	 *            the cuenta to set
	 */
	public void setCuenta(CuentaDTO cuenta) {
		this.cuenta = cuenta;
	}

}
