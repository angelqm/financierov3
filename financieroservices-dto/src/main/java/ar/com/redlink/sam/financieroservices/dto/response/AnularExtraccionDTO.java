/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  28/01/2015 - 14:43:31
 */

package ar.com.redlink.sam.financieroservices.dto.response;


/**
 * DTO para la respuesta de la operacion de anulacion de extraccion.
 * 
 * @author aguerrea
 * 
 */
public class AnularExtraccionDTO extends FinancieroServiceBaseResponseDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8230755366907099090L;
	private String ticket;

	/**
	 * @return the ticket
	 */
	public String getTicket() {
		return ticket;
	}

	/**
	 * @param ticket
	 *            the ticket to set
	 */
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
}
