package ar.com.redlink.sam.financieroservices.dto.request;

import java.io.Serializable;

/**
 * 
 * @author AQM
 *
 */
public class RealizarDevolucionPagoDevolucionPEIPagarRequestDTO implements Serializable {


	private static final long serialVersionUID = 3250482428407539099L;
	/*ESTO VA ENLACEPAGOS!!!!!!!!!!!!!!!!!!!!!!!!!!*/

	private String codTerminal;
	private String codSucursal;
	private EntidadPEIRequestDTO entidad; 
	private String panEntidad;	
	private OperacionOrigenPEIRequestDTO operacionOrigen;
	private RealizarTransferenciaPEITrackDataRequestDTO tracks;
	private CuentaDestinoDTO cuentaDestino;
	private TipoDocumentoDTO documento;
	private String codigoSeguridad;
	private String numero;	
	private String idReferenciaTrx;
	private String idReferenciaOperacion;
	private String idCanal;
	/**
	 * @return the codTerminal
	 */
	public String getCodTerminal() {
		return codTerminal;
	}
	/**
	 * @param codTerminal the codTerminal to set
	 */
	public void setCodTerminal(String codTerminal) {
		this.codTerminal = codTerminal;
	}
	/**
	 * @return the codSucursal
	 */
	public String getCodSucursal() {
		return codSucursal;
	}
	/**
	 * @param codSucursal the codSucursal to set
	 */
	public void setCodSucursal(String codSucursal) {
		this.codSucursal = codSucursal;
	}
	/**
	 * @return the entidad
	 */
	public EntidadPEIRequestDTO getEntidad() {
		return entidad;
	}
	/**
	 * @param entidad the entidad to set
	 */
	public void setEntidad(EntidadPEIRequestDTO entidad) {
		this.entidad = entidad;
	}
	/**
	 * @return the panEntidad
	 */
	public String getPanEntidad() {
		return panEntidad;
	}
	/**
	 * @param panEntidad the panEntidad to set
	 */
	public void setPanEntidad(String panEntidad) {
		this.panEntidad = panEntidad;
	}
	/**
	 * @return the operacionOrigen
	 */
	public OperacionOrigenPEIRequestDTO getOperacionOrigen() {
		return operacionOrigen;
	}
	/**
	 * @param operacionOrigen the operacionOrigen to set
	 */
	public void setOperacionOrigen(OperacionOrigenPEIRequestDTO operacionOrigen) {
		this.operacionOrigen = operacionOrigen;
	}
	/**
	 * @return the tracks
	 */
	public RealizarTransferenciaPEITrackDataRequestDTO getTracks() {
		return tracks;
	}
	/**
	 * @param tracks the tracks to set
	 */
	public void setTracks(RealizarTransferenciaPEITrackDataRequestDTO tracks) {
		this.tracks = tracks;
	}
	/**
	 * @return the cuentaDestino
	 */
	public CuentaDestinoDTO getCuentaDestino() {
		return cuentaDestino;
	}
	/**
	 * @param cuentaDestino the cuentaDestino to set
	 */
	public void setCuentaDestino(CuentaDestinoDTO cuentaDestino) {
		this.cuentaDestino = cuentaDestino;
	}
	/**
	 * @return the documento
	 */
	public TipoDocumentoDTO getDocumento() {
		return documento;
	}
	/**
	 * @param documento the documento to set
	 */
	public void setDocumento(TipoDocumentoDTO documento) {
		this.documento = documento;
	}
	/**
	 * @return the codigoSeguridad
	 */
	public String getCodigoSeguridad() {
		return codigoSeguridad;
	}
	/**
	 * @param codigoSeguridad the codigoSeguridad to set
	 */
	public void setCodigoSeguridad(String codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}
	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}
	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}
	/**
	 * @return the idReferenciaTrx
	 */
	public String getIdReferenciaTrx() {
		return idReferenciaTrx;
	}
	/**
	 * @param idReferenciaTrx the idReferenciaTrx to set
	 */
	public void setIdReferenciaTrx(String idReferenciaTrx) {
		this.idReferenciaTrx = idReferenciaTrx;
	}
	/**
	 * @return the idReferenciaOperacion
	 */
	public String getIdReferenciaOperacion() {
		return idReferenciaOperacion;
	}
	/**
	 * @param idReferenciaOperacion the idReferenciaOperacion to set
	 */
	public void setIdReferenciaOperacion(String idReferenciaOperacion) {
		this.idReferenciaOperacion = idReferenciaOperacion;
	}
	/**
	 * @return the idCanal
	 */
	public String getIdCanal() {
		return idCanal;
	}
	/**
	 * @param idCanal the idCanal to set
	 */
	public void setIdCanal(String idCanal) {
		this.idCanal = idCanal;
	}

}
