/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  10/03/2015 - 23:34:28
 */

package ar.com.redlink.sam.financieroservices.dto.request;

import ar.com.redlink.sam.financieroservices.dto.request.common.AnulacionCommonDTO;

/**
 * DTO para la operacion de anulacion de extraccion para debito.
 * 
 * @author aguerrea
 * 
 */
public class AnularDebitoRequestDTO extends AnulacionCommonDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1141652974205517432L;

}
