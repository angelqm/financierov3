package ar.com.redlink.sam.financieroservices.dto.request;

import java.io.Serializable;

/**
 * 
 * @author AQM
 * PEI desde PAGAR
 * ConsultaPEIPagar
 *
 */
public class ConsultaOperacionPEIPagarRequestDTO implements Serializable {

	/**
	 * AQM
	 * Esto va a enlacePagos
	 */
	private static final long serialVersionUID = -5058962178420120792L;

	private OrigenPEIRequestDTO origen;
	
	private ConsultaPEIPagarRequestDTO consulta;
	

	/**
	 * @return the origen
	 */
	public OrigenPEIRequestDTO getOrigen() {
		return origen;
	}

	/**
	 * @param origen the origen to set
	 */
	public void setOrigen(OrigenPEIRequestDTO origen) {
		this.origen = origen;
	}

	/**
	 * @return the consulta
	 */
	public ConsultaPEIPagarRequestDTO getConsulta() {
		return consulta;
	}

	/**
	 * @param consulta the consulta to set
	 */
	public void setConsulta(ConsultaPEIPagarRequestDTO consulta) {
		this.consulta = consulta;
	}
}
