package ar.com.redlink.sam.financieroservices.dto.request;

/**
 * 
 * @author EXT-IBANEZMA
 *
 */
public class HeaderPEIRequestDTO {

	private String requerimiento;
	private String cliente;
	
	public String getRequerimiento() {
		return requerimiento;
	}
	/**
	 * @return the cliente
	 */
	public String getCliente() {
		return cliente;
	}
	/**
	 * @param cliente the cliente to set
	 */
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	/**
	 * @param requerimiento the requerimiento to set
	 */
	public void setRequerimiento(String requerimiento) {
		this.requerimiento = requerimiento;
	}
	


	
}
