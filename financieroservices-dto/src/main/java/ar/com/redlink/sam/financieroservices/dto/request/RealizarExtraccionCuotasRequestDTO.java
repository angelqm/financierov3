/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  30/03/2015 - 00:55:50
 */

package ar.com.redlink.sam.financieroservices.dto.request;

import ar.com.redlink.sam.financieroservices.dto.request.common.FinancieroAccountDTO;

/**
 * DTO para la solicitud de extraccion en cuotas. 
 * 
 * @author aguerrea
 * 
 */
public class RealizarExtraccionCuotasRequestDTO extends FinancieroAccountDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 135043218498508689L;

	private int cuotas;

	/**
	 * @return the cuotas
	 */
	public int getCuotas() {
		return cuotas;
	}

	/**
	 * @param cuotas
	 *            the cuotas to set
	 */
	public void setCuotas(int cuotas) {
		this.cuotas = cuotas;
	}
}
