package ar.com.redlink.sam.financieroservices.dto.response;

import java.io.Serializable;

/**
 * DTO para la respuesta de la operacion de transferencia PEI.
 * 
 * @author AQM
 *
 */
public class RealizarDevolucionResultadoPEIPagarDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String secuenciaOn;
	private String idoperacion;
	private String tipooperacion;
	private String estado;
	private String fecha;
	private String numeroReferenciaBancaria;
	private String terminalPEI;
	private String sucursalPEI;	

	/**
	 * 
	 * @return the secuenciaOn
	 */
	public String getSecuenciaOn() {
		return secuenciaOn;
	}

	/**
	 * 
	 * @param secuenciaOn
	 */
	public void setSecuenciaOn(String secuenciaOn) {
		this.secuenciaOn = secuenciaOn;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado
	 *            the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha
	 *            the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the idoperacion
	 */
	public String getIdoperacion() {
		return idoperacion;
	}

	/**
	 * @param idoperacion
	 *            the idoperacion to set
	 */
	public void setIdoperacion(String idoperacion) {
		this.idoperacion = idoperacion;
	}

	/**
	 * @return the tipooperacion
	 */
	public String getTipooperacion() {
		return tipooperacion;
	}

	/**
	 * @param tipooperacion
	 *            the tipooperacion to set
	 */
	public void setTipooperacion(String tipooperacion) {
		this.tipooperacion = tipooperacion;
	}

	public String getNumeroReferenciaBancaria() {
		return numeroReferenciaBancaria;
	}

	public void setNumeroReferenciaBancaria(String numeroReferenciaBancaria) {
		this.numeroReferenciaBancaria = numeroReferenciaBancaria;
	}

	/**
	 * @return the terminalPEI
	 */
	public String getTerminalPEI() {
		return terminalPEI;
	}

	/**
	 * @param terminalPEI the terminalPEI to set
	 */
	public void setTerminalPEI(String terminalPEI) {
		this.terminalPEI = terminalPEI;
	}

	/**
	 * @return the sucursalPEI
	 */
	public String getSucursalPEI() {
		return sucursalPEI;
	}

	/**
	 * @param sucursalPEI the sucursalPEI to set
	 */
	public void setSucursalPEI(String sucursalPEI) {
		this.sucursalPEI = sucursalPEI;
	}
	
	
}
