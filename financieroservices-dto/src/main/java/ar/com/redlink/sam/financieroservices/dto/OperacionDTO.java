/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  28/01/2015 - 23:11:11
 */

package ar.com.redlink.sam.financieroservices.dto;

import java.io.Serializable;
import java.util.List;

/**
 * DTO para una operacion.
 * 
 * @author aguerrea
 * 
 */
public class OperacionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1705682691655696279L;

	private String codigoOperacion;

	private String nombreOperacion;

	private String descripcionOperacion;
	
	private String idOperacion;

	private boolean habilitada;

	private boolean requiereSfx;

	private boolean requiereSeg;

	private boolean requierePin;

	private boolean requiereDNI;

	private boolean requiereAut;

	private boolean requiereTar;

	private boolean ticketFirma;

	private int ticketCantidadCopias;

	private List<LimiteDTO> limites;

	/**
	 * @return the codigoOperacion
	 */
	public String getCodigoOperacion() {
		return codigoOperacion;
	}

	/**
	 * @param codigoOperacion
	 *            the codigoOperacion to set
	 */
	public void setCodigoOperacion(String codigoOperacion) {
		this.codigoOperacion = codigoOperacion;
	}

	/**
	 * @return the nombreOperacion
	 */
	public String getNombreOperacion() {
		return nombreOperacion;
	}

	/**
	 * @param nombreOperacion
	 *            the nombreOperacion to set
	 */
	public void setNombreOperacion(String nombreOperacion) {
		this.nombreOperacion = nombreOperacion;
	}

	/**
	 * @return the descripcionOperacion
	 */
	public String getDescripcionOperacion() {
		return descripcionOperacion;
	}

	/**
	 * @param descripcionOperacion
	 *            the descripcionOperacion to set
	 */
	public void setDescripcionOperacion(String descripcionOperacion) {
		this.descripcionOperacion = descripcionOperacion;
	}

	/**
	 * @return the habilitada
	 */
	public boolean isHabilitada() {
		return habilitada;
	}

	/**
	 * @param habilitada
	 *            the habilitada to set
	 */
	public void setHabilitada(boolean habilitada) {
		this.habilitada = habilitada;
	}

	/**
	 * @return the requiereSfx
	 */
	public boolean isRequiereSfx() {
		return requiereSfx;
	}

	/**
	 * @param requiereSfx
	 *            the requiereSfx to set
	 */
	public void setRequiereSfx(boolean requiereSfx) {
		this.requiereSfx = requiereSfx;
	}

	/**
	 * @return the requiereSeg
	 */
	public boolean isRequiereSeg() {
		return requiereSeg;
	}

	/**
	 * @param requiereSeg
	 *            the requiereSeg to set
	 */
	public void setRequiereSeg(boolean requiereSeg) {
		this.requiereSeg = requiereSeg;
	}

	/**
	 * @return the requierePin
	 */
	public boolean isRequierePin() {
		return requierePin;
	}

	/**
	 * @param requierePin
	 *            the requierePin to set
	 */
	public void setRequierePin(boolean requierePin) {
		this.requierePin = requierePin;
	}

	/**
	 * @return the requiereDNI
	 */
	public boolean isRequiereDNI() {
		return requiereDNI;
	}

	/**
	 * @param requiereDNI
	 *            the requiereDNI to set
	 */
	public void setRequiereDNI(boolean requiereDNI) {
		this.requiereDNI = requiereDNI;
	}

	/**
	 * @return the requiereAut
	 */
	public boolean isRequiereAut() {
		return requiereAut;
	}

	/**
	 * @param requiereAut
	 *            the requiereAut to set
	 */
	public void setRequiereAut(boolean requiereAut) {
		this.requiereAut = requiereAut;
	}

	/**
	 * @return the limites
	 */
	public List<LimiteDTO> getLimites() {
		return limites;
	}

	/**
	 * @param limites
	 *            the limites to set
	 */
	public void setLimites(List<LimiteDTO> limites) {
		this.limites = limites;
	}

	/**
	 * @return the ticketFirma
	 */
	public boolean getTicketFirma() {
		return ticketFirma;
	}

	/**
	 * @param ticketFirma
	 *            the ticketFirma to set
	 */
	public void setTicketFirma(boolean ticketFirma) {
		this.ticketFirma = ticketFirma;
	}

	/**
	 * @return the ticketCantidadCopias
	 */
	public int getTicketCantidadCopias() {
		return ticketCantidadCopias;
	}

	/**
	 * @param ticketCantidadCopias
	 *            the ticketCantidadCopias to set
	 */
	public void setTicketCantidadCopias(int ticketCantidadCopias) {
		this.ticketCantidadCopias = ticketCantidadCopias;
	}

	/**
	 * @return the requiereTar
	 */
	public boolean isRequiereTar() {
		return requiereTar;
	}

	/**
	 * @param requiereTar
	 *            the requiereTar to set
	 */
	public void setRequiereTar(boolean requiereTar) {
		this.requiereTar = requiereTar;
	}

	/**
	 * @return the idOperacion
	 */
	public String getIdOperacion() {
		return idOperacion;
	}

	/**
	 * @param idOperacion the idOperacion to set
	 */
	public void setIdOperacion(String idOperacion) {
		this.idOperacion = idOperacion;
	}
}
