/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  11/03/2015 - 22:14:38
 */

package ar.com.redlink.sam.financieroservices.dto.request;

import ar.com.redlink.sam.financieroservices.dto.request.common.AnulacionCommonDTO;

/**
 * DTO para la solicitud de anulacion de extraccion de prestamo.
 * 
 * @author aguerrea
 * 
 */
public class AnularExtraccionPrestamoRequestDTO extends AnulacionCommonDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2504449030237987158L;

}
