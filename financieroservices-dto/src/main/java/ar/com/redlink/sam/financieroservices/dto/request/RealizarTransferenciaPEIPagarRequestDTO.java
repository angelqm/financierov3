package ar.com.redlink.sam.financieroservices.dto.request;

import java.io.Serializable;

/**
 * DTO para la operacion de transferencia PEI.
 * 
 * @author he_e90104
 *
 */
public class RealizarTransferenciaPEIPagarRequestDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private RealizarTransferenciaPEIPagarPagoRequestDTO pago;

	/**
	 * @return the pago
	 */
	public RealizarTransferenciaPEIPagarPagoRequestDTO getPago() {
		return pago;
	}

	/**
	 * @param pago the pago to set
	 */
	public void setPago(RealizarTransferenciaPEIPagarPagoRequestDTO pago) {
		this.pago = pago;
	}

}
