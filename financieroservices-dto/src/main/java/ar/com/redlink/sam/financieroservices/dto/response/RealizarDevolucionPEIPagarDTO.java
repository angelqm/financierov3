package ar.com.redlink.sam.financieroservices.dto.response;

import java.io.Serializable;

public class RealizarDevolucionPEIPagarDTO  implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String secuenciaOn;
	
	private RealizarDevolucionResultadoPEIPagarDTO resultado;
	
	/**
	 * 
	 * @return the secuenciaOn
	 */
	public String getSecuenciaOn() {
		return secuenciaOn;
	}

	/**
	 * 
	 * @param secuenciaOn
	 */
	public void setSecuenciaOn(String secuenciaOn) {
		this.secuenciaOn = secuenciaOn;
	}

	/**
	 * @return the resultado
	 */
	public RealizarDevolucionResultadoPEIPagarDTO getResultado() {
		return resultado;
	}

	/**
	 * @param resultado the resultado to set
	 */
	public void setResultado(RealizarDevolucionResultadoPEIPagarDTO resultado) {
		this.resultado = resultado;
	}
	
}
