package ar.com.redlink.sam.financieroservices.dto.request;

import java.io.Serializable;

public class OperacionOrigenPEIRequestDTO implements Serializable {

	/**
	 * 
	 * @author AQM
	 *
	 */
	private static final long serialVersionUID = 2719570625978139944L;
	private String idPago;
	private String idReferenciaTrxOrigen;
	/**
	 * @return the idPago
	 */
	public String getIdPago() {
		return idPago;
	}
	/**
	 * @param idPago the idPago to set
	 */
	public void setIdPago(String idPago) {
		this.idPago = idPago;
	}
	/**
	 * @return the idReferenciaTrxOrigen
	 */
	public String getIdReferenciaTrxOrigen() {
		return idReferenciaTrxOrigen;
	}
	/**
	 * @param idReferenciaTrxOrigen the idReferenciaTrxOrigen to set
	 */
	public void setIdReferenciaTrxOrigen(String idReferenciaTrxOrigen) {
		this.idReferenciaTrxOrigen = idReferenciaTrxOrigen;
	}	
	
}
