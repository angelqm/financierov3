/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  19/03/2015 - 23:19:10
 */

package ar.com.redlink.sam.financieroservices.dto.request;

import ar.com.redlink.sam.financieroservices.dto.request.common.FinancieroAccountDTO;

/**
 * DTO para el request de extraccion de prestamo. Se define su representacion
 * para eventuales extensiones.
 * 
 * @author aguerrea
 * 
 */
public class RealizarExtraccionPrestamoRequestDTO extends FinancieroAccountDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7980244193464128544L;
}
