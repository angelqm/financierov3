/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  30/03/2015 - 00:54:09
 */

package ar.com.redlink.sam.financieroservices.dto.response;

/**
 * DTO para la respuesta de anulacion de extraccion en cuotas.
 * 
 * @author aguerrea
 * 
 */
public class AnularExtraccionCuotasDTO extends FinancieroServiceBaseResponseDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 958860010585771753L;

	private String ticket;

	/**
	 * @return the ticket
	 */
	public String getTicket() {
		return ticket;
	}

	/**
	 * @param ticket
	 *            the ticket to set
	 */
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

}
