/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  20/04/2015 - 22:30:59
 */

package ar.com.redlink.sam.financieroservices.dto.request.common;


/**
 * DTO para los atributos comunes de una anulacion.
 * 
 * @author aguerrea
 * 
 */
public class AnulacionCommonDTO extends FinancieroAccountDTO {

	private static final long serialVersionUID = -5605474083685957943L;

	private Long trxFinancieroId;
	
	/**
	 * 
	 * @return
	 */
	public Long getTrxFinancieroId() {
		return trxFinancieroId;
	}

	/**
	 * 
	 * @param trxFinancieroId
	 */
	public void setTrxFinancieroId(Long trxFinancieroId) {
		this.trxFinancieroId = trxFinancieroId;
	}
}
