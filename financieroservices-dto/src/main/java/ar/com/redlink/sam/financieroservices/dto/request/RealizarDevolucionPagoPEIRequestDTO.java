package ar.com.redlink.sam.financieroservices.dto.request;

import java.io.Serializable;

/**
 * DTO para la operacion de transferencia PEI.
 * 
 * @author EXT-IBANEZMA
 *
 */
public class RealizarDevolucionPagoPEIRequestDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private RealizarDevolucionPagoDevolucionPEIRequestDTO devolucion;

	/**
	 * @return the devolucion
	 */
	public RealizarDevolucionPagoDevolucionPEIRequestDTO getDevolucion() {
		return devolucion;
	}

	/**
	 * @param devolucion
	 *            the devolucion to set
	 */
	public void setDevolucion(RealizarDevolucionPagoDevolucionPEIRequestDTO devolucion) {
		this.devolucion = devolucion;
	}

}
