package ar.com.redlink.sam.financieroservices.dto.request;

import java.io.Serializable;

/**
 * 
 * @author EXT-ibanezma
 *
 */
public class SessionPEIRequestDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private CredencialesPEIRequestDTO credenciales;

	/**
	 * @return the credenciales
	 */
	public CredencialesPEIRequestDTO getCredenciales() {
		return credenciales;
	}

	/**
	 * @param credenciales the credenciales to set
	 */
	public void setCredenciales(CredencialesPEIRequestDTO credenciales) {
		this.credenciales = credenciales;
	}

}