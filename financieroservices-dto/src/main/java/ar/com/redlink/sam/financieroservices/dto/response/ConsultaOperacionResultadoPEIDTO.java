package ar.com.redlink.sam.financieroservices.dto.response;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author EXT-IBANEZMA
 *
 */
public class ConsultaOperacionResultadoPEIDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer total;

	private List<ConsultaPEIDTO> operacion;

	private String estado;

	private String fecha;

	private String idoperacion;

	private String secuenciaOn;

	/**
	 * @return the total
	 */
	public Integer getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(Integer total) {
		this.total = total;
	}

	/**
	 * @return the operacion
	 */
	public List<ConsultaPEIDTO> getOperacion() {
		return operacion;
	}

	/**
	 * @param operacion
	 *            the operacion to set
	 */
	public void setOperacion(List<ConsultaPEIDTO> operacion) {
		this.operacion = operacion;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado
	 *            the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha
	 *            the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the idoperacion
	 */
	public String getIdoperacion() {
		return idoperacion;
	}

	/**
	 * @param idoperacion
	 *            the idoperacion to set
	 */
	public void setIdoperacion(String idoperacion) {
		this.idoperacion = idoperacion;
	}

	/**
	 * @return the secuenciaOn
	 */
	public String getSecuenciaOn() {
		return secuenciaOn;
	}

	/**
	 * @param secuenciaOn
	 *            the secuenciaOn to set
	 */
	public void setSecuenciaOn(String secuenciaOn) {
		this.secuenciaOn = secuenciaOn;
	}

}
