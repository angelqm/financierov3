/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  28/01/2015 - 14:46:11
 */

package ar.com.redlink.sam.financieroservices.dto.request;

import ar.com.redlink.sam.financieroservices.dto.request.common.FinancieroServiceBaseRequestDTO;

/**
 * DTO para los datos de identificacion de una terminal.
 * 
 * @author aguerrea
 * 
 */
public class TerminalRequestDTO extends FinancieroServiceBaseRequestDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6712100128725184702L;

}
