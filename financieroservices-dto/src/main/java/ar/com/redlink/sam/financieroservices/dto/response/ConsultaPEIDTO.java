package ar.com.redlink.sam.financieroservices.dto.response;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 
 * @author EXT-IBANEZMA
 *
 */
public class ConsultaPEIDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String pan;
	private String idreferenciaoperacioncomercio;
	private BigDecimal importe;
	private BigDecimal saldo;
	private String motivorechazo;
	private String idoperacion;
	private String tipooperacion;
	private String estado;
	private String fecha;
	private String concepto;
	private String numeroReferenciaBancaria;

	/**
	 * @return the pan
	 */
	public String getPan() {
		return pan;
	}

	/**
	 * @param pan
	 *            the pan to set
	 */
	public void setPan(String pan) {
		this.pan = pan;
	}

	/**
	 * @return the idReferenciaOperacionComercio
	 */
	public String getIdreferenciaoperacioncomercio() {
		return idreferenciaoperacioncomercio;
	}

	/**
	 * @param idReferenciaOperacionComercio
	 *            the idReferenciaOperacionComercio to set
	 */
	public void setIdreferenciaoperacioncomercio(String idreferenciaoperacioncomercio) {
		this.idreferenciaoperacioncomercio = idreferenciaoperacioncomercio;
	}

	/**
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}

	/**
	 * @param importe
	 *            the importe to set
	 */
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	/**
	 * @return the saldo
	 */
	public BigDecimal getSaldo() {
		return saldo;
	}

	/**
	 * @param saldo
	 *            the saldo to set
	 */
	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	/**
	 * @return the motivoRechazo
	 */
	public String getMotivorechazo() {
		return motivorechazo;
	}

	/**
	 * @param motivoRechazo
	 *            the motivoRechazo to set
	 */
	public void setMotivorechazo(String motivorechazo) {
		this.motivorechazo = motivorechazo;
	}

	/**
	 * @return the idoperacion
	 */
	public String getIdoperacion() {
		return idoperacion;
	}

	/**
	 * @param idoperacion
	 *            the idoperacion to set
	 */
	public void setIdoperacion(String idoperacion) {
		this.idoperacion = idoperacion;
	}

	/**
	 * @return the tipooperacion
	 */
	public String getTipooperacion() {
		return tipooperacion;
	}

	/**
	 * @param tipooperacion
	 *            the tipooperacion to set
	 */
	public void setTipooperacion(String tipooperacion) {
		this.tipooperacion = tipooperacion;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado
	 *            the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha
	 *            the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the concepto
	 */
	public String getConcepto() {
		return concepto;
	}

	/**
	 * @param concepto the concepto to set
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getNumeroReferenciaBancaria() {
		return numeroReferenciaBancaria;
	}

	public void setNumeroReferenciaBancaria(String numeroReferenciaBancaria) {
		this.numeroReferenciaBancaria = numeroReferenciaBancaria;
	}
	
	

}
