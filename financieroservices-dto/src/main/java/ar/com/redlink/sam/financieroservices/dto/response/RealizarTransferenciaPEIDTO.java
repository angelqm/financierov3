package ar.com.redlink.sam.financieroservices.dto.response;

import java.io.Serializable;

/**
 * DTO para la respuesta de la operacion de transferencia PEI.
 * 
 * @author EXT-IBANEZMA
 *
 */
public class RealizarTransferenciaPEIDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private RealizarTransferenciaResultadoPEIDTO resultado;
	
	/**
	 * @return the resultado
	 */
	public RealizarTransferenciaResultadoPEIDTO getResultado() {
		return resultado;
	}

	/**
	 * @param resultado the resultado to set
	 */
	public void setResultado(RealizarTransferenciaResultadoPEIDTO resultado) {
		this.resultado = resultado;
	}
	
}
