package ar.com.redlink.sam.financieroservices.dto.request;

import java.io.Serializable;

public class CuentaDestinoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2719570625978139944L;
	private String cbu;
	private String aliasCbu;

	/**
	 * @return the cbu
	 */
	public String getCbu() {
		return cbu;
	}

	/**
	 * @param cbu
	 *            the cbu to set
	 */
	public void setCbu(String cbu) {
		this.cbu = cbu;
	}

	/**
	 * @return the aliasCbu
	 */
	public String getAliasCbu() {
		return aliasCbu;
	}

	/**
	 * @param aliasCbu the aliasCbu to set
	 */
	public void setAliasCbu(String aliasCbu) {
		this.aliasCbu = aliasCbu;
	}
}
