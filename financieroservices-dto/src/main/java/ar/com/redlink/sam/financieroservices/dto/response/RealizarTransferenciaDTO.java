/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  11/03/2015 - 22:41:22
 */

package ar.com.redlink.sam.financieroservices.dto.response;

import ar.com.redlink.sam.financieroservices.dto.response.common.OperacionCommonDTO;

/**
 * DTO para la respuesta de la operacion de transferencia.
 * 
 * @author aguerrea
 * 
 */
public class RealizarTransferenciaDTO extends OperacionCommonDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6659966790546938128L;
	
	private String tipoCambio;

	/**
	 * @return the tipoCambio
	 */
	public String getTipoCambio() {
		return tipoCambio;
	}

	/**
	 * @param tipoCambio the tipoCambio to set
	 */
	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	
}
