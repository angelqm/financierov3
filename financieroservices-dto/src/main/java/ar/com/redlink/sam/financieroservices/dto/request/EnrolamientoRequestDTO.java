/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.dto.request;

import ar.com.redlink.sam.financieroservices.dto.request.common.FinancieroServiceBaseRequestDTO;

/**
 * DTO para status del enrolamiento.
 * 
 * @author aguerrea
 * 
 */
public class EnrolamientoRequestDTO extends FinancieroServiceBaseRequestDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2505806891716290125L;

}
