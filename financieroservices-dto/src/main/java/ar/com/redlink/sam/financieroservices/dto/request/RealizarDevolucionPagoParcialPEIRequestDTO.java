package ar.com.redlink.sam.financieroservices.dto.request;

import java.io.Serializable;

/**
 * DTO para la operacion de transferencia PEI.
 * 
 * @author EXT-IBANEZMA
 *
 */
public class RealizarDevolucionPagoParcialPEIRequestDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private RealizarDevolucionPagoParcialDevolucionPEIRequestDTO devolucion;

	/**
	 * @return the devolucion
	 */
	public RealizarDevolucionPagoParcialDevolucionPEIRequestDTO getDevolucion() {
		return devolucion;
	}

	/**
	 * @param devolucion
	 *            the devolucion to set
	 */
	public void setDevolucion(RealizarDevolucionPagoParcialDevolucionPEIRequestDTO devolucion) {
		this.devolucion = devolucion;
	}

}
