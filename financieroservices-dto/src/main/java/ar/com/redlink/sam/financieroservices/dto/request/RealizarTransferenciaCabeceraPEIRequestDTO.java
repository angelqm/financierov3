package ar.com.redlink.sam.financieroservices.dto.request;

import java.io.Serializable;

public class RealizarTransferenciaCabeceraPEIRequestDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String idAplicacion;
	private String idRequerimiento;
	private String ipCliente;

	/**
	 * @return the idAplicacion
	 */
	public String getIdAplicacion() {
		return idAplicacion;
	}

	/**
	 * @param idAplicacion
	 *            the idAplicacion to set
	 */
	public void setIdAplicacion(String idAplicacion) {
		this.idAplicacion = idAplicacion;
	}

	/**
	 * @return the idRequerimiento
	 */
	public String getIdRequerimiento() {
		return idRequerimiento;
	}

	/**
	 * @param idRequerimiento
	 *            the idRequerimiento to set
	 */
	public void setIdRequerimiento(String idRequerimiento) {
		this.idRequerimiento = idRequerimiento;
	}

	/**
	 * @return the ipCliente
	 */
	public String getIpCliente() {
		return ipCliente;
	}

	/**
	 * @param ipCliente
	 *            the ipCliente to set
	 */
	public void setIpCliente(String ipCliente) {
		this.ipCliente = ipCliente;
	}

}
