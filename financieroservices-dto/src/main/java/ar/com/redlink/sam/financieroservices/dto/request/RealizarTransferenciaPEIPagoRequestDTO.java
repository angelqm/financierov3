package ar.com.redlink.sam.financieroservices.dto.request;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * DTO para la operacion de transferencia PEI.
 * 
 * @author EXT-IBANEZMA
 *
 */
public class RealizarTransferenciaPEIPagoRequestDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer importe;

	private String moneda;

	private String numero;

	private String idReferenciaTrxComercio;

	private String idReferenciaOperacionComercio;

	private String idCanal;

	private String idTerminal;

	private String posEntryMode;

	private RealizarTransferenciaPEITrackDataRequestDTO tracks;

	private String titularDocumento;

	private String codigoSeguridad;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String concepto;

	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * @param numero
	 *            the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * @return the importe
	 */
	public Integer getImporte() {
		return importe;
	}

	/**
	 * @param importe
	 *            the importe to set
	 */
	public void setImporte(Integer importe) {
		this.importe = importe;
	}

	/**
	 * @return the moneda
	 */
	public String getMoneda() {
		return moneda;
	}

	/**
	 * @param moneda
	 *            the moneda to set
	 */
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	/**
	 * @return the idReferenciaTrxComercio
	 */
	public String getIdReferenciaTrxComercio() {
		return idReferenciaTrxComercio;
	}

	/**
	 * @param idReferenciaTrxComercio
	 *            the idReferenciaTrxComercio to set
	 */
	public void setIdReferenciaTrxComercio(String idReferenciaTrxComercio) {
		this.idReferenciaTrxComercio = idReferenciaTrxComercio;
	}

	/**
	 * @return the idReferenciaOperacionComercio
	 */
	public String getIdReferenciaOperacionComercio() {
		return idReferenciaOperacionComercio;
	}

	/**
	 * @param idReferenciaOperacionComercio
	 *            the idReferenciaOperacionComercio to set
	 */
	public void setIdReferenciaOperacionComercio(String idReferenciaOperacionComercio) {
		this.idReferenciaOperacionComercio = idReferenciaOperacionComercio;
	}

	/**
	 * @return the idCanal
	 */
	public String getIdCanal() {
		return idCanal;
	}

	/**
	 * @param idCanal
	 *            the idCanal to set
	 */
	public void setIdCanal(String idCanal) {
		this.idCanal = idCanal;
	}

	/**
	 * @return the idTerminal
	 */
	public String getIdTerminal() {
		return idTerminal;
	}

	/**
	 * @param idTerminal
	 *            the idTerminal to set
	 */
	public void setIdTerminal(String idTerminal) {
		this.idTerminal = idTerminal;
	}

	/**
	 * @return the posEntryMode
	 */
	public String getPosEntryMode() {
		return posEntryMode;
	}

	/**
	 * @param posEntryMode
	 *            the posEntryMode to set
	 */
	public void setPosEntryMode(String posEntryMode) {
		this.posEntryMode = posEntryMode;
	}

	/**
	 * @return the titularDocumento
	 */
	public String getTitularDocumento() {
		return titularDocumento;
	}

	/**
	 * @param titularDocumento
	 *            the titularDocumento to set
	 */
	public void setTitularDocumento(String titularDocumento) {
		this.titularDocumento = titularDocumento;
	}

	/**
	 * @return the codigoSeguridad
	 */
	public String getCodigoSeguridad() {
		return codigoSeguridad;
	}

	/**
	 * @param codigoSeguridad
	 *            the codigoSeguridad to set
	 */
	public void setCodigoSeguridad(String codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}

	/**
	 * @return the tracks
	 */
	public RealizarTransferenciaPEITrackDataRequestDTO getTracks() {
		return tracks;
	}

	/**
	 * @param tracks
	 *            the tracks to set
	 */
	public void setTracks(RealizarTransferenciaPEITrackDataRequestDTO tracks) {
		this.tracks = tracks;
	}

	/**
	 * @return the concepto
	 */
	public String getConcepto() {
		return concepto;
	}

	/**
	 * @param concepto the concepto to set
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

}
