/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  10/03/2015 - 23:15:12
 */

package ar.com.redlink.sam.financieroservices.dto.response;

import ar.com.redlink.sam.financieroservices.dto.response.common.OperacionCommonDTO;

/**
 * DTO para la respuesta de la operacion de extraccion para debito.
 * 
 * @author aguerrea
 * 
 */
public class RealizarDebitoDTO extends OperacionCommonDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2549902824565484785L;

}
