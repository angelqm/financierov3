/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  28/01/2015 - 22:46:09
 */

package ar.com.redlink.sam.financieroservices.dto;

import java.io.Serializable;

/**
 * DTO para una cuenta.
 * 
 * @author aguerrea
 * 
 */
public class CuentaDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7162217115256375612L;

	private String numeroCuenta;

	private String tipoCuenta;

	private String monedaCuenta;
	
	private String estadoCuenta;

	/**
	 * @return the numeroCuenta
	 */
	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	/**
	 * @param numeroCuenta
	 *            the numeroCuenta to set
	 */
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	/**
	 * @return the tipoCuenta
	 */
	public String getTipoCuenta() {
		return tipoCuenta;
	}

	/**
	 * @param tipoCuenta
	 *            the tipoCuenta to set
	 */
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	/**
	 * @return the monedaCuenta
	 */
	public String getMonedaCuenta() {
		return monedaCuenta;
	}

	/**
	 * @param monedaCuenta
	 *            the monedaCuenta to set
	 */
	public void setMonedaCuenta(String monedaCuenta) {
		this.monedaCuenta = monedaCuenta;
	}

	/**
	 * @return the estadoCuenta
	 */
	public String getEstadoCuenta() {
		return estadoCuenta;
	}

	/**
	 * @param estadoCuenta the estadoCuenta to set
	 */
	public void setEstadoCuenta(String estadoCuenta) {
		this.estadoCuenta = estadoCuenta;
	}
}
