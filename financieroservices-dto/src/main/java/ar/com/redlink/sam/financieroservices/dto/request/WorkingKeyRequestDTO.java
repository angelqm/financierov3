/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  28/01/2015 - 14:41:53
 */

package ar.com.redlink.sam.financieroservices.dto.request;

import ar.com.redlink.sam.financieroservices.dto.request.common.FinancieroServiceBaseRequestDTO;

/**
 * DTO para una solicitud de Working Key.
 * 
 * @author aguerrea
 * 
 */
public class WorkingKeyRequestDTO extends FinancieroServiceBaseRequestDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4110677030565039703L;

	private String tipoTerminal;

	private String tipo;

	/**
	 * @return the tipo
	 */
	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipo
	 *            the tipo to set
	 */
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	/**
	 * @return the tipoTerminal
	 */
	public String getTipoTerminal() {
		return tipoTerminal;
	}

	/**
	 * @param tipoTerminal
	 *            the tipoTerminal to set
	 */
	public void setTipoTerminal(String tipoTerminal) {
		this.tipoTerminal = tipoTerminal;
	}

}
