package ar.com.redlink.sam.financieroservices.dto.response;

import java.io.Serializable;

/**
 * 
 * @author AQM
 *
 */
public class RealizarTransferenciaResultadoPEIPagarDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String secuenciaOn;
	private String idOperacion;
	private String idOperacionOrigen;
	private String numeroReferenciaBancaria;
	private String tipoOperacion;
	private String fecha;
	private String estado;
	private String terminalPEI;
	private String sucursalPEI;	
	
	/**
	 * @return the secuenciaOn
	 */
	public String getSecuenciaOn() {
		return secuenciaOn;
	}
	/**
	 * @param secuenciaOn the secuenciaOn to set
	 */
	public void setSecuenciaOn(String secuenciaOn) {
		this.secuenciaOn = secuenciaOn;
	}
	/**
	 * @return the idOperacion
	 */
	public String getIdOperacion() {
		return idOperacion;
	}
	/**
	 * @param idOperacion the idOperacion to set
	 */
	public void setIdOperacion(String idOperacion) {
		this.idOperacion = idOperacion;
	}
	/**
	 * @return the idOperacionOrigen
	 */
	public String getIdOperacionOrigen() {
		return idOperacionOrigen;
	}
	/**
	 * @param idOperacionOrigen the idOperacionOrigen to set
	 */
	public void setIdOperacionOrigen(String idOperacionOrigen) {
		this.idOperacionOrigen = idOperacionOrigen;
	}
	/**
	 * @return the numeroReferenciaBancaria
	 */
	public String getNumeroReferenciaBancaria() {
		return numeroReferenciaBancaria;
	}
	/**
	 * @param numeroReferenciaBancaria the numeroReferenciaBancaria to set
	 */
	public void setNumeroReferenciaBancaria(String numeroReferenciaBancaria) {
		this.numeroReferenciaBancaria = numeroReferenciaBancaria;
	}
	/**
	 * @return the tipoOperacion
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	/**
	 * @param tipoOperacion the tipoOperacion to set
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}
	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * @return the terminalPEI
	 */
	public String getTerminalPEI() {
		return terminalPEI;
	}
	/**
	 * @param terminalPEI the terminalPEI to set
	 */
	public void setTerminalPEI(String terminalPEI) {
		this.terminalPEI = terminalPEI;
	}
	/**
	 * @return the sucursalPEI
	 */
	public String getSucursalPEI() {
		return sucursalPEI;
	}
	/**
	 * @param sucursalPEI the sucursalPEI to set
	 */
	public void setSucursalPEI(String sucursalPEI) {
		this.sucursalPEI = sucursalPEI;
	}
	
}
