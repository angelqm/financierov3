
/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.logging.services.impl;

import ar.com.redlink.sam.financieroservices.logging.services.SequencesProvider;



public class SequencesProviderImpl implements SequencesProvider{

	private static final long serialVersionUID = 1L;
	private Long idOrden;
	private Long NroPaquete;
	
	SequencesProviderImpl(){
		this.idOrden = 0l;
		this.NroPaquete = 0l;
	}
	
	@Override
	public Long getIdOrden() {
		return idOrden;
	}

	@Override
	public Long getNextIdOrden() {
		this.idOrden ++;
		
		return this.idOrden;
	}

	@Override
	public Long getNextNroPaquete() {
		this.NroPaquete ++;
		return this.NroPaquete;
	}

	@Override
	public Long getNroPaquete() {
		return this.NroPaquete;
	}

}
