
/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.logging.services.impl;

import ar.com.redlink.sam.financieroservices.logging.services.LoggingContext;
import ar.com.redlink.sam.financieroservices.logging.services.LoggingContextProvider;


public class LoggingContextProviderImpl implements LoggingContextProvider{

	private static final ThreadLocalLoggingContext threadLocalLoggingContext = new ThreadLocalLoggingContext();

	private LoggingContextProviderImpl() {
		super();
	}

	public LoggingContext getContext() {
		LoggingContext ctx = threadLocalLoggingContext.get();
		return ctx;
	}

	public void putContext(LoggingContext ctx) {
		threadLocalLoggingContext.set(ctx);
	}

	public void clear() {
		threadLocalLoggingContext.clear();
	}

	private static class ThreadLocalLoggingContext extends ThreadLocal<LoggingContext> {

		@Override
		protected LoggingContext initialValue() {
			final LoggingContext value = (LoggingContext) new LoggingContextDefaultImpl(new SequencesProviderImpl());
			
			return value;
		}
		
		public void clear() {
			this.remove();
		}

	}

}
