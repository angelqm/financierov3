
/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.logging.services;

import java.util.List;

import ar.com.redlink.framework.entities.RedLinkLogElement;

public interface RedLinkLogElementFactory {
	
	public List<RedLinkLogElement> genenerarLogElementPostInvoque(Object[] informacion, Object resultado, LoggingContext contexto); 
	
	public List<RedLinkLogElement> genenerarLogElementInvoqueExcepcion(Object[] informacion, Exception e, LoggingContext contexto);

}
