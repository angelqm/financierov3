
/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.logging.services;

public interface LoggingContextProvider {
	
	public LoggingContext getContext();
	public void putContext(LoggingContext ctx);
	public void clear();
	
}
