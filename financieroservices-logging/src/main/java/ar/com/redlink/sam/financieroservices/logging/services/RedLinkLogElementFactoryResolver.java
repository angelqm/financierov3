
/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.logging.services;

public interface RedLinkLogElementFactoryResolver {

	RedLinkLogElementFactory get(String className, String methodName);
	
}
