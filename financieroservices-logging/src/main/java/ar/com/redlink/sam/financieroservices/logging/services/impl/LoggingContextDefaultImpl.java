
/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.logging.services.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import ar.com.redlink.sam.financieroservices.logging.services.LoggingContext;
import ar.com.redlink.sam.financieroservices.logging.services.SequencesProvider;



public class LoggingContextDefaultImpl implements LoggingContext, Serializable{

	private static final long serialVersionUID = 1L;
	private String codActividad;
	private String id;
	private Long nroPaquete;
	private Long idOrden;
	private SequencesProvider sequencesProvider;
	private Map<String,Serializable> map;
	
	public LoggingContextDefaultImpl(SequencesProvider sequencesProvider) {
		this.sequencesProvider = sequencesProvider;
		map = new HashMap<String,Serializable>();
		this.increaseIdOrden();
		this.increaseNroPaquete();
	}

	@Override
	public String getCodActividad() {
		return codActividad;
	}

	@Override
	public void setCodActividad(String codActividad) {
		this.codActividad = codActividad;
	}
	
	@Override
	public Long increaseIdOrden() {
		Long idOrden = sequencesProvider.getNextIdOrden();
		setIdOrden(idOrden);
		return idOrden;
	}

	@Override
	public Long increaseNroPaquete() {
		Long nroPaquete = sequencesProvider.getNextNroPaquete();
		this.setNroPaquete(nroPaquete);
		return nroPaquete;
	}
	
	public void setSequencesProvider(SequencesProvider sequencesProvider) {
		this.sequencesProvider = sequencesProvider;
	}

	@Override
	public Long getNroPaquete() {
		return nroPaquete;
	}

	@Override
	public void setNroPaquete(Long nroPaquete) {
		this.nroPaquete = nroPaquete;
	}

	@Override
	public Long getIdOrden() {
		return this.idOrden;
	}

	@Override
	public void setIdOrden(Long idOrden) {
		this.idOrden = idOrden;
	}

	@Override
	public String generateNewId() {
		this.id = UUID.randomUUID().toString();
		return this.id;
	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public Serializable get(String id) {
		return map.get(id);
	}

	@Override
	public void put(String id, Serializable o) {
		map.put(id, o);
	}

	@Override
	public void clear() {
		codActividad = null;
		id = null;
		nroPaquete = 0L;
		idOrden = 0L;
		map.clear();
	}

}
