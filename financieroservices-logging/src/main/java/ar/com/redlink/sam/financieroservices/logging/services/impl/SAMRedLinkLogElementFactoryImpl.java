/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.logging.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.SystemUtils;

import ar.com.redlink.framework.entities.RedLinkLogElement;
import ar.com.redlink.sam.financieroservices.logging.services.LoggingContext;
import ar.com.redlink.sam.financieroservices.logging.services.RedLinkLogElementFactory;
import ar.com.redlink.sam.financieroservices.logging.util.IdentificadoresLog;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;

public class SAMRedLinkLogElementFactoryImpl implements
		RedLinkLogElementFactory {

	private String serverId;
	private static final String NL = SystemUtils.LINE_SEPARATOR;

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}

	@Override
	public List<RedLinkLogElement> genenerarLogElementInvoqueExcepcion(
			Object[] informacion, Exception e, LoggingContext contexto) {

		List<RedLinkLogElement> resultadoLog = new ArrayList<RedLinkLogElement>();

		RedLinkLogElement proceso = generateLog(
				(WSAbstractRequest) informacion[0], contexto);
		RedLinkLogElement error = generateError(
				(WSAbstractRequest) informacion[0], e, contexto);

		setearError(proceso, contexto, e);

		resultadoLog.add(proceso);
		resultadoLog.add(error);

		return resultadoLog;
	}

	@Override
	public List<RedLinkLogElement> genenerarLogElementPostInvoque(
			Object[] informacion, Object resultado, LoggingContext contexto) {
		List<RedLinkLogElement> resultadoLog = new ArrayList<RedLinkLogElement>();

		resultadoLog.add(generateLog((WSAbstractRequest) informacion[0],
				contexto));

		return resultadoLog;
	}

	private RedLinkLogElement generateError(WSAbstractRequest requestInfo,
			Exception e, LoggingContext contexto) {
		RedLinkLogElement error = new RedLinkLogElement();

		error.setProducto(IdentificadoresLog.ID_SERVICIO);
		error.setId(IdentificadoresLog.ID_LOG_ELEMENT_ERROR);

		error.put(IdentificadoresLog.IDENTIFICADOR, contexto.getId());

		if (e instanceof Exception) {
			error.put(IdentificadoresLog.ERROR, e.getMessage());
		} else {
			error.put(IdentificadoresLog.ERROR, buildStackTrace(e));
		}
		;

		error.put(IdentificadoresLog.TS_ENTRADA,
				contexto.get(IdentificadoresLog.TS_ENTRADA));

		return error;
	}

	private RedLinkLogElement generateLog(WSAbstractRequest requestInfo,
			LoggingContext contexto) {
		RedLinkLogElement proceso = new RedLinkLogElement();

		proceso.setProducto(IdentificadoresLog.ID_SERVICIO);
		proceso.setId(IdentificadoresLog.ID_LOG_ELEMENT_ACTIVIDAD);

		proceso.put(IdentificadoresLog.CODIGO_RESPUESTA, "00");
		proceso.put(IdentificadoresLog.IDENTIFICADOR, contexto.generateNewId());

		proceso.put(IdentificadoresLog.ID_REQUERIMIENTO, requestInfo
				.getHeader().getIdRequerimiento());
		proceso.put(IdentificadoresLog.ENTIDAD, requestInfo.getHeader()
				.getEntidad());
		proceso.put(IdentificadoresLog.IP_CLIENTE, requestInfo.getHeader()
				.getIpCliente());
		proceso.put(IdentificadoresLog.TIME_STAMP_INFORMADO, requestInfo
				.getHeader().getTimeStamp());
		proceso.put(IdentificadoresLog.CANAL, requestInfo.getHeader()
				.getTerminal());

		proceso.put(IdentificadoresLog.ID_SERVER, this.serverId);
		proceso.put(IdentificadoresLog.TS_ENTRADA,
				contexto.get(IdentificadoresLog.TS_ENTRADA));
		proceso.put(IdentificadoresLog.DURACION,
				contexto.get(IdentificadoresLog.DURACION));
		proceso.put(IdentificadoresLog.OPERACION,
				contexto.get(IdentificadoresLog.OPERACION));

		return proceso;
	}

	private void setearError(RedLinkLogElement element,
			LoggingContext contexto, Exception e) {

		if (e instanceof Exception) {
			element.put(IdentificadoresLog.CODIGO_RESPUESTA,
					((Exception) e).getMessage());
		} else {
			element.put(IdentificadoresLog.CODIGO_RESPUESTA, "99");
		}

	}

	private String buildStackTrace(Throwable t) {
		final int maxStacks = 5;
		final StringBuilder stackTrace = new StringBuilder();
		Throwable currT = t;
		int nroStack = 0;
		while ((currT != null) && (nroStack < maxStacks)) {
			if (currT != t) {
				stackTrace.append("caused by: ").append(NL);
			}
			stackTrace.append(currT.toString()).append(NL);
			for (StackTraceElement stElement : currT.getStackTrace()) {
				stackTrace.append(stElement.toString()).append(NL);
			}
			nroStack++;
			currT = currT.getCause();
		}

		if (stackTrace.length() > 0) {
			stackTrace.setLength(stackTrace.length() - 1);
		}

		return stackTrace.toString();
	}
}
