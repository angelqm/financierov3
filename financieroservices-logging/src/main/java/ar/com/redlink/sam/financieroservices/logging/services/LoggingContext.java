
/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.logging.services;

import java.io.Serializable;

public interface LoggingContext {

	public String getCodActividad();
	public void setCodActividad(String codActividad);
	
	public Long increaseNroPaquete();
	public Long getNroPaquete();
	public void setNroPaquete(Long nroPaquete);

	public Long increaseIdOrden();
	public Long getIdOrden();
	public void setIdOrden(Long idOrden);
	
	public String generateNewId();
	public String getId();
	public void setId(String id);

	void put(String id, Serializable o);
	Serializable get(String id);
	
	void clear();
	
}

