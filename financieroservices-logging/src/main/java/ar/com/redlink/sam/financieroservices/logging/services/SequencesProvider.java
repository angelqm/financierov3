
/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.logging.services;

import java.io.Serializable;

public interface SequencesProvider extends Serializable {

	public Long getNextIdOrden();
	public Long getIdOrden();
	
	public Long getNextNroPaquete();
	public Long getNroPaquete();

}
