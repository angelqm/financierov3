/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.logging.interceptor;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;

import ar.com.redlink.sam.financieroservices.logging.services.LoggingContext;
import ar.com.redlink.sam.financieroservices.logging.services.LoggingContextProvider;
import ar.com.redlink.sam.financieroservices.logging.services.RedLinkLogElementFactory;
import ar.com.redlink.sam.financieroservices.logging.services.RedLinkLogElementFactoryResolver;
import ar.com.redlink.sam.financieroservices.logging.util.IdentificadoresLog;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import ar.com.redlink.framework.entities.RedLinkLogElement;
import ar.com.redlink.framework.logging.client.service.RedLinkLoggingService;

public class LoggingInterceptor implements MethodInterceptor {

	private static final org.apache.commons.logging.Log LOG = org.apache.commons.logging.LogFactory
			.getLog(LoggingInterceptor.class);

	private LoggingContextProvider loggingContextProvider;
	private RedLinkLogElementFactoryResolver redLinkLogElementFactoryResolver;
	private RedLinkLoggingService redLinkLoggingService;

	public void setLoggingContextProvider(
			LoggingContextProvider loggingContextProvider) {
		this.loggingContextProvider = loggingContextProvider;
	}

	public void setRedLinkLoggingService(
			RedLinkLoggingService redLinkLoggingService) {
		this.redLinkLoggingService = redLinkLoggingService;
	}

	public void setRedLinkLogElementFactoryResolver(
			RedLinkLogElementFactoryResolver redLinkLogElementFactoryResolver) {
		this.redLinkLogElementFactoryResolver = redLinkLogElementFactoryResolver;
	}

	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {

		long tsEntrada = 0;
		Object result = null;
		Method method = invocation.getMethod();

		LOG.debug("*******Inicio - LoggingInterceptor*******");
		String methodName = method.getName();
		String className = method.getDeclaringClass().getName();

		boolean auditar = false;
		LoggingContext context = null;
		context = this.getContext();

		RedLinkLogElementFactory rllef = null;
		auditar = false;
		if (context != null) {

			rllef = redLinkLogElementFactoryResolver.get(className, methodName);

			if (rllef != null) {
				tsEntrada = System.currentTimeMillis();
				context.put(IdentificadoresLog.TS_ENTRADA, new Date(tsEntrada));
				context.put(IdentificadoresLog.OPERACION, methodName);
				auditar = true;
			}
		}

		try {
			result = invocation.proceed();

			if (auditar) {
				context.put(IdentificadoresLog.DURACION,
						Long.valueOf(System.currentTimeMillis() - tsEntrada));
				this.logPostInvoque(invocation, context, result, rllef);
			}

		} catch (Exception exception) {

			if (auditar) {
				context.put(IdentificadoresLog.DURACION,
						new Long(System.currentTimeMillis() - tsEntrada));
				this.logExceptionInvoque(invocation, context, exception, rllef);
			}

			throw exception;
		}
		LOG.debug("*******Fin - LoggingInterceptor*******");
		return result;
	}

	private void logExceptionInvoque(MethodInvocation invocation,
			LoggingContext context, Exception exception,
			RedLinkLogElementFactory redLinkLogElementFactory) {
		try {
			LOG.debug("       Inicio - logExceptionInvoque");
			List<RedLinkLogElement> logElements = redLinkLogElementFactory
					.genenerarLogElementInvoqueExcepcion(
							invocation.getArguments(), exception, context);

			if (logElements != null) {
				for (RedLinkLogElement logElement : logElements) {
					redLinkLoggingService.logAsync(logElement);
				}

			}
			LOG.debug("       Fin - logExceptionInvoque");
		} catch (Exception e) {
			LOG.error("       Error - logExceptionInvoque", e);
		}
	}

	private void logPostInvoque(MethodInvocation invocation,
			LoggingContext context, Object resultado,
			RedLinkLogElementFactory redLinkLogElementFactory) {
		try {
			LOG.debug("       Inicio - logPostInvoque");
			List<RedLinkLogElement> logElements = redLinkLogElementFactory
					.genenerarLogElementPostInvoque(invocation.getArguments(),
							resultado, context);

			if (logElements != null) {
				for (RedLinkLogElement logElement : logElements) {
					redLinkLoggingService.logAsync(logElement);
				}
			}
			LOG.debug("       Fin - logPostInvoque");
		} catch (Exception e) {
			LOG.error("       Error - logPostInvoque", e);
		}
	}

	private LoggingContext getContext() {
		LoggingContext context = null;

		try {
			LOG.debug("       Inicio - getContext");
			context = this.loggingContextProvider.getContext();
			LOG.debug("       Fin - getContext");
		} catch (Exception e) {
			LOG.error("       Error - getContext", e);
		}

		return context;
	}

}
