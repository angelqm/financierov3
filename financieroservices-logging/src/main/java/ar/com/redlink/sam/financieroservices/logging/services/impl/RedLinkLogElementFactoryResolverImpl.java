
/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.logging.services.impl;

import java.util.Map;

import ar.com.redlink.sam.financieroservices.logging.services.RedLinkLogElementFactory;
import ar.com.redlink.sam.financieroservices.logging.services.RedLinkLogElementFactoryResolver;


public class RedLinkLogElementFactoryResolverImpl implements
		RedLinkLogElementFactoryResolver {

	private Map<String,Map<String,RedLinkLogElementFactory>> classNameAndMethodNameToRedLinkLogElementFactory;	
	
	@Override
	public RedLinkLogElementFactory get(String className, String methodName) {

		Map<String,RedLinkLogElementFactory> m = classNameAndMethodNameToRedLinkLogElementFactory.get(className);
		
		return m != null ? m.get(methodName) : null;
	}

	public void setClassNameAndMethodNameToRedLinkLogElementFactory(Map<String, Map<String, RedLinkLogElementFactory>> classNameAndMethodNameToRedLinkLogElementFactory) {
		this.classNameAndMethodNameToRedLinkLogElementFactory = classNameAndMethodNameToRedLinkLogElementFactory;
	}

}
