/*
 * Flux IT S.A
 * La Plata - Buenos Aires - Argentina
 * http://www.fluxit.com.ar
 * Author: Alan Aguerreberre
 * Date:  08/05/2012 - 12:03:09
 */
package ar.com.redlink.sam.financieroservices.log.processor.common.entity;

import java.util.Date;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Entidad de Logging.
 * 
 * @author aguerrea
 * 
 */
public class LogEntity implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8630047222500873828L;

	private Long id;
	private String identificador;
	private String resultado;
	private Date timeStamp;
	private Long duracion;
	private String servicio;
	private String servidor;
	private String requerimiento;
	private String entidad;
	private String canal;
	private String timeStampInformado;
	private String ipCliente;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the identificador
	 */
	public String getIdentificador() {
		return identificador;
	}

	/**
	 * @param identificador
	 *            the identificador to set
	 */
	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	/**
	 * @return the resultado
	 */
	public String getResultado() {
		return resultado;
	}

	/**
	 * @param resultado
	 *            the resultado to set
	 */
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	/**
	 * @return the timeStamp
	 */
	public Date getTimeStamp() {
		return timeStamp;
	}

	/**
	 * @param timeStamp
	 *            the timeStamp to set
	 */
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	/**
	 * @return the duracion
	 */
	public Long getDuracion() {
		return duracion;
	}

	/**
	 * @param duracion
	 *            the duracion to set
	 */
	public void setDuracion(Long duracion) {
		this.duracion = duracion;
	}

	/**
	 * @return the servicio
	 */
	public String getServicio() {
		return servicio;
	}

	/**
	 * @param servicio
	 *            the servicio to set
	 */
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	/**
	 * @return the servidor
	 */
	public String getServidor() {
		return servidor;
	}

	/**
	 * @param servidor
	 *            the servidor to set
	 */
	public void setServidor(String servidor) {
		this.servidor = servidor;
	}

	/**
	 * @return the requerimiento
	 */
	public String getRequerimiento() {
		return requerimiento;
	}

	/**
	 * @param requerimiento
	 *            the requerimiento to set
	 */
	public void setRequerimiento(String requerimiento) {
		this.requerimiento = requerimiento;
	}

	/**
	 * @return the entidad
	 */
	public String getEntidad() {
		return entidad;
	}

	/**
	 * @param entidad
	 *            the entidad to set
	 */
	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}

	/**
	 * @return the canal
	 */
	public String getCanal() {
		return canal;
	}

	/**
	 * @param canal
	 *            the canal to set
	 */
	public void setCanal(String canal) {
		this.canal = canal;
	}

	/**
	 * @return the timeStampInformado
	 */
	public String getTimeStampInformado() {
		return timeStampInformado;
	}

	/**
	 * @param timeStampInformado
	 *            the timeStampInformado to set
	 */
	public void setTimeStampInformado(String timeStampInformado) {
		this.timeStampInformado = timeStampInformado;
	}

	/**
	 * @return the ipCliente
	 */
	public String getIpCliente() {
		return ipCliente;
	}

	/**
	 * @param ipCliente
	 *            the ipCliente to set
	 */
	public void setIpCliente(String ipCliente) {
		this.ipCliente = ipCliente;
	}

}
