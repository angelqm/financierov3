/*
 * Flux IT S.A
 * La Plata - Buenos Aires - Argentina
 * http://www.fluxit.com.ar
 * Author: Alan Aguerreberre
 * Date:  08/05/2012 - 12:15:57
 */
package ar.com.redlink.sam.financieroservices.log.processor.backend.service.impl;

import java.util.Date;

import org.springframework.transaction.PlatformTransactionManager;

import ar.com.redlink.framework.entities.RedLinkLogElement;
import ar.com.redlink.framework.entities.exception.RedLinkFrameworkException;
import ar.com.redlink.framework.persistence.dao.crud.RedLinkGenericDAO;
import ar.com.redlink.framework.services.RedLinkService;
import ar.com.redlink.sam.financieroservices.log.processor.backend.service.LogProcessorService;
import ar.com.redlink.sam.financieroservices.log.processor.common.entity.LogEntity;
import ar.com.redlink.sam.financieroservices.log.processor.logging.util.IdentificadoresLog;

/**
 * Implementacion del servicio de procesamiento de logs.
 * 
 * @author aguerrea
 * 
 */
public class LogProcessorServiceImpl implements RedLinkService,
		LogProcessorService {

	private PlatformTransactionManager transactionManager;

	private RedLinkGenericDAO<LogEntity> genericDao;

	/**
	 * 
	 * @see ar.com.redlink.framework.logging.processor.RedLinkLoggingProcessor#process(ar.com.redlink.framework.entities.RedLinkLogElement)
	 */
	@Override
	public void process(RedLinkLogElement log) throws RedLinkFrameworkException {
		LogEntity entity = new LogEntity();
		Long duration;

		entity.setIdentificador((String) log
				.get(IdentificadoresLog.IDENTIFICADOR));
		entity.setCanal((String) log.get(IdentificadoresLog.CANAL));
		try {
			duration = (Long) log.get(IdentificadoresLog.DURACION);

		} catch (Exception e) {
			duration = 0L;
		}
		entity.setDuracion(duration);
		entity.setEntidad((String) log.get(IdentificadoresLog.ENTIDAD));
		entity.setIpCliente((String) log.get(IdentificadoresLog.IP_CLIENTE));
		entity.setRequerimiento((String) log
				.get(IdentificadoresLog.ID_REQUERIMIENTO));
		entity.setResultado((String) log
				.get(IdentificadoresLog.CODIGO_RESPUESTA));
		entity.setServicio((String) log.get(IdentificadoresLog.OPERACION));
		entity.setServidor((String) log.get(IdentificadoresLog.ID_SERVER));
		entity.setTimeStamp((Date) log.get(IdentificadoresLog.TS_ENTRADA));
		entity.setTimeStampInformado((String) log
				.get(IdentificadoresLog.TIME_STAMP_INFORMADO));

		this.getGenericDao().save(entity);
	}

	/**
	 * @see ar.com.redlink.soat.framework.logging.processor.RedLinkLoggingProcessor#processorID()
	 */
	@Override
	public String processorID() {
		return IdentificadoresLog.ID_LOG_ELEMENT_ACTIVIDAD + "."
				+ IdentificadoresLog.ID_SERVICIO;
	}

	/**
	 * @return the genericDao
	 */
	public RedLinkGenericDAO<LogEntity> getGenericDao() {
		return genericDao;
	}

	/**
	 * @param genericDao
	 *            the genericDao to set
	 */
	public void setGenericDao(RedLinkGenericDAO<LogEntity> genericDao) {
		this.genericDao = genericDao;
	}

	/**
	 * 
	 * @return platformTransactionManager
	 */
	public PlatformTransactionManager getTransactionManager() {
		return this.transactionManager;
	}

	/**
	 * @param transactionManager
	 *            the transactionManager to set
	 */
	public void setTransactionManager(
			PlatformTransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}

}
