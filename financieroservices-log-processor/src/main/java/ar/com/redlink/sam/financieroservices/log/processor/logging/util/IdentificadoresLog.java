package ar.com.redlink.sam.financieroservices.log.processor.logging.util;

/**
 * Constantes para identificacion de los campos.
 * 
 * @author aguerrea
 * 
 */
public class IdentificadoresLog {

	public final static String IDENTIFICADOR = "identificador";
	public final static String CODIGO_RESPUESTA = "codigo-respuesta";
	public final static String TS_ENTRADA = "ts-entrada";
	public final static String DURACION = "duracion";
	public final static String OPERACION = "operacion";

	public final static String ID_SERVER = "id-server";

	public final static String ERROR = "error";

	public final static String ID_REQUERIMIENTO = "id-requerimiento";
	public final static String ENTIDAD = "entidad";
	public final static String IP_CLIENTE = "ip-cliente";
	public final static String TIME_STAMP_INFORMADO = "time-stamp-inf";
	public final static String CANAL = "canal";

	public final static String ID_SERVICIO = "SOATSERVICES";
	public final static String ID_LOG_ELEMENT_ACTIVIDAD = "1";
	public final static String ID_LOG_ELEMENT_ERROR = "2";

}
