/*
 * Flux IT S.A
 * La Plata - Buenos Aires - Argentina
 * http://www.fluxit.com.ar
 * Author: Alan Aguerreberre
 * Date:  08/05/2012 - 12:10:03
 */
package ar.com.redlink.sam.financieroservices.log.processor.backend.service;

import ar.com.redlink.framework.logging.processor.RedLinkLoggingProcessor;

/**
 * Interface del servicio para procesamiento de logs.
 * 
 * @author aguerrea
 * 
 */
public interface LogProcessorService extends RedLinkLoggingProcessor {

}
