
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Manejo de Cuentas</title>
        <script src="/js/jquery/jquery-1.7.2.js"></script>
        <script type="text/javascript">
        function doAjaxPost() {
        // Obtener los valores del form
        var name = $('#name').val();

        $.ajax({
        type: "POST",
        url: "/GetCuentaById",
        data: "cuenta=" + cuenta,
        success: function(response){
        // Hubo una respuesta...
        $('#info').html(response);
        $('#id').val('');
        $('#titular').val('');
        $('#saldo').val('');
        },
        error: function(e){
        alert('Error: ' + e);
        }
        });
        }
        </script>
    </head>
    <body>
        <h1>Busqueda de cuenta por ID</h1>
        <table>
            <tr><td>Id de cuenta: </td><td> <input type="text" id="cuenta"><br/></td></tr>
            <tr><td colspan="2"><input type="button" value="Buscar" onclick="doAjaxPost()"><br/></td></tr>
            <tr><td colspan="2"><div id="info" style="color: green;"></div></td></tr>
        </table>
        <a href="/">Mostrar todas las cuentas</a>
    </body>
</html>
