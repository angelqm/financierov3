/*
 * Red Link Argentina Ciudad Autónoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar Author: "Joaquín Díaz Vélez" Date: 27/04/2012 -
 * 14:14:55
 */

package ar.com.redlink.sam.financieroservices.logging.consumer;

import ar.com.redlink.framework.entities.RedLinkLogElement;
import ar.com.redlink.framework.entities.exception.RedLinkFrameworkException;

/**
 * Interfaz que define el comportamiento para la consumicion de mensajes de log.
 * 
 * @author "Joaquín Díaz Vélez"
 * 
 */
public interface RedLinkLogElementConsumer {

	/**
	 * @param timeout
	 * @return
	 * @throws RedLinkFrameworkException
	 */
	public RedLinkLogElement receive(long timeout) throws RedLinkFrameworkException;
}
