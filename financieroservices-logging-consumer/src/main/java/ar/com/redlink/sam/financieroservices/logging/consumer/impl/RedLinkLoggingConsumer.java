/*
 * Red Link Argentina Ciudad Autónoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar Author: "Joaquín Díaz Vélez" Date: 02/05/2012 -
 * 11:21:41
 */

package ar.com.redlink.sam.financieroservices.logging.consumer.impl;

import org.apache.log4j.Logger;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import ar.com.redlink.framework.entities.RedLinkLogElement;
import ar.com.redlink.framework.entities.exception.RedLinkFrameworkException;
import ar.com.redlink.framework.logging.processor.RedLinkLoggingProcessor;
import ar.com.redlink.sam.financieroservices.logging.consumer.jms.RedLinkJmsLogElementConsumer;
import ar.com.redlink.sam.financieroservices.logging.consumer.resolver.RedLinkLoggingProcessorResolver;
import ar.com.redlink.sam.financieroservices.logging.consumer.serialize.RedLinkMessageSerialization;

/**
 * @author "Joaquín Díaz Vélez"
 * 
 */
public class RedLinkLoggingConsumer {

	private static final Logger LOG = Logger.getLogger(RedLinkLoggingConsumer.class);

	private PlatformTransactionManager transactionManager;
	private TransactionStatus transactionStatus;

	private RedLinkJmsLogElementConsumer consumer;
	private RedLinkLoggingProcessorResolver processorResolver;

	private long maxTransactionTime = 21000;
	private long maxMessage = 100;
	private long checkTimeout = 10000;

	private RedLinkMessageSerialization messageSerializer;

	/**
	 * 
	 */
	public RedLinkLoggingConsumer() {
	}

	/**
	 * @param checkTimeout
	 */
	public void setCheckTimeout(long checkTimeout) {
		this.checkTimeout = checkTimeout;
	}

	/**
	 * @param maxTransactionTime
	 */
	public void setMaxTransactionTime(long maxTransactionTime) {
		this.maxTransactionTime = maxTransactionTime;
	}

	/**
	 * @param maxMessage
	 */
	public void setMaxMessage(long maxMessage) {
		this.maxMessage = maxMessage;
	}

	/**
	 * 
	 */
	public void stop() {
		this.getConsumer().close();
	}
	
	/**
	 * @return
	 */
	private boolean beginTransaction() {
		boolean result = true;
		DefaultTransactionDefinition transactionDefinition = new DefaultTransactionDefinition();
		transactionDefinition.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
		try {
			this.transactionStatus = getTransactionManager().getTransaction(transactionDefinition);
		}
		catch (TransactionException e) {
			LOG.error("Ocurrio un error durante el beginTransaction de la transaccion",e);
			result = false;
		}
		catch (RuntimeException e) {
			LOG.error("Ocurrio un error desconocido durante el beginTransaction de la transaccion",e);
			result = false;
		}
		return result;
	}

	/**
	 * @return
	 */
	private boolean commitTransaction() {
		boolean result = true;
		try {
			LOG.info("Comiteando la transaccion");
			this.getTransactionManager().commit(this.transactionStatus);
		}catch (TransactionException e) {
			LOG.error("Ocurrio un error durante el commitTransaction de la transaccion",e);
			result = false;
		}catch (RuntimeException e) {
			LOG.error("Ocurrio un error desconocido durante el commitTransaction de la transaccion",e);
			result = false;
		}
		return result;
	}

	/**
	 * 
	 */
	public void process() {
		long currentTime = System.currentTimeMillis();
		RedLinkLogElement message = null;
		int messageCount = 0;
		boolean existError = false;
		LOG.info("Arrancando a escuchar por mensajes");
		try {
			this.beginTransaction();
			this.getConsumer().initialize();
			while (messageCount < maxMessage && ((System.currentTimeMillis() - currentTime) < maxTransactionTime)) {
				message = this.getConsumer().receive(checkTimeout);
				if (null != message) {
					LOG.info("Recibiendo un mensaje");
					RedLinkLoggingProcessor processor = this.getProcessorResolver().resolve(message.processorID());
					
					if (null != processor) {
						processor.process(message);
					}else {
						LOG.warn("No se encontro el resolver para el id:" + message.processorID() + ". Se descarta el mensaje. Refrescar la configuracion de la aplicacion");
					}
					messageCount++;
				}
			}
		}catch (RedLinkFrameworkException e) {
			existError = true;
			LOG.error("Ocurrio un error procesando una tanda de mensajes. Se hace rollback de la transaccion",e);
		}catch (Throwable e) {
			existError = true;
			LOG.error("Ocurrio un error procesando una tanda de mensajes. Se hace rollback de la transaccion",e);
		}finally {
			this.getConsumer().close();
			this.commitTransaction();
			
			
			if (existError) {
				this.getMessageSerializer().performSerialization(message);
			}			
		}
	}

	/**
	 * @return the consumer
	 */
	public RedLinkJmsLogElementConsumer getConsumer() {
		return consumer;
	}

	/**
	 * @param consumer
	 *            the consumer to set
	 */
	public void setConsumer(RedLinkJmsLogElementConsumer consumer) {
		this.consumer = consumer;
	}

	/**
	 * @return the processorResolver
	 */
	public RedLinkLoggingProcessorResolver getProcessorResolver() {
		return processorResolver;
	}

	/**
	 * @param processorResolver
	 *            the processorResolver to set
	 */
	public void setProcessorResolver(RedLinkLoggingProcessorResolver processorResolver) {
		this.processorResolver = processorResolver;
	}

	/**
	 * @return
	 */
	public PlatformTransactionManager getTransactionManager() {
		return transactionManager;
	}

	/**
	 * @param transactionManager
	 */
	public void setTransactionManager(PlatformTransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}

	/**
	 * @return the messageSerializer
	 */
	public RedLinkMessageSerialization getMessageSerializer() {
		return messageSerializer;
	}

	/**
	 * @param messageSerializer the messageSerializer to set
	 */
	public void setMessageSerializer(RedLinkMessageSerialization messageSerializer) {
		this.messageSerializer = messageSerializer;
	}

}
