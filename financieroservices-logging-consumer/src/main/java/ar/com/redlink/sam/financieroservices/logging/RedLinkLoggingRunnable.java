/**
 * 
 */
package ar.com.redlink.sam.financieroservices.logging;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.task.TaskExecutor;

import ar.com.redlink.sam.financieroservices.logging.consumer.impl.RedLinkLoggingConsumer;

import commonj.work.Work;

/**
 * @author "Joaquín Díaz Vélez"
 *
 */
public class RedLinkLoggingRunnable implements Work, InitializingBean, DisposableBean {

	private static final Logger LOG = Logger.getLogger(RedLinkLoggingRunnable.class);
	private RedLinkLoggingConsumer loggingConsumer;
	private TaskExecutor workManager;
	private boolean running = true;

	/**
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		while(running) {
			try {
				if(LOG.isDebugEnabled()) {
					LOG.debug("Consumiendo una tanda de mensajes desde la cola JMS");
				}
				this.loggingConsumer.process();
			}
			catch(Throwable e) {
				LOG.error("Ocurrio una excepcion procesando un mensaje. Se captura el error y se prosigue con la proxima ejecución",e);
			}
		}
	}

	/**
	 * 
	 */
	public void stop() {
		this.running = false;
	}

	/**
	 * @return
	 */
	public RedLinkLoggingConsumer getLoggingConsumer() {
		return loggingConsumer;
	}

	/**
	 * @param loggingConsumer
	 */
	public void setLoggingConsumer(RedLinkLoggingConsumer loggingConsumer) {
		this.loggingConsumer = loggingConsumer;
	}

	/**
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		LOG.info("Inicializando el proceso de consumo de mensajes JMS de la cola de Log");
		this.workManager.execute(this);
	}

	/** 
	 * @see commonj.work.Work#isDaemon()
	 */
	@Override
	public boolean isDaemon() {
		return true;
	}

	/** 
	 * @see commonj.work.Work#release()
	 */
	@Override
	public void release() {
		LOG.info("Desactivando el procesamiento de mensajes JMS");
		this.getLoggingConsumer().stop();
		this.running = false;
	}


	/**
	 * @return
	 */
	public TaskExecutor getWorkManager() {
		return workManager;
	}


	/**
	 * @param workManager
	 */
	public void setWorkManager(TaskExecutor workManager) {
		this.workManager = workManager;
	}

	/** 
	 * @see org.springframework.beans.factory.DisposableBean#destroy()
	 */
	@Override
	public void destroy() throws Exception {
		this.release();
	}

}
