/*
 * Red Link Argentina Ciudad Autónoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar Author: "Joaquín Díaz Vélez" Date: 17/04/2012 -
 * 21:31:44
 */

package ar.com.redlink.sam.financieroservices.logging.consumer.jms;

import java.util.Properties;

import javax.jms.ConnectionFactory;
import javax.naming.NamingException;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jndi.JndiObjectFactoryBean;

import ar.com.redlink.framework.core.RedLinkFrameworkEnvironment;
import ar.com.redlink.framework.crypto.service.RedLinkCryptoService;
import ar.com.redlink.framework.entities.exception.RedLinkFrameworkException;

/**
 * Crea una {@link ConnectionFactory} dependiendo del valor de la propiedad
 * environment. El resultado puede ser una {@link ConnectionFactory} obtenida
 * desde JNDI o creada utilizando ActiveMQ
 * 
 * @author "Joaquín Díaz Vélez"
 * 
 */
public class RedLinkJmsLogElementConsumerConnectionFactoryBean implements
		FactoryBean<ConnectionFactory>, InitializingBean {

	private static final String NAMING_PRINCIPAL = "java.naming.security.principal";
	private static final String NAMING_CREDENCIALS = "java.naming.security.credentials";
	private static final Logger LOG = Logger.getLogger(RedLinkJmsLogElementConsumerConnectionFactoryBean.class);

	private String jndiName;
	private Properties jndiProperties;

	private String brokerURL;
	private String username;
	private String password;
	
	private RedLinkCryptoService cryptoService;
	private RedLinkFrameworkEnvironment frameworkEnviroment;

	/**
	 * 
	 */
	public RedLinkJmsLogElementConsumerConnectionFactoryBean() {
	}

	/**
	 * @see org.springframework.beans.factory.FactoryBean#getObject()
	 */
	@Override
	public ConnectionFactory getObject() throws Exception {
		ConnectionFactory connectionFactory = null;
		if (this.frameworkEnviroment.isLocalEnvironment()) {
			if(LOG.isInfoEnabled()) {
				LOG.info("Ejecutando el cliente de log configurado para desarrollo.Creando la conexion JMS utilizando ActiveMQ.");
			}
			connectionFactory = new ActiveMQConnectionFactory(this.getBrokerURL());
		}else if (this.frameworkEnviroment.isDevelopmentEnvironment() || this.frameworkEnviroment.isTestingEnvironment() || this.frameworkEnviroment.isProductionEnvironment()) {
			if(LOG.isInfoEnabled()) {
				LOG.info("Ejecutando el cliente de log configurado para produccion.Creando la conexion JMS utilizando JNDI.");
			}
			connectionFactory = getJNDIConnectionFactory();
		}else {
			LOG.warn("No se pudo establecer el ambiente de ejecucion. La conexion no estara disponible");
			throw new RedLinkFrameworkException("No se pudo inicializar la conexion JMS de logging porque debido a que el parametro environment no posee un valor valido");
		}
		
		return connectionFactory;
	}

	/**
	 * @throws NamingException
	 */
	private ConnectionFactory getJNDIConnectionFactory() throws NamingException {
		JndiObjectFactoryBean factoryBean = new JndiObjectFactoryBean();
		factoryBean.setJndiName(jndiName);
		if(null != this.jndiProperties) {
			factoryBean.setJndiEnvironment(getJndiProperties());
		}
		factoryBean.afterPropertiesSet();
		return (ConnectionFactory) factoryBean.getObject();
	}

	/**
	 * @see org.springframework.beans.factory.FactoryBean#getObjectType()
	 */
	@Override
	public Class<?> getObjectType() {
		return ConnectionFactory.class;
	}

	/**
	 * @see org.springframework.beans.factory.FactoryBean#isSingleton()
	 */
	@Override
	public boolean isSingleton() {
		return false;
	}

	/**
	 * @return the brokerURL
	 */
	public String getBrokerURL() {
		return brokerURL;
	}

	/**
	 * @param brokerURL
	 *            the brokerURL to set
	 */
	public void setBrokerURL(String brokerURL) {
		this.brokerURL = brokerURL;
	}

	/**
	 * @return the jndiName
	 */
	public String getJndiName() {
		return jndiName;
	}

	/**
	 * @param jndiName
	 *            the jndiName to set
	 */
	public void setJndiName(String jndiName) {
		this.jndiName = jndiName;
	}

	/**
	 * @return
	 */
	public Properties getJndiProperties() {
		return jndiProperties;
	}

	/**
	 * @param jndiProperties
	 */
	public void setJndiProperties(Properties jndiProperties) {
		this.jndiProperties = jndiProperties;
	}

	/**
	 * @return
	 */
	public RedLinkFrameworkEnvironment getFrameworkEnviroment() {
		return frameworkEnviroment;
	}

	
	/**
	 * @param frameworkEnviroment
	 */
	public void setFrameworkEnviroment(RedLinkFrameworkEnvironment frameworkEnviroment) {
		this.frameworkEnviroment = frameworkEnviroment;
	}

	
	/**
	 * @return
	 */
	public String getUsername() {
		return username;
	}

	
	/**
	 * @param username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	
	/**
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	
	/**
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	
	/**
	 * @return
	 */
	public RedLinkCryptoService getCryptoService() {
		return cryptoService;
	}

	
	/**
	 * @param cryptoService
	 */
	public void setCryptoService(RedLinkCryptoService cryptoService) {
		this.cryptoService = cryptoService;
	}

	/** 
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		// la utilizacion es opcional, por eso tanto if
		if(null != this.cryptoService) {
			if(null != this.username) {
				this.username = this.cryptoService.decrypt(username);
			}
			if(null != this.password) {
				this.password = this.cryptoService.decrypt(password);
			}
			if(null != this.jndiProperties) {
				if(this.jndiProperties.containsKey(NAMING_CREDENCIALS)) {
					this.jndiProperties.put(NAMING_CREDENCIALS, this.cryptoService.decrypt(this.jndiProperties.getProperty(NAMING_CREDENCIALS)));
				}
				if(this.jndiProperties.containsKey(NAMING_PRINCIPAL)) {
					this.jndiProperties.put(NAMING_PRINCIPAL, this.cryptoService.decrypt(this.jndiProperties.getProperty(NAMING_PRINCIPAL)));
				}
			}
		}
	}

}
