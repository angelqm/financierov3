/*
 * Red Link Argentina Ciudad Autónoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar Author: "Joaquín Díaz Vélez" Date: 17/04/2012 -
 * 23:51:43
 */

package ar.com.redlink.sam.financieroservices.logging.consumer.resolver.impl;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import ar.com.redlink.framework.entities.exception.RedLinkFrameworkException;
import ar.com.redlink.framework.logging.processor.RedLinkLoggingProcessor;
import ar.com.redlink.sam.financieroservices.logging.consumer.resolver.RedLinkLoggingProcessorResolver;

/**
 * Componente encargado de la obtencion del processor requerido, buscando el jar
 * correspondiente en el directorio de la aplicacion a traves del id provisto.
 * Una vez ubicado el jar, se cargan las clases del mismo y se importa el
 * contexto de Spring.
 * 
 * Trabaja con esta noción de classpath
 * 
 * extendedClasspath descriptors jarNames --> los lee periodicamente
 * contextNames --> los lee periodicamente jars contexts
 * 
 * @author "Joaquín Díaz Vélez"
 */
public class RedLinkLoggingProcessorResolverImpl implements RedLinkLoggingProcessorResolver, ApplicationContextAware, InitializingBean {

	private static final Logger LOGGER = Logger.getLogger(RedLinkLoggingProcessorResolverImpl.class);

	private static final String FILE_PREFIX = "file://";

	private static final String JAR_FOLDER = "jars";

	private static final String CONTEXT_FOLDER = "contexts";

	private String extendedClasspath = "/tmp/extended-classpath";

	private String processorBeanName = "redlink.framework.logging.processor.defaultProcessor";

	private ApplicationContext applicationContext;

	private Map<String, String> jarNames;

	private Map<String, String> contextFileNames;

	private Map<String, RedLinkLoggingProcessor> resolvedProcessors;

	/**
	 * 
	 */
	public RedLinkLoggingProcessorResolverImpl() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see ar.com.redlink.framework.logging.consumer.resolver.RedLinkLoggingProcessorResolver#resolve(java.lang.String)
	 */
	@Override
	public RedLinkLoggingProcessor resolve(String processorID) throws RedLinkFrameworkException {
		RedLinkLoggingProcessor processor = this.resolvedProcessors.get(processorID);
		if (null == processor) {
			this.doResolve(processorID);
			processor = this.resolvedProcessors.get(processorID);
		}
		return processor;
	}

	/**
	 * @return the resolvedProcessors
	 */
	public Map<String, RedLinkLoggingProcessor> getResolvedProcessors() {
		return resolvedProcessors;
	}

	/**
	 * @param resolvedProcessors
	 *            the resolvedProcessors to set
	 */
	public void setResolvedProcessors(Map<String, RedLinkLoggingProcessor> resolvedProcessors) {
		this.resolvedProcessors = resolvedProcessors;
	}

	/**
	 * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
	 */
	@Override
	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		this.applicationContext = arg0;
	}

	/**
	 * @return the applicationContext
	 */
	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	/**
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		this.setResolvedProcessors(new HashMap<String, RedLinkLoggingProcessor>());
		this.setContextFileNames(new HashMap<String, String>());
		this.setJarNames(new HashMap<String, String>());
		this.loadSettings();
	}

	/**
	 * 
	 */
	private void loadSettings() {
		File dir = new File(this.getExtendedClasspath() + File.separator + "descriptors" + File.separator);
		FilenameFilter filter = new FilenameFilter() {

			public boolean accept(File dir, String name) {
				return name.endsWith(".log-config");
			} 
		};

		File[] descriptors = dir.listFiles(filter);
		if (descriptors != null) {
			for (int i = 0; i < descriptors.length; i++) {
				loadDescriptor(descriptors, i);
			}
		}
	}

	/**
	 * @param descriptors
	 * @param i
	 */
	private void loadDescriptor(File[] descriptors, int i) {
		File file = descriptors[i];
		FileInputStream fstream = null;
		DataInputStream in = null;
		BufferedReader br = null;
		try {
			fstream = new FileInputStream(file);
			in = new DataInputStream(fstream);
			br = new BufferedReader(new InputStreamReader(in));
			String strLine = null;
			
			while ((strLine = br.readLine()) != null) {
				putValues(strLine);
			}
			
		}catch (IOException e) {
			LOGGER.error("Ocurrio un error leyendo los descriptores de contextos", e);
		}finally {
			closeAll(br, in, fstream);
		}
	}

	/**
	 * @param strLine
	 */
	private void putValues(String strLine) {
		String[] content = strLine.split(",");
		this.getJarNames().put(content[0], content[1]);
		this.getContextFileNames().put(content[0], content[2]);
	}

	/**
	 * @param br
	 * @param in
	 * @param fstream
	 */
	private void closeAll(BufferedReader br, DataInputStream in, FileInputStream fstream) {
		try {
			br.close();
			in.close();
			fstream.close();
		}catch (Exception e) {
			LOGGER.error("Ocurrio un error cerrando las conexiones:" + e.getMessage(), e);
		}
	}

	/**
	 * Levanta el JAR y el contexto de un processor dado por el ID No valida que
	 * el contexto haya sido levantado previamente.
	 * 
	 * @param id
	 */
	public void doResolve(String id) {
		StringBuilder pathBuilder = new StringBuilder();
		ClassLoader classLoader = null;
		try {
			pathBuilder.append(FILE_PREFIX).append(this.getExtendedClasspath()).append(File.separator).append(JAR_FOLDER).append(File.separator).append(this.getJarNames().get(id));
			classLoader = URLClassLoader.newInstance(new URL[] { new URL(pathBuilder.toString()) }, this.getClass().getClassLoader());

			pathBuilder.setLength(0);
			pathBuilder.append(FILE_PREFIX).append(this.getExtendedClasspath()).append(File.separator).append(CONTEXT_FOLDER).append(File.separator).append(this.getContextFileNames().get(id));

			FileSystemXmlApplicationContext context = new FileSystemXmlApplicationContext();
			context.setClassLoader(classLoader);
			context.setConfigLocation(pathBuilder.toString());
			context.afterPropertiesSet();

			RedLinkLoggingProcessor processor = (RedLinkLoggingProcessor) context.getBean(this.getProcessorBeanName());
			this.resolvedProcessors.put(id, processor);

		}
		catch (Exception e) {
			LOGGER.error("Ocurrio un error importando el archivo de contexto",e);
		}
	}

	/**
	 * @return the extendedClasspath
	 */
	public String getExtendedClasspath() {
		return extendedClasspath;
	}

	/**
	 * @param extendedClasspath
	 *            the extendedClasspath to set
	 */
	public void setExtendedClasspath(String extendedClasspath) {
		this.extendedClasspath = extendedClasspath;
	}

	/**
	 * @return the jarNames
	 */
	public Map<String, String> getJarNames() {
		return jarNames;
	}

	/**
	 * @param jarNames
	 *            the jarNames to set
	 */
	public void setJarNames(Map<String, String> jarNames) {
		this.jarNames = jarNames;
	}

	/**
	 * @return the contextFileNames
	 */
	public Map<String, String> getContextFileNames() {
		return contextFileNames;
	}

	/**
	 * @param contextFileNames
	 *            the contextFileNames to set
	 */
	public void setContextFileNames(Map<String, String> contextFileNames) {
		this.contextFileNames = contextFileNames;
	}

	/**
	 * @return the processorBeanName
	 */
	public String getProcessorBeanName() {
		return processorBeanName;
	}

	/**
	 * @param processorBeanName
	 *            the processorBeanName to set
	 */
	public void setProcessorBeanName(String processorBeanName) {
		this.processorBeanName = processorBeanName;
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		RedLinkLoggingProcessorResolverImpl impl = new RedLinkLoggingProcessorResolverImpl();
		impl.setExtendedClasspath("/tmp/extended-classpath");
		impl.afterPropertiesSet();
	}

	/**
	 * @see ar.com.redlink.framework.logging.consumer.resolver.RedLinkLoggingProcessorResolver#allProcessors()
	 */
	@Override
	public Collection<RedLinkLoggingProcessor> allProcessors() throws RedLinkFrameworkException {
		return this.getResolvedProcessors().values();
	}
}
