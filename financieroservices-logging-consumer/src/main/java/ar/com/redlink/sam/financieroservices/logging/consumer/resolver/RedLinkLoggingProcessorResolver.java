/*
 * Red Link Argentina Ciudad Autónoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar Author: "Joaquín Díaz Vélez" Date: 17/04/2012 -
 * 23:50:05
 */

package ar.com.redlink.sam.financieroservices.logging.consumer.resolver;

import java.util.Collection;

import ar.com.redlink.framework.entities.exception.RedLinkFrameworkException;
import ar.com.redlink.framework.logging.processor.RedLinkLoggingProcessor;

/**
 * Interface que define el comportamiento de un processor resolver, para la
 * obtencion de un {@link RedLinkLoggingProcessor}.
 * 
 * @author "Joaquín Díaz Vélez"
 * 
 */
public interface RedLinkLoggingProcessorResolver {

	/**
	 * @return
	 * @throws RedLinkFrameworkException
	 */
	public Collection<RedLinkLoggingProcessor> allProcessors() throws RedLinkFrameworkException;

	/**
	 * @param processorID
	 * @return
	 * @throws RedLinkFrameworkException
	 */
	public RedLinkLoggingProcessor resolve(String processorID) throws RedLinkFrameworkException;
}
