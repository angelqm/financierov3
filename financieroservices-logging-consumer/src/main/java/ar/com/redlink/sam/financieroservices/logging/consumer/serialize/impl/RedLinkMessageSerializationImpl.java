/*
 * Red Link Argentina Ciudad Autónoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar Author: "Joaquín Díaz Vélez"
 */
package ar.com.redlink.sam.financieroservices.logging.consumer.serialize.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Layout;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;

import ar.com.redlink.framework.entities.RedLinkLogElement;
import ar.com.redlink.sam.financieroservices.logging.consumer.serialize.RedLinkMessageSerialization;

/**
 * Componente encargado de la serialización de RedLinkLogElement haciendo uso de log4j.
 * 
 * 
 * @author "Joaquín Díaz Vélez"
 * 
 */
public class RedLinkMessageSerializationImpl implements RedLinkMessageSerialization {

	private static final String DELIMITER = "	";

	private static final Logger LOG = Logger.getLogger(RedLinkMessageSerializationImpl.class);
	
	private Map<String, Logger> loggers = new ConcurrentHashMap<String, Logger>();

	private int maxBackupIndex;
	private String maxFileSize;
	private String conversionPattern;
	private String loggerPath;
	
	@Override
	public void performSerialization(RedLinkLogElement message) {
		if(null != message) {
			this.getLog(message).info(this.objectSerialize(message));
		}
	}

	private StringBuffer objectSerialize(RedLinkLogElement message) {
		StringBuffer oneLine = new StringBuffer();
		oneLine.append(message.getId());
		oneLine.append(DELIMITER);
		oneLine.append(message.getProducto());
		oneLine.append(DELIMITER);
		return this.processMap(oneLine, message.getProperties());		
	}

	private StringBuffer processMap(StringBuffer oneLine, Map<String,Serializable> properties) {
		Set<Entry<String,Serializable>> mapSet = properties.entrySet();
		for (Iterator<Entry<String,Serializable>> iterator = mapSet.iterator(); iterator.hasNext();) {
			Entry<String,Serializable> mapEntry = iterator.next();
			oneLine.append(mapEntry.getKey());
			oneLine.append("=");
			oneLine.append(mapEntry.getValue());
			oneLine.append(DELIMITER);
		}
		if(oneLine.length() > 0) {
			oneLine.deleteCharAt(oneLine.length()-1);
		}
		return oneLine;
	}

	public Logger getLog(RedLinkLogElement message) {
		if (!this.loggers.containsKey(message.processorID())) {
			Logger logger = Logger.getLogger(message.processorID());
			Layout layout = new PatternLayout(this.getConversionPattern());
			RollingFileAppender rfa;
			try {
				rfa = new RollingFileAppender(layout, this.getLoggerPath() + message.processorID() + ".log");
				rfa.setMaxFileSize(this.getMaxFileSize());
				rfa.setMaxBackupIndex(this.getMaxBackupIndex());

				logger.setAdditivity(false);
				logger.addAppender(rfa);
				this.loggers.put(message.processorID(), logger);
			} 
			catch (IOException e) {
				LOG.error("No se pudo crear el LOGGER para el processor:"+message.processorID()+". Error original:"+e.getMessage(),e);
			}
			return logger;
		} else {
			return this.getLoggers().get(message.processorID());
		}
	}

	/**
	 * @return the loggers
	 */
	public Map<String, Logger> getLoggers() {
		return loggers;
	}

	/**
	 * @param loggers the loggers to set
	 */
	public void setLoggers(Map<String, Logger> loggers) {
		this.loggers = loggers;
	}

	/**
	 * @return the maxBackupIndex
	 */
	public int getMaxBackupIndex() {
		return maxBackupIndex;
	}

	/**
	 * @param maxBackupIndex the maxBackupIndex to set
	 */
	public void setMaxBackupIndex(int maxBackupIndex) {
		this.maxBackupIndex = maxBackupIndex;
	}

	/**
	 * @return the maxFileSize
	 */
	public String getMaxFileSize() {
		return maxFileSize;
	}

	/**
	 * @param maxFileSize the maxFileSize to set
	 */
	public void setMaxFileSize(String maxFileSize) {
		this.maxFileSize = maxFileSize;
	}

	/**
	 * @return the conversionPattern
	 */
	public String getConversionPattern() {
		return conversionPattern;
	}

	/**
	 * @param conversionPattern the conversionPattern to set
	 */
	public void setConversionPattern(String conversionPattern) {
		this.conversionPattern = conversionPattern;
	}

	/**
	 * @return the loggerPath
	 */
	public String getLoggerPath() {
		return loggerPath;
	}

	/**
	 * @param loggerPath the loggerPath to set
	 */
	public void setLoggerPath(String loggerPath) {
		this.loggerPath = loggerPath;
	}

}
