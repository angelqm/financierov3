/*
 * Red Link Argentina Ciudad Autónoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar Author: "Joaquín Díaz Vélez" Date: 17/04/2012 -
 * 23:46:22
 */

package ar.com.redlink.sam.financieroservices.logging.consumer.jms;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;

import ar.com.redlink.framework.entities.RedLinkLogElement;
import ar.com.redlink.framework.entities.exception.RedLinkFrameworkException;
import ar.com.redlink.sam.financieroservices.logging.consumer.RedLinkLogElementConsumer;

/**
 * @author "Joaquín Díaz Vélez"
 * 
 */
public class RedLinkJmsLogElementConsumer implements RedLinkLogElementConsumer, ExceptionListener, InitializingBean {

	private static final Logger LOGGER = Logger.getLogger(RedLinkJmsLogElementConsumer.class);

	/**
	 * Session de JMS. Utilizada para cacheo de sesiones. No existe problemas de
	 * concurrencia ya que todas las instancias utilizan la misma sesion.
	 */
	private Session session;

	/**
	 * Cola destino de JMS. Utilizada para cacheo de destinos. No existe
	 * problemas de concurrencia ya que todas las instancias utilizan la misma
	 * cola.
	 */
	private Destination destination;

	/**
	 * Conexion de JMS. Utilizada para cacheo de conexiones. No existe problemas
	 * de concurrencia ya que todas las instancias utilizan la misma conexion.
	 */
	private Connection connection;

	/**
	 * ConecftionFactory en desde kla cual generar la conexion, sesiones y
	 * destination.
	 */
	private ConnectionFactory connectionFactory;

	/**
	 * Producer de la cola de mensajes.
	 */
	private MessageConsumer consumer;

	/**
	 * Nombre de la cola de mensajes a utilizar.
	 */
	private String destinationName;

	/**
	 * Mode de acknowledge de los mensajes consumidos de la cola.
	 */
	private int sessionAcknowledgeMode = Session.AUTO_ACKNOWLEDGE;

	/**
	 * Indica si la sesion tiene que estar involucrada en una transaccion o no.
	 * No tiene importancia ante una transaccion distribuida.
	 */
	private boolean sessionTransacted = false;

	/**
	 * 
	 */
	public RedLinkJmsLogElementConsumer() {

	}

	/**
	 * @see javax.jms.ExceptionListener#onException(javax.jms.JMSException)
	 */
	@Override
	public void onException(JMSException arg0) {
		handleException(arg0);
	}

	/**
	 * @param e
	 */
	private void handleException(Throwable e) {
		try {
			this.close();
			this.initialize();
		}
		catch (RedLinkFrameworkException e1) {
			LOGGER.warn("Ocurrio una excepcion cerrando y volviendo a abrir las conexiones",e1);
		}
	}

	/**
	 * @return the session
	 */
	public Session getSession() {
		return session;
	}

	/**
	 * @param session
	 *            the session to set
	 */
	public void setSession(Session session) {
		this.session = session;
	}

	/**
	 * @return the destination
	 */
	public Destination getDestination() {
		return destination;
	}

	/**
	 * @param destination
	 *            the destination to set
	 */
	public void setDestination(Destination destination) {
		this.destination = destination;
	}

	/**
	 * @return the connection
	 */
	public Connection getConnection() {
		return connection;
	}

	/**
	 * @param connection
	 *            the connection to set
	 */
	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	/**
	 * @return the connectionFactory
	 */
	public ConnectionFactory getConnectionFactory() {
		return connectionFactory;
	}

	/**
	 * @param connectionFactory
	 *            the connectionFactory to set
	 */
	public void setConnectionFactory(ConnectionFactory connectionFactory) {
		this.connectionFactory = connectionFactory;
	}

	/**
	 * @return the destinationName
	 */
	public String getDestinationName() {
		return destinationName;
	}

	/**
	 * @param destinationName
	 *            the destinationName to set
	 */
	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}

	/**
	 * @return the sessionAcknowledgeMode
	 */
	public int getSessionAcknowledgeMode() {
		return sessionAcknowledgeMode;
	}

	/**
	 * @param sessionAcknowledgeMode
	 *            the sessionAcknowledgeMode to set
	 */
	public void setSessionAcknowledgeMode(int sessionAcknowledgeMode) {
		this.sessionAcknowledgeMode = sessionAcknowledgeMode;
	}

	/**
	 * @return the sessionTransacted
	 */
	public boolean isSessionTransacted() {
		return sessionTransacted;
	}

	/**
	 * @param sessionTransacted
	 *            the sessionTransacted to set
	 */
	public void setSessionTransacted(boolean sessionTransacted) {
		this.sessionTransacted = sessionTransacted;
	}

	/**
	 * @return the consumer
	 */
	public MessageConsumer getConsumer() {
		return consumer;
	}

	/**
	 * @param consumer
	 *            the consumer to set
	 */
	public void setConsumer(MessageConsumer consumer) {
		this.consumer = consumer;
	}

	/**
	 * 
	 */
	public void close() {
		close(this.consumer);
		close(this.session);
		close(this.connection);
		this.consumer = null;
		this.session = null;
		this.connection = null;
	}

	/**
	 * @throws RedLinkFrameworkException
	 */
	public void initialize() throws RedLinkFrameworkException {
		try {
			this.connection = this.connectionFactory.createConnection();
			this.session = this.connection.createSession(this.isSessionTransacted(), this.getSessionAcknowledgeMode());
			this.destination = this.createDestination();
			this.consumer = this.session.createConsumer(this.getDestination());
			this.connection.setExceptionListener(this);
			this.connection.start();
		}catch (JMSException e) {
			throw new RedLinkFrameworkException("No se pudo inicializar la conexion JMS. No estará disponible el log",e);
		}
	}

	/**
	 * @return
	 * @throws JMSException
	 */
	private Destination createDestination() throws JMSException {
		if(null == this.destination) {
			// si no esta inyectada el destination, lo creamos a 
			// traves de la session
			return this.session.createQueue(this.getDestinationName());
		}
		// si no se asume que ya esta asignada
		return this.destination;
	}

	/**
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		initialize();
	}

	/**
	 * @see ar.com.redlink.soat.logging.consumer.consumer.RedLinkLogElementConsumer#receive(long)
	 */
	@Override
	public RedLinkLogElement receive(long timeout) throws RedLinkFrameworkException {
		RedLinkLogElement logElement = null;
		try {
			ObjectMessage message = (ObjectMessage) this.consumer.receive(timeout);
			if(null != message) {
				logElement = (RedLinkLogElement) message.getObject();
			}
			return logElement;
		}catch (JMSException e) {
			throw new RedLinkFrameworkException(e);
		}
	}

	/**
	 * @param connectionToBeClosed
	 */
	public void close(Connection connectionToBeClosed) {
		try {
			if (null != connectionToBeClosed) {
				connectionToBeClosed.close();
			}
		}catch (JMSException e) {
			LOGGER.warn("Ocurrio una excepcion cerrando la conexion. Se descarta.",e);
		}
	}

	/**
	 * @param sessionToBeClosed
	 */
	public void close(Session sessionToBeClosed) {
		try {
			if (null != sessionToBeClosed) {
				if(sessionToBeClosed.getTransacted()) {
					sessionToBeClosed.commit();
				}
				sessionToBeClosed.close();
			}
		}catch (JMSException e) {
			LOGGER.warn("Ocurrio una excepcion cerrando la session. Se descarta.",e);
		}
	}

	/**
	 * @param consumerToBeClosed
	 */
	public void close(MessageConsumer consumerToBeClosed) {
		try {
			if (null != consumerToBeClosed) {
				consumerToBeClosed.close();
			}
		}catch (JMSException e) {
			LOGGER.warn("Ocurrio una excepcion cerrando el MessageConsumer. Se descarta.",e);
		}
	}

	/**
	 * 
	 */
	public void rollback() {
		try {
			this.session.rollback();
			this.close();
			
		}catch(JMSException e) {
			LOGGER.error("Se encontro un error haciendo rollback de la Session. Se descarta",e);
		}
	}

}
