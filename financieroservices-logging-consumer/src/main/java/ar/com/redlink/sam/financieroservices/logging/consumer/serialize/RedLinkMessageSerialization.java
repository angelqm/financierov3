/*
 * Red Link Argentina Ciudad Autónoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar Author: "Joaquín Díaz Vélez"
 */
package ar.com.redlink.sam.financieroservices.logging.consumer.serialize;

import ar.com.redlink.framework.entities.RedLinkLogElement;

/**
 * Interface que serializa los mensajes que no han sido procesados.
 * 
 * 
 * @author "Joaquín Díaz Vélez"
 * 
 */

public interface RedLinkMessageSerialization {

	/**
	 * @param message
	 */
	public void performSerialization(RedLinkLogElement message);
}
