package ar.com.redlink.framework.rest.client.factory;

import java.util.ArrayList;
import java.util.List;

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.interceptor.Interceptor;
import org.apache.cxf.jaxrs.client.ClientConfiguration;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.message.Message;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;

import ar.com.redlink.framework.rest.common.RedLinkRESTService;

/**
 * Similar a {@link RedLinkRSFactoryBean}, pero agregando la capacidad de
 * definir interceptores de entrada y salida.
 * 
 * @author Ivan Dackiewicz <ivan.dackiewicz@fluxit.com.ar>
 *
 */
public class ExtendedRedLinkRSFactoryBean extends RedLinkRSFactoryBean {
		
	private int timeout;
	
	private boolean disableCNCheck;
	private String secureSocketProtocol;
	private boolean useHttps;
	
	private List<Interceptor<? extends Message>> inInterceptors= new ArrayList<>();
	private List<Interceptor<? extends Message>> outInterceptors= new ArrayList<>();

	@Override
	public RedLinkRESTService getObject() throws Exception {
		RedLinkRESTService service = super.getObject();
		this.configureTLS(service);
		ClientConfiguration config = WebClient.getConfig(service);
		config.getInInterceptors().addAll(getInInterceptors());
		config.getOutInterceptors().addAll(getOutInterceptors());

		HTTPClientPolicy policy = new HTTPClientPolicy();
		policy.setConnectionTimeout(Integer.valueOf(timeout));
		policy.setReceiveTimeout(Integer.valueOf(timeout));
		policy.setAllowChunking(false);

		config.getHttpConduit().setClient(policy);
		
		return service;
	}

	private void configureTLS(RedLinkRESTService service) {
		ClientConfiguration config = WebClient.getConfig(service);
		HTTPConduit http = (HTTPConduit) config.getConduit();
		http.setTlsClientParameters(this.getTLSClientParameters());
		if (this.isDisableCNCheck()){
			http.getTlsClientParameters().setSecureSocketProtocol(this.getSecureSocketProtocol());
		}

		
	}

	/**
	 * Retorna los parametros de TLS para el cliente.
	 * 
	 * @return {@link TLSClientParameters}.
	 */
	private TLSClientParameters getTLSClientParameters() {
		final TLSClientParameters tlsCP = new TLSClientParameters();
		tlsCP.setUseHttpsURLConnectionDefaultSslSocketFactory(this.isUseHttps());
		tlsCP.setDisableCNCheck(this.isDisableCNCheck());
	
		
		return tlsCP;
	}
	
	public List<Interceptor<? extends Message>> getOutInterceptors() {
		return outInterceptors;
	}

	public void setOutInterceptors(List<Interceptor<? extends Message>> outInterceptors) {
		this.outInterceptors = outInterceptors;
	}

	public List<Interceptor<? extends Message>> getInInterceptors() {
		return inInterceptors;
	}

	public void setInInterceptors(List<Interceptor<? extends Message>> inInterceptors) {
		this.inInterceptors = inInterceptors;
	}

	/**
	 * @return
	 */
	public String getSecureSocketProtocol() {
		return secureSocketProtocol;
	}
	
	/**
	 * @param secureSocketProtocol
	 */
	public void setSecureSocketProtocol(String secureSocketProtocol) {
		this.secureSocketProtocol = secureSocketProtocol;
	}
	
	public boolean isDisableCNCheck() {
		return disableCNCheck;
	}

	public void setDisableCNCheck(boolean disableCNCheck) {
		this.disableCNCheck = disableCNCheck;
	}

	public boolean isUseHttps() {
		return useHttps;
	}

	public void setUseHttps(boolean useHttps) {
		this.useHttps = useHttps;
	}

	/**
	 * @return the timeout
	 */
	public int getTimeout() {
		return timeout;
	}

	/**
	 * @param timeout the timeout to set
	 */
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}	
}
