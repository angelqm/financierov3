package ar.com.redlink.framework.rest.client.factory;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.Phase;
import org.apache.log4j.Logger;

import ar.com.redlink.framework.crypto.service.RedLinkCryptoService;

/**
 * Interceptor para agregar las credenciales del usuario de servicio en cada request saliente
 * 
 * @author Juan Pablo Nievas
 * @author RedLink S.A.
 *
 */
public class ClientHeaderInterceptor extends org.apache.cxf.phase.AbstractPhaseInterceptor<Message> {

	private static final Logger LOGGER = Logger.getLogger(ClientHeaderInterceptor.class);

	private RedLinkCryptoService cryptoService;	

	private String username;
	private String password;

	public ClientHeaderInterceptor() {
		super(Phase.WRITE);
	}

	private List<String> routesToIgnore = new ArrayList<>();

	@Override
	public void handleMessage(Message message) throws Fault {
		try {
			String uri = message.get("org.apache.cxf.request.uri").toString();
			String method = message.get("org.apache.cxf.request.method").toString();
			LOGGER.info("Calling: " + uri + " - method: " + method);
			if (!hasToIgnore(uri, method)) {
				LOGGER.info("Building out headers: " + uri + " - method: " + method);
				@SuppressWarnings("unchecked")
				Map<String, List<String>> outHeaders = (Map<String, List<String>>) message
						.get(Message.PROTOCOL_HEADERS);
				if (outHeaders == null) {
					outHeaders = new HashMap<>();
					message.put(Message.PROTOCOL_HEADERS, outHeaders);
				}
				String credenciales = this.generateCredenciales();
				LOGGER.info("About to call with user credentials: " + credenciales);
				List<String> list = new ArrayList<String>();
				list.add(credenciales);
				// Copy Custom HTTP header on the response
				outHeaders.put("credenciales", list);
				LOGGER.info("Calling with user credentials: " + credenciales);
			}
		} catch (Exception e) {
			LOGGER.error("Error handling message", e);
			throw new Fault(e);
		}
	}

	private String generateCredenciales() {
		
//		String usernameCrypt = this.getCryptoService().crypt("ENLACEPAGOS_SAM_FIN");
//		String passwordCrypt = this.getCryptoService().crypt("Redlink*9");
//		String username = this.getCryptoService().decrypt(usernameCrypt);
//		String password = this.getCryptoService().decrypt(passwordCrypt);
		 
		String username = this.getCryptoService().decrypt(this.getUsername());
		String password = this.getCryptoService().decrypt(this.getPassword());
		
		byte[] bytes = (username + ":" + password).getBytes();
		String credenciales = Base64.getEncoder().encodeToString(bytes);
		LOGGER.debug("Credenciales: " + credenciales);
		return credenciales;
	}

	private boolean hasToIgnore(String uri, String method) {
		for (String route : getRoutesToIgnore()) {
			String[] route_method = route.split("\\|");
			if (uri.endsWith(route_method[0]) && method.equals(route_method[1])) {
				LOGGER.info("Ignoring: " + uri + " - method: " + method);
				return true;
			}
		}
		LOGGER.info("Not Ignoring: " + uri + " - method: " + method);
		return false;
	}

	public List<String> getRoutesToIgnore() {
		return routesToIgnore;
	}

	public void setRoutesToIgnore(List<String> routesToIgnore) {
		this.routesToIgnore = routesToIgnore;
	}

	/**
	 * @return cryptoService
	 */
	public RedLinkCryptoService getCryptoService() {
		return cryptoService;
	}

	/**
	 * @param cryptoService
	 *            cryptoService a asignar
	 */
	public void setCryptoService(RedLinkCryptoService cryptoService) {
		this.cryptoService = cryptoService;
	}

	/**
	 * @return user
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param user
	 *            user a asignar
	 */
	public void setUsername(String user) {
		this.username = user;
	}

	/**
	 * @return password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            password a asignar
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}