package ar.com.redlink.sam.financieroservices.pei.service.utils;

import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import ar.com.redlink.framework.rest.common.exception.RedLinkRESTServiceException;
import ar.com.redlink.sam.financieroservices.pei.response.PeiErrorResponse;
import ar.com.redlink.sam.financieroservices.service.enums.PeiFinancieroMsgsEnums;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroPeiException;

public class ServiceUtils {

	private static final Logger LOGGER = Logger.getLogger(ServiceUtils.class);

	public static PeiErrorResponse getEntityFromErrorResponse(Response r) throws Exception {

		PeiErrorResponse resp = null;
		try {

			String responseBody = r.readEntity(String.class);
			JsonElement jelement = new JsonParser().parse(responseBody);
			JsonObject jobject = jelement.getAsJsonObject();

			resp = new PeiErrorResponse();

			if (jobject.get("codigo") != null) {
				resp.setCodigoPei(jobject.get("codigo").toString().replaceAll("\"", ""));
			}
			if (jobject.get("descripcion") == null || jobject.get("descripcion").toString().equals("null")) {
				resp.setDescripcionPei(null);
			} else {
				resp.setDescripcionPei(jobject.get("descripcion").toString().replaceAll("\"", ""));
			}

		} catch (Exception ex) {
			throw new Exception(ex);
		}

		return resp;

	}

	/**
	 * Genera mensaje de error PEI
	 * 
	 * @param e
	 * @throws FinancieroException
	 */
	public static void throwFinaExc(RedLinkRESTServiceException e) throws FinancieroException {

		PeiErrorResponse peiErrorResponse = null;

		try {

			peiErrorResponse =
			ServiceUtils.getEntityFromErrorResponse(e.getResponse());

			peiErrorResponse.setCodigo(PeiFinancieroMsgsEnums.ERRORGENERICO3.getFinaEnum().getCode());
			peiErrorResponse.setDescripcion(PeiFinancieroMsgsEnums.ERRORGENERICO3.getMsg());

		} catch (Exception e1) {

			String errorNumber = "";
			if (e.getResponse() != null) {
				errorNumber = String.valueOf(e.getResponse().getStatus());
			}
			LOGGER.error("No se pudo extraer el objeto de respuesta: error " + errorNumber);
			
			if(peiErrorResponse!=null){
				throw new FinancieroException(peiErrorResponse.getCodigo(), peiErrorResponse.getDescripcion() + ". " + errorNumber);
			}
			else{
				throw new FinancieroException("FINA-600", "Url no encontrado" + ". " + errorNumber);
			}
		}

		if (peiErrorResponse.getCodigoPei() != null && !peiErrorResponse.getCodigoPei().isEmpty()) {

			if (PeiFinancieroMsgsEnums.ERRORGENERICO1.getFinaEnum().getMsg().equals(peiErrorResponse.getCodigoPei())) {

				llenarMensajeGenericoPei(peiErrorResponse);

			} else {

				PeiFinancieroMsgsEnums finaEnum = findPeiFinancieroMsgsEnums(peiErrorResponse.getCodigoPei());

				if (finaEnum != null) {

					peiErrorResponse.setCodigo(finaEnum.getFinaEnum().getCode());
					peiErrorResponse.setDescripcion(String.format(finaEnum.getMsg(), finaEnum.getFinaEnum().getMsg()));

				} else {

					llenarMensajeGenericoPei(peiErrorResponse);

				}

			}
			
		}

		throw new FinancieroPeiException(peiErrorResponse.getCodigo(), peiErrorResponse.getDescripcion(),
				peiErrorResponse.getCodigoPei(), peiErrorResponse.getDescripcionPei());
	}

	/**
	 * Crea una respuesta generica
	 * 
	 * @param errorPei
	 */
	private static void llenarMensajeGenericoPei(PeiErrorResponse peiErrorResponse) {

		if (peiErrorResponse.getDescripcionPei() == null || peiErrorResponse.getDescripcionPei().isEmpty()) {

			peiErrorResponse.setCodigo(PeiFinancieroMsgsEnums.ERRORGENERICO2.getFinaEnum().getCode());
			peiErrorResponse.setDescripcion(
					String.format(PeiFinancieroMsgsEnums.ERRORGENERICO2.getMsg(), peiErrorResponse.getCodigoPei()));

		} else {

			peiErrorResponse.setCodigo(PeiFinancieroMsgsEnums.ERRORGENERICO1.getFinaEnum().getCode());
			peiErrorResponse.setDescripcion(String.format(PeiFinancieroMsgsEnums.ERRORGENERICO1.getMsg(),
					peiErrorResponse.getDescripcionPei(), peiErrorResponse.getCodigoPei()));

		}

	}

	/**
	 * Busca si existe un código de error conocido
	 * 
	 * @param codigo
	 * @return
	 */
	private static PeiFinancieroMsgsEnums findPeiFinancieroMsgsEnums(String codigo) {

		for (PeiFinancieroMsgsEnums finaEnum : PeiFinancieroMsgsEnums.values()) {
			if (finaEnum.getFinaEnum().getMsg().equals(codigo)) {
				return finaEnum;
			}
		}

		return null;
	}

}
