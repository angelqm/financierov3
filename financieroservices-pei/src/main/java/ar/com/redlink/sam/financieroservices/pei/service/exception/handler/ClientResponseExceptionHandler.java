package ar.com.redlink.sam.financieroservices.pei.service.exception.handler;

import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.client.ResponseExceptionMapper;

import ar.com.redlink.framework.rest.common.exception.RedLinkRESTServiceException;

public class ClientResponseExceptionHandler implements ResponseExceptionMapper<RedLinkRESTServiceException> {

	@Override
	public RedLinkRESTServiceException fromResponse(Response r) {		
		return new RedLinkRESTServiceException(r.getHeaderString("exception-message"), r, new Exception());
	}
	
}
