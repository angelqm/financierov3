package ar.com.redlink.sam.financieroservices.pei.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author EXT-IBANEZMA
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RealizarDevolucionPEIResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String tipoOperacion;
	private String estado;
	private String fecha;
	private String idOperacion;
	private Integer idOperacionOrigen;
	private String numeroReferenciaBancaria; 

	/**
	 * @return the tipoOperacion
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}

	/**
	 * @param tipoOperacion the tipoOperacion to set
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado
	 *            the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha
	 *            the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the idOperacion
	 */
	public String getIdOperacion() {
		return idOperacion;
	}

	/**
	 * @param idOperacion the idOperacion to set
	 */
	public void setIdOperacion(String idOperacion) {
		this.idOperacion = idOperacion;
	}

	/**
	 * @return the idOperacionOrigen
	 */
	public Integer getIdOperacionOrigen() {
		return idOperacionOrigen;
	}

	/**
	 * @param idOperacionOrigen the idOperacionOrigen to set
	 */
	public void setIdOperacionOrigen(Integer idOperacionOrigen) {
		this.idOperacionOrigen = idOperacionOrigen;
	}

	public String getNumeroReferenciaBancaria() {
		return numeroReferenciaBancaria;
	}

	public void setNumeroReferenciaBancaria(String numeroReferenciaBancaria) {
		this.numeroReferenciaBancaria = numeroReferenciaBancaria;
	}
	
}
