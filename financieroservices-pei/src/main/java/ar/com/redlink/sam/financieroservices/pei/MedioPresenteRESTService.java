package ar.com.redlink.sam.financieroservices.pei;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import ar.com.redlink.framework.rest.common.RedLinkRESTService;
import ar.com.redlink.framework.rest.common.exception.RedLinkRESTServiceException;
import ar.com.redlink.sam.financieroservices.dto.request.ConsultaOperacionPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoParcialPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.pei.response.ConsultaOperacionPEIResponse;
import ar.com.redlink.sam.financieroservices.pei.response.RealizarDevolucionPEIResponse;
import ar.com.redlink.sam.financieroservices.pei.response.TransferenciaPEIResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Cliente REST Enlace pagos.
 * 
 * @author EXT-IBANEZMA
 *
 */
public interface MedioPresenteRESTService extends RedLinkRESTService {

	@Path("/bandamagnetica")
	@Consumes("application/json")
	@Produces("application/json")
	@POST
	@ApiOperation(value = "Persiste un objeto", notes = "Retorna el id del objeto persistido.")
	public TransferenciaPEIResponse pagar(@HeaderParam("requerimiento") String requerimiento,
			@HeaderParam("cliente") String cliente,
			@ApiParam(value = "Objeto a persistir", required = true) RealizarTransferenciaPEIRequestDTO object)
			throws RedLinkRESTServiceException;

	@Path("/bandamagnetica/consulta/{idComercio}")
	@Consumes("application/json")
	@Produces("application/json")
	@POST
	public ConsultaOperacionPEIResponse consultar(@PathParam("idComercio") String idComercio,
			@HeaderParam("requerimiento") String requerimiento, @HeaderParam("cliente") String cliente,
			@ApiParam(value = "Objeto a persistir", required = true) ConsultaOperacionPEIRequestDTO object)
			throws RedLinkRESTServiceException;

	@Path("/bandamagnetica/devolucion/total")
	@Consumes("application/json")
	@Produces("application/json")
	@POST
	public RealizarDevolucionPEIResponse devolucion(@HeaderParam("requerimiento") String requerimiento,
			@HeaderParam("cliente") String cliente,
			@ApiParam(value = "Objeto a persistir", required = true) RealizarDevolucionPagoPEIRequestDTO object)
			throws RedLinkRESTServiceException;

	@Path("/bandamagnetica/devolucion/parcial")
	@Consumes("application/json")
	@Produces("application/json")
	@POST
	public RealizarDevolucionPEIResponse devolucionParcial(@HeaderParam("requerimiento") String requerimiento,
			@HeaderParam("cliente") String cliente,
			@ApiParam(value = "Objeto a persistir", required = true) RealizarDevolucionPagoParcialPEIRequestDTO object)
			throws RedLinkRESTServiceException;
}
