package ar.com.redlink.sam.financieroservices.pei.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PeiErrorResponse {

	private String codigo;
	private String descripcion;
	private String codigoPei;
	private String descripcionPei;

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the codigoPei
	 */
	public String getCodigoPei() {
		return codigoPei;
	}

	/**
	 * @param codigoPei
	 *            the codigoPei to set
	 */
	public void setCodigoPei(String codigoPei) {
		this.codigoPei = codigoPei;
	}

	/**
	 * @return the descripcionPei
	 */
	public String getDescripcionPei() {
		return descripcionPei;
	}

	/**
	 * @param descripcionPei
	 *            the descripcionPei to set
	 */
	public void setDescripcionPei(String descripcionPei) {
		this.descripcionPei = descripcionPei;
	}

}
