package ar.com.redlink.sam.financieroservices.pei.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author EXT-ibanezma
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionPEIResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
