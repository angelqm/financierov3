package ar.com.redlink.sam.financieroservices.pei.service;

import ar.com.redlink.framework.rest.common.exception.RedLinkRESTServiceException;
import ar.com.redlink.sam.financieroservices.dto.request.ConsultaOperacionPEIPagarRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ConsultaOperacionPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.HeaderPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoPEIPagarRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoParcialPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaPEIPagarRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.pei.response.ConsultaOperacionPEIResponse;
import ar.com.redlink.sam.financieroservices.pei.response.ConsultaOperacionPEIPagarResponse;
import ar.com.redlink.sam.financieroservices.pei.response.RealizarDevolucionPEIResponse;
import ar.com.redlink.sam.financieroservices.pei.response.TransferenciaPEIPagarResponse;
import ar.com.redlink.sam.financieroservices.pei.response.ConsultaTipoDocumentoPEIResponse;
import ar.com.redlink.sam.financieroservices.pei.response.RealizarDevolucionPEIPagarResponse;
import ar.com.redlink.sam.financieroservices.pei.response.TransferenciaPEIResponse;

/**
 * 
 * 
 * @author EXT-IBANEZMA
 *
 */
public interface PeiService {
	
	/**
	 * 
	 * @param request
	 * @return
	 * @throws RedLinkRESTServiceException
	 */
	TransferenciaPEIResponse realizaPago(HeaderPEIRequestDTO header, RealizarTransferenciaPEIRequestDTO request); //throws RedLinkRESTServiceException;
	
	/**
	 * 	
	 * @param token
	 * @param requestDTO
	 * @return
	 */
	ConsultaOperacionPEIResponse consultaOperaciones(HeaderPEIRequestDTO header, String comercioId, ConsultaOperacionPEIRequestDTO requestDTO);

	/**
	 * 	
	 * @param token
	 * @param requestDTO
	 * @return
	 * AQM PEI desde PAGAR
	 */
	//ConsultaOperacionPEIPagarResponse consultaOperacionesPEIPagar(HeaderPEIRequestDTO header, String comercioId, ConsultaOperacionPEIPagarRequestDTO requestDTO);
	ConsultaOperacionPEIPagarResponse consultaOperacionesPEIPagar(HeaderPEIRequestDTO header, ConsultaOperacionPEIPagarRequestDTO requestDTO);

	
	/**
	 * 
	 * @param header
	 * @param request
	 * @return
	 */
	RealizarDevolucionPEIResponse devolucion(HeaderPEIRequestDTO header, RealizarDevolucionPagoPEIRequestDTO request);
	
	/**
	 * 
	 * @param header
	 * @param request
	 * @return
	 */
	RealizarDevolucionPEIResponse devolucionParcial(HeaderPEIRequestDTO header, RealizarDevolucionPagoParcialPEIRequestDTO request);
	
	/**
	 * 
	 * @param header
	 * @param request
	 * @return
	 * AQM PEI desde PAGAR
	 */
	RealizarDevolucionPEIPagarResponse devolucionPEIPagar(HeaderPEIRequestDTO header, RealizarDevolucionPagoPEIPagarRequestDTO request);
	
	/**
	 * 
	 * @param request
	 * @return
	 * AQM PEI desde PAGAR
	 * @throws RedLinkRESTServiceException
	 */
	TransferenciaPEIPagarResponse realizaPagoPEIPagar(HeaderPEIRequestDTO header, RealizarTransferenciaPEIPagarRequestDTO request);
	
	/**
	 * 
	 * @param request
	 * @return
	 * @throws RedLinkRESTServiceException
	 */
	ConsultaTipoDocumentoPEIResponse tipoDocumento(HeaderPEIRequestDTO header);
	
	
}
