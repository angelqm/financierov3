package ar.com.redlink.sam.financieroservices.pei.response;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author EXT-IBANEZMA
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsultaOperacionPEIPagarResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer total;

	private List<OperacionesPEIResponse> resultado;

	/**
	 * @return the total
	 */
	public Integer getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(Integer total) {
		this.total = total;
	}

	/**
	 * @return the resultado
	 */
	public List<OperacionesPEIResponse> getResultado() {
		return resultado;
	}

	/**
	 * @param resultado
	 *            the resultado to set
	 */
	public void setResultado(List<OperacionesPEIResponse> resultado) {
		this.resultado = resultado;
	}

}
