package ar.com.redlink.sam.financieroservices.pei.service.mock;

import java.io.Serializable;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import ar.com.redlink.framework.rest.common.exception.RedLinkRESTServiceException;
import ar.com.redlink.sam.financieroservices.dto.request.ConsultaOperacionPEIPagarRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ConsultaOperacionPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.HeaderPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoPEIPagarRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoParcialPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaPEIPagarRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.pei.response.ConsultaOperacionPEIPagarResponse;
import ar.com.redlink.sam.financieroservices.pei.response.ConsultaOperacionPEIResponse;
import ar.com.redlink.sam.financieroservices.pei.response.ConsultaTipoDocumentoPEIResponse;
import ar.com.redlink.sam.financieroservices.pei.response.OperacionesPEIResponse;
import ar.com.redlink.sam.financieroservices.pei.response.RealizarDevolucionPEIPagarResponse;
import ar.com.redlink.sam.financieroservices.pei.response.RealizarDevolucionPEIResponse;
import ar.com.redlink.sam.financieroservices.pei.response.TransferenciaPEIPagarResponse;
import ar.com.redlink.sam.financieroservices.pei.response.TransferenciaPEIResponse;
import ar.com.redlink.sam.financieroservices.pei.service.PeiService;

/**
 * Cliente REST MOCK
 * 
 * @author EXT-IBANEZMA
 *
 */
public class PeiMOCKServiceImpl implements PeiService {
	
	private static final String REQUEST = "REQUEST: ";	
	private static final String INVOCACION_A_ENLACEPAGOS = "--------------MOCK ENLACEPAGOS MOCK: ";
	private static final String INVOCACION_A_ENLACEPAGOS_PAGAR = "--------------MOCK ENLACEPAGOS(PAGAR): ";
	
	private static final Logger LOGGER = Logger.getLogger(PeiMOCKServiceImpl.class);
	
	@Override
	public TransferenciaPEIResponse realizaPago(HeaderPEIRequestDTO header, RealizarTransferenciaPEIRequestDTO requestDTO) throws RedLinkRESTServiceException {
		LOGGER.info(INVOCACION_A_ENLACEPAGOS + "realizarPago");
		if (LOGGER.isDebugEnabled()) {						
			LOGGER.debug(REQUEST + objectToJson(requestDTO));			
		}		
		TransferenciaPEIResponse response = new TransferenciaPEIResponse();
		response.setEstado("ACEPTADA");
		response.setFecha("2017-07-18T12:37:10Z");
		response.setIdOperacion("2738");
		response.setTipoOperacion("PAGO");		
		return response;				
	}	
	
	@Override
	public ConsultaOperacionPEIResponse consultaOperaciones(HeaderPEIRequestDTO header, String comercioId, ConsultaOperacionPEIRequestDTO requestDTO) {
		LOGGER.info(INVOCACION_A_ENLACEPAGOS + "consultaOperaciones");
		if (LOGGER.isDebugEnabled()) {			
			LOGGER.debug(REQUEST + objectToJson(requestDTO));
		}
		ConsultaOperacionPEIResponse response = new ConsultaOperacionPEIResponse();
		response.setTotal(0);
		response.setResultado(new ArrayList<OperacionesPEIResponse>());		
		return response;
	}
	
	@Override
	public RealizarDevolucionPEIResponse devolucion(HeaderPEIRequestDTO header, RealizarDevolucionPagoPEIRequestDTO request) {
		LOGGER.info(INVOCACION_A_ENLACEPAGOS + "devolucionTotal");
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(REQUEST + objectToJson(request));
		}
		RealizarDevolucionPEIResponse response = new RealizarDevolucionPEIResponse();		
		response.setEstado("ACEPTADA");
		response.setFecha("2017-07-18T12:37:10Z");
		response.setIdOperacion("2739");
		response.setTipoOperacion("DEVOLUCION");
		return response;
	}	

	/*AQM*/
	@Override
	public RealizarDevolucionPEIPagarResponse devolucionPEIPagar(HeaderPEIRequestDTO header, RealizarDevolucionPagoPEIPagarRequestDTO request) {
		LOGGER.info(INVOCACION_A_ENLACEPAGOS_PAGAR + "devolucionTotal");
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(REQUEST + objectToJson(request));
		}
		RealizarDevolucionPEIPagarResponse response = new RealizarDevolucionPEIPagarResponse();		
		response.setEstado("ACEPTADA");
		response.setFecha("2017-07-18T12:37:10Z");
		response.setIdOperacion("2739");
		response.setTipoOperacion("DEVOLUCION");
		return response;
	}		
	
	
	@Override
	public RealizarDevolucionPEIResponse devolucionParcial(HeaderPEIRequestDTO header, RealizarDevolucionPagoParcialPEIRequestDTO request) {
		LOGGER.info(INVOCACION_A_ENLACEPAGOS + "devolucionParcial");
		if (LOGGER.isDebugEnabled()) {			
			LOGGER.debug(REQUEST + objectToJson(request));
		}		
		RealizarDevolucionPEIResponse response = new RealizarDevolucionPEIResponse();		
		response.setEstado("ACEPTADA");
		response.setFecha("2017-07-18T12:37:10Z");
		response.setIdOperacion("2740");
		response.setTipoOperacion("DEVOLUCION");
		return response;
	}
	
	public ConsultaOperacionPEIPagarResponse consultaOperacionesPEIPagar(HeaderPEIRequestDTO header, ConsultaOperacionPEIPagarRequestDTO requestDTO) {
		// TODO Auto-generated method stub
		return null;
	}


	private String objectToJson(Serializable requestDTO) {
		Gson gson = new Gson();
		return gson.toJson(requestDTO);
	}

	@Override
	public TransferenciaPEIPagarResponse realizaPagoPEIPagar(HeaderPEIRequestDTO header,	RealizarTransferenciaPEIPagarRequestDTO request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ConsultaTipoDocumentoPEIResponse tipoDocumento(HeaderPEIRequestDTO header) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
