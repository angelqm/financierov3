package ar.com.redlink.sam.financieroservices.pei.response;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author he_e90104
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsultaTipoDocumentoPEIResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<TipoDocumentoPEIResponse> documentoTipos;

	/**
	 * @return the documentoTipos
	 */
	public List<TipoDocumentoPEIResponse> getDocumentoTipos() {
		return documentoTipos;
	}

	/**
	 * @param documentoTipos the documentoTipos to set
	 */
	public void setDocumentoTipos(List<TipoDocumentoPEIResponse> documentoTipos) {
		this.documentoTipos = documentoTipos;
	}
	
}
