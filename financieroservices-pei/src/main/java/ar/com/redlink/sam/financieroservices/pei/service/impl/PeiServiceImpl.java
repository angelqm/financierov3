package ar.com.redlink.sam.financieroservices.pei.service.impl;

import java.io.Serializable;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import ar.com.redlink.framework.rest.common.exception.RedLinkRESTServiceException;
import ar.com.redlink.sam.financieroservices.dto.request.ConsultaOperacionPEIPagarRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ConsultaOperacionPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.HeaderPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoPEIPagarRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoParcialPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaPEIPagarRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.pei.ConCuentaRecaudadoraRESTService;
import ar.com.redlink.sam.financieroservices.pei.MedioPresenteRESTService;
import ar.com.redlink.sam.financieroservices.pei.response.ConsultaOperacionPEIPagarResponse;
import ar.com.redlink.sam.financieroservices.pei.response.ConsultaOperacionPEIResponse;
import ar.com.redlink.sam.financieroservices.pei.response.RealizarDevolucionPEIResponse;
import ar.com.redlink.sam.financieroservices.pei.response.TransferenciaPEIPagarResponse;
import ar.com.redlink.sam.financieroservices.pei.response.ConsultaTipoDocumentoPEIResponse;
import ar.com.redlink.sam.financieroservices.pei.response.RealizarDevolucionPEIPagarResponse;
import ar.com.redlink.sam.financieroservices.pei.response.TransferenciaPEIResponse;
import ar.com.redlink.sam.financieroservices.pei.service.PeiService;

/**
 * Cliente REST para consumir de PEI Empesas
 * 
 * @author EXT-IBANEZMA
 *
 */
public class PeiServiceImpl implements PeiService {
	
	private static final String REQUEST = "REQUEST: ";	
	private static final String INVOCACION_A_ENLACEPAGOS = "INVOCACION A ENLACEPAGOS: ";
	private static final String INVOCACION_A_ENLACEPAGOS_PAGAR = "INVOCACION A ENLACEPAGOS(PAGAR): ";
	
	private static final Logger LOGGER = Logger.getLogger(PeiServiceImpl.class);
	
	private MedioPresenteRESTService medioPresenteRESTService;
	private ConCuentaRecaudadoraRESTService conCuentaRecaudadoraRESTService;
	
	
	@Override
	public TransferenciaPEIResponse realizaPago(HeaderPEIRequestDTO header, RealizarTransferenciaPEIRequestDTO requestDTO) throws RedLinkRESTServiceException {
		LOGGER.info(INVOCACION_A_ENLACEPAGOS + "realizarPago");
		if (LOGGER.isDebugEnabled()) {						
			LOGGER.debug(REQUEST + objectToJson(requestDTO));			
		}
		
		return this.getMedioPresenteRESTService().pagar(header.getRequerimiento(), header.getCliente(), requestDTO);				
	}	
	
	@Override
	public ConsultaOperacionPEIResponse consultaOperaciones(HeaderPEIRequestDTO header, String comercioId, ConsultaOperacionPEIRequestDTO requestDTO) {
		LOGGER.info(INVOCACION_A_ENLACEPAGOS + "consultaOperaciones");
		if (LOGGER.isDebugEnabled()) {			
			LOGGER.debug(REQUEST + objectToJson(requestDTO));
		}
		return this.getMedioPresenteRESTService().consultar(comercioId, header.getRequerimiento(), header.getCliente(), requestDTO);
	}
	
	@Override
	public ConsultaOperacionPEIPagarResponse consultaOperacionesPEIPagar(HeaderPEIRequestDTO header, ConsultaOperacionPEIPagarRequestDTO requestDTO) {
		LOGGER.info(INVOCACION_A_ENLACEPAGOS_PAGAR + "consultaOperacionesPEIPagar");
		if (LOGGER.isDebugEnabled()) {			
			LOGGER.debug(REQUEST + objectToJson(requestDTO));
		}
		return this.getConCuentaRecaudadoraRESTService().consultar(header.getRequerimiento(), header.getCliente(), requestDTO);
	}
		
	@Override
	public RealizarDevolucionPEIResponse devolucion(HeaderPEIRequestDTO header, RealizarDevolucionPagoPEIRequestDTO request) {
		LOGGER.info(INVOCACION_A_ENLACEPAGOS + "devolucionTotal");
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(REQUEST + objectToJson(request));
		}
		return this.getMedioPresenteRESTService().devolucion(header.getRequerimiento(), header.getCliente(), request);
	}	

	@Override
	public RealizarDevolucionPEIPagarResponse devolucionPEIPagar(HeaderPEIRequestDTO header, RealizarDevolucionPagoPEIPagarRequestDTO request) {		                                                                             
		LOGGER.info(INVOCACION_A_ENLACEPAGOS_PAGAR + "devolucionPEIPagarTotal");
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(REQUEST + objectToJson(request));
		}	
		return this.getConCuentaRecaudadoraRESTService().devolucionPEIPagar(header.getRequerimiento(), header.getCliente(), request);				
	}	

	@Override
	public RealizarDevolucionPEIResponse devolucionParcial(HeaderPEIRequestDTO header, RealizarDevolucionPagoParcialPEIRequestDTO request) {
		LOGGER.info(INVOCACION_A_ENLACEPAGOS + "devolucionParcial");
		if (LOGGER.isDebugEnabled()) {			
			LOGGER.debug(REQUEST + objectToJson(request));
		}
		return this.getMedioPresenteRESTService().devolucionParcial(header.getRequerimiento(), header.getCliente(), request);
	}
	
	@Override
	public TransferenciaPEIPagarResponse realizaPagoPEIPagar(HeaderPEIRequestDTO header, RealizarTransferenciaPEIPagarRequestDTO requestDTO) {
		LOGGER.info(INVOCACION_A_ENLACEPAGOS_PAGAR + "realizarPagoPEIPagar");
		if (LOGGER.isDebugEnabled()) {						
			LOGGER.debug(REQUEST + objectToJson(requestDTO));			
		}
		
		//LOGGER.info(this.getConCuentaRecaudadoraRESTService() );
		return this.getConCuentaRecaudadoraRESTService().pagar(header.getRequerimiento(), header.getCliente(), requestDTO);

	}
	
	@Override
	public ConsultaTipoDocumentoPEIResponse tipoDocumento(HeaderPEIRequestDTO header) {
		LOGGER.info(INVOCACION_A_ENLACEPAGOS_PAGAR + "tipoDocumento");
		if (LOGGER.isDebugEnabled()) {						
			LOGGER.debug(REQUEST +"cliente:" +header.getCliente()+", requerimiento:"+header.getRequerimiento());			
		}
		
		return this.getConCuentaRecaudadoraRESTService().tipoDocumento(header.getRequerimiento(), header.getCliente());
	}
	
	
	private String objectToJson(Serializable requestDTO) {
		Gson gson = new Gson();
		return gson.toJson(requestDTO);
	}

	public MedioPresenteRESTService getMedioPresenteRESTService() {
		return medioPresenteRESTService;
	}

	public void setMedioPresenteRESTService(MedioPresenteRESTService medioPresenteRESTService) {
		this.medioPresenteRESTService = medioPresenteRESTService;
	}

	/**
	 * @return the conCuentaRecaudadoraRESTService
	 */
	public ConCuentaRecaudadoraRESTService getConCuentaRecaudadoraRESTService() {
		return conCuentaRecaudadoraRESTService;
	}

	/**
	 * @param conCuentaRecaudadoraRESTService the conCuentaRecaudadoraRESTService to set
	 */
	public void setConCuentaRecaudadoraRESTService(ConCuentaRecaudadoraRESTService conCuentaRecaudadoraRESTService) {
		this.conCuentaRecaudadoraRESTService = conCuentaRecaudadoraRESTService;
	}

	
	
}
