package ar.com.redlink.sam.financieroservices.pei.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Representacion de una respuesta a la transaccion de transferencia PEI desde PAGAR
 * 
 * @author AQM
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransferenciaPEIPagarResponse implements Serializable {

	private static final long serialVersionUID = -6891418741053189035L;
	private String idOperacion;
	private String idOperacionOrigen;
	private String numeroReferenciaBancaria;
	private String tipoOperacion;
	private String fecha;
	private String estado;
	private String terminalPEI;

	
	/**
	 * @return the idOperacion
	 */
	public String getIdOperacion() {
		return idOperacion;
	}
	/**
	 * @param idOperacion the idOperacion to set
	 */
	public void setIdOperacion(String idOperacion) {
		this.idOperacion = idOperacion;
	}
	/**
	 * @return the idOperacionOrigen
	 */
	public String getIdOperacionOrigen() {
		return idOperacionOrigen;
	}
	/**
	 * @param idOperacionOrigen the idOperacionOrigen to set
	 */
	public void setIdOperacionOrigen(String idOperacionOrigen) {
		this.idOperacionOrigen = idOperacionOrigen;
	}
	/**
	 * @return the numeroReferenciaBancaria
	 */
	public String getNumeroReferenciaBancaria() {
		return numeroReferenciaBancaria;
	}
	/**
	 * @param numeroReferenciaBancaria the numeroReferenciaBancaria to set
	 */
	public void setNumeroReferenciaBancaria(String numeroReferenciaBancaria) {
		this.numeroReferenciaBancaria = numeroReferenciaBancaria;
	}
	/**
	 * @return the tipoOperacion
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}
	/**
	 * @param tipoOperacion the tipoOperacion to set
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}
	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	/**
	 * @return the terminalPEI
	 */
	public String getTerminalPEI() {
		return terminalPEI;
	}
	/**
	 * @param terminalPEI the terminalPEI to set
	 */
	public void setTerminalPEI(String terminalPEI) {
		this.terminalPEI = terminalPEI;
	} 	
	
}
