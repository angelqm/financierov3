package ar.com.redlink.sam.financieroservices.pei.service;

import ar.com.redlink.framework.rest.common.exception.RedLinkRESTServiceException;
import ar.com.redlink.sam.financieroservices.pei.response.TransferenciaPEIResponse;

/**
 * 
 * @author EXT-ibanezma
 *
 */
public interface SesionPeiService {

	TransferenciaPEIResponse getSession(String comercioId) throws RedLinkRESTServiceException;

}
