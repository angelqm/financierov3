package ar.com.redlink.sam.financieroservices.pei;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import ar.com.redlink.framework.rest.common.RedLinkRESTService;
import ar.com.redlink.framework.rest.common.exception.RedLinkRESTServiceException;
import ar.com.redlink.sam.financieroservices.dto.request.ConsultaOperacionPEIPagarRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoPEIPagarRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoParcialPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaPEIPagarRequestDTO;
import ar.com.redlink.sam.financieroservices.pei.response.ConsultaOperacionPEIPagarResponse;
import ar.com.redlink.sam.financieroservices.pei.response.ConsultaTipoDocumentoPEIResponse;
import ar.com.redlink.sam.financieroservices.pei.response.RealizarDevolucionPEIPagarResponse;
import ar.com.redlink.sam.financieroservices.pei.response.RealizarDevolucionPEIResponse;
import ar.com.redlink.sam.financieroservices.pei.response.TransferenciaPEIPagarResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Cliente REST Enlace pagos.
 * 
 * @author he_e90104
 *
 */
public interface ConCuentaRecaudadoraRESTService extends RedLinkRESTService {

	@Path("/bandamagnetica")
	@Consumes("application/json")
	@Produces("application/json")
	@POST
	@ApiOperation(value = "Persiste un objeto", notes = "Retorna el id del objeto persistido.")
	public TransferenciaPEIPagarResponse pagar(@HeaderParam("requerimiento") String requerimiento,
			@HeaderParam("cliente") String cliente,
			@ApiParam(value = "Objeto a persistir", required = true) RealizarTransferenciaPEIPagarRequestDTO object)
			throws RedLinkRESTServiceException;

	@Path("/bandamagnetica/consulta/{idComercio}")
	@Consumes("application/json")
	@Produces("application/json")
	@POST
	public ConsultaOperacionPEIPagarResponse consultar(@HeaderParam("requerimiento") String requerimiento, @HeaderParam("cliente") String cliente,
			@ApiParam(value = "Objeto a persistir", required = true) ConsultaOperacionPEIPagarRequestDTO object)
			throws RedLinkRESTServiceException;


	@Path("/bandamagnetica/devolucion/total")
	@Consumes("application/json")
	@Produces("application/json")
	@POST
	public RealizarDevolucionPEIPagarResponse devolucionPEIPagar(@HeaderParam("requerimiento") String requerimiento,
			@HeaderParam("cliente") String cliente,
			@ApiParam(value = "Objeto a persistir", required = true) RealizarDevolucionPagoPEIPagarRequestDTO object)
			throws RedLinkRESTServiceException;

	
	@Path("/bandamagnetica/devolucion/parcial")
	@Consumes("application/json")
	@Produces("application/json")
	@POST
	public RealizarDevolucionPEIResponse devolucionParcial(@HeaderParam("requerimiento") String requerimiento,
			@HeaderParam("cliente") String cliente,
			@ApiParam(value = "Objeto a persistir", required = true) RealizarDevolucionPagoParcialPEIRequestDTO object)
			throws RedLinkRESTServiceException;
	
	@Path("/tipodoc")
	@Consumes("application/json")
	@Produces("application/json")
	@GET
	public ConsultaTipoDocumentoPEIResponse tipoDocumento(@HeaderParam("requerimiento") String requerimiento, @HeaderParam("cliente") String cliente)
			throws RedLinkRESTServiceException;
	
}
