package ar.com.redlink.sam.financieroservices.pei.response;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author EXT-IBANEZMA
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class OperacionesPEIResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String tipoOperacion;
	private String estado;
	private String fecha;
	private String idOperacion;
	private String pan;
	private String idReferenciaOperacionComercio;
	private BigDecimal importe;
	private BigDecimal saldo;
	private MotivoRechazoPEIResponse motivoRechazo;
	private Integer idSucursal;
	private String idTerminal;
	private String idCanal;
	private String concepto;
	private Integer idOperacionOrigen;
	private String idReferenciaTrxComercio;
	private String numeroReferenciaBancaria;


	/**
	 * @return the tipoOperacion
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}

	/**
	 * @param tipoOperacion the tipoOperacion to set
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado
	 *            the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @param fecha
	 *            the fecha to set
	 */
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	/**
	 * @return the idOperacion
	 */
	public String getIdOperacion() {
		return idOperacion;
	}

	/**
	 * @param idOperacion
	 *            the idOperacion to set
	 */
	public void setIdOperacion(String idOperacion) {
		this.idOperacion = idOperacion;
	}

	/**
	 * @return the pan
	 */
	public String getPan() {
		return pan;
	}

	/**
	 * @param pan
	 *            the pan to set
	 */
	public void setPan(String pan) {
		this.pan = pan;
	}

	/**
	 * @return the idReferenciaOperacionComercio
	 */
	public String getIdReferenciaOperacionComercio() {
		return idReferenciaOperacionComercio;
	}

	/**
	 * @param idReferenciaOperacionComercio
	 *            the idReferenciaOperacionComercio to set
	 */
	public void setIdReferenciaOperacionComercio(String idReferenciaOperacionComercio) {
		this.idReferenciaOperacionComercio = idReferenciaOperacionComercio;
	}

	/**
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}

	/**
	 * @param importe
	 *            the importe to set
	 */
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	/**
	 * @return the saldo
	 */
	public BigDecimal getSaldo() {
		return saldo;
	}

	/**
	 * @param saldo
	 *            the saldo to set
	 */
	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	/**
	 * @return the idSucursal
	 */
	public Integer getIdSucursal() {
		return idSucursal;
	}

	/**
	 * @param idSucursal
	 *            the idSucursal to set
	 */
	public void setIdSucursal(Integer idSucursal) {
		this.idSucursal = idSucursal;
	}

	/**
	 * @return the idTerminal
	 */
	public String getIdTerminal() {
		return idTerminal;
	}

	/**
	 * @param idTerminal
	 *            the idTerminal to set
	 */
	public void setIdTerminal(String idTerminal) {
		this.idTerminal = idTerminal;
	}

	/**
	 * @return the idCanal
	 */
	public String getIdCanal() {
		return idCanal;
	}

	/**
	 * @param idCanal
	 *            the idCanal to set
	 */
	public void setIdCanal(String idCanal) {
		this.idCanal = idCanal;
	}

	/**
	 * @return the motivoRechazo
	 */
	public MotivoRechazoPEIResponse getMotivoRechazo() {
		return motivoRechazo;
	}

	/**
	 * @param motivoRechazo the motivoRechazo to set
	 */
	public void setMotivoRechazo(MotivoRechazoPEIResponse motivoRechazo) {
		this.motivoRechazo = motivoRechazo;
	}

	/**
	 * @return the concepto
	 */
	public String getConcepto() {
		return concepto;
	}

	/**
	 * @param concepto the concepto to set
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	/**
	 * @return the idOperacionOrigen
	 */
	public Integer getIdOperacionOrigen() {
		return idOperacionOrigen;
	}

	/**
	 * @param idOperacionOrigen the idOperacionOrigen to set
	 */
	public void setIdOperacionOrigen(Integer idOperacionOrigen) {
		this.idOperacionOrigen = idOperacionOrigen;
	}

	/**
	 * @return the idReferenciaTrxComercio
	 */
	public String getIdReferenciaTrxComercio() {
		return idReferenciaTrxComercio;
	}

	/**
	 * @param idReferenciaTrxComercio the idReferenciaTrxComercio to set
	 */
	public void setIdReferenciaTrxComercio(String idReferenciaTrxComercio) {
		this.idReferenciaTrxComercio = idReferenciaTrxComercio;
	}

	public String getNumeroReferenciaBancaria() {
		return numeroReferenciaBancaria;
	}

	public void setNumeroReferenciaBancaria(String numeroReferenciaBancaria) {
		this.numeroReferenciaBancaria = numeroReferenciaBancaria;
	}
	
}
