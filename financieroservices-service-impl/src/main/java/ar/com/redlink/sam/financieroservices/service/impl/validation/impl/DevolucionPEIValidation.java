package ar.com.redlink.sam.financieroservices.service.impl.validation.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA138;

import ar.com.redlink.sam.financieroservices.dto.TrxFinancieroValidationDTO;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.Pago;
import ar.com.redlink.sam.financieroservices.service.TrxFinancieroService;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;

public class DevolucionPEIValidation extends AbstractFSValidation {
	
	private TrxFinancieroService trxFinancieroService;
	private boolean parcial;
	
	@Override
	public ValidationDTO validate(ValidationDTO validation) throws FinancieroValidationException, FinancieroException {
		Long terminalSamId = (Long) validation.getOutputValue(ValidationDTO.TERMINAL_SAM_ID);
		String prefijoOpId = validation	.getInputValue(ValidationDTO.PAGO_PREFIJO_OP_ID);
		String idReqOrig = validation.getInputValue(ValidationDTO.ID_REQ_ORIG);
		String importe = validation.getInputValue(ValidationDTO.IMPORTE);
		String tokenizado = validation.getInputValue(ValidationDTO.TOKENIZADO);

		TrxFinancieroValidationDTO trxFinancieroValidationDTO = new TrxFinancieroValidationDTO(importe,
				terminalSamId.toString(), idReqOrig, prefijoOpId, tokenizado);
		
		trxFinancieroValidationDTO.setImporteParcial(isParcial());
		
		//TrxFinanciero trxFinanciero = trxFinancieroService.validateTrx(trxFinancieroValidationDTO);
		Pago trxFinanciero = trxFinancieroService.validateCancel(trxFinancieroValidationDTO);
		//validateCancel

		if (null == trxFinanciero) {
			throw new FinancieroValidationException(FINA138.getCode(),	FINA138.getMsg());
		}

		nextValidation(validation);

		return validation;
	}

	/**
	 * @return the trxFinancieroService
	 */
	public TrxFinancieroService getTrxFinancieroService() {
		return trxFinancieroService;
	}

	/**
	 * @param trxFinancieroService
	 *            the trxFinancieroService to set
	 */
	public void setTrxFinancieroService(
			TrxFinancieroService trxFinancieroService) {
		this.trxFinancieroService = trxFinancieroService;
	}

	/**
	 * @return the parcial
	 */
	public boolean isParcial() {
		return parcial;
	}

	/**
	 * @param parcial the parcial to set
	 */
	public void setParcial(boolean parcial) {
		this.parcial = parcial;
	}
	
}
