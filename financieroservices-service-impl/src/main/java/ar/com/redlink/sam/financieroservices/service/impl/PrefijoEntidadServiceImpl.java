/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  26/03/2015 - 15:07:57
 */

package ar.com.redlink.sam.financieroservices.service.impl;

import org.apache.log4j.Logger;

import ar.com.redlink.framework.services.crud.impl.RedLinkGenericServiceImpl;
import ar.com.redlink.sam.financieroservices.dao.PrefijoEntidadDAO;
import ar.com.redlink.sam.financieroservices.entity.PrefijoEntidad;
import ar.com.redlink.sam.financieroservices.service.PrefijoEntidadService;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA200;

/**
 * Implementacion del servicio para obtencion de PrefijoEntidad.
 * 
 * @author aguerrea
 * 
 */
public class PrefijoEntidadServiceImpl extends
		RedLinkGenericServiceImpl<PrefijoEntidad> implements
		PrefijoEntidadService {

	private static final Logger LOGGER = Logger.getLogger(PrefijoEntidadServiceImpl.class);

	private PrefijoEntidadDAO prefijoEntidadDAO;

	/**
	 * 
	 * @throws FinancieroException 
	 * @see ar.com.redlink.sam.financieroservices.service.PrefijoEntidadService#getPrefijoEntidadByTarjeta(java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	public PrefijoEntidad getPrefijoEntidadByTarjeta(String tarjeta,
			String entidad) throws FinancieroException {
		PrefijoEntidad prefijoEntidad = null;

		try {
			prefijoEntidad = getPrefijoEntidadDAO().getPrefijoByTarjeta(tarjeta, entidad);
		} catch (Throwable t) {
			LOGGER.error("Error en DAO de PrefijoEntidad: " + t.getMessage(), t);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), t.getCause().toString());
		}

		return prefijoEntidad;
	}

	/**
	 * @return the prefijoEntidadDAO
	 */
	public PrefijoEntidadDAO getPrefijoEntidadDAO() {
		return prefijoEntidadDAO;
	}

	/**
	 * @param prefijoEntidadDAO
	 *            the prefijoEntidadDAO to set
	 */
	public void setPrefijoEntidadDAO(PrefijoEntidadDAO prefijoEntidadDAO) {
		this.prefijoEntidadDAO = prefijoEntidadDAO;
	}

}
