/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  23/04/2015 - 13:24:38
 */

package ar.com.redlink.sam.financieroservices.service.impl.validation.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA141;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA142;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA143;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA144;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA149;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA174;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA200;

import java.text.ParseException;

import org.apache.log4j.Logger;

import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.util.DateUtil;
import ar.com.redlink.sam.financieroservices.service.TrxFinancieroService;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;

/**
 * Componente que valida los atributos comunes de todos los requests recibidos
 * por el servicio financiero.
 * 
 * @author aguerrea
 * 
 */
public class RequestValidation extends AbstractFSValidation {

	private static final Logger LOGGER = Logger.getLogger(RequestValidation.class);

	private TrxFinancieroService trxFinancieroService;

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.validation.FSValidation#validate(ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO)
	 */
	@Override
	public ValidationDTO validate(ValidationDTO validation)	throws FinancieroValidationException, FinancieroException {
		nullCheck(validation, ValidationDTO.ID_REQ, FINA141);
		nullCheck(validation, ValidationDTO.IP_CLIENTE, FINA142);
		nullCheck(validation, ValidationDTO.TIMESTAMP, FINA143);

		try {
			DateUtil.parseTimestamp(validation.getInputValue(ValidationDTO.TIMESTAMP));
		} catch (ParseException e) {
			throw new FinancieroValidationException(FINA144.getCode(),	FINA144.getMsg());
		}

		boolean exists;

		try {
			exists = trxFinancieroService.checkIdRequerimientoOperacionPeiNulo(validation.getInputValue(ValidationDTO.ID_REQ));
		} catch (Exception e) {
			LOGGER.error("Error al acceder el servicio de TRXFinanciero: "+ e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}

		if (exists) {
			throw new FinancieroValidationException(FINA174.getCode(),FINA174.getMsg());
		}
		
		try {
			exists = trxFinancieroService.checkIdRequerimiento(validation.getInputValue(ValidationDTO.ID_REQ));
		} catch (Exception e) {
			LOGGER.error("Error al acceder el servicio de TRXFinanciero: "+ e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}

		if (exists) {
			throw new FinancieroValidationException(FINA149.getCode(),FINA149.getMsg());
		}		

		nextValidation(validation);

		return validation;
	}

	/**
	 * @return the trxFinancieroService
	 */
	public TrxFinancieroService getTrxFinancieroService() {
		return trxFinancieroService;
	}

	/**
	 * @param trxFinancieroService
	 *            the trxFinancieroService to set
	 */
	public void setTrxFinancieroService(
			TrxFinancieroService trxFinancieroService) {
		this.trxFinancieroService = trxFinancieroService;
	}

}
