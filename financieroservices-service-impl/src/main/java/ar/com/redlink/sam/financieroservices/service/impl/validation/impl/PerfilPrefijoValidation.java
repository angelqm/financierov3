/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  01/04/2015 - 17:26:26
 */

package ar.com.redlink.sam.financieroservices.service.impl.validation.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA131;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA200;

import java.util.List;

import org.apache.log4j.Logger;

import ar.com.redlink.framework.services.crud.RedLinkGenericService;
import ar.com.redlink.framework.services.exception.RedLinkServiceException;
import ar.com.redlink.sam.financieroservices.dao.search.filter.PerfilPrefijoByPerfilFinancieroIdSearchFilter;
import ar.com.redlink.sam.financieroservices.dao.search.filter.PerfilPrefijoGrupoByIdAndPrefijoEntidadIdSearchFilter;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.PerfilPrefijo;
import ar.com.redlink.sam.financieroservices.entity.PerfilPrefijoGrupo;
import ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;

/**
 * Valida un PerfilPrefijo (O varios, segun la relacion) a partir de un ID de
 * Perfil Financiero.
 * 
 * @author aguerrea
 * 
 */
public class PerfilPrefijoValidation extends AbstractFSValidation {

	private static final Logger LOGGER = Logger.getLogger(PerfilPrefijoValidation.class);
	private RedLinkGenericService<PerfilPrefijo> perfilPrefijoService;
	private RedLinkGenericService<PerfilPrefijoGrupo> perfilPrefijoGrupoService;

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.validation.FSValidation#validate(ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO)
	 */
	@Override
	public ValidationDTO validate(ValidationDTO validation) throws FinancieroException {

		final Long valor = (Long) validation.getOutputValue(ValidationDTO.PERFIL_FINANCIERO_ID);

		List<PerfilPrefijo> perfilPrefijos = null;

		try {
			//LOGGER.info("[DAO INI] PerfilPrefijoByPerfilFinancieroIdSearchFilter");
			
			perfilPrefijos = getPerfilPrefijoService().getAll(new PerfilPrefijoByPerfilFinancieroIdSearchFilter(valor),	null).getResult();
			
			//LOGGER.info("[DAO FIN] PerfilPrefijoByPerfilFinancieroIdSearchFilter");
			
		} catch (RedLinkServiceException e) {
			LOGGER.error("Error al acceder el servicio de PerfilPrefijo: "	+ e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}

		if (perfilPrefijos.size() > 0) {
			PerfilPrefijoGrupo perfilPrefijoGrupo = extractPerfilPrefijoGrupo(validation, perfilPrefijos.get(0));

			if (null == perfilPrefijoGrupo) {
				throw new FinancieroValidationException(
						FinancieroServicesMsgEnum.FINA132.getCode(),
						FinancieroServicesMsgEnum.FINA132.getMsg());
			}
			validateEstadoRegistro(perfilPrefijoGrupo, FINA131);
			validateDates(perfilPrefijoGrupo, FINA131);
			validation.addOutputValue(ValidationDTO.PERFIL_PREFIJO_GRUPO_ID,perfilPrefijoGrupo.getPerfilPrefijoGrupoId());
		}

		nextValidation(validation);

		return validation;
	}

	/**
	 * Busca un {@link PerfilPrefijoGrupo} valido en forma recursiva dentro de
	 * un {@link PerfilPrefijo}.
	 * 
	 * @param validation
	 * @param perfilPrefijos
	 * @throws FinancieroException
	 */
	private PerfilPrefijoGrupo extractPerfilPrefijoGrupo( ValidationDTO validation, PerfilPrefijo perfilPrefijo) throws FinancieroException {
		final Long perfilPrefijoId = perfilPrefijo.getPerfilPrefijoId();

		PerfilPrefijoGrupo perfilPrefijoGrupo = null;
		List<PerfilPrefijoGrupo> perfilPrefijoGrupos = null;

		try {
			perfilPrefijoGrupos = perfilPrefijoGrupoService
					.getAll(new PerfilPrefijoGrupoByIdAndPrefijoEntidadIdSearchFilter(
							perfilPrefijoId,
							(Long) validation.getOutputValue(ValidationDTO.PREFIJO_ENTIDAD_ID)),
							null).getResult();

			if (perfilPrefijoGrupos.size() == 0	&& null != perfilPrefijo.getPerfilPrefijo()) {
				perfilPrefijoGrupo = extractPerfilPrefijoGrupo(validation, perfilPrefijo.getPerfilPrefijo());
			} else {
				if (perfilPrefijoGrupos.size() != 0) {
					perfilPrefijoGrupo = perfilPrefijoGrupos.get(0);
				}
			}
		} catch (RedLinkServiceException e) {
			LOGGER.error("Error al acceder el servicio de PerfilPrefijoGrupo: "	+ e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}

		return perfilPrefijoGrupo;
	}

	/**
	 * @return the perfilPrefijoService
	 */
	public RedLinkGenericService<PerfilPrefijo> getPerfilPrefijoService() {
		return perfilPrefijoService;
	}

	/**
	 * @param perfilPrefijoService
	 *            the perfilPrefijoService to set
	 */
	public void setPerfilPrefijoService(
			RedLinkGenericService<PerfilPrefijo> perfilPrefijoService) {
		this.perfilPrefijoService = perfilPrefijoService;
	}

	/**
	 * @return the perfilPrefijoGrupoService
	 */
	public RedLinkGenericService<PerfilPrefijoGrupo> getPerfilPrefijoGrupoService() {
		return perfilPrefijoGrupoService;
	}

	/**
	 * @param perfilPrefijoGrupoService
	 *            the perfilPrefijoGrupoService to set
	 */
	public void setPerfilPrefijoGrupoService(RedLinkGenericService<PerfilPrefijoGrupo> perfilPrefijoGrupoService) {
		this.perfilPrefijoGrupoService = perfilPrefijoGrupoService;
	}

}
