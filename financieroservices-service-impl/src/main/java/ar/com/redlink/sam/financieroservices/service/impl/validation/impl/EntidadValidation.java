/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  29/03/2015 - 14:34:44
 */

package ar.com.redlink.sam.financieroservices.service.impl.validation.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA100;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA101;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA102;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA200;

import java.util.List;

import org.apache.log4j.Logger;

import ar.com.redlink.framework.services.crud.RedLinkGenericService;
import ar.com.redlink.framework.services.exception.RedLinkServiceException;
import ar.com.redlink.sam.financieroservices.dao.search.filter.EntidadByCodSearchFilter;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.Entidad;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;

/**
 * Valida la entidad recibida.
 * 
 * @author aguerrea
 * 
 */
public class EntidadValidation extends AbstractFSValidation {

	private static final Logger LOGGER = Logger.getLogger(EntidadValidation.class);

	private RedLinkGenericService<Entidad> entidadService;

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.validation.FSValidation#validate(ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO)
	 */
	@Override
	public ValidationDTO validate(ValidationDTO validation)	throws FinancieroException {
		nullCheck(validation, ValidationDTO.ENTIDAD_COD, FINA100);

		Entidad entidad = null;

		try {
			//LOGGER.info("[DAO INI] EntidadByCodSearchFilter");
			
			List<Entidad> entidades = getEntidadService().getAll(new EntidadByCodSearchFilter(validation.getInputValue(ValidationDTO.ENTIDAD_COD)),	null).getResult();
			
			//LOGGER.info("[DAO FIN] EntidadByCodSearchFilter");

			if (entidades.size() > 0) {
				entidad = entidades.get(0);
			}
		} catch (RedLinkServiceException e) {
			LOGGER.error("Error al acceder el servicio de Entidad: "+ e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}

		if (null == entidad) {
			throw new FinancieroValidationException(FINA102.getCode(),FINA102.getMsg());
		} else {
			this.validateEstadoRegistro(entidad, FINA101);
			this.validateDates(entidad, FINA101);
			validation.addOutputValue(ValidationDTO.ENTIDAD_ID,	entidad.getEntidadId());
		}

		nextValidation(validation);

		return validation;
	}

	/**
	 * @return the entidadService
	 */
	public RedLinkGenericService<Entidad> getEntidadService() {
		return entidadService;
	}

	/**
	 * @param entidadService
	 *            the entidadService to set
	 */
	public void setEntidadService(RedLinkGenericService<Entidad> entidadService) {
		this.entidadService = entidadService;
	}

}
