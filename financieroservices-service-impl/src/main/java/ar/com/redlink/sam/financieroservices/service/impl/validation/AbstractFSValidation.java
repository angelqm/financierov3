/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  29/03/2015 - 14:29:10
 */

package ar.com.redlink.sam.financieroservices.service.impl.validation;

import java.util.Date;

import org.joda.time.DateTime;

import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.BaseEntity;
import ar.com.redlink.sam.financieroservices.entity.EstadoRegistro;
import ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.validation.FSValidation;

/**
 * Clase abstracta con operaciones comunes para la validacion de parametros.
 * 
 * @author aguerrea
 * 
 */
public abstract class AbstractFSValidation implements FSValidation {

	protected FSValidation nextValidation;

	/**
	 * Valida la vigencia de la entity provista y arroja una excepcion con los
	 * detalles del Enum indicado.
	 * 
	 * @param baseEntity
	 * @param errorToThrow
	 * @throws FinancieroValidationException
	 */
	protected void validateDates(BaseEntity baseEntity,
			FinancieroServicesMsgEnum errorToThrow)
			throws FinancieroValidationException {
		boolean successVigencia = true;
		Date vigenciaDesde = baseEntity.getVigenciaDesde();
		Date vigenciaHasta = baseEntity.getVigenciaHasta();

		successVigencia = validateTwoDates(vigenciaDesde, vigenciaHasta);

		boolean successAlta = true;

		Date fechaAlta = baseEntity.getFechaAlta();
		Date fechaBaja = baseEntity.getFechaBaja();

		successAlta = validateTwoDates(fechaAlta, fechaBaja);

		boolean successActivacion = true;

		Date fechaActivacion = baseEntity.getFechaActivacion();

		if (fechaActivacion != null) {
			DateTime fechaActivacionDateTime = new DateTime(fechaActivacion);
			successActivacion = fechaActivacionDateTime.isBeforeNow();
		}

		if (!successVigencia || !successAlta || !successActivacion) {
			throw new FinancieroValidationException(errorToThrow.getCode(),
					errorToThrow.getMsg());
		}
	}

	/**
	 * Valida la vigencia de las fechas provistas.
	 * 
	 * @param fromDate
	 * @param toDate
	 * @return true si es valida.
	 */
	private boolean validateTwoDates(Date fromDate, Date toDate) {
		boolean success = true;

		if (fromDate != null) {
			DateTime fromDateTime = new DateTime(fromDate);
			DateTime toDateTime = null;

			if (toDate != null) {
				toDateTime = new DateTime(toDate).plusHours(23).plusMinutes(59)
						.plusSeconds(59);
			}

			success = fromDateTime.isBeforeNow() && ((toDateTime != null) ? toDateTime.isAfterNow() : true);
		}

		return success;
	}

	/**
	 * Valida el estado del registro.
	 * 
	 * @param baseEntity
	 * @param Error
	 *            a arrojar si falla la validacion.
	 */
	protected void validateEstadoRegistro(BaseEntity baseEntity,
			FinancieroServicesMsgEnum errorToThrow)
			throws FinancieroValidationException {
		EstadoRegistro estado = baseEntity.getEstadoRegistro();

		if (estado.getCodigo() != '0') {
			throw new FinancieroValidationException(errorToThrow.getCode(),	errorToThrow.getMsg());
		}
	}

	/**
	 * Valida el objeto con el siguiente validator.
	 * 
	 * @param validation
	 * @return el objeto validado.
	 * @throws FinancieroException
	 */
	protected ValidationDTO nextValidation(ValidationDTO validation)
			throws FinancieroException {
		if (null != nextValidation) {
			nextValidation.validate(validation);
		}

		return validation;
	}

	/**
	 * Checkeo generico de nulos.
	 * 
	 * @param validation
	 * @param tipo
	 * @param error
	 * @throws FinancieroValidationException
	 */
	protected void nullCheck(ValidationDTO validation, String tipo,	FinancieroServicesMsgEnum error) throws FinancieroValidationException {
		if (null == validation.getInputValue(tipo) || validation.getInputValue(tipo).isEmpty()) {
			throw new FinancieroValidationException(error.getCode(), error.getMsg());
		}
	}

	/**
	 * @return the nextValidation
	 */
	public FSValidation getNextValidation() {
		return nextValidation;
	}

	/**
	 * @param nextValidation
	 *            the nextValidation to set
	 */
	public void setNextValidation(FSValidation nextValidation) {
		this.nextValidation = nextValidation;
	}

}
