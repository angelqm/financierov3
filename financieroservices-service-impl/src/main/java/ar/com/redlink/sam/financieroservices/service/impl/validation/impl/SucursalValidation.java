/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  29/03/2015 - 14:54:54
 */

package ar.com.redlink.sam.financieroservices.service.impl.validation.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA103;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA104;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA105;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA200;

import java.util.List;

import org.apache.log4j.Logger;

import ar.com.redlink.framework.services.crud.RedLinkGenericService;
import ar.com.redlink.framework.services.exception.RedLinkServiceException;
import ar.com.redlink.sam.financieroservices.dao.search.filter.SucursalByCodAndEntidadSearchFilter;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.Sucursal;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;

/**
 * Validacion de Sucursal.
 * 
 * @author aguerrea
 * 
 */
public class SucursalValidation extends AbstractFSValidation {

	private static final Logger LOGGER = Logger.getLogger(SucursalValidation.class);

	private RedLinkGenericService<Sucursal> sucursalService;

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.validation.FSValidation#validate(ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO)
	 */
	@Override
	public ValidationDTO validate(ValidationDTO validation)
			throws FinancieroException {
		nullCheck(validation, ValidationDTO.SUCURSAL_COD, FINA103);

		Sucursal sucursal = null;

		try {
			
			//LOGGER.info("[DAO INI] SucursalByCodAndEntidadSearchFilter");
			
			List<Sucursal> sucursales = getSucursalService()
					.getAll(new SucursalByCodAndEntidadSearchFilter(
							(String) validation.getInputValue(ValidationDTO.SUCURSAL_COD), 
							(Integer) validation.getOutputValue(ValidationDTO.ENTIDAD_ID)),
							null).getResult();
			
		//	LOGGER.info("[DAO FIN] SucursalByCodAndEntidadSearchFilter");

			if (sucursales.size() == 1) {
				sucursal = sucursales.get(0);
			}
		} catch (RedLinkServiceException e) {
			LOGGER.error("Error al acceder el servicio de Sucursal: "+ e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}

		if (null == sucursal) {
			throw new FinancieroValidationException(FINA105.getCode(),	FINA105.getMsg());
		} else {
			validateEstadoRegistro(sucursal, FINA104);
			validateDates(sucursal, FINA104);
			validation.addOutputValue(ValidationDTO.SUCURSAL_ID,sucursal.getSucursalId());
			validation.addOutputValue(ValidationDTO.AGENTE_ID, sucursal	.getAgente().getAgenteId());
			validation.addOutputValue(ValidationDTO.RED_ID, sucursal.getRed().getRedId());
		}

		nextValidation(validation);

		return validation;
	}

	/**
	 * @return the sucursalService
	 */
	public RedLinkGenericService<Sucursal> getSucursalService() {
		return sucursalService;
	}

	/**
	 * @param sucursalService
	 *            the sucursalService to set
	 */
	public void setSucursalService(
			RedLinkGenericService<Sucursal> sucursalService) {
		this.sucursalService = sucursalService;
	}
}
