package ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA166;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA167;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import ar.com.redlink.framework.services.crud.RedLinkGenericService;
import ar.com.redlink.framework.services.exception.RedLinkServiceException;
import ar.com.redlink.sam.financieroservices.dao.search.filter.ComercioPEIByAgenteSearchFilter;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.ComercioPEI;
import ar.com.redlink.sam.financieroservices.entity.EstadoRegistro;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;

public class ComercioPEIMockValidation extends AbstractFSValidation {

	private static final Logger LOGGER = Logger.getLogger(TerminalSamMockValidation.class);

	private RedLinkGenericService<ComercioPEI> comercioPEIService;

	@Override
	public ValidationDTO validate(ValidationDTO validation) throws FinancieroValidationException, FinancieroException {

		ComercioPEI comercioPEI = null;

		//try {
			ComercioPEI cp= new ComercioPEI();
			cp.setAgenteId((long) validation.getOutputValue(ValidationDTO.AGENTE_ID));
			cp.setComercioPEIId(26);
			cp.setCodigoComercioPei(135);
			
			EstadoRegistro es= new EstadoRegistro();
			es.setEstadoRegistroId((short)0);
			cp.setEstadoRegistro(es);
			cp.setFechaActivacion(new Date());
			cp.setFechaAlta(new Date());
			
			/*List<ComercioPEI> comerciosPEI = getComercioPEIService().getAll(
					new ComercioPEIByAgenteSearchFilter((long) validation.getOutputValue(ValidationDTO.AGENTE_ID)),
					null).getResult();*/
			
			List<ComercioPEI> comerciosPEI = new ArrayList();
			comerciosPEI.add(cp);
			

			if (comerciosPEI.size() > 0) {
				comercioPEI = comerciosPEI.get(0);
			}

		/*} catch (RedLinkServiceException e) {
			LOGGER.error("Error al acceder el servicio de ComercioPEI: " + e.getMessage(), e);
			throw new FinancieroException(FINA166.getCode(), FINA166.getMsg());
		}*/
		
		if (null == comercioPEI) {
			throw new FinancieroValidationException(FINA166.getCode(), FINA166.getMsg());
		} else {
			//validateEstadoRegistro(comercioPEI, FINA167);
			//validateDates(comercioPEI, FINA167);
			validation.addOutputValue(ValidationDTO.CODIGO_COMERCIO_PEI,comercioPEI.getCodigoComercioPei());
		}

		nextValidation(validation);

		return validation;
	}

	/**
	 * @return the comercioPEIService
	 */
	public RedLinkGenericService<ComercioPEI> getComercioPEIService() {
		return comercioPEIService;
	}

	/**
	 * @param comercioPEIService the comercioPEIService to set
	 */
	public void setComercioPEIService(RedLinkGenericService<ComercioPEI> comercioPEIService) {
		this.comercioPEIService = comercioPEIService;
	}

}