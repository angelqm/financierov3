package ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA168;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA169;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ar.com.redlink.framework.services.crud.RedLinkGenericService;
import ar.com.redlink.framework.services.exception.RedLinkServiceException;
import ar.com.redlink.sam.financieroservices.dao.search.filter.TerminalPEIByTerminalSamSearchFilter;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.EstadoRegistro;
import ar.com.redlink.sam.financieroservices.entity.TerminalPEI;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;

public class TerminalPEIMockValidation extends AbstractFSValidation {

	private static final Logger LOGGER = Logger.getLogger(TerminalPEIMockValidation.class);

	private RedLinkGenericService<TerminalPEI> terminalPEIService;

	@Override
	public ValidationDTO validate(ValidationDTO validation) throws FinancieroValidationException, FinancieroException {

		TerminalPEI terminalPEI = null;

		//try {
			/*List<TerminalPEI> terminalesPEI = getTerminalPEIService().getAll(new TerminalPEIByTerminalSamSearchFilter(
					(long) validation.getOutputValue(ValidationDTO.TERMINAL_SAM_ID)), null).getResult();*/

			List<TerminalPEI> terminalesPEI = new ArrayList();
			
			TerminalPEI tPei= new TerminalPEI();
			tPei.setCodigoTerminalPei("VXE9MW");
			tPei.setTerminalPEIId(19706);
			tPei.setTerminalSamId(945132500017000L);
			
			EstadoRegistro e= new EstadoRegistro();
			e.setEstadoRegistroId((short)0);
			e.setCodigo('0');
			
			tPei.setEstadoRegistro(e);
			
			terminalesPEI.add(tPei);
			
			if (terminalesPEI.size() > 0) {
				terminalPEI = terminalesPEI.get(0);
			}

		/*} catch (RedLinkServiceException e) {
			LOGGER.error("Error al acceder el servicio de TerminalPEI: " + e.getMessage(), e);
			throw new FinancieroException(FINA168.getCode(), FINA168.getMsg());
		}*/

		if (null == terminalPEI) {
			throw new FinancieroValidationException(FINA168.getCode(), FINA168.getMsg());
		} else {
			validateEstadoRegistro(terminalPEI, FINA169);
			validateDates(terminalPEI, FINA169);
			validation.addOutputValue(ValidationDTO.CODIGO_TERMINAL_PEI, terminalPEI.getCodigoTerminalPei());
		}

		nextValidation(validation);

		return validation;
	}

	/**
	 * @return the terminalPEIService
	 */
	public RedLinkGenericService<TerminalPEI> getTerminalPEIService() {
		return terminalPEIService;
	}

	/**
	 * @param terminalPEIService
	 *            the terminalPEIService to set
	 */
	public void setTerminalPEIService(RedLinkGenericService<TerminalPEI> terminalPEIService) {
		this.terminalPEIService = terminalPEIService;
	}

}