/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  01/04/2015 - 14:55:58
 */

package ar.com.redlink.sam.financieroservices.service.impl.validation.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA100;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA124;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA125;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA126;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.PrefijoEntidad;
import ar.com.redlink.sam.financieroservices.service.PrefijoEntidadService;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;

/**
 * Encapsula la validacion de Prefijo y obtencion de PrefijoEntidad asociado.
 * 
 * @author aguerrea
 * 
 */
public class PrefijoEntidadValidation extends AbstractFSValidation {

	private PrefijoEntidadService prefijoEntidadService;

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.validation.FSValidation#validate(ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO)
	 */
	@Override
	public ValidationDTO validate(ValidationDTO validation)	throws FinancieroException {
		// Necesito la tarjeta para extraer adecuadamente el prefijo, ya que es
		// variable.
		nullCheck(validation, ValidationDTO.TARJETA, FINA124);
		nullCheck(validation, ValidationDTO.ENTIDAD_ID, FINA100);

		PrefijoEntidad prefijoEntidad = prefijoEntidadService
				.getPrefijoEntidadByTarjeta(
						validation.getInputValue(ValidationDTO.TARJETA),
						validation.getInputValue(ValidationDTO.ENTIDAD_ID));

		// Se nullea la tarjeta, para que no quede en memoria.
		validation.getInputValues().put(ValidationDTO.TARJETA, null);

		if (prefijoEntidad == null) {
			throw new FinancieroValidationException(FINA125.getCode(),	FINA125.getMsg());
		} else {
			validateEstadoRegistro(prefijoEntidad, FINA126);
			validateDates(prefijoEntidad, FINA126);
			Long prefijoEntidadId = prefijoEntidad.getPrefijoEntidadId();
			validation.addOutputValue(ValidationDTO.PREFIJO_ENTIDAD_ID,	prefijoEntidadId);

			nextValidation(validation);
		}

		return validation;
	}

	/**
	 * @return the prefijoEntidadService
	 */
	public PrefijoEntidadService getPrefijoEntidadService() {
		return prefijoEntidadService;
	}

	/**
	 * @param prefijoEntidadService
	 *            the prefijoEntidadService to set
	 */
	public void setPrefijoEntidadService(PrefijoEntidadService prefijoEntidadService) {
		this.prefijoEntidadService = prefijoEntidadService;
	}

}
