/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  12/04/2015 - 16:02:36
 */

package ar.com.redlink.sam.financieroservices.service.impl.validation.rules.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA300;

import java.util.Date;

import org.apache.log4j.Logger;

import ar.com.redlink.sam.financieroservices.service.TrxFinancieroService;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.rules.LimiteRuleStrategy;

/**
 * Realiza el calculo correspondiente a un limite de monto.
 * 
 * @author aguerrea
 * 
 */
public class LimiteMontoRuleStrategy implements LimiteRuleStrategy {

	private static final Logger LOGGER = Logger.getLogger(LimiteMontoRuleStrategy.class);

	private TrxFinancieroService trxFinancieroService;
	private String campo;
	private String id;
	private Double valor;
	private Double cantidad;
	private Date dateFrom;
	private Date dateTo;
	private String tokenizado;
	private Long prefijoOpId;

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.service.impl.validation.rules.LimiteRuleStrategy#validate(java.lang.String)
	 */
	@Override
	public void validate() throws FinancieroException {
		Double all = null;
		Double total = null;
		try {
			total = trxFinancieroService.getSum(campo + "_id", id, dateFrom,dateTo, tokenizado, prefijoOpId);
			all = total + cantidad;
		} catch (FinancieroException e) {
			LOGGER.error("Error al acceder el servicio de TRX_FINANCIERO: "	+ e.getMessage(), e);
			throw e;
		}

		if (all != null && all > valor) {
			Double remanent = (valor - total);

			if (remanent < 0) {
				remanent = (double) 0;
			}

			throw new FinancieroValidationException(FINA300.getCode(),	String.format(FINA300.getMsg(), campo, valor, remanent));
		}
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.impl.validation.rules.LimiteRuleStrategy#setCampo(java.lang.String)
	 */
	@Override
	public void setCampo(String campo) {
		this.campo = campo;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the trxFinancieroService
	 */
	public TrxFinancieroService getTrxFinancieroService() {
		return trxFinancieroService;
	}

	/**
	 * @param trxFinancieroService
	 *            the trxFinancieroService to set
	 */
	public void setTrxFinancieroService(TrxFinancieroService trxFinancieroService) {
		this.trxFinancieroService = trxFinancieroService;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.impl.validation.rules.LimiteRuleStrategy#setValor(java.lang.Double)
	 */
	@Override
	public void setValor(Double valor) {
		this.valor = valor;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.impl.validation.rules.LimiteRuleStrategy#setCantidad(java.lang.Double)
	 */
	@Override
	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.impl.validation.rules.LimiteRuleStrategy#setDateFrom(java.util.Date)
	 */
	@Override
	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.impl.validation.rules.LimiteRuleStrategy#setDateTo(java.util.Date)
	 */
	@Override
	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.impl.validation.rules.LimiteRuleStrategy#setTokenizado(java.lang.String)
	 */
	@Override
	public void setTokenizado(String tokenizado) {
		this.tokenizado = tokenizado;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.impl.validation.rules.LimiteRuleStrategy#setCodigoOp(java.lang.Long)
	 */
	@Override
	public void setCodigoOp(Long prefijoOpId) {
		this.prefijoOpId = prefijoOpId;

	}
}
