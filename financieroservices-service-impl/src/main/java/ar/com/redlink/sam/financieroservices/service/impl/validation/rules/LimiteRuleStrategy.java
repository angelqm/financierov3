/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  12/04/2015 - 16:00:14
 */

package ar.com.redlink.sam.financieroservices.service.impl.validation.rules;

import java.util.Date;

import ar.com.redlink.sam.financieroservices.service.TrxFinancieroService;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;

/**
 * Interface que define la interface del strategy para calculo de reglas.
 * 
 * @author aguerrea
 * 
 */
public interface LimiteRuleStrategy {

	/**
	 * Realiza el calculo correspondiente al limite.
	 * 
	 * @throws FinancieroValidationException
	 * @throws FinancieroException
	 * 
	 */
	public void validate() throws FinancieroException;

	/**
	 * Setea el campo a ser validado.
	 * 
	 * @param campo
	 */
	public void setCampo(String campo);

	/**
	 * Setea el valor de ID del campo en cuestion.
	 * 
	 * @param id
	 */
	public void setId(String id);

	/**
	 * El valor de la regla.
	 * 
	 * @param valor
	 */
	public void setValor(Double valor);

	/**
	 * La cantidad, ya sea el importe o el total.
	 * 
	 * @param cantidad
	 */
	public void setCantidad(Double cantidad);

	/**
	 * La fecha desde para el filtro.
	 * 
	 * @param dateFrom
	 */
	public void setDateFrom(Date dateFrom);

	/**
	 * La fecha hasta para el filtro.
	 * 
	 * @param dateTo
	 */
	public void setDateTo(Date dateTo);

	/**
	 * La tarjeta a validarse.
	 * 
	 * @param tokenizado
	 */
	public void setTokenizado(String tokenizado);

	/**
	 * El codigo de operacion a utilizar como filtro.
	 * 
	 * @param prefijoOpId
	 */
	public void setCodigoOp(Long prefijoOpId);

	/**
	 * Setea el servicio a utilizarse.
	 * 
	 * @param trxFinancieroService
	 */
	public void setTrxFinancieroService(
			TrxFinancieroService trxFinancieroService);
}
