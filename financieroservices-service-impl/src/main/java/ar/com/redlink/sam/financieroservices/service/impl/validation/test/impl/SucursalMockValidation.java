/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA103;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA104;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA105;
import java.util.ArrayList;
import java.util.List;
import ar.com.redlink.framework.services.crud.RedLinkGenericService;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.Agente;
import ar.com.redlink.sam.financieroservices.entity.EstadoRegistro;
import ar.com.redlink.sam.financieroservices.entity.Red;
import ar.com.redlink.sam.financieroservices.entity.Sucursal;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;

/**
 * Validacion de Sucursal.
 * 
 * @author aguerrea
 * 
 */
public class SucursalMockValidation extends AbstractFSValidation {

	//private static final Logger LOGGER = Logger.getLogger(SucursalMockValidation.class);

	private RedLinkGenericService<Sucursal> sucursalService;

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.validation.FSValidation#validate(ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO)
	 */
	@Override
	public ValidationDTO validate(ValidationDTO validation)	throws FinancieroException {
		nullCheck(validation, ValidationDTO.SUCURSAL_COD, FINA103);

		Sucursal sucursal = null;

		//try {
			
			/*List<Sucursal> sucursales = getSucursalService()
					.getAll(new SucursalByCodAndEntidadSearchFilter(
							(String) validation.getInputValue(ValidationDTO.SUCURSAL_COD), 
							(Integer) validation.getOutputValue(ValidationDTO.ENTIDAD_ID)),
							null).getResult();
			*/
			List<Sucursal> sucursales = new ArrayList();
			
			sucursal = new Sucursal();
			
			EstadoRegistro e= new EstadoRegistro();
			e.setEstadoRegistroId((short)0);
			e.setCodigo('0');
			
			sucursal.setCodSucursal("1325");
			sucursal.setDescripcion("RAPIPAGO CENTRAL");
			sucursal.setEstadoRegistro(e);
			sucursal.setSucursalId(9451325L);
			
			Agente ag = new Agente();
			ag.setAgenteId(94500000001L);
			sucursal.setAgente(ag);
			
			Red r= new Red();
			r.setRedId(94509451);
			sucursal.setRed(r);
			
			sucursales.add(sucursal);
		
			if (sucursales.size() == 1) {
				sucursal = sucursales.get(0);
			}
		//} catch (RedLinkServiceException e) {
		//	throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		//}

		if (null == sucursal) {
			throw new FinancieroValidationException(FINA105.getCode(),	FINA105.getMsg());
		} else {
			validateEstadoRegistro(sucursal, FINA104);
			validateDates(sucursal, FINA104);
			validation.addOutputValue(ValidationDTO.SUCURSAL_ID,sucursal.getSucursalId());
			validation.addOutputValue(ValidationDTO.AGENTE_ID, sucursal	.getAgente().getAgenteId());
			validation.addOutputValue(ValidationDTO.RED_ID, sucursal.getRed().getRedId());
		}

		nextValidation(validation);

		return validation;
	}

	/**
	 * @return the sucursalService
	 */
	public RedLinkGenericService<Sucursal> getSucursalService() {
		return sucursalService;
	}

	/**
	 * @param sucursalService
	 *            the sucursalService to set
	 */
	public void setSucursalService(
			RedLinkGenericService<Sucursal> sucursalService) {
		this.sucursalService = sucursalService;
	}
}
