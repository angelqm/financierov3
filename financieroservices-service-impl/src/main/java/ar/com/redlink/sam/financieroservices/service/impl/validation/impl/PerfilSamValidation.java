/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  01/04/2015 - 15:43:05
 */

package ar.com.redlink.sam.financieroservices.service.impl.validation.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA129;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA130;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA135;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA147;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA200;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import ar.com.redlink.framework.services.crud.RedLinkGenericService;
import ar.com.redlink.framework.services.exception.RedLinkServiceException;
import ar.com.redlink.sam.financieroservices.dao.search.filter.PerfilSamByIdSearchFilter;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.PerfilFinanciero;
import ar.com.redlink.sam.financieroservices.entity.PerfilSam;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;

/**
 * Valida el PerfilSAM y obtiene el Perfil Financiero vinculado.
 * 
 * @author aguerrea
 * 
 */
public class PerfilSamValidation extends AbstractFSValidation {

	private static final Logger LOGGER = Logger.getLogger(PerfilSamValidation.class);

	private RedLinkGenericService<PerfilSam> perfilSamService;

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.validation.FSValidation#validate(ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO)
	 */
	@Override
	public ValidationDTO validate(ValidationDTO validation)	throws FinancieroException {
		PerfilSam perfilSam = null;

		try {
			
			//LOGGER.info("[DAO INI] PerfilSamByIdSearchFilter");
			
			perfilSam = perfilSamService.getAll(new PerfilSamByIdSearchFilter((Long) validation
							.getOutputValue(ValidationDTO.PERFIL_SAM_ID)),
							null).getResult().get(0);
			
			//LOGGER.info("[DAO INI] PerfilSamByIdSearchFilter");
			
		} catch (RedLinkServiceException e) {
			LOGGER.error("Error al acceder el servicio de PerfilSam: "	+ e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}
		validateEstadoRegistro(perfilSam, FINA135);
		validateDates(perfilSam, FINA135);

		Set<PerfilFinanciero> perfilFinancieroSet = perfilSam.getPerfilesFinanciero();

		List<PerfilFinanciero> perfilFinancieroValid = new ArrayList<PerfilFinanciero>();

		for (PerfilFinanciero perfilFinanciero : perfilFinancieroSet) {

			try {
				validateEstadoRegistro(perfilFinanciero, FINA129);
				validateDates(perfilFinanciero, FINA129);
			} catch (FinancieroValidationException e) {
				LOGGER.error("Se registra una relacion (ID:"+ perfilFinanciero.getPerfilFinancieroId()
						+ ") que no cumple con el estado o fecha de vigencia: "
						+ e.getMsj());
				continue;
			}

			perfilFinancieroValid.add(perfilFinanciero);
		}
		int size = perfilFinancieroValid.size();

		if (size == 1) {
			validation.addOutputValue(ValidationDTO.PERFIL_FINANCIERO_ID,perfilFinancieroValid.get(0).getPerfilFinancieroId());
		} else {
			if (size == 0) {
				throw new FinancieroValidationException(FINA147.getCode(),	FINA147.getMsg());
			} else {
				throw new FinancieroValidationException(FINA130.getCode(),	FINA130.getMsg());
			}
		}

		nextValidation(validation);

		return validation;
	}

	/**
	 * @return the perfilSamService
	 */
	public RedLinkGenericService<PerfilSam> getPerfilSamService() {
		return perfilSamService;
	}

	/**
	 * @param perfilSamService
	 *            the perfilSamService to set
	 */
	public void setPerfilSamService(RedLinkGenericService<PerfilSam> perfilSamService) {
		this.perfilSamService = perfilSamService;
	}

}