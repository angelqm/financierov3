/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  12/04/2015 - 15:49:15
 */

package ar.com.redlink.sam.financieroservices.service.impl.validation.rules;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA200;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import ar.com.redlink.sam.financieroservices.dto.LimiteDTO;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.service.TrxFinancieroService;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.impl.PerfilSamValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.rules.impl.LimiteCantidadRuleStrategy;
import ar.com.redlink.sam.financieroservices.service.impl.validation.rules.impl.LimiteMontoRuleStrategy;

/**
 * Componente para la composicion de reglas de negocio, a partir de Limites.
 * 
 * @author aguerrea
 * 
 */
public class RulesComposer {

	private static final Logger LOGGER = Logger.getLogger(PerfilSamValidation.class);

	private Map<String, Class<? extends LimiteRuleStrategy>> ruleStrategies;
	private Map<String, String> fieldMapping;
	private TrxFinancieroService trxFinancieroService;

	/**
	 * Constructor default, inicializa el mapa de rules.
	 */
	public RulesComposer() {
		ruleStrategies = new ConcurrentHashMap<String, Class<? extends LimiteRuleStrategy>>();
		ruleStrategies.put("Monto", LimiteMontoRuleStrategy.class);
		ruleStrategies.put("Cantidad", LimiteCantidadRuleStrategy.class);

		fieldMapping = new ConcurrentHashMap<String, String>();
		fieldMapping.put("Entidad", ValidationDTO.ENTIDAD_ID);
		fieldMapping.put("Red", ValidationDTO.RED_ID);
		fieldMapping.put("Agente", ValidationDTO.AGENTE_ID);
		fieldMapping.put("Sucursal", ValidationDTO.SUCURSAL_ID);
		fieldMapping.put("Servidor", ValidationDTO.SERVIDOR_ID);
		fieldMapping.put("Terminal", ValidationDTO.TERMINAL_SAM_ID);
		fieldMapping.put("Monto", ValidationDTO.IMPORTE);
	}

	/**
	 * Compone las reglas a partir de los DTOs recibidos, con el strategy
	 * correspondiente para su validacion. Obtiene el valor de ID del DTO
	 * correspondiente en base al campo procesado.
	 * 
	 * @param limiteDTOList
	 * @param validationDTO
	 * @throws FinancieroException
	 */
	public List<LimiteRuleStrategy> compose(List<LimiteDTO> limiteDTOList,
			ValidationDTO validationDTO) throws FinancieroException {
		List<LimiteRuleStrategy> returnList = new ArrayList<LimiteRuleStrategy>();

		for (LimiteDTO limiteDTO : limiteDTOList) {
			String[] nombreArray = limiteDTO.getNombreLimite().split(" ");

			if (nombreArray.length == 3) {
				LimiteRuleStrategy ruleStrategy;
				try {
					String campo = nombreArray[2];
					String strategyName = nombreArray[0];
					ruleStrategy = ruleStrategies.get(strategyName)	.newInstance();
					ruleStrategy.setCampo(nombreArray[2]);

					String cantidad = validationDTO.getInputValue(fieldMapping.get(strategyName));

					if (cantidad == null) {
						cantidad = "1";
					}

					ruleStrategy.setCantidad(Double.parseDouble(cantidad));
					ruleStrategy.setValor(Double.parseDouble(limiteDTO
							.getValorLimite()));

					// Habria que agregar un flag a nivel de BDD para ver si es
					// necesario usar la tarjeta o no.
					ruleStrategy.setTokenizado(validationDTO.getInputValue(ValidationDTO.TOKENIZADO));
					Long codigoOp = null;

					if (null != validationDTO.getInputValue(ValidationDTO.PREFIJO_OP_ID)) {
						codigoOp = Long.valueOf(validationDTO.getInputValue(ValidationDTO.PREFIJO_OP_ID));
					}

					ruleStrategy.setCodigoOp(codigoOp);

					// Por ahora se deja con la fecha del dia, si eventualmente
					// es necesario algo mensual, se puede agregar un campo en
					// la tabla o un string a parsear en la descripcion.
					DateTime now = DateTime.now();

					Date nowStartDate = new Date(now.withTimeAtStartOfDay().getMillis());
					Date nowEndDate = new Date(now.withTime(23, 59, 59, 999).getMillis());

					ruleStrategy.setDateFrom(nowStartDate);
					ruleStrategy.setDateTo(nowEndDate);

					ruleStrategy.setTrxFinancieroService(trxFinancieroService);

					String fieldMappingValue = fieldMapping.get(campo);

					String id = validationDTO.getInputValue(fieldMappingValue);

					id = typeToString(validationDTO, fieldMappingValue, id);

					ruleStrategy.setId(id);
					returnList.add(ruleStrategy);
				} catch (Exception e) {
					LOGGER.error("Error al procesar una regla: " + e.getMessage(), e);
					throw new FinancieroException(FINA200.getCode(),FINA200.getMsg());
				}
			}
		}

		return returnList;
	}

	/**
	 * Convierte a String el campo dado, obteniendolo del ValidationDTO
	 * recibido.
	 * 
	 * @param validationDTO
	 * @param fieldMappingValue
	 * @param id
	 * @return String con el valor.
	 */
	private String typeToString(ValidationDTO validationDTO,
			String fieldMappingValue, String id) {
		if (null == id) {
			// Type Safety en todo su esplendor...
			Object value = validationDTO.getOutputValue(fieldMappingValue);
			if (value instanceof Long) {
				id = Long.toString((Long) value);
			} else {
				if (value instanceof Double) {
					id = Double.toString((Double) value);
				} else {
					id = Integer.toString((Integer) value);
				}
			}
		}
		return id;
	}

	/**
	 * @return the ruleStrategies
	 */
	public Map<String, Class<? extends LimiteRuleStrategy>> getRuleStrategies() {
		return ruleStrategies;
	}

	/**
	 * @param ruleStrategies
	 *            the ruleStrategies to set
	 */
	public void setRuleStrategies(Map<String, Class<? extends LimiteRuleStrategy>> ruleStrategies) {
		this.ruleStrategies = ruleStrategies;
	}

	/**
	 * @return the fieldMapping
	 */
	public Map<String, String> getFieldMapping() {
		return fieldMapping;
	}

	/**
	 * @param fieldMapping
	 *            the fieldMapping to set
	 */
	public void setFieldMapping(Map<String, String> fieldMapping) {
		this.fieldMapping = fieldMapping;
	}

	/**
	 * @return the trxFinancieroService
	 */
	public TrxFinancieroService getTrxFinancieroService() {
		return trxFinancieroService;
	}

	/**
	 * @param trxFinancieroService
	 *            the trxFinancieroService to set
	 */
	public void setTrxFinancieroService(
			TrxFinancieroService trxFinancieroService) {
		this.trxFinancieroService = trxFinancieroService;
	}
}
