/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  01/04/2015 - 18:34:39
 */

package ar.com.redlink.sam.financieroservices.service.impl.validation.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA133;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA134;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA148;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA200;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ar.com.redlink.framework.services.crud.RedLinkGenericService;
import ar.com.redlink.framework.services.exception.RedLinkServiceException;
import ar.com.redlink.sam.financieroservices.dao.search.filter.GrupoPrefijoOpByPerfilPrefijoGrupoIdAndOpIdSearchFilter;
import ar.com.redlink.sam.financieroservices.dao.search.filter.GrupoPrefijoOpReglaByGrupoPrefijoOpIdSearchFilter;
import ar.com.redlink.sam.financieroservices.dto.LimiteDTO;
import ar.com.redlink.sam.financieroservices.dto.OperacionDTO;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.GrupoPrefijoOp;
import ar.com.redlink.sam.financieroservices.entity.GrupoPrefijoOpRegla;
import ar.com.redlink.sam.financieroservices.entity.PrefijoOp;
import ar.com.redlink.sam.financieroservices.entity.TipoPrefijoRegla;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;

/**
 * Validacion de {@link GrupoPrefijoOp} a partir de un ID de PerfilPrefijoGrupo.
 * 
 * @author aguerrea
 * 
 */
public class GrupoPrefijoOpValidation extends AbstractFSValidation {

	private static final Logger LOGGER = Logger
			.getLogger(GrupoPrefijoOpValidation.class);

	private RedLinkGenericService<GrupoPrefijoOp> grupoPrefijoOpService;
	private RedLinkGenericService<GrupoPrefijoOpRegla> grupoPrefijoOpReglaService;

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.service.validation.FSValidation#validate(ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO)
	 */
	@Override
	public ValidationDTO validate(ValidationDTO validation)
			throws FinancieroException {
		List<GrupoPrefijoOp> grupoPrefijoOps = null;

		try {
			//LOGGER.info("[DAO INI] GrupoPrefijoOpByPerfilPrefijoGrupoIdAndOpIdSearchFilter");
			
			grupoPrefijoOps = getGrupoPrefijoOpService().getAll(new GrupoPrefijoOpByPerfilPrefijoGrupoIdAndOpIdSearchFilter(
							(Long) validation.getOutputValue(ValidationDTO.PERFIL_PREFIJO_GRUPO_ID),
							validation.getInputValue(ValidationDTO.PREFIJO_OP_ID)),
							null).getResult();
			
			//LOGGER.info("[DAO FIN] GrupoPrefijoOpByPerfilPrefijoGrupoIdAndOpIdSearchFilter");
			
		} catch (RedLinkServiceException e) {
			LOGGER.error("Error al acceder el servicio de GrupoPrefijoOp: "	+ e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}

		List<OperacionDTO> operacionDTOs = new ArrayList<OperacionDTO>();

		for (GrupoPrefijoOp grupoPrefijoOp : grupoPrefijoOps) {
			try {
				validateEstadoRegistro(grupoPrefijoOp, FINA133);
				validateDates(grupoPrefijoOp, FINA133);
			} catch (FinancieroValidationException e) {
				LOGGER.warn("Se proceso un GrupoPrefijoOp y se rechazo por invalidez pero se continua con el procesamiento: "
						+ e.getCod() + " " + e.getMsj());
				continue;
			}
			Long grupoPrefijoOpId = grupoPrefijoOp.getGrupoPrefijoOpId();

			List<GrupoPrefijoOpRegla> grupoPrefijoOpReglas = null;

			try {
				grupoPrefijoOpReglas = getGrupoPrefijoOpReglaService().getAll(
						new GrupoPrefijoOpReglaByGrupoPrefijoOpIdSearchFilter(
								grupoPrefijoOpId), null).getResult();
			} catch (RedLinkServiceException e) {
				LOGGER.error("Error al acceder el servicio de GrupoPrefijoOpRegla: "+ e.getMessage(), e);
				throw new FinancieroException(FINA200.getCode(),FINA200.getMsg());
			}

			List<LimiteDTO> limiteDTOs = new ArrayList<LimiteDTO>();

			for (GrupoPrefijoOpRegla grupoPrefijoOpRegla : grupoPrefijoOpReglas) {
				try {
					validateEstadoRegistro(grupoPrefijoOpRegla, FINA134);
					validateDates(grupoPrefijoOpRegla, FINA134);

					validateEstadoTipoPrefijoRegla(grupoPrefijoOpRegla.getTipoPrefijoRegla());
				} catch (FinancieroValidationException e) {
					LOGGER.warn("Se proceso un GrupoPrefijoOpRegla y se rechazo por invalidez ("
							+ grupoPrefijoOpRegla.getGrupoPrefijoOpReglaId()
							+ ") pero se continua con el procesamiento: "
							+ e.getCod() + "  " + e.getMsj());
					continue;
				}

				LimiteDTO limiteDTO = new LimiteDTO(grupoPrefijoOpRegla
						.getTipoPrefijoRegla().getNombre(), grupoPrefijoOpRegla
						.getTipoPrefijoRegla().getDescripcion(),
						grupoPrefijoOpRegla.getValor());
				limiteDTO.setPrecisionValorLimite(grupoPrefijoOpRegla.getTipoPrefijoRegla().getPrecisionValor());

				limiteDTOs.add(limiteDTO);
			}
			PrefijoOp prefijoOp = grupoPrefijoOp.getPrefijoOpEntidad().getPrefijoOp();

			OperacionDTO operacionDTO = new OperacionDTO();
			operacionDTO.setCodigoOperacion(prefijoOp.getCodigo());
			operacionDTO.setIdOperacion(Long.toString(prefijoOp.getPrefijoOpId()));
			operacionDTO.setNombreOperacion(prefijoOp.getNombre());
			operacionDTO.setHabilitada(grupoPrefijoOp.getEstadoRegistro().getCodigo() == '0');
			operacionDTO.setDescripcionOperacion(prefijoOp.getDescripcion());
			operacionDTO.setRequiereAut(grupoPrefijoOp.isRequiereAut());
			operacionDTO.setRequiereDNI(grupoPrefijoOp.isRequiereDni());
			operacionDTO.setRequierePin(grupoPrefijoOp.isRequierePin());
			operacionDTO.setRequiereSeg(grupoPrefijoOp.isRequiereSeg());
			operacionDTO.setRequiereSfx(grupoPrefijoOp.isRequiereSfx());
			operacionDTO.setTicketFirma(grupoPrefijoOp.isTicketFirma());
			operacionDTO.setRequiereTar(grupoPrefijoOp.isRequiereTar());
			operacionDTO.setTicketCantidadCopias(grupoPrefijoOp.getTicketCantidadCopias());
			operacionDTO.setLimites(limiteDTOs);

			operacionDTOs.add(operacionDTO);
		}

		validation.addOutputValue(ValidationDTO.OPERACION_DTOS, operacionDTOs);

		nextValidation(validation);

		return validation;
	}

	/**
	 * Valida el estado de TipoPrefijoRegla acorde al campo "Habilitado".
	 * 
	 * @param tipoPrefijoRegla
	 * @throws FinancieroValidationException
	 */
	private void validateEstadoTipoPrefijoRegla(
			TipoPrefijoRegla tipoPrefijoRegla)
			throws FinancieroValidationException {
		if (!tipoPrefijoRegla.isHabilitado()) {
			throw new FinancieroValidationException(FINA148.getCode(),FINA148.getMsg());
		}
	}

	/**
	 * @return the grupoPrefijoOpService
	 */
	public RedLinkGenericService<GrupoPrefijoOp> getGrupoPrefijoOpService() {
		return grupoPrefijoOpService;
	}

	/**
	 * @param grupoPrefijoOpService
	 *            the grupoPrefijoOpService to set
	 */
	public void setGrupoPrefijoOpService(
			RedLinkGenericService<GrupoPrefijoOp> grupoPrefijoOpService) {
		this.grupoPrefijoOpService = grupoPrefijoOpService;
	}

	/**
	 * @return the grupoPrefijoOpReglaService
	 */
	public RedLinkGenericService<GrupoPrefijoOpRegla> getGrupoPrefijoOpReglaService() {
		return grupoPrefijoOpReglaService;
	}

	/**
	 * @param grupoPrefijoOpReglaService
	 *            the grupoPrefijoOpReglaService to set
	 */
	public void setGrupoPrefijoOpReglaService(
			RedLinkGenericService<GrupoPrefijoOpRegla> grupoPrefijoOpReglaService) {
		this.grupoPrefijoOpReglaService = grupoPrefijoOpReglaService;
	}

}
