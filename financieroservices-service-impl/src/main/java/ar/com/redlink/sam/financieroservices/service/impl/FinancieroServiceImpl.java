/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 */
package ar.com.redlink.sam.financieroservices.service.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA136;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA164;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA200;

import java.math.BigDecimal;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import ar.com.link.connector.tandem.messages.TandemResponse;
import ar.com.redlink.framework.rest.common.exception.RedLinkRESTServiceException;
import ar.com.redlink.framework.services.exception.RedLinkServiceException;
import ar.com.redlink.sam.financieroservices.dao.search.filter.TrxFinancieroByTerminalSamIdMaxResSearchFilter;
import ar.com.redlink.sam.financieroservices.dao.search.filter.TrxFinancieroByTerminalSamIdReqAltaSearchFilter;
import ar.com.redlink.sam.financieroservices.dto.CuentaDTO;
import ar.com.redlink.sam.financieroservices.dto.OperacionDTO;
import ar.com.redlink.sam.financieroservices.dto.RequerimientoDTO;
import ar.com.redlink.sam.financieroservices.dto.TrxFinancieroDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularDebitoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularDepositoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionCuotasRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionPrestamoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.BalanceRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ConsultaOperacionPEIPagarRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ConsultaOperacionPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.DestinoPEIPagarPagoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.EnrolamientoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ExtraccionRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.HeaderPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDebitoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDepositoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoPEIPagarRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoParcialPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarExtraccionCuotasRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarExtraccionPrestamoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarPagoDebitoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaPEIPagarRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaPEITrackDataRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ResultadoOperacionRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.TerminalRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ValidarTarjetaRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.WorkingKeyRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.common.FinancieroServiceBaseRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.response.AnularDebitoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.AnularDepositoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.AnularExtraccionCuotasDTO;
import ar.com.redlink.sam.financieroservices.dto.response.AnularExtraccionDTO;
import ar.com.redlink.sam.financieroservices.dto.response.AnularExtraccionPrestamoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ConsultaOperacionPEIDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ConsultaOperacionPEIPagarDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ConsultaOperacionResultadoPEIDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ConsultaOperacionResultadoPEIPagarDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ConsultaPEIDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ConsultaTipoDocumentoPEIDTO;
import ar.com.redlink.sam.financieroservices.dto.response.DesencriptarTarjetaDTO;
import ar.com.redlink.sam.financieroservices.dto.response.EnrolamientoStatusDTO;
import ar.com.redlink.sam.financieroservices.dto.response.EstadoCuentaDTO;
import ar.com.redlink.sam.financieroservices.dto.response.EstadoOperacionDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ExtraccionCuotasDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ExtraccionPrestamoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ExtraccionResultadoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarDebitoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarDepositoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarDevolucionResultadoPEIDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarDevolucionResultadoPEIPagarDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarPagoDebitoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarTransferenciaDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarTransferenciaPEIDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarTransferenciaPEIPagarDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarTransferenciaResultadoPEIDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarTransferenciaResultadoPEIPagarDTO;
import ar.com.redlink.sam.financieroservices.dto.response.TipoDocumentoPEIDTO;
import ar.com.redlink.sam.financieroservices.dto.response.UltimasOperacionesDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ValidacionTarjetaDTO;
import ar.com.redlink.sam.financieroservices.dto.response.WorkingKeyDTO;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.EstadoTrx;
import ar.com.redlink.sam.financieroservices.entity.Intencion;
import ar.com.redlink.sam.financieroservices.entity.Pago;
import ar.com.redlink.sam.financieroservices.entity.PrefijoEntidad;
import ar.com.redlink.sam.financieroservices.entity.TrxFinanciero;
import ar.com.redlink.sam.financieroservices.entity.util.DateUtil;
import ar.com.redlink.sam.financieroservices.pei.response.ConsultaOperacionPEIPagarResponse;
import ar.com.redlink.sam.financieroservices.pei.response.ConsultaOperacionPEIResponse;
import ar.com.redlink.sam.financieroservices.pei.response.OperacionesPEIResponse;
import ar.com.redlink.sam.financieroservices.pei.response.RealizarDevolucionPEIPagarResponse;
import ar.com.redlink.sam.financieroservices.pei.response.RealizarDevolucionPEIResponse;
import ar.com.redlink.sam.financieroservices.pei.response.TipoDocumentoPEIResponse;
import ar.com.redlink.sam.financieroservices.pei.response.TransferenciaPEIPagarResponse;
import ar.com.redlink.sam.financieroservices.pei.response.ConsultaTipoDocumentoPEIResponse;
import ar.com.redlink.sam.financieroservices.pei.response.TransferenciaPEIResponse;
import ar.com.redlink.sam.financieroservices.pei.service.PeiService;
import ar.com.redlink.sam.financieroservices.pei.service.utils.ServiceUtils;
import ar.com.redlink.sam.financieroservices.service.EnrolamientoSamService;
import ar.com.redlink.sam.financieroservices.service.PrefijoEntidadService;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.translation.TrxFinancieroTranslator;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.impl.DevolucionPEIValidation;
import ar.com.redlink.sam.financieroservices.tandem.response.AnulacionDepositoResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.AnulacionExtraccionResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.ConsultaExtraccionResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.ConsultaSaldoResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.DepositoResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.ExtraccionResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.TransferenciaResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.WorkingKeyResponse;
import ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService;
import ar.com.redlink.sam.financieroservices.tandem.service.TandemWorkingKeyService;

/**
 * Implementacion del servicio de operaciones del modulo financiero.
 * 
 * @author aguerrea
 * 
 */
public class FinancieroServiceImpl extends AbstractFinancieroService {

	private static final String NRO_TARJETA_FICTICIA = "999999999";
	private static final String ESTADO_OPERACION_ACEPTADA = "ACEPTADA";
	private static final String ESTADO_OPERACION_ACTIVA = "0";
	private static final String ESTADO_OPERACION_INCOMPLETA = "3";
	private static final String FECHA_NULL = "1900-01-01T00:00:00Z";
	private static final String VALOR_NULL = "-1";

	private static final Logger LOGGER = Logger.getLogger(FinancieroServiceImpl.class);
	private TandemWorkingKeyService tandemWorkingKeyService;
	private TandemAccountService tandemAccountService;
	private EnrolamientoSamService enrolamientoSamService;
	private AbstractFSValidation operacionLimitesValidation;
	private PeiService peiService;
	private Integer maxResults;
	private PrefijoEntidadService prefijoEntidadService;

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.service.FinancieroService#enroll(ar.com.redlink.sam.financieroservices.dto.request.EnrolamientoRequestDTO)
	 */
	@Override
	public EnrolamientoStatusDTO enroll(EnrolamientoRequestDTO enrolamiento) throws FinancieroException {
		enrolamientoSamService.enrollDevice(enrolamiento);

		EnrolamientoStatusDTO status = new EnrolamientoStatusDTO();

		status.setCodRta("00");
		status.setEstado("OK");

		return status;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.service.FinancieroService#getWorkingKey(ar.com.redlink.sam.financieroservices.dto.request.WorkingKeyRequestDTO)
	 */
	@Override
	public WorkingKeyDTO getWorkingKey(WorkingKeyRequestDTO workingKey, Intencion in) throws FinancieroException {

		Long trxFinancieroId = this.getTrxFinancieroService().getReversableOperationIds(workingKey.getSecuenciaOn(),
				workingKey.getTerminalSamId(), prefijoOpService.getNotReversablePrefijoOps());
		boolean existe = null != trxFinancieroId;

		if (existe) {
			this.registerReverse(trxFinancieroId, workingKey.getIdRequerimiento());
		}

		WorkingKeyResponse response = tandemWorkingKeyService.getWorkingKey(workingKey);
		WorkingKeyDTO returnValue = new WorkingKeyDTO(response.getClaveComm());
		returnValue.setSecuenciaOn(response.getSeqNum());

		try {
			returnValue.setFechaOn(DateUtil.fechaOnExtFormat(response.getPostDat()));
		} catch (ParseException e) {
			LOGGER.error("Error al parsear la fecha on: " + e.getMessage() + " Se continua con la ejecucion.");
		}

		workingKey.setTokenizado("-");
		workingKey.setSecuenciaOnRta(returnValue.getSecuenciaOn());
		workingKey.setPrefijoOpId(prefijoOpService.getPrefijoOpCod("getWorkingKey"));
		workingKey.setFechaOn(returnValue.getFechaOn());

		// this.registerTrx(workingKey);
		this.registerWorkingKey(workingKey, in);
		return returnValue;
	}

	/**
	 * @throws FinancieroException
	 * @see ar.com.redlink.sam.financieroservices.service.FinancieroService#getBalance(ar.com.redlink.sam.financieroservices.dto.request.BalanceRequestDTO)
	 */
	@Override
	public EstadoCuentaDTO getBalance(BalanceRequestDTO balance, Intencion in) throws FinancieroException {
		String cod = prefijoOpService.getPrefijoOpCod("getBalance");

		Long trxFinancieroId = this.getTrxFinancieroService().getReversableOperationIds(balance.getSecuenciaOn(),
				balance.getTerminalSamId(), prefijoOpService.getNotReversablePrefijoOps());
		boolean existe = null != trxFinancieroId;

		if (existe) {
			this.registerReverse(trxFinancieroId, balance.getIdRequerimiento());
		}

		EstadoCuentaDTO dtoResponse = null;
		balance.setPrefijoOpId(cod);

		ValidationDTO validation = validate(balance, balance.getTokenizado(), true);
		operacionLimitesValidation.validate(validation);
		ConsultaSaldoResponse tandemResponse = tandemAccountService.getBalance(balance);
		dtoResponse = modelMapperService.modelMapperHelper(tandemResponse, EstadoCuentaDTO.class);
		populateResponseData(dtoResponse, tandemResponse);		
		String disponible = tandemResponse.getDineroDisponible();
		dtoResponse.setDisponible(this.addComma(disponible.substring(1, disponible.length())));		
		String saldo = tandemResponse.getSaldo();
		dtoResponse.setSaldo(addComma(saldo));						
		balance.setFechaOn(dtoResponse.getFechaOn());
		balance.setSecuenciaOnRta(dtoResponse.getSecuenciaOn());

		// this.registerTrx(balance);
		this.getSaldo(balance, in);
		return dtoResponse;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.service.FinancieroService#validateCard(ar.com.redlink.sam.financieroservices.dto.request.ValidarTarjetaRequestDTO)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ValidacionTarjetaDTO validateCard(ValidarTarjetaRequestDTO validarTarjeta, Intencion in)
			throws FinancieroException {
		final String cod = prefijoOpService.getPrefijoOpCod("validateCard");

		Long trxFinancieroId = this.getTrxFinancieroService().getReversableOperationIds(validarTarjeta.getSecuenciaOn(),
				validarTarjeta.getTerminalSamId(), prefijoOpService.getNotReversablePrefijoOps());

		boolean existe = null != trxFinancieroId;

		if (existe) {
			this.registerReverse(trxFinancieroId, validarTarjeta.getIdRequerimiento());
		}

		ValidacionTarjetaDTO dtoResponse = new ValidacionTarjetaDTO();
		ConsultaExtraccionResponse tandemResponse = tandemAccountService.validateCard(validarTarjeta);
		String pan = extractPan(tandemResponse);
		ValidationDTO validation = validate(validarTarjeta, pan, false);
		populateResponseData(dtoResponse, tandemResponse);
		boolean hideAccounts = false;

		try {
			validation.addInputValue(ValidationDTO.PREFIJO_OP_ID, cod);
			operacionLimitesValidation.validate(validation);
		} catch (FinancieroValidationException fve) {
			LOGGER.error("Ocurrio un error de validacion: " + fve.getCod() + " " + fve.getMsj());
			hideAccounts = true;
			dtoResponse.setCodError(fve.getCod());
			dtoResponse.setDescripcionError(fve.getMsj());
			in.setObservacion(in.getObservacion() + "*" + fve.getCod() + "*" + fve.getMsj());
			this.registrarErrorTransaccion(in);
		}

		List<CuentaDTO> cuentas = extractAccounts(tandemResponse);
		dtoResponse.setEstadoTarjeta(Character.toString(tandemResponse.getData().charAt(0)));

		List<OperacionDTO> operacionDTOs = (List<OperacionDTO>) validation.getOutputValue(ValidationDTO.OPERACION_DTOS);

		if (!hideAccounts) {
			dtoResponse.setCuentas(cuentas);

		} else {
			// Si hideAccounts es true, significa que requiere un dato que no se
			// proveyo y se devuelve
			// unicamente la 62.
			CollectionUtils.filter(operacionDTOs, new Predicate() {
				/**
				 * @see org.apache.commons.collections.Predicate#evaluate(java.lang.Object)
				 */
				@Override
				public boolean evaluate(Object object) {
					return cod.equals(((OperacionDTO) object).getIdOperacion());
				}
			});
		}

		dtoResponse.setTokenizado(tokenServiceConectado.getToken(pan));
		dtoResponse.setOperaciones(operacionDTOs);
		dtoResponse.setEntidadEmisora(tandemResponse.getCrdFiid());

		validarTarjeta.setTokenizado(dtoResponse.getTokenizado());
		validarTarjeta.setFechaOn(dtoResponse.getFechaOn());
		validarTarjeta.setPrefijoOpId(cod); // Redundante para registro de trx

		validarTarjeta.setSecuenciaOnRta(dtoResponse.getSecuenciaOn());
		// this.registerTrx(validarTarjeta);
		this.validarTarjeta(validarTarjeta, in);
		return dtoResponse;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.service.FinancieroService#validateCard(ar.com.redlink.sam.financieroservices.dto.request.ValidarTarjetaRequestDTO)
	 */
	private DesencriptarTarjetaDTO decryptValidateCard(ValidarTarjetaRequestDTO validarTarjeta)
			throws FinancieroException {
		DesencriptarTarjetaDTO dtoResponse = null;

		try {
			Long trxFinancieroId = this.getTrxFinancieroService().getReversableOperationIds(
					validarTarjeta.getSecuenciaOn(), validarTarjeta.getTerminalSamId(),
					prefijoOpService.getNotReversablePrefijoOps());

			boolean existe = null != trxFinancieroId;

			if (existe) {
				this.registerReverse(trxFinancieroId, validarTarjeta.getIdRequerimiento());
			}

			dtoResponse = new DesencriptarTarjetaDTO();

			ConsultaExtraccionResponse tandemResponse = tandemAccountService.decryptCard(validarTarjeta);

			dtoResponse.setTrack1(tandemResponse.getData().substring(0, 80).trim());
			dtoResponse.setTrack2(tandemResponse.getTrack2().trim());
			dtoResponse.setSecuenciaOn(tandemResponse.getSeqNum());

			validarTarjeta(validarTarjeta, tandemResponse);

		} catch (FinancieroException e) {
			LOGGER.error("Error al desencriptar el numero de tarjeta: " + e.getMsj(), e);
			throw e;
		}
		return dtoResponse;
	}

	private void validarTarjeta(ValidarTarjetaRequestDTO validarTarjeta, ConsultaExtraccionResponse tandemResponse)
			throws FinancieroException {
		String pan = extractPan(tandemResponse);
		PrefijoEntidad prefijoEntidad = getPrefijoEntidadService().getPrefijoEntidadByTarjeta(pan,
				validarTarjeta.getIdEntidad());
		if (prefijoEntidad == null) { // este es el caso para tarjetas banelco
			pan = NRO_TARJETA_FICTICIA; // Si es banelco se asigna una tarjeta			
										// ficticia PEI
		} else {
			//SOLO PARA FIINANCIEROPEI DESDE PAGAR
			//PAGOPEI/DEVOLUCIONPEI
			//SOLO SI EL PREFIJO ESTA DESHABILITADO
			if (prefijoEntidad.getEstadoRegistro().getEstadoRegistroId() != 0) {
				if ("29".equals(validarTarjeta.getPrefijoOpId()) || "30".equals(validarTarjeta.getPrefijoOpId())){
					pan = NRO_TARJETA_FICTICIA;
				}
			} 			
		}
		
		validate(validarTarjeta, pan, false);
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.FinancieroService#doWithdraw(ar.com.redlink.sam.financieroservices.dto.request.ExtraccionRequestDTO)
	 */
	@Override
	public ExtraccionResultadoDTO doWithdraw(ExtraccionRequestDTO extraccion, Intencion in) throws FinancieroException {

		String cod = prefijoOpService.getPrefijoOpCod("doWithdraw");
		extraccion.setPrefijoOpId(cod);
		Long trxFinancieroId = this.getTrxFinancieroService().getReversableOperationIds(extraccion.getSecuenciaOn(),
				extraccion.getTerminalSamId(), prefijoOpService.getNotReversablePrefijoOps());

		boolean existe = null != trxFinancieroId;

		if (existe) {
			this.registerReverse(trxFinancieroId, extraccion.getIdRequerimiento());
		}

		ExtraccionResultadoDTO dtoResponse = new ExtraccionResultadoDTO();
		ValidationDTO validation = validate(extraccion, extraccion.getTokenizado(), true);
		validation.addInputValue(ValidationDTO.REVERSA, Boolean.toString(existe));
		operacionLimitesValidation.validate(validation);

		ExtraccionResponse tandemResponse = tandemAccountService.doWithdraw(extraccion);

		populateResponseData(dtoResponse, tandemResponse);
		extractWithdrawData(tandemResponse, dtoResponse);
		extraccion.setFechaOn(dtoResponse.getFechaOn());
		extraccion.setHoraOn(tandemResponse.getOrigTim());
		extraccion.setSecuenciaOnRta(dtoResponse.getSecuenciaOn());

		// this.registerTrx(extraccion);
		this.doWithDraw(extraccion, in);
		return dtoResponse;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.service.FinancieroService#rollbackWithdraw(ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionRequestDTO)
	 */
	@Override
	public AnularExtraccionDTO rollbackWithdraw(AnularExtraccionRequestDTO anularExtraccion, Intencion in)
			throws FinancieroException {
		String cod = prefijoOpService.getPrefijoOpCod("rollbackWithdraw");
		String originalCod = prefijoOpService.getPrefijoOpCod("doWithdraw");

		anularExtraccion.setPrefijoOpId(cod);

		ValidationDTO validation = validate(anularExtraccion, anularExtraccion.getTokenizado(), true);
		operacionLimitesValidation.validate(validation);
		this.rollbackValidation(anularExtraccion, originalCod, validation);

		anularExtraccion.setTrxFinancieroId((Long) validation.getOutputValue(ValidationDTO.TRXT_FINANCIERO_ID));
		
		anularExtraccion.setFechaOn(validation.getOutputValue(ValidationDTO.FECHA_ON).toString());
		anularExtraccion.setHoraOn(validation.getOutputValue(ValidationDTO.HORA_ON).toString());
		
		AnularExtraccionDTO dtoResponse = new AnularExtraccionDTO();
		AnulacionExtraccionResponse tandemResponse = tandemAccountService.rollbackWithdraw(anularExtraccion);
		
		populateResponseData(dtoResponse, tandemResponse);

		// trxFinancieroService.annulTrx(anularExtraccion.getTrxFinancieroId(),
		// anularExtraccion.getIdRequerimiento());
		trxFinancieroService.cancelExtraction(anularExtraccion.getTrxFinancieroId(),
				anularExtraccion.getIdRequerimiento(), in);

		return dtoResponse;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.service.FinancieroService#doDebito(ar.com.redlink.sam.financieroservices.dto.request.RealizarDebitoRequestDTO)
	 */
	@Override
	public RealizarDebitoDTO doDebit(RealizarDebitoRequestDTO realizarDebitoRequestDTO, Intencion in)
			throws FinancieroException {
		String cod = prefijoOpService.getPrefijoOpCod("doDebit");
		realizarDebitoRequestDTO.setPrefijoOpId(cod);

		Long trxFinancieroId = this.getTrxFinancieroService().getReversableOperationIds(
				realizarDebitoRequestDTO.getSecuenciaOn(), realizarDebitoRequestDTO.getTerminalSamId(),
				prefijoOpService.getNotReversablePrefijoOps());

		boolean existe = null != trxFinancieroId;

		if (existe) {
			this.registerReverse(trxFinancieroId, realizarDebitoRequestDTO.getIdRequerimiento());
		}

		RealizarDebitoDTO dtoResponse = new RealizarDebitoDTO();
		ValidationDTO validation = validate(realizarDebitoRequestDTO, realizarDebitoRequestDTO.getTokenizado(), true);

		validation.addInputValue(ValidationDTO.REVERSA, Boolean.toString(existe));
		operacionLimitesValidation.validate(validation);

		ExtraccionResponse tandemResponse = tandemAccountService.doDebit(realizarDebitoRequestDTO);
		populateResponseData(dtoResponse, tandemResponse);
		extractWithdrawData(tandemResponse, dtoResponse);

		realizarDebitoRequestDTO.setFechaOn(dtoResponse.getFechaOn());
		realizarDebitoRequestDTO.setHoraOn(tandemResponse.getOrigTim());
		realizarDebitoRequestDTO.setSecuenciaOnRta(dtoResponse.getSecuenciaOn());
		this.debit(realizarDebitoRequestDTO, in);

		return dtoResponse;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.service.FinancieroService#doDebito(ar.com.redlink.sam.financieroservices.dto.request.RealizarDebitoRequestDTO)
	 */
	@Override
	public RealizarPagoDebitoDTO doDebitPay(RealizarPagoDebitoRequestDTO realizarPagoDebitoRequestDTO, Intencion in)
			throws FinancieroException {
		String cod = prefijoOpService.getPrefijoOpCod("doDebitPay");
		realizarPagoDebitoRequestDTO.setPrefijoOpId(cod);

		Long trxFinancieroId = this.getTrxFinancieroService().getReversableOperationIds(
				realizarPagoDebitoRequestDTO.getSecuenciaOn(), realizarPagoDebitoRequestDTO.getTerminalSamId(),
				prefijoOpService.getNotReversablePrefijoOps());

		boolean existe = null != trxFinancieroId;

		if (existe) {
			this.registerReverse(trxFinancieroId, realizarPagoDebitoRequestDTO.getIdRequerimiento());
		}

		RealizarPagoDebitoDTO dtoResponse = new RealizarPagoDebitoDTO();
		ValidationDTO validation = validate(realizarPagoDebitoRequestDTO, realizarPagoDebitoRequestDTO.getTokenizado(),
				true);
		validation.addInputValue(ValidationDTO.REVERSA, Boolean.toString(existe));
		operacionLimitesValidation.validate(validation);

		ExtraccionResponse tandemResponse = tandemAccountService.doDebitPay(realizarPagoDebitoRequestDTO);

		populateResponseData(dtoResponse, tandemResponse);
		extractWithdrawData(tandemResponse, dtoResponse);

		/*ACA TOMAR EL VALOR DE FECHA y HORA DE PAGO DEVUELTA POR TANDEM*/
		realizarPagoDebitoRequestDTO.setFechaOn(dtoResponse.getFechaOn());
		realizarPagoDebitoRequestDTO.setHoraOn(tandemResponse.getOrigTim());				
		realizarPagoDebitoRequestDTO.setSecuenciaOnRta(dtoResponse.getSecuenciaOn());
		
		// this.registerTrx(realizarPagoDebitoRequestDTO);
		this.doDebitPayment(realizarPagoDebitoRequestDTO, in);
		return dtoResponse;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.service.FinancieroService#rollbackDebit(ar.com.redlink.sam.financieroservices.dto.request.AnularDebitoRequestDTO)
	 */
	@Override
	public AnularDebitoDTO rollbackDebit(AnularDebitoRequestDTO anularDebitoRequestDTO, Intencion in)
			throws FinancieroException {
		String cod = prefijoOpService.getPrefijoOpCod("rollbackDebit");
		String originalCod = prefijoOpService.getPrefijoOpCod("doDebit");
		anularDebitoRequestDTO.setPrefijoOpId(cod);

		ValidationDTO validation = validate(anularDebitoRequestDTO, anularDebitoRequestDTO.getTokenizado(), true);

		operacionLimitesValidation.validate(validation);
		this.rollbackValidation(anularDebitoRequestDTO, originalCod, validation);

		anularDebitoRequestDTO.setTrxFinancieroId((Long) validation.getOutputValue(ValidationDTO.TRXT_FINANCIERO_ID));

		anularDebitoRequestDTO.setFechaOn(validation.getOutputValue(ValidationDTO.FECHA_ON).toString());
		anularDebitoRequestDTO.setHoraOn(validation.getOutputValue(ValidationDTO.HORA_ON).toString());
				
		AnularDebitoDTO dtoResponse = new AnularDebitoDTO();
		AnulacionExtraccionResponse tandemResponse = tandemAccountService.rollbackDebit(anularDebitoRequestDTO);
		populateResponseData(dtoResponse, tandemResponse);

		// trxFinancieroService.annulTrx(anularDebitoRequestDTO.getTrxFinancieroId(),anularDebitoRequestDTO.getIdRequerimiento());
		trxFinancieroService.rollbackDebit(anularDebitoRequestDTO.getTrxFinancieroId(),
				anularDebitoRequestDTO.getIdRequerimiento(), in);

		return dtoResponse;
	}

	/**
	 * AQM- PAGODEBITO 10D -PAGAR-
	 * @see ar.com.redlink.sam.financieroservices.service.FinancieroService#rollbackDebit(ar.com.redlink.sam.financieroservices.dto.request.AnularDebitoRequestDTO)
	 */
	@Override
	public AnularDebitoDTO rollbackDebitPay(AnularDebitoRequestDTO anularDebitoRequestDTO, Intencion in)
			throws FinancieroException {
		String cod = prefijoOpService.getPrefijoOpCod("rollbackDebitPay");
		String originalCod = prefijoOpService.getPrefijoOpCod("doDebitPay");
		anularDebitoRequestDTO.setPrefijoOpId(cod);

		ValidationDTO validation = validate(anularDebitoRequestDTO, anularDebitoRequestDTO.getTokenizado(), true);

		operacionLimitesValidation.validate(validation);
		this.rollbackDebitPayValidation(anularDebitoRequestDTO, originalCod, validation);

		anularDebitoRequestDTO.setTrxFinancieroId((Long) validation.getOutputValue(ValidationDTO.TRXT_FINANCIERO_ID));
		
		anularDebitoRequestDTO.setFechaOn(validation.getOutputValue(ValidationDTO.FECHA_ON).toString());
		anularDebitoRequestDTO.setHoraOn(validation.getOutputValue(ValidationDTO.HORA_ON).toString());
		
		AnularDebitoDTO dtoResponse = new AnularDebitoDTO();

		AnulacionExtraccionResponse tandemResponse = tandemAccountService.rollbackDebitPay(anularDebitoRequestDTO);
		populateResponseData(dtoResponse, tandemResponse);

		// trxFinancieroService.annulTrx(anularDebitoRequestDTO.getTrxFinancieroId(),anularDebitoRequestDTO.getIdRequerimiento());
		trxFinancieroService.anullDebitPay(anularDebitoRequestDTO.getTrxFinancieroId(),
				anularDebitoRequestDTO.getIdRequerimiento(), in);
		// trxFinancieroService.cancelExtraction(anularExtraccion
		// .getTrxFinancieroId(), anularExtraccion.getIdRequerimiento() , in);

		return dtoResponse;
	}

	/**
	 * @throws FinancieroException
	 * @see ar.com.redlink.sam.financieroservices.service.FinancieroService#doDeposit(ar.com.redlink.sam.financieroservices.dto.request.RealizarDepositoRequestDTO)
	 */
	@Override
	public RealizarDepositoDTO doDeposit(RealizarDepositoRequestDTO realizarDepositoRequestDTO, Intencion in)
			throws FinancieroException {
		String cod = prefijoOpService.getPrefijoOpCod("doDeposit");
		realizarDepositoRequestDTO.setPrefijoOpId(cod);

		Long trxFinancieroId = this.getTrxFinancieroService().getReversableOperationIds(
				realizarDepositoRequestDTO.getSecuenciaOn(), realizarDepositoRequestDTO.getTerminalSamId(),
				prefijoOpService.getNotReversablePrefijoOps());

		boolean existe = null != trxFinancieroId;

		if (existe) {
			this.registerReverse(trxFinancieroId, realizarDepositoRequestDTO.getIdRequerimiento());
		}

		RealizarDepositoDTO dtoResponse = new RealizarDepositoDTO();
		ValidationDTO validation = validate(realizarDepositoRequestDTO, realizarDepositoRequestDTO.getTokenizado(),
				true);

		validation.addInputValue(ValidationDTO.REVERSA, Boolean.toString(existe));
		operacionLimitesValidation.validate(validation);

		DepositoResponse tandemResponse = tandemAccountService.doDeposit(realizarDepositoRequestDTO);
		populateResponseData(dtoResponse, tandemResponse);
		realizarDepositoRequestDTO.setFechaOn(dtoResponse.getFechaOn());
		realizarDepositoRequestDTO.setSecuenciaOnRta(dtoResponse.getSecuenciaOn());
		// this.registerTrx(realizarDepositoRequestDTO);
		this.doDeposito(realizarDepositoRequestDTO, in);

		return dtoResponse;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.FinancieroService#rollbackDeposit(ar.com.redlink.sam.financieroservices.dto.request.AnularDepositoRequestDTO)
	 */
	@Override
	public AnularDepositoDTO rollbackDeposit(AnularDepositoRequestDTO anularDepositoRequestDTO, Intencion in)
			throws FinancieroException {
		String cod = prefijoOpService.getPrefijoOpCod("rollbackDeposit");
		String originalCod = prefijoOpService.getPrefijoOpCod("doDeposit");

		anularDepositoRequestDTO.setPrefijoOpId(cod);

		ValidationDTO validation = validate(anularDepositoRequestDTO, anularDepositoRequestDTO.getTokenizado(), true);

		operacionLimitesValidation.validate(validation);
		this.rollbackValidation(anularDepositoRequestDTO, originalCod, validation);

		anularDepositoRequestDTO.setTrxFinancieroId((Long) validation.getOutputValue(ValidationDTO.TRXT_FINANCIERO_ID));

		AnularDepositoDTO dtoResponse = new AnularDepositoDTO();
		AnulacionDepositoResponse tandemResponse = tandemAccountService.rollbackDeposit(anularDepositoRequestDTO);
		populateResponseData(dtoResponse, tandemResponse);

		// trxFinancieroService.annulTrx(anularDepositoRequestDTO.getTrxFinancieroId(),anularDepositoRequestDTO.getIdRequerimiento());
		trxFinancieroService.rollbackDeposit(anularDepositoRequestDTO.getTrxFinancieroId(),
				anularDepositoRequestDTO.getIdRequerimiento(), in);
		return dtoResponse;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.service.FinancieroService#doTransfer(ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaRequestDTO)
	 */
	@Override
	public RealizarTransferenciaDTO doTransfer(RealizarTransferenciaRequestDTO realizarTransferenciaRequestDTO,
			Intencion in) throws FinancieroException {
		String cod = prefijoOpService.getPrefijoOpCod("doTransfer");
		realizarTransferenciaRequestDTO.setPrefijoOpId(cod);

		Long trxFinancieroId = this.getTrxFinancieroService().getReversableOperationIds(
				realizarTransferenciaRequestDTO.getSecuenciaOn(), realizarTransferenciaRequestDTO.getTerminalSamId(),
				prefijoOpService.getNotReversablePrefijoOps());

		boolean existe = null != trxFinancieroId;

		if (existe) {
			this.registerReverse(trxFinancieroId, realizarTransferenciaRequestDTO.getIdRequerimiento());
		}

		RealizarTransferenciaDTO dtoResponse = new RealizarTransferenciaDTO();

		ValidationDTO validation = validate(realizarTransferenciaRequestDTO,
				realizarTransferenciaRequestDTO.getTokenizado(), true);
		validation.addInputValue(ValidationDTO.REVERSA, Boolean.toString(existe));
		operacionLimitesValidation.validate(validation);

		TransferenciaResponse tandemResponse = tandemAccountService.doTransfer(realizarTransferenciaRequestDTO);

		populateResponseData(dtoResponse, tandemResponse);
		populateTransferData(dtoResponse, tandemResponse);

		realizarTransferenciaRequestDTO.setFechaOn(dtoResponse.getFechaOn());
		realizarTransferenciaRequestDTO.setSecuenciaOnRta(dtoResponse.getSecuenciaOn());

		// this.registerTrx(realizarTransferenciaRequestDTO);
		this.doTransferOperation(realizarTransferenciaRequestDTO, in);

		return dtoResponse;
	}

	@Override
	public RealizarTransferenciaPEIDTO doTransferPEI(String idRequerimiento, String ipCliente, ValidationDTO validation,
			RealizarTransferenciaPEIRequestDTO realizarTransferenciaRequestPEIDTO, String secuenciaOn, Intencion in)
			throws FinancieroException {

		RealizarTransferenciaPEIDTO dtoResponse = null;
		dtoResponse = new RealizarTransferenciaPEIDTO();

		// desencripta y valida track2

		DesencriptarTarjetaDTO desencriptarTarjetaDTO = null;

		desencriptarTarjetaDTO = decryptValidateCard(validation,
				realizarTransferenciaRequestPEIDTO.getPago().getTracks().getTrack1(),
				realizarTransferenciaRequestPEIDTO.getPago().getTracks().getTrack2(), secuenciaOn);

		realizarTransferenciaRequestPEIDTO.getPago().getTracks().setTrack1(desencriptarTarjetaDTO.getTrack1());
		realizarTransferenciaRequestPEIDTO.getPago().getTracks().setTrack2(desencriptarTarjetaDTO.getTrack2());
		realizarTransferenciaRequestPEIDTO.getPago().getTracks().setTrack3(StringUtils.EMPTY);

		comercioPEIValidation.validate(validation);
		terminalPEIValidation.validate(validation);

		String codigoOp = prefijoOpService.getPrefijoOpCod("realizarPagoPei");
		validation.addInputValue(ValidationDTO.PREFIJO_OP_ID, codigoOp);

		HeaderPEIRequestDTO header = new HeaderPEIRequestDTO();
		header.setRequerimiento(idRequerimiento);
		header.setCliente(ipCliente);
		realizarTransferenciaRequestPEIDTO.getPago().setIdTerminal(setearCodigoTerminal(validation));

		TransferenciaPEIResponse peiResponse = null;

		// TrxFinancieroDTO trxFinancieroDTO =
		// this.registerPagoPEITrx(validation,
		// realizarTransferenciaRequestPEIDTO, FECHA_NULL, secuenciaOn,
		// VALOR_NULL, VALOR_NULL, ESTADO_OPERACION_INCOMPLETA, in);

		try {
			peiResponse = this.getPeiService().realizaPago(header, realizarTransferenciaRequestPEIDTO);
		} catch (RedLinkRESTServiceException e) {
			LOGGER.error(e);

			in.setEstadoTrxId(new EstadoTrx((byte) Integer.valueOf(ESTADO_OPERACION_INCOMPLETA).intValue()));
			this.updateIntencion(in);

			ServiceUtils.throwFinaExc(e);

		}

		Pago newPago = this.registerPagoPEITrxIntencion(validation, realizarTransferenciaRequestPEIDTO, FECHA_NULL,
				secuenciaOn, VALOR_NULL, VALOR_NULL, ESTADO_OPERACION_INCOMPLETA, in);

		dtoResponse.setResultado(new RealizarTransferenciaResultadoPEIDTO());
		dtoResponse.getResultado()
				.setEstado(peiResponse.getEstado() == null ? ESTADO_OPERACION_ACEPTADA : peiResponse.getEstado());
		dtoResponse.getResultado().setFecha(peiResponse.getFecha());
		dtoResponse.getResultado().setIdoperacion(peiResponse.getIdOperacion());
		dtoResponse.getResultado().setTipooperacion(peiResponse.getTipoOperacion());
		dtoResponse.getResultado().setSecuenciaOn(desencriptarTarjetaDTO.getSecuenciaOn());
		dtoResponse.getResultado().setNumeroReferenciaBancaria(peiResponse.getNumeroReferenciaBancaria());

		/*
		 * this.registerPagoPEITrx(trxFinancieroDTO,
		 * dtoResponse.getResultado().getFecha(), secuenciaOn,
		 * dtoResponse.getResultado().getSecuenciaOn(),
		 * dtoResponse.getResultado().getIdoperacion(),
		 * ESTADO_OPERACION_ACTIVA);
		 */

		newPago.setSecuenciaOnB24(Integer.valueOf(dtoResponse.getResultado().getSecuenciaOn()));
		newPago.setSecuenciaOnwS(Integer.valueOf(secuenciaOn));
		newPago.setOperacionPei(Long.valueOf(dtoResponse.getResultado().getIdoperacion()));
		this.updatePagoPEITrx(newPago);

		return dtoResponse;
	}

	@Override
	public RealizarTransferenciaPEIPagarDTO doTransferPEIPagar(String idRequerimiento, String ipCliente,
			ValidationDTO validation, RealizarTransferenciaPEIPagarRequestDTO realizarTransferenciaRequestPEIPagarDTO,
			String secuenciaOn, Intencion in) throws FinancieroException {

		RealizarTransferenciaPEIPagarDTO dtoResponse = null;
		dtoResponse = new RealizarTransferenciaPEIPagarDTO();

		String codigoOp = prefijoOpService.getPrefijoOpCod("realizarPagoPeiPagar");
		validation.addInputValue(ValidationDTO.PREFIJO_OP_ID, codigoOp);

		// desencripta y valida track2
		DesencriptarTarjetaDTO desencriptarTarjetaDTO = null;

		if (secuenciaOn != null) {
			desencriptarTarjetaDTO = decryptValidateCardPEIPagar(validation,
					realizarTransferenciaRequestPEIPagarDTO.getPago().getTracks().getTrack1(),
					realizarTransferenciaRequestPEIPagarDTO.getPago().getTracks().getTrack2(), secuenciaOn);

			realizarTransferenciaRequestPEIPagarDTO.getPago().getTracks().setTrack1(desencriptarTarjetaDTO.getTrack1());
			realizarTransferenciaRequestPEIPagarDTO.getPago().getTracks().setTrack2(desencriptarTarjetaDTO.getTrack2());
			// realizarTransferenciaRequestPEIPagarDTO.getPago().getTracks().setTrack3(StringUtils.EMPTY);
		}

		terminalPEIValidation.validate(validation);
		/*Paso TERMINAL/SUCURSAL PEI relacionada a la terminalSAM DE TERMINAL_PEI*/
		realizarTransferenciaRequestPEIPagarDTO.getPago().getDestino().setCodTerminal(setearCodigoTerminal(validation));
		realizarTransferenciaRequestPEIPagarDTO.getPago().getDestino().setCodSucursal(setearCodigoSucursal(validation));
		
		HeaderPEIRequestDTO header = new HeaderPEIRequestDTO();

		header.setRequerimiento(idRequerimiento);
		header.setCliente(ipCliente);

		TransferenciaPEIPagarResponse peiResponse = null;

		// TrxFinancieroDTO trxFinancieroDTO =
		// this.registerPagoPEITrx(validation,
		// realizarTransferenciaRequestPEIDTO, FECHA_NULL, secuenciaOn,
		// VALOR_NULL, VALOR_NULL, ESTADO_OPERACION_INCOMPLETA, in);

		try {
			peiResponse = this.getPeiService().realizaPagoPEIPagar(header, realizarTransferenciaRequestPEIPagarDTO);
		} catch (RedLinkRESTServiceException e) {
			LOGGER.error(e);

			in.setEstadoTrxId(new EstadoTrx((byte) Integer.valueOf(ESTADO_OPERACION_INCOMPLETA).intValue()));
			this.updateIntencion(in);

			// e.printStackTrace();
			ServiceUtils.throwFinaExc(e);
		}

		Pago newPago = this.registerPagoPEIPagarTrxIntencion(validation, realizarTransferenciaRequestPEIPagarDTO,
				FECHA_NULL, secuenciaOn, VALOR_NULL, VALOR_NULL, ESTADO_OPERACION_INCOMPLETA, in);

		dtoResponse.setResultado(new RealizarTransferenciaResultadoPEIPagarDTO());
		dtoResponse.getResultado()
				.setEstado(peiResponse.getEstado() == null ? ESTADO_OPERACION_ACEPTADA : peiResponse.getEstado());
		dtoResponse.getResultado().setFecha(peiResponse.getFecha());
		dtoResponse.getResultado().setIdOperacion(peiResponse.getIdOperacion());
		dtoResponse.getResultado().setTipoOperacion(peiResponse.getTipoOperacion());
		dtoResponse.getResultado().setSecuenciaOn(desencriptarTarjetaDTO.getSecuenciaOn());
		dtoResponse.getResultado().setNumeroReferenciaBancaria(peiResponse.getNumeroReferenciaBancaria());		
		dtoResponse.getResultado().setTerminalPEI(setearCodigoTerminal(validation));
		dtoResponse.getResultado().setSucursalPEI(setearCodigoSucursal(validation));		
		
		newPago.setSecuenciaOnB24(Integer.valueOf(dtoResponse.getResultado().getSecuenciaOn()));
		newPago.setSecuenciaOnwS(Integer.valueOf(secuenciaOn));
		newPago.setOperacionPei(Long.valueOf(dtoResponse.getResultado().getIdOperacion()));
		this.updatePagoPEITrx(newPago);

		return dtoResponse;
	}

	@Override
	public ConsultaTipoDocumentoPEIDTO consultaTipoDocumentoPEI(String idRequerimiento, String ipCliente)
			throws FinancieroException {

		ConsultaTipoDocumentoPEIDTO dtoResponse = new ConsultaTipoDocumentoPEIDTO();

		HeaderPEIRequestDTO header = new HeaderPEIRequestDTO();
		header.setRequerimiento(idRequerimiento);
		header.setCliente(ipCliente);

		ConsultaTipoDocumentoPEIResponse peiResponse = null;

		try {
			peiResponse = this.getPeiService().tipoDocumento(header);
		} catch (RedLinkRESTServiceException e) {
			LOGGER.error(e);
			ServiceUtils.throwFinaExc(e);
		}

		dtoResponse.setDocumentoTipos(setearListaTiposDocumentos(peiResponse));

		return dtoResponse;
	}

	/*
	 * @Deprecated private void registerPagoPEITrx(TrxFinancieroDTO
	 * trxFinancieroDTO, String fechaOn, String secuenciaOnWS, String
	 * secuenciaOnB24, String operacionPei, String estado) throws
	 * FinancieroException { try { SimpleDateFormat dt = new
	 * SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'"); Date date =
	 * dt.parse(fechaOn); Format formatter = new SimpleDateFormat("yyyyMMdd");
	 * fechaOn = formatter.format(date); trxFinancieroDTO.setFechaOn(fechaOn);
	 * 
	 * } catch (ParseException e) { LOGGER.error(e); throw new
	 * FinancieroException(FINA164.getCode(), FINA164.getMsg()); } catch
	 * (Exception e) { LOGGER.error(e); throw new
	 * FinancieroException(FINA164.getCode(), FINA164.getMsg()); }
	 * trxFinancieroDTO.setEstadoTrxId(estado);
	 * 
	 * trxFinancieroDTO.setSecuenciaOnWS(secuenciaOnWS);
	 * trxFinancieroDTO.setSecuenciaOnB24(secuenciaOnB24);
	 * trxFinancieroDTO.setOperacionPei(operacionPei);
	 * 
	 * //trxFinancieroService.updatePei(trxFinancieroDTO); }
	 */

	private void updatePagoPEITrx(Pago pg) throws FinancieroException {
		trxFinancieroService.updatePei(pg);
	}

	private void updateDevolucionPagoPEITrx(Pago pg, String idRequerimientoOrig, String idRequerimientoBaja)
			throws FinancieroException {
		trxFinancieroService.updateDevolucionPei(pg, idRequerimientoOrig, false);
	}

	private void updateDevolucionParcialPagoPEITrx(Pago pg, String idRequerimientoOrig, String idRequerimientoBaja)
			throws FinancieroException {
		trxFinancieroService.updateDevolucionPei(pg, idRequerimientoOrig, true);
	}

	@Override
	public ConsultaOperacionPEIDTO getOperationsPEI(String idRequerimiento, String ipCliente, ValidationDTO validation,
			ConsultaOperacionPEIRequestDTO requestDTO, String secuenciaOn, Intencion in) throws FinancieroException {
		ConsultaOperacionPEIDTO dtoResponse = null;

		// desencripta y valida track2
		DesencriptarTarjetaDTO desencriptarTarjetaDTO = decryptValidateCard(validation,
				requestDTO.getConsulta().getTracks().getTrack1(), requestDTO.getConsulta().getTracks().getTrack2(),
				secuenciaOn);

		requestDTO.getConsulta().getTracks().setTrack1(desencriptarTarjetaDTO.getTrack1());
		requestDTO.getConsulta().getTracks().setTrack2(desencriptarTarjetaDTO.getTrack2());
		requestDTO.getConsulta().getTracks().setTrack3(StringUtils.EMPTY);

		dtoResponse = new ConsultaOperacionPEIDTO();
		dtoResponse.setResultado(new ConsultaOperacionResultadoPEIDTO());
		dtoResponse.getResultado().setSecuenciaOn(desencriptarTarjetaDTO.getSecuenciaOn());

		comercioPEIValidation.validate(validation);
		terminalPEIValidation.validate(validation);

		String codigoOp = prefijoOpService.getPrefijoOpCod("consultaOperacionesPei");
		validation.addInputValue(ValidationDTO.PREFIJO_OP_ID, codigoOp);

		requestDTO.getConsulta().setIdterminal(setearCodigoTerminal(validation));

		ConsultaOperacionPEIResponse response = null;

		HeaderPEIRequestDTO header = new HeaderPEIRequestDTO();
		header.setRequerimiento(idRequerimiento);
		header.setCliente(ipCliente);

		try {
			response = this.getPeiService().consultaOperaciones(header,
					String.valueOf(validation.getOutputValue(ValidationDTO.CODIGO_COMERCIO_PEI)), requestDTO);
		} catch (RedLinkRESTServiceException e) {
			LOGGER.error(e);
			ServiceUtils.throwFinaExc(e);
		}

		dtoResponse.getResultado().setTotal(response.getTotal());
		dtoResponse.getResultado().setOperacion(setearListaOperaciones(response));

		this.registerConsultaPEITrx(validation, requestDTO, response, secuenciaOn,
				dtoResponse.getResultado().getSecuenciaOn(), in);

		return dtoResponse;
	}

	/**
	 * Obtiene la lista de operaciones.
	 * 
	 * @param response
	 * @return
	 */
	private List<ConsultaPEIDTO> setearListaOperaciones(ConsultaOperacionPEIResponse response) {
		List<ConsultaPEIDTO> operaciones = new ArrayList<ConsultaPEIDTO>();
		for (OperacionesPEIResponse ope : response.getResultado()) {
			ConsultaPEIDTO operacion = new ConsultaPEIDTO();
			operacion.setEstado(ope.getEstado());
			operacion.setFecha(ope.getFecha());
			operacion.setIdoperacion(ope.getIdOperacion());
			operacion.setIdreferenciaoperacioncomercio(ope.getIdReferenciaOperacionComercio());
			operacion.setImporte(ope.getImporte());

			if (ope.getMotivoRechazo() != null)
				operacion.setMotivorechazo(ope.getMotivoRechazo().getCodigo());

			operacion.setPan(ope.getPan());
			operacion.setSaldo(ope.getSaldo());
			operacion.setTipooperacion(ope.getTipoOperacion());
			operacion.setConcepto(ope.getConcepto());
			operacion.setNumeroReferenciaBancaria(ope.getNumeroReferenciaBancaria());

			operaciones.add(operacion);
		}
		return operaciones;
	}

	/*
	 * AQM
	 */
	@Override
	public ConsultaOperacionPEIPagarDTO getOperationsPEIPagar(String idRequerimiento, String ipCliente,
			ValidationDTO validation, ConsultaOperacionPEIPagarRequestDTO requestDTO, String secuenciaOn, Intencion in)
			throws FinancieroException {
		ConsultaOperacionPEIPagarDTO dtoResponse = null;

		String codigoOp = prefijoOpService.getPrefijoOpCod("consultaOperacionesPei");
		validation.addInputValue(ValidationDTO.PREFIJO_OP_ID, codigoOp);		
		// desencripta y valida track2
		DesencriptarTarjetaDTO desencriptarTarjetaDTO = decryptValidateCardPEIPagar(validation,
				requestDTO.getConsulta().getTracks().getTrack1(), requestDTO.getConsulta().getTracks().getTrack2(),
				secuenciaOn);

		requestDTO.getConsulta().setTracks(new RealizarTransferenciaPEITrackDataRequestDTO());
		requestDTO.getConsulta().getTracks().setTrack1(desencriptarTarjetaDTO.getTrack1());
		requestDTO.getConsulta().getTracks().setTrack2(desencriptarTarjetaDTO.getTrack2());
		requestDTO.getConsulta().getTracks().setTrack3(StringUtils.EMPTY);

		dtoResponse = new ConsultaOperacionPEIPagarDTO();
		dtoResponse.setResultado(new ConsultaOperacionResultadoPEIPagarDTO());
		dtoResponse.getResultado().setSecuenciaOn(desencriptarTarjetaDTO.getSecuenciaOn());

		ConsultaOperacionPEIPagarResponse response = null;

		HeaderPEIRequestDTO header = new HeaderPEIRequestDTO();
		header.setRequerimiento(idRequerimiento);
		header.setCliente(ipCliente);

		try {
			response = this.getPeiService().consultaOperacionesPEIPagar(header, requestDTO);

		} catch (RedLinkRESTServiceException e) {
			LOGGER.error(e);
			ServiceUtils.throwFinaExc(e);
		}

		dtoResponse.getResultado().setTotal(response.getTotal());
		dtoResponse.getResultado().setOperacion(setearListaOperaciones(response));

		this.registerConsultaPEIPagarTrx(validation, requestDTO, response, secuenciaOn,
				dtoResponse.getResultado().getSecuenciaOn(), in);

		return dtoResponse;
	}

	/**
	 * Obtiene la lista de operaciones.
	 * 
	 * @param response
	 * @return
	 */
	private List<ConsultaPEIDTO> setearListaOperaciones(ConsultaOperacionPEIPagarResponse response) {
		List<ConsultaPEIDTO> operaciones = new ArrayList<ConsultaPEIDTO>();
		for (OperacionesPEIResponse ope : response.getResultado()) {
			ConsultaPEIDTO operacion = new ConsultaPEIDTO();
			operacion.setEstado(ope.getEstado());
			operacion.setFecha(ope.getFecha());
			operacion.setIdoperacion(ope.getIdOperacion());
			operacion.setIdreferenciaoperacioncomercio(ope.getIdReferenciaOperacionComercio());
			operacion.setImporte(ope.getImporte());

			if (ope.getMotivoRechazo() != null)
				operacion.setMotivorechazo(ope.getMotivoRechazo().getCodigo());

			operacion.setPan(ope.getPan());
			operacion.setSaldo(ope.getSaldo());
			operacion.setTipooperacion(ope.getTipoOperacion());
			operacion.setConcepto(ope.getConcepto());
			operacion.setNumeroReferenciaBancaria(ope.getNumeroReferenciaBancaria());

			operaciones.add(operacion);
		}
		return operaciones;
	}

	private List<TipoDocumentoPEIDTO> setearListaTiposDocumentos(ConsultaTipoDocumentoPEIResponse response) {
		List<TipoDocumentoPEIDTO> tiposDocumento = new ArrayList<TipoDocumentoPEIDTO>();
		for (TipoDocumentoPEIResponse tip : response.getDocumentoTipos()) {
			TipoDocumentoPEIDTO newTip = new TipoDocumentoPEIDTO();
			newTip.setCodigo(tip.getCodigo());
			newTip.setDescripcion(tip.getDescripcion());
			tiposDocumento.add(newTip);
		}
		return tiposDocumento;
	}

	/**
	 * Registra la operacion
	 * 
	 * @param validation
	 * @param fechaOn
	 * @return
	 * @throws FinancieroException
	 */
	private TrxFinancieroDTO registerPEITrx(ValidationDTO validation, String fechaOn) throws FinancieroException {
		TrxFinancieroDTO trxFinancieroDTO = new TrxFinancieroDTO();
		trxFinancieroDTO.setIdEntidad(String.valueOf(validation.getOutputValue(ValidationDTO.ENTIDAD_ID)));
		trxFinancieroDTO.setAgenteId(String.valueOf(validation.getOutputValue(ValidationDTO.AGENTE_ID)));
		trxFinancieroDTO.setRedId(String.valueOf(validation.getOutputValue(ValidationDTO.RED_ID)));
		trxFinancieroDTO.setSucursal(String.valueOf(validation.getOutputValue(ValidationDTO.SUCURSAL_ID)));
		trxFinancieroDTO.setServidorId(String.valueOf(validation.getOutputValue(ValidationDTO.SERVIDOR_ID)));
		trxFinancieroDTO.setIdRequerimiento(String.valueOf(validation.getInputValue(ValidationDTO.ID_REQ)));
		trxFinancieroDTO.setSecuenciaOnB24(StringUtils.EMPTY);
		trxFinancieroDTO.setSecuenciaOnWS(StringUtils.EMPTY);
		trxFinancieroDTO.setTerminalSamId(String.valueOf(validation.getOutputValue(ValidationDTO.TERMINAL_SAM_ID)));
		trxFinancieroDTO.setPrefijoOpId(String.valueOf(validation.getInputValue(ValidationDTO.PREFIJO_OP_ID)));

		try {
			SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			Date date = dt.parse(fechaOn);
			Format formatter = new SimpleDateFormat("yyyyMMdd");
			fechaOn = formatter.format(date);
			trxFinancieroDTO.setFechaOn(fechaOn);
		} catch (ParseException e) {
			LOGGER.error(e);
			throw new FinancieroException(FINA164.getCode(), FINA164.getMsg());
		} catch (Exception e) {
			LOGGER.error(e);
			throw new FinancieroException(FINA164.getCode(), FINA164.getMsg());
		}

		trxFinancieroDTO.setSucursal(StringUtils.leftPad(trxFinancieroDTO.getSucursal(), 8, "0"));
		return trxFinancieroDTO;
	}

	private void registerConsultaPEITrx(ValidationDTO validation, ConsultaOperacionPEIRequestDTO trxData,
			ConsultaOperacionPEIResponse response, String secuenciaOnWS, String secuenciaOnB24, Intencion in)
			throws FinancieroException {
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		TrxFinancieroDTO trxFinancieroDTO = this.registerPEITrx(validation, dt.format(new Date()));

		TandemResponse tr = new TandemResponse();
		tr.setTrack2(trxData.getConsulta().getTracks().getTrack2());
		trxFinancieroDTO.setTokenizado(tokenServiceConectado.getToken(this.extractPan(tr)));

		if (null == trxFinancieroDTO.getImporte()) {
			trxFinancieroDTO.setImporte("0");
		}
		if (null == trxFinancieroDTO.getEstadoTrxId()) {
			trxFinancieroDTO.setEstadoTrxId("0");
		}

		trxFinancieroDTO.setSecuenciaOnWS(secuenciaOnWS);
		trxFinancieroDTO.setSecuenciaOnB24(secuenciaOnB24);

		// trxFinancieroService.insert(trxFinancieroDTO);
		trxFinancieroService.registerConsultaPEITrx(trxFinancieroDTO, in);
	}

	private void registerConsultaPEIPagarTrx(ValidationDTO validation, ConsultaOperacionPEIPagarRequestDTO trxData,
			ConsultaOperacionPEIPagarResponse response, String secuenciaOnWS, String secuenciaOnB24, Intencion in)
			throws FinancieroException {
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		TrxFinancieroDTO trxFinancieroDTO = this.registerPEITrx(validation, dt.format(new Date()));

		TandemResponse tr = new TandemResponse();
		tr.setTrack2(trxData.getConsulta().getTracks().getTrack2());
		trxFinancieroDTO.setTokenizado(tokenServiceConectado.getToken(this.extractPan(tr)));

		if (null == trxFinancieroDTO.getImporte()) {
			trxFinancieroDTO.setImporte("0");
		}
		if (null == trxFinancieroDTO.getEstadoTrxId()) {
			trxFinancieroDTO.setEstadoTrxId("0");
		}

		trxFinancieroDTO.setSecuenciaOnWS(secuenciaOnWS);
		trxFinancieroDTO.setSecuenciaOnB24(secuenciaOnB24);

		// trxFinancieroService.insert(trxFinancieroDTO);
		trxFinancieroService.registerConsultaPEITrx(trxFinancieroDTO, in);
	}

	/*
	 * @Deprecated private TrxFinancieroDTO registerPagoPEITrx(ValidationDTO
	 * validation, RealizarTransferenciaPEIRequestDTO trxData, String fechaOn,
	 * String secuenciaOnWS, String secuenciaOnB24, String operacionPei, String
	 * estado, Intencion in) throws FinancieroException {
	 * 
	 * 
	 * TrxFinancieroDTO trxFinancieroDTO = this.registerPEITrx(validation,
	 * fechaOn);
	 * 
	 * TandemResponse tr = new TandemResponse();
	 * tr.setTrack2(trxData.getPago().getTracks().getTrack2());
	 * trxFinancieroDTO.setTokenizado(tokenServiceConectado.getToken(this.
	 * extractPan(tr))); BigDecimal importe = new
	 * BigDecimal(trxData.getPago().getImporte()).divide(new BigDecimal(100));
	 * trxFinancieroDTO.setImporte(String.valueOf(importe));
	 * 
	 * if (null == trxFinancieroDTO.getImporte()) {
	 * trxFinancieroDTO.setImporte("0"); } // if (null ==
	 * trxFinancieroDTO.getEstadoTrxId()) { //
	 * trxFinancieroDTO.setEstadoTrxId("0"); // }
	 * trxFinancieroDTO.setEstadoTrxId(estado);
	 * trxFinancieroDTO.setSucursal(StringUtils.leftPad(trxFinancieroDTO.
	 * getSucursal(), 8, "0"));
	 * 
	 * trxFinancieroDTO.setSecuenciaOnWS(secuenciaOnWS);
	 * trxFinancieroDTO.setSecuenciaOnB24(secuenciaOnB24);
	 * trxFinancieroDTO.setOperacionPei(operacionPei);
	 * 
	 * //trxFinancieroService.insert(trxFinancieroDTO);
	 * trxFinancieroService.pagoPei(trxFinancieroDTO, in);
	 * 
	 * //trxFinancieroService.registerConsultaPEITrx(trxFinancieroDTO);
	 * //this.doDebitPayment(realizarPagoDebitoRequestDTO, in);
	 * 
	 * return trxFinancieroDTO;
	 * 
	 * }
	 */

	private Pago registerPagoPEITrxIntencion(ValidationDTO validation, RealizarTransferenciaPEIRequestDTO trxData,
			String fechaOn, String secuenciaOnWS, String secuenciaOnB24, String operacionPei, String estado,
			Intencion in) throws FinancieroException {

		TrxFinancieroDTO trxFinancieroDTO = this.registerPEITrx(validation, fechaOn);

		TandemResponse tr = new TandemResponse();
		tr.setTrack2(trxData.getPago().getTracks().getTrack2());
		trxFinancieroDTO.setTokenizado(tokenServiceConectado.getToken(this.extractPan(tr)));
		BigDecimal importe = new BigDecimal(trxData.getPago().getImporte()).divide(new BigDecimal(100));
		trxFinancieroDTO.setImporte(String.valueOf(importe));

		if (null == trxFinancieroDTO.getImporte()) {
			trxFinancieroDTO.setImporte("0");
		}
		// if (null == trxFinancieroDTO.getEstadoTrxId()) {
		// trxFinancieroDTO.setEstadoTrxId("0");
		// }
		trxFinancieroDTO.setEstadoTrxId(estado);
		trxFinancieroDTO.setSucursal(StringUtils.leftPad(trxFinancieroDTO.getSucursal(), 8, "0"));

		trxFinancieroDTO.setSecuenciaOnWS(secuenciaOnWS);
		trxFinancieroDTO.setSecuenciaOnB24(secuenciaOnB24);
		trxFinancieroDTO.setOperacionPei(operacionPei);

		// trxFinancieroService.insert(trxFinancieroDTO);
		Pago newPago = trxFinancieroService.pagoPei(trxFinancieroDTO, in);

		// trxFinancieroService.registerConsultaPEITrx(trxFinancieroDTO);
		// this.doDebitPayment(realizarPagoDebitoRequestDTO, in);

		return newPago;
	}

	private Pago registerPagoPEIPagarTrxIntencion(ValidationDTO validation,
			RealizarTransferenciaPEIPagarRequestDTO trxData, String fechaOn, String secuenciaOnWS,
			String secuenciaOnB24, String operacionPei, String estado, Intencion in) throws FinancieroException {

		TrxFinancieroDTO trxFinancieroDTO = this.registerPEITrx(validation, fechaOn);

		TandemResponse tr = new TandemResponse();
		tr.setTrack2(trxData.getPago().getTracks().getTrack2());
		trxFinancieroDTO.setTokenizado(tokenServiceConectado.getToken(this.extractPan(tr)));
		BigDecimal importe = new BigDecimal(trxData.getPago().getImporte()).divide(new BigDecimal(100));
		trxFinancieroDTO.setImporte(String.valueOf(importe));

		if (null == trxFinancieroDTO.getImporte()) {
			trxFinancieroDTO.setImporte("0");
		}
		// if (null == trxFinancieroDTO.getEstadoTrxId()) {
		// trxFinancieroDTO.setEstadoTrxId("0");
		// }
		trxFinancieroDTO.setEstadoTrxId(estado);
		trxFinancieroDTO.setSucursal(StringUtils.leftPad(trxFinancieroDTO.getSucursal(), 8, "0"));

		trxFinancieroDTO.setSecuenciaOnWS(secuenciaOnWS);
		trxFinancieroDTO.setSecuenciaOnB24(secuenciaOnB24);
		trxFinancieroDTO.setOperacionPei(operacionPei);

		// trxFinancieroService.insert(trxFinancieroDTO);
		Pago newPago = trxFinancieroService.pagoPei(trxFinancieroDTO, in);

		// trxFinancieroService.registerConsultaPEITrx(trxFinancieroDTO);
		// this.doDebitPayment(realizarPagoDebitoRequestDTO, in);

		return newPago;
	}

	/**
	 * Extrae los datos requeridos para la respuesta de una transferencia.
	 * 
	 * @param dtoResponse
	 * @param tandemResponse
	 */
	private void populateTransferData(RealizarTransferenciaDTO dtoResponse, TransferenciaResponse tandemResponse) {
		dtoResponse.setTipoCambio(tandemResponse.getRespuesta().substring(24, 32));
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.FinancieroService#doLoanWithdraw(ar.com.redlink.sam.financieroservices.dto.request.RealizarExtraccionPrestamoRequestDTO)
	 */
	@Override
	public ExtraccionPrestamoDTO doLoanWithdraw(
			RealizarExtraccionPrestamoRequestDTO realizarExtraccionPrestamoRequestDTO) throws FinancieroException {
		String cod = prefijoOpService.getPrefijoOpCod("doLoanWithdraw");
		realizarExtraccionPrestamoRequestDTO.setPrefijoOpId(cod);

		Long trxFinancieroId = this.getTrxFinancieroService().getReversableOperationIds(
				realizarExtraccionPrestamoRequestDTO.getSecuenciaOn(),
				realizarExtraccionPrestamoRequestDTO.getTerminalSamId(), prefijoOpService.getNotReversablePrefijoOps());

		boolean reversable = prefijoOpService.isReversable(cod);
		boolean existe = null != trxFinancieroId;

		if (existe && reversable) {
			this.registerReverse(trxFinancieroId, realizarExtraccionPrestamoRequestDTO.getIdRequerimiento());
		}

		ExtraccionPrestamoDTO dtoResponse = new ExtraccionPrestamoDTO();

		ValidationDTO validation = validate(realizarExtraccionPrestamoRequestDTO,
				realizarExtraccionPrestamoRequestDTO.getTokenizado(), true);

		validation.addInputValue(ValidationDTO.REVERSA, Boolean.toString(existe));
		operacionLimitesValidation.validate(validation);

		ExtraccionResponse tandemResponse = tandemAccountService.doLoanWithdraw(realizarExtraccionPrestamoRequestDTO);

		populateResponseData(dtoResponse, tandemResponse);
		extractWithdrawData(tandemResponse, dtoResponse);

		realizarExtraccionPrestamoRequestDTO.setFechaOn(dtoResponse.getFechaOn());

		realizarExtraccionPrestamoRequestDTO.setSecuenciaOnRta(dtoResponse.getSecuenciaOn());
		this.registerTrx(realizarExtraccionPrestamoRequestDTO);

		return dtoResponse;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.FinancieroService#rollbackLoanWithdraw(ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionPrestamoRequestDTO)
	 */
	@Override
	public AnularExtraccionPrestamoDTO rollbackLoanWithdraw(
			AnularExtraccionPrestamoRequestDTO anularExtraccionPrestamoRequestDTO) throws FinancieroException {
		String cod = prefijoOpService.getPrefijoOpCod("rollbackLoanWithdraw");
		String originalCod = prefijoOpService.getPrefijoOpCod("doLoanWithdraw");

		anularExtraccionPrestamoRequestDTO.setPrefijoOpId(cod);

		ValidationDTO validation = validate(anularExtraccionPrestamoRequestDTO,
				anularExtraccionPrestamoRequestDTO.getTokenizado(), true);

		operacionLimitesValidation.validate(validation);
		this.rollbackValidation(anularExtraccionPrestamoRequestDTO, originalCod, validation);

		anularExtraccionPrestamoRequestDTO
				.setTrxFinancieroId((Long) validation.getOutputValue(ValidationDTO.TRXT_FINANCIERO_ID));

		AnularExtraccionPrestamoDTO dtoResponse = new AnularExtraccionPrestamoDTO();

		AnulacionExtraccionResponse tandemResponse = tandemAccountService
				.rollbackLoanWithdraw(anularExtraccionPrestamoRequestDTO);
		populateResponseData(dtoResponse, tandemResponse);

		trxFinancieroService.annulTrx(anularExtraccionPrestamoRequestDTO.getTrxFinancieroId(),
				anularExtraccionPrestamoRequestDTO.getIdRequerimiento());

		return dtoResponse;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.FinancieroService#doSharesWithdraw(ar.com.redlink.sam.financieroservices.dto.request.RealizarExtraccionCuotasRequestDTO)
	 */
	@Override
	public ExtraccionCuotasDTO doSharesWithdraw(RealizarExtraccionCuotasRequestDTO realizarExtraccionCuotasRequestDTO)
			throws FinancieroException {
		String cod = prefijoOpService.getPrefijoOpCod("doSharesWithdraw");
		realizarExtraccionCuotasRequestDTO.setPrefijoOpId(cod);

		Long trxFinancieroId = this.getTrxFinancieroService().getReversableOperationIds(
				realizarExtraccionCuotasRequestDTO.getSecuenciaOn(),
				realizarExtraccionCuotasRequestDTO.getTerminalSamId(), prefijoOpService.getNotReversablePrefijoOps());

		boolean existe = null != trxFinancieroId;

		if (existe) {
			this.registerReverse(trxFinancieroId, realizarExtraccionCuotasRequestDTO.getIdRequerimiento());
		}

		ExtraccionCuotasDTO dtoResponse = new ExtraccionCuotasDTO();

		ValidationDTO validation = validate(realizarExtraccionCuotasRequestDTO,
				realizarExtraccionCuotasRequestDTO.getTokenizado(), true);

		validation.addInputValue(ValidationDTO.REVERSA, Boolean.toString(existe));
		operacionLimitesValidation.validate(validation);

		ExtraccionResponse tandemResponse = tandemAccountService.doSharesWithdraw(realizarExtraccionCuotasRequestDTO);

		populateResponseData(dtoResponse, tandemResponse);
		extractWithdrawData(tandemResponse, dtoResponse);

		realizarExtraccionCuotasRequestDTO.setFechaOn(dtoResponse.getFechaOn());
		realizarExtraccionCuotasRequestDTO.setSecuenciaOnRta(dtoResponse.getSecuenciaOn());
		this.registerTrx(realizarExtraccionCuotasRequestDTO);

		return dtoResponse;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.FinancieroService#rollbackSharesWithdraw(ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionCuotasRequestDTO)
	 */
	@Override
	public AnularExtraccionCuotasDTO rollbackSharesWithdraw(AnularExtraccionCuotasRequestDTO anularExtraccionCuotasDTO)
			throws FinancieroException {
		String cod = prefijoOpService.getPrefijoOpCod("rollbackSharesWithdraw");
		String originalCod = prefijoOpService.getPrefijoOpCod("doSharesWithdraw");
		anularExtraccionCuotasDTO.setPrefijoOpId(cod);

		ValidationDTO validation = validate(anularExtraccionCuotasDTO, anularExtraccionCuotasDTO.getTokenizado(), true);

		operacionLimitesValidation.validate(validation);
		this.rollbackValidation(anularExtraccionCuotasDTO, originalCod, validation);

		anularExtraccionCuotasDTO
				.setTrxFinancieroId((Long) validation.getOutputValue(ValidationDTO.TRXT_FINANCIERO_ID));

		AnularExtraccionCuotasDTO dtoResponse = new AnularExtraccionCuotasDTO();

		AnulacionExtraccionResponse tandemResponse = tandemAccountService
				.rollbackSharesWithdraw(anularExtraccionCuotasDTO);

		populateResponseData(dtoResponse, tandemResponse);

		trxFinancieroService.annulTrx(anularExtraccionCuotasDTO.getTrxFinancieroId(),
				anularExtraccionCuotasDTO.getIdRequerimiento());

		return dtoResponse;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.FinancieroService#getOperationResult(ar.com.redlink.sam.financieroservices.dto.request.ResultadoOperacionRequestDTO)
	 */
	@Override
	public EstadoOperacionDTO getOperationResult(ResultadoOperacionRequestDTO resultadoOperacion)
			throws FinancieroException {
		final String reqOrig = resultadoOperacion.getIdRequerimientoOrig();
		final Long terminalSamId = Long.parseLong(resultadoOperacion.getTerminalSamId());

		List<TrxFinanciero> trxFinancieroResult = null;

		try {
			// LOGGER.info("[INI] getOperationResult");
			trxFinancieroResult = trxFinancieroService
					.getAll(new TrxFinancieroByTerminalSamIdReqAltaSearchFilter(terminalSamId, reqOrig), null)
					.getResult();
			// LOGGER.info("[FIN] getOperationResult");

		} catch (RedLinkServiceException e) {
			LOGGER.error("Excepcion al obtener la transaccion: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}
		EstadoOperacionDTO estadoOperacionDTO = null;

		if (trxFinancieroResult != null && trxFinancieroResult.size() == 1) {
			estadoOperacionDTO = (EstadoOperacionDTO) TrxFinancieroTranslator
					.translateTrxFinanciero(trxFinancieroResult.get(0));
		} else {
			throw new FinancieroException(FINA136.getCode(), FINA136.getMsg());
		}

		return estadoOperacionDTO;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.FinancieroService#getLatestOperations(ar.com.redlink.sam.financieroservices.dto.request.TerminalRequestDTO)
	 */
	@Override
	public UltimasOperacionesDTO getLatestOperations(TerminalRequestDTO terminal) throws FinancieroException {
		final Long terminalSamId = Long.parseLong(terminal.getTerminalSamId());
		final Integer maxResults = this.maxResults;

		List<TrxFinanciero> trxFinancieroResult = null;

		try {
			// LOGGER.info("[DAO INI]
			// TrxFinancieroByTerminalSamIdMaxResSearchFilter");

			trxFinancieroResult = trxFinancieroService
					.getAll(new TrxFinancieroByTerminalSamIdMaxResSearchFilter(terminalSamId, maxResults), null)
					.getResult();

			// LOGGER.info("[DAO FIN]
			// TrxFinancieroByTerminalSamIdMaxResSearchFilter");

		} catch (RedLinkServiceException e) {
			LOGGER.error("Excepcion al obtener el historial de transacciones: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}

		List<RequerimientoDTO> requerimientos = new ArrayList<RequerimientoDTO>();

		for (TrxFinanciero trxFinanciero : trxFinancieroResult) {
			RequerimientoDTO requerimientoDTO = TrxFinancieroTranslator.translateTrxFinanciero(trxFinanciero);
			requerimientos.add(requerimientoDTO);
		}

		UltimasOperacionesDTO ultOpDTO = new UltimasOperacionesDTO();
		ultOpDTO.setRequerimientos(requerimientos);

		return ultOpDTO;
	}

	@Override
	public RealizarDevolucionResultadoPEIDTO doReturnPEI(String idRequerimiento, String ipCliente,
			ValidationDTO validation, RealizarDevolucionPagoPEIRequestDTO realizarDevolucionPagoPEIRequestDTO,
			String secuenciaOn, String idRequerimientoOrig, BigDecimal importe, Intencion in)
			throws FinancieroException {

		validation.addInputValue(ValidationDTO.PREFIJO_OP_ID, prefijoOpService.getPrefijoOpCod("devolucionPagoPei"));
		RealizarDevolucionResultadoPEIDTO dtoResponse = new RealizarDevolucionResultadoPEIDTO();

		// desencripta y valida track2
		DesencriptarTarjetaDTO desencriptarTarjetaDTO = decryptValidateCard(validation,
				realizarDevolucionPagoPEIRequestDTO.getDevolucion().getTracks().getTrack1(),
				realizarDevolucionPagoPEIRequestDTO.getDevolucion().getTracks().getTrack2(), secuenciaOn);

		realizarDevolucionPagoPEIRequestDTO.getDevolucion().getTracks().setTrack1(desencriptarTarjetaDTO.getTrack1());
		realizarDevolucionPagoPEIRequestDTO.getDevolucion().getTracks().setTrack2(desencriptarTarjetaDTO.getTrack2());
		realizarDevolucionPagoPEIRequestDTO.getDevolucion().getTracks().setTrack3(StringUtils.EMPTY);

		validarDevolucion(validation, idRequerimientoOrig, importe, desencriptarTarjetaDTO, false);

		HeaderPEIRequestDTO header = new HeaderPEIRequestDTO();
		header.setRequerimiento(idRequerimiento);
		header.setCliente(ipCliente);

		realizarDevolucionPagoPEIRequestDTO.getDevolucion().setIdTerminal(setearCodigoTerminal(validation));

		// Pago newPago= this.registerPagoPEITrxIntencion(validation,
		// realizarTransferenciaRequestPEIDTO, FECHA_NULL, secuenciaOn,
		// VALOR_NULL, VALOR_NULL, ESTADO_OPERACION_INCOMPLETA, in);
		// TrxFinancieroDTO trxFinancieroDTO =
		// this.registerDevolucionPagoPEITrx(validation,
		// realizarDevolucionPagoPEIRequestDTO, FECHA_NULL, secuenciaOn,
		// VALOR_NULL, idRequerimientoOrig, importe, VALOR_NULL,
		// ESTADO_OPERACION_INCOMPLETA);

		RealizarDevolucionPEIResponse peiResponse = null;
		try {
			// realiza la devolucion en PEI
			peiResponse = this.getPeiService().devolucion(header, realizarDevolucionPagoPEIRequestDTO);
		} catch (RedLinkRESTServiceException e) {
			LOGGER.error(e);

			in.setEstadoTrxId(new EstadoTrx((byte) Integer.valueOf(ESTADO_OPERACION_INCOMPLETA).intValue()));
			this.updateIntencion(in);

			ServiceUtils.throwFinaExc(e);
		}

		Pago newPago = this.registerDevolucionPagoPEIIntencion(validation, realizarDevolucionPagoPEIRequestDTO,
				FECHA_NULL, secuenciaOn, VALOR_NULL, idRequerimientoOrig, importe, VALOR_NULL,
				ESTADO_OPERACION_INCOMPLETA, in);

		dtoResponse.setEstado(peiResponse.getEstado() == null ? ESTADO_OPERACION_ACEPTADA : peiResponse.getEstado());
		dtoResponse.setFecha(peiResponse.getFecha());
		dtoResponse.setTipooperacion(peiResponse.getTipoOperacion());
		dtoResponse.setIdoperacion(peiResponse.getIdOperacion());
		dtoResponse.setSecuenciaOn(desencriptarTarjetaDTO.getSecuenciaOn());
		dtoResponse.setNumeroReferenciaBancaria(peiResponse.getNumeroReferenciaBancaria());

		// this.registerDevolucionPagoPEITrx(trxFinancieroDTO,
		// dtoResponse.getFecha(), secuenciaOn, dtoResponse.getSecuenciaOn(),
		// idRequerimientoOrig, importe, peiResponse.getIdOperacion(),
		// ESTADO_OPERACION_ACTIVA);
		// debo retornar la intencion? se hace update al pago??
		// this.registerDevolucionPagoPEIIntencion(trxFinancieroDTO,
		// dtoResponse.getFecha(), secuenciaOn, dtoResponse.getSecuenciaOn(),
		// idRequerimientoOrig, importe, peiResponse.getIdOperacion(),
		// ESTADO_OPERACION_ACTIVA);
		// -----------------------------------------
		// this.registerPagoPEITrx(trxFinancieroDTO,
		// dtoResponse.getResultado().getFecha(), secuenciaOn,
		// dtoResponse.getResultado().getSecuenciaOn(),
		// dtoResponse.getResultado().getIdoperacion(),
		// ESTADO_OPERACION_ACTIVA);

		newPago.setSecuenciaOnB24(Integer.valueOf(secuenciaOn));
		newPago.setSecuenciaOnwS(Integer.valueOf(dtoResponse.getSecuenciaOn()));
		newPago.setOperacionPei(Long.valueOf(peiResponse.getIdOperacion()));

		try {
			SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			Date date = dt.parse(dtoResponse.getFecha());
			newPago.setFechaOn(date);
		} catch (ParseException e) {
			LOGGER.error(e);
			throw new FinancieroException(FINA164.getCode(), FINA164.getMsg());
		} catch (Exception e) {
			LOGGER.error(e);
			throw new FinancieroException(FINA164.getCode(), FINA164.getMsg());
		}

		this.updateDevolucionPagoPEITrx(newPago, idRequerimientoOrig, idRequerimiento);

		// trxFinancieroService.annulTrxPei(idRequerimientoOrig,
		// idRequerimiento,
		// prefijoOpService.getPrefijoOpCod("realizarPagoPei")/*23*/);
		return dtoResponse;
	}

	/* AQM */
	public RealizarDevolucionResultadoPEIPagarDTO doReturnPEIPagar(String idRequerimiento, String ipCliente,
			ValidationDTO validation, RealizarDevolucionPagoPEIPagarRequestDTO realizarDevolucionPagoPEIPagarRequestDTO,
			String secuenciaOn, String idRequerimientoOrig, BigDecimal importe, Intencion in)
			throws FinancieroException {

		validation.addInputValue(ValidationDTO.PREFIJO_OP_ID, prefijoOpService.getPrefijoOpCod("devolucionPagoPeiPagar"));
		RealizarDevolucionResultadoPEIPagarDTO dtoResponse = new RealizarDevolucionResultadoPEIPagarDTO();

		// desencripta y valida track2
		DesencriptarTarjetaDTO desencriptarTarjetaDTO = decryptValidateCardPEIPagar(validation,
				realizarDevolucionPagoPEIPagarRequestDTO.getDevolucion().getTracks().getTrack1(),
				realizarDevolucionPagoPEIPagarRequestDTO.getDevolucion().getTracks().getTrack2(), secuenciaOn);

		realizarDevolucionPagoPEIPagarRequestDTO.getDevolucion().getTracks()
				.setTrack1(desencriptarTarjetaDTO.getTrack1());
		realizarDevolucionPagoPEIPagarRequestDTO.getDevolucion().getTracks()
				.setTrack2(desencriptarTarjetaDTO.getTrack2());
		realizarDevolucionPagoPEIPagarRequestDTO.getDevolucion().getTracks().setTrack3(StringUtils.EMPTY);
		/*
		 * NO VAAAAAAA
		 */
		 //validarDevolucion(validation, idRequerimientoOrig, importe,
		 //desencriptarTarjetaDTO, false);

		terminalPEIValidation.validate(validation);
		/*Paso TERMINAL/SUCURSAL PEI relacionada a la terminalSAM DE TERMINAL_PEI*/
		realizarDevolucionPagoPEIPagarRequestDTO.getDevolucion().setCodTerminal(setearCodigoTerminal(validation));
		realizarDevolucionPagoPEIPagarRequestDTO.getDevolucion().setCodSucursal(setearCodigoSucursal(validation));

		
		HeaderPEIRequestDTO header = new HeaderPEIRequestDTO();
		header.setRequerimiento(idRequerimiento);
		header.setCliente(ipCliente);


		RealizarDevolucionPEIPagarResponse peiResponse = null;
		try {
			// realiza la devolucion en PEI desde PAGAR
			peiResponse = this.getPeiService().devolucionPEIPagar(header, realizarDevolucionPagoPEIPagarRequestDTO);

		} catch (RedLinkRESTServiceException e) {
			LOGGER.error(e);

			in.setEstadoTrxId(new EstadoTrx((byte) Integer.valueOf(ESTADO_OPERACION_INCOMPLETA).intValue()));
			this.updateIntencion(in);

			ServiceUtils.throwFinaExc(e);
		}

		Pago newPago = this.registerDevolucionPagoPEIPagarIntencion(validation,
				realizarDevolucionPagoPEIPagarRequestDTO, FECHA_NULL, secuenciaOn, VALOR_NULL, idRequerimientoOrig,
				VALOR_NULL, ESTADO_OPERACION_INCOMPLETA, importe, in);
		
		dtoResponse.setEstado(peiResponse.getEstado() == null ? ESTADO_OPERACION_ACEPTADA : peiResponse.getEstado());
		dtoResponse.setFecha(peiResponse.getFecha());		
		dtoResponse.setTipooperacion(peiResponse.getTipoOperacion());		
		dtoResponse.setIdoperacion(peiResponse.getIdOperacion());		
		dtoResponse.setSecuenciaOn(desencriptarTarjetaDTO.getSecuenciaOn());
		dtoResponse.setNumeroReferenciaBancaria(peiResponse.getNumeroReferenciaBancaria());
		dtoResponse.setTerminalPEI(setearCodigoTerminal(validation));
		dtoResponse.setSucursalPEI(setearCodigoSucursal(validation));

		newPago.setSecuenciaOnB24(Integer.valueOf(secuenciaOn));
		newPago.setSecuenciaOnwS(Integer.valueOf(dtoResponse.getSecuenciaOn()));
		newPago.setOperacionPei(Long.valueOf(peiResponse.getIdOperacion()));

		try {
			SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			Date date = dt.parse(dtoResponse.getFecha());
			newPago.setFechaOn(date);
		} catch (ParseException e) {
			LOGGER.error(e);
			throw new FinancieroException(FINA164.getCode(), FINA164.getMsg());
		} catch (Exception e) {
			LOGGER.error(e);
			throw new FinancieroException(FINA164.getCode(), FINA164.getMsg());
		}

		this.updateDevolucionPagoPEITrx(newPago, idRequerimientoOrig, idRequerimiento);

		// trxFinancieroService.annulTrxPei(idRequerimientoOrig,
		// idRequerimiento,
		// prefijoOpService.getPrefijoOpCod("realizarPagoPei")/*23*/);
		return dtoResponse;
	}

	private void registerDevolucionPagoPEITrx(TrxFinancieroDTO trxFinancieroDTO, String fechaOn, String secuenciaOnWS,
			String secuenciaOnB24, String idRequerimientoOrig, BigDecimal importe, String operacionPei, String estado)
			throws FinancieroException {
		try {
			SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			Date date = dt.parse(fechaOn);
			Format formatter = new SimpleDateFormat("yyyyMMdd");
			fechaOn = formatter.format(date);
			trxFinancieroDTO.setFechaOn(fechaOn);
		} catch (ParseException e) {
			LOGGER.error(e);
			throw new FinancieroException(FINA164.getCode(), FINA164.getMsg());
		} catch (Exception e) {
			LOGGER.error(e);
			throw new FinancieroException(FINA164.getCode(), FINA164.getMsg());
		}
		trxFinancieroDTO.setEstadoTrxId(estado);
		trxFinancieroDTO.setSecuenciaOnWS(secuenciaOnWS);
		trxFinancieroDTO.setSecuenciaOnB24(secuenciaOnB24);
		trxFinancieroDTO.setOperacionPei(operacionPei);
		trxFinancieroService.updatePeiBaja(trxFinancieroDTO);
	}

	private String setearCodigoTerminal(ValidationDTO validation) {
		return (String) validation.getOutputValue(ValidationDTO.CODIGO_TERMINAL_PEI);
	}

	private String setearCodigoSucursal(ValidationDTO validation) {
		return (String) validation.getOutputValue(ValidationDTO.CODIGO_SUCURSAL_PEI);
	}

	private String setearCodigoComercio(ValidationDTO validation) {
		Long codComercio = (Long) validation.getOutputValue(ValidationDTO.CODIGO_COMERCIO_PEI);
		return codComercio.toString();

	}

	/**
	 * 
	 * @param validation
	 * @param track2
	 * @param secuenciaOn
	 * @return
	 * @throws FinancieroException
	 */
	private DesencriptarTarjetaDTO decryptValidateCard(ValidationDTO validation, String track1, String track2,
			String secuenciaOn) throws FinancieroException {
		ValidarTarjetaRequestDTO validarTarjetaRequestDTO = new ValidarTarjetaRequestDTO();

		validarTarjetaRequestDTO.setSecuenciaOn(secuenciaOn);
		validarTarjetaRequestDTO.setTrack1(track1);
		validarTarjetaRequestDTO.setTrack2(track2);
		validarTarjetaRequestDTO.setTerminalB24((String) validation.getOutputValue(ValidationDTO.TERMINAL_B24));
		validarTarjetaRequestDTO
				.setTerminalSamId(String.valueOf(validation.getOutputValue(ValidationDTO.TERMINAL_SAM_ID)));

		validarTarjetaRequestDTO.setIdEntidad(validation.getOutputValue(ValidationDTO.ENTIDAD_ID).toString());
		validarTarjetaRequestDTO.setSucursal(validation.getOutputValue(ValidationDTO.SUCURSAL_ID).toString());
		validarTarjetaRequestDTO.setPrefijoOpId(validation.getInputValue(ValidationDTO.PREFIJO_OP_ID));
		validarTarjetaRequestDTO.setSecuenciaOn(secuenciaOn);

		DesencriptarTarjetaDTO desencriptarTarjetaDTO = decryptValidateCard(validarTarjetaRequestDTO);
		return desencriptarTarjetaDTO;
	}

	/**
	 *AQM 
	 * @param validation SOLO para metodos PEIPagar
	 * @param track2
	 * @param secuenciaOn
	 * @return
	 * @throws FinancieroException
	 */
	private DesencriptarTarjetaDTO decryptValidateCardPEIPagar(ValidationDTO validation, String track1, String track2,
			String secuenciaOn) throws FinancieroException {
		ValidarTarjetaRequestDTO validarTarjetaRequestDTO = new ValidarTarjetaRequestDTO();

		validarTarjetaRequestDTO.setSecuenciaOn(secuenciaOn);
		validarTarjetaRequestDTO.setTrack1(track1);
		validarTarjetaRequestDTO.setTrack2(track2);
		validarTarjetaRequestDTO.setTerminalB24((String) validation.getOutputValue(ValidationDTO.TERMINAL_B24));
		validarTarjetaRequestDTO
				.setTerminalSamId(String.valueOf(validation.getOutputValue(ValidationDTO.TERMINAL_SAM_ID)));

		validarTarjetaRequestDTO.setIdEntidad(validation.getOutputValue(ValidationDTO.ENTIDAD_ID).toString());
		validarTarjetaRequestDTO.setSucursal(validation.getOutputValue(ValidationDTO.SUCURSAL_ID).toString());
		validarTarjetaRequestDTO.setPrefijoOpId(validation.getInputValue(ValidationDTO.PREFIJO_OP_ID));		
		validarTarjetaRequestDTO.setSecuenciaOn(secuenciaOn);
		
		/*ACA TOMA el PRODUCTO_ID para su posterior validacion 
		 * EN TABLA: ASOCIACION_PERFIL/DETALLE-AsociacionPerfilSamValidation.java
		 * */
		validarTarjetaRequestDTO.setProductoId(validation.getOutputValue(ValidationDTO.PRODUCTO_ID).toString());

		DesencriptarTarjetaDTO desencriptarTarjetaDTO = decryptValidateCard(validarTarjetaRequestDTO);
		return desencriptarTarjetaDTO;
	}

	
	@Override
	public RealizarDevolucionResultadoPEIDTO doReturnParcialPEI(String idRequerimiento, String ipCliente,
			ValidationDTO validation, RealizarDevolucionPagoParcialPEIRequestDTO requestDTO, String secuenciaOn,
			String idRequerimientoOrig, BigDecimal importe, Intencion in) throws FinancieroException {

		validation.addInputValue(ValidationDTO.PREFIJO_OP_ID,
				prefijoOpService.getPrefijoOpCod("devolucionPagoParcialPei"));
		RealizarDevolucionResultadoPEIDTO dtoResponse = new RealizarDevolucionResultadoPEIDTO();

		// desencripta y valida track2
		DesencriptarTarjetaDTO desencriptarTarjetaDTO = decryptValidateCard(validation,
				requestDTO.getDevolucion().getTracks().getTrack1(), requestDTO.getDevolucion().getTracks().getTrack2(),
				secuenciaOn);

		requestDTO.getDevolucion().getTracks().setTrack1(desencriptarTarjetaDTO.getTrack1());
		requestDTO.getDevolucion().getTracks().setTrack2(desencriptarTarjetaDTO.getTrack2());
		requestDTO.getDevolucion().getTracks().setTrack3(StringUtils.EMPTY);
		dtoResponse = new RealizarDevolucionResultadoPEIDTO();
		dtoResponse.setSecuenciaOn(desencriptarTarjetaDTO.getSecuenciaOn());

		validarDevolucion(validation, idRequerimientoOrig, importe, desencriptarTarjetaDTO, true);

		HeaderPEIRequestDTO header = new HeaderPEIRequestDTO();
		header.setRequerimiento(idRequerimiento);
		header.setCliente(ipCliente);

		requestDTO.getDevolucion().setIdTerminal(setearCodigoTerminal(validation));

		// TrxFinancieroDTO trxFinancieroDTO =
		// this.registerDevolucionPagoParcialPEITrx(validation, requestDTO,
		// FECHA_NULL, secuenciaOn, VALOR_NULL, idRequerimientoOrig, VALOR_NULL,
		// ESTADO_OPERACION_INCOMPLETA);

		RealizarDevolucionPEIResponse peiResponse = null;
		try {
			peiResponse = this.getPeiService().devolucionParcial(header, requestDTO);
		} catch (RedLinkRESTServiceException e) {
			LOGGER.error(e);

			in.setEstadoTrxId(new EstadoTrx((byte) Integer.valueOf(ESTADO_OPERACION_INCOMPLETA).intValue()));
			this.updateIntencion(in);

			ServiceUtils.throwFinaExc(e);
		}

		Pago newPago = this.registerDevolucionPagoParcialPEIIntencion(validation, requestDTO, FECHA_NULL, secuenciaOn,
				VALOR_NULL, idRequerimientoOrig, VALOR_NULL, ESTADO_OPERACION_INCOMPLETA, in);

		dtoResponse.setEstado(peiResponse.getEstado() == null ? ESTADO_OPERACION_ACEPTADA : peiResponse.getEstado());
		dtoResponse.setFecha(peiResponse.getFecha());
		dtoResponse.setTipooperacion(peiResponse.getTipoOperacion());
		dtoResponse.setIdoperacion(peiResponse.getIdOperacion());
		dtoResponse.setNumeroReferenciaBancaria(peiResponse.getNumeroReferenciaBancaria());

		try {
			SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			Date date = dt.parse(dtoResponse.getFecha());
			newPago.setFechaOn(date);
		} catch (ParseException e) {
			LOGGER.error(e);
			throw new FinancieroException(FINA164.getCode(), FINA164.getMsg());
		} catch (Exception e) {
			LOGGER.error(e);
			throw new FinancieroException(FINA164.getCode(), FINA164.getMsg());
		}

		newPago.setSecuenciaOnB24(Integer.valueOf(secuenciaOn));
		newPago.setSecuenciaOnwS(Integer.valueOf(dtoResponse.getSecuenciaOn()));
		newPago.setOperacionPei(Long.valueOf(peiResponse.getIdOperacion()));

		this.updateDevolucionParcialPagoPEITrx(newPago, idRequerimientoOrig, idRequerimiento);

		// this.registerDevolucionPagoPEITrx(trxFinancieroDTO,
		// dtoResponse.getFecha(), secuenciaOn, dtoResponse.getSecuenciaOn(),
		// idRequerimientoOrig, importe, peiResponse.getIdOperacion(),
		// ESTADO_OPERACION_ACTIVA);
		// trxFinancieroService.annulTrxPei(idRequerimientoOrig,
		// idRequerimiento,
		// prefijoOpService.getPrefijoOpCod("realizarPagoPei"));

		return dtoResponse;
	}

	private void validarDevolucion(ValidationDTO validation, String idRequerimientoOrig, BigDecimal importe,
			DesencriptarTarjetaDTO desencriptarTarjetaDTO, boolean isParcial)
			throws FinancieroException, FinancieroValidationException {
		TandemResponse tr = new TandemResponse();
		tr.setTrack2(desencriptarTarjetaDTO.getTrack2());
		String token = tokenServiceConectado.getToken(this.extractPan(tr));

		validation.addInputValue(ValidationDTO.IMPORTE, importe.toPlainString());
		validation.addInputValue(ValidationDTO.TOKENIZADO, token);
		validation.addInputValue(ValidationDTO.PAGO_PREFIJO_OP_ID, prefijoOpService.getPrefijoOpCod("realizarPagoPei"));

		comercioPEIValidation.validate(validation);
		terminalPEIValidation.validate(validation);
		this.returnPEIValidation(idRequerimientoOrig, validation, isParcial);
	}

	private TrxFinancieroDTO registerDevolucionPagoPEITrx(ValidationDTO validation,
			RealizarDevolucionPagoPEIRequestDTO trxData, String fechaOn, String secuenciaOnWS, String secuenciaOnB24,
			String idRequerimientoOrig, BigDecimal importe, String operacionPei, String estado)
			throws FinancieroException {
		TrxFinancieroDTO trxFinancieroDTO = this.registerPEITrx(validation, fechaOn);

		trxFinancieroDTO.setIdRequerimientoBaja(trxFinancieroDTO.getIdRequerimiento());
		trxFinancieroDTO.setIdRequerimiento(idRequerimientoOrig);
		trxFinancieroDTO.setTokenizado(validation.getInputValue(ValidationDTO.TOKENIZADO));

		if (null == importe) {
			trxFinancieroDTO.setImporte("0");
		} else {

			trxFinancieroDTO.setImporte(String.valueOf(importe));
		}
		// if (null == trxFinancieroDTO.getEstadoTrxId()) {
		// trxFinancieroDTO.setEstadoTrxId("0");
		// }
		trxFinancieroDTO.setEstadoTrxId(estado);
		trxFinancieroDTO.setSucursal(StringUtils.leftPad(trxFinancieroDTO.getSucursal(), 8, "0"));
		trxFinancieroDTO.setSecuenciaOnWS(secuenciaOnWS);
		trxFinancieroDTO.setSecuenciaOnB24(secuenciaOnB24);
		trxFinancieroDTO.setOperacionPei(operacionPei);

		trxFinancieroService.insert(trxFinancieroDTO);

		return trxFinancieroDTO;

	}

	private Pago registerDevolucionPagoPEIIntencion(ValidationDTO validation,
			RealizarDevolucionPagoPEIRequestDTO trxData, String fechaOn, String secuenciaOnWS, String secuenciaOnB24,
			String idRequerimientoOrig, BigDecimal importe, String operacionPei, String estado, Intencion in)
			throws FinancieroException {

		TrxFinancieroDTO trxFinancieroDTO = this.registerPEITrx(validation, fechaOn);

		trxFinancieroDTO.setIdRequerimientoBaja(trxFinancieroDTO.getIdRequerimiento());
		trxFinancieroDTO.setIdRequerimiento(idRequerimientoOrig);
		trxFinancieroDTO.setTokenizado(validation.getInputValue(ValidationDTO.TOKENIZADO));

		if (null == importe) {
			trxFinancieroDTO.setImporte("0");
		} else {

			trxFinancieroDTO.setImporte(String.valueOf(importe));
		}
		// if (null == trxFinancieroDTO.getEstadoTrxId()) {
		// trxFinancieroDTO.setEstadoTrxId("0");
		// }
		trxFinancieroDTO.setEstadoTrxId(estado);
		trxFinancieroDTO.setSucursal(StringUtils.leftPad(trxFinancieroDTO.getSucursal(), 8, "0"));
		trxFinancieroDTO.setSecuenciaOnWS(secuenciaOnWS);
		trxFinancieroDTO.setSecuenciaOnB24(secuenciaOnB24);
		trxFinancieroDTO.setOperacionPei(operacionPei);

		// trxFinancieroService.insert(trxFinancieroDTO);
		// return trxFinancieroDTO;

		Pago newPago = trxFinancieroService.devolucionPagoPei(trxFinancieroDTO, in, false);
		return newPago;
	}

	/* AQM */
	private Pago registerDevolucionPagoPEIPagarIntencion(ValidationDTO validation,
			RealizarDevolucionPagoPEIPagarRequestDTO trxData, String fechaOn, String secuenciaOnWS,
			String secuenciaOnB24, String idRequerimientoOrig, String operacionPei, String estado,
			BigDecimal importe, Intencion in) throws FinancieroException {

		TrxFinancieroDTO trxFinancieroDTO = this.registerPEITrx(validation, fechaOn);

		trxFinancieroDTO.setIdRequerimientoBaja(trxFinancieroDTO.getIdRequerimiento());
		trxFinancieroDTO.setIdRequerimiento(idRequerimientoOrig);
		
		TandemResponse tr = new TandemResponse();
		tr.setTrack2(trxData.getDevolucion().getTracks().getTrack2());
		trxFinancieroDTO.setTokenizado(tokenServiceConectado.getToken(this.extractPan(tr)));
		//trxFinancieroDTO.setTokenizado(validation.getInputValue(ValidationDTO.TOKENIZADO));
		
		//trxFinancieroDTO.setImporte(String.valueOf(importe));
		//BigDecimal importe_1 = new BigDecimal(trxFinancieroDTO.getImporte()).divide(new BigDecimal(100));

		if (null == importe) {
			trxFinancieroDTO.setImporte("0");
		} else {

			trxFinancieroDTO.setImporte(String.valueOf(importe));
		}
		// if (null == trxFinancieroDTO.getEstadoTrxId()) {
		// trxFinancieroDTO.setEstadoTrxId("0");
		// }
		trxFinancieroDTO.setEstadoTrxId(estado);
		trxFinancieroDTO.setSucursal(StringUtils.leftPad(trxFinancieroDTO.getSucursal(), 8, "0"));
		trxFinancieroDTO.setSecuenciaOnWS(secuenciaOnWS);
		trxFinancieroDTO.setSecuenciaOnB24(secuenciaOnB24);
		trxFinancieroDTO.setOperacionPei(operacionPei);

		// trxFinancieroService.insert(trxFinancieroDTO);
		// return trxFinancieroDTO;

		Pago newPago = trxFinancieroService.devolucionPagoPei(trxFinancieroDTO, in, false);
		return newPago;
	}

	private TrxFinancieroDTO registerDevolucionPagoParcialPEITrx(ValidationDTO validation,
			RealizarDevolucionPagoParcialPEIRequestDTO trxData, String fechaOn, String secuenciaOnWS,
			String secuenciaOnB24, String idRequerimientoOrig, String operacionPei, String estado)
			throws FinancieroException {
		TrxFinancieroDTO trxFinancieroDTO = this.registerPEITrx(validation, fechaOn);

		trxFinancieroDTO.setIdRequerimientoBaja(trxFinancieroDTO.getIdRequerimiento());
		trxFinancieroDTO.setIdRequerimiento(idRequerimientoOrig);

		TandemResponse tr = new TandemResponse();
		tr.setTrack2(trxData.getDevolucion().getTracks().getTrack2());
		trxFinancieroDTO.setTokenizado(tokenServiceConectado.getToken(this.extractPan(tr)));

		BigDecimal importe = new BigDecimal(trxData.getDevolucion().getImporte()).divide(new BigDecimal(100));
		trxFinancieroDTO.setImporte(String.valueOf(importe));

		if (null == trxFinancieroDTO.getImporte()) {
			trxFinancieroDTO.setImporte("0");
		}
		// if (null == trxFinancieroDTO.getEstadoTrxId()) {
		// trxFinancieroDTO.setEstadoTrxId("0");
		// }
		trxFinancieroDTO.setEstadoTrxId(estado);
		trxFinancieroDTO.setSucursal(StringUtils.leftPad(trxFinancieroDTO.getSucursal(), 8, "0"));
		trxFinancieroDTO.setSecuenciaOnWS(secuenciaOnWS);
		trxFinancieroDTO.setSecuenciaOnB24(secuenciaOnB24);
		trxFinancieroDTO.setOperacionPei(operacionPei);

		trxFinancieroService.insert(trxFinancieroDTO);

		return trxFinancieroDTO;
	}

	private Pago registerDevolucionPagoParcialPEIIntencion(ValidationDTO validation,
			RealizarDevolucionPagoParcialPEIRequestDTO trxData, String fechaOn, String secuenciaOnWS,
			String secuenciaOnB24, String idRequerimientoOrig, String operacionPei, String estado, Intencion in)
			throws FinancieroException {
		TrxFinancieroDTO trxFinancieroDTO = this.registerPEITrx(validation, fechaOn);

		trxFinancieroDTO.setIdRequerimientoBaja(trxFinancieroDTO.getIdRequerimiento());
		trxFinancieroDTO.setIdRequerimiento(idRequerimientoOrig);

		TandemResponse tr = new TandemResponse();
		tr.setTrack2(trxData.getDevolucion().getTracks().getTrack2());
		trxFinancieroDTO.setTokenizado(tokenServiceConectado.getToken(this.extractPan(tr)));

		BigDecimal importe = new BigDecimal(trxData.getDevolucion().getImporte()).divide(new BigDecimal(100));
		trxFinancieroDTO.setImporte(String.valueOf(importe));

		if (null == trxFinancieroDTO.getImporte()) {
			trxFinancieroDTO.setImporte("0");
		}
		// if (null == trxFinancieroDTO.getEstadoTrxId()) {
		// trxFinancieroDTO.setEstadoTrxId("0");
		// }
		trxFinancieroDTO.setEstadoTrxId(estado);
		trxFinancieroDTO.setSucursal(StringUtils.leftPad(trxFinancieroDTO.getSucursal(), 8, "0"));
		trxFinancieroDTO.setSecuenciaOnWS(secuenciaOnWS);
		trxFinancieroDTO.setSecuenciaOnB24(secuenciaOnB24);
		trxFinancieroDTO.setOperacionPei(operacionPei);

		// trxFinancieroService.insert(trxFinancieroDTO);

		Pago newPago = trxFinancieroService.devolucionPagoPei(trxFinancieroDTO, in, true);

		// return trxFinancieroDTO;
		return newPago;
	}

	/**
	 * @return the tandemWorkingKeyService
	 */
	public TandemWorkingKeyService getTandemWorkingKeyService() {
		return tandemWorkingKeyService;
	}

	/**
	 * @param tandemWorkingKeyService
	 *            the tandemWorkingKeyService to set
	 */
	public void setTandemWorkingKeyService(TandemWorkingKeyService tandemWorkingKeyService) {
		this.tandemWorkingKeyService = tandemWorkingKeyService;
	}

	/**
	 * @return the tandemAccountService
	 */
	public TandemAccountService getTandemAccountService() {
		return tandemAccountService;
	}

	/**
	 * @param tandemAccountService
	 *            the tandemAccountService to set
	 */
	public void setTandemAccountService(TandemAccountService tandemAccountService) {
		this.tandemAccountService = tandemAccountService;
	}

	/**
	 * @return the enrolamientoSamService
	 */
	public EnrolamientoSamService getEnrolamientoSamService() {
		return enrolamientoSamService;
	}

	/**
	 * @param enrolamientoSamService
	 *            the enrolamientoSamService to set
	 */
	public void setEnrolamientoSamService(EnrolamientoSamService enrolamientoSamService) {
		this.enrolamientoSamService = enrolamientoSamService;
	}

	/**
	 * @return the operacionLimitesValidation
	 */
	public AbstractFSValidation getOperacionLimitesValidation() {
		return operacionLimitesValidation;
	}

	/**
	 * @param operacionLimitesValidation
	 *            the operacionLimitesValidation to set
	 */
	public void setOperacionLimitesValidation(AbstractFSValidation operacionLimitesValidation) {
		this.operacionLimitesValidation = operacionLimitesValidation;
	}

	/**
	 * @return the anulacionValidation
	 */
	public AbstractFSValidation getAnulacionValidation() {
		return anulacionValidation;
	}

	/**
	 * @param anulacionValidation
	 *            the anulacionValidation to set
	 */
	public void setAnulacionValidation(AbstractFSValidation anulacionValidation) {
		this.anulacionValidation = anulacionValidation;
	}

	/**
	 * @return the maxResults
	 */
	public Integer getMaxResults() {
		return maxResults;
	}

	/**
	 * @param maxResults
	 *            the maxResults to set
	 */
	public void setMaxResults(Integer maxResults) {
		this.maxResults = maxResults;
	}

	/**
	 * @return the peiService
	 */
	public PeiService getPeiService() {
		return peiService;
	}

	/**
	 * @param peiService
	 *            the peiService to set
	 */
	public void setPeiService(PeiService peiService) {
		this.peiService = peiService;
	}

	/**
	 * @return the prefijoEntidadService
	 */
	public PrefijoEntidadService getPrefijoEntidadService() {
		return prefijoEntidadService;
	}

	/**
	 * @param prefijoEntidadService
	 *            the prefijoEntidadService to set
	 */
	public void setPrefijoEntidadService(PrefijoEntidadService prefijoEntidadService) {
		this.prefijoEntidadService = prefijoEntidadService;
	}

	/**
	 * @return the anulacionValidation
	 */
	public AbstractFSValidation getDevolucionPEIValidation() {
		return devolucionPEIValidation;
	}

	/**
	 * @param anulacionValidation
	 *            the anulacionValidation to set
	 */
	public void setDevolucionPEIValidation(DevolucionPEIValidation devolucionPEIValidation) {
		this.devolucionPEIValidation = devolucionPEIValidation;
	}

	/**
	 * @param anulacionValidation
	 *            the anulacionValidation to set
	 */
	public void setAnulacionPagoDebitoValidation(AbstractFSValidation anulacionPagoDebitoValidation) {
		this.anulacionPagoDebitoValidation = anulacionPagoDebitoValidation;
	}

	@Override
	public Intencion registrarInicioTransaccion(FinancieroServiceBaseRequestDTO request) {
		return this.inicioTransaccion(request);
	}

	@Override
	public Intencion registrarInicioTransaccionPei(Long terminalSamId, int prefijo_op, String idRequerimiento) {
		return this.inicioTransaccionPei(terminalSamId, prefijo_op, idRequerimiento);
	}

	@Override
	public void registrarErrorTransaccion(Intencion inten) {
		this.updateIntencion(inten);
	}

	@Override
	public Intencion registrarPruebaServicio(Long terminalSamId, int prefijo_op, String idRequerimiento)
			throws FinancieroException {
		return this.pruebaServicio(terminalSamId, prefijo_op, idRequerimiento);
	}

}
