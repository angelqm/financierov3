/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA106;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA108;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA123;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA150;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import ar.com.redlink.framework.services.crud.RedLinkGenericService;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.DispositivoPos;
import ar.com.redlink.sam.financieroservices.entity.EstadoRegistro;
import ar.com.redlink.sam.financieroservices.entity.TerminalSam;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;

/**
 * Valida la terminal SAM.
 * 
 * @author aguerrea
 * 
 */
public class TerminalSamMockValidation extends AbstractFSValidation {

	//private static final Logger LOGGER = Logger.getLogger(TerminalSamMockValidation.class);

	private RedLinkGenericService<TerminalSam> terminalSamService;

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.validation.FSValidation#validate(ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO)
	 */
	@Override
	public ValidationDTO validate(ValidationDTO validation)	throws FinancieroException {
		nullCheck(validation, ValidationDTO.TERMINAL_COD, FINA106);

		TerminalSam terminalSam = null;

		//try {
		/*	List<TerminalSam> terminalesSam = getTerminalSamService().getAll(new TerminalSamByCodAndEntidadSearchFilter(
							(Integer) validation.getOutputValue(ValidationDTO.ENTIDAD_ID),
							validation.getInputValue(ValidationDTO.TERMINAL_COD)),
							null).getResult();
		*/
			
			List<TerminalSam> terminalesSam = new ArrayList();
			
			TerminalSam t = new TerminalSam();
			
			EstadoRegistro e= new EstadoRegistro();
			e.setEstadoRegistroId((short)0);
			e.setCodigo('0');
			
			t.setCodTerminal("00007000");
			t.setEstadoRegistro(e);
			t.setFechaActivacion(new Date());
			t.setFechaAlta(new Date());
			t.setProductoId(4L);
			t.setTerminalB24("01088800003191");
			t.setTerminalSamId(945132500017000L);
			t.setTipoTerminal((short)1);
			t.setTipoTerminalB24("A5");
			
			DispositivoPos dp= new DispositivoPos();
			dp.setDispositivoPosId(951957381L);
			t.setDispositivoPos(dp);
			
			terminalesSam.add(t);
	
			if (terminalesSam.size() > 0) {
				terminalSam = terminalesSam.get(0);
			}
		//} catch (RedLinkServiceException e) {
		//	LOGGER.error("Error al acceder el servicio de TerminalSAM: "+ e.getMessage(), e);
		//	throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		//}
		
		if (null == terminalSam) {
			throw new FinancieroValidationException(FINA108.getCode(),FINA108.getMsg());
		} else {
			
			//validateEstadoRegistro(terminalSam, FINA107);
			//validateDates(terminalSam, FINA107);
			
			DispositivoPos dispositivoPos = terminalSam.getDispositivoPos();

			if (dispositivoPos != null) {
				validation.addOutputValue(ValidationDTO.DISPOSITIVO_POS_ID,	dispositivoPos.getDispositivoPosId());

			} else {
				throw new FinancieroValidationException(FINA123.getCode(),FINA123.getMsg());
			}

			validation.addOutputValue(ValidationDTO.TERMINAL_SAM_ID, terminalSam.getTerminalSamId());
			
			if (terminalSam.getTerminalB24().equals(null)) {
				throw new FinancieroValidationException(FINA150.getCode(),	FINA150.getMsg());
			} 
				
			validation.addOutputValue(ValidationDTO.TERMINAL_B24, terminalSam.getTerminalB24().trim());

			if (terminalSam.getTipoTerminalB24()!=null) {			
				validation.addOutputValue(ValidationDTO.TIPO_TERMINAL_B24,	terminalSam.getTipoTerminalB24().trim());
			}
			
			if (terminalSam.getProductoId()!=null) {			
				validation.addOutputValue(ValidationDTO.PRODUCTO_ID, terminalSam.getProductoId());
			}
			
			validation.addOutputValue(ValidationDTO.TERM_TYPE,	terminalSam.getTipoTerminal());
			
		}
		nextValidation(validation);

		return validation;
	}

	/**
	 * @return the terminalSamService
	 */
	public RedLinkGenericService<TerminalSam> getTerminalSamService() {
		return terminalSamService;
	}

	/**
	 * @param terminalSamService
	 *            the terminalSamService to set
	 */
	public void setTerminalSamService(
			RedLinkGenericService<TerminalSam> terminalSamService) {
		this.terminalSamService = terminalSamService;
	}

}
