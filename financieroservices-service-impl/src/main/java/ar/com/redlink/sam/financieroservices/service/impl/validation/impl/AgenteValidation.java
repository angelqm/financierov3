/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  29/03/2015 - 14:41:59
 */

package ar.com.redlink.sam.financieroservices.service.impl.validation.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA117;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA118;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA200;

import java.util.List;

import org.apache.log4j.Logger;

import ar.com.redlink.framework.services.crud.RedLinkGenericService;
import ar.com.redlink.framework.services.exception.RedLinkServiceException;
import ar.com.redlink.sam.financieroservices.dao.search.filter.AgenteByAgenteIdAndEntidadIdSearchFilter;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.Agente;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;

/**
 * Validacion de Agente.
 * 
 * @author aguerrea
 * 
 */
public class AgenteValidation extends AbstractFSValidation {

	private static final Logger LOGGER = Logger
			.getLogger(AgenteValidation.class);
	
	private RedLinkGenericService<Agente> agenteService;
	

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.service.validation.FSValidation#validate(ar.com.redlink.sam.financieroservices.dto.validation.validation)
	 */
	@Override
	public ValidationDTO validate(ValidationDTO validation)	throws FinancieroException {
		Agente agente = null;

		try {

			//LOGGER.info("[DAO INI] AgenteByAgenteIdAndEntidadIdSearchFilter");
			
			List<Agente> agentes = getAgenteService().getAll(new AgenteByAgenteIdAndEntidadIdSearchFilter(
							(Long) validation.getOutputValue(ValidationDTO.AGENTE_ID),
							(Integer) validation.getOutputValue(ValidationDTO.ENTIDAD_ID)),
							null).getResult();
			
			//LOGGER.info("[DAO FIN] AgenteByAgenteIdAndEntidadIdSearchFilter");

			if (agentes.size() > 0) {
				agente = agentes.get(0);
			}
		} catch (RedLinkServiceException e) {
			LOGGER.error("Error al acceder el servicio de Agente: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}

		if (null == agente) {
			throw new FinancieroValidationException(FINA118.getCode(),	FINA118.getMsg());
		} else {
			validateEstadoRegistro(agente, FINA117);
			validateDates(agente, FINA117);
		}

		nextValidation(validation);

		return validation;
	}

	/**
	 * @return the agenteService
	 */
	public RedLinkGenericService<Agente> getAgenteService() {
		return agenteService;
	}

	/**
	 * @param agenteService
	 *            the agenteService to set
	 */
	public void setAgenteService(RedLinkGenericService<Agente> agenteService) {
		this.agenteService = agenteService;
	}

}
