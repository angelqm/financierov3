/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA109;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA110;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA121;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA122;
import java.util.ArrayList;
import java.util.List;
import ar.com.redlink.framework.services.crud.RedLinkGenericService;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.DispositivoPos;
import ar.com.redlink.sam.financieroservices.entity.EnrolamientoSam;
import ar.com.redlink.sam.financieroservices.entity.EstadoRegistro;
import ar.com.redlink.sam.financieroservices.entity.TipoEncripcionB24;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;

/**
 * Valida el dispositivo POS.
 * 
 */
public class DispositivoPosMockValidation extends AbstractFSValidation {
	
	private RedLinkGenericService<DispositivoPos> dispositivoPosService;
	private RedLinkGenericService<EnrolamientoSam> enrolamientoSamService;

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.validation.FSValidation#validate(ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO)
	 */
	@Override
	public ValidationDTO validate(ValidationDTO validation)	throws FinancieroException {
		DispositivoPos dispositivoPos = null;

		//try {
			//dispositivoPos = getDispositivoPosService().getById((Long) validation.getOutputValue(ValidationDTO.DISPOSITIVO_POS_ID));
			dispositivoPos= new DispositivoPos();
			dispositivoPos.setDispositivoPosId(951957381);
			dispositivoPos.setIdentificadorDisp("000-326-904-231");
			
			EstadoRegistro e= new EstadoRegistro();
			e.setEstadoRegistroId((short)0);
			e.setCodigo('0');
			dispositivoPos.setEstadoRegistro(e);
			
			TipoEncripcionB24 te= new TipoEncripcionB24();
			te.setTipoEncripcionB24Id((short)2);
			te.setEstadoRegistro(e);
			dispositivoPos.setTipoEncripcionB24(te);
			
			
		//} catch (RedLinkServiceException e) {
		//	throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		//}

		if (!dispositivoPos.getIdentificadorDisp().equals(validation.getInputValue(ValidationDTO.IDENT_DISPOSITIVO))) {
			throw new FinancieroValidationException(FINA122.getCode(),	FINA122.getMsg());
		}

		validateEstadoRegistro(dispositivoPos, FINA121);
		validateDates(dispositivoPos, FINA121);
		
		/*verificar*/
		validateEstadoRegistro(dispositivoPos.getTipoEncripcionB24(), FINA121);
		
		validation.addOutputValue(ValidationDTO.TIPO_ENCRIPCION_B24,dispositivoPos.getTipoEncripcionB24().getTipoEncripcionB24Id());

		// Parte 2 de la validacion.
		EnrolamientoSam enrolamientoSam = null;
		//try {
			
			//List<EnrolamientoSam> enrolamientosSam = getEnrolamientoSamService().getAll(new TerminalSamIdSearchFilter((Long) validation.getOutputValue(ValidationDTO.TERMINAL_SAM_ID)),null).getResult();
			List<EnrolamientoSam> enrolamientosSam = new ArrayList();
			
			EnrolamientoSam es= new EnrolamientoSam();
			es.setEnrolamientoSamId(30424);
			es.setIdentificadorTerm("PEI00000000022");
			
			enrolamientosSam.add(es);
			
			if (enrolamientosSam.size() > 0) {
				enrolamientoSam = enrolamientosSam.get(0);
			}
		//} catch (RedLinkServiceException e) {
		//	throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		//}

		if (null == enrolamientoSam) {
			throw new FinancieroValidationException(FINA109.getCode(),	FINA109.getMsg());
		}

		if (!enrolamientoSam.getIdentificadorTerm().equals(	validation.getInputValue(ValidationDTO.IDENT_TERMINAL))) {
			// No coincide el identificador de terminal.
			throw new FinancieroValidationException(FINA110.getCode(),	FINA110.getMsg());
		}

		nextValidation(validation);

		return validation;
	}

	/**
	 * @return the dispositivoPosService
	 */
	public RedLinkGenericService<DispositivoPos> getDispositivoPosService() {
		return dispositivoPosService;
	}

	/**
	 * @param dispositivoPosService
	 *            the dispositivoPosService to set
	 */
	public void setDispositivoPosService(
			RedLinkGenericService<DispositivoPos> dispositivoPosService) {
		this.dispositivoPosService = dispositivoPosService;
	}

	/**
	 * @return the enrolamientoSamService
	 */
	public RedLinkGenericService<EnrolamientoSam> getEnrolamientoSamService() {
		return enrolamientoSamService;
	}

	/**
	 * @param enrolamientoSamService
	 *            the enrolamientoSamService to set
	 */
	public void setEnrolamientoSamService(	RedLinkGenericService<EnrolamientoSam> enrolamientoSamService) {
		this.enrolamientoSamService = enrolamientoSamService;
	}

}
