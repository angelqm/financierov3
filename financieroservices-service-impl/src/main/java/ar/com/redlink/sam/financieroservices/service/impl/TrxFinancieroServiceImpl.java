/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  31/03/2015 - 08:12:44
 */

package ar.com.redlink.sam.financieroservices.service.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA200;
import java.util.Date;
import org.apache.log4j.Logger;
import ar.com.redlink.framework.services.crud.impl.RedLinkGenericServiceImpl;
import ar.com.redlink.sam.financieroservices.dao.TrxFinancieroDAO;
import ar.com.redlink.sam.financieroservices.dto.TrxFinancieroDTO;
import ar.com.redlink.sam.financieroservices.dto.TrxFinancieroValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.Intencion;
import ar.com.redlink.sam.financieroservices.entity.Pago;
import ar.com.redlink.sam.financieroservices.entity.SimpleTrxFinanciero;
import ar.com.redlink.sam.financieroservices.entity.TrxFinanciero;
import ar.com.redlink.sam.financieroservices.entity.TrxFinancieroValidation;
import ar.com.redlink.sam.financieroservices.service.ModelMapperService;
import ar.com.redlink.sam.financieroservices.service.TrxFinancieroService;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;

/**
 * Implementacion del servicio para {@link TrxFinanciero}.
 * 
 * @author aguerrea
 * 
 */
public class TrxFinancieroServiceImpl extends RedLinkGenericServiceImpl<TrxFinanciero> implements TrxFinancieroService {

	private static final Logger LOGGER = Logger.getLogger(TrxFinancieroServiceImpl.class);

	private TrxFinancieroDAO trxFinancieroDAO;
	private ModelMapperService modelMapperService;

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.TrxFinancieroService#validateTrx(ar.com.redlink.sam.financieroservices.dto.TrxFinancieroValidationDTO)
	 */
	@Override
	@Deprecated
	public TrxFinanciero validateTrx(TrxFinancieroValidationDTO trxFinancieroValidationDTO) throws FinancieroException {
		TrxFinancieroValidation trxFinancieroValidation = modelMapperService
				.modelMapperHelper(trxFinancieroValidationDTO, TrxFinancieroValidation.class);
		TrxFinanciero result;

		try {
			result = trxFinancieroDAO.validateTrx(trxFinancieroValidation);
		} catch (Exception e) {
			LOGGER.error("Error de DAO: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}

		return result;
	}

	public Pago validateCancel(TrxFinancieroValidationDTO trxFinancieroValidationDTO) throws FinancieroException {
		TrxFinancieroValidation trxFinancieroValidation = modelMapperService
				.modelMapperHelper(trxFinancieroValidationDTO, TrxFinancieroValidation.class);

		// TrxFinanciero result;
		Pago result;
		try {
			// result = trxFinancieroDAO.validateTrx(trxFinancieroValidation);
			result = trxFinancieroDAO.validateCancel(trxFinancieroValidation);
		} catch (Exception e) {
			LOGGER.error("Error de DAO [validateCancel]: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), e.getCause().toString());
		}

		return result;
	}

	/**
	 * s
	 * 
	 * @see ar.com.redlink.sam.financieroservices.service.TrxFinancieroService#getMaxSeqForTerm(java.lang.String)
	 */
	@Override
	public String getMaxSeqForTerm(String terminalSamId) throws FinancieroException {
		String result = "0";
		try {
			// result = trxFinancieroDAO.getMaxSeqNumForTerm(terminalSamId);
			result = trxFinancieroDAO.getMaxSeqNumForTermAct(terminalSamId);
		} catch (Exception e) {
			LOGGER.error("[getMaxSeqForTerm] Error de DAO: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}

		return result;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.service.TrxFinancieroService#insert()
	 */
	@Override
	@Deprecated
	public void insert(TrxFinancieroDTO trxFinancieroDTO) throws FinancieroException {
		SimpleTrxFinanciero trxFinanciero = modelMapperService.modelMapperHelper(trxFinancieroDTO,
				SimpleTrxFinanciero.class);

		try {
			trxFinancieroDAO.insert(trxFinanciero);
		} catch (Exception e) {
			LOGGER.error("Error de DAO: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.service.TrxFinancieroService#rollbackTrx(java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	public void rollbackTrx(Long trxFinancieroId, String idRequerimientoBaja) throws FinancieroException {
		try {
			// trxFinancieroDAO.updateEntry(trxFinancieroId,idRequerimientoBaja,
			// "2");
			trxFinancieroDAO.reverso(trxFinancieroId, idRequerimientoBaja, "2");

		} catch (Exception e) {
			LOGGER.error("Error de DAO - TrxFinanciero: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), e.getCause().toString());
		}
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.TrxFinancieroService#annulTrx(java.lang.String)
	 */
	@Override
	@Deprecated
	public void annulTrx(Long trxFinancieroId, String idRequerimientoBaja) throws FinancieroException {
		try {
			trxFinancieroDAO.updateEntry(trxFinancieroId, idRequerimientoBaja, "1");
		} catch (Exception e) {
			LOGGER.error("Error de DAO - TrxFinanciero: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}
	}

	@Override
	public void cancelExtraction(Long trxFinancieroId, String idRequerimientoBaja, Intencion in)
			throws FinancieroException {
		try {
			// trxFinancieroDAO.updateEntry(trxFinancieroId,idRequerimientoBaja,
			// "1");
			trxFinancieroDAO.cancelExtraction(trxFinancieroId, idRequerimientoBaja, "1", in);

		} catch (Exception e) {
			LOGGER.error("Error de DAO - cancelExtraction: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), e.getCause().toString());
		}
	}

	@Override
	public void rollbackDeposit(Long trxFinancieroId, String idRequerimientoBaja, Intencion in)
			throws FinancieroException {
		try {
			// trxFinancieroDAO.updateEntry(trxFinancieroId,idRequerimientoBaja,
			// "1");
			trxFinancieroDAO.rollbackDeposit(trxFinancieroId, idRequerimientoBaja, "1", in);

		} catch (Exception e) {
			LOGGER.error("Error de DAO - rollbackDeposit: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), e.getCause().toString());
		}
	}

	public void anullDebitPay(Long trxFinancieroId, String idRequerimientoBaja, Intencion in)
			throws FinancieroException {
		try {
			trxFinancieroDAO.rollBackDebitPay(trxFinancieroId, idRequerimientoBaja, "1", in);
		} catch (Exception e) {
			LOGGER.error("Error de DAO - anullbackDebitPay: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), e.getCause().toString());
		}
	}

	public void rollbackDebit(Long trxFinancieroId, String idRequerimientoBaja, Intencion in)
			throws FinancieroException {
		try {
			// trxFinancieroDAO.updateEntry(trxFinancieroId,idRequerimientoBaja,
			// "1");
			trxFinancieroDAO.anullDebit(trxFinancieroId, idRequerimientoBaja, "1", in);

		} catch (Exception e) {
			LOGGER.error("Error de DAO - rollbackDebit: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), e.getCause().toString());
		}
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.TrxFinancieroService#annulTrx(java.lang.String)
	 */
	@Override
	@Deprecated
	public void annulTrxPei(String idRequerimientoOrig, String idRequerimientoBaja, String prefijoOpId)
			throws FinancieroException {
		try {
			trxFinancieroDAO.updateEntryPei(idRequerimientoOrig, idRequerimientoBaja, "1", prefijoOpId);
		} catch (Exception e) {
			LOGGER.error("Error de DAO - TrxFinanciero: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}
	}

	@Deprecated
	public void annulTrxPeiIntencion(String idRequerimientoOrig, String idRequerimientoBaja, String prefijoOpId)
			throws FinancieroException {
		try {
			trxFinancieroDAO.updateEntryPei(idRequerimientoOrig, idRequerimientoBaja, "1", prefijoOpId);
		} catch (Exception e) {
			LOGGER.error("Error de DAO - TrxFinanciero: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.service.TrxFinancieroService#getSum(java.lang.String,
	 *      java.lang.String, java.util.Date, java.util.Date, java.lang.String,
	 *      byte, java.lang.Long)
	 */
	@Override
	public Double getSum(String filterField, String filterId, Date dateFrom, Date dateTo, String tokenizado,
			Long prefijoOpId) throws FinancieroException {
		Double sum = 0D;
		try {
			sum = trxFinancieroDAO.getSum(filterField, filterId, dateFrom, dateTo, tokenizado, prefijoOpId);
		} catch (Exception e) {
			LOGGER.error("Error al obtener la suma en TRXFinancieroDAO: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}

		return sum;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.service.TrxFinancieroService#getCount(java.lang.String,
	 *      java.lang.String, java.util.Date, java.util.Date, java.lang.String,
	 *      byte, java.lang.Long)
	 */
	@Override
	public Integer getCount(String filterField, String filterId, Date dateFrom, Date dateTo, String tokenizado,
			Long prefijoOpId) throws FinancieroException {
		Integer count = 0;

		try {
			count = trxFinancieroDAO.getCount(filterField, filterId, dateFrom, dateTo, tokenizado, prefijoOpId);
		} catch (Exception e) {
			LOGGER.error("Error al obtener el count en TRXFinancieroDAO: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}

		return count;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.service.TrxFinancieroService#checkIdRequerimiento(java.lang.String)
	 */
	@Override
	public boolean checkIdRequerimiento(String idRequerimiento) throws FinancieroException {
		// List<Long> result = null;
		boolean resultNew;
		try {
			// result = trxFinancieroDAO.checkIdRequerimiento(idRequerimiento);
			resultNew = trxFinancieroDAO.checkIdRequerimientoAct(idRequerimiento);
		} catch (Exception e) {
			LOGGER.error("Error al checkear el ID de Requerimiento en TRXFinancieroDAO: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}

		// return !(null == result || 0 == result.size());
		return resultNew;
	}

	public boolean checkIdRequerimientoOperacionPeiNulo(String idRequerimiento) throws FinancieroException {
		// List<Long> result = null;
		boolean resultNew;
		try {
			// result =
			// trxFinancieroDAO.checkIdRequerimientoOperacionPeiNulo(idRequerimiento);
			resultNew = trxFinancieroDAO.checkIdRequerimientoOperacionPeiNuloAct(idRequerimiento);
		} catch (Exception e) {
			LOGGER.error("Error al checkear el ID de Requerimiento en TRXFinancieroDAO: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg() + " - " + e.getMessage());
		}
		// return !(null == result || 0 == result.size());
		return resultNew;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.service.TrxFinancieroService#checkSeqNum(java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	public Long getReversableOperationIds(String seqNum, String terminalSamId, String reversableOperationIds)
			throws FinancieroException {
		// List<Long> seqNumList = null;

		Long seqNumResult = null;

		try {
			// seqNumList = trxFinancieroDAO.getReversableOperationIds(seqNum,
			// terminalSamId, reversableOperationIds);
			seqNumResult = trxFinancieroDAO.getReversableOperationIdsAct(seqNum, terminalSamId, reversableOperationIds);
		} catch (Exception e) {
			LOGGER.error("Error al obtener el valor de SeqNum en TRXFinancieroDAO: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), e.getCause().toString());
		}

		/*
		 * if (seqNumList.size() > 1) { // TODO: Modificar el mensaje. Devolver
		 * un mensaje que diferencie. // -1 } else if (seqNumList.size() == 1) {
		 * seqNumResult = seqNumList.get(0); }
		 */

		// return seqNumResult;
		return seqNumResult;
	}

	/**
	 * @return the trxFinancieroDAO
	 */
	public TrxFinancieroDAO getTrxFinancieroDAO() {
		return trxFinancieroDAO;
	}

	/**
	 * @param trxFinancieroDAO
	 *            the trxFinancieroDAO to set
	 */
	public void setTrxFinancieroDAO(TrxFinancieroDAO trxFinancieroDAO) {
		this.trxFinancieroDAO = trxFinancieroDAO;
	}

	/**
	 * @return the modelMapperService
	 */
	public ModelMapperService getModelMapperService() {
		return modelMapperService;
	}

	/**
	 * @param modelMapperService
	 *            the modelMapperService to set
	 */
	public void setModelMapperService(ModelMapperService modelMapperService) {
		this.modelMapperService = modelMapperService;
	}

	@Override
	public void updatePei(Pago pago) throws FinancieroException {

		try {
			trxFinancieroDAO.updatePagoPei(pago);
		} catch (Exception e) {
			LOGGER.error("Error de DAO: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), e.getCause().toString());
		}
	}

	public void updateDevolucionPei(Pago pago, String idRequerimientoOrig, boolean isParcial)
			throws FinancieroException {
		try {
			trxFinancieroDAO.updateDevolucionPagoPei(pago, idRequerimientoOrig, isParcial);
		} catch (Exception e) {
			LOGGER.error("Error de DAO[updateDevolucionPei]: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), e.getCause().toString());
		}
	}

	@Override
	public void updatePeiBaja(TrxFinancieroDTO trxFinancieroDTO) throws FinancieroException {
		SimpleTrxFinanciero trxFinanciero = modelMapperService.modelMapperHelper(trxFinancieroDTO,
				SimpleTrxFinanciero.class);

		try {
			trxFinancieroDAO.updatePeiBaja(trxFinanciero);
		} catch (Exception e) {
			LOGGER.error("Error de DAO: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}
	}

	@Override
	public void registerWorkingKey(TrxFinancieroDTO trxFinancieroDTO, Intencion in) throws FinancieroException {
		SimpleTrxFinanciero trxFinanciero = modelMapperService.modelMapperHelper(trxFinancieroDTO,
				SimpleTrxFinanciero.class);

		try {
			// trxFinancieroDAO.insert(trxFinanciero);
			trxFinancieroDAO.registerWorkingKey(trxFinanciero, in);
		} catch (Exception e) {
			LOGGER.error("Error de DAO[registerWorkingKey]: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), e.getCause().toString());
		}

	}

	@Override
	public void validarTarjeta(TrxFinancieroDTO trxFinancieroDTO, Intencion in) throws FinancieroException {
		SimpleTrxFinanciero trxFinanciero = modelMapperService.modelMapperHelper(trxFinancieroDTO,
				SimpleTrxFinanciero.class);

		try {
			// trxFinancieroDAO.insert(trxFinanciero);
			trxFinancieroDAO.validarTarjeta(trxFinanciero, in);
		} catch (Exception e) {
			LOGGER.error("Error de DAO[validarTarjeta]: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), e.getCause().toString());
		}

	}

	@Override
	public void getBalance(TrxFinancieroDTO trxFinancieroDTO, Intencion in) throws FinancieroException {
		SimpleTrxFinanciero trxFinanciero = modelMapperService.modelMapperHelper(trxFinancieroDTO,
				SimpleTrxFinanciero.class);

		try {
			// trxFinancieroDAO.insert(trxFinanciero);
			trxFinancieroDAO.getBalance(trxFinanciero, in);
		} catch (Exception e) {
			LOGGER.error("Error de DAO[getBalance]: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), e.getCause().toString());
		}
	}

	@Override
	public void doDeposit(TrxFinancieroDTO trxFinancieroDTO, Intencion in) throws FinancieroException {
		SimpleTrxFinanciero trxFinanciero = modelMapperService.modelMapperHelper(trxFinancieroDTO,
				SimpleTrxFinanciero.class);

		try {
			// trxFinancieroDAO.insert(trxFinanciero);
			trxFinancieroDAO.doDeposit(trxFinanciero, in);
		} catch (Exception e) {
			LOGGER.error("Error de DAO[doDeposit]: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), e.getCause().toString());
		}
	}

	@Override
	public void doDebitPayment(TrxFinancieroDTO trxFinancieroDTO, Intencion in) throws FinancieroException {
		SimpleTrxFinanciero trxFinanciero = modelMapperService.modelMapperHelper(trxFinancieroDTO,
				SimpleTrxFinanciero.class);

		try {
			trxFinancieroDAO.doDebitPayment(trxFinanciero, in);
		} catch (Exception e) {
			LOGGER.error("Error de DAO[doDebitPayment]: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}
	}

	@Override
	public void doTransfer(TrxFinancieroDTO trxFinancieroDTO, Intencion in) throws FinancieroException {
		SimpleTrxFinanciero trxFinanciero = modelMapperService.modelMapperHelper(trxFinancieroDTO,
				SimpleTrxFinanciero.class);

		try {
			// trxFinancieroDAO.insert(trxFinanciero);
			trxFinancieroDAO.doTransfer(trxFinanciero, in);
		} catch (Exception e) {
			LOGGER.error("Error de DAO[doTransfer]: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}
	}

	@Override
	public void doDebit(TrxFinancieroDTO trxFinancieroDTO, Intencion in) throws FinancieroException {
		SimpleTrxFinanciero trxFinanciero = modelMapperService.modelMapperHelper(trxFinancieroDTO,
				SimpleTrxFinanciero.class);

		try {
			// trxFinancieroDAO.insert(trxFinanciero);
			trxFinancieroDAO.doDebit(trxFinanciero, in);
		} catch (Exception e) {
			LOGGER.error("Error de DAO[doDebit]: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), e.getCause().toString());
		}
	}

	@Override
	public Pago pagoPei(TrxFinancieroDTO trxFinancieroDTO, Intencion in) throws FinancieroException {
		SimpleTrxFinanciero trxFinanciero = modelMapperService.modelMapperHelper(trxFinancieroDTO,
				SimpleTrxFinanciero.class);

		try {
			// trxFinancieroDAO.insert(trxFinanciero);
			return trxFinancieroDAO.doPagoPei(trxFinanciero, in);
		} catch (Exception e) {
			LOGGER.error("Error de DAO[pagoPei]: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), e.getCause().toString());
		}
		// return null;
	}

	@Override
	public Pago devolucionPagoPei(TrxFinancieroDTO trxFinancieroDTO, Intencion in, boolean isParcial)
			throws FinancieroException {
		SimpleTrxFinanciero trxFinanciero = modelMapperService.modelMapperHelper(trxFinancieroDTO,
				SimpleTrxFinanciero.class);

		try {
			// trxFinancieroDAO.insert(trxFinanciero);
			return trxFinancieroDAO.doDevolucionPagoPei(trxFinanciero, in, isParcial);
		} catch (Exception e) {
			LOGGER.error("Error de DAO[devolucionPagoPei]: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), e.getCause().toString());
		}
		// return null;
	}

	public void doWithdraw(TrxFinancieroDTO trxFinancieroDTO, Intencion in) throws FinancieroException {
		SimpleTrxFinanciero trxFinanciero = modelMapperService.modelMapperHelper(trxFinancieroDTO,
				SimpleTrxFinanciero.class);

		try {
			// trxFinancieroDAO.insert(trxFinanciero);
			trxFinancieroDAO.doWithdraw(trxFinanciero, in);
		} catch (Exception e) {
			LOGGER.error("Error de DAO[doWithdraw]: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), e.getCause().toString());
		}
	}

	@Override
	@Deprecated
	public void rollbackWithdraw(Long trxFinancieroId, String idRequerimientoBaja) throws FinancieroException {
		try {
			trxFinancieroDAO.updateEntry(trxFinancieroId, idRequerimientoBaja, "1");
		} catch (Exception e) {
			LOGGER.error("Error de DAO - rollbackWithdraw: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}
	}

	@Override
	public void registerConsultaPEITrx(TrxFinancieroDTO trxFinancieroDTO, Intencion in) throws FinancieroException {
		SimpleTrxFinanciero trxFinanciero = modelMapperService.modelMapperHelper(trxFinancieroDTO,
				SimpleTrxFinanciero.class);

		try {
			// trxFinancieroDAO.insert(trxFinanciero);
			trxFinancieroDAO.registerConsultaPEITrx(trxFinanciero, in);
		} catch (Exception e) {
			LOGGER.error("Error de DAO[registerConsultaPEITrx]: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), e.getCause().toString());
		}
	}

	@Override
	public Intencion registrarInicioTransaccion(TrxFinancieroDTO trxFinancieroDTO) throws FinancieroException {
		SimpleTrxFinanciero trxFinanciero = modelMapperService.modelMapperHelper(trxFinancieroDTO,
				SimpleTrxFinanciero.class);

		try {
			return trxFinancieroDAO.registrarInicioTransaccion(trxFinanciero);
		} catch (Exception e) {
			LOGGER.error("Error de DAO[registrarInicioTransaccion]: " + e.getMessage(), e);
			return null;
		}
	}

	@Override
	public Intencion registrarPruebaServicio(TrxFinancieroDTO trxFinancieroDTO) throws FinancieroException {
		SimpleTrxFinanciero trxFinanciero = modelMapperService.modelMapperHelper(trxFinancieroDTO,
				SimpleTrxFinanciero.class);

		try {
			return trxFinancieroDAO.registrarInicioTransaccion(trxFinanciero);
		} catch (Exception e) {
			LOGGER.error("Error de DAO[registrarPruebaServicio]: " + e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), e.getMessage());
		}
	}

	@Override
	public void updateIntencion(Intencion in) {
		try {
			trxFinancieroDAO.updateIntencion(in);
		} catch (Exception e) {
			LOGGER.error("Error de DAO[updateIntencion]: " + e.getMessage(), e);
		}
	}

}
