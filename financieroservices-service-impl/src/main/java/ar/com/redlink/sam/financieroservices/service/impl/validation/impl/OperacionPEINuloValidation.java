package ar.com.redlink.sam.financieroservices.service.impl.validation.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA166;

import org.apache.log4j.Logger;

import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.service.TrxFinancieroService;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;

public class OperacionPEINuloValidation extends AbstractFSValidation {

	private static final Logger LOGGER = Logger.getLogger(TerminalSamValidation.class);

	protected TrxFinancieroService trxFinancieroService;	

	@Override
	public ValidationDTO validate(ValidationDTO validation) throws FinancieroValidationException, FinancieroException {	
		boolean exitsteTrx = false;
		try {
			exitsteTrx = trxFinancieroService.checkIdRequerimientoOperacionPeiNulo(validation.getOutputValue(ValidationDTO.ID_REQ).toString());
		} catch (FinancieroException e) {
			LOGGER.error("Error al acceder el servicio de ComercioPEI: " + e.getMessage(), e);
			throw new FinancieroException(FINA166.getCode(), FINA166.getMsg());
		}
		
		if (exitsteTrx) {
			throw new FinancieroValidationException(FINA166.getCode(),FINA166.getMsg());
		}	

		nextValidation(validation);

		return validation;
	}

	/**
	 * @return the trxFinancieroService
	 */
	public TrxFinancieroService getTrxFinancieroService() {
		return trxFinancieroService;
	}

	/**
	 * @param trxFinancieroService the trxFinancieroService to set
	 */
	public void setTrxFinancieroService(TrxFinancieroService trxFinancieroService) {
		this.trxFinancieroService = trxFinancieroService;
	}

}