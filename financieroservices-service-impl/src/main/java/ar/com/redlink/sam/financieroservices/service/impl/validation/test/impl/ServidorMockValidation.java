/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA119;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA120;
import java.util.ArrayList;
import java.util.List;
import ar.com.redlink.framework.services.crud.RedLinkGenericService;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.EstadoRegistro;
import ar.com.redlink.sam.financieroservices.entity.Servidor;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;

/**
 * Valida el servidor.
 * 
 * @author aguerrea
 * 
 */
public class ServidorMockValidation extends AbstractFSValidation {

	//private static final Logger LOGGER = Logger.getLogger(ServidorMockValidation.class);

	private RedLinkGenericService<Servidor> servidorService;

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.validation.FSValidation#validate(ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO)
	 */
	@Override
	public ValidationDTO validate(ValidationDTO validation)	throws FinancieroException {
		Servidor servidor = null;

		//try {
			//LOGGER.info("[DAO INI] SucursalByIdSearchFilter");
			
			//List<Servidor> servidores = getServidorService().getAll(new SucursalByIdSearchFilter((Long) validation
			//				.getOutputValue(ValidationDTO.SUCURSAL_ID)),null).getResult();
			
			List<Servidor> servidores = new ArrayList();
			
			Servidor s= new Servidor();
			s.setServidorId(94513250001L);
			
			EstadoRegistro e= new EstadoRegistro();
			e.setEstadoRegistroId((short)0);
			e.setCodigo('0');
			s.setEstadoRegistro(e);
			
			
			servidores.add(s);
			
			
			//LOGGER.info("[DAO FIN] SucursalByIdSearchFilter");

			if (servidores.size() > 0) {
				servidor = servidores.get(0);
			}
		//} catch (RedLinkServiceException e) {
		//	LOGGER.error("Error al acceder el servicio de Servidor: "+ e.getMessage(), e);
		//	throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		//}

		if (null == servidor) {
			throw new FinancieroValidationException(FINA120.getCode(),	FINA120.getMsg());
		} else {
			validateEstadoRegistro(servidor, FINA119);
			validateDates(servidor, FINA119);
			validation.addOutputValue(ValidationDTO.SERVIDOR_ID,servidor.getServidorId());

		}
		nextValidation(validation);

		return validation;
	}

	/**
	 * @return the servidorService
	 */
	public RedLinkGenericService<Servidor> getServidorService() {
		return servidorService;
	}

	/**
	 * @param servidorService
	 *            the servidorService to set
	 */
	public void setServidorService(	RedLinkGenericService<Servidor> servidorService) {
		this.servidorService = servidorService;
	}

}
