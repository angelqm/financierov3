/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  29/03/2015 - 15:13:28
 */

package ar.com.redlink.sam.financieroservices.service.impl.validation.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA109;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA110;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA121;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA122;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA200;

import java.util.List;

import org.apache.log4j.Logger;

import ar.com.redlink.framework.services.crud.RedLinkGenericService;
import ar.com.redlink.framework.services.exception.RedLinkServiceException;
import ar.com.redlink.sam.financieroservices.dao.search.filter.TerminalSamIdSearchFilter;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.DispositivoPos;
import ar.com.redlink.sam.financieroservices.entity.EnrolamientoSam;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;

/**
 * Valida el dispositivo POS.
 * 
 * @author aguerrea
 * 
 */
public class DispositivoPosValidation extends AbstractFSValidation {

	private static final Logger LOGGER = Logger
			.getLogger(DispositivoPosValidation.class);

	private RedLinkGenericService<DispositivoPos> dispositivoPosService;
	private RedLinkGenericService<EnrolamientoSam> enrolamientoSamService;

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.validation.FSValidation#validate(ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO)
	 */
	@Override
	public ValidationDTO validate(ValidationDTO validation)	throws FinancieroException {
		DispositivoPos dispositivoPos = null;

		try {
			dispositivoPos = getDispositivoPosService().getById((Long) validation.getOutputValue(ValidationDTO.DISPOSITIVO_POS_ID));
		} catch (RedLinkServiceException e) {
			LOGGER.error("Error al acceder el servicio de DispositivoPOS: "	+ e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}

		if (!dispositivoPos.getIdentificadorDisp().equals(validation.getInputValue(ValidationDTO.IDENT_DISPOSITIVO))) {
			throw new FinancieroValidationException(FINA122.getCode(),	FINA122.getMsg());
		}

		validateEstadoRegistro(dispositivoPos, FINA121);
		validateDates(dispositivoPos, FINA121);
		validateEstadoRegistro(dispositivoPos.getTipoEncripcionB24(), FINA121);
		validation.addOutputValue(ValidationDTO.TIPO_ENCRIPCION_B24,dispositivoPos.getTipoEncripcionB24().getTipoEncripcionB24Id());

		// Parte 2 de la validacion.
		EnrolamientoSam enrolamientoSam = null;
		try {
			//LOGGER.info("[DAO INI] TerminalSamIdSearchFilter");
			
			List<EnrolamientoSam> enrolamientosSam = getEnrolamientoSamService().getAll(new TerminalSamIdSearchFilter((Long) validation.getOutputValue(ValidationDTO.TERMINAL_SAM_ID)),null).getResult();
			
			//LOGGER.info("[DAO FIN] TerminalSamIdSearchFilter");

			if (enrolamientosSam.size() > 0) {
				enrolamientoSam = enrolamientosSam.get(0);
			}
		} catch (RedLinkServiceException e) {
			LOGGER.error("Error al acceder el servicio de enrolamiento: "+ e.getMessage(), e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}

		if (null == enrolamientoSam) {
			throw new FinancieroValidationException(FINA109.getCode(),	FINA109.getMsg());
		}

		if (!enrolamientoSam.getIdentificadorTerm().equals(	validation.getInputValue(ValidationDTO.IDENT_TERMINAL))) {
			// No coincide el identificador de terminal.
			throw new FinancieroValidationException(FINA110.getCode(),	FINA110.getMsg());
		}

		nextValidation(validation);

		return validation;
	}

	/**
	 * @return the dispositivoPosService
	 */
	public RedLinkGenericService<DispositivoPos> getDispositivoPosService() {
		return dispositivoPosService;
	}

	/**
	 * @param dispositivoPosService
	 *            the dispositivoPosService to set
	 */
	public void setDispositivoPosService(
			RedLinkGenericService<DispositivoPos> dispositivoPosService) {
		this.dispositivoPosService = dispositivoPosService;
	}

	/**
	 * @return the enrolamientoSamService
	 */
	public RedLinkGenericService<EnrolamientoSam> getEnrolamientoSamService() {
		return enrolamientoSamService;
	}

	/**
	 * @param enrolamientoSamService
	 *            the enrolamientoSamService to set
	 */
	public void setEnrolamientoSamService(
			RedLinkGenericService<EnrolamientoSam> enrolamientoSamService) {
		this.enrolamientoSamService = enrolamientoSamService;
	}

}
