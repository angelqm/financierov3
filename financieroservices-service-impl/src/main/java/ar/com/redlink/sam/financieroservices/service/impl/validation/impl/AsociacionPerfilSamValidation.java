/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  01/04/2015 - 15:18:49
 */

package ar.com.redlink.sam.financieroservices.service.impl.validation.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA103;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA127;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA146;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA170;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA171;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA172;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA173;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA200;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ar.com.redlink.framework.services.crud.RedLinkGenericService;
import ar.com.redlink.framework.services.exception.RedLinkServiceException;
import ar.com.redlink.sam.financieroservices.dao.search.filter.SucAsociacionPerfilSamBySucIdSearchFilter;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.AsociacionPerfil;
import ar.com.redlink.sam.financieroservices.entity.AsociacionPerfilDetalle;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;

/**
 * Encapsula la validacion de SucursalPerfilSam y obtiene el Perfil Financiero
 * correspondiente.
 * 
 * @author aguerrea
 * 
 */
public class AsociacionPerfilSamValidation extends AbstractFSValidation {

	private static final Logger LOGGER = Logger
			.getLogger(AsociacionPerfilSamValidation.class);

 
	private RedLinkGenericService<AsociacionPerfil> asociacionPerfilService;
	

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.validation.FSValidation#validate(ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO)
	 */
	@Override
	public ValidationDTO validate(ValidationDTO validation)
			throws FinancieroException {
				
		if ("4".equals(validation.getInputValue(ValidationDTO.PRODUCTO_ID))) {
		
			nullCheck(validation, ValidationDTO.SUCURSAL_ID, FINA103);
			
			/*List<SucursalPerfilSam> sucursalesPerfilSam = null;*/
			List<AsociacionPerfil> asociacionesPerfil = null;
	
			try {
				asociacionesPerfil = asociacionPerfilService.getAll(
						new SucAsociacionPerfilSamBySucIdSearchFilter(Long.valueOf(validation						
								.getInputValue(ValidationDTO.SUCURSAL_ID))), null).getResult();
			} catch (RedLinkServiceException e) {
				LOGGER.error("Error al acceder el servicio de AsociacionPerfilService: "+ e.getMessage(), e);
				throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
			}
	
			
			AsociacionPerfil asociacionPerfil = null;
			
			if (asociacionesPerfil.size() == 0) {
				throw new FinancieroValidationException(FINA170.getCode(),	FINA170.getMsg());
			} else {
				List<Integer> idxValidos = new ArrayList<Integer>();
	
				for (int i = 0; i < asociacionesPerfil.size(); i++) {
					asociacionPerfil = asociacionesPerfil.get(i);
	
					try {
						validateDates(asociacionPerfil, FINA171);
					} catch (FinancieroValidationException e) {
						throw new FinancieroValidationException(FINA171.getCode(),FINA171.getMsg());
					}
					idxValidos.add(i);
				}
				
				AsociacionPerfilDetalle asociacionPerfilDetalle = null;

				if (asociacionPerfil.getAsociacionPerfilDetalle() != null && asociacionPerfil.getAsociacionPerfilDetalle().size() > 0) {

						
						asociacionPerfilDetalle =
								(AsociacionPerfilDetalle)asociacionPerfil.getAsociacionPerfilDetalle().iterator().next();

						try {
							validateEstadoRegistro(asociacionPerfilDetalle, FINA172); 
							validateDates(asociacionPerfilDetalle, FINA172); 
						} catch (FinancieroValidationException e) {
							throw new FinancieroValidationException(FINA172.getCode(),	FINA172.getMsg());
						}
			
				} else {
					throw new FinancieroValidationException(FINA173.getCode(),FINA173.getMsg()); 

				}
				
				int size = idxValidos.size();
				if (size == 1) {
					validation.addOutputValue(ValidationDTO.PERFIL_SAM_ID,
							asociacionPerfilDetalle.getPerfilSam().getPerfilSamId());
				} else {
					if (size == 0) {
						throw new FinancieroValidationException(FINA127.getCode(), FINA127.getMsg());
					} else {
						throw new FinancieroValidationException(FINA146.getCode(), FINA146.getMsg());
					}
				}
	
			}
		}
		
		nextValidation(validation); // PerfilSam.

		return validation;
	}


	public RedLinkGenericService<AsociacionPerfil> getAsociacionPerfilService() {
		return asociacionPerfilService;
	}


	public void setAsociacionPerfilService(RedLinkGenericService<AsociacionPerfil> asociacionPerfilService) {
		this.asociacionPerfilService = asociacionPerfilService;
	}


}
