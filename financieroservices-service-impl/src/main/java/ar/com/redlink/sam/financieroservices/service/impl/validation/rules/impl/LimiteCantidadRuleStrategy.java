/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  12/04/2015 - 16:18:09
 */

package ar.com.redlink.sam.financieroservices.service.impl.validation.rules.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA306;

import java.util.Date;

import org.apache.log4j.Logger;

import ar.com.redlink.sam.financieroservices.service.TrxFinancieroService;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.rules.LimiteRuleStrategy;

/**
 * Strategy para calculo de limite de regla por cantidad de registros del campo
 * indicado.
 * 
 * @author aguerrea
 * 
 */
public class LimiteCantidadRuleStrategy implements LimiteRuleStrategy {

	private static final Logger LOGGER = Logger.getLogger(LimiteCantidadRuleStrategy.class);

	private TrxFinancieroService trxFinancieroService;
	private String campo;
	private String id;
	private Double valor;
	private Double cantidad;

	private Date dateFrom;
	private Date dateTo;

	private String tokenizado;
	private Long prefijoOpId;

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.service.impl.validation.rules.LimiteRuleStrategy#validate(java.lang.String)
	 */
	@Override
	public void validate() throws FinancieroException {
		Integer all = null;

		try {
			all = trxFinancieroService.getCount(campo + "_id", id, dateFrom,dateTo, tokenizado, prefijoOpId);
			all += cantidad.intValue();
		} catch (FinancieroException e) {
			LOGGER.error("Error al acceder getCount-ACUMULADO_TOTAL: "	+ e.getMessage(), e);
			throw e;
		}

		if (all != null && all > valor) {
			throw new FinancieroValidationException(FINA306.getCode(),	String.format(FINA306.getMsg(), campo, valor, 0D));
		}
	}

	/**
	 * @param campo
	 *            the campo to set
	 */
	public void setCampo(String campo) {
		this.campo = campo;
	}

	/**
	 * @return the trxFinancieroService
	 */
	public TrxFinancieroService getTrxFinancieroService() {
		return trxFinancieroService;
	}

	/**
	 * @param trxFinancieroService
	 *            the trxFinancieroService to set
	 */
	public void setTrxFinancieroService(TrxFinancieroService trxFinancieroService) {
		this.trxFinancieroService = trxFinancieroService;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.impl.validation.rules.LimiteRuleStrategy#setValor(java.lang.Double)
	 */
	@Override
	public void setValor(Double valor) {
		this.valor = valor;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.impl.validation.rules.LimiteRuleStrategy#setCantidad(java.lang.Double)
	 */
	@Override
	public void setCantidad(Double cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.impl.validation.rules.LimiteRuleStrategy#setDateFrom(java.util.Date)
	 */
	@Override
	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.impl.validation.rules.LimiteRuleStrategy#setDateTo(java.util.Date)
	 */
	@Override
	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.impl.validation.rules.LimiteRuleStrategy#setTokenizado(java.lang.String)
	 */
	@Override
	public void setTokenizado(String tokenizado) {
		this.tokenizado = tokenizado;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.impl.validation.rules.LimiteRuleStrategy#setCodigoOp(java.lang.Long)
	 */
	@Override
	public void setCodigoOp(Long prefijoOpId) {
		this.prefijoOpId = prefijoOpId;

	}

}
