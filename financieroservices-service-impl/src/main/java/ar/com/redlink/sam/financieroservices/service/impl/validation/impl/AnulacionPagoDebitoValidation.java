package ar.com.redlink.sam.financieroservices.service.impl.validation.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA138;


import org.apache.commons.lang.StringUtils;

import ar.com.redlink.sam.financieroservices.dto.TrxFinancieroValidationDTO;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.Pago;
import ar.com.redlink.sam.financieroservices.entity.util.DateUtil;
import ar.com.redlink.sam.financieroservices.service.TrxFinancieroService;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;

public class AnulacionPagoDebitoValidation extends AbstractFSValidation  {
	private TrxFinancieroService trxFinancieroService;

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.validation.FSValidation#validate(ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO)
	 */
	@Override
	public ValidationDTO validate(ValidationDTO validation)	throws FinancieroValidationException, FinancieroException {
		
		String terminalSamId = validation.getInputValue(ValidationDTO.TERMINAL_SAM_ID);
		String prefijoOpId = validation	.getInputValue(ValidationDTO.PREFIJO_OP_ID);
		String idReqOrig = validation.getInputValue(ValidationDTO.ID_REQ_ORIG);
		String importe = validation.getInputValue(ValidationDTO.IMPORTE);
		String tokenizado = validation.getInputValue(ValidationDTO.TOKENIZADO);

		
		Pago trxFinanciero = trxFinancieroService.validateCancel(new TrxFinancieroValidationDTO(importe,terminalSamId, idReqOrig, prefijoOpId, tokenizado));
		
		//TrxFinanciero trxFinanciero = trxFinancieroService.validateTrx(new TrxFinancieroValidationDTO(importe,terminalSamId, idReqOrig, prefijoOpId, tokenizado));
		
		if (null == trxFinanciero) {
			throw new FinancieroValidationException(FINA138.getCode(),	FINA138.getMsg());
		}

		String seqNumAnulacion = StringUtils.stripStart(validation.getInputValue(ValidationDTO.SEQ_NUM), "0");

		if (!trxFinanciero.getSecuenciaOnB24().toString().equals(seqNumAnulacion)) {
			throw new FinancieroValidationException(FINA138.getCode(),	FINA138.getMsg());
		}
		/*AQM CARGA LA FECHA/HORA ON DEL PAGO*/
		if (trxFinanciero.getFechaOn() != null)
		    validation.addOutputValue(ValidationDTO.FECHA_ON, DateUtil.fechaOnExtFormat(trxFinanciero.getFechaOn()));

		if (trxFinanciero.getHoraOn() != null)
			validation.addOutputValue(ValidationDTO.HORA_ON, trxFinanciero.getHoraOn());
		
		//validation.addOutputValue(ValidationDTO.TRXT_FINANCIERO_ID,	trxFinanciero.getTrxFinancieroId());
		validation.addOutputValue(ValidationDTO.TRXT_FINANCIERO_ID,	trxFinanciero.getIntencionId().getIntencionId());
		
		nextValidation(validation);

		return validation;
	}

	/**
	 * @return the trxFinancieroService
	 */
	public TrxFinancieroService getTrxFinancieroService() {
		return trxFinancieroService;
	}

	/**
	 * @param trxFinancieroService
	 *            the trxFinancieroService to set
	 */
	public void setTrxFinancieroService(
			TrxFinancieroService trxFinancieroService) {
		this.trxFinancieroService = trxFinancieroService;
	}

}
