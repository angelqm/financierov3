/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  29/03/2015 - 14:24:29
 */

package ar.com.redlink.sam.financieroservices.service.impl.validation;

import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.validation.FSValidation;
import ar.com.redlink.sam.financieroservices.service.validation.ValidationChain;

/**
 * Implementacion de la cadena de validacion.
 * 
 * @author aguerrea
 * 
 */
public class ValidationChainImpl implements ValidationChain {

	private FSValidation initialValidation;

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.validation.ValidationChain#validate(ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO)
	 */
	@Override
	public ValidationDTO validate(ValidationDTO validation)	throws FinancieroException {
		getInitialValidation().validate(validation);

		return validation;
	}

	/**
	 * @return the initialValidation
	 */
	public FSValidation getInitialValidation() {
		return initialValidation;
	}

	/**
	 * @param initialValidation
	 *            the initialValidation to set
	 */
	public void setInitialValidation(FSValidation initialValidation) {
		this.initialValidation = initialValidation;
	}

}
