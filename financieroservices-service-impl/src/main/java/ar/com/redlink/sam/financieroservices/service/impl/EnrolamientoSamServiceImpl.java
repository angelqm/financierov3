/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  12/04/2015 - 00:12:48
 */

package ar.com.redlink.sam.financieroservices.service.impl;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA109;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA110;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA176;

import java.sql.SQLException;
import org.apache.log4j.Logger;

import ar.com.redlink.framework.services.crud.impl.RedLinkGenericServiceImpl;
import ar.com.redlink.sam.financieroservices.dao.EnrolamientoSamDAO;
import ar.com.redlink.sam.financieroservices.dto.request.EnrolamientoRequestDTO;
import ar.com.redlink.sam.financieroservices.entity.EnrolamientoSam;
import ar.com.redlink.sam.financieroservices.service.EnrolamientoSamService;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;

/**
 * Implementacion del servicio extendido sobre el CRUD generico para el manejo
 * de la entidad {@link EnrolamientoSam}.
 * 
 * @author aguerrea
 * 
 */
public class EnrolamientoSamServiceImpl extends
		RedLinkGenericServiceImpl<EnrolamientoSam> implements
		EnrolamientoSamService {

	private static final Logger LOGGER = Logger.getLogger(EnrolamientoSamServiceImpl.class);
	private EnrolamientoSamDAO enrolamientoSamDAO;

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.service.EnrolamientoSamService#enrollDevice(ar.com.redlink.sam.financieroservices.dto.request.EnrolamientoRequestDTO)
	 */
	@Override
	public void enrollDevice(EnrolamientoRequestDTO enrolamientoRequestDTO)	throws FinancieroException {
		try {
			getEnrolamientoSamDAO().enrollDevice(enrolamientoRequestDTO.getTerminalSamId(), enrolamientoRequestDTO.getIdentTerminal());
		} catch (Exception e) {
			LOGGER.error("Error al enrolar el dispositivo: " + e.getMessage(), e);
			
			if(e.getCause() != null && e.getCause() instanceof SQLException){
				 SQLException se = (SQLException)e.getCause();
				 
				 if(se.getErrorCode()==20006){ /*EXISTE TERMINAL/ NO IDENTIFICADOR */
					 throw new FinancieroException(FINA110.getCode(), FINA110.getMsg());
				 }
				 else if(se.getErrorCode()==20005 ){ /*EXISTE TERMINAL/IDENTIFICADOR */
					 throw new FinancieroException(FINA176.getCode(), FINA176.getMsg());
				 }
				 else{ /*NO EXISTE TERMINAL/IDENTIFICADOR*/
					 throw new FinancieroException(FINA109.getCode(), FINA109.getMsg());
				 }
			 }
			 else
				 throw new FinancieroException(FINA109.getCode(), FINA109.getMsg());
			
		}
	}

	/**
	 * @return the enrolamientoSamDAO
	 */
	public EnrolamientoSamDAO getEnrolamientoSamDAO() {
		return enrolamientoSamDAO;
	}

	/**
	 * @param enrolamientoSamDAO
	 *            the enrolamientoSamDAO to set
	 */
	public void setEnrolamientoSamDAO(EnrolamientoSamDAO enrolamientoSamDAO) {
		this.enrolamientoSamDAO = enrolamientoSamDAO;
	}

}
