/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  13/04/2015 - 23:03:14
 */

package ar.com.redlink.sam.financieroservices.service.impl.validation.impl;

import java.util.List;
import java.util.Map;

import ar.com.redlink.sam.financieroservices.dto.OperacionDTO;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.rules.LimiteRuleStrategy;
import ar.com.redlink.sam.financieroservices.service.impl.validation.rules.RulesComposer;

/**
 * Validacion para las operaciones y sus limites correspondientes.
 * 
 * @author aguerrea
 * 
 */
public class OperacionLimitesValidation extends AbstractFSValidation {

	private RulesComposer rulesComposer;
	private ValidationDTO validation;
	private Map<String, String> exceptionMap;

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.validation.FSValidation#validate(ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ValidationDTO validate(ValidationDTO validation) throws FinancieroException {
		this.validation = validation;
		for (OperacionDTO operacionDTO : (List<OperacionDTO>) validation.getOutputValue(ValidationDTO.OPERACION_DTOS)) {

			// Checkeo si la trx es la que viene y ahi valido las reglas.
			if (null != validation.getInputValue(ValidationDTO.PREFIJO_OP_ID)
					&& validation.getInputValue(ValidationDTO.PREFIJO_OP_ID).equals(operacionDTO.getIdOperacion())) {
				
				checkOperacionDTO(operacionDTO.isRequierePin(),	ValidationDTO.REQUIERE_PIN); // 1. PIN.
				checkOperacionDTO(operacionDTO.isRequiereAut(),	ValidationDTO.REQUIERE_AUT); // 2. Requiere AUT.
				checkOperacionDTO(operacionDTO.isRequiereSeg(),	ValidationDTO.REQUIERE_SEG); // 3. CCV
				checkOperacionDTO(operacionDTO.isRequiereSfx(),	ValidationDTO.REQUIERE_SFX); // 4. SFX - Sufijo.
				checkOperacionDTO(operacionDTO.isRequiereDNI(),	ValidationDTO.REQUIERE_DNI); // 5. DNI.

				if (null == validation.getInputValue(ValidationDTO.REVERSA)	|| "false".equals(validation.getInputValue(ValidationDTO.REVERSA))) {
					List<LimiteRuleStrategy> limiteRules = rulesComposer.compose(operacionDTO.getLimites(), validation);

					for (LimiteRuleStrategy limiteRule : limiteRules) {
						limiteRule.validate();
					}
				}
			}
		}

		nextValidation(validation);

		return validation;
	}

	/**
	 * Toma el booleano del tipo de requerimiento y extrae del mapa de
	 * validaciones el valor correspondiente.
	 * 
	 * @param requiere
	 * @param validationReference
	 * @throws FinancieroValidationException
	 */
	private void checkOperacionDTO(boolean requiere, String validationReference)
			throws FinancieroValidationException {

		if (requiere) {
			String value = validation.getInputValue(validationReference);

			if (null == value || value.isEmpty() || "0".equals(value)) {
				FinancieroServicesMsgEnum msgEnum = FinancieroServicesMsgEnum.valueOf(exceptionMap.get(validationReference));
				throw new FinancieroValidationException(msgEnum.getCode(),msgEnum.getMsg());
			}
		}
	}

	/**
	 * @return the rulesComposer
	 */
	public RulesComposer getRulesComposer() {
		return rulesComposer;
	}

	/**
	 * @param rulesComposer
	 *            the rulesComposer to set
	 */
	public void setRulesComposer(RulesComposer rulesComposer) {
		this.rulesComposer = rulesComposer;
	}

	/**
	 * @return the exceptionMap
	 */
	public Map<String, String> getExceptionMap() {
		return exceptionMap;
	}

	/**
	 * @param exceptionMap
	 *            the exceptionMap to set
	 */
	public void setExceptionMap(Map<String, String> exceptionMap) {
		this.exceptionMap = exceptionMap;
	}

}
