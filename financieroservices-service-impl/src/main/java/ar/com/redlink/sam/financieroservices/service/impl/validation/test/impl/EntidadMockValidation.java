/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA100;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA101;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA102;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import ar.com.redlink.framework.services.crud.RedLinkGenericService;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.Entidad;
import ar.com.redlink.sam.financieroservices.entity.EstadoRegistro;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;

/**
 * Valida la entidad recibida.
 * 
 * @author aguerrea
 * 
 */
public class EntidadMockValidation extends AbstractFSValidation {

	//private static final Logger LOGGER = Logger.getLogger(EntidadMockValidation.class);

	private RedLinkGenericService<Entidad> entidadService;

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.validation.FSValidation#validate(ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO)
	 */
	@Override
	public ValidationDTO validate(ValidationDTO validation)	throws FinancieroException {
		nullCheck(validation, ValidationDTO.ENTIDAD_COD, FINA100);

		Entidad entidad = null;

		//try {
			//List<Entidad> entidades = getEntidadService().getAll(new EntidadByCodSearchFilter(validation.getInputValue(ValidationDTO.ENTIDAD_COD)),	null).getResult();
			
			EstadoRegistro e= new EstadoRegistro();
			e.setEstadoRegistroId((short)0);
			e.setCodigo('0');
			
			List<Entidad> entidades = new ArrayList();
			Entidad p= new Entidad();
			p.setCodEntidad("0945");
			p.setCodEntidadB24("0945");
			p.setCuit("30643990632");
			p.setDescripcion("GIRE SA");
			p.setEntidadId(945);
			p.setEstadoRegistro(e);
			p.setFechaAlta(new Date());
			p.setFechaBaja(null);
			p.setRepositorio("0945");
			
			entidades.add(p);
			
			//LOGGER.info("[DAO FIN] EntidadByCodSearchFilter");

			if (entidades.size() > 0) {
				entidad = entidades.get(0);
			}
		//} catch (RedLinkServiceException e) {
		//	throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		//}

		if (null == entidad) {
			throw new FinancieroValidationException(FINA102.getCode(),FINA102.getMsg());
		} else {
			//this.validateEstadoRegistro(entidad, FINA101);
			//this.validateDates(entidad, FINA101);
			validation.addOutputValue(ValidationDTO.ENTIDAD_ID,	entidad.getEntidadId());
		}

		nextValidation(validation);

		return validation;
	}

	/**
	 * @return the entidadService
	 */
	public RedLinkGenericService<Entidad> getEntidadService() {
		return entidadService;
	}

	/**
	 * @param entidadService
	 *            the entidadService to set
	 */
	public void setEntidadService(RedLinkGenericService<Entidad> entidadService) {
		this.entidadService = entidadService;
	}

}
