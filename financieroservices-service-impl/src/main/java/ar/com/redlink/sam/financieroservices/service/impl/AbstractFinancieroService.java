/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  10/04/2015 - 22:08:58
 */

package ar.com.redlink.sam.financieroservices.service.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA145;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA200;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA203;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import ar.com.link.connector.tandem.messages.TandemResponse;
import ar.com.redlink.framework.entities.exception.RedLinkFrameworkRuntimeException;
import ar.com.redlink.framework.token.core.service.TokenizacionPANService;
import ar.com.redlink.sam.financieroservices.dto.CuentaDTO;
import ar.com.redlink.sam.financieroservices.dto.TrxFinancieroDTO;

import ar.com.redlink.sam.financieroservices.dto.request.common.AnulacionCommonDTO;
import ar.com.redlink.sam.financieroservices.dto.request.common.FinancieroAccountDTO;
import ar.com.redlink.sam.financieroservices.dto.request.common.FinancieroCommonDTO;
import ar.com.redlink.sam.financieroservices.dto.request.common.FinancieroServiceBaseRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.response.FinancieroServiceBaseResponseDTO;
import ar.com.redlink.sam.financieroservices.dto.response.common.OperacionCommonDTO;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.Intencion;
import ar.com.redlink.sam.financieroservices.entity.util.DateUtil;
import ar.com.redlink.sam.financieroservices.service.FinancieroService;
import ar.com.redlink.sam.financieroservices.service.ModelMapperService;
import ar.com.redlink.sam.financieroservices.service.PrefijoOpService;
import ar.com.redlink.sam.financieroservices.service.TrxFinancieroService;

import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.impl.DevolucionPEIValidation;
import ar.com.redlink.sam.financieroservices.service.validation.ValidationChain;
import ar.com.redlink.sam.financieroservices.tandem.response.ConsultaExtraccionResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.base.SAMTandemResponse;

/**
 * Clase abstracta que define metodos base para su utilizacion en la
 * implementacion concreta.
 * 
 * @author aguerrea
 * 
 */
public abstract class AbstractFinancieroService implements FinancieroService {

	private static final Logger LOGGER = Logger.getLogger(AbstractFinancieroService.class);

	protected ValidationChain financieroValidationChain;
	protected TrxFinancieroService trxFinancieroService;
	protected ModelMapperService modelMapperService;
	protected PrefijoOpService prefijoOpService;
	protected AbstractFSValidation anulacionValidation;
	protected DevolucionPEIValidation devolucionPEIValidation;
	protected AbstractFSValidation comercioPEIValidation;
	protected AbstractFSValidation terminalPEIValidation;
	protected AbstractFSValidation anulacionPagoDebitoValidation;
	protected TokenizacionPANService tokenServiceConectado;

	/**
	 * Inserta un registro en TRX_FINANCIERO con la informacion del DTO.
	 */
	@Deprecated
	public void registerTrx(FinancieroServiceBaseRequestDTO trxData) throws FinancieroException {
		TrxFinancieroDTO trxFinancieroDTO = modelMapperService.modelMapperHelper(trxData, TrxFinancieroDTO.class);

		if (null == trxFinancieroDTO.getImporte()) {
			trxFinancieroDTO.setImporte("0");
		}

		if (null == trxFinancieroDTO.getEstadoTrxId()) {
			trxFinancieroDTO.setEstadoTrxId("0");
		}

		trxFinancieroDTO.setSecuenciaOnWS(trxData.getSecuenciaOn());
		trxFinancieroDTO.setSecuenciaOnB24(trxData.getSecuenciaOnRta());

		trxFinancieroDTO.setSucursal(StringUtils.leftPad(trxFinancieroDTO.getSucursal(), 8, "0"));

		trxFinancieroService.insert(trxFinancieroDTO);
	}
	
	public void doTransferOperation(FinancieroServiceBaseRequestDTO trxData, Intencion in) throws FinancieroException {
		TrxFinancieroDTO trxFinancieroDTO = modelMapperService.modelMapperHelper(trxData, TrxFinancieroDTO.class);

		if (null == trxFinancieroDTO.getImporte()) {
			trxFinancieroDTO.setImporte("0");
		}

		if (null == trxFinancieroDTO.getEstadoTrxId()) {
			trxFinancieroDTO.setEstadoTrxId("0");
		}

		trxFinancieroDTO.setSecuenciaOnWS(trxData.getSecuenciaOn());
		trxFinancieroDTO.setSecuenciaOnB24(trxData.getSecuenciaOnRta());

		trxFinancieroDTO.setSucursal(StringUtils.leftPad(trxFinancieroDTO.getSucursal(), 8, "0"));

		//trxFinancieroService.insert(trxFinancieroDTO);
		trxFinancieroService.doTransfer(trxFinancieroDTO, in);
		
	}
	
	
	public void debit(FinancieroServiceBaseRequestDTO trxData, Intencion in) throws FinancieroException {
		TrxFinancieroDTO trxFinancieroDTO = modelMapperService.modelMapperHelper(trxData, TrxFinancieroDTO.class);

		if (null == trxFinancieroDTO.getImporte()) {
			trxFinancieroDTO.setImporte("0");
		}

		if (null == trxFinancieroDTO.getEstadoTrxId()) {
			trxFinancieroDTO.setEstadoTrxId("0");
		}

		trxFinancieroDTO.setSecuenciaOnWS(trxData.getSecuenciaOn());
		trxFinancieroDTO.setSecuenciaOnB24(trxData.getSecuenciaOnRta());

		trxFinancieroDTO.setSucursal(StringUtils.leftPad(trxFinancieroDTO.getSucursal(), 8, "0"));
		
		//trxFinancieroService.insert(trxFinancieroDTO);
		trxFinancieroService.doDebit(trxFinancieroDTO, in);
	}
	
	
	public void doWithDraw(FinancieroServiceBaseRequestDTO trxData, Intencion in) throws FinancieroException {
		TrxFinancieroDTO trxFinancieroDTO = modelMapperService.modelMapperHelper(trxData, TrxFinancieroDTO.class);

		if (null == trxFinancieroDTO.getEstadoTrxId()) {
			trxFinancieroDTO.setEstadoTrxId("0");
		}

		trxFinancieroDTO.setSecuenciaOnWS(trxData.getSecuenciaOn());
		trxFinancieroDTO.setSecuenciaOnB24(trxData.getSecuenciaOnRta());
		trxFinancieroDTO.setSucursal(StringUtils.leftPad(trxFinancieroDTO.getSucursal(), 8, "0"));

		//trxFinancieroService.insert(trxFinancieroDTO);
		trxFinancieroService.doWithdraw(trxFinancieroDTO, in);
	}
	
	
	public void doDebitPayment(FinancieroServiceBaseRequestDTO trxData, Intencion in) throws FinancieroException {
		TrxFinancieroDTO trxFinancieroDTO = modelMapperService.modelMapperHelper(trxData, TrxFinancieroDTO.class);

		if (null == trxFinancieroDTO.getImporte()) {
			trxFinancieroDTO.setImporte("0");
		}

		if (null == trxFinancieroDTO.getEstadoTrxId()) {
			trxFinancieroDTO.setEstadoTrxId("0");
		}

		trxFinancieroDTO.setSecuenciaOnWS(trxData.getSecuenciaOn());
		trxFinancieroDTO.setSecuenciaOnB24(trxData.getSecuenciaOnRta());
		trxFinancieroDTO.setSucursal(StringUtils.leftPad(trxFinancieroDTO.getSucursal(), 8, "0"));

		//trxFinancieroService.insert(trxFinancieroDTO);
		trxFinancieroService.doDebitPayment(trxFinancieroDTO, in);
	}
	
	public void doDeposito(FinancieroServiceBaseRequestDTO trxData, Intencion in) throws FinancieroException {
		TrxFinancieroDTO trxFinancieroDTO = modelMapperService.modelMapperHelper(trxData, TrxFinancieroDTO.class);

		/*if (null == trxFinancieroDTO.getImporte()) {
			trxFinancieroDTO.setImporte("0");
		}*/

		if (null == trxFinancieroDTO.getEstadoTrxId()) {
			trxFinancieroDTO.setEstadoTrxId("0");
		}

		trxFinancieroDTO.setSecuenciaOnWS(trxData.getSecuenciaOn());
		trxFinancieroDTO.setSecuenciaOnB24(trxData.getSecuenciaOnRta());
		trxFinancieroDTO.setSucursal(StringUtils.leftPad(trxFinancieroDTO.getSucursal(), 8, "0"));

		//trxFinancieroService.insert(trxFinancieroDTO);
		trxFinancieroService.doDeposit(trxFinancieroDTO, in);
	}
	
	
	
	/**
	 * Registra la consulta de saldo.
	 * 
	 * @param idRequerimientoOrig
	 * @param idRequerimiento
	 * @throws FinancieroException
	 */
	
	public void getSaldo(FinancieroServiceBaseRequestDTO trxData, Intencion in) throws FinancieroException {
		TrxFinancieroDTO trxFinancieroDTO = modelMapperService.modelMapperHelper(trxData, TrxFinancieroDTO.class);

		/*if (null == trxFinancieroDTO.getImporte()) {
			trxFinancieroDTO.setImporte("0");
		}*/

		if (null == trxFinancieroDTO.getEstadoTrxId()) {
			trxFinancieroDTO.setEstadoTrxId("0");
		}

		trxFinancieroDTO.setSecuenciaOnWS(trxData.getSecuenciaOn());
		trxFinancieroDTO.setSecuenciaOnB24(trxData.getSecuenciaOnRta());
		trxFinancieroDTO.setSucursal(StringUtils.leftPad(trxFinancieroDTO.getSucursal(), 8, "0"));

		//trxFinancieroService.insert(trxFinancieroDTO);
		trxFinancieroService.getBalance(trxFinancieroDTO, in);
	}
	
	
	/**
	 * Registra validacacion de la tarjeta.
	 * 
	 * @param idRequerimientoOrig
	 * @param idRequerimiento
	 * @throws FinancieroException
	 */
	public void validarTarjeta(FinancieroServiceBaseRequestDTO trxData, Intencion in) throws FinancieroException {
		TrxFinancieroDTO trxFinancieroDTO = modelMapperService.modelMapperHelper(trxData, TrxFinancieroDTO.class);

		if (null == trxFinancieroDTO.getImporte()) {
			trxFinancieroDTO.setImporte("0");
		}

		/*if (null == trxFinancieroDTO.getEstadoTrxId()) {
			trxFinancieroDTO.setEstadoTrxId("0");
		}*/

		trxFinancieroDTO.setSecuenciaOnWS(trxData.getSecuenciaOn());
		trxFinancieroDTO.setSecuenciaOnB24(trxData.getSecuenciaOnRta());
		trxFinancieroDTO.setSucursal(StringUtils.leftPad(trxFinancieroDTO.getSucursal(), 8, "0"));

		//trxFinancieroService.insert(trxFinancieroDTO);
		trxFinancieroService.validarTarjeta(trxFinancieroDTO, in);
	}
	

	/**
	 * Registra la reversa en TRX_FINANCIERO.
	 * 
	 * @param idRequerimientoOrig
	 * @param idRequerimiento
	 * @throws FinancieroException
	 */
	public void registerReverse(Long trxFinancieroId,String idRequerimiento) throws FinancieroException {
		this.getTrxFinancieroService().rollbackTrx(trxFinancieroId,	idRequerimiento);
	}
	
	public Intencion inicioTransaccion(FinancieroServiceBaseRequestDTO trxData) {
		
		TrxFinancieroDTO trxFinancieroDTO = modelMapperService.modelMapperHelper(trxData, TrxFinancieroDTO.class);
		if (null == trxFinancieroDTO.getImporte()) {
			trxFinancieroDTO.setImporte("0");
		}
		trxFinancieroDTO.setSecuenciaOnWS(trxData.getSecuenciaOn());
		trxFinancieroDTO.setSecuenciaOnB24(trxData.getSecuenciaOnRta());
		trxFinancieroDTO.setSucursal(StringUtils.leftPad(trxFinancieroDTO.getSucursal(), 8, "0"));
		try {
			return trxFinancieroService.registrarInicioTransaccion(trxFinancieroDTO);
		} catch (FinancieroException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public Intencion inicioTransaccionPei(Long terminalSamId, int prefijo_op, String idRequerimiento) {
	
		TrxFinancieroDTO trxFinancieroDTO = new TrxFinancieroDTO();
		trxFinancieroDTO.setTerminalSamId(terminalSamId.toString());
		trxFinancieroDTO.setPrefijoOpId(String.valueOf(prefijo_op));
		trxFinancieroDTO.setIdRequerimiento(idRequerimiento);
		
		try {
			return trxFinancieroService.registrarInicioTransaccion(trxFinancieroDTO);
		} catch (FinancieroException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Intencion pruebaServicio(Long terminalSamId, int prefijo_op, String idRequerimiento) throws FinancieroException{
		
		TrxFinancieroDTO trxFinancieroDTO = new TrxFinancieroDTO();
		trxFinancieroDTO.setTerminalSamId(terminalSamId.toString());
		trxFinancieroDTO.setPrefijoOpId(String.valueOf(prefijo_op));
		trxFinancieroDTO.setIdRequerimiento(idRequerimiento);
		
		try {
			return trxFinancieroService.registrarPruebaServicio(trxFinancieroDTO);
		} catch (FinancieroException e) {
			throw e;
		}
	}
	
	public void updateIntencion(Intencion inten) {
		try {
			trxFinancieroService.updateIntencion(inten);
		} catch (Exception e) {
			LOGGER.error("Error de AbstractFinancieroService [updateIntencion]: "+ e.getMessage(), e);
		}
	}
	
	/**
	 * Registra la workingkey en TRX_FINANCIERO.
	 * 
	 * @param FinancieroServiceBaseRequestDTO
	 */
	public void registerWorkingKey(FinancieroServiceBaseRequestDTO trxData, Intencion in) throws FinancieroException {
		TrxFinancieroDTO trxFinancieroDTO = modelMapperService.modelMapperHelper(trxData, TrxFinancieroDTO.class);

		if (null == trxFinancieroDTO.getImporte()) {
			trxFinancieroDTO.setImporte("0");
		}

		/*if (null == trxFinancieroDTO.getEstadoTrxId()) {
			trxFinancieroDTO.setEstadoTrxId("0");
		}*/

		trxFinancieroDTO.setSecuenciaOnWS(trxData.getSecuenciaOn());
		trxFinancieroDTO.setSecuenciaOnB24(trxData.getSecuenciaOnRta());
		trxFinancieroDTO.setSucursal(StringUtils.leftPad(trxFinancieroDTO.getSucursal(), 8, "0"));

		//trxFinancieroService.insert(trxFinancieroDTO);
		trxFinancieroService.registerWorkingKey(trxFinancieroDTO, in);
	}

	/**
	 * Valida los parametros ingresados y con el validationChain de negocio,
	 * obtiene datos que sirven para la posteridad del flujo.
	 * 
	 * @param baseRequestDTO
	 * @param pan
	 *            (tokenizado o no)
	 * @param tokenizado
	 *            true si esta tokenizado
	 * @return DTO de Validation enriquecido.
	 * @throws FinancieroException
	 */
	protected ValidationDTO validate(FinancieroServiceBaseRequestDTO baseRequestDTO, String pan, Boolean tokenizado)
			throws FinancieroException {
		if (null == pan && tokenizado) {
			throw new FinancieroException(FINA145.getCode(), FINA145.getMsg());
		}

		ValidationDTO validation = new ValidationDTO();
		String tarjeta = pan;

		if (tokenizado && null != tarjeta && !tarjeta.isEmpty()) {
			try {
				validation.addInputValue(ValidationDTO.TOKENIZADO, tarjeta);

				tarjeta = tokenServiceConectado.getPanFromToken(tarjeta);
			} catch (RedLinkFrameworkRuntimeException re) {
				LOGGER.error("Ocurrio un error al obtener el pan del token: " + re.getMessage());
				
				if(re.getCause()!=null)
					throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), re.getCause().toString());
				else
					throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), "");
			}
		}

		//ACA SE VALIDA LOS DATOS OBTENIDOS
		try{			
			validation.addInputValue(ValidationDTO.TARJETA, tarjeta);
			
		} catch (RedLinkFrameworkRuntimeException re) {
			LOGGER.error("FINA203-Ocurrio un error al obtener el pan de la tarjeta: " + re.getMessage());
			throw new FinancieroException(FINA203.getCode(), FINA203.getMsg());
		}

		try{			
			validation.addInputValue(ValidationDTO.ENTIDAD_ID, baseRequestDTO.getIdEntidad());
			
		} catch (RedLinkFrameworkRuntimeException re) {
			LOGGER.error("FINA203-Ocurrio un error al obtener Entidad_Id: " + re.getMessage());
			throw new FinancieroException(FINA203.getCode(), FINA203.getMsg());
		}
				
		try{			
			validation.addInputValue(ValidationDTO.SUCURSAL_ID, baseRequestDTO.getSucursal());
			
		} catch (RedLinkFrameworkRuntimeException re) {
			LOGGER.error("FINA203-Ocurrio un error al obtener Sucursal_Id: " + re.getMessage());
			throw new FinancieroException(FINA203.getCode(), FINA203.getMsg());
		}

		try{			
			validation.addInputValue(ValidationDTO.PREFIJO_OP_ID, baseRequestDTO.getPrefijoOpId());
			
		} catch (RedLinkFrameworkRuntimeException re) {
			LOGGER.error("FINA203-Ocurrio un error al obtener Prefijo_OP_Id: " + re.getMessage());
			throw new FinancieroException(FINA203.getCode(), FINA203.getMsg());
		}
		
		try{			
			validation.addInputValue(ValidationDTO.SEQ_NUM, baseRequestDTO.getSecuenciaOn());
			
		} catch (RedLinkFrameworkRuntimeException re) {
			LOGGER.error("FINA203-Ocurrio un error al obtener Seq_NUM: " + re.getMessage());
			throw new FinancieroException(FINA203.getCode(), FINA203.getMsg());
		}

		try{			
			validation.addInputValue(ValidationDTO.PRODUCTO_ID, baseRequestDTO.getProductoId());
			
		} catch (RedLinkFrameworkRuntimeException re) {
			LOGGER.error("FINA203-Ocurrio un error al obtener Producto_ID: " + re.getMessage());
			throw new FinancieroException(FINA203.getCode(), FINA203.getMsg());
		}
		
		//validation.addInputValue(ValidationDTO.TARJETA, tarjeta);
		//validation.addInputValue(ValidationDTO.ENTIDAD_ID, baseRequestDTO.getIdEntidad());
		//validation.addInputValue(ValidationDTO.SUCURSAL_ID, baseRequestDTO.getSucursal());
		//validation.addInputValue(ValidationDTO.PREFIJO_OP_ID, baseRequestDTO.getPrefijoOpId());
		//validation.addInputValue(ValidationDTO.SEQ_NUM, baseRequestDTO.getSecuenciaOn());
		//validation.addInputValue(ValidationDTO.PRODUCTO_ID, baseRequestDTO.getProductoId());
		
		if (baseRequestDTO instanceof FinancieroCommonDTO) {
			populateOperationDataForValidation(validation, (FinancieroCommonDTO) baseRequestDTO);
		}

		financieroValidationChain.validate(validation);

		return validation;
	}

	/**
	 * Puebla el objeto de Validation con los datos requeridos para la
	 * validacion de limites y operacion.
	 * 
	 * @param validation
	 * @param baseRequestDTO
	 */
	private void populateOperationDataForValidation(ValidationDTO validation, FinancieroCommonDTO baseRequestDTO) {
		validation.addInputValue(ValidationDTO.REQUIERE_PIN, baseRequestDTO.getPin());
		validation.addInputValue(ValidationDTO.REQUIERE_DNI, baseRequestDTO.getDni());
		validation.addInputValue(ValidationDTO.REQUIERE_SEG, baseRequestDTO.getCardSecurityCode());
		validation.addInputValue(ValidationDTO.REQUIERE_AUT, baseRequestDTO.getAutorizacion());
		validation.addInputValue(ValidationDTO.REQUIERE_SFX, baseRequestDTO.getDebitCardSuffix());

		if (baseRequestDTO instanceof FinancieroAccountDTO) {
			validation.addInputValue(ValidationDTO.IMPORTE,
					((FinancieroAccountDTO) baseRequestDTO).getImporte().toPlainString());
		}
	}

	/**
	 * Agrega los campos requeridos para la validacion de la anulacion e invoca
	 * al componente.
	 * 
	 * @param anulacion
	 * @param originalCod
	 * @param validation
	 * 
	 * @throws FinancieroException
	 */
	protected void rollbackValidation(AnulacionCommonDTO anulacion, String originalCod, ValidationDTO validation) throws FinancieroException {
		validation.addInputValue(ValidationDTO.TERMINAL_SAM_ID, anulacion.getTerminalSamId());
		validation.addInputValue(ValidationDTO.ID_REQ_ORIG, anulacion.getIdRequerimientoOrig());
		validation.addInputValue(ValidationDTO.TOKENIZADO, anulacion.getTokenizado());
		validation.addInputValue(ValidationDTO.PREFIJO_OP_ID, originalCod);

		anulacionValidation.validate(validation);
	}
	
	/**
	 * 
	 * @param realizarDevolucionPagoPEIRequestDTO
	 * @param originalCod
	 * @param validation
	 * @throws FinancieroException
	 */
	protected void returnPEIValidation(String idRequerimientoOrig, ValidationDTO validation, boolean isParcial)
			throws FinancieroException {

		validation.addInputValue(ValidationDTO.ID_REQ_ORIG, idRequerimientoOrig);
		devolucionPEIValidation.setParcial(isParcial);
		devolucionPEIValidation.validate(validation);
	}
		
	
	/**
	 * Agrega los campos requeridos para la validacion de la anulacion e invoca
	 * al componente.
	 * 
	 * @param anulacion
	 * @param originalCod
	 * @param validation
	 * 
	 * @throws FinancieroException
	 */
	protected void rollbackDebitPayValidation(AnulacionCommonDTO anulacion,	String originalCod, ValidationDTO validation)
			throws FinancieroException {
		validation.addInputValue(ValidationDTO.TERMINAL_SAM_ID,	anulacion.getTerminalSamId());
		validation.addInputValue(ValidationDTO.ID_REQ_ORIG,	anulacion.getIdRequerimientoOrig());
		validation.addInputValue(ValidationDTO.TOKENIZADO,	anulacion.getTokenizado());
		validation.addInputValue(ValidationDTO.PREFIJO_OP_ID, originalCod);

		anulacionPagoDebitoValidation.validate(validation);
	}

	/**
	 * Extrae el PAN del track2 de la respuesta de Tandem.
	 * 
	 * @param tandemResponse
	 * @throws FinancieroException
	 */
	protected String extractPan(TandemResponse tandemResponse) throws FinancieroException {
		String track2 = tandemResponse.getTrack2();
		String pan = null;
		if (null != track2 && !track2.isEmpty()) {
			pan = track2.split(("="))[0].replace(";", "");
		} else {
			LOGGER.error("Error al extraer el PAN de la respuesta de Tandem. Track nulo o vacio.");
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}

		return pan;
	}

	/**
	 * Extrae las cuentas de la respuesta de Tandem y genera el DTO
	 * correspondiente.
	 * 
	 * @param tandemResponse
	 * @return lista de CuentaDTO.
	 */
	protected List<CuentaDTO> extractAccounts(ConsultaExtraccionResponse tandemResponse) {
		String[] accounts = tandemResponse.getData().substring(1).split("\\s+");

		List<CuentaDTO> accountsList = new ArrayList<CuentaDTO>();

		for (int i = 0; i < accounts.length; i++) {
			CuentaDTO account = new CuentaDTO();
			account.setNumeroCuenta(accounts[i]);
			String tipoCuenta = accounts[i + 1];
			account.setTipoCuenta(tipoCuenta);
			account.setEstadoCuenta(accounts[i + 2]);

			String monedaCuenta = "0";

			// No valia la pena complicar algo tan simple con componentes
			// adicionales.
			switch (Integer.parseInt(tipoCuenta)) {
			case 7:
			case 15:
			case 33:
				monedaCuenta = "1";
				break;
			case 14:
				monedaCuenta = "2";
				break;
			case 16:
				monedaCuenta = "3";
				break;
			}

			account.setMonedaCuenta(monedaCuenta);
			i += 2;
			accountsList.add(account);
		}

		return accountsList;
	}

	/**
	 * Puebla los datos comunes en los DTO de respuesta.
	 * 
	 * @param dtoResponse
	 * @param tandemResponse
	 */
	protected void populateResponseData(FinancieroServiceBaseResponseDTO dtoResponse, SAMTandemResponse tandemResponse) {
		dtoResponse.setCodRta(tandemResponse.getCodRta());
		if (null != tandemResponse.getPostDat()) {
			try {
				dtoResponse.setFechaOn(DateUtil.fechaOnExtFormat(tandemResponse.getPostDat()));
			} catch (ParseException e) {
				LOGGER.error("Error de formato al convertir la fechaOn: " + e.getMessage() + " Se continua con la ejecucion.");
			}
		}
		dtoResponse.setMsg(tandemResponse.getMsg());
		dtoResponse.setSecuenciaOn(tandemResponse.getSeqNum());
	}

	/**
	 * Extrae la informacion recibida de Tandem sobre la extraccion realizada.
	 * 
	 * @param tandemResponse
	 * @param commonDTO
	 */
	protected void extractWithdrawData(SAMTandemResponse tandemResponse, OperacionCommonDTO commonDTO) {
		String mensaje = tandemResponse.getRespuesta();

		if (null != mensaje && !mensaje.isEmpty()) {

			commonDTO.setSaldo(addComma(evaluateValue(mensaje, 1, 12)));
			commonDTO.setDisponible(addComma(evaluateValue(mensaje, 12, 21)));
			commonDTO.setExtraccionesDisp(Integer.parseInt(evaluateValue(mensaje, 21, 23)));
			commonDTO.setTNA(addComma(evaluateValue(mensaje, 32, 40)));
			commonDTO.setTEM(addComma(evaluateValue(mensaje, 40, 48)));
			commonDTO.setCantidadCuotas(Integer.parseInt(evaluateValue(mensaje, 48, 50)));
			commonDTO.setDisponibleEfCuotas(addComma(evaluateValue(mensaje, 50, 58)));
			commonDTO.setAnticipo(evaluateValue(mensaje, 58, 69));
		}
	}

	/**
	 * Agrega una coma al String.
	 * 
	 * @param value
	 * @return el string con una coma agregada 2 posiciones antes del final.
	 */
	protected String addComma(String value) {
		return value = new StringBuilder(value.trim()).insert(value.trim().length() - 2, ".").toString();
	}

	/**
	 * Extrae el valor del String indicado siempre y cuando se trate de numeros,
	 * caso contrario devuelve un String con valor 0.
	 */
	private String evaluateValue(String message, int start, int end) {
		Pattern pattern = Pattern.compile("\\d+");

		return (pattern.matcher(message.substring(start, end)).matches()) ? message.substring(start, end)
				: StringUtils.leftPad("0", end - start, "0");
	}

	/**
	 * @return the financieroValidationChain
	 */
	public ValidationChain getFinancieroValidationChain() {
		return financieroValidationChain;
	}

	/**
	 * @param financieroValidationChain
	 *            the financieroValidationChain to set
	 */
	public void setFinancieroValidationChain(ValidationChain financieroValidationChain) {
		this.financieroValidationChain = financieroValidationChain;
	}

	/**
	 * @return the trxFinancieroService
	 */
	public TrxFinancieroService getTrxFinancieroService() {
		return trxFinancieroService;
	}

	/**
	 * @param trxFinancieroService
	 *            the trxFinancieroService to set
	 */
	public void setTrxFinancieroService(TrxFinancieroService trxFinancieroService) {
		this.trxFinancieroService = trxFinancieroService;
	}

	/**
	 * @return the modelMapperService
	 */
	public ModelMapperService getModelMapperService() {
		return modelMapperService;
	}

	/**
	 * @param modelMapperService
	 *            the modelMapperService to set
	 */
	public void setModelMapperService(ModelMapperService modelMapperService) {
		this.modelMapperService = modelMapperService;
	}

	/**
	 * @return the prefijoOpService
	 */
	public PrefijoOpService getPrefijoOpService() {
		return prefijoOpService;
	}

	/**
	 * @param prefijoOpService
	 *            the prefijoOpService to set
	 */
	public void setPrefijoOpService(PrefijoOpService prefijoOpService) {
		this.prefijoOpService = prefijoOpService;
	}

	/**
	 * @return the tokenServiceConectado
	 */
	public TokenizacionPANService getTokenServiceConectado() {
		return tokenServiceConectado;
	}

	/**
	 * @param tokenServiceConectado
	 *            the tokenServiceConectado to set
	 */
	public void setTokenServiceConectado(TokenizacionPANService tokenServiceConectado) {
		this.tokenServiceConectado = tokenServiceConectado;
	}

	/**
	 * @return the comercioPEIValidation
	 */
	public AbstractFSValidation getComercioPEIValidation() {
		return comercioPEIValidation;
	}

	/**
	 * @param comercioPEIValidation
	 *            the comercioPEIValidation to set
	 */
	public void setComercioPEIValidation(AbstractFSValidation comercioPEIValidation) {
		this.comercioPEIValidation = comercioPEIValidation;
	}

	/**
	 * @return the terminalPEIValidation
	 */
	public AbstractFSValidation getTerminalPEIValidation() {
		return terminalPEIValidation;
	}

	/**
	 * @param terminalPEIValidation
	 *            the terminalPEIValidation to set
	 */
	public void setTerminalPEIValidation(AbstractFSValidation terminalPEIValidation) {
		this.terminalPEIValidation = terminalPEIValidation;
	}
	
}


