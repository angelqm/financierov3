/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  12/04/2015 - 12:44:46
 */

package ar.com.redlink.sam.financieroservices.service.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA200;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA202;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import ar.com.redlink.framework.services.crud.impl.RedLinkGenericServiceImpl;
import ar.com.redlink.sam.financieroservices.entity.PrefijoOp;
import ar.com.redlink.sam.financieroservices.service.PrefijoOpService;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;

/**
 * Extension del servicio generico de {@link PrefijoOp}.
 * 
 * @author aguerrea
 * 
 */
public class PrefijoOpServiceImpl extends RedLinkGenericServiceImpl<PrefijoOp>
		implements PrefijoOpService, InitializingBean {

	private static final Logger LOGGER = Logger.getLogger(PrefijoOpServiceImpl.class);

	private static final String CLASSPATH = "classpath";
	private Properties prefijoOpMethodMap;
	private String propertiesPath;
	private Map<String, Boolean> reversableMap;
	private String notReversables;

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.PrefijoOpService#getPrefijoOpCod(java.lang.String)
	 */
	@Override
	public String getPrefijoOpCod(String methodName) {
		return prefijoOpMethodMap.getProperty(methodName);
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.service.PrefijoOpService#isReversable(java.lang.String)
	 */
	@Override
	public boolean isReversable(String prefijoOp) {
		return reversableMap.get(prefijoOp); // TODO: Hacerlo online en startup.
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.service.PrefijoOpService#getNotReversablePrefijoOps()
	 */
	public String getNotReversablePrefijoOps() {
		if (null == notReversables) {
			StringBuilder sb = new StringBuilder();
			for (Entry<String, Boolean> entry : reversableMap.entrySet()) {
				if (!entry.getValue()) {
					sb.append(entry.getKey() + ",");
				}
			}

			sb.deleteCharAt(sb.length() - 1);
			notReversables = sb.toString();
		}

		return notReversables;
	}

	/**
	 * @return the prefijoOpMethodMap
	 */
	public Properties getPrefijoOpMethodMap() {
		return prefijoOpMethodMap;
	}

	/**
	 * @param prefijoOpMethodMap
	 *            the prefijoOpMethodMap to set
	 */
	public void setPrefijoOpMethodMap(Properties prefijoOpMethodMap) {
		this.prefijoOpMethodMap = prefijoOpMethodMap;
	}

	/**
	 * @return the propertiesPath
	 */
	public String getPropertiesPath() {
		return propertiesPath;
	}

	/**
	 * @param propertiesPath
	 *            the propertiesPath to set
	 */
	public void setPropertiesPath(String propertiesPath) {
		this.propertiesPath = propertiesPath;
	}

	/**
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		prefijoOpMethodMap = new Properties();

		InputStream fis = null;
		if (propertiesPath.contains(CLASSPATH)) {
			fis = retrieveResourcesFromClasspath(propertiesPath);
		} else {
			fis = retrieveResourcesFromFileSystem(propertiesPath);
		}

		if (null != fis) {
			prefijoOpMethodMap.load(fis);
		} else {
			LOGGER.error("Error al obtener el IS correspondiente al archivo de Mappings.");
			throw new FinancieroException(FINA202.getCode(), FINA202.getMsg());
		}
	}

	/**
	 * Obtiene los inputStreams desde el FS.
	 * 
	 * @param path
	 * @param streamsMap
	 * 
	 * @throws FinancieroException
	 */
	private InputStream retrieveResourcesFromFileSystem(String path)
			throws FinancieroException {
		File dir = new File(path);

		InputStream fis = null;
		try {
			fis = new FileInputStream(dir);
		} catch (FileNotFoundException e) {
			LOGGER.error(
					"No se encontro el archivo de mappings: " + e.getMessage(),
					e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}

		return fis;
	}

	/**
	 * Obtiene los inputStreams traversando un directorio en el classpath.
	 * 
	 * @param path
	 * @param streamsMap
	 * 
	 * @throws FinancieroException
	 */
	private InputStream retrieveResourcesFromClasspath(String path)
			throws FinancieroException {
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		InputStream fis = null;
		try {
			Resource[] resources = resolver.getResources(path);

			if (resources.length == 1) {
				fis = resources[0].getInputStream();
			}

		} catch (IOException e) {
			LOGGER.error(
					"Excepcion al obtener los recursos desde el classpath: ", e);
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
		}

		return fis;
	}

	/**
	 * @return the reversableMap
	 */
	public Map<String, Boolean> getReversableMap() {
		return reversableMap;
	}

	/**
	 * @param reversableMap
	 *            the reversableMap to set
	 */
	public void setReversableMap(Map<String, Boolean> reversableMap) {
		this.reversableMap = reversableMap;
	}
}
