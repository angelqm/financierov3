package ar.com.redlink.sam.financieroservices.service.impl;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.convention.MatchingStrategies;

import ar.com.redlink.sam.financieroservices.service.ModelMapperService;

/**
 * Implementacion del Servicio ModelMapper. Mapea los atributos de dos objetos
 * diferentes. Se utiliza para mapear de Entidades a DTOs y viceversa.
 * 
 * Extraido de SFA-WS.
 * 
 * @author Hernan Di Tota
 * 
 */
public class ModelMapperServiceImpl implements ModelMapperService {

	private static final Logger LOGGER = Logger.getLogger(ModelMapperServiceImpl.class);

	/**
	 * Mapea dos Objectos (Pueden ser entidades o DTOs).
	 * 
	 * @see ar.com.redlink.sfa.wservices.service.ModelMapperService#modelMapperHelper(java.lang.Object,
	 *      java.lang.Class)
	 */
	@Override
	public <T1, T2> T2 modelMapperHelper(T1 entidad, Class<T2> entityClass) {
		ModelMapper modelMapper = new ModelMapper();
		T2 response = null;
		try {
			// Se setea la estrategia para realizar la conversion.
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
			response = modelMapper.map(entidad, entityClass);
		} catch (Exception e) {
			LOGGER.error("Error en ModelMapperServiceImpl.modelMapperHelper", e);
		}
		return response;
	}

	/**
	 * Mapea dos Objectos (Pueden ser entidades o DTOs). Se le agrega un
	 * parametro con el detalle de mapeo de ciertos atributos.
	 * 
	 * @see ar.com.redlink.sfa.wservices.service.ModelMapperService#modelMapperHelper(org.modelmapper.PropertyMap,
	 *      java.lang.Object, java.lang.Class)
	 */
	@Override
	public <T1, T2> T2 modelMapperHelper(PropertyMap<T1, T2> orderMap,
			T1 requestDTO, Class<? extends T2> entityClass) {
		ModelMapper modelMapper = new ModelMapper();
		T2 response = null;
		try {
			modelMapper.addMappings(orderMap);
			response = modelMapper.map(requestDTO, entityClass);
		} catch (Exception e) {
			LOGGER.error("Error en ModelMapperServiceImpl.modelMapperHelper", e);
		}
		return response;
	}

}
