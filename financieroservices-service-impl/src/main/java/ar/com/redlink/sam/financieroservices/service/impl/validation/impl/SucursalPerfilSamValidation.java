/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  01/04/2015 - 15:18:49
 */

package ar.com.redlink.sam.financieroservices.service.impl.validation.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA103;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA127;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA128;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA146;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA200;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ar.com.redlink.framework.services.crud.RedLinkGenericService;
import ar.com.redlink.framework.services.exception.RedLinkServiceException;
import ar.com.redlink.sam.financieroservices.dao.search.filter.SucPerfilSamBySucIdSearchFilter;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.SucursalPerfilSam;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.validation.AbstractFSValidation;

/**
 * Encapsula la validacion de SucursalPerfilSam y obtiene el Perfil Financiero
 * correspondiente.
 * 
 * @author aguerrea
 * 
 */
public class SucursalPerfilSamValidation extends AbstractFSValidation {

	private static final Logger LOGGER = Logger
			.getLogger(SucursalPerfilSamValidation.class);

	private RedLinkGenericService<SucursalPerfilSam> sucursalPerfilSamService;

	/**
	 * @see ar.com.redlink.sam.financieroservices.service.validation.FSValidation#validate(ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO)
	 */
	@Override
	public ValidationDTO validate(ValidationDTO validation)
			throws FinancieroException {
		
		if (!"4".equals(validation.getInputValue(ValidationDTO.PRODUCTO_ID))) {
		
			nullCheck(validation, ValidationDTO.SUCURSAL_ID, FINA103);
	
			List<SucursalPerfilSam> sucursalesPerfilSam = null;

			try {
				//LOGGER.info("[DAO INI] SucPerfilSamBySucIdSearchFilter");
				sucursalesPerfilSam = sucursalPerfilSamService.getAll(
						new SucPerfilSamBySucIdSearchFilter(Long.valueOf(validation
								.getInputValue(ValidationDTO.SUCURSAL_ID))), null).getResult();

				//LOGGER.info("[DAO FIN] SucPerfilSamBySucIdSearchFilter");
			} catch (RedLinkServiceException e) {
				LOGGER.error("Error al acceder el servicio de SucursalPerfilSam: "+ e.getMessage(), e);
				throw new FinancieroException(FINA200.getCode(), FINA200.getMsg());
			}
	
			if (sucursalesPerfilSam.size() == 0) {
				throw new FinancieroValidationException(FINA127.getCode(),FINA127.getMsg());
			} else {
				List<Integer> idxValidos = new ArrayList<Integer>();
	
				for (int i = 0; i < sucursalesPerfilSam.size(); i++) {
					SucursalPerfilSam sucursalPerfilSam = sucursalesPerfilSam.get(i);
	
					try {
						validateEstadoRegistro(sucursalPerfilSam, FINA128);
						validateDates(sucursalPerfilSam, FINA128);
					} catch (FinancieroValidationException e) {
						LOGGER.error("Se registra una relacion (ID:"
								+ sucursalPerfilSam.getSucursalPerfilSamId()
								+ ") que no cumple con el estado o fecha de vigencia: "
								+ e.getMsj());
						continue;
					}
					idxValidos.add(i);
				}
	
				int size = idxValidos.size();
				if (size == 1) {
					validation.addOutputValue(ValidationDTO.PERFIL_SAM_ID,
							sucursalesPerfilSam.get(idxValidos.get(0)).getPerfilSam().getPerfilSamId());
				} else {
					if (size == 0) {
						throw new FinancieroValidationException(FINA127.getCode(),FINA127.getMsg());
					} else {
						throw new FinancieroValidationException(FINA146.getCode(),FINA146.getMsg());
					}
				}
	
			}
		}
		
		nextValidation(validation); // PerfilSam.

		return validation;
	}

	/**
	 * @return the sucursalPerfilSamService
	 */
	public RedLinkGenericService<SucursalPerfilSam> getSucursalPerfilSamService() {
		return sucursalPerfilSamService;
	}

	/**
	 * @param sucursalPerfilSamService
	 *            the sucursalPerfilSamService to set
	 */
	public void setSucursalPerfilSamService(
			RedLinkGenericService<SucursalPerfilSam> sucursalPerfilSamService) {
		this.sucursalPerfilSamService = sucursalPerfilSamService;
	}

}
