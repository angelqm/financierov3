/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  27/04/2015 - 11:28:31
 */

package ar.com.redlink.sam.financieroservices.service.impl.translation;

import ar.com.redlink.sam.financieroservices.dto.RequerimientoDTO;
import ar.com.redlink.sam.financieroservices.entity.TrxFinanciero;
import ar.com.redlink.sam.financieroservices.entity.util.DateUtil;

/**
 * Translator para transformar un entity de tipo {@link TrxFinanciero} a
 * {@link RequerimientoDTO}.
 * 
 * @author aguerrea
 * 
 */
public final class TrxFinancieroTranslator {

	/**
	 * Transforma un {@link TrxFinanciero} en un {@link RequerimientoDTO}.
	 * 
	 * @param trxFinanciero
	 * @return RequerimientoDTO.
	 * 
	 */
	public static RequerimientoDTO translateTrxFinanciero(TrxFinanciero trxFinanciero) {
		RequerimientoDTO requerimientoDTO = new RequerimientoDTO();

		requerimientoDTO.setIdRequerimiento(trxFinanciero.getIdRequerimientoAlta());
		requerimientoDTO.setCodigoOperacion(Long.toString(trxFinanciero	.getPrefijoOp().getPrefijoOpId()));
		requerimientoDTO.setEstadoRequerimiento(trxFinanciero.getEstadoTrx().getDescripcion());
		requerimientoDTO.setImporteOperacion(trxFinanciero.getImporte()	.toPlainString());
		requerimientoDTO.setNombreOperacion(trxFinanciero.getPrefijoOp().getNombre());
		requerimientoDTO.setSecuenciaOnOperacion(Integer.toString(trxFinanciero.getSecuenciaOnWS()));
		requerimientoDTO.setFechaHoraOperacion(DateUtil.timestampFormat(trxFinanciero.getFechaHoraAlta()));
		requerimientoDTO.setFechaOn(DateUtil.fechaOnExtFormat(trxFinanciero.getFechaOn()));

		return requerimientoDTO;
	}
}
