package ar.com.redlink.sam.financieroservices.ws.service.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import ar.com.redlink.framework.services.crud.impl.RedLinkGenericServiceImpl;
import ar.com.redlink.sam.financieroservices.entity.Entidad;
import ar.com.redlink.sam.financieroservices.service.TrxFinancieroService;
import ar.com.redlink.sam.financieroservices.service.impl.ModelMapperServiceImpl;
import ar.com.redlink.sam.financieroservices.service.impl.test.FinancieroServiceMockImpl;
import ar.com.redlink.sam.financieroservices.service.impl.validation.ValidationChainImpl;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.AgenteMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.DispositivoPosMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.EntidadMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.RequestMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.ServidorMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.SucursalMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.TerminalSamMockValidation;
import ar.com.redlink.sam.financieroservices.service.validation.FSValidation;
import ar.com.redlink.sam.financieroservices.ws.request.WSObtenerWorkingKeyRequest;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSHeaderRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosObtenerWorkingKeyRequest;
import ar.com.redlink.sam.financieroservices.ws.response.WSObtenerWorkingKeyResponse;
import ar.com.redlink.sam.financieroservices.ws.service.impl.WSFinancieroServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class ObtenerWorkingKeyTest {

	//private ApplicationContext ctx;
	
	private ValidationChainImpl financieroValidationChain;
	
	private ModelMapperServiceImpl modelMapperService;
	@Mock
	private TrxFinancieroService trxFinancieroService;
	@Mock
	private FSValidation initialValidation;
	
	private RedLinkGenericServiceImpl<Entidad> entidadService;
	
	@InjectMocks
	WSFinancieroServiceImpl wsFinancieroService= new WSFinancieroServiceImpl();
	
	FinancieroServiceMockImpl financieroService= new FinancieroServiceMockImpl();
	
	WSObtenerWorkingKeyRequest workingKeyrequest;
	WSHeaderRequest workingKeyHaderequest;
	WSParametrosObtenerWorkingKeyRequest workingKeyParametros;
	
	@Test
	public void obtenerWorkingKeyTest01(){
		workingKeyHaderequest = new WSHeaderRequest();
		workingKeyHaderequest.setEntidad("0945");
		workingKeyHaderequest.setIdentDispositivo("000-326-904-231");
		workingKeyHaderequest.setIdentTerminal("PEI00000000022");
		workingKeyHaderequest.setIdRequerimiento("DESAPRUEBA2019012700021842");
		workingKeyHaderequest.setIpCliente(">127.0.0.1");
		workingKeyHaderequest.setSucursal("1325");
		workingKeyHaderequest.setTerminal("00007000");
		workingKeyHaderequest.setTimeStamp("20170302145432");
		
		workingKeyParametros= new WSParametrosObtenerWorkingKeyRequest();
		workingKeyParametros.setSecuenciaOn("00033055");
		workingKeyParametros.setTipo("D");
		
		workingKeyrequest= new WSObtenerWorkingKeyRequest();
		workingKeyrequest.setHeader(workingKeyHaderequest);
		workingKeyrequest.setObtenerWorkingKeyDataRequest(workingKeyParametros);
		
		financieroValidationChain= new ValidationChainImpl();
		
		RequestMockValidation req = new RequestMockValidation();
		EntidadMockValidation ent =  new EntidadMockValidation();
		TerminalSamMockValidation ter = new TerminalSamMockValidation();
		SucursalMockValidation suc = new SucursalMockValidation();
		ServidorMockValidation ser = new ServidorMockValidation();
		AgenteMockValidation agt	= new AgenteMockValidation();
		DispositivoPosMockValidation dis = new DispositivoPosMockValidation();
		
		entidadService= new RedLinkGenericServiceImpl<Entidad>();
		ent.setEntidadService(entidadService);
			
		agt.setNextValidation(dis);
		ser.setNextValidation(agt);
		suc.setNextValidation(ser);
		ter.setNextValidation(suc);
		ent.setNextValidation(ter);
		req.setNextValidation(ent);
		
		financieroValidationChain.setInitialValidation(req);
		
		modelMapperService = new ModelMapperServiceImpl();
		wsFinancieroService.setModelMapperService(modelMapperService);
			
		wsFinancieroService.setFinancieroValidationChain(financieroValidationChain);
		financieroService.setTrxFinancieroService(trxFinancieroService);
		
		wsFinancieroService.setFinancieroService(financieroService);
		MockitoAnnotations.initMocks(wsFinancieroService);
		financieroService.setModelMapperService(modelMapperService);
		WSObtenerWorkingKeyResponse response= wsFinancieroService.obtenerWorkingKey(workingKeyrequest);
		Assert.assertEquals(response.getHeader().getCodigoRespuesta(), "FINA-000");
	}
	
	@Test
	public void obtenerWorkingKeyTest02(){
		workingKeyHaderequest = new WSHeaderRequest();
		workingKeyHaderequest.setEntidad("0945");
		workingKeyHaderequest.setIdentDispositivo("000-326-904-231");
		//workingKeyHaderequest.setIdentTerminal("PEI00000000022");
		workingKeyHaderequest.setIdRequerimiento("DESAPRUEBA2019012700021842");
		workingKeyHaderequest.setIpCliente(">127.0.0.1");
		workingKeyHaderequest.setSucursal("1325");
		workingKeyHaderequest.setTerminal("00007000");
		workingKeyHaderequest.setTimeStamp("20170302145432");
		
		workingKeyParametros= new WSParametrosObtenerWorkingKeyRequest();
		workingKeyParametros.setSecuenciaOn("00033055");
		workingKeyParametros.setTipo("D");
		
		workingKeyrequest= new WSObtenerWorkingKeyRequest();
		workingKeyrequest.setHeader(workingKeyHaderequest);
		workingKeyrequest.setObtenerWorkingKeyDataRequest(workingKeyParametros);
		
		financieroValidationChain= new ValidationChainImpl();
		
		RequestMockValidation req = new RequestMockValidation();
		EntidadMockValidation ent =  new EntidadMockValidation();
		TerminalSamMockValidation ter = new TerminalSamMockValidation();
		SucursalMockValidation suc = new SucursalMockValidation();
		ServidorMockValidation ser = new ServidorMockValidation();
		AgenteMockValidation agt	= new AgenteMockValidation();
		DispositivoPosMockValidation dis = new DispositivoPosMockValidation();
		
		entidadService= new RedLinkGenericServiceImpl<Entidad>();
		ent.setEntidadService(entidadService);
			
		agt.setNextValidation(dis);
		ser.setNextValidation(agt);
		suc.setNextValidation(ser);
		ter.setNextValidation(suc);
		ent.setNextValidation(ter);
		req.setNextValidation(ent);
		
		financieroValidationChain.setInitialValidation(req);
		
		modelMapperService = new ModelMapperServiceImpl();
		wsFinancieroService.setModelMapperService(modelMapperService);
			
		wsFinancieroService.setFinancieroValidationChain(financieroValidationChain);
		financieroService.setTrxFinancieroService(trxFinancieroService);
		
		wsFinancieroService.setFinancieroService(financieroService);
		MockitoAnnotations.initMocks(wsFinancieroService);
		financieroService.setModelMapperService(modelMapperService);
		WSObtenerWorkingKeyResponse response= wsFinancieroService.obtenerWorkingKey(workingKeyrequest);
		Assert.assertEquals(response.getHeader().getCodigoRespuesta(), "FINA-110");
	}
	
	
	
}
