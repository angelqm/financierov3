package ar.com.redlink.sam.financieroservices.ws.service.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import ar.com.redlink.framework.services.crud.impl.RedLinkGenericServiceImpl;
import ar.com.redlink.sam.financieroservices.entity.Entidad;
import ar.com.redlink.sam.financieroservices.service.TrxFinancieroService;
import ar.com.redlink.sam.financieroservices.service.impl.ModelMapperServiceImpl;
import ar.com.redlink.sam.financieroservices.service.impl.PrefijoOpServiceImpl;
import ar.com.redlink.sam.financieroservices.service.impl.test.FinancieroServiceMockImpl;
import ar.com.redlink.sam.financieroservices.service.impl.validation.ValidationChainImpl;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.AgenteMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.DispositivoPosMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.EntidadMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.RequestMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.ServidorMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.SucursalMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.TerminalSamMockValidation;
import ar.com.redlink.sam.financieroservices.service.validation.FSValidation;
import ar.com.redlink.sam.financieroservices.tandem.service.mock.TandemAccountMOCKServiceImpl;
import ar.com.redlink.sam.financieroservices.ws.request.WSObtenerSaldoCuentaRequest;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSHeaderRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosObtenerSaldoRequest;
import ar.com.redlink.sam.financieroservices.ws.response.WSObtenerSaldoCuentaResponse;
import ar.com.redlink.sam.financieroservices.ws.service.impl.WSFinancieroServiceImpl;


@RunWith(MockitoJUnitRunner.class)
public class ObtenerSaldoTest {

	//private ApplicationContext ctx;
	
	private ValidationChainImpl financieroValidationChain;
	
	private ModelMapperServiceImpl modelMapperService;
	@Mock
	private TrxFinancieroService trxFinancieroService;
	@Mock
	private FSValidation initialValidation;
	
	private RedLinkGenericServiceImpl<Entidad> entidadService;
	
	@InjectMocks
	WSFinancieroServiceImpl wsFinancieroService= new WSFinancieroServiceImpl();
	
	FinancieroServiceMockImpl financieroService= new FinancieroServiceMockImpl();
	
	WSObtenerSaldoCuentaRequest saldoCuentaRequest;
	WSHeaderRequest saldoCuentaHeaderRequest;
	WSParametrosObtenerSaldoRequest obtenerSaldoParametros;
	
	PrefijoOpServiceImpl prefijoOpService = new PrefijoOpServiceImpl(); 
	
	@Test
	public void obtenerSaldoTest01(){
		saldoCuentaHeaderRequest = new WSHeaderRequest();
		saldoCuentaHeaderRequest.setEntidad("0945");
		saldoCuentaHeaderRequest.setIdentDispositivo("000-326-904-231");
		saldoCuentaHeaderRequest.setIdentTerminal("PEI00000000022");
		saldoCuentaHeaderRequest.setIdRequerimiento("DESAPRUEBA2019012700021842");
		saldoCuentaHeaderRequest.setIpCliente(">127.0.0.1");
		saldoCuentaHeaderRequest.setSucursal("1325");
		saldoCuentaHeaderRequest.setTerminal("00007000");
		saldoCuentaHeaderRequest.setTimeStamp("20170302145432");
		
		obtenerSaldoParametros= new WSParametrosObtenerSaldoRequest();
		obtenerSaldoParametros.setSecuenciaOn("00033055");
		obtenerSaldoParametros.setTokenizado("%B778899000000525001^APELLIDO499/NOMBRE499    ^371212000000000579?      ");
		//workingKeyParametros.setTipo("D");
		
		saldoCuentaRequest= new WSObtenerSaldoCuentaRequest();
		saldoCuentaRequest.setHeader(saldoCuentaHeaderRequest);
		saldoCuentaRequest.setObtenerSaldoRequest(obtenerSaldoParametros);
		
		financieroValidationChain= new ValidationChainImpl();
		
		RequestMockValidation req = new RequestMockValidation();
		EntidadMockValidation ent =  new EntidadMockValidation();
		TerminalSamMockValidation ter = new TerminalSamMockValidation();
		SucursalMockValidation suc = new SucursalMockValidation();
		ServidorMockValidation ser = new ServidorMockValidation();
		AgenteMockValidation agt	= new AgenteMockValidation();
		DispositivoPosMockValidation dis = new DispositivoPosMockValidation();
		
		entidadService= new RedLinkGenericServiceImpl<Entidad>();
		ent.setEntidadService(entidadService);
			
		agt.setNextValidation(dis);
		ser.setNextValidation(agt);
		suc.setNextValidation(ser);
		ter.setNextValidation(suc);
		ent.setNextValidation(ter);
		req.setNextValidation(ent);
		
		financieroValidationChain.setInitialValidation(req);
		
		modelMapperService = new ModelMapperServiceImpl();
		wsFinancieroService.setModelMapperService(modelMapperService);
			
		wsFinancieroService.setFinancieroValidationChain(financieroValidationChain);
		financieroService.setTrxFinancieroService(trxFinancieroService);
		financieroService.setTandemAccountService(new TandemAccountMOCKServiceImpl());

		//TandemAccountService tandemAccountService;
		
		wsFinancieroService.setFinancieroService(financieroService);
		MockitoAnnotations.initMocks(wsFinancieroService);
		financieroService.setModelMapperService(modelMapperService);
		financieroService.setPrefijoOpService(prefijoOpService);
		
		WSObtenerSaldoCuentaResponse response= wsFinancieroService.obtenerSaldo(saldoCuentaRequest);
		Assert.assertEquals(response.getHeader().getCodigoRespuesta(), "FINA-000");
	}
	
	@Test
	public void obtenerSaldoTest02(){
		saldoCuentaHeaderRequest = new WSHeaderRequest();
		saldoCuentaHeaderRequest.setEntidad("0945");
		saldoCuentaHeaderRequest.setIdentDispositivo("000-326-904-231");
		saldoCuentaHeaderRequest.setIdentTerminal("PEI00000000022");
		saldoCuentaHeaderRequest.setIdRequerimiento("DESAPRUEBA2019012700021842");
		//saldoCuentaHeaderRequest.setIpCliente(">127.0.0.XXXXXX");
		saldoCuentaHeaderRequest.setSucursal("1325");
		saldoCuentaHeaderRequest.setTerminal("00007000");
		saldoCuentaHeaderRequest.setTimeStamp("20170302145432");
		
		obtenerSaldoParametros= new WSParametrosObtenerSaldoRequest();
		obtenerSaldoParametros.setSecuenciaOn("00033055");
		obtenerSaldoParametros.setTokenizado("%B778899000000525001^APELLIDO499/NOMBRE499    ^371212000000000579?      ");
		
		saldoCuentaRequest= new WSObtenerSaldoCuentaRequest();
		saldoCuentaRequest.setHeader(saldoCuentaHeaderRequest);
		saldoCuentaRequest.setObtenerSaldoRequest(obtenerSaldoParametros);
		
		financieroValidationChain= new ValidationChainImpl();
		
		RequestMockValidation req = new RequestMockValidation();
		EntidadMockValidation ent =  new EntidadMockValidation();
		TerminalSamMockValidation ter = new TerminalSamMockValidation();
		SucursalMockValidation suc = new SucursalMockValidation();
		ServidorMockValidation ser = new ServidorMockValidation();
		AgenteMockValidation agt	= new AgenteMockValidation();
		DispositivoPosMockValidation dis = new DispositivoPosMockValidation();
		
		entidadService= new RedLinkGenericServiceImpl<Entidad>();
		ent.setEntidadService(entidadService);
			
		agt.setNextValidation(dis);
		ser.setNextValidation(agt);
		suc.setNextValidation(ser);
		ter.setNextValidation(suc);
		ent.setNextValidation(ter);
		req.setNextValidation(ent);
		
		financieroValidationChain.setInitialValidation(req);
		
		modelMapperService = new ModelMapperServiceImpl();
		wsFinancieroService.setModelMapperService(modelMapperService);
			
		wsFinancieroService.setFinancieroValidationChain(financieroValidationChain);
		financieroService.setTrxFinancieroService(trxFinancieroService);
		financieroService.setTandemAccountService(new TandemAccountMOCKServiceImpl());

		//TandemAccountService tandemAccountService;
		
		wsFinancieroService.setFinancieroService(financieroService);
		MockitoAnnotations.initMocks(wsFinancieroService);
		financieroService.setModelMapperService(modelMapperService);
		financieroService.setPrefijoOpService(prefijoOpService);
		
		WSObtenerSaldoCuentaResponse response= wsFinancieroService.obtenerSaldo(saldoCuentaRequest);
		Assert.assertEquals(response.getHeader().getCodigoRespuesta(), "FINA-142");
	}
	
	
	
}
