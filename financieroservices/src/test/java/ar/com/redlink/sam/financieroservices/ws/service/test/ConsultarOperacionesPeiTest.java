package ar.com.redlink.sam.financieroservices.ws.service.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;


import ar.com.redlink.framework.services.crud.impl.RedLinkGenericServiceImpl;
import ar.com.redlink.sam.financieroservices.entity.Entidad;
import ar.com.redlink.sam.financieroservices.service.TrxFinancieroService;
import ar.com.redlink.sam.financieroservices.service.impl.ModelMapperServiceImpl;
import ar.com.redlink.sam.financieroservices.service.impl.PrefijoOpServiceImpl;
import ar.com.redlink.sam.financieroservices.service.impl.test.FinancieroServiceMockImpl;
import ar.com.redlink.sam.financieroservices.service.impl.validation.ValidationChainImpl;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.AgenteMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.DispositivoPosMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.EntidadMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.RequestMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.ServidorMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.SucursalMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.TerminalSamMockValidation;
import ar.com.redlink.sam.financieroservices.ws.request.WSConsultarOperacionesPEIRequest;

import ar.com.redlink.sam.financieroservices.ws.request.base.WSHeaderRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosConsultarOperacionesPEIRequest;
import ar.com.redlink.sam.financieroservices.ws.response.WSConsultarOperacionesPEIResponse;
import ar.com.redlink.sam.financieroservices.ws.service.impl.WSFinancieroServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class ConsultarOperacionesPeiTest {
	
	private ValidationChainImpl financieroValidationChain;
	private RedLinkGenericServiceImpl<Entidad> entidadService;
	private ModelMapperServiceImpl modelMapperService;
	
	@InjectMocks
	WSFinancieroServiceImpl wsFinancieroService= new WSFinancieroServiceImpl();
	@InjectMocks
	FinancieroServiceMockImpl financieroService= new FinancieroServiceMockImpl();

	PrefijoOpServiceImpl prefijoOpService = new PrefijoOpServiceImpl(); 

	@Mock
	private TrxFinancieroService trxFinancieroService;
	
	WSConsultarOperacionesPEIRequest request;
	WSHeaderRequest headerRequest;
	WSParametrosConsultarOperacionesPEIRequest consultaParametros;
	
	@Test
	public void consultaOperacionesPeiTest01() {
		headerRequest = new WSHeaderRequest();
		headerRequest.setEntidad("0945");
		headerRequest.setIdentDispositivo("000-326-904-231");
		headerRequest.setIdentTerminal("PEI00000000022");
		headerRequest.setIdRequerimiento("DESAPRUEBA2019012700021842");
		headerRequest.setIpCliente(">127.0.0.1");
		headerRequest.setSucursal("1325");
		headerRequest.setTerminal("00007000");
		headerRequest.setTimeStamp("20170302145432");
		
		consultaParametros = new WSParametrosConsultarOperacionesPEIRequest();
		consultaParametros.setSecuenciaOn("00005641");
		consultaParametros.setTrack2(";501028006001764002=491212000000000502?");
		consultaParametros.setFechadesde("01/12/2018");
		consultaParametros.setFechahasta("17/12/2018");
		
		request= new WSConsultarOperacionesPEIRequest();
		request.setHeader(headerRequest);
		request.setConsultarOperacionesPEIRequest(consultaParametros);
		
		//request.setEnrolamientoDataRequest(consultaParametros); 
		
		financieroValidationChain= new ValidationChainImpl();
		
		RequestMockValidation req = new RequestMockValidation();
		EntidadMockValidation ent =  new EntidadMockValidation();
		TerminalSamMockValidation ter = new TerminalSamMockValidation();
		SucursalMockValidation suc = new SucursalMockValidation();
		ServidorMockValidation ser = new ServidorMockValidation();
		AgenteMockValidation agt	= new AgenteMockValidation();
		DispositivoPosMockValidation dis = new DispositivoPosMockValidation();
		
		entidadService= new RedLinkGenericServiceImpl<Entidad>();
		ent.setEntidadService(entidadService);
		
		agt.setNextValidation(dis);
		ser.setNextValidation(agt);
		suc.setNextValidation(ser);
		ter.setNextValidation(suc);
		ent.setNextValidation(ter);
		req.setNextValidation(ent);
		
		financieroValidationChain.setInitialValidation(req);
		
		modelMapperService = new ModelMapperServiceImpl();
		wsFinancieroService.setModelMapperService(modelMapperService);
			
		wsFinancieroService.setFinancieroValidationChain(financieroValidationChain);
		financieroService.setTrxFinancieroService(trxFinancieroService);
		
		wsFinancieroService.setFinancieroService(financieroService);
		MockitoAnnotations.initMocks(wsFinancieroService);
		financieroService.setModelMapperService(modelMapperService);
		financieroService.setPrefijoOpService(prefijoOpService);
		//WSObtenerWorkingKeyResponse response= wsFinancieroService.obtenerWorkingKey(workingKeyrequest);
		
		WSConsultarOperacionesPEIResponse response= wsFinancieroService.consultarOperacionesPEI(request);
		
		Assert.assertEquals(response.getHeader().getCodigoRespuesta(), "FINA-000");
	}
	
	@Test
	public void consultaOperacionesPeiTest02() {
		headerRequest = new WSHeaderRequest();
		headerRequest.setEntidad("0945");
		headerRequest.setIdentDispositivo("000-326-904-231");
		headerRequest.setIdentTerminal("PEI00000000022");
		headerRequest.setIdRequerimiento("DESAPRUEBA2019012700021842");
		headerRequest.setIpCliente(">127.0.0.1");
		headerRequest.setSucursal("1325");
		headerRequest.setTerminal("00007000");
		headerRequest.setTimeStamp("20170302145432");
		
		consultaParametros = new WSParametrosConsultarOperacionesPEIRequest();
		consultaParametros.setSecuenciaOn("00005641");
		//consultaParametros.setTrack2(";501028006001764002=491212000000000502?");
		consultaParametros.setFechadesde("01/12/2018");
		consultaParametros.setFechahasta("01/12/2018");
		
		request= new WSConsultarOperacionesPEIRequest();
		request.setHeader(headerRequest);
		request.setConsultarOperacionesPEIRequest(consultaParametros);
		
		//request.setEnrolamientoDataRequest(consultaParametros); 
		
		financieroValidationChain= new ValidationChainImpl();
		
		RequestMockValidation req = new RequestMockValidation();
		EntidadMockValidation ent =  new EntidadMockValidation();
		TerminalSamMockValidation ter = new TerminalSamMockValidation();
		SucursalMockValidation suc = new SucursalMockValidation();
		ServidorMockValidation ser = new ServidorMockValidation();
		AgenteMockValidation agt	= new AgenteMockValidation();
		DispositivoPosMockValidation dis = new DispositivoPosMockValidation();
		
		entidadService= new RedLinkGenericServiceImpl<Entidad>();
		ent.setEntidadService(entidadService);
		
		agt.setNextValidation(dis);
		ser.setNextValidation(agt);
		suc.setNextValidation(ser);
		ter.setNextValidation(suc);
		ent.setNextValidation(ter);
		req.setNextValidation(ent);
		
		financieroValidationChain.setInitialValidation(req);
		
		modelMapperService = new ModelMapperServiceImpl();
		wsFinancieroService.setModelMapperService(modelMapperService);
			
		wsFinancieroService.setFinancieroValidationChain(financieroValidationChain);
		financieroService.setTrxFinancieroService(trxFinancieroService);
		
		wsFinancieroService.setFinancieroService(financieroService);
		MockitoAnnotations.initMocks(wsFinancieroService);
		financieroService.setModelMapperService(modelMapperService);
		financieroService.setPrefijoOpService(prefijoOpService);
		//WSObtenerWorkingKeyResponse response= wsFinancieroService.obtenerWorkingKey(workingKeyrequest);
		
		WSConsultarOperacionesPEIResponse response= wsFinancieroService.consultarOperacionesPEI(request);
		
		Assert.assertEquals(response.getHeader().getCodigoRespuesta(), "FINA-152");
	}
	
	
}
