package ar.com.redlink.sam.financieroservices.ws.service.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import ar.com.redlink.framework.services.crud.impl.RedLinkGenericServiceImpl;
import ar.com.redlink.sam.financieroservices.entity.Entidad;
import ar.com.redlink.sam.financieroservices.service.TrxFinancieroService;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.impl.ModelMapperServiceImpl;
import ar.com.redlink.sam.financieroservices.service.impl.test.FinancieroServiceMockImpl;
import ar.com.redlink.sam.financieroservices.service.impl.validation.ValidationChainImpl;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.AgenteMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.DispositivoPosMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.EntidadMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.RequestMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.ServidorMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.SucursalMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.TerminalSamMockValidation;
import ar.com.redlink.sam.financieroservices.ws.request.WSEnrolamientoRequest;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSHeaderRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosEnrolamientoRequest;
import ar.com.redlink.sam.financieroservices.ws.response.WSEnrolamientoResponse;
import ar.com.redlink.sam.financieroservices.ws.service.impl.WSFinancieroServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class EnrolamientoTest {
	
	private ValidationChainImpl financieroValidationChain;
	private RedLinkGenericServiceImpl<Entidad> entidadService;
	private ModelMapperServiceImpl modelMapperService;

	@InjectMocks
	WSFinancieroServiceImpl wsFinancieroService= new WSFinancieroServiceImpl();
	
	FinancieroServiceMockImpl financieroService= new FinancieroServiceMockImpl();
	
	@Mock
	private TrxFinancieroService trxFinancieroService;
	
	WSEnrolamientoRequest request;
	WSHeaderRequest headerRequest;
	WSParametrosEnrolamientoRequest enrolamientoParametros;
	
	
	@Test
	public void enrolamientoTest01() throws FinancieroValidationException{
		
		headerRequest = new WSHeaderRequest();
		headerRequest.setEntidad("0945");
		headerRequest.setIdentDispositivo("000-326-904-231");
		headerRequest.setIdentTerminal("PEI00000000022");
		headerRequest.setIdRequerimiento("DESAPRUEBA2019012700021842");
		headerRequest.setIpCliente(">127.0.0.1");
		headerRequest.setSucursal("1325");
		headerRequest.setTerminal("00007000");
		headerRequest.setTimeStamp("20170302145432");
		
		enrolamientoParametros = new WSParametrosEnrolamientoRequest();
		request= new WSEnrolamientoRequest();
		request.setHeader(headerRequest);
		request.setEnrolamientoDataRequest(enrolamientoParametros); 
		
		financieroValidationChain= new ValidationChainImpl();
		
		RequestMockValidation req = new RequestMockValidation();
		EntidadMockValidation ent =  new EntidadMockValidation();
		TerminalSamMockValidation ter = new TerminalSamMockValidation();
		SucursalMockValidation suc = new SucursalMockValidation();
		ServidorMockValidation ser = new ServidorMockValidation();
		AgenteMockValidation agt	= new AgenteMockValidation();
		DispositivoPosMockValidation dis = new DispositivoPosMockValidation();
		
		entidadService= new RedLinkGenericServiceImpl<Entidad>();
		ent.setEntidadService(entidadService);
			
		agt.setNextValidation(dis);
		ser.setNextValidation(agt);
		suc.setNextValidation(ser);
		ter.setNextValidation(suc);
		ent.setNextValidation(ter);
		req.setNextValidation(ent);
		
		financieroValidationChain.setInitialValidation(req);
		
		modelMapperService = new ModelMapperServiceImpl();
		wsFinancieroService.setModelMapperService(modelMapperService);
			
		wsFinancieroService.setFinancieroValidationChain(financieroValidationChain);
		financieroService.setTrxFinancieroService(trxFinancieroService);
		
		wsFinancieroService.setFinancieroService(financieroService);
		MockitoAnnotations.initMocks(wsFinancieroService);
		financieroService.setModelMapperService(modelMapperService);
		
		WSEnrolamientoResponse response= wsFinancieroService.enrolamiento(request);
		
		Assert.assertEquals(response.getHeader().getCodigoRespuesta(), "FINA-000");
		
	}
	

}
