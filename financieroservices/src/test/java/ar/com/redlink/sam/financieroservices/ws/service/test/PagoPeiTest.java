package ar.com.redlink.sam.financieroservices.ws.service.test;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import ar.com.redlink.framework.services.crud.impl.RedLinkGenericServiceImpl;
import ar.com.redlink.sam.financieroservices.entity.Entidad;
import ar.com.redlink.sam.financieroservices.service.TrxFinancieroService;
import ar.com.redlink.sam.financieroservices.service.impl.ModelMapperServiceImpl;
import ar.com.redlink.sam.financieroservices.service.impl.PrefijoOpServiceImpl;
import ar.com.redlink.sam.financieroservices.service.impl.test.FinancieroServiceMockImpl;
import ar.com.redlink.sam.financieroservices.service.impl.validation.ValidationChainImpl;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.AgenteMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.ComercioPEIMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.DispositivoPosMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.EntidadMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.RequestMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.ServidorMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.SucursalMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.TerminalPEIMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.TerminalSamMockValidation;
import ar.com.redlink.sam.financieroservices.service.validation.FSValidation;
import ar.com.redlink.sam.financieroservices.tandem.service.mock.TandemAccountMOCKServiceImpl;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarTransferenciaPEIRequest;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSHeaderRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosRealizarTransfPEIRequest;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarTransferenciaPEIResponse;
import ar.com.redlink.sam.financieroservices.ws.service.impl.WSFinancieroServiceImpl;


@RunWith(MockitoJUnitRunner.class)
public class PagoPeiTest {

	//private ApplicationContext ctx;
	
	private ValidationChainImpl financieroValidationChain;
	
	private ModelMapperServiceImpl modelMapperService;
	@Mock
	private TrxFinancieroService trxFinancieroService;
	@Mock
	private FSValidation initialValidation;
	
	private RedLinkGenericServiceImpl<Entidad> entidadService;
	
	@InjectMocks
	WSFinancieroServiceImpl wsFinancieroService= new WSFinancieroServiceImpl();
	
	FinancieroServiceMockImpl financieroService= new FinancieroServiceMockImpl();
	
	WSRealizarTransferenciaPEIRequest pagoPeiRequest;
	WSRealizarTransferenciaPEIRequest pagoPeiRequest2;
	
	WSHeaderRequest pagoPeiHaderequest;
	
	WSParametrosRealizarTransfPEIRequest pagoPeiParametros;
	
	PrefijoOpServiceImpl prefijoOpService = new PrefijoOpServiceImpl(); 
	
	@Test
	public void PagoPeiTestt01(){
		pagoPeiHaderequest = new WSHeaderRequest();
		pagoPeiHaderequest.setEntidad("0945");
		pagoPeiHaderequest.setIdentDispositivo("000-326-904-231");
		pagoPeiHaderequest.setIdentTerminal("PEI00000000022");
		pagoPeiHaderequest.setIdRequerimiento("DESAPRUEBA2019012700021842");
		pagoPeiHaderequest.setIpCliente(">127.0.0.1");
		pagoPeiHaderequest.setSucursal("1325");
		pagoPeiHaderequest.setTerminal("00007000");
		pagoPeiHaderequest.setTimeStamp("20170302145432");
		
		pagoPeiParametros= new WSParametrosRealizarTransfPEIRequest();
		//pagoPeiParametros.setSecuenciaOn("00033055");
		pagoPeiParametros.setTrack1("%B778899000000525001^APELLIDO499/NOMBRE499    ^371212000000000579?      ");
		pagoPeiParametros.setTrack2(";778899000000525001=371212000000000579?");
		pagoPeiParametros.setNumero("5001");
		pagoPeiParametros.setCodigoseguridad("367");
		pagoPeiParametros.setTitulardocumento("34004982");
		pagoPeiParametros.setPosentrymode("902");
		pagoPeiParametros.setImporte(new BigDecimal(10));
		pagoPeiParametros.setMoneda("0");
		
		pagoPeiRequest= new WSRealizarTransferenciaPEIRequest();
		pagoPeiRequest.setHeader(pagoPeiHaderequest);
		pagoPeiRequest.setRealizarTransferenciaPEIRequest(pagoPeiParametros);
		
		financieroValidationChain= new ValidationChainImpl();
		
		RequestMockValidation req = new RequestMockValidation();
		EntidadMockValidation ent =  new EntidadMockValidation();
		TerminalSamMockValidation ter = new TerminalSamMockValidation();
		SucursalMockValidation suc = new SucursalMockValidation();
		ServidorMockValidation ser = new ServidorMockValidation();
		AgenteMockValidation agt	= new AgenteMockValidation();
		DispositivoPosMockValidation dis = new DispositivoPosMockValidation();
		
		entidadService= new RedLinkGenericServiceImpl<Entidad>();
		ent.setEntidadService(entidadService);
			
		agt.setNextValidation(dis);
		ser.setNextValidation(agt);
		suc.setNextValidation(ser);
		ter.setNextValidation(suc);
		ent.setNextValidation(ter);
		req.setNextValidation(ent);
		
		financieroValidationChain.setInitialValidation(req);
		
		modelMapperService = new ModelMapperServiceImpl();
		wsFinancieroService.setModelMapperService(modelMapperService);
			
		wsFinancieroService.setFinancieroValidationChain(financieroValidationChain);
		financieroService.setTrxFinancieroService(trxFinancieroService);
		financieroService.setTandemAccountService(new TandemAccountMOCKServiceImpl());
		
		financieroService.setComercioPEIValidation(new ComercioPEIMockValidation());
		financieroService.setTerminalPEIValidation( new TerminalPEIMockValidation());
		
		wsFinancieroService.setFinancieroService(financieroService);
		MockitoAnnotations.initMocks(wsFinancieroService);
		financieroService.setModelMapperService(modelMapperService);
		
		WSRealizarTransferenciaPEIResponse response= wsFinancieroService.realizarPagoPEI(pagoPeiRequest);
		
		Assert.assertEquals(response.getHeader().getCodigoRespuesta(), "FINA-156");
	}
	
	@Test
	public void PagoPeiTest02(){
		pagoPeiHaderequest = new WSHeaderRequest();
		pagoPeiHaderequest.setEntidad("0945");
		pagoPeiHaderequest.setIdentDispositivo("000-326-904-231");
		pagoPeiHaderequest.setIdentTerminal("PEI00000000022");
		pagoPeiHaderequest.setIdRequerimiento("DESAPRUEBA2019012700021842");
		pagoPeiHaderequest.setIpCliente(">127.0.0.1");
		pagoPeiHaderequest.setSucursal("1325");
		pagoPeiHaderequest.setTerminal("00007000");
		pagoPeiHaderequest.setTimeStamp("20170302145432");
		
		pagoPeiParametros= new WSParametrosRealizarTransfPEIRequest();
		pagoPeiParametros.setSecuenciaOn("00033055");
		pagoPeiParametros.setTrack1("%B778899000000525001^APELLIDO499/NOMBRE499    ^371212000000000579?      ");
		pagoPeiParametros.setTrack2(";778899000000525001=371212000000000579?");
		pagoPeiParametros.setNumero("5001");
		pagoPeiParametros.setCodigoseguridad("367");
		pagoPeiParametros.setTitulardocumento("34004982");
		pagoPeiParametros.setPosentrymode("902");
		pagoPeiParametros.setImporte(new BigDecimal(10));
		pagoPeiParametros.setMoneda("0");
		
		pagoPeiRequest2= new WSRealizarTransferenciaPEIRequest();
		pagoPeiRequest2.setHeader(pagoPeiHaderequest);
		pagoPeiRequest2.setRealizarTransferenciaPEIRequest(pagoPeiParametros);
		
		financieroValidationChain= new ValidationChainImpl();
		
		RequestMockValidation req = new RequestMockValidation();
		EntidadMockValidation ent =  new EntidadMockValidation();
		TerminalSamMockValidation ter = new TerminalSamMockValidation();
		SucursalMockValidation suc = new SucursalMockValidation();
		ServidorMockValidation ser = new ServidorMockValidation();
		AgenteMockValidation agt	= new AgenteMockValidation();
		DispositivoPosMockValidation dis = new DispositivoPosMockValidation();
		
		entidadService= new RedLinkGenericServiceImpl<Entidad>();
		ent.setEntidadService(entidadService);
			
		agt.setNextValidation(dis);
		ser.setNextValidation(agt);
		suc.setNextValidation(ser);
		ter.setNextValidation(suc);
		ent.setNextValidation(ter);
		req.setNextValidation(ent);
		
		financieroValidationChain.setInitialValidation(req);
		
		modelMapperService = new ModelMapperServiceImpl();
		wsFinancieroService.setModelMapperService(modelMapperService);
			
		wsFinancieroService.setFinancieroValidationChain(financieroValidationChain);
		financieroService.setTrxFinancieroService(trxFinancieroService);
		financieroService.setTandemAccountService(new TandemAccountMOCKServiceImpl());
		
		financieroService.setComercioPEIValidation(new ComercioPEIMockValidation());
		financieroService.setTerminalPEIValidation( new TerminalPEIMockValidation());
		
		wsFinancieroService.setFinancieroService(financieroService);
		MockitoAnnotations.initMocks(wsFinancieroService);
		financieroService.setModelMapperService(modelMapperService);
		
		WSRealizarTransferenciaPEIResponse response2= wsFinancieroService.realizarPagoPEI(pagoPeiRequest2);
		
		//Assert.assertEquals("FINA-000", "FINA-000");
		Assert.assertEquals(response2.getHeader().getCodigoRespuesta(), "FINA-000");
	}
	
}
