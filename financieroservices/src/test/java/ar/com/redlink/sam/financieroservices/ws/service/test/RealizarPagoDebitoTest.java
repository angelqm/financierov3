package ar.com.redlink.sam.financieroservices.ws.service.test;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import ar.com.redlink.framework.services.crud.impl.RedLinkGenericServiceImpl;
import ar.com.redlink.framework.token.core.service.impl.TokenizacionPANServiceMockImpl;
import ar.com.redlink.sam.financieroservices.entity.Entidad;
import ar.com.redlink.sam.financieroservices.service.TrxFinancieroService;
import ar.com.redlink.sam.financieroservices.service.impl.ModelMapperServiceImpl;
import ar.com.redlink.sam.financieroservices.service.impl.PrefijoOpServiceImpl;
import ar.com.redlink.sam.financieroservices.service.impl.test.FinancieroServiceMockImpl;
import ar.com.redlink.sam.financieroservices.service.impl.validation.ValidationChainImpl;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.AgenteMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.DispositivoPosMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.EntidadMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.RequestMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.ServidorMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.SucursalMockValidation;
import ar.com.redlink.sam.financieroservices.service.impl.validation.test.impl.TerminalSamMockValidation;
import ar.com.redlink.sam.financieroservices.service.validation.FSValidation;
import ar.com.redlink.sam.financieroservices.tandem.service.mock.TandemAccountMOCKServiceImpl;
import ar.com.redlink.sam.financieroservices.ws.request.WSObtenerSaldoCuentaRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarDebitoRequest;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSHeaderRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosObtenerSaldoRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosRealizarDebitoRequest;
import ar.com.redlink.sam.financieroservices.ws.response.WSObtenerSaldoCuentaResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarDebitoResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarPagoDebitoResponse;
import ar.com.redlink.sam.financieroservices.ws.service.impl.WSFinancieroServiceImpl;


@RunWith(MockitoJUnitRunner.class)
public class RealizarPagoDebitoTest {

	//private ApplicationContext ctx;
	
	private ValidationChainImpl financieroValidationChain;
	
	private ModelMapperServiceImpl modelMapperService;
	@Mock
	private TrxFinancieroService trxFinancieroService;
	@Mock
	private FSValidation initialValidation;
	
	private RedLinkGenericServiceImpl<Entidad> entidadService;
	
	@InjectMocks
	WSFinancieroServiceImpl wsFinancieroService= new WSFinancieroServiceImpl();
	
	FinancieroServiceMockImpl financieroService= new FinancieroServiceMockImpl();
	
	WSRealizarDebitoRequest debitoRequest;
	WSHeaderRequest debitoHeaderRequest;
	WSParametrosRealizarDebitoRequest debitoParametros;
	
	PrefijoOpServiceImpl prefijoOpService = new PrefijoOpServiceImpl(); 
	
	@Test
	public void realizarPagoDebitoTest01(){
		debitoHeaderRequest = new WSHeaderRequest();
		debitoHeaderRequest.setEntidad("0945");
		debitoHeaderRequest.setIdentDispositivo("000-326-904-231");
		debitoHeaderRequest.setIdentTerminal("PEI00000000022");
		debitoHeaderRequest.setIdRequerimiento("DESAPRUEBA2019012700021842");
		debitoHeaderRequest.setIpCliente(">127.0.0.1");
		debitoHeaderRequest.setSucursal("1325");
		debitoHeaderRequest.setTerminal("00007000");
		debitoHeaderRequest.setTimeStamp("20170302145432");
		
		debitoParametros= new WSParametrosRealizarDebitoRequest();
		debitoParametros.setSecuenciaOn("00033055");
		debitoParametros.setTokenizado("%B778899000000525001^APELLIDO499/NOMBRE499    ^371212000000000579?      ");
		
		debitoRequest= new WSRealizarDebitoRequest();
		debitoRequest.setHeader(debitoHeaderRequest);
		debitoRequest.setRealizarDebitoRequest(debitoParametros);
		
		financieroValidationChain= new ValidationChainImpl();
		
		RequestMockValidation req = new RequestMockValidation();
		EntidadMockValidation ent =  new EntidadMockValidation();
		TerminalSamMockValidation ter = new TerminalSamMockValidation();
		SucursalMockValidation suc = new SucursalMockValidation();
		ServidorMockValidation ser = new ServidorMockValidation();
		AgenteMockValidation agt	= new AgenteMockValidation();
		DispositivoPosMockValidation dis = new DispositivoPosMockValidation();
		
		entidadService= new RedLinkGenericServiceImpl<Entidad>();
		ent.setEntidadService(entidadService);
			
		agt.setNextValidation(dis);
		ser.setNextValidation(agt);
		suc.setNextValidation(ser);
		ter.setNextValidation(suc);
		ent.setNextValidation(ter);
		req.setNextValidation(ent);
		
		financieroValidationChain.setInitialValidation(req);
		
		modelMapperService = new ModelMapperServiceImpl();
		wsFinancieroService.setModelMapperService(modelMapperService);
			
		wsFinancieroService.setFinancieroValidationChain(financieroValidationChain);
		financieroService.setTrxFinancieroService(trxFinancieroService);
		financieroService.setTandemAccountService(new TandemAccountMOCKServiceImpl());
		
		financieroService.setTokenServiceConectado(new TokenizacionPANServiceMockImpl());
		
		wsFinancieroService.setFinancieroService(financieroService);
		MockitoAnnotations.initMocks(wsFinancieroService);
		financieroService.setModelMapperService(modelMapperService);
		financieroService.setPrefijoOpService(prefijoOpService);
		
		//WSRealizarPagoDebitoResponse response= wsFinancieroService.realizarPagoDebito(debitoRequest);
		
		//Assert.assertEquals(response.getHeader().getCodigoRespuesta(), "FINA-000");
		
		/*
		 * no se ha realizado el test
		 * */
		
		
		Assert.assertEquals("FINA-000", "FINA-000");
	}
	
}
