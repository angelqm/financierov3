/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  03/02/2015 - 22:07:22
 */

package ar.com.redlink.sam.financieroservices.tandem.msg.callback;

import ar.com.link.connector.ServiceMessage;

/**
 * Callback para la transaccion de consulta y extraccion.
 * 
 * @author aguerrea
 * 
 */
public class ConsultaExtraccionMsgCallback extends FinancieroMsgCallback {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7026339789582111533L;

	/**
	 * Constructor default.
	 */
	public ConsultaExtraccionMsgCallback() {
		super();
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.tandem.msg.callback.FinancieroMsgCallback#handleAndTransformResponse(ar.com.link.connector.ServiceMessage)
	 */
	@Override
	protected Object handleAndTransformResponse(ServiceMessage arg0) {
		return super.handleAndTransformResponse(arg0);
	}

}
