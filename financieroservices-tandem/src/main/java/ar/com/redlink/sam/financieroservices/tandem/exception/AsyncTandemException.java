package ar.com.redlink.sam.financieroservices.tandem.exception;

import ar.com.redlink.sam.financieroservices.tandem.constant.ErrorTandem;

/**
 * Excepcion para requests asincronicos a Tandem.
 * 
 * @author RL
 * 
 */
public class AsyncTandemException extends TandemException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5329178019413683624L;

	/**
	 * Constructor default.
	 * 
	 * @param numeroTerminal
	 * @param numeroSecuencia
	 */
	public AsyncTandemException(String numeroTerminal, String numeroSecuencia) {
		super(ErrorTandem.TimeOutSolicitudAsincronica.codigo,
				ErrorTandem.TimeOutSolicitudAsincronica.descripcion,
				numeroTerminal, numeroSecuencia);
	}
}
