/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  08/04/2015 - 22:18:12
 */

package ar.com.redlink.sam.financieroservices.tandem.msg.callback;

import ar.com.link.connector.ServiceMessage;

/**
 * Callback para la operacion de extraccion en cuotas.
 * 
 * @author aguerrea
 * 
 */
public class ExtraccionCuotasMsgCallback extends FinancieroMsgCallback {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4281568250509652437L;

	/**
	 * Constructor default.
	 */
	public ExtraccionCuotasMsgCallback() {
		super();
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.tandem.msg.callback.FinancieroMsgCallback#handleAndTransformResponse(ar.com.link.connector.ServiceMessage)
	 */
	@Override
	protected Object handleAndTransformResponse(ServiceMessage arg0) {
		return super.handleAndTransformResponse(arg0);
	}
}
