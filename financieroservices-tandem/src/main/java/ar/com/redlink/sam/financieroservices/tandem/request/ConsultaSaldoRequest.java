/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  15/03/2015 - 11:52:43
 */

package ar.com.redlink.sam.financieroservices.tandem.request;

import ar.com.redlink.sam.financieroservices.tandem.request.base.SAMTandemRequest;

/**
 * Representacion de una peticion de consulta de saldo (30).
 * 
 * @author aguerrea
 * 
 */
public class ConsultaSaldoRequest extends SAMTandemRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2721459371152796584L;

	private static final String FORMAT_NAME = "30_REQUEST";
	private static final String CONSULTA_SALDO_TRX_CODE = "30";

	/**
	 * Constructor con seteo de valores default.
	 */
	public ConsultaSaldoRequest() {
		this.setTranCde(CONSULTA_SALDO_TRX_CODE);
		this.setFromAcct("0");
		this.setFromAcctTyp("0");
		this.setToAcct("0");
		this.setToAcctTyp("0");
		this.setImporte("0");
		this.setFiller(" ");
		this.setTipoDep(" ");
		this.setClearing(" ");
		this.setTrnData(" ");
		this.setDatosTrx(" ");
		this.setTarjeta(" ");
		this.setCodMon("0");
		this.setTrack2(" ");
		this.setTermName(" ");
	}

	/**
	 * @return the formatName
	 */
	@Override
	public String getFormatName() {
		return FORMAT_NAME;
	}
}
