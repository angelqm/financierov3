/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  15/03/2015 - 11:48:03
 */

package ar.com.redlink.sam.financieroservices.tandem.request;

import ar.com.redlink.sam.financieroservices.tandem.request.base.SAMTandemRequest;

/**
 * Representacion de una peticion de anulacion de extraccion (11).
 * 
 * @author aguerrea
 * 
 */
public class AnulacionExtraccionRequest extends SAMTandemRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5168990878758764681L;

	private static final String FORMAT_NAME = "11_REQUEST";
	private static final String EXT_TRX_CODE = "10";

	/**
	 * Constructor con seteo de valores default.
	 */
	public AnulacionExtraccionRequest() {
		this.setTranCde(EXT_TRX_CODE);
		this.setToAcct("0");
		this.setToAcctTyp("0");
		this.setFiller(" ");
		this.setTipoDep(" ");
		this.setTrnData(" ");
		this.setDatosTrx(" ");
		this.setTarjeta(" ");
		this.setCodMon("0");
		this.setTrack2(" ");
		this.setTermName(" ");
		
		this.setDescEnte(" ");		
		this.setOrigTime(" ");
		this.setOrigPostDat(" ");
		
	}

	/**
	 * @return the formatName
	 */
	@Override
	public String getFormatName() {
		return FORMAT_NAME;
	}
}
