package ar.com.redlink.sam.financieroservices.tandem.request;

import ar.com.redlink.sam.financieroservices.tandem.request.base.SAMTandemRequest;

public class DesencriptarRequest extends SAMTandemRequest {


	/**
	 * 
	 */
	private static final long serialVersionUID = -7360552887633498793L;
	private static final String FORMAT_NAME = "62D_REQUEST";
	private static final String CONSULTA_EXT_TRX_CODE = "62";
	private static final String TIPO_ENCRIPCION = "2";
	private static final String TIPO_DEP = "D";
	
	public DesencriptarRequest() {
		this.setTranCde(CONSULTA_EXT_TRX_CODE);
		this.setFromAcct("0");
		this.setFromAcctTyp("0");
		this.setToAcct("0");
		this.setToAcctTyp("0");
		this.setImporte("0");
		this.setFiller(" ");
		this.setTipoDep(TIPO_DEP);
		this.setClearing(" ");
		this.setTrnData(" ");
		this.setDatosTrx(" ");
		this.setTarjeta(" ");
		this.setCodMon("0");
		this.setTrack2(" ");
		this.setTermName(" ");
		this.setTipoEncripcion(TIPO_ENCRIPCION);
		
	}
	
	@Override
	public String getFormatName() {
		return FORMAT_NAME;
	}
	
}
