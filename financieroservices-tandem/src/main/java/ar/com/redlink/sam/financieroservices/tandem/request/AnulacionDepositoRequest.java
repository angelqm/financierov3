/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  15/03/2015 - 11:48:03
 */

package ar.com.redlink.sam.financieroservices.tandem.request;

import ar.com.redlink.sam.financieroservices.tandem.request.base.SAMTandemRequest;

/**
 * Representacion de una peticion de anulacion de deposito (20).
 * 
 * @author aguerrea
 * 
 */
public class AnulacionDepositoRequest extends SAMTandemRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5168990878758764681L;

	private static final String FORMAT_NAME = "21_REQUEST";
	/*
	 * AQM Se cambia EXT_TRX_CODE =21 POR 20 
	 * */
	private static final String EXT_TRX_CODE = "20";
	private static final String TIPO_DEP = "E";

	private String plazo;

	/**
	 * Constructor default.
	 * 
	 */
	public AnulacionDepositoRequest() {
		this.setTermName(" ");
		/*
		 * AQM
		 * */
		this.setTranCde(EXT_TRX_CODE);
		this.setToAcct("0");
		this.setToAcctTyp("0");
		this.setFiller(" ");
		this.setTipoDep(TIPO_DEP);
		this.setTrnData(" ");
		this.setDatosTrx(" ");
		this.setTarjeta(" ");
		this.setCodMon("0");
		this.setTrack2(" ");
		this.setFromAcct("0");
		this.setFromAcctTyp("0");		
	}

	/**
	 * @return the plazo
	 */
	public String getPlazo() {
		return plazo;
	}

	/**
	 * @param plazo
	 *            the plazo to set
	 */
	public void setPlazo(String plazo) {
		this.plazo = plazo;
	}

	/**
	 * @return the formatName
	 */
	@Override
	public String getFormatName() {
		return FORMAT_NAME;
	}
}
