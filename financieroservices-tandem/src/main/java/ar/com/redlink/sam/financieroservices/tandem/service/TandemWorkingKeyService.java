/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  16/03/2015 - 23:16:57
 */

package ar.com.redlink.sam.financieroservices.tandem.service;

import ar.com.redlink.framework.services.RedLinkService;
import ar.com.redlink.sam.financieroservices.dto.request.WorkingKeyRequestDTO;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.tandem.response.WorkingKeyResponse;

/**
 * Interface que wrappea las interacciones con Tandem para la negociacion de una
 * WorkingKey valida.
 * 
 * @author aguerrea
 * 
 */
public interface TandemWorkingKeyService extends RedLinkService {

	/**
	 * Recibe la parametria correspondiente a la peticion de working key y cursa
	 * el pedido a Tandem.
	 * 
	 * @param workingKey
	 * @return Wrapper de WorkingKey.
	 * @throws FinancieroException
	 */
	public WorkingKeyResponse getWorkingKey(WorkingKeyRequestDTO workingKey) throws FinancieroException;
}
