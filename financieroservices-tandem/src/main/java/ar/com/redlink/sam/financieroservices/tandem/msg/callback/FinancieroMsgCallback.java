/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  03/02/2015 - 22:06:15
 */

package ar.com.redlink.sam.financieroservices.tandem.msg.callback;

import ar.com.link.connector.ServiceMessage;
import ar.com.link.connector.TransformingCallback;
import ar.com.link.logging.services.LoggingContext;

/**
 * Callback base para los callbacks de las transacciones vinculadas al modulo
 * financiero.
 * 
 * @author aguerrea
 * 
 */
public class FinancieroMsgCallback extends TransformingCallback {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9060785356616577801L;

	/**
	 * @see ar.com.link.connector.ResponseCallback#restoreContext(java.lang.String)
	 */
	@Override
	public void restoreContext(String arg0) {

	}

	/**
	 * @see ar.com.link.connector.ResponseCallback#storeContext(java.lang.String,
	 *      ar.com.link.logging.services.LoggingContext)
	 */
	@Override
	public void storeContext(String arg0, LoggingContext arg1) {

	}

	/**
	 * @see ar.com.link.connector.ResponseCallback#waitResponseFor(ar.com.link.connector.ServiceMessage)
	 */
	@Override
	public void waitResponseFor(ServiceMessage arg0) {

	}

	/**
	 * @see ar.com.link.connector.TransformingCallback#handleAndTransformResponse(ar.com.link.connector.ServiceMessage)
	 */
	@Override
	protected Object handleAndTransformResponse(ServiceMessage arg0) {
		return arg0;
	}

}
