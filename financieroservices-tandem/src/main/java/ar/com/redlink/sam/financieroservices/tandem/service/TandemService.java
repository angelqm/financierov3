package ar.com.redlink.sam.financieroservices.tandem.service;

import ar.com.link.connector.ResponseCallback;
import ar.com.link.connector.ServiceResponse;
import ar.com.link.connector.tandem.messages.TandemRequest;
import ar.com.link.connector.tandem.messages.TandemResponse;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.tandem.exception.AsyncTandemException;
import ar.com.redlink.sam.financieroservices.tandem.exception.SyncTandemTimeOutException;

/**
 * Abstraccion de cliente de Tandem. (Extracto de SoatServices)
 * 
 * @author RL
 * 
 */
public interface TandemService {

	/**
	 * Envio asincronico de mensajes de Tandem.
	 * 
	 * @param request
	 * @param callback
	 * @return respuesta de Tandem u error.
	 * @throws AsyncTandemException
	 */
	TandemResponse sendAsync(TandemRequest request, ResponseCallback callback)
			throws AsyncTandemException;

	/**
	 * Envio sincronico de mensajes de Tandem.
	 * 
	 * @param request
	 * @param callback
	 * @return respuesta de Tandem u error.
	 * @throws SyncTandemTimeOutException
	 */
	TandemResponse sendSync(TandemRequest request, ResponseCallback callback)
			throws SyncTandemTimeOutException;

	/**
	 * Manejo manual de peticiones. Permite agregar un request.
	 * 
	 * @param correlationId
	 * @param callback
	 */
	void addExternalRequest(String correlationId, ResponseCallback callback);

	/**
	 * Manejo manual de peticiones. Permite agregar un response.
	 * 
	 * @param response
	 */
	void addExternalResponse(TandemResponse response);

	/**
	 * Permite pollear en busca de un request con un correlationId especifico.
	 * 
	 * @param correlationId
	 * @return response asociado
	 */
	public TandemResponse pollForResponse(String correlationId);

	/**
	 * Permite obtener el callback asociado a la respuesta de Tandem.
	 * 
	 * @param serviceResponse
	 * @return
	 * @throws FinancieroException 
	 */
	Object getResponse(ServiceResponse serviceResponse) throws FinancieroException;

}
