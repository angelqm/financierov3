/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  12/02/2015 - 18:47:25
 */

package ar.com.redlink.sam.financieroservices.tandem.util.format.exception;

/**
 * Excepcion arrojada por el componente de procesamiento de archivos format.
 * 
 * @author aguerrea
 * 
 */
public class FormatProcessorException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5952525390647578880L;

	/**
	 * Constructor que recibe el mensaje.
	 * 
	 * @param msg
	 */
	public FormatProcessorException(String msg) {
		super(msg);
	}

}
