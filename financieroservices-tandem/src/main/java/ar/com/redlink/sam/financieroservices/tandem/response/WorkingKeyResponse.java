/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  03/02/2015 - 22:20:52
 */

package ar.com.redlink.sam.financieroservices.tandem.response;

import ar.com.redlink.sam.financieroservices.tandem.response.base.SAMTandemResponse;

/**
 * Representacion de una response de solicitud de Working Key de Tandem (31).
 * 
 * @author aguerrea
 * 
 */
public class WorkingKeyResponse extends SAMTandemResponse {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6414801168535720334L;
	private String termPref;
	private String termFiid;
	private String termName;
	private String filler;
	private String tarjeta;
	private String amt1;
	private String codRta;
	private String seqNum;
	private String postDat;
	private String origDat;
	private String origTime;
	private String rcvdInstId;
	private String claveComm;
	private String etx;

	/**
	 * @return the termPref
	 */
	public String getTermPref() {
		return termPref;
	}

	/**
	 * @param termPref
	 *            the termPref to set
	 */
	public void setTermPref(String termPref) {
		this.termPref = termPref;
	}

	/**
	 * @return the termFiid
	 */
	public String getTermFiid() {
		return termFiid;
	}

	/**
	 * @param termFiid
	 *            the termFiid to set
	 */
	public void setTermFiid(String termFiid) {
		this.termFiid = termFiid;
	}

	/**
	 * @return the termName
	 */
	public String getTermName() {
		return termName;
	}

	/**
	 * @param termName
	 *            the termName to set
	 */
	public void setTermName(String termName) {
		this.termName = termName;
	}

	/**
	 * @return the filler
	 */
	public String getFiller() {
		return filler;
	}

	/**
	 * @param filler
	 *            the filler to set
	 */
	public void setFiller(String filler) {
		this.filler = filler;
	}

	/**
	 * @return the tarjeta
	 */
	public String getTarjeta() {
		return tarjeta;
	}

	/**
	 * @param tarjeta
	 *            the tarjeta to set
	 */
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	/**
	 * @return the amt1
	 */
	public String getAmt1() {
		return amt1;
	}

	/**
	 * @param amt1
	 *            the amt1 to set
	 */
	public void setAmt1(String amt1) {
		this.amt1 = amt1;
	}

	/**
	 * @return the codRta
	 */
	public String getCodRta() {
		return codRta;
	}

	/**
	 * @param codRta
	 *            the codRta to set
	 */
	public void setCodRta(String codRta) {
		this.codRta = codRta;
	}

	/**
	 * @return the seqNum
	 */
	public String getSeqNum() {
		return seqNum;
	}

	/**
	 * @param seqNum
	 *            the seqNum to set
	 */
	public void setSeqNum(String seqNum) {
		this.seqNum = seqNum;
	}

	/**
	 * @return the postDat
	 */
	public String getPostDat() {
		return postDat;
	}

	/**
	 * @param postDat
	 *            the postDat to set
	 */
	public void setPostDat(String postDat) {
		this.postDat = postDat;
	}

	/**
	 * @return the origDat
	 */
	public String getOrigDat() {
		return origDat;
	}

	/**
	 * @param origDat
	 *            the origDat to set
	 */
	public void setOrigDat(String origDat) {
		this.origDat = origDat;
	}

	/**
	 * @return the origTime
	 */
	public String getOrigTime() {
		return origTime;
	}

	/**
	 * @param origTime
	 *            the origTime to set
	 */
	public void setOrigTime(String origTime) {
		this.origTime = origTime;
	}

	/**
	 * @return the rcvdInstId
	 */
	public String getRcvdInstId() {
		return rcvdInstId;
	}

	/**
	 * @param rcvdInstId
	 *            the rcvdInstId to set
	 */
	public void setRcvdInstId(String rcvdInstId) {
		this.rcvdInstId = rcvdInstId;
	}

	/**
	 * @return the claveComm
	 */
	public String getClaveComm() {
		return claveComm;
	}

	/**
	 * @param claveComm
	 *            the claveComm to set
	 */
	public void setClaveComm(String claveComm) {
		this.claveComm = claveComm;
	}

	/**
	 * @return the etx
	 */
	public String getEtx() {
		return etx;
	}

	/**
	 * @param etx
	 *            the etx to set
	 */
	public void setEtx(String etx) {
		this.etx = etx;
	}
}
