/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  15/03/2015 - 11:35:07
 */

package ar.com.redlink.sam.financieroservices.tandem.request;

/**
 * Representacion de una peticion de extraccion (10).
 * 
 * @author Diego Lucchelli
 * 
 */
public class PagoDebitoRequest extends ExtraccionRequest {

	private static final long serialVersionUID = 2175682529666878973L;
	private static final String FORMAT_NAME = "10D_REQUEST";
	private static final String TIPO_DEP = "D";
	
	private String descEnte;
	
	/**
	 * Constructor con seteo de valores default.
	 */
	public PagoDebitoRequest() {
		this.setTipoDep(TIPO_DEP);
		this.setDescEnte(" ");
	}

	/**
	 * @return the formatName
	 */
	@Override
	public String getFormatName() {
		return FORMAT_NAME;
	}

	public String getDescEnte() {
		return descEnte;
	}

	public void setDescEnte(String descEnte) {
		this.descEnte = descEnte;
	}
}
