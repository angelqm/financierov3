/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  16/03/2015 - 23:37:20
 */

package ar.com.redlink.sam.financieroservices.tandem.service.impl;

import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.log4j.Logger;

import ar.com.link.connector.tandem.messages.TandemRequest;
import ar.com.redlink.sam.financieroservices.tandem.service.TandemMessagePopulatorService;
import ar.com.redlink.sam.financieroservices.tandem.util.padding.TandemMessagePadder;

/**
 * Implementacion del servicio de formateo de mensajes de Tandem.
 * 
 * @author aguerrea
 * 
 */
public class TandemMessagePopulatorServiceImpl implements
		TandemMessagePopulatorService {

	private static final Logger LOGGER = Logger
			.getLogger(TandemMessagePopulatorServiceImpl.class);
	private static final String NULL_TAG = "<null>";
	private static final String SEQ_NUM = "seqNum";
	private TandemMessagePadder tandemMessagePadder;
	private Map<String, String> defaultValues;

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemMessagePopulatorService#populateMessage(java.lang.Object)
	 */
	public void populateMessage(Object request) {
		// Buildea un String con todos los atributos del objeto.
		ReflectionToStringBuilder stringBuilder = new ReflectionToStringBuilder(
				(Object) request);

		// Se parsean y splittean en Key/Value.
		String[] keyValues = StringUtils.substringsBetween(
				stringBuilder.toString(), "[", "]")[0].split(",");
		for (String keyValue : keyValues) {

			// Se les setea un valor por defecto.
			String[] kvArray = keyValue.split("=");
			String value = "0";

			if (!kvArray[0].equals(SEQ_NUM)) {
				if (kvArray.length > 1) {
					value = kvArray[1];
				}
				if (value == null || NULL_TAG.equals(value)) {
					value = "0";
				}
				if (null != defaultValues.get(kvArray[0])) {
					value = defaultValues.get(kvArray[0]);
				}
				// }
				try {
					// Se puebla cada atributo obtenido en el objeto recibido.
					BeanUtils.setProperty(
							request,
							kvArray[0],
							getTandemMessagePadder().padMessage(
									((TandemRequest) request).getFormatName(),
									kvArray[0], value));
				} catch (NullPointerException e) {
					LOGGER.debug("Se ignora una NPE en el procesamiento de campos del request a Tandem.");
				} catch (Exception e) {
					LOGGER.error(
							"Error inesperado al procesar los campos del request a Tandem: "
									+ e.getMessage(), e);
				}
			}
		}
	}

	/**
	 * @return the tandemMessagePadder
	 */
	public TandemMessagePadder getTandemMessagePadder() {
		return tandemMessagePadder;
	}

	/**
	 * @param tandemMessagePadder
	 *            the tandemMessagePadder to set
	 */
	public void setTandemMessagePadder(TandemMessagePadder tandemMessagePadder) {
		this.tandemMessagePadder = tandemMessagePadder;
	}

	/**
	 * @return the defaultValues
	 */
	public Map<String, String> getDefaultValues() {
		return defaultValues;
	}

	/**
	 * @param defaultValues
	 *            the defaultValues to set
	 */
	public void setDefaultValues(Map<String, String> defaultValues) {
		this.defaultValues = defaultValues;
	}
}
