/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  10/02/2015 - 20:41:39
 */

package ar.com.redlink.sam.financieroservices.tandem.util.padding;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.InitializingBean;

import ar.com.redlink.sam.financieroservices.tandem.util.format.FormatProcessor;
import ar.com.redlink.sam.financieroservices.tandem.util.format.TandemFinancieroFormatProcessor;

/**
 * Componente responsable de leer las definiciones de formatos y generar la
 * estructura de padding correspondiente.
 * 
 * @author aguerrea
 * 
 */
public class TandemMessagePadder implements InitializingBean {

	private Map<String, Map<String, Integer>> attributesMap;
	private String formatsPath;
	private FormatProcessor<Map<String, Map<String, Integer>>> formatProcessor;

	/**
	 * Recibe el nombre de formato, atributo y valor para obtener el tamano
	 * correspondiente y hacer el padding.
	 * 
	 * @param formatName
	 * @param attributeName
	 * @param value
	 * @return value paddeado.
	 */
	public String padMessage(String formatName, String attributeName,
			String value) {
		String padChar = " ";

		if (value.startsWith("0") && value.length() == 1) {
			padChar = "0";
		}

		Integer size = attributesMap.get(formatName).get(attributeName);

		return StringUtils.rightPad(value, size, padChar);
	}

	/**
	 * @return the attributesMap
	 */
	public Map<String, Map<String, Integer>> getAttributesMap() {
		return attributesMap;
	}

	/**
	 * @param attributesMap
	 *            the attributesMap to set
	 */
	public void setAttributesMap(Map<String, Map<String, Integer>> attributesMap) {
		this.attributesMap = attributesMap;
	}

	/**
	 * @return the formatsPath
	 */
	public String getFormatsPath() {
		return formatsPath;
	}

	/**
	 * @param formatsPath
	 *            the formatsPath to set
	 */
	public void setFormatsPath(String formatsPath) {
		this.formatsPath = formatsPath;
	}

	/**
	 * @return the formatProcessor
	 */
	public FormatProcessor<Map<String, Map<String, Integer>>> getFormatProcessor() {
		return formatProcessor;
	}

	/**
	 * @param formatProcessor
	 *            the formatProcessor to set
	 */
	public void setFormatProcessor(
			FormatProcessor<Map<String, Map<String, Integer>>> formatProcessor) {
		this.formatProcessor = formatProcessor;
	}

	/**
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		// Se procesan los XML al instanciar el bean.
		this.formatProcessor = new TandemFinancieroFormatProcessor();
		attributesMap = formatProcessor.process(this.getFormatsPath());
	}
}
