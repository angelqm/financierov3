/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  03/02/2015 - 22:28:38
 */

package ar.com.redlink.sam.financieroservices.tandem.response;

import ar.com.link.connector.tandem.messages.TandemErrorMessage;

/**
 * Representacion de un error generico de Tandem asociado a las transacciones
 * del modulo financiero.
 * 
 * @author aguerrea
 * 
 */
public class FinancieroErrorResponse extends TandemErrorMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5875264450288082408L;

	private String termPref;
	private String termName;
	private String rcvdInstId;

	/**
	 * @return the termPref
	 */
	public String getTermPref() {
		return termPref;
	}

	/**
	 * @param termPref
	 *            the termPref to set
	 */
	public void setTermPref(String termPref) {
		this.termPref = termPref;
	}

	/**
	 * @return the termName
	 */
	public String getTermName() {
		return termName;
	}

	/**
	 * @param termName
	 *            the termName to set
	 */
	public void setTermName(String termName) {
		this.termName = termName;
	}

	/**
	 * @return the rcvdInstId
	 */
	public String getRcvdInstId() {
		return rcvdInstId;
	}

	/**
	 * @param rcvdInstId
	 *            the rcvdInstId to set
	 */
	public void setRcvdInstId(String rcvdInstId) {
		this.rcvdInstId = rcvdInstId;
	}

	/**
	 * 
	 * @see ar.com.link.connector.tandem.messages.TandemRequest#getCorrelationId()
	 */
	@Override
	public String getCorrelationId() {
		correlationId = this.getTranCde() + this.getTermId();

		return correlationId;
	}
}
