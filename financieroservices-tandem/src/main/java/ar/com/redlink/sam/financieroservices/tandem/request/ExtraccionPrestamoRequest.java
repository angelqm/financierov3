/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  15/03/2015 - 11:35:07
 */

package ar.com.redlink.sam.financieroservices.tandem.request;

import ar.com.redlink.sam.financieroservices.tandem.request.base.SAMTandemRequest;

/**
 * Representacion de una peticion de extraccion de prestamo (10). Se genera su
 * representacion por si eventualmente se requiere modificar el mensaje
 * unicamente para este tipo de extraccion.
 * 
 * @author aguerrea
 * 
 */
public class ExtraccionPrestamoRequest extends SAMTandemRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7410003912966212846L;

	private static final String FORMAT_NAME = "10P_REQUEST";
	
	/**
	 * @return the formatName
	 */
	@Override
	public String getFormatName() {
		return FORMAT_NAME;
	}
}
