/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  15/03/2015 - 11:35:07
 */

package ar.com.redlink.sam.financieroservices.tandem.request;

import ar.com.redlink.sam.financieroservices.tandem.request.base.SAMTandemRequest;

/**
 * Representacion de una peticion de extraccion (10).
 * 
 * @author aguerrea
 * 
 */
public class ExtraccionRequest extends SAMTandemRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7410003912966212846L;

	private static final String FORMAT_NAME = "10_REQUEST";
	private static final String EXT_TRX_CODE = "10";

	private String filler2;
	private String tipoExtraccion;
	private String cantCuotas;

	/**
	 * Constructor con seteo de valores default.
	 */
	public ExtraccionRequest() {
		this.setTranCde(EXT_TRX_CODE);
		this.setToAcct("0");
		this.setToAcctTyp("0");
		this.setFiller(" ");
		this.setTipoDep(" ");
		this.setTrnData(" ");
		this.setDatosTrx(" ");
		this.setTarjeta(" ");
		this.setCodMon("0");
		this.setTrack2(" ");
		this.setFiller2(" ");
		this.setTipoExtraccion(" ");
		this.setCantCuotas(" ");
		this.setTermName(" ");
	}

	/**
	 * @return the formatName
	 */
	@Override
	public String getFormatName() {
		return FORMAT_NAME;
	}

	/**
	 * @return the filler2
	 */
	public String getFiller2() {
		return filler2;
	}

	/**
	 * @param filler2
	 *            the filler2 to set
	 */
	public void setFiller2(String filler2) {
		this.filler2 = filler2;
	}

	/**
	 * @return the tipoExtraccion
	 */
	public String getTipoExtraccion() {
		return tipoExtraccion;
	}

	/**
	 * @param tipoExtraccion
	 *            the tipoExtraccion to set
	 */
	public void setTipoExtraccion(String tipoExtraccion) {
		this.tipoExtraccion = tipoExtraccion;
	}

	/**
	 * @return the cantCuotas
	 */
	public String getCantCuotas() {
		return cantCuotas;
	}

	/**
	 * @param cantCuotas
	 *            the cantCuotas to set
	 */
	public void setCantCuotas(String cantCuotas) {
		this.cantCuotas = cantCuotas;
	}
}
