/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  15/03/2015 - 11:48:03
 */

package ar.com.redlink.sam.financieroservices.tandem.request;

import ar.com.redlink.sam.financieroservices.tandem.request.base.SAMTandemRequest;

/**
 * Representacion de una peticion de anulacion de extraccion de prestamo.
 * 
 * @author aguerrea
 * 
 */
public class AnulacionExtraccionPrestamoRequest extends SAMTandemRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5168990878758764681L;

	private static final String FORMAT_NAME = "11_REQUEST";
//	private String termPref;
//	private String termName;
//	private String tarjeta;
//	private String codSuperv;
//
//	private String codMon;
//	private String tipoDep;
//	private String trnData;
//	private String cvc2Informado;
//	private String cvc2;
//	private String tipoEncripcion;
//	private String track2Encriptado;
//	private String trck2CifradoLen;
//	private String nroTerminal;
//
//	/**
//	 * @return the termPref
//	 */
//	public String getTermPref() {
//		return termPref;
//	}
//
//	/**
//	 * @param termPref
//	 *            the termPref to set
//	 */
//	public void setTermPref(String termPref) {
//		this.termPref = termPref;
//	}
//
//	/**
//	 * @return the termName
//	 */
//	public String getTermName() {
//		return termName;
//	}
//
//	/**
//	 * @param termName
//	 *            the termName to set
//	 */
//	public void setTermName(String termName) {
//		this.termName = termName;
//	}
//
//	/**
//	 * @return the tarjeta
//	 */
//	public String getTarjeta() {
//		return tarjeta;
//	}
//
//	/**
//	 * @param tarjeta
//	 *            the tarjeta to set
//	 */
//	public void setTarjeta(String tarjeta) {
//		this.tarjeta = tarjeta;
//	}
//
//	/**
//	 * @return the codSuperv
//	 */
//	public String getCodSuperv() {
//		return codSuperv;
//	}
//
//	/**
//	 * @param codSuperv
//	 *            the codSuperv to set
//	 */
//	public void setCodSuperv(String codSuperv) {
//		this.codSuperv = codSuperv;
//	}
//
//	/**
//	 * @return the codMon
//	 */
//	public String getCodMon() {
//		return codMon;
//	}
//
//	/**
//	 * @param codMon
//	 *            the codMon to set
//	 */
//	public void setCodMon(String codMon) {
//		this.codMon = codMon;
//	}
//
//	/**
//	 * @return the tipoDep
//	 */
//	public String getTipoDep() {
//		return tipoDep;
//	}
//
//	/**
//	 * @param tipoDep
//	 *            the tipoDep to set
//	 */
//	public void setTipoDep(String tipoDep) {
//		this.tipoDep = tipoDep;
//	}
//
//	/**
//	 * @return the trnData
//	 */
//	public String getTrnData() {
//		return trnData;
//	}
//
//	/**
//	 * @param trnData
//	 *            the trnData to set
//	 */
//	public void setTrnData(String trnData) {
//		this.trnData = trnData;
//	}
//
//	/**
//	 * @return the cvc2Informado
//	 */
//	public String getCvc2Informado() {
//		return cvc2Informado;
//	}
//
//	/**
//	 * @param cvc2Informado
//	 *            the cvc2Informado to set
//	 */
//	public void setCvc2Informado(String cvc2Informado) {
//		this.cvc2Informado = cvc2Informado;
//	}
//
//	/**
//	 * @return the cvc2
//	 */
//	public String getCvc2() {
//		return cvc2;
//	}
//
//	/**
//	 * @param cvc2
//	 *            the cvc2 to set
//	 */
//	public void setCvc2(String cvc2) {
//		this.cvc2 = cvc2;
//	}
//
//	/**
//	 * @return the tipoEncripcion
//	 */
//	public String getTipoEncripcion() {
//		return tipoEncripcion;
//	}
//
//	/**
//	 * @param tipoEncripcion
//	 *            the tipoEncripcion to set
//	 */
//	public void setTipoEncripcion(String tipoEncripcion) {
//		this.tipoEncripcion = tipoEncripcion;
//	}
//
//	/**
//	 * @return the track2Encriptado
//	 */
//	public String getTrack2Encriptado() {
//		return track2Encriptado;
//	}
//
//	/**
//	 * @param track2Encriptado
//	 *            the track2Encriptado to set
//	 */
//	public void setTrack2Encriptado(String track2Encriptado) {
//		this.track2Encriptado = track2Encriptado;
//	}
//
//	/**
//	 * @return the trck2CifradoLen
//	 */
//	public String getTrck2CifradoLen() {
//		return trck2CifradoLen;
//	}
//
//	/**
//	 * @param trck2CifradoLen
//	 *            the trck2CifradoLen to set
//	 */
//	public void setTrck2CifradoLen(String trck2CifradoLen) {
//		this.trck2CifradoLen = trck2CifradoLen;
//	}
//
//	/**
//	 * @return the nroTerminal
//	 */
//	public String getNroTerminal() {
//		return nroTerminal;
//	}
//
//	/**
//	 * @param nroTerminal
//	 *            the nroTerminal to set
//	 */
//	public void setNroTerminal(String nroTerminal) {
//		this.nroTerminal = nroTerminal;
//	}

	/**
	 * @return the formatName
	 */
	@Override
	public String getFormatName() {
		return FORMAT_NAME;
	}
}
