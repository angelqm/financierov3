package ar.com.redlink.sam.financieroservices.tandem.exception;

import ar.com.redlink.sam.financieroservices.tandem.constant.ErrorTandem;

/**
 * Excepcion de Timeout para requests sincronicos a Tandem. (Extracto de
 * SoatServices)
 * 
 * @author RL
 * 
 */
public class SyncTandemTimeOutException extends TandemException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2688734453439039178L;

	/**
	 * Constructor default.
	 * 
	 * @param numeroTerminal
	 * @param numeroSecuencia
	 */
	public SyncTandemTimeOutException(String numeroTerminal,
			String numeroSecuencia) {
		super(ErrorTandem.TimeOutSolicitudSincronica.codigo,
				ErrorTandem.TimeOutSolicitudSincronica.descripcion,
				numeroTerminal, numeroSecuencia);
	}
}
