/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  16/03/2015 - 18:30:00
 */

package ar.com.redlink.sam.financieroservices.tandem.service.mock;

import ar.com.redlink.sam.financieroservices.dto.request.AnularDebitoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularDepositoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionCuotasRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionPrestamoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.BalanceRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ExtraccionRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDebitoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDepositoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarExtraccionCuotasRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarExtraccionPrestamoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarPagoDebitoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ValidarTarjetaRequestDTO;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.tandem.response.AnulacionDepositoResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.AnulacionExtraccionResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.ConsultaExtraccionResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.ConsultaSaldoResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.DepositoResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.ExtraccionResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.TransferenciaResponse;
import ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService;


/**
 * MOCK MOCK MOCK
 * 
 */
public class TandemAccountMOCKServiceImpl implements TandemAccountService {
	
	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#getBalance(ar.com.redlink.sam.financieroservices.dto.request.BalanceRequestDTO)
	 */
	@Override
	public ConsultaSaldoResponse getBalance(BalanceRequestDTO balance)
			throws FinancieroException {
		ConsultaSaldoResponse response = new ConsultaSaldoResponse();
		response.setDineroDisponible("100");
		response.setSaldo("150");
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#validateCard(ar.com.redlink.sam.financieroservices.dto.request.ValidarTarjetaRequestDTO)
	 */
	@Override
	public ConsultaExtraccionResponse validateCard(
			ValidarTarjetaRequestDTO validarTarjeta) throws FinancieroException {
		ConsultaExtraccionResponse response = new ConsultaExtraccionResponse();
		return response;
	}
	
	/**
	 * 
	 */
	@Override
	public ConsultaExtraccionResponse decryptCard(	ValidarTarjetaRequestDTO validarTarjeta) throws FinancieroException {
		ConsultaExtraccionResponse tandemResponse = new ConsultaExtraccionResponse();
		//tandemResponse.setTrack2(";99999999999999=180112000000000726?");
		//tandemResponse.setTrack2("501054301444654010=491212003721000008?");		
		//tandemResponse.setTrack1("B4100820000018004^WISZNIOVSKY VALERIA     ^220812110000        00595000000?");
		//tandemResponse.setTrack2(";4100820000018004=22081215950000000000?");	
		//tandemResponse.setTrack2(";4513779850692006\u003d18051217640000000000? ");
		//tandemResponse.setTrack2("4100820000000002=22081212180000000000?");		
		
		
		//tandemResponse.setTrack1("%B4062900238734006^TREBINO FIGUEROA JO00000^180612110000        00525000000");
		
		tandemResponse.setData("                                                                                                                                                                         ");
		tandemResponse.setTrack2(";501041921007365005=180112000000000726?");  

		
		
		tandemResponse.setSeqNum(String.valueOf(Integer.valueOf(validarTarjeta.getSecuenciaOn())+1));		
		return tandemResponse;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#doDebit(ar.com.redlink.sam.financieroservices.dto.request.RealizarDebitoRequestDTO)
	 */
	@Override
	public ExtraccionResponse doDebit(RealizarDebitoRequestDTO realizarDebitoRequestDTO) throws FinancieroException {
		ExtraccionResponse response = new ExtraccionResponse();
		return response;
	}

	
	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#doDebit(ar.com.redlink.sam.financieroservices.dto.request.RealizarDebitoRequestDTO)
	 */
	@Override
	public ExtraccionResponse doDebitPay(	RealizarPagoDebitoRequestDTO realizarPagoDebitoRequestDTO)
			throws FinancieroException {
		ExtraccionResponse response = new ExtraccionResponse();
		return response;
	}
		
	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#rollbackDebit(ar.com.redlink.sam.financieroservices.dto.request.AnularDebitoRequestDTO)
	 */
	@Override
	public AnulacionExtraccionResponse rollbackDebit(
			AnularDebitoRequestDTO anularDebitoRequestDTO)
			throws FinancieroException {
		AnulacionExtraccionResponse response = new AnulacionExtraccionResponse();
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#doWithdraw(ar.com.redlink.sam.financieroservices.dto.request.ExtraccionRequestDTO)
	 */
	@Override
	public ExtraccionResponse doWithdraw(ExtraccionRequestDTO extraccion)
			throws FinancieroException {
		ExtraccionResponse response = new ExtraccionResponse();
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#rollbackWithdraw(ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionRequestDTO)
	 */
	@Override
	public AnulacionExtraccionResponse rollbackWithdraw(
			AnularExtraccionRequestDTO anularExtraccion)
			throws FinancieroException {
		AnulacionExtraccionResponse response = new AnulacionExtraccionResponse();
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#doDeposit(ar.com.redlink.sam.financieroservices.dto.request.RealizarDepositoRequestDTO)
	 */
	@Override
	public DepositoResponse doDeposit(
			RealizarDepositoRequestDTO realizarDepositoRequestDTO)
			throws FinancieroException {
		DepositoResponse response = new DepositoResponse();
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#rollbackDeposit(ar.com.redlink.sam.financieroservices.dto.request.AnularDepositoRequestDTO)
	 */
	@Override
	public AnulacionDepositoResponse rollbackDeposit(
			AnularDepositoRequestDTO anularDepositoRequestDTO)
			throws FinancieroException {
		AnulacionDepositoResponse response = new AnulacionDepositoResponse();
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#doTransfer(ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaRequestDTO)
	 */
	@Override
	public TransferenciaResponse doTransfer(
			RealizarTransferenciaRequestDTO realizarTransferenciaRequestDTO)
			throws FinancieroException {
		TransferenciaResponse response = new TransferenciaResponse();
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#doLoanWithdraw(ar.com.redlink.sam.financieroservices.dto.request.RealizarExtraccionPrestamoRequestDTO)
	 */
	@Override
	public ExtraccionResponse doLoanWithdraw(
			RealizarExtraccionPrestamoRequestDTO realizarExtraccionPrestamoRequestDTO)
			throws FinancieroException {
		ExtraccionResponse response = new ExtraccionResponse();
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#rollbackLoanWithdraw(ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionPrestamoRequestDTO)
	 */
	@Override
	public AnulacionExtraccionResponse rollbackLoanWithdraw(
			AnularExtraccionPrestamoRequestDTO anularExtraccionPrestamoRequestDTO)
			throws FinancieroException {
		AnulacionExtraccionResponse response = new AnulacionExtraccionResponse();
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#doSharesWithdraw(ar.com.redlink.sam.financieroservices.dto.request.RealizarExtraccionCuotasRequestDTO)
	 */
	@Override
	public ExtraccionResponse doSharesWithdraw(
			RealizarExtraccionCuotasRequestDTO realizarExtraccionCuotasRequestDTO)
			throws FinancieroException {
		ExtraccionResponse response = new ExtraccionResponse();
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#rollbackSharesWithdraw(ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionCuotasRequestDTO)
	 */
	@Override
	public AnulacionExtraccionResponse rollbackSharesWithdraw(
			AnularExtraccionCuotasRequestDTO anularExtraccionCuotasRequestDTO)
			throws FinancieroException {
		AnulacionExtraccionResponse response = new AnulacionExtraccionResponse();
		return response;
	}

	@Override
	public AnulacionExtraccionResponse rollbackDebitPay(AnularDebitoRequestDTO anularDebitoRequestDTO)
			throws FinancieroException {
		return null;
	}

}
