/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  06/04/2015 - 15:48:34
 */

package ar.com.redlink.sam.financieroservices.tandem.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;

import ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.tandem.response.FinancieroErrorResponse;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA507;

/**
 * Componente para el manejo de errores de Tandem. Transforma los mensajes en
 * excepciones y las arroja.
 * 
 * @author aguerrea
 * 
 */
public class TandemErrorHandler implements InitializingBean {

	private static final Logger LOGGER = Logger
			.getLogger(TandemErrorHandler.class);
	private Properties properties;
	private String messagesFilePath;

	/**
	 * Maneja un {@link FinancieroErrorResponse} y arroja una excepcion
	 * detallada.
	 * 
	 * @param errorResponse
	 */
	public void handle(FinancieroErrorResponse errorResponse) {
		String errorCode = errorResponse.getCodRta();

		String message = (String) properties.get(errorCode);

		FinancieroException financieroException = new FinancieroException(FINA507.getCode(), String.format(FINA507.getMsg(), errorCode));
		
		if (null != message) {
			String[] messages = message.split("|");
			FinancieroServicesMsgEnum.valueOf(messages[1]);
		}
	}

	/**
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		properties = new Properties();
		try {
			properties.load(new FileInputStream(new File(messagesFilePath)));

		} catch (FileNotFoundException e) {
			LOGGER.error("Excepcion no se encontro el archivo de mensajes", e);
		} catch (IOException e) {
			LOGGER.error("Excepcion al abrir el archivo de mensajes", e);
		}
	}

	/**
	 * @return the messagesFilePath
	 */
	public String getMessagesFilePath() {
		return messagesFilePath;
	}

	/**
	 * @param messagesFilePath
	 *            the messagesFilePath to set
	 */
	public void setMessagesFilePath(String messagesFilePath) {
		this.messagesFilePath = messagesFilePath;
	}

	/**
	 * @return the properties
	 */
	public Properties getProperties() {
		return properties;
	}

	/**
	 * @param properties
	 *            the properties to set
	 */
	public void setProperties(Properties properties) {
		this.properties = properties;
	}

}
