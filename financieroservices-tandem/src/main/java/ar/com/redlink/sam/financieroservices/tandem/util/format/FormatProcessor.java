/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  10/02/2015 - 23:49:07
 */

package ar.com.redlink.sam.financieroservices.tandem.util.format;

import ar.com.redlink.sam.financieroservices.tandem.util.format.exception.FormatProcessorException;

/**
 * Interface que define las operaciones de un processor de archivos de formato
 * de Tandem.
 * 
 * @author aguerrea
 * 
 */
public interface FormatProcessor<R> {

	/**
	 * Recibe un path para realizar el procesamiento de los archivos de formato.
	 * 
	 * @param path
	 * @return R siendo el tipo de retorno requerido.
	 */
	public R process(String path) throws FormatProcessorException;
}
