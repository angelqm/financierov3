package ar.com.redlink.sam.financieroservices.tandem.exception;

/**
 * Excepcion base para Tandem (Extracto de SoatServices).
 * 
 * @author RL
 * 
 */
public class TandemException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String codigoError;
	private String descripcion;

	private String numeroTerminal;
	private String numeroSecuencia;

	/**
	 * Constructor default.
	 * 
	 * @param codigo
	 * @param descripcion
	 * @param numeroTerminal
	 * @param numeroSecuencia
	 */
	public TandemException(String codigo, String descripcion,
			String numeroTerminal, String numeroSecuencia) {
		super("Tandem respondio con error " + codigo + " - " + descripcion
				+ " - Numero de terminal: " + numeroTerminal
				+ " - Numero de secuencia: " + numeroSecuencia);

		this.codigoError = codigo;
		this.descripcion = descripcion;
		this.numeroTerminal = numeroTerminal;
		this.numeroSecuencia = numeroSecuencia;
	}

	/**
	 * @return the codigoError
	 */
	public String getCodigoError() {
		return codigoError;
	}

	/**
	 * @param codigoError
	 *            the codigoError to set
	 */
	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the numeroTerminal
	 */
	public String getNumeroTerminal() {
		return numeroTerminal;
	}

	/**
	 * @param numeroTerminal
	 *            the numeroTerminal to set
	 */
	public void setNumeroTerminal(String numeroTerminal) {
		this.numeroTerminal = numeroTerminal;
	}

	/**
	 * @return the numeroSecuencia
	 */
	public String getNumeroSecuencia() {
		return numeroSecuencia;
	}

	/**
	 * @param numeroSecuencia
	 *            the numeroSecuencia to set
	 */
	public void setNumeroSecuencia(String numeroSecuencia) {
		this.numeroSecuencia = numeroSecuencia;
	}

}
