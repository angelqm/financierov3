/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  10/02/2015 - 23:52:57
 */

package ar.com.redlink.sam.financieroservices.tandem.util.format;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;


import org.apache.cxf.helpers.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import ar.com.redlink.sam.financieroservices.tandem.util.format.exception.FormatProcessorException;

/**
 * Implementacion para el procesamiento de formatos requerido por
 * FinancieroService.
 * 
 * @author aguerrea
 * 
 */
public class TandemFinancieroFormatProcessor implements
		FormatProcessor<Map<String, Map<String, Integer>>> {

	private static final String CLASSPATH = "classpath";
	private static final String FILE_PATTERN_WILDCARD = "*REQUEST.xml";
	private static final String FILE_PATTERN = ".+REQUEST.xml";
	private static final String LENGTH_XPATH_EXPRESSION = "/Link/parse/field/@length";
	private static final String TO_XPATH_EXPRESSION = "/Link/parse/field/copy/@to";
	private static final Logger LOGGER = Logger
			.getLogger(TandemFinancieroFormatProcessor.class);

	/**
	 * @throws FormatProcessorException
	 * @see ar.com.redlink.sam.financieroservices.tandem.util.format.FormatProcessor#process(java.lang.String)
	 */
	@Override
	public Map<String, Map<String, Integer>> process(String path)
			throws FormatProcessorException {
		Map<String, InputStream> streamsMap = new HashMap<String, InputStream>();

		if (path.contains(CLASSPATH)) {
			retrieveResourcesFromClasspath(path, streamsMap);

		} else {
			retrieveResourcesFromFileSystem(path, streamsMap);
		}

		return processXmlFiles(streamsMap);
	}

	/**
	 * Obtiene los inputStreams desde el FS.
	 * 
	 * @param path
	 * @param streamsMap
	 * @throws FormatProcessorException
	 */
	private void retrieveResourcesFromFileSystem(String path,
			Map<String, InputStream> streamsMap)
			throws FormatProcessorException {
		File dir = new File(path);

		List<File> files = (List<File>) FileUtils.getFiles(dir, FILE_PATTERN);

		for (File file : files) {
			try {
				streamsMap.put(file.getName(), new FileInputStream(file));
			} catch (FileNotFoundException e) {
				LOGGER.error("No se encontro el archivo de formato! ", e);
				throw new FormatProcessorException(
						"No se encontro el archivo de formato, se interrumpe el procesamiento.");
			}
		}
	}

	/**
	 * Obtiene los inputStreams traversando un directorio en el classpath.
	 * 
	 * @param path
	 * @param streamsMap
	 * @throws FormatProcessorException
	 */
	private void retrieveResourcesFromClasspath(String path,
			Map<String, InputStream> streamsMap)
			throws FormatProcessorException {
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		try {
			Resource[] resources = resolver.getResources(path
					+ FILE_PATTERN_WILDCARD);

			for (Resource resource : resources) {
				streamsMap.put(resource.getFilename(),
						resource.getInputStream());
			}
		} catch (IOException e) {
			LOGGER.error(
					"Excepcion al obtener los recursos desde el classpath: ", e);
			throw new FormatProcessorException(
					"Excepcion al obtener los recursos desde el classpath");
		}
	}

	/**
	 * Procesa los archivos XML de Tandem.
	 * 
	 * @param files
	 * @throws FormatProcessorException
	 * @throws ParserConfigurationException
	 */
	private Map<String, Map<String, Integer>> processXmlFiles(
			Map<String, InputStream> streams) throws FormatProcessorException {
		DocumentBuilderFactory domFactory = DocumentBuilderFactory
				.newInstance();
		domFactory.setNamespaceAware(true);
		DocumentBuilder builderXML = null;

		try {
			builderXML = domFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e1) {
			LOGGER.error("Excepcion al instanciar el Parser ", e1);
			throw new FormatProcessorException(
					"Excepcion al instanciar el Parser");
		}

		Map<String, Map<String, Integer>> resultMap = new HashMap<String, Map<String, Integer>>();

		for (Entry<String, InputStream> entry : streams.entrySet()) {
			try {
				Map<String, Integer> fileMap = new HashMap<String, Integer>();

				Document doc = builderXML.parse(entry.getValue());

				XPathFactory factory = XPathFactory.newInstance();
				XPath xpath = factory.newXPath();
				xpath.setNamespaceContext(new FormatNamespaceContext(doc));

				NodeList fieldNodes = (NodeList) xpath.evaluate(
						TO_XPATH_EXPRESSION, doc, XPathConstants.NODESET);

				NodeList lengthNodes = (NodeList) xpath.evaluate(
						LENGTH_XPATH_EXPRESSION, doc, XPathConstants.NODESET);
				// Se compara la cantidad de fields y lengths obtenidos, si
				// coinciden se hace el matcheo.
				if (fieldNodes.getLength() == lengthNodes.getLength()) {
					for (int i = 0; i < fieldNodes.getLength(); i++) {
						fileMap.put(fieldNodes.item(i).getNodeValue(), Integer
								.parseInt(lengthNodes.item(i).getNodeValue()));
					}
				}
				resultMap.put(entry.getKey().split("\\.")[0], fileMap);

			} catch (Exception e) {
				LOGGER.error("Excepcion al procesar el XML: " + e.getMessage(),
						e);
				throw new FormatProcessorException(
						"Excepcion al procesar el XML.");
			}
		}

		return resultMap;
	}
}
