/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  16/03/2015 - 12:26:01
 */

package ar.com.redlink.sam.financieroservices.tandem.request.base;

import ar.com.link.connector.tandem.messages.TandemRequest;

/**
 * Request base para las transacciones a Tandem relacionadas con SAM.
 * 
 * @author aguerrea
 * 
 */
public class SAMTandemRequest extends TandemRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6964319785893855123L;
	
	private String termPref;
	private String termName;
	private String termAdmin;
	private String tarjeta;
	private String codSuperv;
	private String codMon;
	private String tipoDep;
	private String trnData;
	private String clearing;
	private String cvc2Informado;
	private String cvc2;
	private String tipoEncripcion;
	private String track1Encriptado;
	private String trck1CifradoLen;
	private String track2Encriptado;
	private String trck2CifradoLen;
	private String nroTerminal;
	
	/*AQM*/
	private String descEnte;
	private String origTime;
	private String origPostDat;

	/**
	 * @return the termPref
	 */
	public String getTermPref() {
		return termPref;
	}

	/**
	 * @param termPref
	 *            the termPref to set
	 */
	public void setTermPref(String termPref) {
		this.termPref = termPref;
	}

	/**
	 * @return the termName
	 */
	public String getTermName() {
		return termName;
	}

	/**
	 * @param termName
	 *            the termName to set
	 */
	public void setTermName(String termName) {
		this.termName = termName;
	}

	/**
	 * @return the termAdmin
	 */
	public String getTermAdmin() {
		return termAdmin;
	}

	/**
	 * @param termAdmin
	 *            the termAdmin to set
	 */
	public void setTermAdmin(String termAdmin) {
		this.termAdmin = termAdmin;
	}

	/**
	 * @return the tarjeta
	 */
	public String getTarjeta() {
		return tarjeta;
	}

	/**
	 * @param tarjeta
	 *            the tarjeta to set
	 */
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	/**
	 * @return the codSuperv
	 */
	public String getCodSuperv() {
		return codSuperv;
	}

	/**
	 * @param codSuperv
	 *            the codSuperv to set
	 */
	public void setCodSuperv(String codSuperv) {
		this.codSuperv = codSuperv;
	}

	/**
	 * @return the codMon
	 */
	public String getCodMon() {
		return codMon;
	}

	/**
	 * @param codMon
	 *            the codMon to set
	 */
	public void setCodMon(String codMon) {
		this.codMon = codMon;
	}

	/**
	 * @return the tipoDep
	 */
	public String getTipoDep() {
		return tipoDep;
	}

	/**
	 * @param tipoDep
	 *            the tipoDep to set
	 */
	public void setTipoDep(String tipoDep) {
		this.tipoDep = tipoDep;
	}

	/**
	 * @return the clearing
	 */
	public String getClearing() {
		return clearing;
	}

	/**
	 * @param clearing
	 *            the clearing to set
	 */
	public void setClearing(String clearing) {
		this.clearing = clearing;
	}

	/**
	 * @return the trnData
	 */
	public String getTrnData() {
		return trnData;
	}

	/**
	 * @param trnData
	 *            the trnData to set
	 */
	public void setTrnData(String trnData) {
		this.trnData = trnData;
	}

	/**
	 * @return the cvc2Informado
	 */
	public String getCvc2Informado() {
		return cvc2Informado;
	}

	/**
	 * @param cvc2Informado
	 *            the cvc2Informado to set
	 */
	public void setCvc2Informado(String cvc2Informado) {
		this.cvc2Informado = cvc2Informado;
	}

	/**
	 * @return the cvc2
	 */
	public String getCvc2() {
		return cvc2;
	}

	/**
	 * @param cvc2
	 *            the cvc2 to set
	 */
	public void setCvc2(String cvc2) {
		this.cvc2 = cvc2;
	}

	/**
	 * @return the tipoEncripcion
	 */
	public String getTipoEncripcion() {
		return tipoEncripcion;
	}

	/**
	 * @param tipoEncripcion
	 *            the tipoEncripcion to set
	 */
	public void setTipoEncripcion(String tipoEncripcion) {
		this.tipoEncripcion = tipoEncripcion;
	}

	/**
	 * @return the track2Encriptado
	 */
	public String getTrack2Encriptado() {
		return track2Encriptado;
	}
	
	/**
	 * @return the track1Encriptado
	 */
	public String getTrack1Encriptado() {
		return track1Encriptado;
	}

	/**
	 * @param track1Encriptado the track1Encriptado to set
	 */
	public void setTrack1Encriptado(String track1Encriptado) {
		this.track1Encriptado = track1Encriptado;
	}

	/**
	 * @return the trck1CifradoLen
	 */
	public String getTrck1CifradoLen() {
		return trck1CifradoLen;
	}

	/**
	 * @param trck1CifradoLen the trck1CifradoLen to set
	 */
	public void setTrck1CifradoLen(String trck1CifradoLen) {
		this.trck1CifradoLen = trck1CifradoLen;
	}

	/**
	 * @param track2Encriptado
	 *            the track2Encriptado to set
	 */
	public void setTrack2Encriptado(String track2Encriptado) {
		this.track2Encriptado = track2Encriptado;
	}

	/**
	 * @return the trck2CifradoLen
	 */
	public String getTrck2CifradoLen() {
		return trck2CifradoLen;
	}

	/**
	 * @param trck2CifradoLen
	 *            the trck2CifradoLen to set
	 */
	public void setTrck2CifradoLen(String trck2CifradoLen) {
		this.trck2CifradoLen = trck2CifradoLen;
	}

	/**
	 * @return the nroTerminal
	 */
	public String getNroTerminal() {
		return nroTerminal;
	}

	/**
	 * @param nroTerminal
	 *            the nroTerminal to set
	 */
	public void setNroTerminal(String nroTerminal) {
		this.nroTerminal = nroTerminal;
	}

	/**
	 * 
	 * @see ar.com.link.connector.tandem.messages.TandemRequest#getCorrelationId()
	 */
	@Override
	public String getCorrelationId() {
		correlationId = this.getTranCde() + this.getTermId();

		return correlationId;
	}

	/**
	 * @return the descEnte
	 */
	public String getDescEnte() {
		return descEnte;
	}

	/**
	 * @param descEnte the descEnte to set
	 */
	public void setDescEnte(String descEnte) {
		this.descEnte = descEnte;
	}

	/**
	 * @return the origTime
	 */
	public String getOrigTime() {
		return origTime;
	}

	/**
	 * @param origTime the origTime to set
	 */
	public void setOrigTime(String origTime) {
		this.origTime = origTime;
	}

	/**
	 * @return the origPostDat
	 */
	public String getOrigPostDat() {
		return origPostDat;
	}

	/**
	 * @param origPostDat the origPostDat to set
	 */
	public void setOrigPostDat(String origPostDat) {
		this.origPostDat = origPostDat;
	}
	
}
