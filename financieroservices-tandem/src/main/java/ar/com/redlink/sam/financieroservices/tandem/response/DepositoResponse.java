/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  15/03/2015 - 11:41:15
 */

package ar.com.redlink.sam.financieroservices.tandem.response;

import ar.com.redlink.sam.financieroservices.tandem.response.base.SAMTandemResponse;

/**
 * Representacion de una respuesta a la transaccion de deposito (20).
 * 
 * @author aguerrea
 * 
 */
public class DepositoResponse extends SAMTandemResponse {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3435977061279024884L;
	private String termPref;
	private String termName;
	private String filler;

	private String etx;

	/**
	 * @return the termPref
	 */
	public String getTermPref() {
		return termPref;
	}

	/**
	 * @param termPref
	 *            the termPref to set
	 */
	public void setTermPref(String termPref) {
		this.termPref = termPref;
	}

	/**
	 * @return the termName
	 */
	public String getTermName() {
		return termName;
	}

	/**
	 * @param termName
	 *            the termName to set
	 */
	public void setTermName(String termName) {
		this.termName = termName;
	}

	/**
	 * @return the filler
	 */
	public String getFiller() {
		return filler;
	}

	/**
	 * @param filler
	 *            the filler to set
	 */
	public void setFiller(String filler) {
		this.filler = filler;
	}

	/**
	 * @return the etx
	 */
	public String getEtx() {
		return etx;
	}

	/**
	 * @param etx
	 *            the etx to set
	 */
	public void setEtx(String etx) {
		this.etx = etx;
	}
}
