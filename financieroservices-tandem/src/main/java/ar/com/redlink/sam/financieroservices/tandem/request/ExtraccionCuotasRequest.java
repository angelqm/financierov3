/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  08/04/2015 - 22:18:28
 */

package ar.com.redlink.sam.financieroservices.tandem.request;

import ar.com.redlink.sam.financieroservices.tandem.request.base.SAMTandemRequest;

/**
 * Wrapper para el request a Tandem de extraccion en cuotas.
 * 
 * @author aguerrea
 * 
 */
public class ExtraccionCuotasRequest extends SAMTandemRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2855589288088608801L;
	private static final String FORMAT_NAME = "10_REQUEST";

	/**
	 * @return the formatName
	 */
	@Override
	public String getFormatName() {
		return FORMAT_NAME;
	}
}
