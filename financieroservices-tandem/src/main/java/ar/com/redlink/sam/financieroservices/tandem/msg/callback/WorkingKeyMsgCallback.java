/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  03/02/2015 - 22:09:28
 */

package ar.com.redlink.sam.financieroservices.tandem.msg.callback;

import ar.com.link.connector.ServiceMessage;

/**
 * Callback para la transaccion de obtencion de working key.
 * 
 * @author aguerrea
 * 
 */
public class WorkingKeyMsgCallback extends FinancieroMsgCallback {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5098570679214270858L;

	/**
	 * Constructor default.
	 */
	public WorkingKeyMsgCallback() {

	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.tandem.msg.callback.FinancieroMsgCallback#handleAndTransformResponse(ar.com.link.connector.ServiceMessage)
	 */
	@Override
	protected Object handleAndTransformResponse(ServiceMessage arg0) {
		return super.handleAndTransformResponse(arg0);
	}

}
