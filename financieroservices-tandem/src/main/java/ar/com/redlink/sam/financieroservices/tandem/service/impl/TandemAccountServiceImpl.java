/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  16/03/2015 - 18:30:00
 */

package ar.com.redlink.sam.financieroservices.tandem.service.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA140;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import ar.com.redlink.sam.financieroservices.dto.CuentaDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularDebitoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularDepositoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionCuotasRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionPrestamoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.BalanceRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ExtraccionRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDebitoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDepositoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarExtraccionCuotasRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarExtraccionPrestamoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarPagoDebitoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ValidarTarjetaRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.common.FinancieroAccountDTO;
import ar.com.redlink.sam.financieroservices.dto.request.common.FinancieroCommonDTO;
import ar.com.redlink.sam.financieroservices.dto.request.common.FinancieroServiceBaseRequestDTO;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.tandem.msg.callback.AnulacionDepositoMsgCallback;
import ar.com.redlink.sam.financieroservices.tandem.msg.callback.AnulacionExtraccionMsgCallback;
import ar.com.redlink.sam.financieroservices.tandem.msg.callback.ConsultaExtraccionMsgCallback;
import ar.com.redlink.sam.financieroservices.tandem.msg.callback.ConsultaSaldoMsgCallback;
import ar.com.redlink.sam.financieroservices.tandem.msg.callback.DepositoMsgCallback;
import ar.com.redlink.sam.financieroservices.tandem.msg.callback.ExtraccionMsgCallback;
import ar.com.redlink.sam.financieroservices.tandem.msg.callback.TransferenciaMsgCallback;
import ar.com.redlink.sam.financieroservices.tandem.request.AnulacionDepositoRequest;
import ar.com.redlink.sam.financieroservices.tandem.request.AnulacionExtraccionRequest;
import ar.com.redlink.sam.financieroservices.tandem.request.ConsultaExtraccionRequest;
import ar.com.redlink.sam.financieroservices.tandem.request.ConsultaSaldoRequest;
import ar.com.redlink.sam.financieroservices.tandem.request.DepositoRequest;
import ar.com.redlink.sam.financieroservices.tandem.request.DesencriptarRequest;
import ar.com.redlink.sam.financieroservices.tandem.request.ExtraccionRequest;
import ar.com.redlink.sam.financieroservices.tandem.request.PagoDebitoRequest;
import ar.com.redlink.sam.financieroservices.tandem.request.TransferenciaRequest;
import ar.com.redlink.sam.financieroservices.tandem.request.base.SAMTandemRequest;
import ar.com.redlink.sam.financieroservices.tandem.response.AnulacionDepositoResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.AnulacionExtraccionResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.ConsultaExtraccionResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.ConsultaSaldoResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.DepositoResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.ExtraccionResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.TransferenciaResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.base.SAMTandemResponse;
import ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService;

/**
 * Implementacion del servicio que maneja todo lo referido a la cuenta y sus
 * tarjetas con Tandem.
 * 
 * @author aguerrea
 * 
 */
public class TandemAccountServiceImpl extends AbstractTandemBaseService
		implements TandemAccountService {

	private static final Logger LOGGER = Logger
			.getLogger(TandemAccountServiceImpl.class);

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#getBalance(ar.com.redlink.sam.financieroservices.dto.request.BalanceRequestDTO)
	 */
	@Override
	public ConsultaSaldoResponse getBalance(BalanceRequestDTO balance)	throws FinancieroException {
		LOGGER.info("[TAN INI] getBalance");
		
		ConsultaSaldoRequest request = new ConsultaSaldoRequest();

		populateCommon(balance, request);
		request.setFromAcct(balance.getCuenta().getNumeroCuenta());
		request.setFromAcctTyp(balance.getCuenta().getTipoCuenta());

		ConsultaSaldoMsgCallback callback = new ConsultaSaldoMsgCallback();

		ConsultaSaldoResponse response = (ConsultaSaldoResponse) this
				.getTandemService().getResponse(this.formatAndSendTandemMessage(request, callback));

		LOGGER.info("[TAN FIN] getBalance");
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#validateCard(ar.com.redlink.sam.financieroservices.dto.request.ValidarTarjetaRequestDTO)
	 */
	@Override
	public ConsultaExtraccionResponse validateCard(
			ValidarTarjetaRequestDTO validarTarjeta) throws FinancieroException {
		LOGGER.info("[TAN INI] validateCard");
		ConsultaExtraccionRequest consultaExtraccionRequest = new ConsultaExtraccionRequest();

		populateCommon(validarTarjeta, consultaExtraccionRequest);
		
		ConsultaExtraccionMsgCallback callback = new ConsultaExtraccionMsgCallback();

		ConsultaExtraccionResponse response = (ConsultaExtraccionResponse) tandemService
				.getResponse(this.formatAndSendTandemMessage(
						consultaExtraccionRequest, callback));

		LOGGER.info("[TAN FIN] validateCard");
		return response;
	}
	
	/**
	 * 
	 */
	@Override
	public ConsultaExtraccionResponse decryptCard( ValidarTarjetaRequestDTO validarTarjeta) throws FinancieroException {
		LOGGER.info("[TAN INI] decryptCard");
		DesencriptarRequest desencriptarRequest = new DesencriptarRequest();

		populateCommon(validarTarjeta, desencriptarRequest);
		
		desencriptarRequest.setTipoDep("D");
		desencriptarRequest.setTipoEncripcion("2");
		
		ConsultaExtraccionMsgCallback callback = new ConsultaExtraccionMsgCallback();

		ConsultaExtraccionResponse response = (ConsultaExtraccionResponse) tandemService
				.getResponse(this.formatAndSendTandemMessage(desencriptarRequest, callback));

		LOGGER.info("[TAN FIN] decryptCard");
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#doDebit(ar.com.redlink.sam.financieroservices.dto.request.RealizarDebitoRequestDTO)
	 */
	@Override
	public ExtraccionResponse doDebit(	RealizarDebitoRequestDTO realizarDebitoRequestDTO)	throws FinancieroException {
		LOGGER.info("[TAN INI] doDebit");
		ExtraccionRequest request = new ExtraccionRequest();

		populateOperationData(realizarDebitoRequestDTO, request);
		request.setTipoDep("X");

		ExtraccionMsgCallback callback = new ExtraccionMsgCallback();

		ExtraccionResponse response = (ExtraccionResponse) tandemService.getResponse(this.formatAndSendTandemMessage(request, callback));

		LOGGER.info("[TAN FIN] doDebit");
		return response;
	}
	
	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#doDebit(ar.com.redlink.sam.financieroservices.dto.request.RealizarDebitoRequestDTO)
	 */
	@Override
	public ExtraccionResponse doDebitPay(RealizarPagoDebitoRequestDTO realizarPagoDebitoRequestDTO)	throws FinancieroException {
		LOGGER.info("[TAN INI] doDebitPay");
		PagoDebitoRequest request = new PagoDebitoRequest();

		populateOperationData(realizarPagoDebitoRequestDTO, request);

		request.setDescEnte(realizarPagoDebitoRequestDTO.getDescripcionDebito());
		
		if (realizarPagoDebitoRequestDTO.getTipoTerminalB24() != null && !realizarPagoDebitoRequestDTO.getTipoTerminalB24().isEmpty())
			request.setTermTyp(realizarPagoDebitoRequestDTO.getTipoTerminalB24());
		
		ExtraccionMsgCallback callback = new ExtraccionMsgCallback();

		ExtraccionResponse response = (ExtraccionResponse) tandemService.getResponse(this.formatAndSendTandemMessage(request, callback));

		LOGGER.info("[TAN FIN] doDebitPay");
		return response;
	}
	
	
	
	
	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#rollbackDebit(ar.com.redlink.sam.financieroservices.dto.request.AnularDebitoRequestDTO)
	 */
	@Override
	public AnulacionExtraccionResponse rollbackDebit(AnularDebitoRequestDTO anularDebitoRequestDTO)
			throws FinancieroException {
		LOGGER.info("[TAN INI] rollbackDebit");
		AnulacionExtraccionRequest request = new AnulacionExtraccionRequest();

		populateOperationData(anularDebitoRequestDTO, request);
		request.setCodSuperv("2");

		/*AQM ACA VAN LOS CAMPOS FEHCHA/HORA*/
		request.setOrigPostDat(anularDebitoRequestDTO.getFechaOn().substring(2, 8));
		request.setOrigTime(anularDebitoRequestDTO.getHoraOn());
		
		AnulacionExtraccionMsgCallback callback = new AnulacionExtraccionMsgCallback();

		AnulacionExtraccionResponse response = (AnulacionExtraccionResponse) tandemService
				.getResponse(this.formatAndSendTandemMessage(request, callback));

		this.checkSeqNum(anularDebitoRequestDTO, response);

		LOGGER.info("[TAN FIN] rollbackDebit");
		return response;
	}
	
	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#rollbackDebit(ar.com.redlink.sam.financieroservices.dto.request.AnularDebitoRequestDTO)
	 */
	@Override
	public AnulacionExtraccionResponse rollbackDebitPay(AnularDebitoRequestDTO anularDebitoRequestDTO) throws FinancieroException {
		LOGGER.info("[TAN INI] rollbackDebitPay");
		
		AnulacionExtraccionRequest request = new AnulacionExtraccionRequest();

		populateOperationData(anularDebitoRequestDTO, request);
		request.setCodSuperv("8");
		request.setTipoDep("D");
		/*AQM ACA VAN LOS CAMPOS FEHCHA/HORA*/
		request.setOrigPostDat(anularDebitoRequestDTO.getFechaOn().substring(2, 8));
		request.setOrigTime(anularDebitoRequestDTO.getHoraOn());
		
		if (anularDebitoRequestDTO.getTipoTerminalB24() != null && !anularDebitoRequestDTO.getTipoTerminalB24().isEmpty())
			request.setTermTyp(anularDebitoRequestDTO.getTipoTerminalB24());

		AnulacionExtraccionMsgCallback callback = new AnulacionExtraccionMsgCallback();

		AnulacionExtraccionResponse response = (AnulacionExtraccionResponse) tandemService.getResponse(this.formatAndSendTandemMessage(request, callback));

		this.checkSeqNum(anularDebitoRequestDTO, response);

		LOGGER.info("[TAN FIN] rollbackDebitPay");
		return response;
	}
	

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#doWithdraw(ar.com.redlink.sam.financieroservices.dto.request.ExtraccionRequestDTO)
	 */
	@Override
	public ExtraccionResponse doWithdraw(ExtraccionRequestDTO extraccion)throws FinancieroException {
		LOGGER.info("[TAN INI] doWithdraw");
		
		ExtraccionRequest request = new ExtraccionRequest();

		populateOperationData(extraccion, request);

		ExtraccionMsgCallback callback = new ExtraccionMsgCallback();
		
		if (extraccion.getTipoTerminalB24() != null && !extraccion.getTipoTerminalB24().isEmpty())
			request.setTermTyp(extraccion.getTipoTerminalB24());

		ExtraccionResponse response = (ExtraccionResponse) tandemService.getResponse(this.formatAndSendTandemMessage(request, callback));

		LOGGER.info("[TAN FIN] doWithdraw");
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#rollbackWithdraw(ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionRequestDTO)
	 */
	@Override
	public AnulacionExtraccionResponse rollbackWithdraw(AnularExtraccionRequestDTO anularExtraccion) throws FinancieroException {
		LOGGER.info("[TAN INI] rollbackWithdraw");
		AnulacionExtraccionRequest request = new AnulacionExtraccionRequest();

		populateOperationData(anularExtraccion, request);
		//PAGAR envia  CodSuperv=8(reverso a pedido del cliente) a tandem y para el resto=2(reverso automatico)
		if ("4".equals(anularExtraccion.getProductoId())) {					
			request.setCodSuperv("8");
		}
		
		else {
			request.setCodSuperv("2");
		}

		/*AQM ACA VAN LOS CAMPOS FEHCHA/HORA*/
		request.setOrigPostDat(anularExtraccion.getFechaOn().substring(2, 8));
		request.setOrigTime(anularExtraccion.getHoraOn());
		
		AnulacionExtraccionMsgCallback callback = new AnulacionExtraccionMsgCallback();
		
		if (anularExtraccion.getTipoTerminalB24() != null && !anularExtraccion.getTipoTerminalB24().isEmpty())
			request.setTermTyp(anularExtraccion.getTipoTerminalB24());

		AnulacionExtraccionResponse response = (AnulacionExtraccionResponse) tandemService.getResponse(this.formatAndSendTandemMessage(request, callback));

		this.checkSeqNum(anularExtraccion, response);

		LOGGER.info("[TAN FIN] rollbackWithdraw");
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#doDeposit(ar.com.redlink.sam.financieroservices.dto.request.RealizarDepositoRequestDTO)
	 */
	@Override
	public DepositoResponse doDeposit(RealizarDepositoRequestDTO realizarDepositoRequestDTO)throws FinancieroException {
		LOGGER.info("[TAN INI] doDeposit");
		
		DepositoRequest request = new DepositoRequest();

		CuentaDTO cuentaDestino = realizarDepositoRequestDTO.getCuenta();
		request.setToAcct(cuentaDestino.getNumeroCuenta());
		request.setToAcctTyp(cuentaDestino.getTipoCuenta());

		populateCommon(realizarDepositoRequestDTO, request);
		
		request.setImporte(formatImporte(realizarDepositoRequestDTO.getImporte()));

		DepositoMsgCallback callback = new DepositoMsgCallback();

		DepositoResponse response = (DepositoResponse) this.getTandemService().getResponse(this.formatAndSendTandemMessage(request, callback));

		LOGGER.info("[TAN FIN] doDeposit");
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#rollbackDeposit(ar.com.redlink.sam.financieroservices.dto.request.AnularDepositoRequestDTO)
	 */
	@Override
	public AnulacionDepositoResponse rollbackDeposit(AnularDepositoRequestDTO anularDepositoRequestDTO)	throws FinancieroException {
		LOGGER.info("[TAN INI] rollbackDeposit");
		AnulacionDepositoRequest request = new AnulacionDepositoRequest();

		populateOperationData(anularDepositoRequestDTO, request);

		/*AQM*/
		//PAGAR envia  CodSuperv=8(reverso a pedido del cliente) a tandem y para el resto=2(reverso automatico)
		if ("4".equals(anularDepositoRequestDTO.getProductoId())) {					
			request.setCodSuperv("8");
		}	
		else {
			request.setCodSuperv("2");
		}
		/*AQM*/
		CuentaDTO cuentaDestino = anularDepositoRequestDTO
				.getCuenta();			
		request.setToAcct(cuentaDestino.getNumeroCuenta());
		request.setToAcctTyp(cuentaDestino.getTipoCuenta());
		request.setFromAcct("0");
		request.setFromAcctTyp("0");

		AnulacionDepositoMsgCallback callback = new AnulacionDepositoMsgCallback();

		AnulacionDepositoResponse response = (AnulacionDepositoResponse) this.getTandemService().getResponse(this.formatAndSendTandemMessage(request, callback));

		this.checkSeqNum(anularDepositoRequestDTO, response);

		LOGGER.info("[TAN FIN] rollbackDeposit");
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#doTransfer(ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaRequestDTO)
	 */
	@Override
	public TransferenciaResponse doTransfer(
			RealizarTransferenciaRequestDTO realizarTransferenciaRequestDTO)
			throws FinancieroException {
		LOGGER.info("[TAN INI] doTransfer");
		TransferenciaRequest request = new TransferenciaRequest();

		populateCommon(realizarTransferenciaRequestDTO, request);
		// Se hace explicitamente aca, para diferenciarse del resto de metodos
		// por su operacion.

		CuentaDTO cuentaOrigen = realizarTransferenciaRequestDTO
				.getCuentaOrigen();
		request.setFromAcct(cuentaOrigen.getNumeroCuenta());
		request.setFromAcctTyp(cuentaOrigen.getTipoCuenta());
		CuentaDTO cuentaDestino = realizarTransferenciaRequestDTO
				.getCuentaDestino();
		request.setToAcct(cuentaDestino.getNumeroCuenta());
		request.setToAcctTyp(cuentaDestino.getTipoCuenta());

		TransferenciaMsgCallback callback = new TransferenciaMsgCallback();

		TransferenciaResponse response = (TransferenciaResponse) this
				.getTandemService().getResponse(
						this.formatAndSendTandemMessage(request, callback));

		LOGGER.info("[TAN FIN] doTransfer");
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#doLoanWithdraw(ar.com.redlink.sam.financieroservices.dto.request.RealizarExtraccionPrestamoRequestDTO)
	 */
	@Override
	public ExtraccionResponse doLoanWithdraw(
			RealizarExtraccionPrestamoRequestDTO realizarExtraccionPrestamoRequestDTO)
			throws FinancieroException {
		LOGGER.info("[TAN INI] doLoanWithdraw");
		ExtraccionRequest request = new ExtraccionRequest();
		populateOperationData(realizarExtraccionPrestamoRequestDTO, request);

		ExtraccionMsgCallback callback = new ExtraccionMsgCallback();

		ExtraccionResponse response = (ExtraccionResponse) this
				.getTandemService().getResponse(
						this.formatAndSendTandemMessage(request, callback));

		LOGGER.info("[TAN FIN] doLoanWithdraw");
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#rollbackLoanWithdraw(ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionPrestamoRequestDTO)
	 */
	@Override
	public AnulacionExtraccionResponse rollbackLoanWithdraw(
			AnularExtraccionPrestamoRequestDTO anularExtraccionPrestamoRequestDTO)
			throws FinancieroException {
		LOGGER.info("[TAN INI] rollbackLoanWithdraw");
		AnulacionExtraccionRequest request = new AnulacionExtraccionRequest();

		populateOperationData(anularExtraccionPrestamoRequestDTO, request);
		request.setCodSuperv("2");

		AnulacionExtraccionMsgCallback callback = new AnulacionExtraccionMsgCallback();

		AnulacionExtraccionResponse response = (AnulacionExtraccionResponse) this
				.getTandemService().getResponse(
						this.formatAndSendTandemMessage(request, callback));

		this.checkSeqNum(anularExtraccionPrestamoRequestDTO, response);

		LOGGER.info("[TAN FIN] rollbackLoanWithdraw");
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#doSharesWithdraw(ar.com.redlink.sam.financieroservices.dto.request.RealizarExtraccionCuotasRequestDTO)
	 */
	@Override
	public ExtraccionResponse doSharesWithdraw(
			RealizarExtraccionCuotasRequestDTO realizarExtraccionCuotasRequestDTO)
			throws FinancieroException {
		LOGGER.info("[TAN INI] doSharesWithdraw");
		ExtraccionRequest request = new ExtraccionRequest();
		populateOperationData(realizarExtraccionCuotasRequestDTO, request);

		request.setTipoExtraccion("1");
		request.setCantCuotas(Integer
				.toString(realizarExtraccionCuotasRequestDTO.getCuotas()));

		ExtraccionMsgCallback callback = new ExtraccionMsgCallback();

		ExtraccionResponse response = (ExtraccionResponse) this
				.getTandemService().getResponse(
						this.formatAndSendTandemMessage(request, callback));
		
		LOGGER.info("[TAN FIN] doSharesWithdraw");
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemAccountService#rollbackSharesWithdraw(ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionCuotasRequestDTO)
	 */
	@Override
	public AnulacionExtraccionResponse rollbackSharesWithdraw(
			AnularExtraccionCuotasRequestDTO anularExtraccionCuotasRequestDTO)
			throws FinancieroException {
		LOGGER.info("[TAN FIN] rollbackSharesWithdraw");
		
		AnulacionExtraccionRequest request = new AnulacionExtraccionRequest();

		populateOperationData(anularExtraccionCuotasRequestDTO, request);
		request.setCodSuperv("2");

		AnulacionExtraccionMsgCallback callback = new AnulacionExtraccionMsgCallback();

		AnulacionExtraccionResponse response = (AnulacionExtraccionResponse) this
				.getTandemService().getResponse(
						this.formatAndSendTandemMessage(request, callback));

		this.checkSeqNum(anularExtraccionCuotasRequestDTO, response);

		LOGGER.info("[TAN FIN] rollbackSharesWithdraw");
		return response;
	}

	/**
	 * Checkea que el numero de secuencia recibido de Tandem coincida con el
	 * provisto.
	 * 
	 * @param secuenciaOn
	 * @param seqNum
	 * @throws FinancieroException
	 */
	private void checkSeqNum(FinancieroAccountDTO request,
			SAMTandemResponse response) throws FinancieroException {
		if (!StringUtils.stripStart(request.getSecuenciaOn(), "0").equals(
				StringUtils.stripStart(response.getSeqNum(), "0"))) {
			LOGGER.error("Los numeros de secuencia no coinciden, se arroja una excepcion.");
			throw new FinancieroException(FINA140.getCode(), FINA140.getMsg());
		}

	}

	/**
	 * Realiza el tratamiento especifico para lo referido a una operacion
	 * (Cuenta, Importe) y luego, delega al metodo de poblado de datos comunes.
	 * 
	 * @param realizarDebitoRequestDTO
	 * @param request
	 */
	private void populateOperationData(FinancieroAccountDTO commonAccountDTO,
			SAMTandemRequest request) {
		CuentaDTO cuentaDTO = commonAccountDTO.getCuenta();
		request.setFromAcct(cuentaDTO.getNumeroCuenta());
		request.setFromAcctTyp(cuentaDTO.getTipoCuenta());

		request.setImporte(formatImporte(commonAccountDTO.getImporte()));

		this.populateCommon(commonAccountDTO, request);
	}

	/**
	 * Formatea el importe. Se traslada a metodo para poder ser aprovechado por
	 * la trx de Transferencia que mantiene una estructura particular.
	 * 
	 * @param commonAccountDTO
	 * @return el importe formateado.
	 */
	private String formatImporte(BigDecimal importe) {

		return StringUtils.leftPad(	new DecimalFormat(IMPORTE_MASK).format(importe)	.replace(",", "").replace(".", ""), 11, "0");
	}

	/**
	 * Puebla los atributos comunes en el objetos {@link SAMTandemRequest} a
	 * partir de los comunes en {@link FinancieroCommonDTO} y
	 * {@link FinancieroServiceBaseRequestDTO}.
	 * 
	 * @param commonDTO
	 * @param request
	 */
	private void populateCommon(FinancieroCommonDTO commonDTO,	SAMTandemRequest request) {
		
		request.setTermFiid(StringUtils.leftPad("", 4, " "));
		request.setNroTerminal(commonDTO.getTerminalB24());
		request.setTermId(commonDTO.getTerminalB24());
		
		request.setTipoEncripcion("1"); // Variable. BDD.

		if (null != commonDTO.getPin() && !commonDTO.getPin().isEmpty()) {
			request.setCit(commonDTO.getPin());
			request.setCodSuperv("0");
		} else {
			request.setCodSuperv("1");
		}

		if (commonDTO.getSecuenciaOn() != null) {
			request.setSeqNum(commonDTO.getSecuenciaOn());
		}

		String cvc2Informado = "N";
		String cvc2 = commonDTO.getCardSecurityCode();

		if (null == cvc2 || cvc2.isEmpty()) {
			cvc2 = " ";
		} else {
			cvc2Informado = "S";
		}

		request.setCvc2Informado(cvc2Informado);
		request.setCvc2(cvc2);

		request.setTrack2Encriptado(commonDTO.getTrack2());
		request.setTrck2CifradoLen(StringUtils.leftPad(
				Integer.toString(commonDTO.getTrack2().length()), 3, "0"));
		
		request.setTrack1Encriptado(commonDTO.getTrack1());
		request.setTrck1CifradoLen(StringUtils.leftPad(
				Integer.toString(commonDTO.getTrack1().length()), 3, "0"));	
		
		if (commonDTO.getTipoTerminalB24() != null && !commonDTO.getTipoTerminalB24().isEmpty()) {
			request.setTermTyp(commonDTO.getTipoTerminalB24());
		} else {
			request.setTermTyp("60");
		}
		
	}
}
