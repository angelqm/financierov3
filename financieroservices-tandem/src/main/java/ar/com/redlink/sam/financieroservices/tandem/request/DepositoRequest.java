/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  15/03/2015 - 11:52:43
 */

package ar.com.redlink.sam.financieroservices.tandem.request;

import ar.com.redlink.sam.financieroservices.tandem.request.base.SAMTandemRequest;

/**
 * Representacion de una peticion de deposito (20).
 * 
 * @author aguerrea
 * 
 */
public class DepositoRequest extends SAMTandemRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2721459371152796584L;

	private static final String FORMAT_NAME = "20_REQUEST";
	private static final String DEPOSITO_TRX_CODE = "20";

	/**
	 * Constructor default.
	 */
	public DepositoRequest() {
		this.setTranCde(DEPOSITO_TRX_CODE);
		this.setFromAcct("0");
		this.setFromAcctTyp("0");
		this.setFiller(" ");
		this.setTipoDep("E  ");
		this.setTrnData(" ");
		this.setDatosTrx(" ");
		this.setTarjeta(" ");
		this.setCodMon("0");
		this.setTrack2(" ");
		this.setTermName(" ");
	}

	/**
	 * @return the formatName
	 */
	@Override
	public String getFormatName() {
		return FORMAT_NAME;
	}
}
