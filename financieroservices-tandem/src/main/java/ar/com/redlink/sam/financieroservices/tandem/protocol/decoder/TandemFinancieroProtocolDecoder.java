package ar.com.redlink.sam.financieroservices.tandem.protocol.decoder;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.mina.core.session.IoSession;
import ar.com.link.connector.tandem.impl.TandemProtocolDecoder;

/**
 * Decoder Custom para las transacciones invocadas por Financiero.
 * 
 * @author aguerrea
 * 
 */
public class TandemFinancieroProtocolDecoder extends TandemProtocolDecoder {

	private Log logger = LogFactory.getLog(getClass().getCanonicalName());
	private Map<String, String> tranCodeList = new HashMap<String, String>();

	/**
	 * 
	 * @see ar.com.link.connector.tandem.impl.TandemProtocolDecoder#unmarshall(org.apache.mina.core.session.IoSession,
	 *      java.lang.String)
	 */
	@Override
	protected Object unmarshall(IoSession session, String message)
			throws IOException {

		logger.debug("Mensaje recibido desde Tandem: [" + message + "]");
		try {
			String transactionCode = extractTranCode(message);

			return doUnmarshall(message, transactionCode);
		} catch (RuntimeException e) {
			logger.error("Error en marshalling de mensaje: " + message, e);
			throw e;
		} catch (Exception e) {
			String errormsg = "Error en marshalling de mensaje: " + message;
			logger.error(errormsg, e);
			throw new RuntimeException(errormsg, e);
		}
	}

	/**
	 * Extrae el codigo de transaccion.
	 * 
	 * @param message
	 * @return codigo de trx.
	 */
	protected String extractTranCode(String message) {
		// Response
		String codRta = message.substring(117, 119);

		logger.debug("codRta =" + codRta);
		if (!"00".equals(codRta) && !"01".equals(codRta)) // 00 y 01 son OK.
		{
			logger.error("Error - Mensaje con codRta =" + codRta);
			return "ERROR";
		}

		String tranCode = message.substring(64, 66);

		// Se debe hacer lo mismo para la 40...
		if (tranCode.equals("10") && message.length() != 227) {
			tranCode = "11";
		}
		//AQM RESPPONSE DEVOLUCION
		if (tranCode.equals("20") && message.length() != 160) {
			tranCode = "21";
		}
		String msgFormat = tranCodeList.get(tranCode);

		return msgFormat != null ? msgFormat : "ERROR";
	}

	/**
	 * @return the tranCodeList
	 */
	public Map<String, String> getTranCodeList() {
		return tranCodeList;
	}

	/**
	 * @param tranCodeList
	 *            the tranCodeList to set
	 */
	public void setTranCodeList(Map<String, String> tranCodeList) {
		this.tranCodeList = tranCodeList;
	}
}
