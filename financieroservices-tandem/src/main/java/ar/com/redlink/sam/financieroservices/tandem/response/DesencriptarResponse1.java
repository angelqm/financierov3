package ar.com.redlink.sam.financieroservices.tandem.response;

import ar.com.redlink.sam.financieroservices.tandem.response.base.SAMTandemResponse;

public class DesencriptarResponse1 extends SAMTandemResponse {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2022651106162074709L;
	
	private String termPref;
	private String termName;
	private String filler;
	private String data;
	private String etx;
	private String track1;

	/**
	 * @return the track1
	 */
	public String getTrack1() {
		return track1;
	}

	/**
	 * @param track1 the track1 to set
	 */
	public void setTrack1(String track1) {
		this.track1 = track1;
	}

	/**
	 * @return the termPref
	 */
	public String getTermPref() {
		return termPref;
	}

	/**
	 * @param termPref the termPref to set
	 */
	public void setTermPref(String termPref) {
		this.termPref = termPref;
	}

	/**
	 * @return the termName
	 */
	public String getTermName() {
		return termName;
	}

	/**
	 * @param termName the termName to set
	 */
	public void setTermName(String termName) {
		this.termName = termName;
	}

	/**
	 * @return the filler
	 */
	public String getFiller() {
		return filler;
	}

	/**
	 * @param filler the filler to set
	 */
	public void setFiller(String filler) {
		this.filler = filler;
	}

	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}

	/**
	 * @return the etx
	 */
	public String getEtx() {
		return etx;
	}

	/**
	 * @param etx the etx to set
	 */
	public void setEtx(String etx) {
		this.etx = etx;
	}
}
