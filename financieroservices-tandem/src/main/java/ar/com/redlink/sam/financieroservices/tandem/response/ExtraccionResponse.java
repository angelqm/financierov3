/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  15/03/2015 - 11:41:15
 */

package ar.com.redlink.sam.financieroservices.tandem.response;

import ar.com.redlink.sam.financieroservices.tandem.response.base.SAMTandemResponse;

/**
 * Representacion de una respuesta a la transaccion de extraccion (10).
 * 
 * @author aguerrea
 * 
 */
public class ExtraccionResponse extends SAMTandemResponse {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3435977061279024884L;
	private String termPref;
	private String termName;
	private String filler;

	private String saldo;
	private String dineroDisponible;
	private String extraccionesDisp;
	private String intereses;
	private String tna;
	private String tem;
	private String cantCuotas;
	private String dispEfect;
	private String overdraftLimit;
	private String etx;

	/**
	 * @return the termPref
	 */
	public String getTermPref() {
		return termPref;
	}

	/**
	 * @param termPref
	 *            the termPref to set
	 */
	public void setTermPref(String termPref) {
		this.termPref = termPref;
	}

	/**
	 * @return the termName
	 */
	public String getTermName() {
		return termName;
	}

	/**
	 * @param termName
	 *            the termName to set
	 */
	public void setTermName(String termName) {
		this.termName = termName;
	}

	/**
	 * @return the filler
	 */
	public String getFiller() {
		return filler;
	}

	/**
	 * @param filler
	 *            the filler to set
	 */
	public void setFiller(String filler) {
		this.filler = filler;
	}

	/**
	 * @return the etx
	 */
	public String getEtx() {
		return etx;
	}

	/**
	 * @param etx
	 *            the etx to set
	 */
	public void setEtx(String etx) {
		this.etx = etx;
	}

	/**
	 * @return the saldo
	 */
	public String getSaldo() {
		return saldo;
	}

	/**
	 * @param saldo
	 *            the saldo to set
	 */
	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}

	/**
	 * @return the dineroDisponible
	 */
	public String getDineroDisponible() {
		return dineroDisponible;
	}

	/**
	 * @param dineroDisponible
	 *            the dineroDisponible to set
	 */
	public void setDineroDisponible(String dineroDisponible) {
		this.dineroDisponible = dineroDisponible;
	}

	/**
	 * @return the extraccionesDisp
	 */
	public String getExtraccionesDisp() {
		return extraccionesDisp;
	}

	/**
	 * @param extraccionesDisp
	 *            the extraccionesDisp to set
	 */
	public void setExtraccionesDisp(String extraccionesDisp) {
		this.extraccionesDisp = extraccionesDisp;
	}

	/**
	 * @return the intereses
	 */
	public String getIntereses() {
		return intereses;
	}

	/**
	 * @param intereses
	 *            the intereses to set
	 */
	public void setIntereses(String intereses) {
		this.intereses = intereses;
	}

	/**
	 * @return the tna
	 */
	public String getTna() {
		return tna;
	}

	/**
	 * @param tna
	 *            the tna to set
	 */
	public void setTna(String tna) {
		this.tna = tna;
	}

	/**
	 * @return the tem
	 */
	public String getTem() {
		return tem;
	}

	/**
	 * @param tem
	 *            the tem to set
	 */
	public void setTea(String tem) {
		this.tem = tem;
	}

	/**
	 * @return the cantCuotas
	 */
	public String getCantCuotas() {
		return cantCuotas;
	}

	/**
	 * @param cantCuotas
	 *            the cantCuotas to set
	 */
	public void setCantCuotas(String cantCuotas) {
		this.cantCuotas = cantCuotas;
	}

	/**
	 * @return the dispEfect
	 */
	public String getDispEfect() {
		return dispEfect;
	}

	/**
	 * @param dispEfect
	 *            the dispEfect to set
	 */
	public void setDispEfect(String dispEfect) {
		this.dispEfect = dispEfect;
	}

	/**
	 * @return the overdraftLimit
	 */
	public String getOverdraftLimit() {
		return overdraftLimit;
	}

	/**
	 * @param overdraftLimit
	 *            the overdraftLimit to set
	 */
	public void setOverdraftLimit(String overdraftLimit) {
		this.overdraftLimit = overdraftLimit;
	}
}
