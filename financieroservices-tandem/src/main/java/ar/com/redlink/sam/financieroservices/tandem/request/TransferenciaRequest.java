/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  15/03/2015 - 11:52:43
 */

package ar.com.redlink.sam.financieroservices.tandem.request;

import ar.com.redlink.sam.financieroservices.tandem.request.base.SAMTandemRequest;

/**
 * Representacion de una peticion de transferencia (40).
 * 
 * @author aguerrea
 * 
 */
public class TransferenciaRequest extends SAMTandemRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2721459371152796584L;

	private static final String FORMAT_NAME = "40_REQUEST";
	private static final String TRANSFERENCIA_TRX_CODE = "40";

	/**
	 * Constructor default.
	 */
	public TransferenciaRequest() {
		this.setTranCde(TRANSFERENCIA_TRX_CODE);
		this.setFiller(" ");
		this.setTipoDep(" ");
		this.setTrnData(" ");
		this.setDatosTrx(" ");
		this.setTarjeta(" ");
		this.setCodMon("0");
		this.setTrack2(" ");
		this.setTermName(" ");
	}

	/**
	 * @return the formatName
	 */
	@Override
	public String getFormatName() {
		return FORMAT_NAME;
	}
}
