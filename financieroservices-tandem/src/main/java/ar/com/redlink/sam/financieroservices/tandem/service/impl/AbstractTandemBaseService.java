/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  17/03/2015 - 16:59:17
 */

package ar.com.redlink.sam.financieroservices.tandem.service.impl;

import org.apache.log4j.Logger;

import ar.com.link.connector.tandem.messages.TandemRequest;
import ar.com.link.connector.tandem.messages.TandemResponse;
import ar.com.redlink.framework.services.RedLinkService;
import ar.com.redlink.sam.financieroservices.service.ModelMapperService;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.tandem.exception.TandemException;
import ar.com.redlink.sam.financieroservices.tandem.msg.callback.FinancieroMsgCallback;
import ar.com.redlink.sam.financieroservices.tandem.service.TandemMessagePopulatorService;
import ar.com.redlink.sam.financieroservices.tandem.service.TandemService;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA200;

/**
 * Abstraccion de servicio de Tandem que define los componentes base y su forma
 * de uso, normalizando el modo de envio de mensaje y manejo.
 * 
 * @author aguerrea
 * 
 */
public abstract class AbstractTandemBaseService implements RedLinkService {

	private static final Logger LOGGER = Logger.getLogger(AbstractTandemBaseService.class);

	protected static final String IMPORTE_MASK = "0.00";

	protected TandemService tandemService;
	protected ModelMapperService modelMapperService;
	protected TandemMessagePopulatorService tandemMessagePopulatorService;

	/**
	 * Formatea apropiadamente el mensaje a Tandem y lo envia.
	 * 
	 * @param request
	 * @return el response de Tandem.
	 * @throws FinancieroException
	 */
	public TandemResponse formatAndSendTandemMessage(TandemRequest request,	FinancieroMsgCallback callback) throws FinancieroException {
		tandemMessagePopulatorService.populateMessage(request);

		TandemResponse tandemResponse = null;
		try {
			tandemResponse = tandemService.sendSync(request, callback);
		} catch (TandemException e) {
			LOGGER.error("Error de Tandem: " + e.getCodigoError() + " "+ e.getDescripcion());
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), e.getMessage());
		}

		return tandemResponse;
	}

	/**
	 * @return the tandemService
	 */
	public TandemService getTandemService() {
		return tandemService;
	}

	/**
	 * @param tandemService
	 *            the tandemService to set
	 */
	public void setTandemService(TandemService tandemService) {
		this.tandemService = tandemService;
	}

	/**
	 * @return the modelMapperService
	 */
	public ModelMapperService getModelMapperService() {
		return modelMapperService;
	}

	/**
	 * @param modelMapperService
	 *            the modelMapperService to set
	 */
	public void setModelMapperService(ModelMapperService modelMapperService) {
		this.modelMapperService = modelMapperService;
	}

	/**
	 * @return the tandemMessagePopulatorService
	 */
	public TandemMessagePopulatorService getTandemMessagePopulatorService() {
		return tandemMessagePopulatorService;
	}

	/**
	 * @param tandemMessagePopulatorService
	 *            the tandemMessagePopulatorService to set
	 */
	public void setTandemMessagePopulatorService(
			TandemMessagePopulatorService tandemMessagePopulatorService) {
		this.tandemMessagePopulatorService = tandemMessagePopulatorService;
	}
}
