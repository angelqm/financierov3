/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  08/04/2015 - 22:23:12
 */

package ar.com.redlink.sam.financieroservices.tandem.request;

import ar.com.redlink.sam.financieroservices.tandem.request.base.SAMTandemRequest;

/**
 * Wrapper para el request de anulacion de extraccion en cuotas.
 * 
 * @author aguerrea
 * 
 */
public class AnulacionExtraccionCuotasRequest extends SAMTandemRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5168990878758764681L;

	private static final String FORMAT_NAME = "11_REQUEST";

	/**
	 * Constructor default.
	 */
	public AnulacionExtraccionCuotasRequest() {
		this.setTermName(" ");
	}

	/**
	 * @return the formatName
	 */
	@Override
	public String getFormatName() {
		return FORMAT_NAME;
	}
}
