package ar.com.redlink.sam.financieroservices.tandem.constant;

/**
 * Enum para codigos de error normales de Tandem. (Extracto de SoatServices)
 * 
 * @author RL
 * 
 */
public enum ErrorTandem {
	TimeOutSolicitudAsincronica("-1",
			"No se obtuvo respuesta de una solicitud asincronica."), ExcepcionCallback(
			"-2", "Se produjo una excepcion en el callback: "), RespuestPendiente(
			"-3", "La respuesta de Tandem queda pendiente."), ErrorNoDefinido(
			"-4", "Undefined error."), TimeOutSolicitudSincronica("-5",
			"No se obtuvo respuesta de una solicitud sincrona.");

	public final String codigo;
	public final String descripcion;

	/**
	 * Constructor default.
	 * 
	 * @param codigo
	 * @param descripcion
	 */
	ErrorTandem(String codigo, String descripcion) {
		this.codigo = codigo;
		this.descripcion = descripcion;
	};

}
