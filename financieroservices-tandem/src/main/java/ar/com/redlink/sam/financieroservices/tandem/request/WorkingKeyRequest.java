/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  03/02/2015 - 22:17:01
 */

package ar.com.redlink.sam.financieroservices.tandem.request;

import ar.com.redlink.sam.financieroservices.tandem.request.base.SAMTandemRequest;

/**
 * Representacion de una peticion de WorkingKey a Tandem (31).
 * 
 * @author aguerrea
 * 
 */
public class WorkingKeyRequest extends SAMTandemRequest {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8248936671837557681L;
	private static final String FORMAT_NAME = "31_REQUEST";
	private static final String WORKING_KEY_TRX_CODE = "31";

	// private String termPref;
	// private String termName;
	// private String termAdmin;
	// private String tarjeta;
	// private String codSuperv;
	// private String codMon;
	// private String tipoDep;
	// private String clearing;
	// private String trnData;
	// private String cvc2Informado;
	// private String cvc2;
	// private String tipoEncripcion;
	// private String track2Encriptado;
	// private String trck2CifradoLen;
	// private String nroTerminal;
	private String tipoWorkingKey;
	private String filler2;

	/**
	 * Se inicializa con varios valores por defecto asociados al request en si.
	 */
	public WorkingKeyRequest() {
		this.setTranCde(WORKING_KEY_TRX_CODE);
		this.setFromAcct("0");
		this.setFromAcctTyp("0");
		this.setToAcct("0");
		this.setToAcctTyp("0");
		this.setImporte("0");
		this.setFiller(" ");
		this.setTipoDep(" ");
		this.setClearing(" ");
		this.setTrnData(" ");
		this.setDatosTrx(" ");
		this.setTarjeta(" ");
		this.setCodMon("0");
		this.setCvc2Informado("N");
		this.setFiller2(" ");
		this.setTrck2CifradoLen("100");
		this.setTermName(" ");
	}

	// /**
	// * @return the termPref
	// */
	// public String getTermPref() {
	// return termPref;
	// }
	//
	// /**
	// * @param termPref
	// * the termPref to set
	// */
	// public void setTermPref(String termPref) {
	// this.termPref = termPref;
	// }
	//
	// /**
	// * @return the termName
	// */
	// public String getTermName() {
	// return termName;
	// }
	//
	// /**
	// * @param termName
	// * the termName to set
	// */
	// public void setTermName(String termName) {
	// this.termName = termName;
	// }
	//
	// /**
	// * @return the termAdmin
	// */
	// public String getTermAdmin() {
	// return termAdmin;
	// }
	//
	// /**
	// * @param termAdmin
	// * the termAdmin to set
	// */
	// public void setTermAdmin(String termAdmin) {
	// this.termAdmin = termAdmin;
	// }
	//
	// /**
	// * @return the tarjeta
	// */
	// public String getTarjeta() {
	// return tarjeta;
	// }
	//
	// /**
	// * @param tarjeta
	// * the tarjeta to set
	// */
	// public void setTarjeta(String tarjeta) {
	// this.tarjeta = tarjeta;
	// }
	//
	// /**
	// * @return the codSuperv
	// */
	// public String getCodSuperv() {
	// return codSuperv;
	// }
	//
	// /**
	// * @param codSuperv
	// * the codSuperv to set
	// */
	// public void setCodSuperv(String codSuperv) {
	// this.codSuperv = codSuperv;
	// }
	//
	// /**
	// * @return the codMon
	// */
	// public String getCodMon() {
	// return codMon;
	// }
	//
	// /**
	// * @param codMon
	// * the codMon to set
	// */
	// public void setCodMon(String codMon) {
	// this.codMon = codMon;
	// }
	//
	// /**
	// * @return the tipoDep
	// */
	// public String getTipoDep() {
	// return tipoDep;
	// }
	//
	// /**
	// * @param tipoDep
	// * the tipoDep to set
	// */
	// public void setTipoDep(String tipoDep) {
	// this.tipoDep = tipoDep;
	// }
	//
	// /**
	// * @return the clearing
	// */
	// public String getClearing() {
	// return clearing;
	// }
	//
	// /**
	// * @param clearing
	// * the clearing to set
	// */
	// public void setClearing(String clearing) {
	// this.clearing = clearing;
	// }
	//
	// /**
	// * @return the trnData
	// */
	// public String getTrnData() {
	// return trnData;
	// }
	//
	// /**
	// * @param trnData
	// * the trnData to set
	// */
	// public void setTrnData(String trnData) {
	// this.trnData = trnData;
	// }
	//
	// /**
	// * @return the cvcInformado
	// */
	// public String getCvc2Informado() {
	// return cvc2Informado;
	// }
	//
	// /**
	// * @param cvcInformado
	// * the cvcInformado to set
	// */
	// public void setCvc2Informado(String cvc2Informado) {
	// this.cvc2Informado = cvc2Informado;
	// }
	//
	// /**
	// * @return the cvc2
	// */
	// public String getCvc2() {
	// return cvc2;
	// }
	//
	// /**
	// * @param cvc2
	// * the cvc2 to set
	// */
	// public void setCvc2(String cvc2) {
	// this.cvc2 = cvc2;
	// }
	//
	// /**
	// * @return the tipoEncripcion
	// */
	// public String getTipoEncripcion() {
	// return tipoEncripcion;
	// }
	//
	// /**
	// * @param tipoEncripcion
	// * the tipoEncripcion to set
	// */
	// public void setTipoEncripcion(String tipoEncripcion) {
	// this.tipoEncripcion = tipoEncripcion;
	// }
	//
	// /**
	// * @return the track2Encriptado
	// */
	// public String getTrack2Encriptado() {
	// return track2Encriptado;
	// }
	//
	// /**
	// * @param track2Encriptado
	// * the track2Encriptado to set
	// */
	// public void setTrack2Encriptado(String track2Encriptado) {
	// this.track2Encriptado = track2Encriptado;
	// }
	//
	// /**
	// * @return the trck2CifradoLen
	// */
	// public String getTrck2CifradoLen() {
	// return trck2CifradoLen;
	// }
	//
	// /**
	// * @param trck2CifradoLen
	// * the trck2CifradoLen to set
	// */
	// public void setTrck2CifradoLen(String trck2CifradoLen) {
	// this.trck2CifradoLen = trck2CifradoLen;
	// }

	// /**
	// * @return the nroTerminal
	// */
	// public String getNroTerminal() {
	// return nroTerminal;
	// }
	//
	// /**
	// * @param nroTerminal
	// * the nroTerminal to set
	// */
	// public void setNroTerminal(String nroTerminal) {
	// this.nroTerminal = nroTerminal;
	// }

	/**
	 * @return the tipoWorkingKey
	 */
	public String getTipoWorkingKey() {
		return tipoWorkingKey;
	}

	/**
	 * @param tipoWorkingKey
	 *            the tipoWorkingKey to set
	 */
	public void setTipoWorkingKey(String tipoWorkingKey) {
		this.tipoWorkingKey = tipoWorkingKey;
	}

	/**
	 * @return the filler2
	 */
	public String getFiller2() {
		return filler2;
	}

	/**
	 * @param filler2
	 *            the filler2 to set
	 */
	public void setFiller2(String filler2) {
		this.filler2 = filler2;
	}

	/**
	 * @see ar.com.link.connector.tandem.messages.TandemRequest#getFormatName()
	 */
	@Override
	public String getFormatName() {
		return FORMAT_NAME;
	}
}
