package ar.com.redlink.sam.financieroservices.tandem.service.impl;

import org.apache.log4j.Logger;

import ar.com.link.connector.RequestResponseManager;
import ar.com.link.connector.ResponseCallback;
import ar.com.link.connector.ServiceResponse;
import ar.com.link.connector.tandem.TandemConnector;
import ar.com.link.connector.tandem.messages.TandemRequest;
import ar.com.link.connector.tandem.messages.TandemResponse;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroTandemException;
import ar.com.redlink.sam.financieroservices.tandem.constant.ErrorTandem;
import ar.com.redlink.sam.financieroservices.tandem.exception.AsyncTandemException;
import ar.com.redlink.sam.financieroservices.tandem.exception.SyncTandemTimeOutException;
import ar.com.redlink.sam.financieroservices.tandem.exception.TandemException;
import ar.com.redlink.sam.financieroservices.tandem.response.FinancieroErrorResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.base.SAMTandemResponse;
import ar.com.redlink.sam.financieroservices.tandem.service.TandemService;

/**
 * Implementacion de la abstraccion del cliente de Tandem.
 * 
 * @author RL
 * 
 */
public class TandemServiceImpl implements TandemService {
	private TandemConnector connector;
	private RequestResponseManager requestResponseManager;
	private long syncCallTimeout;
	private long asyncCallTimeout;
	private long pollCallTimeout;

	private static final Logger logger = Logger
			.getLogger(TandemServiceImpl.class);

	/**
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemService#sendAsync(ar.com.link.connector.tandem.messages.TandemRequest,
	 *      ar.com.link.connector.ResponseCallback)
	 */
	public TandemResponse sendAsync(TandemRequest request,
			ResponseCallback callback) throws AsyncTandemException {

		TandemResponse response = this.doSend(request, callback,
				this.asyncCallTimeout);

		if (response == null) {
			throw new AsyncTandemException(request.getTermId(),
					request.getSeqNum());
		}

		return response;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemService#sendSync(ar.com.link.connector.tandem.messages.TandemRequest,
	 *      ar.com.link.connector.ResponseCallback)
	 */
	public TandemResponse sendSync(TandemRequest request,ResponseCallback callback) throws SyncTandemTimeOutException {
		TandemResponse response = null;

		response = this.doSend(request, callback, this.syncCallTimeout);

		if (response == null || response.isPending()) {
			throw new SyncTandemTimeOutException(request.getTermId(),request.getSeqNum());
		}

		return response;
	}

	/**
	 * Hace el envio del mensaje a Tandem con el callback y timeout definidos.
	 * 
	 * @param request
	 * @param callback
	 * @param poolTimeout
	 * @return Respuesta de Tandem.
	 */
	private TandemResponse doSend(TandemRequest request,
			ResponseCallback callback, long poolTimeout) {
		TandemResponse response = null;
		try {
			String correlationId = this.connector.send(request, callback);

			response = (TandemResponse) connector.poll(correlationId,
					poolTimeout);
		} catch (Exception e) {

			throw new TandemException(ErrorTandem.ErrorNoDefinido.codigo,
					e.toString(), request.getTermFiid(), request.getSeqNum());
		}
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemService#addExternalRequest(java.lang.String,
	 *      ar.com.link.connector.ResponseCallback)
	 */
	public void addExternalRequest(String correlationId,
			ResponseCallback callback) {
		requestResponseManager.addExternalRequest(correlationId, callback);

	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemService#addExternalResponse(ar.com.link.connector.tandem.messages.TandemResponse)
	 */
	public void addExternalResponse(TandemResponse response) {
		requestResponseManager.addExternalResponse(response);
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemService#pollForResponse(java.lang.String)
	 */
	public TandemResponse pollForResponse(String correlationId) {
		TandemResponse response = (TandemResponse) connector.poll(
				correlationId, this.pollCallTimeout);

		return response;
	}

	/**
	 * @throws FinancieroException
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemService#getResponse(ar.com.link.connector.ServiceResponse)
	 */
	public Object getResponse(ServiceResponse serviceResponse)	throws FinancieroException {
		String numeroTerminal;
		String numeroSecuencia;
		TandemResponse tandemResponse = null;
		boolean isTandemResponse = (serviceResponse instanceof TandemResponse);

		if (isTandemResponse) {
			tandemResponse = (TandemResponse) serviceResponse;
			numeroTerminal = tandemResponse.getTermId();
			numeroSecuencia = tandemResponse.getSeqNum();
		} else {
			numeroTerminal = "";
			numeroSecuencia = "";
		}

		if (serviceResponse.isError()) {
			String codigoError = ErrorTandem.ErrorNoDefinido.codigo;
			String descripcion = ErrorTandem.ErrorNoDefinido.descripcion;

			if (isTandemResponse) {

				descripcion = "";

				if (tandemResponse instanceof FinancieroErrorResponse) {
					FinancieroErrorResponse financieroMsgErrorResponse = (FinancieroErrorResponse) tandemResponse;
					//FinancieroServicesMsgEnum.FINA509.getMsg()
					codigoError = financieroMsgErrorResponse.getCodRta();
					descripcion = financieroMsgErrorResponse.getMsg();
					logger.error("Error en mensaje " + numeroTerminal + "-"	+ numeroSecuencia + " Codigo: " + codigoError	+ " Descripcion:" + descripcion);
					throw new FinancieroTandemException("FINA-500",	"Error de Tandem", codigoError, descripcion);
				}

			} else {
				if (serviceResponse.isPending()) {

					((SAMTandemResponse) serviceResponse.getCallbackResponse()).setCodRta(ErrorTandem.RespuestPendiente.codigo);

					((SAMTandemResponse) serviceResponse.getCallbackResponse()).setMsg(ErrorTandem.RespuestPendiente.descripcion);

					logger.error("Error en mensaje " + numeroTerminal + "-"	+ numeroSecuencia + " "	+ ErrorTandem.RespuestPendiente.descripcion);

				} else {

					if (serviceResponse.getCallbackResponse() instanceof Throwable) {
						((SAMTandemResponse) serviceResponse.getCallbackResponse()).setCodRta(ErrorTandem.ExcepcionCallback.codigo);

						((SAMTandemResponse) serviceResponse.getCallbackResponse()).setMsg(ErrorTandem.ExcepcionCallback.descripcion);
						logger.error("Error en mensaje " + numeroTerminal + "-"	+ numeroSecuencia + " "	+ ErrorTandem.ExcepcionCallback.descripcion);
					}

				}
			}
		}
		return serviceResponse.getCallbackResponse();
	}

	/**
	 * @param connector
	 *            the connector to set
	 */
	public void setConnector(TandemConnector connector) {
		this.connector = connector;
	}

	/**
	 * @param requestResponseManager
	 *            the requestResponseManager to set
	 */
	public void setRequestResponseManager(
			RequestResponseManager requestResponseManager) {
		this.requestResponseManager = requestResponseManager;
	}

	/**
	 * @param syncCallTimeout
	 *            the syncCallTimeout to set
	 */
	public void setSyncCallTimeout(long syncCallTimeout) {
		this.syncCallTimeout = syncCallTimeout;
	}

	/**
	 * @param asyncCallTimeout
	 *            the asyncCallTimeout to set
	 */
	public void setAsyncCallTimeout(long asyncCallTimeout) {
		this.asyncCallTimeout = asyncCallTimeout;
	}

	/**
	 * @param pollCallTimeout
	 *            the pollCallTimeout to set
	 */
	public void setPollCallTimeout(long pollCallTimeout) {
		this.pollCallTimeout = pollCallTimeout;
	}

	/**
	 * @param tandemErrorHandler
	 *            the tandemErrorHandler to set
	 */
	/*
	 * public void setTandemErrorHandler(TandemErrorHandler tandemErrorHandler)
	 * { this.tandemErrorHandler = tandemErrorHandler; }
	 */
}
