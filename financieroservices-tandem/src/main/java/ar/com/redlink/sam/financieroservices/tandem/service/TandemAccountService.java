/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  16/03/2015 - 23:22:03
 */

package ar.com.redlink.sam.financieroservices.tandem.service;

import ar.com.redlink.framework.services.RedLinkService;
import ar.com.redlink.sam.financieroservices.dto.request.AnularDebitoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularDepositoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionCuotasRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionPrestamoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.BalanceRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ExtraccionRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDebitoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDepositoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarExtraccionCuotasRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarExtraccionPrestamoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarPagoDebitoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ValidarTarjetaRequestDTO;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.tandem.response.AnulacionDepositoResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.AnulacionExtraccionResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.ConsultaExtraccionResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.ConsultaSaldoResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.DepositoResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.ExtraccionResponse;
import ar.com.redlink.sam.financieroservices.tandem.response.TransferenciaResponse;

/**
 * Servicio encargado de las operaciones sobre cuentas con Tandem.
 * 
 * @author aguerrea
 * 
 */
public interface TandemAccountService extends RedLinkService {

	/**
	 * Valida la tarjeta invocando una trx 62.
	 * 
	 * @param validarTarjeta
	 * @return Resultado de la validacion.
	 */
	public ConsultaExtraccionResponse validateCard(ValidarTarjetaRequestDTO validarTarjeta) throws FinancieroException;

	/**
	 * Realiza una operacion de extraccion para debito.
	 * 
	 * @param realizarDebitoRequestDTO
	 * @return resultado de la transaccion.
	 */
	public ExtraccionResponse doDebit(RealizarDebitoRequestDTO realizarDebitoRequestDTO) throws FinancieroException;

	/**
	 * Anula una transaccion de extraccion para debito.
	 * 
	 * @param anularDebitoRequestDTO
	 * @return resultado de la transaccion.
	 */
	public AnulacionExtraccionResponse rollbackDebit(AnularDebitoRequestDTO anularDebitoRequestDTO)
			throws FinancieroException;

	/**
	 * Anula una transacción de pago con débito.
	 * 
	 * 
	 * @param anularDebitoRequestDTO
	 * @return resultado de la transaccion.
	 */
	public AnulacionExtraccionResponse rollbackDebitPay(AnularDebitoRequestDTO anularDebitoRequestDTO)
			throws FinancieroException;

	/**
	 * Permite realizar una extraccion.
	 * 
	 * @param extraccionRequest
	 * @return devuelve el resultado de la extraccion e informacion de la misma.
	 */
	public ExtraccionResponse doWithdraw(ExtraccionRequestDTO extraccion) throws FinancieroException;

	/**
	 * Anula una extraccion.
	 * 
	 * @param anularExtraccion
	 */
	public AnulacionExtraccionResponse rollbackWithdraw(AnularExtraccionRequestDTO anularExtraccion)
			throws FinancieroException;

	/**
	 * Devuelve el estado de cuenta.
	 * 
	 * @param balance
	 * @return
	 */
	public ConsultaSaldoResponse getBalance(BalanceRequestDTO balance) throws FinancieroException;

	/**
	 * Realiza una operacion de deposito.
	 * 
	 * @param realizarDepositoRequestDTO
	 * @return resultado de la transaccion.
	 */
	public DepositoResponse doDeposit(RealizarDepositoRequestDTO realizarDepositoRequestDTO) throws FinancieroException;

	/**
	 * Anula una operacion de deposito.
	 * 
	 * @param anularDepositoRequestDTO
	 * @return resultado de la operacion.
	 */
	public AnulacionDepositoResponse rollbackDeposit(AnularDepositoRequestDTO anularDepositoRequestDTO)
			throws FinancieroException;

	/**
	 * Realiza una transferencia.
	 * 
	 * @param realizarTransferenciaRequestDTO
	 * @return resultado de la operacion.
	 */
	public TransferenciaResponse doTransfer(RealizarTransferenciaRequestDTO realizarTransferenciaRequestDTO)
			throws FinancieroException;

	/**
	 * Realiza una extraccion de prestamo.
	 * 
	 * @param realizarExtraccionPrestamoRequestDTO
	 * @return resultado de la operacion.
	 */
	public ExtraccionResponse doLoanWithdraw(RealizarExtraccionPrestamoRequestDTO realizarExtraccionPrestamoRequestDTO)
			throws FinancieroException;

	/**
	 * Realiza una anulacion de extraccion de prestamo.
	 * 
	 * @param anularExtraccionPrestamoRequestDTO
	 * @return resultado de la operacion.
	 */
	public AnulacionExtraccionResponse rollbackLoanWithdraw(
			AnularExtraccionPrestamoRequestDTO anularExtraccionPrestamoRequestDTO) throws FinancieroException;

	/**
	 * Realiza una extraccion en cuotas.
	 * 
	 * @param realizarExtraccionCuotasRequestDTO
	 * @return Resultado de la operacion.
	 * @throws FinancieroException
	 */
	public ExtraccionResponse doSharesWithdraw(RealizarExtraccionCuotasRequestDTO realizarExtraccionCuotasRequestDTO)
			throws FinancieroException;

	/**
	 * Realiza la anulacion de una extraccion en cuotas.
	 * 
	 * @param anularExtraccionCuotasRequestDTO
	 * @return Resultado de la operacion.
	 * @throws FinancieroException
	 */
	public AnulacionExtraccionResponse rollbackSharesWithdraw(
			AnularExtraccionCuotasRequestDTO anularExtraccionCuotasRequestDTO) throws FinancieroException;

	/**
	 * 
	 * @param realizarPagoDebitoRequestDTO
	 * @return
	 * @throws FinancieroException
	 */
	public ExtraccionResponse doDebitPay(RealizarPagoDebitoRequestDTO realizarPagoDebitoRequestDTO)
			throws FinancieroException;

	/**
	 * 
	 * @param validarTarjeta
	 * @return
	 * @throws FinancieroException
	 */
	public ConsultaExtraccionResponse decryptCard(ValidarTarjetaRequestDTO validarTarjeta) throws FinancieroException;
}
