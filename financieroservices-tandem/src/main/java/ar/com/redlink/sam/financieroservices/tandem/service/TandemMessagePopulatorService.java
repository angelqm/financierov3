/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  16/03/2015 - 23:36:07
 */

package ar.com.redlink.sam.financieroservices.tandem.service;

import ar.com.redlink.framework.services.RedLinkService;

/**
 * Servicio que wrappea la logica de tratamiento de formato de mensajes de
 * Tandem.
 * 
 * @author aguerrea
 * 
 */
public interface TandemMessagePopulatorService extends RedLinkService {

	/**
	 * Recibe un objeto y padea sus atributos on the fly, sin retorno.
	 * 
	 * @param request
	 */
	public void populateMessage(Object request);
}
