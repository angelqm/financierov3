package ar.com.redlink.sam.financieroservices.tandem.util.format;
import java.util.Iterator;

import javax.xml.namespace.NamespaceContext;

import org.apache.xml.utils.PrefixResolver;
import org.apache.xml.utils.PrefixResolverDefault;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * Clase que implementa NamespaceContext de xml. 
 * 
 * @author David Cisneros
 *
 */
public class FormatNamespaceContext implements NamespaceContext {

	private PrefixResolver resolver;
	private Node initialNode;

	/**
	 * Constructor por defecto.
	 * 
	 * @param document
	 */
	public FormatNamespaceContext(Document document){
		this.initialNode = document.getDocumentElement();
		this.resolver = new PrefixResolverDefault(this.initialNode);
	}

	/**
	 * @see javax.xml.namespace.NamespaceContext#getNamespaceURI(java.lang.String)
	 */
	public String getNamespaceURI(String prefix) {
		String uri = resolver.getNamespaceForPrefix(prefix);

		int index = 0;

		while (null == uri && index < initialNode.getChildNodes().getLength()){
			uri = this.getChildUri(prefix, initialNode.getChildNodes().item(index));
			index++;
		}
		return uri;
	}

	/**
	 * Devuelve la uri de los childs.
	 * 
	 * @param prefix
	 * @param node
	 * @return
	 */
	private String getChildUri(String prefix, Node node){
		String uri =  this.resolver.getNamespaceForPrefix(prefix, node);
		int index = 0;

		while (uri == null && index < node.getChildNodes().getLength()){
			uri = this.getChildUri(prefix, node.getChildNodes().item(index));
			index++;
		}

		return uri;
	}

	/**
	 * @see javax.xml.namespace.NamespaceContext#getPrefix(java.lang.String)
	 */
	@Override
	public String getPrefix(String namespaceURI) {
		return null;
	}

	/**
	 * @see javax.xml.namespace.NamespaceContext#getPrefixes(java.lang.String)
	 */
	@SuppressWarnings({"rawtypes" })
	@Override
	public Iterator getPrefixes(String namespaceURI) {
		return null;
	}

}