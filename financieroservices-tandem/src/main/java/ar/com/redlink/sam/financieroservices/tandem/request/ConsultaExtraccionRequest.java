/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  03/02/2015 - 22:23:29
 */

package ar.com.redlink.sam.financieroservices.tandem.request;

import ar.com.redlink.sam.financieroservices.tandem.request.base.SAMTandemRequest;

/**
 * Representacion de una peticion de Consulta/Extraccion a Tandem (62/10).
 * 
 * @author aguerrea
 * 
 */
public class ConsultaExtraccionRequest extends SAMTandemRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4870075888477162311L;
	private static final String FORMAT_NAME = "62_REQUEST";
	private static final String CONSULTA_EXT_TRX_CODE = "62";

	/**
	 * Constructor con seteo de valores default.
	 */
	public ConsultaExtraccionRequest() {
		this.setTranCde(CONSULTA_EXT_TRX_CODE);
		this.setFromAcct("0");
		this.setFromAcctTyp("0");
		this.setToAcct("0");
		this.setToAcctTyp("0");
		this.setImporte("0");
		this.setFiller(" ");
		this.setTipoDep(" ");
		this.setClearing(" ");
		this.setTrnData(" ");
		this.setDatosTrx(" ");
		this.setTarjeta(" ");
		this.setCodMon("0");
		this.setTrack2(" ");
		this.setTermName(" ");
	}

	/**
	 * @see ar.com.link.connector.tandem.messages.TandemRequest#getFormatName()
	 */
	@Override
	public String getFormatName() {
		return FORMAT_NAME;
	}
}
