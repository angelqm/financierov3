/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  16/03/2015 - 12:29:16
 */

package ar.com.redlink.sam.financieroservices.tandem.response.base;

import ar.com.link.connector.tandem.messages.TandemResponse;

/**
 * Response base para las transacciones de Tandem para SAM.
 * 
 * @author aguerrea
 * 
 */
public class SAMTandemResponse extends TandemResponse {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1503585285619371800L;

	private String msg;
	private String respuesta;

	/**
	 * 
	 * @see ar.com.link.connector.tandem.messages.TandemRequest#getCorrelationId()
	 */
	@Override
	public String getCorrelationId() {
		correlationId = this.getTranCde() + this.getTermId();

		return correlationId;
	}

	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @param msg
	 *            the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * @return the respuesta
	 */
	public String getRespuesta() {
		return respuesta;
	}

	/**
	 * @param respuesta
	 *            the respuesta to set
	 */
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
}
