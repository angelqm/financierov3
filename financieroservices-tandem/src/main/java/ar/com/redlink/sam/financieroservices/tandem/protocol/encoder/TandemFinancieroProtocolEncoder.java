package ar.com.redlink.sam.financieroservices.tandem.protocol.encoder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

import ar.com.link.connector.tandem.TandemMessage;
import ar.com.link.connector.tandem.impl.MarshallerUnmarshaller;
import ar.com.link.connector.tandem.marshallerunmarshaller.PlainTextMarshallerUnmarshaller;
import ar.com.link.connector.tandem.messages.TandemRequest;

import com.thoughtworks.xstream.XStream;

/**
 * Copia VERBATIM del componente de Encoding de Tandem para Logging. Se
 * removieron atributos y bloques de codigo muertos.
 * 
 * @author aguerrea
 * 
 */
public class TandemFinancieroProtocolEncoder implements ProtocolEncoder {
	private final Log logger = LogFactory.getLog(getClass().getCanonicalName());
	private MarshallerUnmarshaller marshallerUnmarshaller = new PlainTextMarshallerUnmarshaller();

	/**
	 * 
	 * @see org.apache.mina.filter.codec.ProtocolEncoder#encode(org.apache.mina.core.session.IoSession,
	 *      java.lang.Object,
	 *      org.apache.mina.filter.codec.ProtocolEncoderOutput)
	 */
	@Override
	public void encode(IoSession session, Object message,
			ProtocolEncoderOutput out) throws Exception {
		// Leer primero:
		// http://mina.apache.org/tutorial-on-protocolcodecfilter-for-mina-2x.html

		String messagePayload = null;
		TandemMessage request = null;
		try {
			messagePayload = marshall(message);
			
			if (logger.isDebugEnabled()) {
				logger.debug("Mensaje crudo a Tandem: " + messagePayload);
			}
			
			request = (TandemMessage) message;

		} catch (IllegalArgumentException e) {
			logger.fatal("El mensaje a enviar contiene caracteres no validos: "
					+ messagePayload);
			logger.debug("Stack trace: ", e);
		} catch (Throwable e) {
			logger.fatal(
					"No se enviara mensaje por error en encoding de mensaje: "
							+ message, e);
		}

		if (messagePayload != null) {
			// Dado que el simulador usa esta clase a la inversa, no siempre
			// recibe instancias de TandemRequest
			if (request instanceof TandemRequest) {
				TandemRequest tr = (TandemRequest) request;
				tr.setContext(null);
			}
		}

		try {
			if (messagePayload != null) {
				logger.debug("Enviando: " + getMessageId(message) + ": ["
						+ messagePayload + "]");
				byte messageBytes[] = messagePayload.getBytes();
				int capacity = messageBytes.length + 2;

				IoBuffer buffer = IoBuffer.allocate(capacity, false);
				byte[] capacityBytes = intToByteArray(messageBytes.length);
				buffer.put(capacityBytes);

				buffer.put(messageBytes);
				buffer.flip();
				out.write(buffer);
			}
		} catch (Throwable e) {
			logger.fatal("Error en envio de mensaje", e);
		}
	}

	/**
	 * Realiza el marshalling del mensaje a Tandem.
	 * 
	 * @param message
	 * @return String.
	 */
	protected String marshall(Object message) {
		try {
			return marshallerUnmarshaller.marshall(message);
		} catch (RuntimeException e) {
			String messageXml = toXml(message);
			logger.error("Error en marshalling de mensaje: " + messageXml, e);
			throw e;
		} catch (Exception e) {
			String messageXml = toXml(message);
			String errormsg = "Error en marshalling de mensaje: " + messageXml;
			logger.error(errormsg, e);
			throw new RuntimeException("Error en marshalling de mensaje: ", e);
		}
	}

	/**
	 * Obtiene el correlationId del mensaje a Tandem.
	 * 
	 * @param message
	 * @return correlationId.
	 */
	private String getMessageId(Object message) {
		String id;
		if (message instanceof TandemMessage)
			id = ((TandemMessage) message).getCorrelationId();
		else
			id = message.toString();
		return id;
	}

	/**
	 * Convierte un stream a XML.
	 * 
	 * @param message
	 * @return XML.
	 */
	private String toXml(Object message) {
		try {
			return new XStream().toXML(message);
		} catch (Exception e) {
			String msg = "Error armando representacion xml para loguear mensaje: "
					+ message;
			logger.error(msg, e);
			return msg;
		}
	}

	/**
	 * 
	 * @see org.apache.mina.filter.codec.ProtocolEncoder#dispose(org.apache.mina.core.session.IoSession)
	 */
	@Override
	public void dispose(IoSession session) throws Exception {
		// Nada
	}

	/**
	 * Convierte un int a un array de bytes.
	 * 
	 * @param value
	 * @return
	 */
	private static byte[] intToByteArray(int value) {
		return new byte[] { (byte) (value >>> 8), (byte) value };
	}

	/**
	 * Set.
	 * 
	 * @param _marshallerUnmarshaller
	 */
	public void setMarshallerUnmarshaller(
			MarshallerUnmarshaller _marshallerUnmarshaller) {
		marshallerUnmarshaller = _marshallerUnmarshaller;
	}
}