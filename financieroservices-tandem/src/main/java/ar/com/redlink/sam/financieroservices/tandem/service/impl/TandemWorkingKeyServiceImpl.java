/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  16/03/2015 - 18:30:39
 */

package ar.com.redlink.sam.financieroservices.tandem.service.impl;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import ar.com.redlink.sam.financieroservices.dto.request.WorkingKeyRequestDTO;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.tandem.msg.callback.WorkingKeyMsgCallback;
import ar.com.redlink.sam.financieroservices.tandem.request.WorkingKeyRequest;
import ar.com.redlink.sam.financieroservices.tandem.response.WorkingKeyResponse;
import ar.com.redlink.sam.financieroservices.tandem.service.TandemWorkingKeyService;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA200;

/**
 * Implementacion del servicio para manejo de Working Key con Tandem.
 * 
 * @author aguerrea
 * 
 */
public class TandemWorkingKeyServiceImpl extends AbstractTandemBaseService implements TandemWorkingKeyService {

	private static final Logger LOGGER = Logger.getLogger(TandemWorkingKeyServiceImpl.class);

	/**
	 * @see ar.com.redlink.sam.financieroservices.tandem.service.TandemWorkingKeyService#getWorkingKey(ar.com.redlink.sam.financieroservices.dto.request.WorkingKeyRequestDTO)
	 */
	@Override
	public WorkingKeyResponse getWorkingKey(WorkingKeyRequestDTO workingKey) throws FinancieroException {

		WorkingKeyRequest workingKeyRequest = new WorkingKeyRequest();

		workingKeyRequest.setTermFiid(StringUtils.leftPad(workingKey.getIdEntidad(), 4, "0"));
		workingKeyRequest.setTermId(workingKey.getTerminalB24());
		workingKeyRequest.setNroTerminal(workingKey.getTerminalB24());
		
		if (workingKey.getTipoTerminalB24() != null && !workingKey.getTipoTerminalB24().isEmpty()) {
			workingKeyRequest.setTermTyp(workingKey.getTipoTerminalB24());
		} else {
			workingKeyRequest.setTermTyp("60");	
		}

		workingKeyRequest.setTipoEncripcion("1"); // Fijo.
		workingKeyRequest.setTipoWorkingKey(workingKey.getTipo());

		//Setea la secuencia que llega parametrizada.
		workingKeyRequest.setSeqNum(workingKey.getSecuenciaOn());
		
		WorkingKeyMsgCallback callback = new WorkingKeyMsgCallback();

		WorkingKeyResponse response = (WorkingKeyResponse) this.getTandemService().getResponse(this.formatAndSendTandemMessage(workingKeyRequest,callback));

		if ("0".equals(StringUtils.stripStart(response.getSeqNum(), "0"))) {
			LOGGER.error("El numero de secuencia vino en 0, se devuelve una excepcion. ID de Requerimiento: "+ workingKey.getIdRequerimiento());
			throw new FinancieroException(FINA200.getCode(), FINA200.getMsg(), "El numero de secuencia vino en 0, se devuelve una excepcion.");
		}

		return response;
	}
}
