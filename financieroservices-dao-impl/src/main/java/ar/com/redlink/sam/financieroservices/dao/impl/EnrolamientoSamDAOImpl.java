/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  12/04/2015 - 00:20:27
 */

package ar.com.redlink.sam.financieroservices.dao.impl;

import java.util.Date;

import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import ar.com.redlink.framework.persistence.dao.crud.impl.RedLinkGenericDAOImpl;
import ar.com.redlink.sam.financieroservices.dao.EnrolamientoSamDAO;
import ar.com.redlink.sam.financieroservices.entity.EnrolamientoSam;
import ar.com.redlink.sam.financieroservices.entity.util.DateUtil;

/**
 * Implementacion del DAO extendido para {@link EnrolamientoSam}.
 * 
 * 
 * @author aguerrea
 * 
 */
public class EnrolamientoSamDAOImpl extends	RedLinkGenericDAOImpl<EnrolamientoSam> implements EnrolamientoSamDAO {
	
	private static final Logger LOGGER = Logger.getLogger(TrxFinancieroDAOImpl.class);

	/**
	 * 
	 */
	
	private String sgeSchemaName;

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.dao.EnrolamientoSamDAO#enrollDevice(java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	public int enrollDevice(String terminalSamId, String identificadorTerm)	throws Exception {
		LOGGER.info("[DAO INI] enrollDevice");
		
		Session session = this.getSessionFactory().openSession();

		String enrollInsertSql = "INSERT INTO " + sgeSchemaName + ".ENROLAMIENTO_SAM "
				+ "(ENROLAMIENTO_SAM_ID, TERMINAL_SAM_ID, IDENTIFICADOR_TERM, FECHA_ALTA, FECHA_MODIFICACION, ESTADO_REGISTRO_ID) "
				+ "VALUES (" + sgeSchemaName + ".SEQ_ENROLAMIENTO_SAM.NEXTVAL, :terminalSamId, :identificadorTerm,  TO_DATE(:fechaAlta, 'dd/MM/yyyy HH24:MI:SS'), TO_DATE(:fechaAlta, 'dd/MM/yyyy HH24:MI:SS'), :estadoRegistroId)";

		SQLQuery query = session.createSQLQuery(enrollInsertSql);
		query.setParameter("terminalSamId", terminalSamId);
		query.setParameter("identificadorTerm", identificadorTerm);
		query.setParameter("fechaAlta", DateUtil.dateTimeFormat(new Date()));
		query.setParameter("estadoRegistroId", 0);

		Integer result = 0;
		
		try {
			result = query.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			session.close();
		}

		LOGGER.info("[DAO FIN] enrollDevice");
		return result;
	} 
	

	/**
	 * @param sgeSchemaName the sgeSchemaName to set
	 */
	public void setSgeSchemaName(String sgeSchemaName) {
		this.sgeSchemaName = sgeSchemaName;
	}

	
}
