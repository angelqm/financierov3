/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  26/03/2015 - 15:20:43
 */

package ar.com.redlink.sam.financieroservices.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import ar.com.redlink.framework.persistence.dao.crud.impl.RedLinkGenericDAOImpl;
import ar.com.redlink.sam.financieroservices.dao.PrefijoEntidadDAO;
import ar.com.redlink.sam.financieroservices.entity.PrefijoEntidad;

/**
 * Implementacion de PrefijoEntidadDAOImpl con operaciones especificas.
 * 
 * Se utiliza USR_SGE directamente porque no es buena practica inyectar valores estaticos con
 * Spring y el workaround para hacerlo resulta demasiado para el alcance actual.
 * 
 * @author aguerrea
 * 
 */
public class PrefijoEntidadDAOImpl extends
		RedLinkGenericDAOImpl<PrefijoEntidad> implements PrefijoEntidadDAO {
	
	private static final Logger LOGGER = Logger
			.getLogger(TrxFinancieroDAOImpl.class);
	private String sgeSchemaName;

	/**
	 * 
	 * 
	 * @see ar.com.redlink.sam.financieroservices.dao.PrefijoEntidadDAO#getPrefijoByTarjeta(java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	public PrefijoEntidad getPrefijoByTarjeta(String tarjeta, String entidad)
			throws Exception {
		LOGGER.info("[DAO INI] getPrefijoByTarjeta");
		
		String  prefijoByTarjetaSql = "SELECT PE.* from " + sgeSchemaName + ".prefijo_entidad PE INNER JOIN " + sgeSchemaName + ".prefijo P "
				+ "ON PE.prefijo_id= P.prefijo_id "
				+ "WHERE PE.entidad_id = :entidad "
				+ "AND P.prefijo = substr(:tarjeta,1,P.Long_Prefijo) "
				+ "AND P.Long_Tarjeta= :longTarjeta";
		
		Session session = this.getSessionFactory().openSession();
		SQLQuery query = session.createSQLQuery(prefijoByTarjetaSql);
		query.setParameter("entidad", entidad);
		query.setParameter("tarjeta", tarjeta);
		query.setParameter("longTarjeta", tarjeta.length());
		query.addEntity(PrefijoEntidad.class);

		PrefijoEntidad result = null;
		try {
			result = (PrefijoEntidad) query.uniqueResult();
		} catch (Exception e) {
			throw e;
		} finally {
			session.close();
		}

		LOGGER.info("[DAO FIN] getPrefijoByTarjeta");
		return result;
	}

	/**
	 * @param sgeSchemaName the sgeSchemaName to set
	 */
	public void setSgeSchemaName(String sgeSchemaName) {
		this.sgeSchemaName = sgeSchemaName;
	}
	
}
