/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  07/04/2015 - 21:59:37
 */

package ar.com.redlink.sam.financieroservices.dao.impl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StandardBasicTypes;

import ar.com.redlink.framework.persistence.dao.crud.impl.RedLinkGenericDAOImpl;
import ar.com.redlink.sam.financieroservices.dao.TrxFinancieroDAO;
import ar.com.redlink.sam.financieroservices.entity.EstadoTrx;
import ar.com.redlink.sam.financieroservices.entity.Intencion;
import ar.com.redlink.sam.financieroservices.entity.Pago;
import ar.com.redlink.sam.financieroservices.entity.PrefijoOp;
import ar.com.redlink.sam.financieroservices.entity.SimpleTrxFinanciero;
import ar.com.redlink.sam.financieroservices.entity.TerminalSam;
import ar.com.redlink.sam.financieroservices.entity.TrxFinanciero;
import ar.com.redlink.sam.financieroservices.entity.TrxFinancieroValidation;
import ar.com.redlink.sam.financieroservices.entity.util.DateUtil;
import ar.com.redlink.sam.financieroservices.enums.EstadoTrxEnum;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;


/**
 * Implementacion de la extension del DAO para TrxFinanciero.
 * 
 * Se utiliza USR_SGE directamente porque no es buena practica inyectar valores
 * estaticos con Spring y el workaround para hacerlo resulta demasiado para el
 * alcance actual.
 * 
 * @author aguerrea
 * 
 */
public class TrxFinancieroDAOImpl extends RedLinkGenericDAOImpl<TrxFinanciero>	implements TrxFinancieroDAO {

	private String sgeSchemaName;
	
	private static final Logger LOGGER = Logger.getLogger(TrxFinancieroDAOImpl.class);

	/**
	 * @see ar.com.redlink.sam.financieroservices.dao.TrxFinancieroDAO#insert(ar.com.redlink.sam.financieroservices.entity.SimpleTrxFinanciero)
	 */
	@Override
	@Deprecated
	public Integer insert(SimpleTrxFinanciero simpleTrxFinanciero)	throws Exception {
		LOGGER.info("[DAO INI] insert: " + simpleTrxFinanciero.getIdRequerimiento());
		
		Session session = this.getSessionFactory().openSession();
		
		String trxFinancieroInsertSql = "INSERT INTO " + sgeSchemaName + ".TRX_FINANCIERO "
				+ "(TRX_FINANCIERO_ID, ENTIDAD_ID, AGENTE_ID, RED_ID, SUCURSAL_ID, SERVIDOR_ID,	TERMINAL_SAM_ID, PREFIJO_OP_ID,	TOKENIZADO, "
				+ "IMPORTE, FECHA_HORA_ALTA, ESTADO_TRX_ID, SECUENCIA_ON_WS, ID_REQUERIMIENTO_ALTA, FECHA_ON, SECUENCIA_ON_B24, ID_REQUERIMIENTO_BAJA, OPERACION_PEI)"
				+ "VALUES (" + sgeSchemaName + ".SEQ_TRX_FINANCIERO.NEXTVAL, :entidadId, :agenteId, :redId, :sucursalId, :servidorId, :terminalSamId, :prefijoOpId, :tokenizado, TO_NUMBER(:importe , '9999999999999999999.99'), "
				+ "TO_DATE(:fechaHoraAlta, 'dd/mm/yyyy HH24:MI:SS'), :estadoTrxId, :secuenciaOnWS, :idRequerimientoAlta, TO_DATE(:fechaOn, 'yy/mm/dd'), :secuenciaOnB24, :idRequerimientoBaja, :operacionPei)";		
		
		SQLQuery query = session.createSQLQuery(trxFinancieroInsertSql);

		query.setParameter("entidadId", simpleTrxFinanciero.getIdEntidad());
		query.setParameter("agenteId", simpleTrxFinanciero.getAgenteId());
		query.setParameter("redId", simpleTrxFinanciero.getRedId());
		query.setParameter("sucursalId", simpleTrxFinanciero.getSucursal());
		query.setParameter("servidorId", simpleTrxFinanciero.getServidorId());
		query.setParameter("terminalSamId",	simpleTrxFinanciero.getTerminalSamId());
		query.setParameter("prefijoOpId", simpleTrxFinanciero.getPrefijoOpId());
		query.setParameter("tokenizado", simpleTrxFinanciero.getTokenizado());
		query.setParameter("importe", simpleTrxFinanciero.getImporte());
		query.setParameter("fechaHoraAlta", DateUtil.dateTimeFormat(new Date()));
		query.setParameter("estadoTrxId", simpleTrxFinanciero.getEstadoTrxId());
		query.setParameter("secuenciaOnWS",simpleTrxFinanciero.getSecuenciaOnWS());
		query.setParameter("secuenciaOnB24",simpleTrxFinanciero.getSecuenciaOnB24());

		try {
			Date fechaOn = DateUtil.parseFechaOnExt(simpleTrxFinanciero.getFechaOn());
			query.setParameter("fechaOn", DateUtil.fechaOnExtFormat(fechaOn));
		} catch (ParseException e) {
			LOGGER.error("Error de parseo de fechaOn: " + e.getMessage());
			throw e;
		}

		query.setParameter("idRequerimientoAlta",simpleTrxFinanciero.getIdRequerimiento());		
		query.setParameter("idRequerimientoBaja",simpleTrxFinanciero.getIdRequerimientoBaja());
		query.setParameter("operacionPei",simpleTrxFinanciero.getOperacionPei(), StandardBasicTypes.LONG);		

		Integer result = 0;

		try {
			result = query.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			 session.close();
		}

		LOGGER.info("[DAO FIN] insert: " + simpleTrxFinanciero.getIdRequerimiento());
		return result;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.dao.TrxFinancieroDAO#updateEntry(java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	@Deprecated
	public Integer updateEntry(Long trxFinancieroId,String idRequerimientoBaja, String estadoTrx) throws Exception {
		LOGGER.info("[DAO INI] updateEntry: " + trxFinancieroId + " " + idRequerimientoBaja + " " +  estadoTrx);
		
		Session session = this.getSessionFactory().openSession();

		String updateTrxFinancieroAnnul = "UPDATE " + sgeSchemaName + ".TRX_FINANCIERO SET "
				+ "ID_REQUERIMIENTO_BAJA = :idRequerimientoBaja, "
				+ "FECHA_HORA_BAJA = TO_DATE(:fechaHoraBaja, 'dd/mm/yyyy HH24:MI:SS'),"
				+ "ESTADO_TRX_ID = :estadoTrx "
				+ "where TRX_FINANCIERO_ID = :trxFinancieroId";
		
		SQLQuery query = session.createSQLQuery(updateTrxFinancieroAnnul);

		query.setParameter("idRequerimientoBaja", idRequerimientoBaja);
		query.setParameter("trxFinancieroId", trxFinancieroId);
		query.setParameter("fechaHoraBaja", DateUtil.dateTimeFormat(new Date()));
		query.setParameter("estadoTrx", estadoTrx);

		Integer result = 0;

		try {
			result = query.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			session.close();
		}

		LOGGER.info("[DAO FIN] updateEntry" + trxFinancieroId);
		return result;
	}
	
	@Override
	public void rollbackWithdraw(Long trxFinancieroId, String idRequerimientoBaja, String estadoTrx) throws Exception {
		LOGGER.info("[DAO INI] rollbackWithdraw: " + trxFinancieroId + " " + idRequerimientoBaja + " " +  estadoTrx);
	/*	
		String updateTrxFinancieroAnnul = "UPDATE " + sgeSchemaName + ".TRX_FINANCIERO SET "
				+ "ID_REQUERIMIENTO_BAJA = :idRequerimientoBaja, "
				+ "FECHA_HORA_BAJA = TO_DATE(:fechaHoraBaja, 'dd/mm/yyyy HH24:MI:SS'),"
				+ "ESTADO_TRX_ID = :estadoTrx "
				+ "where TRX_FINANCIERO_ID = :trxFinancieroId";
		
		SQLQuery query = session.createSQLQuery(updateTrxFinancieroAnnul);

		query.setParameter("idRequerimientoBaja", idRequerimientoBaja);
		query.setParameter("trxFinancieroId", trxFinancieroId);
		query.setParameter("fechaHoraBaja", DateUtil.dateTimeFormat(new Date()));
		query.setParameter("estadoTrx", estadoTrx);
		*/
		
		Session session = this.getSessionFactory().openSession();		
		
		Criteria criteria = session.createCriteria(Pago.class);
		criteria.add(Restrictions.eq("intencionId.intencionId", trxFinancieroId));
		Pago toUpdate= (Pago) criteria.uniqueResult();
		toUpdate.setIdRequerimientoBaja(idRequerimientoBaja);
		toUpdate.setFechaHoraBaja(new Date());
		
		session.update(toUpdate);
		session.flush();
		LOGGER.info("[DAO FIN] reverso (rollbackWithdraw)");
		
		session.close();
	}
	
	@Override
	public void reverso(Long trxFinancieroId, String idRequerimientoBaja, String estadoTrx) throws Exception {
		LOGGER.info("[DAO INI] reverso: " + trxFinancieroId + " " + idRequerimientoBaja + " " +  estadoTrx);
		
		/*busco el pago, asociado a la intencion con id trxFinancieroId, 
		luego se actualiza el pago colocandole el estado 2 y poniendo en id_requerimiento_baja, el id que me llega
		admas insertar un nuevo pago con el nuevoidreq el estado cero y el tipo que ewl pago anterior*/
				
		Session session = this.getSessionFactory().openSession();		
		
		Criteria criteria = session.createCriteria(Pago.class);
		criteria.add(Restrictions.eq("intencionId.intencionId", trxFinancieroId));
		
		/*1.- hago update del pago asociado a la intencion (idReqbaja y la fecha)*/
		Pago toUpdate= (Pago) criteria.uniqueResult();
		toUpdate.setIdRequerimientoBaja(idRequerimientoBaja);
		toUpdate.setFechaHoraBaja(new Date());
		session.update(toUpdate);
				
		/*2.- creo una nueva intencion con lo que me llego*/
		
		/*2.1 obtengo la intencion original*/
		Criteria criteria2 = session.createCriteria(Intencion.class);
		criteria2.add(Restrictions.eq("intencionId", trxFinancieroId));
		Intencion intencionOriginal = (Intencion) criteria2.uniqueResult();
		
		/*2.2 copio los datos de la intencion original+ idRequerimientoBaja*/
		Intencion newIntencion = new Intencion();
		newIntencion.setPrefijoOpId(intencionOriginal.getPrefijoOpId());
		//newIntencion.setObservacion(intencionOriginal.getObservacion());
		newIntencion.setTerminalSamId(intencionOriginal.getTerminalSamId());
		newIntencion.setEstadoTrxId(new EstadoTrx((byte) EstadoTrxEnum.REVERSADA.getEstadoTrxId()));
		newIntencion.setRequerimientoId(idRequerimientoBaja);
		newIntencion.setFechaInicioIntencion(new Date());
		newIntencion.setFechaFinIntencion(new Date());
		newIntencion.setObservacion("REVERSO OK");
		
		/*3.- creo un nuevo pago, con el idReqBaja que me llego (y los mismos datos del pago anterios)*/
		Pago newPago= new Pago();
		newPago.setFechaHoraAlta(new Date());
		newPago.setFechaHoraBaja(new Date());
		newPago.setFechaOn(toUpdate.getFechaOn());
		//newPago.setIdRequerimientoBaja(idRequerimientoBaja);
		newPago.setImporte(toUpdate.getImporte());
		newPago.setIntencionId(toUpdate.getIntencionId());
		newPago.setOperacionPei(toUpdate.getOperacionPei());
		newPago.setSecuenciaOnB24(toUpdate.getSecuenciaOnB24());
		newPago.setSecuenciaOnwS(toUpdate.getSecuenciaOnwS());
		newPago.setTokenizado(toUpdate.getTokenizado());
		newPago.setIntencionId(newIntencion);
		
		session.save(newPago);
		session.flush();
		LOGGER.info("[DAO FIN] reverso");
		
		session.close();
	}
	
	@Override
	public void anullDebit(Long trxFinancieroId, String idRequerimientoBaja, String estadoTrx, Intencion in) throws Exception {
		LOGGER.info("[DAO INI] anullDebit: " + trxFinancieroId + " " + idRequerimientoBaja + " " +  estadoTrx);
		
		/*busco el pago, asociado a la intencion con id trxFinancieroId, 
		luego se actualiza el pago colocandole el estado 2 y poniendo en id_requerimiento_baja, el id que me llega
		admas insertar un nuevo pago con el nuevoidreq el estado cero y el tipo que ewl pago anterior*/
		
		Session session = this.getSessionFactory().openSession();		
		
		Criteria criteria = session.createCriteria(Pago.class);
		criteria.add(Restrictions.eq("intencionId.intencionId", trxFinancieroId));
		
		/*1.- hago update del pago asociado a la intencion (idReqbaja y la fecha)*/
		Pago toUpdate= (Pago) criteria.uniqueResult();
		toUpdate.getIntencionId().setEstadoTrxId(new EstadoTrx((byte)(Long.valueOf(estadoTrx).longValue())));
		session.update(toUpdate);
				
		/*2.- creo una nueva intencion con lo que me llego*/
		
		/*2.1 obtengo la intencion original*/
		Criteria criteria2 = session.createCriteria(Intencion.class);
		criteria2.add(Restrictions.eq("intencionId", trxFinancieroId));
		Intencion intencionOriginal = (Intencion) criteria2.uniqueResult();
		
		in.setTerminalSamId(intencionOriginal.getTerminalSamId());
		in.setEstadoTrxId(new EstadoTrx((byte) EstadoTrxEnum.ANULADA.getEstadoTrxId()));
		in.setRequerimientoId(idRequerimientoBaja);
		in.setFechaFinIntencion(new Date());
		in.setObservacion("ANULACION OK");
		
		/*3.- creo un nuevo pago, con el idReqBaja que me llego (y los mismos datos del pago anterios)*/
		Pago newPago= new Pago();
		newPago.setFechaHoraAlta(new Date());
		newPago.setFechaHoraBaja(new Date());
		newPago.setFechaOn(toUpdate.getFechaOn());
		newPago.setIdRequerimientoBaja(intencionOriginal.getRequerimientoId());
		newPago.setImporte(toUpdate.getImporte());
		newPago.setIntencionId(toUpdate.getIntencionId());
		newPago.setOperacionPei(toUpdate.getOperacionPei());
		newPago.setSecuenciaOnB24(toUpdate.getSecuenciaOnB24());
		newPago.setSecuenciaOnwS(toUpdate.getSecuenciaOnwS());
		newPago.setTokenizado(toUpdate.getTokenizado());
		newPago.setIntencionId(in);
		
		session.save(newPago);
		session.flush();
		LOGGER.info("[DAO FIN] anullDebit");
		
		session.close();
		
	}
	
	public void cancelExtraction(Long trxFinancieroId,String idRequerimientoBaja, String estadoTrx, Intencion in) throws Exception {
		LOGGER.info("[DAO INI] cancelExtraction: " + trxFinancieroId + " " + idRequerimientoBaja + " " +  estadoTrx);
			
		Session session = this.getSessionFactory().openSession();		
		
		Criteria criteria = session.createCriteria(Pago.class);
		criteria.add(Restrictions.eq("intencionId.intencionId", trxFinancieroId));
		
		/*1.- hago update del pago asociado a la intencion (idReqbaja y la fecha)*/
		Pago toUpdate= (Pago) criteria.uniqueResult();
		toUpdate.getIntencionId().setEstadoTrxId(new EstadoTrx((byte)(Long.valueOf(estadoTrx).longValue())));
		session.update(toUpdate);
				
		/*2.- creo una nueva intencion con lo que me llego*/
		
		/*2.1 obtengo la intencion original*/
		Criteria criteria2 = session.createCriteria(Intencion.class);
		criteria2.add(Restrictions.eq("intencionId", trxFinancieroId));
		Intencion intencionOriginal = (Intencion) criteria2.uniqueResult();
		
		in.setTerminalSamId(intencionOriginal.getTerminalSamId());
		in.setRequerimientoId(idRequerimientoBaja);
		in.setFechaFinIntencion(new Date());
		in.setEstadoTrxId(new EstadoTrx((byte) EstadoTrxEnum.ANULADA.getEstadoTrxId()));
		in.setObservacion("ANULACION DE EXTRACCION OK");
				
		/*3.- creo un nuevo pago, con el idReqBaja que me llego (y los mismos datos del pago anterios)*/
		Pago newPago= new Pago();
		newPago.setFechaHoraAlta(new Date());
		newPago.setFechaHoraBaja(new Date());
		newPago.setFechaOn(toUpdate.getFechaOn());
		newPago.setIdRequerimientoBaja(intencionOriginal.getRequerimientoId());
		newPago.setImporte(toUpdate.getImporte());
		newPago.setIntencionId(toUpdate.getIntencionId());
		newPago.setOperacionPei(toUpdate.getOperacionPei());
		newPago.setSecuenciaOnB24(toUpdate.getSecuenciaOnB24());
		newPago.setSecuenciaOnwS(toUpdate.getSecuenciaOnwS());
		newPago.setTokenizado(toUpdate.getTokenizado());
		newPago.setIntencionId(in);
		
		session.save(newPago);
		session.flush();
		
		LOGGER.info("[DAO FIN] cancelExtraction");
		
		session.close();
	}
	
	public void rollbackDeposit(Long trxFinancieroId,String idRequerimientoBaja, String estadoTrx, Intencion in) throws Exception {
		LOGGER.info("[DAO INI] rollbackDeposit: " + trxFinancieroId + " " + idRequerimientoBaja + " " +  estadoTrx);
			
		Session session = this.getSessionFactory().openSession();		
		
		Criteria criteria = session.createCriteria(Pago.class);
		criteria.add(Restrictions.eq("intencionId.intencionId", trxFinancieroId));
		
		/*1.- hago update del pago asociado a la intencion (idReqbaja y la fecha)*/
		Pago toUpdate= (Pago) criteria.uniqueResult();
		toUpdate.getIntencionId().setEstadoTrxId(new EstadoTrx((byte)(Long.valueOf(estadoTrx).longValue())));
		session.update(toUpdate);
				
		/*2.- creo una nueva intencion con lo que me llego*/
		
		/*2.1 obtengo la intencion original*/
		Criteria criteria2 = session.createCriteria(Intencion.class);
		criteria2.add(Restrictions.eq("intencionId", trxFinancieroId));
		Intencion intencionOriginal = (Intencion) criteria2.uniqueResult();
		
		in.setTerminalSamId(intencionOriginal.getTerminalSamId());
		in.setRequerimientoId(idRequerimientoBaja);
		in.setFechaFinIntencion(new Date());
		in.setEstadoTrxId(new EstadoTrx((byte) EstadoTrxEnum.ANULADA.getEstadoTrxId()));
		in.setObservacion("ANULACION DE DEPOSITO OK");
				
		/*3.- creo un nuevo pago, con el idReqBaja que me llego (y los mismos datos del pago anterios)*/
		Pago newPago= new Pago();
		newPago.setFechaHoraAlta(new Date());
		newPago.setFechaHoraBaja(new Date());
		newPago.setFechaOn(toUpdate.getFechaOn());
		newPago.setIdRequerimientoBaja(intencionOriginal.getRequerimientoId());
		newPago.setImporte(toUpdate.getImporte());
		newPago.setIntencionId(toUpdate.getIntencionId());
		newPago.setOperacionPei(toUpdate.getOperacionPei());
		newPago.setSecuenciaOnB24(toUpdate.getSecuenciaOnB24());
		newPago.setSecuenciaOnwS(toUpdate.getSecuenciaOnwS());
		newPago.setTokenizado(toUpdate.getTokenizado());
		newPago.setIntencionId(in);
		
		session.save(newPago);
		session.flush();
		
		LOGGER.info("[DAO FIN] rollbackDeposit");
		
		session.close();
	}
	
	
	public void rollBackDebitPay(Long trxFinancieroId,String idRequerimientoBaja, String estadoTrx, Intencion in) throws Exception {
		LOGGER.info("[DAO INI] rollBackDebitPay: " + trxFinancieroId + " " + idRequerimientoBaja + " " +  estadoTrx);
			
		Session session = this.getSessionFactory().openSession();		
		
		Criteria criteria = session.createCriteria(Pago.class);
		criteria.add(Restrictions.eq("intencionId.intencionId", trxFinancieroId));
		
		/*1.- hago update del pago asociado a la intencion (idReqbaja y la fecha)*/
		Pago toUpdate= (Pago) criteria.uniqueResult();
		toUpdate.getIntencionId().setEstadoTrxId(new EstadoTrx((byte)(Long.valueOf(estadoTrx).longValue())));
		session.update(toUpdate);
				
		/*2.- creo una nueva intencion con lo que me llego*/
		
		/*2.1 obtengo la intencion original*/
		Criteria criteria2 = session.createCriteria(Intencion.class);
		criteria2.add(Restrictions.eq("intencionId", trxFinancieroId));
		Intencion intencionOriginal = (Intencion) criteria2.uniqueResult();
		
		in.setTerminalSamId(intencionOriginal.getTerminalSamId());
		in.setRequerimientoId(idRequerimientoBaja);
		in.setFechaFinIntencion(new Date());
		in.setEstadoTrxId(new EstadoTrx((byte) EstadoTrxEnum.ANULADA.getEstadoTrxId()));
		in.setObservacion("ANULACION PAGO DEBITO OK");
			
		/*3.- creo un nuevo pago, con el idReqBaja que me llego (y los mismos datos del pago anterios)*/
		Pago newPago= new Pago();
		newPago.setFechaHoraAlta(new Date());
		newPago.setFechaHoraBaja(new Date());
		newPago.setFechaOn(toUpdate.getFechaOn());
		newPago.setIdRequerimientoBaja(intencionOriginal.getRequerimientoId());
		newPago.setImporte(toUpdate.getImporte());
		newPago.setIntencionId(toUpdate.getIntencionId());
		newPago.setOperacionPei(toUpdate.getOperacionPei());
		newPago.setSecuenciaOnB24(toUpdate.getSecuenciaOnB24());
		newPago.setSecuenciaOnwS(toUpdate.getSecuenciaOnwS());
		newPago.setTokenizado(toUpdate.getTokenizado());
		newPago.setIntencionId(in);
		
		session.save(newPago);
		session.flush();
		
		LOGGER.info("[DAO FIN] rollBackDebitPay");
		session.close();
	}
	
	
	/**
	 * @see ar.com.redlink.sam.financieroservices.dao.TrxFinancieroDAO#updateEntry(java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	@Deprecated
	public Integer updateEntryPei(String idRequerimientoOrig,String idRequerimientoBaja, String estadoTrx, String prefijoOpId) throws Exception {
		Session session = this.getSessionFactory().openSession();

		String updateTrxFinancieroAnnul = "UPDATE " + sgeSchemaName + ".TRX_FINANCIERO SET "
				+ "ID_REQUERIMIENTO_BAJA = :idRequerimientoBaja, "
				+ "FECHA_HORA_BAJA = TO_DATE(:fechaHoraBaja, 'dd/mm/yyyy HH24:MI:SS'),"
				+ "ESTADO_TRX_ID = :estadoTrx "
				+ "where ID_REQUERIMIENTO_ALTA = :idRequerimientoOrig "
				+ "and PREFIJO_OP_ID = :prefijoOpId";
		
		SQLQuery query = session.createSQLQuery(updateTrxFinancieroAnnul);

		query.setParameter("idRequerimientoBaja", idRequerimientoBaja);
		query.setParameter("idRequerimientoOrig", idRequerimientoOrig);
		query.setParameter("fechaHoraBaja", DateUtil.dateTimeFormat(new Date()));
		query.setParameter("estadoTrx", estadoTrx);
		query.setParameter("prefijoOpId", prefijoOpId);

		Integer result = 0;

		try {
			result = query.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			session.close();
		}

		return result;
	}
	
	
	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.dao.TrxFinancieroDAO#getSum(java.lang.String,
	 *      java.lang.String, java.util.Date, java.util.Date, java.lang.String)
	 */
	@Override
	public Double getSum(String filterField, String filterId, Date dateFrom, Date dateTo, String tokenizado, Long prefijoOpId) throws Exception {
		LOGGER.info("[DAO INI] getSum "+tokenizado+" PREFIJO_OP_ID "+prefijoOpId);
		
		Session session = this.getSessionFactory().openSession();
		/*SQLQuery query = prepareGetQuery("SELECT SUM(IMPORTE) ", filterField, filterId, session, dateFrom, dateTo, tokenizado, prefijoOpId);

		BigDecimal value = BigDecimal.ZERO;

		try {
			Object obj = query.uniqueResult();
			if (null != obj) {
				value = (BigDecimal) obj;
			}
		} catch (Exception e) {
			throw e;
		} finally {
			session.close();
		}
		
		LOGGER.info("[DAO FIN] getSum");
		
		return value.doubleValue();*/
		
		/*AQM - BUG 3344-Se considera la resta de las anulaciones(prefijoOpId+1)
		 * SOLO OPERACIONES LINK
		 * 5	Extracción
		 * 7	Depósito
		 * 9	Extraccion Prestamos
		 * 11 	Debito BU
		 * 13 	Debito Cta
		 * 27 	Pago con Débito
		 * Las operaciones PEI las valida enlacePagos/PRISMA-BANELCO
		 * 23 Pago PEI
		 * 29 Pago PEI-PAGAR
		 * */
		 String strLisOP = "5-7-9-11-13-27";		 
		 String strOP = Long.toString(prefijoOpId)+"-";					 
		 String q;
		 /*
		 if (strLisOP.indexOf(strOP) !=-1) {
				q="SELECT "
						+ "	SUM(CASE  WHEN PREFIJO_OP_ID=" +prefijoOpId +" "+" THEN ACP.IMPORTE ELSE  - ACP.IMPORTE END) AS IMPORTE "
						+ "FROM "
						+  sgeSchemaName+".ACUMULADO_PARCIAL ACP "
						+ "WHERE"
						+ "	TOKENIZADO ='"+tokenizado+"' "
						+ " AND PREFIJO_OP_ID IN (" +prefijoOpId +","+ (prefijoOpId+1) +") " 				
						+ " AND FECHA_TRX BETWEEN TO_DATE ('"+ DateUtil.dateTimeFormat(dateFrom)+"', 'dd/mm/yyyy HH24:MI:SS') AND TO_DATE('"+ DateUtil.dateTimeFormat(dateTo)+"', 'dd/mm/yyyy HH24:MI:SS')";
		 }
		 else{
				q="SELECT "
						+ "	ACP.IMPORTE "
						+ "FROM "
						+  sgeSchemaName+".ACUMULADO_PARCIAL ACP "
						+ "WHERE"
						+ "	TOKENIZADO ='"+tokenizado+"' "
						+ " AND PREFIJO_OP_ID="+prefijoOpId				
						+ " AND FECHA_TRX BETWEEN TO_DATE ('"+ DateUtil.dateTimeFormat(dateFrom)+"', 'dd/mm/yyyy HH24:MI:SS') AND TO_DATE('"+ DateUtil.dateTimeFormat(dateTo)+"', 'dd/mm/yyyy HH24:MI:SS')";
			 
		 	}	 	
		*/
			/*AQM
			 * PARA LAS BUSQUEDAS: CANTIDAD/MONTO ES POR:
			 * Entidad/Red/Agente/Sucursal ESTAN EN USR_SGE.SUCURSAL SUC
			 * Servidor ESTAN EN USR_SGE.SERVIDOR SER
			 * Terminal ESTAN EN USR_SGE.TERMINAL_SAM TER
			 * */		 
		 if (strLisOP.indexOf(strOP) !=-1) {
				q="SELECT "
						+ "	SUM(CASE  WHEN ACT.PREFIJO_OP_ID=" +prefijoOpId +" "+" THEN ACT.IMPORTE ELSE  - ACT.IMPORTE END) AS IMPORTE "
						+ "FROM "
						+  sgeSchemaName+".ACUMULADO_TOTAL ACT, "
						+  sgeSchemaName+".TERMINAL_SAM TER, "
			         	+  sgeSchemaName+".SERVIDOR SER, "
			         	+  sgeSchemaName+".SUCURSAL SUC "         
			         	+ "	WHERE "    
			         	+ "	ACT.TERMINAL_SAM_ID = TER.TERMINAL_SAM_ID "  
			         	+ "	AND TER.SERVIDOR_ID = SER.SERVIDOR_ID " 
			         	+ "	AND  SER.SUCURSAL_ID = SUC.SUCURSAL_ID " ;
						if ("Terminal_id".equals(filterField)) {
							q +="	AND TER."+filterField +"="+filterId ;
						} else if ("Servidor_id".equals(filterField)) {
								q +="	AND SER."+filterField +"="+filterId ;
							}else {
										q +="	AND SUC."+filterField +"="+filterId ;
						}						
						q += "	AND ACT.TOKENIZADO ='"+tokenizado+"' "
						+ " AND ACT.PREFIJO_OP_ID IN (" +prefijoOpId +","+ (prefijoOpId+1) +") " 				
						+ " AND ACT.FECHA_TRX BETWEEN TO_DATE ('"+ DateUtil.dateTimeFormat(dateFrom)+"', 'dd/mm/yyyy HH24:MI:SS') AND TO_DATE('"+ DateUtil.dateTimeFormat(dateTo)+"', 'dd/mm/yyyy HH24:MI:SS')";
		 }
		 else{
				q="SELECT "
						+ "	SUM(ACP.IMPORTE) AS IMPORTE "
						+ "FROM "
						+  sgeSchemaName+".ACUMULADO_TOTAL ACT, "
						+  sgeSchemaName+".TERMINAL_SAM TER, "
			         	+  sgeSchemaName+".SERVIDOR SER, "
			         	+  sgeSchemaName+".SUCURSAL SUC "         
			         	+ "	WHERE "    
			         	+ "	ACT.TERMINAL_SAM_ID = TER.TERMINAL_SAM_ID "  
			         	+ "	AND TER.SERVIDOR_ID = SER.SERVIDOR_ID " 
			         	+ "	AND  SER.SUCURSAL_ID = SUC.SUCURSAL_ID " ;
						if ("Terminal_id".equals(filterField)) {
							q +="	AND TER."+filterField +"="+filterId ;
						} else if ("Servidor_id".equals(filterField)) {
								q +="	AND SER."+filterField +"="+filterId ;
							}else {
										q +="	AND SUC."+filterField +"="+filterId ;
						}						
						q += "	AND ACT.TOKENIZADO ='"+tokenizado+"' "
						+ " AND ACT.PREFIJO_OP_ID="+prefijoOpId				
						+ " AND ACT.FECHA_TRX BETWEEN TO_DATE ('"+ DateUtil.dateTimeFormat(dateFrom)+"', 'dd/mm/yyyy HH24:MI:SS') AND TO_DATE('"+ DateUtil.dateTimeFormat(dateTo)+"', 'dd/mm/yyyy HH24:MI:SS')";
			 
		 	}

		SQLQuery query2=session.createSQLQuery(q);
		Object acPa = query2.uniqueResult();

		if(acPa!=null){
			BigDecimal sum=(BigDecimal) acPa;
			LOGGER.info("[DAO INI] getSum "+sum);
			return sum.doubleValue();
		}
		else{
			LOGGER.info("[DAO INI] getSum "+0);
			return (double) 0;
		}
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.dao.TrxFinancieroDAO#getCount(java.lang.String,
	 *      java.lang.String, java.util.Date, java.util.Date, java.lang.String)
	 */
	@Override
	public Integer getCount(String filterField, String filterId, Date dateFrom,	Date dateTo, String tokenizado, Long prefijoOpId) throws Exception {
		LOGGER.info("[DAO INI] getCount "+tokenizado+" SUCURSAL_ID "+filterId+" PREFIJO_OP_ID "+prefijoOpId);
		
		Session session = this.getSessionFactory().openSession();

		/*SQLQuery query = prepareGetQuery("SELECT COUNT(" + filterField + ")",
				filterField, filterId, session, dateFrom, dateTo, tokenizado,
				prefijoOpId);

		BigDecimal value = BigDecimal.ZERO;*/

		/*try {
			Object obj = query.uniqueResult();
			if (null != obj) {
				value = (BigDecimal) obj;
			}
		} catch (Exception e) {
			throw e;
		} finally {
			session.close();
		}*/
				
/*
 * 
 *   
 * */		
		/*
		String q="SELECT "
				+ "	ACT.CANTIDAD_TRX "
				+ "FROM "
				+  sgeSchemaName+".ACUMULADO_TOTAL ACT,"+sgeSchemaName+".TERMINAL_SAM TSA,"+sgeSchemaName+".SERVIDOR SER "
				+ "WHERE"
				+ "	ACT.TERMINAL_SAM_ID= TSA.TERMINAL_SAM_ID"
				+ "	AND TSA.SERVIDOR_ID=SER.SERVIDOR_ID"
				+ " AND SER.SUCURSAL_ID="+filterId
				+ "	AND TOKENIZADO ='"+tokenizado+"' "
				+ " AND PREFIJO_OP_ID="+prefijoOpId
				+ " AND FECHA_TRX BETWEEN TO_DATE ('"+ DateUtil.dateTimeFormat(dateFrom)+"', 'dd/mm/yyyy HH24:MI:SS') AND TO_DATE('"+ DateUtil.dateTimeFormat(dateTo)+"', 'dd/mm/yyyy HH24:MI:SS')";
		*/
		/*AQM
		 * PARA LAS BUSQUEDAS: CANTIDA/MONTO ES POR:
		 * Entidad/Red/Agente/Sucursal ESTAN EN USR_SGE.SUCURSAL SUC
		 * Servidor ESTAN EN USR_SGE.SERVIDOR SER
		 * Terminal ESTAN EN USR_SGE.TERMINAL_SAM TER
		 * */
		String q="SELECT SUM(ACT.CANTIDAD_TRX) AS CANTIDAD_TRX " 
				+ "FROM "
				+  sgeSchemaName+".ACUMULADO_TOTAL ACT, " 
				+  sgeSchemaName+".TERMINAL_SAM TER, "
	         	+  sgeSchemaName+".SERVIDOR SER, "
	         	+  sgeSchemaName+".SUCURSAL SUC "         
	         	+ "	WHERE "    
	         	+ "	ACT.TERMINAL_SAM_ID = TER.TERMINAL_SAM_ID "  
	         	+ "	AND TER.SERVIDOR_ID = SER.SERVIDOR_ID " 
	         	+ "	AND  SER.SUCURSAL_ID = SUC.SUCURSAL_ID " ;
				
				if ("Terminal_id".equals(filterField)) {
					q +="	AND TER."+filterField +"="+filterId ;
				} else if ("Servidor_id".equals(filterField)) {
						q +="	AND SER."+filterField +"="+filterId ;
					}else  {
								q +="	AND SUC."+filterField +"="+filterId ;
				}				
				q += "	AND ACT.TOKENIZADO ='"+tokenizado+"' "
				+ " AND ACT.PREFIJO_OP_ID="+prefijoOpId
				+ " AND ACT.FECHA_TRX BETWEEN TO_DATE ('"+ DateUtil.dateTimeFormat(dateFrom)+"', 'dd/mm/yyyy HH24:MI:SS') AND TO_DATE('"+ DateUtil.dateTimeFormat(dateTo)+"', 'dd/mm/yyyy HH24:MI:SS')";
	        
		
		SQLQuery query2=session.createSQLQuery(q);
		Object acTo = query2.uniqueResult();

		if(acTo!=null){
			BigDecimal count=(BigDecimal) acTo;
			LOGGER.info("[DAO INI] getCount "+count);
			return count.intValue();
		}
		else{
			LOGGER.info("[DAO INI] getCount "+0);
			return 0;
		}
		//return value.intValue();
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.dao.TrxFinancieroDAO#getReversableOperationIds(java.lang.String,
	 *      java.lang.String, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	@Deprecated
	public List<Long> getReversableOperationIds(String seqNum, String terminalSamId, String notReversables) throws Exception {
		LOGGER.info("[DAO INI] getReversableOperationIds");
		
		Session session = this.getSessionFactory().openSession();
		
		String getValueQuery = " from " + sgeSchemaName + ".TRX_FINANCIERO WHERE estado_trx_id = 0 AND ";
		
		SQLQuery query = session.createSQLQuery("SELECT TRX_FINANCIERO_ID "
				+ getValueQuery + " SECUENCIA_ON_WS = :seqNum "
				+ "AND PREFIJO_OP_ID NOT IN " + "(" + notReversables + ")"
				+ " AND TERMINAL_SAM_ID = :terminalSamId ")
				.addScalar("TRX_FINANCIERO_ID",  new LongType());

		query.setParameter("seqNum", seqNum);
		query.setParameter("terminalSamId", terminalSamId);

		List<Long> result = null;

		try {
			result = query.list();
		} catch (Exception e) {
			throw e;
		} finally {
			session.close();
		}
		
		LOGGER.info("[DAO FIN] getReversableOperationIds");
		return result;
	}
	
	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.dao.TrxFinancieroDAO#getReversableOperationIds(java.lang.String,
	 *      java.lang.String, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Long getReversableOperationIdsAct(String seqNum, String terminalSamId, String notReversables) throws Exception {
		LOGGER.info("[DAO INI] getReversableOperationIdsAct "+seqNum+" "+terminalSamId+" "+notReversables);
		
		Session session = this.getSessionFactory().openSession();
		
		SQLQuery query2=session.createSQLQuery("SELECT IT.INTENCION_ID FROM "+
						sgeSchemaName+".INTENCION IT, "+sgeSchemaName+".PAGO PG "
						+ "WHERE "
						+ "IT.INTENCION_ID=PG.INTENCION_ID "
						+ "AND IT.ESTADO_TRX_ID=0 "
						+ "AND PG.SECUENCIA_ON_WS='"+seqNum+"' "
						+" AND IT.PREFIJO_OP_ID NOT IN ("+notReversables+") "
						+ "AND IT.TERMINAL_SAM_ID="+terminalSamId);
		
		List<Long> result = null;
		
		try {
			result = query2.list();
			LOGGER.info("[DAO FIN] getReversableOperationIdsAct");
			if(result!=null  && !result.isEmpty()){
				java.math.BigDecimal bd=(BigDecimal) query2.list().get(0);
				return bd.longValue();
			}
			else
				return null;
				
		} catch (Exception e) {
			throw e;
		} finally {
			session.close();
		}
	}
	
	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.dao.TrxFinancieroDAO#getMaxSeqNumForTerm(java.lang.String)
	 */
	@Override
	@Deprecated
	public String getMaxSeqNumForTerm(String terminalSamId) throws Exception {
		LOGGER.info("[DAO INI] getMaxSeqNumForTerm");
		Session session = this.getSessionFactory().openSession();

		SQLQuery query = this.prepareGetQuery("SELECT MAX(SECUENCIA_ON_B24)","TERMINAL_SAM_ID", terminalSamId, session, null, null, null,null);

		BigDecimal result = BigDecimal.ZERO;

		try {
			Object obj = query.uniqueResult();
			if (null != obj) {
				result = (BigDecimal) obj;
			}
		} catch (Exception e) {
			throw e;
		} finally {
			session.close();
		}

		LOGGER.info("[DAO FIN] getMaxSeqNumForTerm");
		return result.toPlainString();
	}
	
	public String getMaxSeqNumForTermAct(String terminalSamId) throws Exception {
		LOGGER.info("[DAO INI] getMaxSeqNumForTermAct terminalSamId:"+terminalSamId);
		
		Session session = this.getSessionFactory().openSession();
			
		Query q= session.createQuery("select max(secuenciaOnB24) from Pago where intencionId.terminalSamId.terminalSamId =:terminal");
		q.setParameter("terminal", Long.valueOf(terminalSamId));
		Integer maxSecuenciaB24= (Integer) q.uniqueResult();
		session.close();
	
		LOGGER.info("[DAO FIN] getMaxSeqNumForTermAct");
		return maxSecuenciaB24.toString();
	}
	
	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.dao.TrxFinancieroDAO#checkIdRequerimiento(java.lang.String)
	 * use @checkIdRequerimientoAct
	 */
	@SuppressWarnings("unchecked")
	@Override
	@Deprecated 
	public List<Long> checkIdRequerimiento(String idRequerimiento)	throws Exception {
		LOGGER.info("[DAO INI] checkIdRequerimiento");
		
		Session session = this.getSessionFactory().openSession();		
		SQLQuery query = session.createSQLQuery("SELECT TRX_FINANCIERO_ID FROM " + sgeSchemaName + ".TRX_FINANCIERO "
						+ "WHERE ID_REQUERIMIENTO_ALTA = :idRequerimiento "
						+ "UNION ALL "
						+ "SELECT TRX_FINANCIERO_ID FROM " + sgeSchemaName + ".TRX_FINANCIERO "
						+ "WHERE ID_REQUERIMIENTO_BAJA = :idRequerimiento ");

		query.setParameter("idRequerimiento", idRequerimiento);

		List<Long> returnList = null;

		try {
			returnList = query.list();
		} catch (Exception e) {
			throw e;
		} finally {
			session.close();
		}
		
		LOGGER.info("[DAO FIN] checkIdRequerimiento");
		return returnList;
	}
	
	
	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.dao.TrxFinancieroDAO#checkIdRequerimiento(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean checkIdRequerimientoAct(String idRequerimiento)	throws Exception {
		LOGGER.info("[DAO INI] checkIdRequerimientoAct "+idRequerimiento);
		
		Session session = this.getSessionFactory().openSession();		
		
		Criteria criteria = session.createCriteria(Intencion.class);
		criteria.add(Restrictions.eq("requerimientoId", idRequerimiento));
		List<Intencion> lista=null;
		lista=criteria.list();
		
		try {
			LOGGER.info("[DAO FIN] checkIdRequerimientoAct "+lista);
			//if(lista!=null || !lista.isEmpty())
			if(lista!=null && !lista.isEmpty())
				return true;
			return false;
		} catch (Exception e) {
			throw e;
		} finally {
			session.close();
		}
	}
	
	/**
	 * @see ar.com.redlink.sam.financieroservices.dao.TrxFinancieroDAO#validateTrx(ar.com.redlink.sam.financieroservices.entity.TrxFinancieroValidation)
	 */
	@Override
	@Deprecated
	public TrxFinanciero validateTrx(TrxFinancieroValidation trxFinancieroValidation) throws Exception {
		LOGGER.info("[DAO INI] validateTrx");
		
		Session session = this.getSessionFactory().openSession();
		
		StringBuilder sb = new StringBuilder();
		
		sb.append("SELECT TRX_FINANCIERO_ID as trxFinancieroId, SECUENCIA_ON_B24 as secuenciaOnB24 ");
		sb.append(" from " + sgeSchemaName + ".TRX_FINANCIERO WHERE TERMINAL_SAM_ID = :terminalSamId ");
		
		if (!trxFinancieroValidation.isImporteParcial()) {
			sb.append("AND estado_trx_id = 0 ");
			sb.append("AND IMPORTE = TO_NUMBER(:importe , '9999999999999999999.99') ");
		}
		
		sb.append("AND ID_REQUERIMIENTO_ALTA = :idRequerimientoOrig ");
		sb.append("AND TERMINAL_SAM_ID = :terminalSamId ");
		sb.append("AND TOKENIZADO = :tokenizado ");
		sb.append("AND PREFIJO_OP_ID = :prefijoOpId");
		
		Query query = session.createSQLQuery(sb.toString())
				.addScalar("trxFinancieroId", new LongType())	
				.addScalar("secuenciaOnB24", new IntegerType())
				.setResultTransformer(Transformers.aliasToBean(TrxFinanciero.class));
		
		if (!trxFinancieroValidation.isImporteParcial()) {
			query.setParameter("importe", trxFinancieroValidation.getImporte());
		}
		
		query.setParameter("terminalSamId",trxFinancieroValidation.getTerminalSamId());
		query.setParameter("idRequerimientoOrig",trxFinancieroValidation.getIdRequerimientoOrig());
		query.setParameter("tokenizado",trxFinancieroValidation.getTokenizado());
		query.setParameter("prefijoOpId",trxFinancieroValidation.getPrefijoOpId());

		TrxFinanciero result = null;

		try {
			TrxFinanciero obj = (TrxFinanciero) query.uniqueResult();
			if (null != obj) {
				result = (TrxFinanciero) obj;
			}
		} catch (Exception e) {
			throw e;
		} finally {
			session.close();
		}

		LOGGER.info("[DAO FIN] validateTrx");
		return result;
	}
	
	public Pago validateCancel(TrxFinancieroValidation trxFinancieroValidation) throws Exception {
		LOGGER.info("[DAO INI] validateCancel getIdRequerimientoOrig "+trxFinancieroValidation.getIdRequerimientoOrig());
		LOGGER.info("[DAO INI] validateCancel getTokenizado "+trxFinancieroValidation.getTokenizado());
		LOGGER.info("[DAO INI] validateCancel getTerminalSamId "+trxFinancieroValidation.getTerminalSamId());
		LOGGER.info("[DAO INI] validateCancel getPrefijoOpId "+trxFinancieroValidation.getPrefijoOpId());
		LOGGER.info("[DAO INI] validateCancel getImporte "+trxFinancieroValidation.getImporte());
		LOGGER.info("[DAO INI] validateCancel isImporteParcial "+trxFinancieroValidation.isImporteParcial());
		
		Session session = this.getSessionFactory().openSession();
		
		String query ="from Pago where intencionId.requerimientoId = :rqId"
				+ " and tokenizado =:token"
				+ " and intencionId.terminalSamId.terminalSamId =:terminal"
				+ " and intencionId.prefijoOpId.prefijoOpId =:prefijo";
		
		Query q;
		if (!trxFinancieroValidation.isImporteParcial()) {
			query+=" and importe =:impt";
			q= session.createQuery(query);
			q.setParameter("rqId", trxFinancieroValidation.getIdRequerimientoOrig());
			q.setParameter("token", trxFinancieroValidation.getTokenizado());
			q.setParameter("terminal", Long.valueOf(trxFinancieroValidation.getTerminalSamId()));
			q.setParameter("prefijo", Long.valueOf(trxFinancieroValidation.getPrefijoOpId()));
			q.setParameter("impt",  new BigDecimal (trxFinancieroValidation.getImporte()) );
		}
		else{
			q= session.createQuery(query);
			q.setParameter("rqId", trxFinancieroValidation.getIdRequerimientoOrig());
			q.setParameter("token", trxFinancieroValidation.getTokenizado());
			q.setParameter("terminal", Long.valueOf(trxFinancieroValidation.getTerminalSamId()));
			q.setParameter("prefijo", Long.valueOf(trxFinancieroValidation.getPrefijoOpId()));
		}
		
		try {
			Pago result= (Pago) q.uniqueResult();
			return result;
		} catch (Exception e) {
			throw e;
		}
		finally {
			LOGGER.info("[DAO FIN] validateCancel");
			session.close();
		}
				
		/*StringBuilder sb = new StringBuilder();
		
		sb.append("SELECT TRX_FINANCIERO_ID as trxFinancieroId, SECUENCIA_ON_B24 as secuenciaOnB24 ");
		sb.append(" from " + sgeSchemaName + ".TRX_FINANCIERO WHERE TERMINAL_SAM_ID = :terminalSamId ");
		
		if (!trxFinancieroValidation.isImporteParcial()) {
			sb.append("AND estado_trx_id = 0 ");
			sb.append("AND IMPORTE = TO_NUMBER(:importe , '9999999999999999999.99') ");
		}
		
		sb.append("AND ID_REQUERIMIENTO_ALTA = :idRequerimientoOrig ");
		sb.append("AND TERMINAL_SAM_ID = :terminalSamId ");
		sb.append("AND TOKENIZADO = :tokenizado ");
		sb.append("AND PREFIJO_OP_ID = :prefijoOpId");
		
		Query query = session.createSQLQuery(sb.toString())
				.addScalar("trxFinancieroId", new LongType())	
				.addScalar("secuenciaOnB24", new IntegerType())
				.setResultTransformer(Transformers.aliasToBean(TrxFinanciero.class));
		
		if (!trxFinancieroValidation.isImporteParcial()) {
			query.setParameter("importe", trxFinancieroValidation.getImporte());
		}
		
		query.setParameter("terminalSamId",trxFinancieroValidation.getTerminalSamId());
		query.setParameter("idRequerimientoOrig",trxFinancieroValidation.getIdRequerimientoOrig());
		query.setParameter("tokenizado",trxFinancieroValidation.getTokenizado());
		query.setParameter("prefijoOpId",trxFinancieroValidation.getPrefijoOpId());

		TrxFinanciero result = null;*/

		/*try {
			TrxFinanciero obj = (TrxFinanciero) query.uniqueResult();
			if (null != obj) {
				result = (TrxFinanciero) obj;
			}
		} catch (Exception e) {
			throw e;
		} finally {
			session.close();
		}*/

		//return result;
	}
	

	/**
	 * Prepara el QUERY para el count o sum por filtro.
	 * 
	 * @param criteria
	 * @param filterField
	 * @param filterId
	 * @param session
	 * @param dateFrom
	 * @param dateTo
	 * @param tokenizado
	 * @param prefijoOpId
	 * 
	 * @return Un query armado con el filtro indicado.
	 */
	@Deprecated
	private SQLQuery prepareGetQuery(String criteria, String filterField,
			String filterId, Session session, Date dateFrom, Date dateTo,
			String tokenizado, Long prefijoOpId) {

		String getValueQuery = " from " + sgeSchemaName + ".TRX_FINANCIERO WHERE estado_trx_id = 0 AND ";
		
		String queryString = criteria + getValueQuery + filterField	+ "= :filterId";
		StringBuilder sb = new StringBuilder(queryString);

		if (null != dateFrom) {
			sb.append(" AND FECHA_HORA_ALTA >= TO_DATE(:dateFrom, 'dd/mm/yyyy HH24:MI:SS') ");
		}

		if (null != dateTo) {
			sb.append(" AND FECHA_HORA_ALTA <= TO_DATE(:dateTo, 'dd/mm/yyyy HH24:MI:SS') ");
		}

		if (null != tokenizado && !tokenizado.isEmpty()) {
			sb.append(" AND TOKENIZADO = :tokenizado ");
		}

		if (null != prefijoOpId) {
			sb.append(" AND PREFIJO_OP_ID = :prefijoOpId ");
		}

		SQLQuery query = session.createSQLQuery(sb.toString());

		query.setParameter("filterId", filterId);

		if (null != dateFrom) {
			query.setParameter("dateFrom", DateUtil.dateTimeFormat(dateFrom));
		}

		if (null != dateTo) {
			query.setParameter("dateTo", DateUtil.dateTimeFormat(dateTo));
		}

		if (null != tokenizado && !tokenizado.isEmpty()) {
			query.setParameter("tokenizado", tokenizado);
		}

		if (null != prefijoOpId) {
			query.setParameter("prefijoOpId", prefijoOpId);
		}

		return query;
	}

	/**
	 * @param sgeSchemaName the sgeSchemaName to set
	 */
	public void setSgeSchemaName(String sgeSchemaName) {
		this.sgeSchemaName = sgeSchemaName;
	}

	@Override
	@Deprecated
	public Integer updatePei(SimpleTrxFinanciero trxFinanciero) throws Exception {
		Session session = this.getSessionFactory().openSession();

		String updateTrxFinancieroAnnul = "UPDATE " + sgeSchemaName + ".TRX_FINANCIERO SET "
				+ "OPERACION_PEI = :operacionPei, "
				+ "ESTADO_TRX_ID = :estadoTrxId, "
				+ "SECUENCIA_ON_B24 = :secuenciaOnB24, "
				+ "FECHA_ON = TO_DATE(:fechaOn, 'yy/mm/dd') "
				+ "where ID_REQUERIMIENTO_ALTA = :idRequerimientoAlta";
		
		SQLQuery query = session.createSQLQuery(updateTrxFinancieroAnnul);

		query.setParameter("idRequerimientoAlta", trxFinanciero.getIdRequerimiento());
		query.setParameter("estadoTrxId", trxFinanciero.getEstadoTrxId());
		query.setParameter("secuenciaOnB24", trxFinanciero.getSecuenciaOnB24());
		
		try {
			Date fechaOn = DateUtil.parseFechaOnExt(trxFinanciero.getFechaOn());
			query.setParameter("fechaOn", DateUtil.fechaOnExtFormat(fechaOn));
		} catch (ParseException e) {
			LOGGER.error("Error de parseo de fechaOn: " + e.getMessage());
			throw e;
		}
		
		query.setParameter("operacionPei", trxFinanciero.getOperacionPei());
		
		Integer result = 0;

		try {
			result = query.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			session.close();
		}

		return result;
	}

	/*use @checkIdRequerimientoOperacionPeiNuloAct*/
	@Override
	@Deprecated
	public List<Long> checkIdRequerimientoOperacionPeiNulo(String idRequerimiento) throws Exception {
		Session session = this.getSessionFactory().openSession();		
		SQLQuery query = session
				.createSQLQuery("SELECT TRX_FINANCIERO_ID FROM " + sgeSchemaName + ".TRX_FINANCIERO "
						+ "WHERE (ID_REQUERIMIENTO_ALTA = :idRequerimiento OR ID_REQUERIMIENTO_BAJA = :idRequerimiento) "
						+ "AND ESTADO_TRX_ID = 3 ");

		query.setParameter("idRequerimiento", idRequerimiento);

		List<Long> returnList = null;

		try {
			returnList = query.list();
		} catch (Exception e) {
			throw e;
		} finally {
			session.close();
		}

		return returnList;
	}
	
	public boolean checkIdRequerimientoOperacionPeiNuloAct(String idRequerimiento) throws Exception {
		Session session = this.getSessionFactory().openSession();		
		
		Criteria criteria = session.createCriteria(Intencion.class);
		criteria.add(Restrictions.eq("requerimientoId", idRequerimiento));
		criteria.add(Restrictions.eq("estadoTrxId.estadoTrxId", (byte)3));
		List<Intencion> lista=null;
		lista=criteria.list();
		
		try {
			LOGGER.info("[DAO FIN] checkIdRequerimientoOperacionPeiNuloAct "+lista);
			
			if(lista!=null && !lista.isEmpty())
			//if(lista!=null || !lista.isEmpty())
				return true;
						
			return false;
		} catch (Exception e) {
			throw e;
		} finally {
			session.close();
		}
	}
	

	@Override
	@Deprecated
	public Integer updatePeiBaja(SimpleTrxFinanciero trxFinanciero) throws Exception {
		Session session = this.getSessionFactory().openSession();

		String updateTrxFinancieroAnnul = "UPDATE " + sgeSchemaName + ".TRX_FINANCIERO SET "
				+ "OPERACION_PEI = :operacionPei, "
				+ "ESTADO_TRX_ID = :estadoTrxId, "
				+ "SECUENCIA_ON_B24 = :secuenciaOnB24, "
				+ "FECHA_ON = TO_DATE(:fechaOn, 'yy/mm/dd') "
				+ "where ID_REQUERIMIENTO_BAJA = :idRequerimientoBaja";
		
		SQLQuery query = session.createSQLQuery(updateTrxFinancieroAnnul);

		query.setParameter("idRequerimientoBaja", trxFinanciero.getIdRequerimientoBaja());
		query.setParameter("estadoTrxId", trxFinanciero.getEstadoTrxId());
		query.setParameter("secuenciaOnB24", trxFinanciero.getSecuenciaOnB24());
		
		try {
			Date fechaOn = DateUtil.parseFechaOnExt(trxFinanciero.getFechaOn());
			query.setParameter("fechaOn", DateUtil.fechaOnExtFormat(fechaOn));
		} catch (ParseException e) {
			LOGGER.error("Error de parseo de fechaOn: " + e.getMessage());
			throw e;
		}
		
		query.setParameter("operacionPei", trxFinanciero.getOperacionPei());
		
		Integer result = 0;

		try {
			result = query.executeUpdate();
		} catch (Exception e) {
			throw e;
		} finally {
			session.close();
		}

		return result;
	}

	@Override
	public void registerWorkingKey(SimpleTrxFinanciero simpleTrxFinanciero, Intencion in) throws Exception {
		LOGGER.info("[DAO INI] insert(registerWorkingKey): " + simpleTrxFinanciero.getIdRequerimiento());
			
		Session session = this.getSessionFactory().openSession();
		
		/*Intencion newIntencion = new Intencion();
		newIntencion.setEstadoTrxId(new EstadoTrx((byte) EstadoTrxEnum.ACTIVA.getEstadoTrxId()));
		newIntencion.setFechaInicioIntencion(new Date());
		newIntencion.setFechaFinIntencion(new Date());
		newIntencion.setPrefijoOpId(new PrefijoOp(Long.valueOf(simpleTrxFinanciero.getPrefijoOpId())));
		newIntencion.setRequerimientoId(simpleTrxFinanciero.getIdRequerimiento());
		newIntencion.setTerminalSamId(new TerminalSam(Long.valueOf(simpleTrxFinanciero.getTerminalSamId())));
		newIntencion.setObservacion("OBTENER WORKINGKEY");*/
		
		in.setEstadoTrxId(new EstadoTrx((byte) EstadoTrxEnum.ACTIVA.getEstadoTrxId()));
		in.setTerminalSamId(new TerminalSam(Long.valueOf(simpleTrxFinanciero.getTerminalSamId())));
		in.setFechaFinIntencion(new Date());
		in.setObservacion("OBTENER WORKINGKEY OK");
		
		
		Pago newPago = new Pago();
		newPago.setFechaHoraAlta(new Date());
		newPago.setFechaOn( DateUtil.parseFechaOnExt(simpleTrxFinanciero.getFechaOn()));
		newPago.setImporte(new BigDecimal (simpleTrxFinanciero.getImporte()));
		
		newPago.setSecuenciaOnB24(Integer.valueOf(simpleTrxFinanciero.getSecuenciaOnB24()));
		newPago.setSecuenciaOnwS(Integer.valueOf(simpleTrxFinanciero.getSecuenciaOnWS()));
		newPago.setTokenizado(simpleTrxFinanciero.getTokenizado().trim());
		if(simpleTrxFinanciero.getOperacionPei()!=null)
		newPago.setOperacionPei(simpleTrxFinanciero.getOperacionPei());
		newPago.setIntencionId(in);
		
		/*String trxFinancieroInsertSql = "INSERT INTO " + sgeSchemaName + ".TRX_FINANCIERO "
				+ "(TRX_FINANCIERO_ID, ENTIDAD_ID, AGENTE_ID, RED_ID, SUCURSAL_ID, SERVIDOR_ID,	TERMINAL_SAM_ID, PREFIJO_OP_ID,	TOKENIZADO, "
				+ "IMPORTE, FECHA_HORA_ALTA, ESTADO_TRX_ID, SECUENCIA_ON_WS, ID_REQUERIMIENTO_ALTA, FECHA_ON, SECUENCIA_ON_B24, ID_REQUERIMIENTO_BAJA, OPERACION_PEI)"
				+ "VALUES (" + sgeSchemaName + ".SEQ_TRX_FINANCIERO.NEXTVAL, :entidadId, :agenteId, :redId, :sucursalId, :servidorId, :terminalSamId, :prefijoOpId, :tokenizado, TO_NUMBER(:importe , '9999999999999999999.99'), "
				+ "TO_DATE(:fechaHoraAlta, 'dd/mm/yyyy HH24:MI:SS'), :estadoTrxId, :secuenciaOnWS, :idRequerimientoAlta, TO_DATE(:fechaOn, 'yy/mm/dd'), :secuenciaOnB24, :idRequerimientoBaja, :operacionPei)";		
		
		SQLQuery query = session.createSQLQuery(trxFinancieroInsertSql);

		query.setParameter("entidadId", simpleTrxFinanciero.getIdEntidad());
		query.setParameter("agenteId", simpleTrxFinanciero.getAgenteId());
		query.setParameter("redId", simpleTrxFinanciero.getRedId());
		query.setParameter("sucursalId", simpleTrxFinanciero.getSucursal());
		query.setParameter("servidorId", simpleTrxFinanciero.getServidorId());
		query.setParameter("terminalSamId",	simpleTrxFinanciero.getTerminalSamId());
		query.setParameter("prefijoOpId", simpleTrxFinanciero.getPrefijoOpId());
		query.setParameter("tokenizado", simpleTrxFinanciero.getTokenizado());
		query.setParameter("importe", simpleTrxFinanciero.getImporte());
		query.setParameter("fechaHoraAlta", DateUtil.dateTimeFormat(new Date()));
		query.setParameter("estadoTrxId", simpleTrxFinanciero.getEstadoTrxId());
		query.setParameter("secuenciaOnWS",simpleTrxFinanciero.getSecuenciaOnWS());
		query.setParameter("secuenciaOnB24",simpleTrxFinanciero.getSecuenciaOnB24());

		try {
			Date fechaOn = DateUtil.parseFechaOnExt(simpleTrxFinanciero.getFechaOn());
			query.setParameter("fechaOn", DateUtil.fechaOnExtFormat(fechaOn));
		} catch (ParseException e) {
			LOGGER.error("Error de parseo de fechaOn: " + e.getMessage());
			throw e;
		}

		query.setParameter("idRequerimientoAlta",simpleTrxFinanciero.getIdRequerimiento());		
		query.setParameter("idRequerimientoBaja",simpleTrxFinanciero.getIdRequerimientoBaja());
		query.setParameter("operacionPei",simpleTrxFinanciero.getOperacionPei(), StandardBasicTypes.LONG);*/		

		//Integer result = 0;

		try {
			//result = query.executeUpdate();
			session.save(newPago);
			session.flush();
			LOGGER.info("[DAO FIN]  insert(registerWorkingKey): " + simpleTrxFinanciero.getIdRequerimiento());
		} catch (Exception e) {
			throw e;
		} finally {
			 session.close();
		}

		//LOGGER.info("[DAO FIN]  insert(registerWorkingKey): " + simpleTrxFinanciero.getIdRequerimiento());
		//return result;
	}
	
	public void validarTarjeta(SimpleTrxFinanciero simpleTrxFinanciero, Intencion in) throws Exception {
		LOGGER.info("[DAO INI] insert(validarTarjeta): " + simpleTrxFinanciero.getIdRequerimiento());
		
		/*VER EL ORIGINAL EN registerWorkingKey*/
		Session session = this.getSessionFactory().openSession();
		
		//in.setEstadoTrxId(new EstadoTrx((byte) EstadoTrxEnum.ACTIVA.getEstadoTrxId()));
		in.setTerminalSamId(new TerminalSam(Long.valueOf(simpleTrxFinanciero.getTerminalSamId())));
		in.setFechaFinIntencion(new Date());
		in.setObservacion("VALIDAR TARJETA OK");
				
		Pago newPago =datosBasicosPago(simpleTrxFinanciero,in);
		newPago.setImporte(new BigDecimal (simpleTrxFinanciero.getImporte()));
		
		try {
			session.save(newPago);
			session.flush();
			LOGGER.info("[DAO FIN]  insert(validarTarjeta): " + simpleTrxFinanciero.getIdRequerimiento());
		} catch (Exception e) {
			throw e;
		} finally {
			 session.close();
		}

		//LOGGER.info("[DAO FIN]  insert(validarTarjeta): " + simpleTrxFinanciero.getIdRequerimiento());
	}
	
	public void getBalance(SimpleTrxFinanciero simpleTrxFinanciero, Intencion in) throws Exception {
		LOGGER.info("[DAO INI] insert(getBalance): " + simpleTrxFinanciero.getIdRequerimiento());
		
		/*VER EL ORIGINAL EN registerWorkingKey*/
		Session session = this.getSessionFactory().openSession();
		
		//in.setEstadoTrxId(new EstadoTrx((byte) EstadoTrxEnum.ACTIVA.getEstadoTrxId()));
		in.setTerminalSamId(new TerminalSam(Long.valueOf(simpleTrxFinanciero.getTerminalSamId())));
		in.setFechaFinIntencion(new Date());
		in.setObservacion("OBTENER SALDO OK");
				
		Pago newPago =datosBasicosPago(simpleTrxFinanciero,in);
		newPago.setImporte(new BigDecimal (0));
		
		try {
			session.save(newPago);
			session.flush();
			LOGGER.info("[DAO FIN]  insert(getBalance): " + simpleTrxFinanciero.getIdRequerimiento());
		} catch (Exception e) {
			throw e;
		} finally {
			 session.close();
		}

		//LOGGER.info("[DAO FIN]  insert(getBalance): " + simpleTrxFinanciero.getIdRequerimiento());
	}
	
	public void doDeposit(SimpleTrxFinanciero simpleTrxFinanciero, Intencion in) throws Exception {
		LOGGER.info("[DAO INI] insert(doDeposit): " + simpleTrxFinanciero.getIdRequerimiento());
		
		/*VER EL ORIGINAL EN registerWorkingKey*/
		Session session = this.getSessionFactory().openSession();
		
		in.setTerminalSamId(new TerminalSam(Long.valueOf(simpleTrxFinanciero.getTerminalSamId())));
		in.setFechaFinIntencion(new Date());
		in.setObservacion("DEPOSITO OK");
		
		/*Intencion newIntencion = datosBasicosIntencion(simpleTrxFinanciero);
		newIntencion.setEstadoTrxId(new EstadoTrx((byte) EstadoTrxEnum.ACTIVA.getEstadoTrxId()));
		newIntencion.setObservacion("DEPOSITO OK");*/
		
		//Pago newPago =datosBasicosPago(simpleTrxFinanciero,newIntencion);
		Pago newPago =datosBasicosPago(simpleTrxFinanciero,in);
		newPago.setImporte(new BigDecimal (simpleTrxFinanciero.getImporte()));
		
		try {
			session.save(newPago);
			session.flush();
			LOGGER.info("[DAO FIN]  insert(doDeposit): " + simpleTrxFinanciero.getIdRequerimiento());
		} catch (Exception e) {
			throw e;
		} finally {
			 session.close();
		}

		//LOGGER.info("[DAO FIN]  insert(doDeposit): " + simpleTrxFinanciero.getIdRequerimiento());
	}
	
	public void doDebitPayment(SimpleTrxFinanciero simpleTrxFinanciero, Intencion in) throws Exception {
		LOGGER.info("[DAO INI] insert(doDebitPayment): " + simpleTrxFinanciero.getIdRequerimiento());
		
		/*VER EL ORIGINAL EN registerWorkingKey*/
		Session session = this.getSessionFactory().openSession();
		
		//in.setEstadoTrxId(new EstadoTrx((byte) EstadoTrxEnum.ACTIVA.getEstadoTrxId()));
		in.setTerminalSamId(new TerminalSam(Long.valueOf(simpleTrxFinanciero.getTerminalSamId())));
		in.setFechaFinIntencion(new Date());
		in.setObservacion("PAGO CON DEBITO OK");
				
		Pago newPago =datosBasicosPago(simpleTrxFinanciero,in);
		newPago.setImporte(new BigDecimal (simpleTrxFinanciero.getImporte()));
		
		try {
			session.save(newPago);
			session.flush();
			LOGGER.info("[DAO FIN]  insert(doDebitPayment): " + simpleTrxFinanciero.getIdRequerimiento());
		} catch (Exception e) {
			throw e;
		} finally {
			 session.close();
		}

		//LOGGER.info("[DAO FIN]  insert(doDebitPayment): " + simpleTrxFinanciero.getIdRequerimiento());
	}
	
	
	public void registerConsultaPEITrx(SimpleTrxFinanciero simpleTrxFinanciero, Intencion in) throws Exception {
		LOGGER.info("[DAO INI] insert(registerConsultaPEITrx): " + simpleTrxFinanciero.getIdRequerimiento());
		
		/*VER EL ORIGINAL EN registerWorkingKey*/
		Session session = this.getSessionFactory().openSession();
		
		
		in.setEstadoTrxId(new EstadoTrx((byte) EstadoTrxEnum.ACTIVA.getEstadoTrxId()));
		in.setTerminalSamId(new TerminalSam(Long.valueOf(simpleTrxFinanciero.getTerminalSamId())));
		in.setFechaFinIntencion(new Date());
		in.setObservacion("CONSULTA OPERACIONES PEI OK");
		
		Pago newPago =datosBasicosPago(simpleTrxFinanciero,in);
		if(simpleTrxFinanciero.getImporte()!=null)
			newPago.setImporte(new BigDecimal (simpleTrxFinanciero.getImporte()));
		
		newPago.setSecuenciaOnwS(Integer.valueOf(simpleTrxFinanciero.getSecuenciaOnWS()));
		newPago.setIdRequerimientoBaja(simpleTrxFinanciero.getIdRequerimientoBaja());
		
		try {
			session.save(newPago);
			session.flush();
			LOGGER.info("[DAO FIN]  insert(registerConsultaPEITrx): " + simpleTrxFinanciero.getIdRequerimiento());
		} catch (Exception e) {
			throw e;
		} finally {
			 session.close();
		}

		//LOGGER.info("[DAO FIN]  insert(registerConsultaPEITrx): " + simpleTrxFinanciero.getIdRequerimiento());
	}
	
	
	public void doDebit(SimpleTrxFinanciero simpleTrxFinanciero, Intencion in) throws Exception {
		LOGGER.info("[DAO INI] insert(doDebit): " + simpleTrxFinanciero.getIdRequerimiento());
		
		/*VER EL ORIGINAL EN registerWorkingKey*/
		Session session = this.getSessionFactory().openSession();
		
		in.setTerminalSamId(new TerminalSam(Long.valueOf(simpleTrxFinanciero.getTerminalSamId())));
		in.setFechaFinIntencion(new Date());
		in.setObservacion("DEBITO OK");
				
		Pago newPago =datosBasicosPago(simpleTrxFinanciero,in);
		newPago.setImporte(new BigDecimal (simpleTrxFinanciero.getImporte()));
		
		try {
			session.save(newPago);
			session.flush();
			LOGGER.info("[DAO FIN]  insert(doDebit): " + simpleTrxFinanciero.getIdRequerimiento());
		} catch (Exception e) {
			throw e;
		} finally {
			 session.close();
		}

		//LOGGER.info("[DAO FIN]  insert(doDebit): " + simpleTrxFinanciero.getIdRequerimiento());
	}
	
	public void doTransfer(SimpleTrxFinanciero simpleTrxFinanciero, Intencion in) throws Exception {
		LOGGER.info("[DAO INI] doTransfer(doDebit): " + simpleTrxFinanciero.getIdRequerimiento());
		
		/*VER EL ORIGINAL EN registerWorkingKey*/
		Session session = this.getSessionFactory().openSession();
		
		in.setTerminalSamId(new TerminalSam(Long.valueOf(simpleTrxFinanciero.getTerminalSamId())));
		in.setFechaFinIntencion(new Date());
		in.setObservacion("TRANSFERENCIA OK");
				
		Pago newPago =datosBasicosPago(simpleTrxFinanciero,in);
		newPago.setImporte(new BigDecimal (simpleTrxFinanciero.getImporte()));
		
		try {
			session.save(newPago);
			session.flush();
			LOGGER.info("[DAO FIN]  doTransfer(doDebit): " + simpleTrxFinanciero.getIdRequerimiento());
		} catch (Exception e) {
			throw e;
		} finally {
			 session.close();
		}

		//LOGGER.info("[DAO FIN]  insert(doDebit): " + simpleTrxFinanciero.getIdRequerimiento());
	}
	
	
	public Pago doPagoPei(SimpleTrxFinanciero simpleTrxFinanciero, Intencion in) throws Exception {
		LOGGER.info("[DAO INI] insert(doPagoPei): " + simpleTrxFinanciero.getIdRequerimiento());
		
		/*VER EL ORIGINAL EN registerWorkingKey*/
		Session session = this.getSessionFactory().openSession();
		
		in.setTerminalSamId(new TerminalSam(Long.valueOf(simpleTrxFinanciero.getTerminalSamId())));
		in.setFechaFinIntencion(new Date());
		in.setObservacion("PAGO PEI STEP1");
				
		Pago newPago =datosBasicosPago(simpleTrxFinanciero,in);
		newPago.setImporte(new BigDecimal (simpleTrxFinanciero.getImporte()));
		
		try {
			session.save(newPago);
			session.flush();
			LOGGER.info("[DAO FIN]  insert(doPagoPei): " + simpleTrxFinanciero.getIdRequerimiento());
			return newPago;
		} catch (Exception e) {
			throw e;
		} finally {
			 session.close();
		}

		//LOGGER.info("[DAO FIN]  insert(doDebit): " + simpleTrxFinanciero.getIdRequerimiento());
		//return null;
	}
	
	public Pago doDevolucionPagoPei(SimpleTrxFinanciero simpleTrxFinanciero, Intencion in, boolean isParcial) throws Exception {
		LOGGER.info("[DAO INI] insert(doDevolucionPagoPei): " + simpleTrxFinanciero.getIdRequerimiento());
		
		Session session = this.getSessionFactory().openSession();
		
		in.setTerminalSamId(new TerminalSam(Long.valueOf(simpleTrxFinanciero.getTerminalSamId())));
		in.setFechaFinIntencion(new Date());
		
		if(isParcial)
			in.setObservacion("DEVOLUCION PARCIAL PAGO PEI STEP1");
		else
			in.setObservacion("DEVOLUCION PAGO PEI STEP1");
		
		//POR ANALIZAR
		//in.setEstadoTrxId(new EstadoTrx((byte)(Long.valueOf(simpleTrxFinanciero.getEstadoTrxId()).longValue())));
		
		Pago newPago =datosBasicosPago(simpleTrxFinanciero,in);
		newPago.setImporte(new BigDecimal (simpleTrxFinanciero.getImporte()));
		
		try {
			session.save(newPago);
			session.flush();
			LOGGER.info("[DAO FIN]  insert(doDevolucionPagoPei): " + simpleTrxFinanciero.getIdRequerimiento());
			return newPago;
		} catch (Exception e) {
			throw e;
		} finally {
			 session.close();
		}
		//LOGGER.info("[DAO FIN]  insert(doDebit): " + simpleTrxFinanciero.getIdRequerimiento());
	}
	
	
	public void doWithdraw(SimpleTrxFinanciero simpleTrxFinanciero, Intencion in) throws Exception {
		LOGGER.info("[DAO INI] insert(doWithdraw): " + simpleTrxFinanciero.getIdRequerimiento());
		
		/*VER EL ORIGINAL EN registerWorkingKey*/
		Session session = this.getSessionFactory().openSession();
		
		//in.setEstadoTrxId(new EstadoTrx((byte) EstadoTrxEnum.ACTIVA.getEstadoTrxId()));
		in.setTerminalSamId(new TerminalSam(Long.valueOf(simpleTrxFinanciero.getTerminalSamId())));
		in.setFechaFinIntencion(new Date());
		in.setObservacion("EXTRACCION OK");
		
		Pago newPago =datosBasicosPago(simpleTrxFinanciero,in);
		newPago.setImporte(new BigDecimal (simpleTrxFinanciero.getImporte()));
		
		try {
			session.save(newPago);
			session.flush();
			LOGGER.info("[DAO FIN]  insert(doWithdraw): " + simpleTrxFinanciero.getIdRequerimiento());
		} catch (Exception e) {
			throw e;
		} finally {
			 session.close();
		}
		//LOGGER.info("[DAO FIN]  insert(doWithdraw): " + simpleTrxFinanciero.getIdRequerimiento());
	}
	
	public void updatePagoPei(Pago pago) throws Exception {
		LOGGER.info("[DAO INI] update(updatePagoPei): " + pago.getPagoId());
		
		Session session = this.getSessionFactory().openSession();
		
		pago.getIntencionId().setFechaFinIntencion(new Date());
		pago.getIntencionId().setObservacion("PAGO PEI OK");
		
		try {
			session.update(pago);
			session.flush();
			LOGGER.info("[DAO FIN]  update(updatePagoPei): " +  pago.getPagoId());
		} catch (Exception e) {
			throw e;
		} finally {
			 session.close();
		}
		//LOGGER.info("[DAO FIN]   update(updatePagoPei): " +  pago.getPagoId());
	}
	
	
	public void updateDevolucionPagoPei(Pago pago, String idRequerimientoOrig, boolean isParcial) throws Exception {
		LOGGER.info("[DAO INI] update(updateDevolucionPagoPei): " + pago.getPagoId());
		
		Session session = this.getSessionFactory().openSession();
		/*se actualiza el pago asociado a la intencion de devolucion*/
		pago.getIntencionId().setFechaFinIntencion(new Date());
		
		if(isParcial)
			pago.getIntencionId().setObservacion("DEVOLUCION PARCIAL PAGO PEI OK");
		else
			pago.getIntencionId().setObservacion("DEVOLUCION PAGO PEI OK");
		
		pago.getIntencionId().setEstadoTrxId(new EstadoTrx((byte) EstadoTrxEnum.ACTIVA.getEstadoTrxId()));
		pago.setIdRequerimientoBaja(idRequerimientoOrig);
		pago.setFechaHoraBaja(new Date());
		
		try {
			session.update(pago);
			session.flush();
			LOGGER.info("[DAO FIN]  update(updateDevolucionPagoPei): " +  pago.getPagoId());
		} catch (Exception e) {
			throw e;
		} finally {
			 session.close();
		}
		//LOGGER.info("[DAO FIN]   update(updateDevolucionPagoPei): " +  pago.getPagoId());
	}
	
	
	Intencion datosBasicosIntencion(SimpleTrxFinanciero simpleTrxFinanciero){
		Intencion newIntencion = new Intencion();
		newIntencion.setFechaInicioIntencion(new Date());
		newIntencion.setFechaFinIntencion(new Date());
		newIntencion.setPrefijoOpId(new PrefijoOp(Long.valueOf(simpleTrxFinanciero.getPrefijoOpId())));
		newIntencion.setRequerimientoId(simpleTrxFinanciero.getIdRequerimiento());
		if(simpleTrxFinanciero.getTerminalSamId()!=null)
			newIntencion.setTerminalSamId(new TerminalSam(Long.valueOf(simpleTrxFinanciero.getTerminalSamId())));
		return newIntencion;
	}
	
	Pago datosBasicosPago(SimpleTrxFinanciero simpleTrxFinanciero, Intencion intencion){
		Pago newPago = new Pago();
		newPago.setFechaHoraAlta(new Date());
		try {
			newPago.setFechaOn( DateUtil.parseFechaOnExt(simpleTrxFinanciero.getFechaOn()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		newPago.setSecuenciaOnB24(Integer.valueOf(simpleTrxFinanciero.getSecuenciaOnB24()));
		newPago.setSecuenciaOnwS(Integer.valueOf(simpleTrxFinanciero.getSecuenciaOnWS()));
		newPago.setTokenizado(simpleTrxFinanciero.getTokenizado());
		
		if (simpleTrxFinanciero.getHoraOn()!=null)
			newPago.setHoraOn(simpleTrxFinanciero.getHoraOn());
		
		if(simpleTrxFinanciero.getOperacionPei()!=null)
			newPago.setOperacionPei(simpleTrxFinanciero.getOperacionPei());
		
		newPago.setIntencionId(intencion);
			
		return newPago;
	}

	@Override
	public Intencion registrarInicioTransaccion(SimpleTrxFinanciero trxFinanciero) throws Exception {
		Session session = this.getSessionFactory().openSession();
		Intencion newIntencion = datosBasicosIntencion(trxFinanciero);
		newIntencion.setEstadoTrxId(new EstadoTrx((byte) EstadoTrxEnum.ACTIVA.getEstadoTrxId()));
		newIntencion.setObservacion("INICIO TRANSACCION");
		session.save(newIntencion);
		session.flush();
		session.close();
		return newIntencion;
	}

	@Override
	public void updateIntencion(Intencion in) {
		Session session = this.getSessionFactory().openSession();
		session.update(in);
		session.flush();
		session.close();
	}
	
}
