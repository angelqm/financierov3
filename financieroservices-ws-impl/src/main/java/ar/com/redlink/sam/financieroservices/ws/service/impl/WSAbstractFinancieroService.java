/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  31/03/2015 - 12:24:46
 */

package ar.com.redlink.sam.financieroservices.ws.service.impl;

import java.util.Date;

import ar.com.redlink.sam.financieroservices.dto.request.common.FinancieroServiceBaseRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.util.DateUtil;
import ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroTandemException;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSAbstractRequest;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSHeaderRequest;
import ar.com.redlink.sam.financieroservices.ws.response.base.WSAbstractResponse;
import ar.com.redlink.sam.financieroservices.ws.response.base.WSHeaderResponse;
import ar.com.redlink.sam.financieroservices.ws.service.WSFinancieroService;

/**
 * Clase abstracta del Servicio de Financiero que disponibiliza metodos de
 * utilidad.
 * 
 * @author aguerrea
 * 
 */
public abstract class WSAbstractFinancieroService implements
		WSFinancieroService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2773567413045947098L;

	/**
	 * Puebla los resultados de la validacion en el objeto DTO a transferirse a
	 * la capa de servicios.
	 * 
	 * @param baseRequestDTO
	 * @param validation
	 */
	protected void llenarResultadosValidation(	FinancieroServiceBaseRequestDTO baseRequestDTO,	ValidationDTO validation) {
		baseRequestDTO.setAgenteId(Long.toString((Long) validation.getOutputValue(ValidationDTO.AGENTE_ID)));
		baseRequestDTO.setSucursal(Long.toString((Long) validation.getOutputValue(ValidationDTO.SUCURSAL_ID)));
		baseRequestDTO.setServidorId(Long.toString((Long) validation.getOutputValue(ValidationDTO.SERVIDOR_ID)));
		baseRequestDTO.setIdEntidad(Integer.toString((Integer) validation.getOutputValue(ValidationDTO.ENTIDAD_ID)));
		baseRequestDTO.setIdentDispositivo(validation.getInputValue(ValidationDTO.IDENT_DISPOSITIVO));
		baseRequestDTO.setTerminalSamId(Long.toString((Long) validation.getOutputValue(ValidationDTO.TERMINAL_SAM_ID)));
		baseRequestDTO.setTerminalB24(((String) validation.getOutputValue(ValidationDTO.TERMINAL_B24)).trim());

		if(validation.getOutputValue(ValidationDTO.TIPO_TERMINAL_B24) != null) 
			baseRequestDTO.setTipoTerminalB24(((String) validation.getOutputValue(ValidationDTO.TIPO_TERMINAL_B24)).trim());

		baseRequestDTO.setIdentTerminal(((String) validation.getInputValue(ValidationDTO.IDENT_TERMINAL)));
		baseRequestDTO.setRedId(Integer.toString((Integer) validation.getOutputValue(ValidationDTO.RED_ID)));
		baseRequestDTO.setPrefijoOpId(validation.getInputValue(ValidationDTO.PREFIJO_OP_ID));
		baseRequestDTO.setIdRequerimiento(validation.getInputValue(ValidationDTO.ID_REQ));		
		
		if(validation.getOutputValue(ValidationDTO.PRODUCTO_ID) != null) 
			baseRequestDTO.setProductoId(Long.toString((Long) validation.getOutputValue(ValidationDTO.PRODUCTO_ID)));
	
	}

	/**
	 * Llena datos comunes del response.
	 * 
	 * @param wsAbstractResponse
	 * @param isOK
	 */
	protected void llenarDatosComunes(WSAbstractResponse wsAbstractResponse,boolean isOK) {

		if (wsAbstractResponse != null && wsAbstractResponse.getHeader() == null) {
			WSHeaderResponse wsHeaderResponse = new WSHeaderResponse();
			wsHeaderResponse.setTimeStamp(DateUtil.timestampFormat(new Date()));

			if (isOK) {
				wsHeaderResponse.setCodigoRespuesta(FinancieroServicesMsgEnum.FINA000.getCode());
				wsHeaderResponse.setDescripcionRespuesta(FinancieroServicesMsgEnum.FINA000.getMsg());
				wsHeaderResponse.setCodigoRespuestaB24("00");
			}

			wsAbstractResponse.setHeader(wsHeaderResponse);
		}
	}

	/**
	 * Genera el objeto correspondiente al Header con el mensaje de error y su
	 * codigo correspondiente.
	 * 
	 * @param e
	 */
	protected void llenarHeaderError(FinancieroException e,	WSAbstractResponse wsAbstractResponse) {
		if (null != wsAbstractResponse) {
			WSHeaderResponse header = new WSHeaderResponse();
			header.setTimeStamp(DateUtil.timestampFormat(new Date()));
			header.setCodigoRespuesta(e.getCod());
			header.setDescripcionRespuesta(e.getMsj());

			if (e instanceof FinancieroTandemException) {
				FinancieroTandemException tException = (FinancieroTandemException) e;
				header.setCodigoRespuestaB24(tException.getCodTandem());
				header.setDescripcionRespuestaB24(tException.getMsjTandem());
			}
			wsAbstractResponse.setHeader(header);
		}
	}

	/**
	 * Genera un ValidationDTO con los datos comunes obtenidos del header del
	 * request.
	 * 
	 * @param request
	 * @return Un validationDTO.
	 */
	protected ValidationDTO createValidationDTO(WSAbstractRequest request) {
		ValidationDTO validation = new ValidationDTO();
		WSHeaderRequest header = request.getHeader();

		validation.addInputValue(ValidationDTO.ENTIDAD_COD, header.getEntidad());
		validation.addInputValue(ValidationDTO.SUCURSAL_COD,header.getSucursal());
		validation.addInputValue(ValidationDTO.TERMINAL_COD,header.getTerminal());
		validation.addInputValue(ValidationDTO.IDENT_TERMINAL,header.getIdentTerminal());
		validation.addInputValue(ValidationDTO.IDENT_DISPOSITIVO,header.getIdentDispositivo());
		validation.addInputValue(ValidationDTO.ID_REQ,header.getIdRequerimiento());
		validation.addInputValue(ValidationDTO.TIMESTAMP, header.getTimeStamp());
		validation.addInputValue(ValidationDTO.IP_CLIENTE,header.getIpCliente());

		return validation;
	}

}
