/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.ws.service.impl;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA109;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA151;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA152;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA153;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA154;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA155;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA156;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA157;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA158;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA159;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA160;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA161;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA162;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA163;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA165;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA177;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.Validate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import ar.com.redlink.sam.financieroservices.dto.request.AnularDebitoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularDepositoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionCuotasRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionPrestamoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.BalanceRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ConsultaOperacionPEIPagarRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ConsultaOperacionPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ConsultaPEIPagarRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ConsultaPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.CuentaDestinoDTO;
import ar.com.redlink.sam.financieroservices.dto.request.CuentaRecaudadoraDTO;
import ar.com.redlink.sam.financieroservices.dto.request.DestinoPEIPagarPagoRequestDTO;
//import ar.com.redlink.sam.financieroservices.dto.request.CuentaRecaudadoraDTO;
import ar.com.redlink.sam.financieroservices.dto.request.EnrolamientoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.EntidadPEIRequestDTO;
//import ar.com.redlink.sam.financieroservices.dto.request.EntidadPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ExtraccionRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.OperacionOrigenPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.OrigenPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDebitoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDepositoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoDevolucionPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoDevolucionPEIPagarRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoPEIPagarRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoParcialDevolucionPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoParcialPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarExtraccionCuotasRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarExtraccionPrestamoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarPagoDebitoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaPEIPagarPagoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaPEIPagarRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaPEIPagoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaPEITrackDataRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ResultadoOperacionRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.TerminalRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.TipoDocumentoDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ValidarTarjetaRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.WorkingKeyRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.response.AnularDebitoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.AnularDepositoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.AnularExtraccionCuotasDTO;
import ar.com.redlink.sam.financieroservices.dto.response.AnularExtraccionDTO;
import ar.com.redlink.sam.financieroservices.dto.response.AnularExtraccionPrestamoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ConsultaOperacionPEIDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ConsultaOperacionPEIPagarDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ConsultaTipoDocumentoPEIDTO;
import ar.com.redlink.sam.financieroservices.dto.response.EstadoCuentaDTO;
import ar.com.redlink.sam.financieroservices.dto.response.EstadoOperacionDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ExtraccionCuotasDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ExtraccionPrestamoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ExtraccionResultadoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarDebitoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarDepositoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarDevolucionPEIDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarDevolucionPEIPagarDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarDevolucionResultadoPEIDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarDevolucionResultadoPEIPagarDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarPagoDebitoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarTransferenciaDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarTransferenciaPEIDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarTransferenciaPEIPagarDTO;
import ar.com.redlink.sam.financieroservices.dto.response.UltimasOperacionesDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ValidacionTarjetaDTO;
import ar.com.redlink.sam.financieroservices.dto.response.WorkingKeyDTO;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.Intencion;
import ar.com.redlink.sam.financieroservices.entity.util.DateUtil;
import ar.com.redlink.sam.financieroservices.service.FinancieroService;
import ar.com.redlink.sam.financieroservices.service.ModelMapperService;
import ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum;
import ar.com.redlink.sam.financieroservices.service.enums.OperationEnum;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroTandemException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;
import ar.com.redlink.sam.financieroservices.service.validation.ValidationChain;
import ar.com.redlink.sam.financieroservices.ws.request.WSAnularDebitoRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSAnularDepositoRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSAnularExtraccionCuotasRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSAnularExtraccionPrestamoRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSAnularExtraccionRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSAnularPagoDebitoRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSConsultarOperacionesPEIPagarRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSConsultarOperacionesPEIRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSEnrolamientoRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSObtenerOperacionesRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSObtenerResultOperacionRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSObtenerSaldoCuentaRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSObtenerWorkingKeyRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarDebitoRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarDepositoRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarDevolucionPagoPEIRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarDevolucionPagoPEIPagarRequest;

import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarDevolucionPagoParcialPEIRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarExtraccionCuotasRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarExtraccionPrestamoRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarExtraccionRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarPagoDebitoRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarTransferenciaPEIPagarRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarTransferenciaPEIRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSRealizarTransferenciaRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSTestServiceRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSTipoDocumentoRequest;
import ar.com.redlink.sam.financieroservices.ws.request.WSValidarTarjetaRequest;
import ar.com.redlink.sam.financieroservices.ws.request.base.WSHeaderRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosRealizarDevolucionPEIRequest;
import ar.com.redlink.sam.financieroservices.ws.request.data.WSParametrosRealizarDevolucionPEIPagarRequest;
import ar.com.redlink.sam.financieroservices.ws.response.WSAnularDebitoResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSAnularDepositoResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSAnularExtraccionCuotasResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSAnularExtraccionPrestamoResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSAnularExtraccionResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSAnularPagoDebitoResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSConsultarOperacionesPEIPagarResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSConsultarOperacionesPEIResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSEnrolamientoResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSObtenerOperacionesResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSObtenerResultOperacionResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSObtenerSaldoCuentaResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSObtenerWorkingKeyResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarDebitoResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarDepositoResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarDevolucionPEIResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarDevolucionPEIPagarResponse;

import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarExtraccionCuotasResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarExtraccionPrestamoResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarExtraccionResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarPagoDebitoResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarTransferenciaPEIPagarResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarTransferenciaPEIResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSRealizarTransferenciaResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSTestServiceResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSTiposDocumentoResponse;
import ar.com.redlink.sam.financieroservices.ws.response.WSValidarTarjetaResponse;
import ar.com.redlink.sam.financieroservices.ws.response.base.WSHeaderResponse;
import ar.com.redlink.sam.financieroservices.ws.service.WSFinancieroService;

/**
 * Implementacion del WS Financiero.
 * 
 * @author aguerrea
 * 
 */
public class WSFinancieroServiceImpl extends WSAbstractFinancieroService implements WSFinancieroService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5671303057779335271L;
	private static final Logger LOGGER = Logger.getLogger(WSFinancieroServiceImpl.class);
	@Autowired
	private FinancieroService financieroService;
	@Autowired
	private ModelMapperService modelMapperService;
	@Autowired
	private ValidationChain financieroValidationChain;

	private String idCanal;

	/**
	 * @see ar.com.redlink.sam.financieroservices.ws.service.WSFinancieroService#enrolamiento(ar.com.redlink.sam.financieroservices.ws.request.WSEnrolamientoRequest)
	 */
	@Override
	public WSEnrolamientoResponse enrolamiento(WSEnrolamientoRequest request) throws FinancieroValidationException {

		LOGGER.info("*[WS INI]*  enrolamiento: " + this.requestToString(request.getHeader()));

		boolean success = true;

		ValidationDTO validation = createValidationDTO(request);

		WSEnrolamientoResponse response = new WSEnrolamientoResponse();

		try {
			financieroValidationChain.validate(validation);
		} catch (FinancieroException e) {
			// Se espera un error de enrolamiento en el flujo del caso.
			if (!e.getCod().equals(FINA109.getCode())) {
				// Si no coinciden los IDs, significa que ya esta enrolado.
				//if (e.getCod().equals(FINA110.getCode())) {
				//	// Esto no se hace, pero necesito redefinir la excepcion.
				//	e = new FinancieroException(FINA137.getCode(), FINA137.getMsg());
				//}
				LOGGER.error("Error de validacion: " + e.getMsj());
				success = false;
				this.llenarHeaderError(e, response);
			}
		}

		if (success) {
			EnrolamientoRequestDTO enrolamiento = modelMapperService.modelMapperHelper(request.getEnrolamientoDataRequest(), EnrolamientoRequestDTO.class);

			this.llenarResultadosValidation(enrolamiento, validation);

			try {
				this.getFinancieroService().enroll(enrolamiento);
			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
			}
		}

		this.llenarDatosComunes(response, success);

		LOGGER.info("*[WS FIN]* enrolamiento: " + request.getHeader().getIdRequerimiento());
		
		return response;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.ws.service.WSFinancieroService#obtenerWorkingKey(ar.com.redlink.sam.financieroservices.ws.request.WSObtenerWorkingKeyRequest)
	 */
	@Override 
	public WSObtenerWorkingKeyResponse obtenerWorkingKey(WSObtenerWorkingKeyRequest request) {

		LOGGER.info("*[WS INI]* obtenerWorkingKey: " + this.requestToString(request.getHeader()));
		boolean success = true;
		//LOGGER.debug("Parametros recibidos: " + this.requestToString(request.getHeader()));
		ValidationDTO validation = createValidationDTO(request);
		WSObtenerWorkingKeyResponse response = new WSObtenerWorkingKeyResponse();
		
		//WorkingKeyRequestDTO workingKeyDTO = modelMapperService	.modelMapperHelper(request.getObtenerWorkingKeyDataRequest(), WorkingKeyRequestDTO.class);
		//workingKeyDTO.setPrefijoOpId(   String.valueOf(OperationEnum.WORKINGKEY.getCode())   );
		//workingKeyDTO.setIdRequerimiento(request.getHeader().getIdRequerimiento());
		
		try {
			financieroValidationChain.validate(validation);
		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
			//intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj());
			//this.getFinancieroService().registrarErrorTransaccion(intenc);
		}
		
		if (success) {
			WorkingKeyRequestDTO workingKeyDTO = modelMapperService.modelMapperHelper(request.getObtenerWorkingKeyDataRequest(), WorkingKeyRequestDTO.class);
			this.llenarResultadosValidation(workingKeyDTO, validation);
			workingKeyDTO.setTipoTerminal(Short.toString((Short) validation.getOutputValue(ValidationDTO.TERM_TYPE)));
			
			workingKeyDTO.setPrefijoOpId(   String.valueOf(OperationEnum.WORKINGKEY.getCode())   );
			Intencion intenc= this.getFinancieroService().registrarInicioTransaccion(workingKeyDTO);
			
			WorkingKeyDTO workingKeyResponseDTO;
			try {
				workingKeyResponseDTO = this.getFinancieroService().getWorkingKey(workingKeyDTO,intenc);

				response.setWorkingKey(workingKeyResponseDTO.getWorkingKey());
				response.setTerminalB24((String) validation.getOutputValue(ValidationDTO.TERMINAL_B24));
				response.setFechaOn(workingKeyResponseDTO.getFechaOn());
				response.setSecuenciaOn(workingKeyResponseDTO.getSecuenciaOn());

				response.setTipoEncripcion((((Short) validation.getOutputValue(ValidationDTO.TIPO_ENCRIPCION_B24)) == 2)
						? Integer.toString(1) : Integer.toString(0));

				/*
				 * response.setTipoTerminalB24(Short.toString((Short) validation
				 * .getOutputValue(ValidationDTO.TERM_TYPE)));
				 */
				response.setTipoTerminalB24("60");
			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
				
				intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj()+  ( e.getMsjInterno()!=null?"->"+e.getMsjInterno():""));
				
				if (e instanceof FinancieroTandemException) {
					FinancieroTandemException tException = (FinancieroTandemException) e;
					
					if( tException.getMsjTandem()!=null)
						intenc.setObservacion(intenc.getObservacion()+"*"+ tException.getMsjTandem().trim());
				}
				this.getFinancieroService().registrarErrorTransaccion(intenc);
			}

			this.llenarDatosComunes(response, success);
		}
		
		LOGGER.info("*[WS FIN]* obtenerWorkingKey: " + request.getHeader().getIdRequerimiento());
		return response;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.ws.service.WSFinancieroService#obtenerResultadoOp(ar.com.redlink.sam.financieroservices.ws.request.WSObtenerResultOperacionRequest)
	 */
	@Override
	public WSObtenerResultOperacionResponse obtenerResultadoOp(WSObtenerResultOperacionRequest request) {
		boolean success = true;

		LOGGER.info("[WS INI] obtenerResultadoOp: " + this.requestToString(request.getHeader()));
		ValidationDTO validation = createValidationDTO(request);
		WSObtenerResultOperacionResponse response = new WSObtenerResultOperacionResponse();

		try {
			financieroValidationChain.validate(validation);
		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}

		if (success) {
			ResultadoOperacionRequestDTO resultadoOperacionRequestDTO = modelMapperService
					.modelMapperHelper(request.getObtenerResultOperacionRequest(), ResultadoOperacionRequestDTO.class);

			this.llenarResultadosValidation(resultadoOperacionRequestDTO, validation);

			try {
				EstadoOperacionDTO estadoOperacionDTO = this.getFinancieroService().getOperationResult(resultadoOperacionRequestDTO);
				response = modelMapperService.modelMapperHelper(estadoOperacionDTO,	WSObtenerResultOperacionResponse.class);
			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
			}
		}
		this.llenarDatosComunes(response, success);

		LOGGER.info("[WS FIN] obtenerResultadoOp: " + request.getHeader().getIdRequerimiento());
		
		return response;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.ws.service.WSFinancieroService#obtenerOperaciones(ar.com.redlink.sam.financieroservices.ws.request.WSObtenerOperacionesRequest)
	 */
	@Override
	public WSObtenerOperacionesResponse obtenerOperaciones(WSObtenerOperacionesRequest request) {
		
		LOGGER.info("[WS INI] obtenerOperaciones: " + this.requestToString(request.getHeader()));
		
		boolean success = true;
		//LOGGER.debug("Parametros recibidos: " + this.requestToString(request.getHeader()));
		ValidationDTO validation = createValidationDTO(request);
		WSObtenerOperacionesResponse response = new WSObtenerOperacionesResponse();

		try {
			financieroValidationChain.validate(validation);
		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}

		if (success) {
			TerminalRequestDTO terminalRequestDTO = modelMapperService.modelMapperHelper(request.getObtenerOperacionesRequest(), TerminalRequestDTO.class);

			this.llenarResultadosValidation(terminalRequestDTO, validation);

			try {
				UltimasOperacionesDTO ultimasOperacionesDTO = this.getFinancieroService().getLatestOperations(terminalRequestDTO);

				response = modelMapperService.modelMapperHelper(ultimasOperacionesDTO,WSObtenerOperacionesResponse.class);

			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
			}
		}

		this.llenarDatosComunes(response, success);
		LOGGER.info("[WS FIN] obtenerOperaciones: " + request.getHeader().getIdRequerimiento());
		return response;
	}

	@Override
	public WSConsultarOperacionesPEIResponse consultarOperacionesPEI(WSConsultarOperacionesPEIRequest request) {
		boolean success = true;
		LOGGER.info("*[WS INI]* consultarOperacionesPEI: " + this.requestToString(request.getHeader()));
		ValidationDTO validation = createValidationDTO(request);
		WSConsultarOperacionesPEIResponse response = new WSConsultarOperacionesPEIResponse();

		try {
			financieroValidationChain.validate(validation);
			// unicos datos obligatorios			
			notEmpty(request.getConsultarOperacionesPEIRequest().getSecuenciaOn(), FINA156);
			notEmpty(request.getConsultarOperacionesPEIRequest().getTrack2(), FINA152);
			notEmpty(request.getConsultarOperacionesPEIRequest().getTrack2(), FINA153);
		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}

		if (success) {			
			ConsultaOperacionPEIRequestDTO requestDTO = null;
			
			Intencion intenc= this.getFinancieroService().registrarInicioTransaccionPei(
					(Long)validation.getOutputValue(ValidationDTO.TERMINAL_SAM_ID),
					Integer.valueOf(OperationEnum.CONSULTA_OPERACIONES_PEI.getCode()),
					(String)String.valueOf(validation.getInputValue(ValidationDTO.ID_REQ)) );
			
			try {				
				requestDTO = setearConsulta(request, requestDTO);				
				
				ConsultaOperacionPEIDTO ultimasOperacionesDTO = this.getFinancieroService().getOperationsPEI(
						validation.getInputValue(ValidationDTO.ID_REQ),
						validation.getInputValue(ValidationDTO.IP_CLIENTE), validation, requestDTO,
						request.getConsultarOperacionesPEIRequest().getSecuenciaOn(), intenc);

				response = modelMapperService.modelMapperHelper(ultimasOperacionesDTO,WSConsultarOperacionesPEIResponse.class);

			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
				
				//intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj());
				intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj()+   (e.getMsjInterno()!=null?"->"+e.getMsjInterno():""));
				
				if (e instanceof FinancieroTandemException) {
					FinancieroTandemException tException = (FinancieroTandemException) e;
					
					if( tException.getMsjTandem()!=null)
						intenc.setObservacion(intenc.getObservacion()+"*"+ tException.getMsjTandem().trim());
				}
				this.getFinancieroService().registrarErrorTransaccion(intenc);
				
			}
			
			this.llenarDatosComunes(response, success);
		}
		
		LOGGER.info("*[WS FIN]* consultarOperacionesPEI: " + request.getHeader().getIdRequerimiento());
		return response;
	}

	private ConsultaOperacionPEIRequestDTO setearConsulta(WSConsultarOperacionesPEIRequest request, ConsultaOperacionPEIRequestDTO requestDTO)
			throws FinancieroException {
		requestDTO = new ConsultaOperacionPEIRequestDTO();
		requestDTO.setConsulta(new ConsultaPEIRequestDTO());
		requestDTO.getConsulta().setEstado(request.getConsultarOperacionesPEIRequest().getEstado());
		requestDTO.getConsulta().setFechaDesde(request.getConsultarOperacionesPEIRequest().getFechadesde());
		requestDTO.getConsulta().setFechaHasta(request.getConsultarOperacionesPEIRequest().getFechahasta());
		requestDTO.getConsulta().setIdOperacion(request.getConsultarOperacionesPEIRequest().getIdoperacion());
		requestDTO.getConsulta().setTracks(new RealizarTransferenciaPEITrackDataRequestDTO());
		requestDTO.getConsulta().getTracks().setTrack1(request.getConsultarOperacionesPEIRequest().getTrack1());
		requestDTO.getConsulta().getTracks().setTrack2(request.getConsultarOperacionesPEIRequest().getTrack2());
		ConsultaPEIRequestDTO consultaDTO = requestDTO.getConsulta();
		consultaDTO.setIdterminal(request.getHeader().getTerminal());				
		consultaDTO = setearFechas(consultaDTO);			
		consultaDTO.setIdOperacion(request.getConsultarOperacionesPEIRequest().getIdoperacion());
		requestDTO.setConsulta(consultaDTO);
		requestDTO.setOrigen(new OrigenPEIRequestDTO());
		requestDTO.getOrigen().setIdCanal(this.getIdCanal());
		requestDTO.getConsulta().setIdReferenciaTrxComercio(request.getConsultarOperacionesPEIRequest().getIdRequerimientoOrigen());
		requestDTO.getConsulta().setIdReferenciaOperacionComercio(request.getConsultarOperacionesPEIRequest().getIdreferenciaoperacioncomercio());
		requestDTO.getConsulta().setNumeroReferenciaBancaria(request.getConsultarOperacionesPEIRequest().getNumeroReferenciaBancaria());
		
		return requestDTO;
	}

	private ConsultaPEIRequestDTO setearFechas(ConsultaPEIRequestDTO consultaDTO) throws FinancieroException {		
		try {	
			DateFormat formatSoap = new SimpleDateFormat("dd/MM/yyyy");
			DateFormat formatRest = new SimpleDateFormat("yyyy-MM-dd");
			Date fechaDesde = formatSoap.parse(consultaDTO.getFechaDesde());			
			Date fechaHasta = formatSoap.parse(consultaDTO.getFechaHasta());			
			consultaDTO.setFechaDesde(formatRest.format(fechaDesde));
			consultaDTO.setFechaHasta(formatRest.format(fechaHasta));		
		} catch (ParseException e) {		
			throw new FinancieroException(FINA163.getCode(), FINA163.getMsg());		
		}		
		return consultaDTO;
	}

	/*
	 * AQM
	 * PEI desde PAGAR 
	 * ConssultaPEIPagar
	 * */
	@Override
	public WSConsultarOperacionesPEIPagarResponse consultarOperacionesPEIPagar(WSConsultarOperacionesPEIPagarRequest request) {
		boolean success = true;
		LOGGER.info("*[WS INI]* consultarOperacionesPEIPagar: " + this.requestToString(request.getHeader()));
		ValidationDTO validation = createValidationDTO(request);
		WSConsultarOperacionesPEIPagarResponse response = new WSConsultarOperacionesPEIPagarResponse();

		try {
			financieroValidationChain.validate(validation);
			// unicos datos obligatorios			
			notEmpty(request.getConsultarOperacionesPEIPagarRequest().getSecuenciaOn(), FINA156);
			notEmpty(request.getConsultarOperacionesPEIPagarRequest().getTracks().getTrack1(), FINA152);
			notEmpty(request.getConsultarOperacionesPEIPagarRequest().getTracks().getTrack2(), FINA153);
			
			
		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}

		if (success) {			
			ConsultaOperacionPEIPagarRequestDTO requestDTO = null;
			
			Intencion intenc= this.getFinancieroService().registrarInicioTransaccionPei(
					(Long)validation.getOutputValue(ValidationDTO.TERMINAL_SAM_ID),
					Integer.valueOf(OperationEnum.CONSULTA_OPERACIONES_PEI.getCode()),
					(String)String.valueOf(validation.getInputValue(ValidationDTO.ID_REQ)) );
			
			try {				
				requestDTO = setearConsulta(request, requestDTO);				
				
				ConsultaOperacionPEIPagarDTO ultimasOperacionesDTO = this.getFinancieroService().getOperationsPEIPagar(
						validation.getInputValue(ValidationDTO.ID_REQ),
						validation.getInputValue(ValidationDTO.IP_CLIENTE), validation, requestDTO,
						request.getConsultarOperacionesPEIPagarRequest().getSecuenciaOn(), intenc);

				response = modelMapperService.modelMapperHelper(ultimasOperacionesDTO,WSConsultarOperacionesPEIPagarResponse.class);

			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
				
				//intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj());
				intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj()+   (e.getMsjInterno()!=null?"->"+e.getMsjInterno():""));
				
				if (e instanceof FinancieroTandemException) {
					FinancieroTandemException tException = (FinancieroTandemException) e;
					
					if( tException.getMsjTandem()!=null)
						intenc.setObservacion(intenc.getObservacion()+"*"+ tException.getMsjTandem().trim());
				}
				this.getFinancieroService().registrarErrorTransaccion(intenc);
				
			}
			
			this.llenarDatosComunes(response, success);
		}
		
		LOGGER.info("*[WS FIN]* consultarOperacionesPEI: " + request.getHeader().getIdRequerimiento());
		return response;
	}

	/*
	 * AQM
	 * */	
	private ConsultaOperacionPEIPagarRequestDTO setearConsulta(WSConsultarOperacionesPEIPagarRequest request, ConsultaOperacionPEIPagarRequestDTO requestDTO)
			throws FinancieroException {
		requestDTO = new ConsultaOperacionPEIPagarRequestDTO();
		/*AQM enlacePagos tiene DOS idCanal y SOLO recibimos 1 SOLO canal en ConsultarOperacionesPEIPagar*/
		requestDTO.setOrigen(new OrigenPEIRequestDTO());		
		requestDTO.getOrigen().setIdCanal(request.getConsultarOperacionesPEIPagarRequest().getCanal());
		
		requestDTO.setConsulta(new ConsultaPEIPagarRequestDTO());
		requestDTO.getConsulta().setIdOperacion(request.getConsultarOperacionesPEIPagarRequest().getIdOperacion());
		requestDTO.getConsulta().setIdOperacionOrigen(request.getConsultarOperacionesPEIPagarRequest().getIdOperacionOrigen());

		requestDTO.getConsulta().setCodTerminal(request.getConsultarOperacionesPEIPagarRequest().getEntidad().getCodTerminal());
		requestDTO.getConsulta().setCodSucursal(request.getConsultarOperacionesPEIPagarRequest().getEntidad().getCodSucursal());
		requestDTO.getConsulta().setEntidad(new EntidadPEIRequestDTO());
		requestDTO.getConsulta().getEntidad().setFiidEntidad(request.getConsultarOperacionesPEIPagarRequest().getEntidad().getFiidEntidad());
		requestDTO.getConsulta().getEntidad().setCodComercio("");
		/*AQM enlacePagos tiene DOS idCanal y SOLO recibimos 1 SOLO canal en ConsultarOperacionesPEIPagar*/
		requestDTO.getConsulta().setIdCanal(request.getConsultarOperacionesPEIPagarRequest().getCanal());
		requestDTO.getConsulta().setTracks(new RealizarTransferenciaPEITrackDataRequestDTO());
		requestDTO.getConsulta().getTracks().setTrack1(request.getConsultarOperacionesPEIPagarRequest().getTracks().getTrack1());
		requestDTO.getConsulta().getTracks().setTrack2(request.getConsultarOperacionesPEIPagarRequest().getTracks().getTrack2());
		requestDTO.getConsulta().getTracks().setTrack3(request.getConsultarOperacionesPEIPagarRequest().getTracks().getTrack3());
		requestDTO.getConsulta().setIdOperacion(request.getConsultarOperacionesPEIPagarRequest().getIdOperacion());
		requestDTO.getConsulta().setIdReferenciaOperacion(request.getConsultarOperacionesPEIPagarRequest().getIdReferenciaOperacion());
		requestDTO.getConsulta().setIdReferenciaTrx(request.getConsultarOperacionesPEIPagarRequest().getIdReferenciaTrx());
		requestDTO.getConsulta().setNumeroReferenciaBancaria(request.getConsultarOperacionesPEIPagarRequest().getNumeroReferenciaBancaria());
		requestDTO.getConsulta().setEstado(request.getConsultarOperacionesPEIPagarRequest().getEstado());			
		requestDTO.getConsulta().setFechaDesde(request.getConsultarOperacionesPEIPagarRequest().getFechadesde());
		requestDTO.getConsulta().setFechaHasta(request.getConsultarOperacionesPEIPagarRequest().getFechahasta());				
		ConsultaPEIPagarRequestDTO consultaDTO = requestDTO.getConsulta();			
		consultaDTO = setearFechasPEIPagar(consultaDTO);			
		requestDTO.setConsulta(consultaDTO);		
		
		return requestDTO;
	}
	/*
	 * AQM
	 * */
	private ConsultaPEIPagarRequestDTO setearFechasPEIPagar(ConsultaPEIPagarRequestDTO consultaDTO) throws FinancieroException {		
		try {	
			DateFormat formatSoap = new SimpleDateFormat("dd/MM/yyyy");
			DateFormat formatRest = new SimpleDateFormat("yyyy-MM-dd");
			Date fechaDesde = formatSoap.parse(consultaDTO.getFechaDesde());			
			Date fechaHasta = formatSoap.parse(consultaDTO.getFechaHasta());
			
			consultaDTO.setFechaDesde(formatRest.format(fechaDesde));
			consultaDTO.setFechaHasta(formatRest.format(fechaHasta));
			
		} catch (ParseException e) {		
			throw new FinancieroException(FINA163.getCode(), FINA163.getMsg());		
		}		
		return consultaDTO;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.ws.service.WSFinancieroService#obtenerSaldo(ar.com.redlink.sam.financieroservices.ws.request.WSObtenerSaldoCuentaRequest)
	 */
	@Override
	public WSObtenerSaldoCuentaResponse obtenerSaldo(WSObtenerSaldoCuentaRequest request) {
		
		LOGGER.info("*[WS INI]* obtenerSaldo: " + this.requestToString(request.getHeader()));
		boolean success = true;
		//LOGGER.debug("Parametros recibidos: " + this.requestToString(request.getHeader()));
		ValidationDTO validation = createValidationDTO(request);
		WSObtenerSaldoCuentaResponse response = new WSObtenerSaldoCuentaResponse();

		try {
			financieroValidationChain.validate(validation);
		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}
		if (success) {
			BalanceRequestDTO balanceRequestDTO = modelMapperService.modelMapperHelper(request.getObtenerSaldoRequest(),BalanceRequestDTO.class);

			this.llenarResultadosValidation(balanceRequestDTO, validation);

			balanceRequestDTO.setPrefijoOpId(   String.valueOf(OperationEnum.CONSULTA_SALDO.getCode())   );
			Intencion intenc= this.getFinancieroService().registrarInicioTransaccion(balanceRequestDTO);
			
			try {
				EstadoCuentaDTO estadoCuentaDTO = this.getFinancieroService().getBalance(balanceRequestDTO, intenc);

				response = modelMapperService.modelMapperHelper(estadoCuentaDTO, WSObtenerSaldoCuentaResponse.class);
			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
				
				//intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj());
				if(intenc!=null){
					intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj()+   (e.getMsjInterno()!=null?"->"+e.getMsjInterno():""));
					
					if (e instanceof FinancieroTandemException) {
						FinancieroTandemException tException = (FinancieroTandemException) e;
						
						if( tException.getMsjTandem()!=null)
							intenc.setObservacion(intenc.getObservacion()+"*"+ tException.getMsjTandem().trim());
					}
					this.getFinancieroService().registrarErrorTransaccion(intenc);	
				}
				
			}
		}

		this.llenarDatosComunes(response, success);
		LOGGER.info("*[WS FIN]* obtenerSaldo: " + request.getHeader().getIdRequerimiento());
		return response;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.ws.service.WSFinancieroService#validarTarjeta(ar.com.redlink.sam.financieroservices.ws.request.WSValidarTarjetaRequest)
	 */
	@Override
	public WSValidarTarjetaResponse validarTarjeta(WSValidarTarjetaRequest request) {
		
		LOGGER.info("*[WS INI]* validarTarjeta: " + this.requestToString(request.getHeader()));
		boolean success = true;
		//LOGGER.debug("Parametros recibidos: " + this.requestToString(request.getHeader()));
		ValidationDTO validation = createValidationDTO(request);
		WSValidarTarjetaResponse response = new WSValidarTarjetaResponse();

		try {
			financieroValidationChain.validate(validation);
		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}

		if (success) {
			
			ValidarTarjetaRequestDTO validarTarjetaRequestDTO = modelMapperService.modelMapperHelper(request.getValidarTarjetaRequest(), ValidarTarjetaRequestDTO.class);
			this.llenarResultadosValidation(validarTarjetaRequestDTO, validation);
	
			validarTarjetaRequestDTO.setPrefijoOpId(   String.valueOf(OperationEnum.CUENTAS_RELACIONADAS.getCode())   );			
			Intencion intenc= this.getFinancieroService().registrarInicioTransaccion(validarTarjetaRequestDTO);
			
			ValidacionTarjetaDTO validacionTarjetaDTO;
			try {
				validacionTarjetaDTO = this.getFinancieroService().validateCard(validarTarjetaRequestDTO, intenc);
				response = modelMapperService.modelMapperHelper(validacionTarjetaDTO, WSValidarTarjetaResponse.class);

				if (validacionTarjetaDTO.getCuentas() == null) {
					// Esto se hace para proveer una respuesta sin cuentas e
					// informando la validacion que fallo para que esto ocurra.
					success = false;
					response.setHeader(new WSHeaderResponse(DateUtil.timestampFormat(new Date())));
					response.getHeader().setCodigoRespuesta(validacionTarjetaDTO.getCodError());
					response.getHeader().setDescripcionRespuesta(validacionTarjetaDTO.getDescripcionError());
					response.getHeader().setCodigoRespuestaB24("00");
				}
			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
				
				//intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj());
				intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj()+   (e.getMsjInterno()!=null?"->"+e.getMsjInterno():""));
				
				if (e instanceof FinancieroTandemException) {
					FinancieroTandemException tException = (FinancieroTandemException) e;
				
					if( tException.getMsjTandem()!=null)
						intenc.setObservacion(intenc.getObservacion()+"*"+ tException.getMsjTandem().trim());
				}
				this.getFinancieroService().registrarErrorTransaccion(intenc);
			}

			this.llenarDatosComunes(response, success);
		}
		
		LOGGER.info("*[WS FIN]* validarTarjeta: " + request.getHeader().getIdRequerimiento());
		return response;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.ws.service.WSFinancieroService#realizarExtraccion(ar.com.redlink.sam.financieroservices.ws.request.WSRealizarExtraccionRequest)
	 */
	@Override
	public WSRealizarExtraccionResponse realizarExtraccion(WSRealizarExtraccionRequest request) {
		boolean success = true;
		LOGGER.info("*[WS INI]* realizarExtraccion: " + this.requestToString(request.getHeader()));
		ValidationDTO validation = createValidationDTO(request);
		WSRealizarExtraccionResponse response = new WSRealizarExtraccionResponse();

		try {
			financieroValidationChain.validate(validation);
		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}

		if (success) {
			ExtraccionRequestDTO realizarExtraccionRequestDTO = modelMapperService.modelMapperHelper(request.getRealizarExtraccionRequest(), ExtraccionRequestDTO.class);

			this.llenarResultadosValidation(realizarExtraccionRequestDTO, validation);
			
			realizarExtraccionRequestDTO.setPrefijoOpId(   String.valueOf(OperationEnum.EXTRACCION.getCode())   );
			Intencion intenc= this.getFinancieroService().registrarInicioTransaccion(realizarExtraccionRequestDTO);

			ExtraccionResultadoDTO extraccionResultadoDTO;
			try {
				extraccionResultadoDTO = this.getFinancieroService().doWithdraw(realizarExtraccionRequestDTO, intenc);
				response = modelMapperService.modelMapperHelper(extraccionResultadoDTO,	WSRealizarExtraccionResponse.class);
			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
				
				//intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj());
				intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj()+   (e.getMsjInterno()!=null?"->"+e.getMsjInterno():""));
				
				if (e instanceof FinancieroTandemException) {
					FinancieroTandemException tException = (FinancieroTandemException) e;
					
					if( tException.getMsjTandem()!=null)
						intenc.setObservacion(intenc.getObservacion()+"*"+ tException.getMsjTandem().trim());
				}
				this.getFinancieroService().registrarErrorTransaccion(intenc);
			}

			this.llenarDatosComunes(response, success);
		}

		LOGGER.info("*[WS FIN]*  realizarExtraccion: " + request.getHeader().getIdRequerimiento());
		
		return response;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.ws.service.WSFinancieroService#anularExtraccion(ar.com.redlink.sam.financieroservices.ws.request.WSAnularExtraccionRequest)
	 */
	@Override
	public WSAnularExtraccionResponse anularExtraccion(WSAnularExtraccionRequest request) {

		LOGGER.info("*[WS INI]* anularExtraccion: " + this.requestToString(request.getHeader()));

		boolean success = true;
		ValidationDTO validation = createValidationDTO(request);

		WSAnularExtraccionResponse response = new WSAnularExtraccionResponse();

		try {
			financieroValidationChain.validate(validation);
		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}

		if (success) {
			AnularExtraccionRequestDTO anularExtraccionRequestDTO = modelMapperService.modelMapperHelper(request.getAnularExtraccionRequest(), AnularExtraccionRequestDTO.class);

			this.llenarResultadosValidation(anularExtraccionRequestDTO, validation);
			
			anularExtraccionRequestDTO.setPrefijoOpId(   String.valueOf(OperationEnum.ANULAR_EXTRACCION.getCode())   );
			Intencion intenc= this.getFinancieroService().registrarInicioTransaccion(anularExtraccionRequestDTO);

			AnularExtraccionDTO anularExtraccionDTO;
			try {
				anularExtraccionDTO = this.getFinancieroService().rollbackWithdraw(anularExtraccionRequestDTO, intenc);
				response = modelMapperService.modelMapperHelper(anularExtraccionDTO, WSAnularExtraccionResponse.class);
			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
				
				//intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj());
				intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj()+   (e.getMsjInterno()!=null?"->"+e.getMsjInterno():""));
				
				if (e instanceof FinancieroTandemException) {
					FinancieroTandemException tException = (FinancieroTandemException) e;
					
					if( tException.getMsjTandem()!=null)
						intenc.setObservacion(intenc.getObservacion()+"*"+ tException.getMsjTandem().trim());
				}
				this.getFinancieroService().registrarErrorTransaccion(intenc);
			}
			this.llenarDatosComunes(response, success);
		}
		
		LOGGER.info("*[WS FIN]* anularExtraccion: " + request.getHeader().getIdRequerimiento());
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.ws.service.WSFinancieroService#realizarDebito(ar.com.redlink.sam.financieroservices.ws.request.WSRealizarDebitoRequest)
	 */
	@Override
	public WSRealizarDebitoResponse realizarDebito(WSRealizarDebitoRequest request) {
		
		LOGGER.info("*[WS INI]* realizarDebito: " + this.requestToString(request.getHeader()));

		boolean success = true;
		ValidationDTO validation = createValidationDTO(request);

		WSRealizarDebitoResponse response = new WSRealizarDebitoResponse();

		try {
			financieroValidationChain.validate(validation);
		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}

		if (success) {
			RealizarDebitoRequestDTO realizarDebitoRequestDTO = modelMapperService.modelMapperHelper(request.getRealizarDebitoRequest(), RealizarDebitoRequestDTO.class);

			this.llenarResultadosValidation(realizarDebitoRequestDTO, validation);
			
			realizarDebitoRequestDTO.setPrefijoOpId(   String.valueOf(OperationEnum.DEBITO.getCode())   );
			Intencion intenc= this.getFinancieroService().registrarInicioTransaccion(realizarDebitoRequestDTO);

			try {
				RealizarDebitoDTO realizarDebitoDTO = this.getFinancieroService().doDebit(realizarDebitoRequestDTO, intenc);
				response = modelMapperService.modelMapperHelper(realizarDebitoDTO, WSRealizarDebitoResponse.class);
				
			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
				
				//intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj());
				intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj()+   (e.getMsjInterno()!=null?"->"+e.getMsjInterno():""));
				
				if (e instanceof FinancieroTandemException) {
					FinancieroTandemException tException = (FinancieroTandemException) e;
					
					if( tException.getMsjTandem()!=null)
						intenc.setObservacion(intenc.getObservacion()+"*"+ tException.getMsjTandem().trim());
				}
				this.getFinancieroService().registrarErrorTransaccion(intenc);
			}
		}

		this.llenarDatosComunes(response, success);

		LOGGER.info("*[WS FIN]* realizarDebito: " + request.getHeader().getIdRequerimiento());
		
		return response;
	}
			
	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.ws.service.WSFinancieroService#realizarDebito(ar.com.redlink.sam.financieroservices.ws.request.WSRealizarDebitoRequest)
	 */
	@Override
	public WSRealizarPagoDebitoResponse realizarPagoDebito(	WSRealizarPagoDebitoRequest request) {
		
		LOGGER.info("*[WS INI]* realizarPagoDebito: " + this.requestToString(request.getHeader()));
		boolean success = true;
		ValidationDTO validation = createValidationDTO(request);
		WSRealizarPagoDebitoResponse response = new WSRealizarPagoDebitoResponse();

		try {
			financieroValidationChain.validate(validation);
		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}

		if (success) {
			RealizarPagoDebitoRequestDTO realizarPagoDebitoRequestDTO = modelMapperService.modelMapperHelper(request.getRealizarPagoDebitoRequest(),RealizarPagoDebitoRequestDTO.class);

			this.llenarResultadosValidation(realizarPagoDebitoRequestDTO,validation);
			
			realizarPagoDebitoRequestDTO.setPrefijoOpId(   String.valueOf(OperationEnum.PAGO_CON_DEBITO.getCode())   );
			Intencion intenc= this.getFinancieroService().registrarInicioTransaccion(realizarPagoDebitoRequestDTO);

			try {
				RealizarPagoDebitoDTO realizarPagoDebitoDTO = this.getFinancieroService().doDebitPay(realizarPagoDebitoRequestDTO, intenc);
				response = modelMapperService.modelMapperHelper(realizarPagoDebitoDTO, WSRealizarPagoDebitoResponse.class);
			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
				
				//intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj());
				intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj()+   (e.getMsjInterno()!=null?"->"+e.getMsjInterno():""));
				
				if (e instanceof FinancieroTandemException) {
					FinancieroTandemException tException = (FinancieroTandemException) e;
					
					if( tException.getMsjTandem()!=null)
						intenc.setObservacion(intenc.getObservacion()+"*"+ tException.getMsjTandem().trim());
				}
				this.getFinancieroService().registrarErrorTransaccion(intenc);
			}
		}

		this.llenarDatosComunes(response, success);
		LOGGER.info("[WS FIN] realizarPagoDebito: " + request.getHeader().getIdRequerimiento());
		return response;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.ws.service.WSFinancieroService#anularDebito(ar.com.redlink.sam.financieroservices.ws.service.WSAnularDebitoRequest)
	 */
	@Override
	public WSAnularDebitoResponse anularDebito(WSAnularDebitoRequest request) {
		
		LOGGER.info("*[WS INI]* anularDebito: " + this.requestToString(request.getHeader()));

		boolean success = true;
		ValidationDTO validation = createValidationDTO(request);

		WSAnularDebitoResponse response = new WSAnularDebitoResponse();

		try {
			financieroValidationChain.validate(validation);
		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}

		if (success) {
			AnularDebitoRequestDTO anularDebitoRequestDTO = modelMapperService.modelMapperHelper(request.getAnularDebitoRequest(), AnularDebitoRequestDTO.class);
			this.llenarResultadosValidation(anularDebitoRequestDTO, validation);
			AnularDebitoDTO anularDebitoDTO;
			
			anularDebitoRequestDTO.setPrefijoOpId(   String.valueOf(OperationEnum.ANULACION_DEBITO.getCode())   );
			Intencion intenc= this.getFinancieroService().registrarInicioTransaccion(anularDebitoRequestDTO);
			
			try {
				anularDebitoDTO = this.getFinancieroService().rollbackDebit(anularDebitoRequestDTO, intenc);
				response = modelMapperService.modelMapperHelper(anularDebitoDTO, WSAnularDebitoResponse.class);
			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
				
				//intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj());
				intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj()+   (e.getMsjInterno()!=null?"->"+e.getMsjInterno():""));
				
				if (e instanceof FinancieroTandemException) {
					FinancieroTandemException tException = (FinancieroTandemException) e;
					
					if( tException.getMsjTandem()!=null)
						intenc.setObservacion(intenc.getObservacion()+"*"+ tException.getMsjTandem().trim());
				}
				this.getFinancieroService().registrarErrorTransaccion(intenc);
			}

			this.llenarDatosComunes(response, success);
		}

		LOGGER.info("*[WS FIN]* anularDebito: " + request.getHeader().getIdRequerimiento());
		return response;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.ws.service.WSFinancieroService#realizarDeposito(ar.com.redlink.sam.financieroservices.ws.request.WSRealizarDepositoRequest)
	 */
	@Override
	public WSRealizarDepositoResponse realizarDeposito(WSRealizarDepositoRequest request) {
		
		LOGGER.info("*[WS INI]* realizarDeposito: " + this.requestToString(request.getHeader()));
		boolean success = true;
		ValidationDTO validation = createValidationDTO(request);
		WSRealizarDepositoResponse response = new WSRealizarDepositoResponse();

		try {
			financieroValidationChain.validate(validation);
		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}

		if (success) {
			RealizarDepositoRequestDTO realizarDepositoRequestDTO = modelMapperService.modelMapperHelper(request.getRealizarDepositoRequest(), RealizarDepositoRequestDTO.class);

			this.llenarResultadosValidation(realizarDepositoRequestDTO, validation);
			
			realizarDepositoRequestDTO.setPrefijoOpId(   String.valueOf(OperationEnum.DEPOSITO.getCode())   );
			Intencion intenc= this.getFinancieroService().registrarInicioTransaccion(realizarDepositoRequestDTO);
			
			RealizarDepositoDTO realizarDepositoDTO;
			try {
				realizarDepositoDTO = this.getFinancieroService().doDeposit(realizarDepositoRequestDTO, intenc);
				response = modelMapperService.modelMapperHelper(realizarDepositoDTO, WSRealizarDepositoResponse.class);
			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
				
				//intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj());
				intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj()+   (e.getMsjInterno()!=null?"->"+e.getMsjInterno():""));
				
				if (e instanceof FinancieroTandemException) {
					FinancieroTandemException tException = (FinancieroTandemException) e;
					
					if( tException.getMsjTandem()!=null)
						intenc.setObservacion(intenc.getObservacion()+"*"+ tException.getMsjTandem().trim());
				}
				this.getFinancieroService().registrarErrorTransaccion(intenc);
			}

			this.llenarDatosComunes(response, success);
		}
		
		LOGGER.info("*[WS FIN]* realizarDeposito: " + request.getHeader().getIdRequerimiento());
		return response;
	}
			
	/**
	 * @see ar.com.redlink.sam.financieroservices.ws.service.WSFinancieroService#anularDebito(ar.com.redlink.sam.financieroservices.ws.service.WSAnularDebitoRequest)
	 */
	@Override
	public WSAnularPagoDebitoResponse anularPagoDebito(WSAnularPagoDebitoRequest request) {
		
		LOGGER.info("*[WS INI]* anularPagoDebito: " + this.requestToString(request.getHeader()));
		
		boolean success = true;
		ValidationDTO validation = createValidationDTO(request);

		WSAnularPagoDebitoResponse response = new WSAnularPagoDebitoResponse();

		try {
			financieroValidationChain.validate(validation);
		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}

		if (success) {
			AnularDebitoRequestDTO anularDebitoRequestDTO = modelMapperService.modelMapperHelper(request.getAnularPagoDebitoRequest(),AnularDebitoRequestDTO.class);

			this.llenarResultadosValidation(anularDebitoRequestDTO, validation);
			
			anularDebitoRequestDTO.setPrefijoOpId(   String.valueOf(OperationEnum.ANULACION_CON_DEBITO.getCode())   );
			Intencion intenc= this.getFinancieroService().registrarInicioTransaccion(anularDebitoRequestDTO);
			
			AnularDebitoDTO anularDebitoDTO;
			try {
				anularDebitoDTO = this.getFinancieroService().rollbackDebitPay(anularDebitoRequestDTO, intenc);

				response = modelMapperService.modelMapperHelper(anularDebitoDTO, WSAnularPagoDebitoResponse.class);
			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
				
				//intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj());
				intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj()+   (e.getMsjInterno()!=null?"->"+e.getMsjInterno():""));
				
				if (e instanceof FinancieroTandemException) {
					FinancieroTandemException tException = (FinancieroTandemException) e;
					
					if( tException.getMsjTandem()!=null)
						intenc.setObservacion(intenc.getObservacion()+"*"+ tException.getMsjTandem().trim());
				}
				this.getFinancieroService().registrarErrorTransaccion(intenc);
				
			}

			this.llenarDatosComunes(response, success);
		}

		LOGGER.info("*[WS FIN]* anularPagoDebito: " + request.getHeader().getIdRequerimiento());
		return response;
	}
	
	/**
	 * @see ar.com.redlink.sam.financieroservices.ws.service.WSFinancieroService#anularDeposito(ar.com.redlink.sam.financieroservices.ws.request.WSAnularDepositoRequest)
	 */
	@Override
	public WSAnularDepositoResponse anularDeposito(WSAnularDepositoRequest request) {
		
		LOGGER.info("[WS INI] anularDeposito: " + this.requestToString(request.getHeader()));

		boolean success = true;
		ValidationDTO validation = createValidationDTO(request);

		WSAnularDepositoResponse response = new WSAnularDepositoResponse();

		try {
			financieroValidationChain.validate(validation);
		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}

		if (success) {
			AnularDepositoRequestDTO anularDepositoRequestDTO = modelMapperService.modelMapperHelper(request.getAnularDepositoRequest(), AnularDepositoRequestDTO.class);

			this.llenarResultadosValidation(anularDepositoRequestDTO, validation);
			AnularDepositoDTO anularDepositoDTO;
			
			anularDepositoRequestDTO.setPrefijoOpId(   String.valueOf(OperationEnum.ANULACION_DEPOSITO.getCode())   );
			Intencion intenc= this.getFinancieroService().registrarInicioTransaccion(anularDepositoRequestDTO);

			try {
				anularDepositoDTO = this.getFinancieroService().rollbackDeposit(anularDepositoRequestDTO, intenc);
				response = modelMapperService.modelMapperHelper(anularDepositoDTO, WSAnularDepositoResponse.class);
			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
				
				//intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj());
				intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj()+   (e.getMsjInterno()!=null?"->"+e.getMsjInterno():""));

				
				if (e instanceof FinancieroTandemException) {
					FinancieroTandemException tException = (FinancieroTandemException) e;
					
					if( tException.getMsjTandem()!=null)
						intenc.setObservacion(intenc.getObservacion()+"*"+ tException.getMsjTandem().trim());
				}
				this.getFinancieroService().registrarErrorTransaccion(intenc);
				
			}

			this.llenarDatosComunes(response, success);
		}

		LOGGER.info("[WS FIN] anularDeposito: " + request.getHeader().getIdRequerimiento());
		return response;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.ws.service.WSFinancieroService#realizarTransferencia(ar.com.redlink.sam.financieroservices.ws.request.WSRealizarTransferenciaRequest)
	 */
	@Override
	public WSRealizarTransferenciaResponse realizarTransferencia(WSRealizarTransferenciaRequest request) {
		LOGGER.info("*[WS INI]* realizarTransferencia: " + this.requestToString(request.getHeader()));

		boolean success = true;
		ValidationDTO validation = createValidationDTO(request);

		WSRealizarTransferenciaResponse response = new WSRealizarTransferenciaResponse();

		try {
			financieroValidationChain.validate(validation);
		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}

		if (success) {
			RealizarTransferenciaRequestDTO realizarTransferenciaRequestDTO = modelMapperService.modelMapperHelper(
					request.getRealizarTransferenciaRequest(), RealizarTransferenciaRequestDTO.class);

			this.llenarResultadosValidation(realizarTransferenciaRequestDTO, validation);
			
			realizarTransferenciaRequestDTO.setPrefijoOpId(   String.valueOf(OperationEnum.TRANSFERENCIA.getCode())   );
			Intencion intenc= this.getFinancieroService().registrarInicioTransaccion(realizarTransferenciaRequestDTO);
			
			try {
				RealizarTransferenciaDTO realizarTransferenciaDTO = this.getFinancieroService().doTransfer(realizarTransferenciaRequestDTO, intenc);
				response = modelMapperService.modelMapperHelper(realizarTransferenciaDTO,WSRealizarTransferenciaResponse.class);

			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
				
				intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj());
				if (e instanceof FinancieroTandemException) {
					FinancieroTandemException tException = (FinancieroTandemException) e;
					
					if( tException.getMsjTandem()!=null)
						intenc.setObservacion(intenc.getObservacion()+"*"+ tException.getMsjTandem().trim());
				}
				this.getFinancieroService().registrarErrorTransaccion(intenc);
				
			}
		}

		this.llenarDatosComunes(response, success);

		LOGGER.info("*[WS FIN]* realizarTransferencia: " + request.getHeader().getIdRequerimiento());
		return response;
	}

	@Override
	public WSRealizarTransferenciaPEIResponse realizarPagoPEI(WSRealizarTransferenciaPEIRequest request) {
		LOGGER.info("*[WS INI]* realizarPagoPEI: " + this.requestToString(request.getHeader()));

		boolean success = true;
		ValidationDTO validation = createValidationDTO(request);

		WSRealizarTransferenciaPEIResponse response = new WSRealizarTransferenciaPEIResponse();
		
		try {

			financieroValidationChain.validate(validation);
			this.validarDatoPEI(request);

		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}
		
		if (success) {			
			
			Intencion intenc= this.getFinancieroService().registrarInicioTransaccionPei(
					(Long)validation.getOutputValue(ValidationDTO.TERMINAL_SAM_ID),
					Integer.valueOf(OperationEnum.PAGO_PEI.getCode()),
					(String)String.valueOf(validation.getInputValue(ValidationDTO.ID_REQ)) );
			
			try {
				RealizarTransferenciaPEIRequestDTO realizarTransferenciaPEIRequestDTO = popularLoQueQuedaRealizarPagoPEI(request);
			
				RealizarTransferenciaPEIDTO realizarTransferenciaPEIDTO = this.getFinancieroService().doTransferPEI(
						validation.getInputValue(ValidationDTO.ID_REQ),
						validation.getInputValue(ValidationDTO.IP_CLIENTE), validation,
						realizarTransferenciaPEIRequestDTO,
						request.getRealizarTransferenciaPEIRequest().getSecuenciaOn(),
						intenc);
				
				response = modelMapperService.modelMapperHelper(realizarTransferenciaPEIDTO,WSRealizarTransferenciaPEIResponse.class);

			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
				
				//intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj());
				intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj()+   (e.getMsjInterno()!=null?"->"+e.getMsjInterno():""));
				
				if (e instanceof FinancieroTandemException) {
					FinancieroTandemException tException = (FinancieroTandemException) e;
					
					if( tException.getMsjTandem()!=null)
						intenc.setObservacion(intenc.getObservacion()+"*"+ tException.getMsjTandem().trim());
				}
				this.getFinancieroService().registrarErrorTransaccion(intenc);
			}
			
			this.llenarDatosComunes(response, success);
		}
		
		LOGGER.info("*[WS FIN]* realizarPagoPEI: " + request.getHeader().getIdRequerimiento());
		return response;
	}
	
	@Override
	public WSRealizarTransferenciaPEIPagarResponse realizarPagoPEIPagar(WSRealizarTransferenciaPEIPagarRequest request) {
		LOGGER.info("*[WS INI]* realizarPagoPEIPagar: " + this.requestToString(request.getHeader()));
		
		boolean success = true;
		ValidationDTO validation = createValidationDTO(request);
		
		WSRealizarTransferenciaPEIPagarResponse response = new WSRealizarTransferenciaPEIPagarResponse();
		
		try {

			financieroValidationChain.validate(validation);
			this.validarDatoPEI(request);

		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}		
		
		if (success) {
			
			Intencion intenc= this.getFinancieroService().registrarInicioTransaccionPei(
					(Long)validation.getOutputValue(ValidationDTO.TERMINAL_SAM_ID),
					Integer.valueOf(OperationEnum.PAGO_PEI_PAGAR.getCode()),
					(String)String.valueOf(validation.getInputValue(ValidationDTO.ID_REQ)) );
			
			try {
				RealizarTransferenciaPEIPagarRequestDTO realizarTransferenciaPEIRequestDTO = popularLoQueQuedaRealizarPagoPEIPagar(request);
				
				RealizarTransferenciaPEIPagarDTO realizarTransferenciaPEIDTO = this.getFinancieroService().doTransferPEIPagar(
						validation.getInputValue(ValidationDTO.ID_REQ),
						validation.getInputValue(ValidationDTO.IP_CLIENTE), validation,
						realizarTransferenciaPEIRequestDTO,
						request.getRealizarTransferenciaPEIPagarRequest().getSecuenciaOn(),
						intenc);
				
				/*RealizarTransferenciaPEIDTO realizarTransferenciaPEIDTO = this.getFinancieroService().doTransferPEIPagar(
						validation.getInputValue(ValidationDTO.ID_REQ),
						validation.getInputValue(ValidationDTO.IP_CLIENTE), validation,
						realizarTransferenciaPEIRequestDTO,
						null,
						intenc);*/
				
				
				response = modelMapperService.modelMapperHelper(realizarTransferenciaPEIDTO,WSRealizarTransferenciaPEIPagarResponse.class);
				
				
			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
				
				//intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj());
				intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj()+   (e.getMsjInterno()!=null?"->"+e.getMsjInterno():""));
				/*
				 * ACA YA NO USA RESPUESTA DE TANDEM. SOLO RECIBE RESPUESTA DE PEI
				if (e instanceof FinancieroTandemException) {
					FinancieroTandemException tException = (FinancieroTandemException) e;
					
					
					if( tException.getMsjTandem()!=null && tException.getCodTandem()!=null)
						intenc.setObservacion(intenc.getObservacion()+"*"+ tException.getMsjTandem().trim());
					
				}
				*/
				this.getFinancieroService().registrarErrorTransaccion(intenc);
			}
			
			this.llenarDatosComunes(response, success);
			
		}
		
		LOGGER.info("*[WS FIN]* realizarPagoPEIPagar: " + request.getHeader().getIdRequerimiento());
		return response;
	}

	@Override
	public WSTestServiceResponse pruebaServicioFinanciero(WSTestServiceRequest request) {
		
		LOGGER.info("*[WS INI]* pruebaServicioFinanciero: " + this.requestToString(request.getHeader()));
		
		boolean success = true;
		ValidationDTO validation = createValidationDTO(request);
		WSTestServiceResponse response = new WSTestServiceResponse();
		
		try {
			financieroValidationChain.validate(validation);
			
			if (success) {
				Intencion intenc= this.getFinancieroService().registrarPruebaServicio(
						(Long)validation.getOutputValue(ValidationDTO.TERMINAL_SAM_ID),
						Integer.valueOf(OperationEnum.PRUEBA_SERVICIO_FINANCIERO.getCode()),
						(String)String.valueOf(validation.getInputValue(ValidationDTO.ID_REQ)) );
			
				this.llenarDatosComunes(response, success);
			}
			
		}  catch (FinancieroException e) {
			LOGGER.error("Error de validacion: (pruebaServicioFinanciero) " + e.getMsj());
			this.llenarHeaderError(e, response);
		}
		
		LOGGER.info("*[WS FIN]* pruebaServicioFinanciero: " + request.getHeader().getIdRequerimiento());
		return response;
	}

	
	@Override
	public WSTiposDocumentoResponse tipoDocumento(WSTipoDocumentoRequest request) {
		LOGGER.info("*[WS INI]* tipoDocumento: " + this.requestToString(request.getHeader()));
		
		boolean success = true;
		ValidationDTO validation = createValidationDTO(request);
		
		WSTiposDocumentoResponse response = new WSTiposDocumentoResponse();
		/* AUN FALTA VERIFICAR LOS DOS PARAMETROS NUEVOS DEL REQUEST*/
		try {

			financieroValidationChain.validate(validation);			
			
			//VER SI APLICA ESTA VALIDACION
			//this.validarDatoPEI(request);
			
		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}
		
		if (success) {
			
			Intencion intenc= this.getFinancieroService().registrarInicioTransaccionPei(
					(Long)validation.getOutputValue(ValidationDTO.TERMINAL_SAM_ID),
					Integer.valueOf(OperationEnum.PAGO_PEI_PAGAR.getCode()),
					(String)String.valueOf(validation.getInputValue(ValidationDTO.ID_REQ)) );
			
			try {
				//RealizarTransferenciaPEIPagarRequestDTO realizarTransferenciaPEIRequestDTO = popularLoQueQuedaRealizarPagoPEIPagar(request);
				
				ConsultaTipoDocumentoPEIDTO tiposDocumentos = this.getFinancieroService().consultaTipoDocumentoPEI(
						validation.getInputValue(ValidationDTO.ID_REQ),
						validation.getInputValue(ValidationDTO.IP_CLIENTE));
				
				response = modelMapperService.modelMapperHelper(tiposDocumentos,WSTiposDocumentoResponse.class);
				
			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
				
				//intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj());
				intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj()+   (e.getMsjInterno()!=null?"->"+e.getMsjInterno():""));
				
				if (e instanceof FinancieroTandemException) {
					FinancieroTandemException tException = (FinancieroTandemException) e;
					
					if( tException.getMsjTandem()!=null)
						intenc.setObservacion(intenc.getObservacion()+"*"+ tException.getMsjTandem().trim());
				}
				this.getFinancieroService().registrarErrorTransaccion(intenc);
			}
			
			this.llenarDatosComunes(response, success);
			
		}
		
		LOGGER.info("*[WS FIN]* tipoDocumento: " + request.getHeader().getIdRequerimiento());
		return response;
	}

	private RealizarTransferenciaPEIRequestDTO popularLoQueQuedaRealizarPagoPEI(WSRealizarTransferenciaPEIRequest request) throws FinancieroException {
		RealizarTransferenciaPEIRequestDTO requestDTO = new RealizarTransferenciaPEIRequestDTO();
		requestDTO.setPago(new RealizarTransferenciaPEIPagoRequestDTO());

		requestDTO.getPago().setNumero(request.getRealizarTransferenciaPEIRequest().getNumero());
		requestDTO.getPago().setCodigoSeguridad(request.getRealizarTransferenciaPEIRequest().getCodigoseguridad());
		requestDTO.getPago().setIdCanal(this.getIdCanal());
		requestDTO.getPago().setIdReferenciaOperacionComercio(request.getRealizarTransferenciaPEIRequest().getIdreferenciaoperacioncomercio());
		requestDTO.getPago().setIdReferenciaTrxComercio(request.getHeader().getIdRequerimiento().concat(
				request.getRealizarTransferenciaPEIRequest().getSecuenciaOn()));
		requestDTO.getPago().setIdTerminal(request.getHeader().getTerminal());

		Integer importe = request.getRealizarTransferenciaPEIRequest().getImporte().multiply(BigDecimal.valueOf(100)).intValue();
		requestDTO.getPago().setImporte(importe);
		if ("0".equals(request.getRealizarTransferenciaPEIRequest().getMoneda())) {
			requestDTO.getPago().setMoneda("ARS");
		} else if ("1".equals(request.getRealizarTransferenciaPEIRequest().getMoneda())) {
			requestDTO.getPago().setMoneda("USD");
		} else {
			throw new FinancieroException(FINA161.getCode(), FINA161.getMsg());
		}
		requestDTO.getPago().setPosEntryMode(request.getRealizarTransferenciaPEIRequest().getPosentrymode());
		requestDTO.getPago().setTitularDocumento(request.getRealizarTransferenciaPEIRequest().getTitulardocumento());
		
		requestDTO.getPago().setTracks(new RealizarTransferenciaPEITrackDataRequestDTO());
		requestDTO.getPago().getTracks().setTrack1(request.getRealizarTransferenciaPEIRequest().getTrack1());
		requestDTO.getPago().getTracks().setTrack2(request.getRealizarTransferenciaPEIRequest().getTrack2());		
		
		requestDTO.getPago().setConcepto(request.getRealizarTransferenciaPEIRequest().getConcepto());
		
		return requestDTO;
	}
	
	private RealizarTransferenciaPEIPagarRequestDTO popularLoQueQuedaRealizarPagoPEIPagar(WSRealizarTransferenciaPEIPagarRequest request) throws FinancieroException {		
		/*POBLAR RealizarTransferenciaPEIPagarPagoRequestDTO
		 * Para enviar a enlace pagos
		 * --getRealizarTransferenciaPEIPagarRequest ESTO ES EL REQUEST DEL METODO RealizarPagoPEIPagar
		 * */
		RealizarTransferenciaPEIPagarRequestDTO requestDTO = new RealizarTransferenciaPEIPagarRequestDTO();
		requestDTO.setPago(new RealizarTransferenciaPEIPagarPagoRequestDTO());
		/*ESTO */
		requestDTO.getPago().setDestino(new DestinoPEIPagarPagoRequestDTO());	
		//requestDTO.getPago().getDestino().setCodTerminal(request.getRealizarTransferenciaPEIPagarRequest().getDestino().getEntidad().getCodTerminal());
		//requestDTO.getPago().getDestino().setCodSucursal(request.getRealizarTransferenciaPEIPagarRequest().getDestino().getEntidad().getCodSucursal());
		
		requestDTO.getPago().getDestino().setEntidad(new EntidadPEIRequestDTO());
		requestDTO.getPago().getDestino().getEntidad().setFiidEntidad(request.getRealizarTransferenciaPEIPagarRequest().getDestino().getEntidad().getFiidEntidad());
		requestDTO.getPago().getDestino().getEntidad().setCodComercio(null);
		requestDTO.getPago().getDestino().setCuentaRecaudadora(new CuentaRecaudadoraDTO());
		requestDTO.getPago().getDestino().getCuentaRecaudadora().setTipo(request.getRealizarTransferenciaPEIPagarRequest().getDestino().getCuentaRecaudadora().getTipo());
		requestDTO.getPago().getDestino().getCuentaRecaudadora().setNumero(request.getRealizarTransferenciaPEIPagarRequest().getDestino().getCuentaRecaudadora().getNumero());
		requestDTO.getPago().getDestino().getCuentaRecaudadora().setFiid(request.getRealizarTransferenciaPEIPagarRequest().getDestino().getCuentaRecaudadora().getFiid());

		requestDTO.getPago().setTracks(new RealizarTransferenciaPEITrackDataRequestDTO());
		requestDTO.getPago().getTracks().setTrack1(request.getRealizarTransferenciaPEIPagarRequest().getTarjeta().getTrack().getTrack1());
		requestDTO.getPago().getTracks().setTrack2(request.getRealizarTransferenciaPEIPagarRequest().getTarjeta().getTrack().getTrack2());		
		requestDTO.getPago().getTracks().setTrack3(null);
		requestDTO.getPago().setNumero(request.getRealizarTransferenciaPEIPagarRequest().getTarjeta().getNumero());		
		requestDTO.getPago().setCodigoSeguridad(request.getRealizarTransferenciaPEIPagarRequest().getTarjeta().getCodSeguridad());
		requestDTO.getPago().setDocumento(new TipoDocumentoDTO());
		requestDTO.getPago().getDocumento()
			.setTipo(request.getRealizarTransferenciaPEIPagarRequest().getDocumento().getTipo());		
		requestDTO.getPago().getDocumento()
			.setNumero(request.getRealizarTransferenciaPEIPagarRequest().getDocumento().getNumero());		
		requestDTO.getPago().setIdCanal(request.getRealizarTransferenciaPEIPagarRequest().getCanal());
		requestDTO.getPago().setIdReferenciaTrx(request.getRealizarTransferenciaPEIPagarRequest().getIdReferenciaTrx());
		requestDTO.getPago().setIdReferenciaOperacion(request.getRealizarTransferenciaPEIPagarRequest().getIdReferenciaOperacion());
		
		Integer importe = request.getRealizarTransferenciaPEIPagarRequest().getImporte().multiply(BigDecimal.valueOf(100)).intValue();
		//Integer importe = request.getRealizarTransferenciaPEIPagarRequest().getImporte();		
		
		requestDTO.getPago().setImporte(importe);
		if ("0".equals(request.getRealizarTransferenciaPEIPagarRequest().getMoneda())) {
			requestDTO.getPago().setMoneda("ARS");
		} else if ("1".equals(request.getRealizarTransferenciaPEIPagarRequest().getMoneda())) {
			requestDTO.getPago().setMoneda("USD");
		} else {
			throw new FinancieroException(FINA161.getCode(), FINA161.getMsg());
		}
		requestDTO.getPago().setConcepto(request.getRealizarTransferenciaPEIPagarRequest().getConcepto());
		
		return requestDTO;
	}
	
	
	private RealizarDevolucionPagoPEIRequestDTO popularLoQueQuedaDevolucionPagoPEI(WSRealizarDevolucionPagoPEIRequest request) {
		RealizarDevolucionPagoPEIRequestDTO requestDTO = new RealizarDevolucionPagoPEIRequestDTO();
		requestDTO.setDevolucion(new RealizarDevolucionPagoDevolucionPEIRequestDTO());
		requestDTO.getDevolucion().setIdReferenciaTrxComercio(request.getRealizarDevolucionferenciaPEIRequest().getIdreferenciaoperacioncomercio()+request.getRealizarDevolucionferenciaPEIRequest().getSecuenciaOn());
		requestDTO.getDevolucion().setIdReferenciaOperacionComercio(request.getRealizarDevolucionferenciaPEIRequest().getIdreferenciaoperacioncomercio());
		requestDTO.getDevolucion().setMoneda("ARS");
		requestDTO.getDevolucion().setIdTerminal(request.getHeader().getTerminal());
		requestDTO.getDevolucion().setIdCanal(this.getIdCanal());
		requestDTO.getDevolucion().setPosEntryMode(request.getRealizarDevolucionferenciaPEIRequest().getPosentrymode());
		requestDTO.getDevolucion().setTracks(new RealizarTransferenciaPEITrackDataRequestDTO());
		requestDTO.getDevolucion().getTracks().setTrack1(request.getRealizarDevolucionferenciaPEIRequest().getTrack1());
		requestDTO.getDevolucion().getTracks().setTrack2(request.getRealizarDevolucionferenciaPEIRequest().getTrack2());
		requestDTO.getDevolucion().setNumero(request.getRealizarDevolucionferenciaPEIRequest().getNumero());
		if (request.getRealizarDevolucionferenciaPEIRequest().getTitulardocumento()!=null) {
			requestDTO.getDevolucion().setTitularDocumento(request.getRealizarDevolucionferenciaPEIRequest().getTitulardocumento());
		}
		requestDTO.getDevolucion().setCodigoSeguridad(request.getRealizarDevolucionferenciaPEIRequest().getCodigoseguridad());
		requestDTO.getDevolucion().setIdPago(request.getRealizarDevolucionferenciaPEIRequest().getIdpago());
				
		requestDTO.getDevolucion().setCuentaDestino(new CuentaDestinoDTO());
		
		requestDTO.getDevolucion().getCuentaDestino().setCbu(request.getRealizarDevolucionferenciaPEIRequest().getCuentaDestino().getCBU()==null?"":request.getRealizarDevolucionferenciaPEIRequest().getCuentaDestino().getCBU());
		requestDTO.getDevolucion().getCuentaDestino().setAliasCbu(request.getRealizarDevolucionferenciaPEIRequest().getCuentaDestino().getAliasCBU()==null?"":request.getRealizarDevolucionferenciaPEIRequest().getCuentaDestino().getAliasCBU());						
		
		return requestDTO;
	}
	
	/* AQM */
	private RealizarDevolucionPagoPEIPagarRequestDTO popularLoQueQuedaDevolucionPagoPEIPagar(
			WSRealizarDevolucionPagoPEIPagarRequest request) {
		RealizarDevolucionPagoPEIPagarRequestDTO requestDTO = new RealizarDevolucionPagoPEIPagarRequestDTO();
		requestDTO.setDevolucion(new RealizarDevolucionPagoDevolucionPEIPagarRequestDTO());

		//requestDTO.getDevolucion().setCodTerminal(request.getRealizarDevolucionferenciaPEIPagarRequest().getEntidad().getCodTerminal());
		//requestDTO.getDevolucion().setCodSucursal(request.getRealizarDevolucionferenciaPEIPagarRequest().getEntidad().getCodSucursal());
		requestDTO.getDevolucion().setEntidad(new EntidadPEIRequestDTO());
		requestDTO.getDevolucion().getEntidad().setFiidEntidad(request.getRealizarDevolucionferenciaPEIPagarRequest().getEntidad().getFiidEntidad());
		requestDTO.getDevolucion().getEntidad().setCodComercio(null);
		requestDTO.getDevolucion().setPanEntidad(request.getRealizarDevolucionferenciaPEIPagarRequest().getPanEntidad());		
		requestDTO.getDevolucion().setOperacionOrigen(new OperacionOrigenPEIRequestDTO());		
		requestDTO.getDevolucion().getOperacionOrigen().setIdPago(request.getRealizarDevolucionferenciaPEIPagarRequest().getOperacionOrigen());
		requestDTO.getDevolucion().getOperacionOrigen().setIdReferenciaTrxOrigen(request.getRealizarDevolucionferenciaPEIPagarRequest().getIdReferenciaTrxOrigen());
				
		requestDTO.getDevolucion().setTracks(new RealizarTransferenciaPEITrackDataRequestDTO());
		requestDTO.getDevolucion().getTracks().setTrack1(request.getRealizarDevolucionferenciaPEIPagarRequest().getTarjeta().getTrack().getTrack1());
		requestDTO.getDevolucion().getTracks().setTrack2(request.getRealizarDevolucionferenciaPEIPagarRequest().getTarjeta().getTrack().getTrack2());		
		requestDTO.getDevolucion().getTracks().setTrack3(request.getRealizarDevolucionferenciaPEIPagarRequest().getTarjeta().getTrack().getTrack3());		
		requestDTO.getDevolucion().setCuentaDestino(new CuentaDestinoDTO());
		requestDTO.getDevolucion().getCuentaDestino().setCbu(request.getRealizarDevolucionferenciaPEIPagarRequest().getCuentaDestino().getCbu());
		requestDTO.getDevolucion().getCuentaDestino().setAliasCbu(request.getRealizarDevolucionferenciaPEIPagarRequest().getCuentaDestino().getAliasCbu());
		
		requestDTO.getDevolucion().setDocumento(new TipoDocumentoDTO());
		requestDTO.getDevolucion().getDocumento()
			.setTipo(request.getRealizarDevolucionferenciaPEIPagarRequest().getDocumento().getTipo());
		requestDTO.getDevolucion().getDocumento()
			.setNumero(request.getRealizarDevolucionferenciaPEIPagarRequest().getDocumento().getNumero());
				
		requestDTO.getDevolucion().setCodigoSeguridad(request.getRealizarDevolucionferenciaPEIPagarRequest().getTarjeta().getCodSeguridad());
		requestDTO.getDevolucion().setNumero(request.getRealizarDevolucionferenciaPEIPagarRequest().getTarjeta().getNumero());
		requestDTO.getDevolucion().setIdReferenciaTrx(request.getRealizarDevolucionferenciaPEIPagarRequest().getIdReferenciaTrx());
		requestDTO.getDevolucion().setIdReferenciaOperacion(request.getRealizarDevolucionferenciaPEIPagarRequest().getIdReferenciaOperacion());		
		requestDTO.getDevolucion().setIdCanal(request.getRealizarDevolucionferenciaPEIPagarRequest().getCanal());
						
		return requestDTO;
	}
	
	
	private RealizarDevolucionPagoParcialPEIRequestDTO popularLoQueQuedaDevolucionParcialPagoPEI(WSRealizarDevolucionPagoParcialPEIRequest request) throws FinancieroException {
		RealizarDevolucionPagoParcialPEIRequestDTO requestDTO = new RealizarDevolucionPagoParcialPEIRequestDTO();
		requestDTO.setDevolucion(new RealizarDevolucionPagoParcialDevolucionPEIRequestDTO());
		requestDTO.getDevolucion().setIdReferenciaTrxComercio(request.getRealizarDevolucionParcialPEIRequest().getIdreferenciaoperacioncomercio()+request.getRealizarDevolucionParcialPEIRequest().getSecuenciaOn());
		requestDTO.getDevolucion().setIdReferenciaOperacionComercio(request.getRealizarDevolucionParcialPEIRequest().getIdreferenciaoperacioncomercio());
		requestDTO.getDevolucion().setMoneda("ARS");
		requestDTO.getDevolucion().setIdTerminal(request.getHeader().getTerminal());
		requestDTO.getDevolucion().setIdCanal(this.getIdCanal());
		requestDTO.getDevolucion().setPosEntryMode(request.getRealizarDevolucionParcialPEIRequest().getPosentrymode());
		requestDTO.getDevolucion().setTracks(new RealizarTransferenciaPEITrackDataRequestDTO());
		requestDTO.getDevolucion().getTracks().setTrack1(request.getRealizarDevolucionParcialPEIRequest().getTrack1());
		requestDTO.getDevolucion().getTracks().setTrack2(request.getRealizarDevolucionParcialPEIRequest().getTrack2());
		requestDTO.getDevolucion().setNumero(request.getRealizarDevolucionParcialPEIRequest().getNumero());
		if (request.getRealizarDevolucionParcialPEIRequest().getTitulardocumento()!=null) {
			requestDTO.getDevolucion().setTitularDocumento(request.getRealizarDevolucionParcialPEIRequest().getTitulardocumento());
		}
		requestDTO.getDevolucion().setCodigoSeguridad(request.getRealizarDevolucionParcialPEIRequest().getCodigoseguridad());
		requestDTO.getDevolucion().setIdPago(request.getRealizarDevolucionParcialPEIRequest().getIdpago());		
		
		Integer importe = request.getRealizarDevolucionParcialPEIRequest().getImporte().multiply(BigDecimal.valueOf(100)).intValue();
		requestDTO.getDevolucion().setImporte(importe);
		
		if ("0".equals(request.getRealizarDevolucionParcialPEIRequest().getMoneda())) {
			requestDTO.getDevolucion().setMoneda("ARS");
		} else if ("1".equals(request.getRealizarDevolucionParcialPEIRequest().getMoneda())) {
			requestDTO.getDevolucion().setMoneda("USD");
		} else {
			throw new FinancieroException(FINA161.getCode(), FINA161.getMsg());
		}
		
		requestDTO.getDevolucion().setCuentaDestino(new CuentaDestinoDTO());
		requestDTO.getDevolucion().getCuentaDestino().setCbu(request.getRealizarDevolucionParcialPEIRequest().getCuentaDestino().getCBU()==null?"":request.getRealizarDevolucionParcialPEIRequest().getCuentaDestino().getCBU());
		requestDTO.getDevolucion().getCuentaDestino().setAliasCbu(request.getRealizarDevolucionParcialPEIRequest().getCuentaDestino().getAliasCBU()==null?"":request.getRealizarDevolucionParcialPEIRequest().getCuentaDestino().getAliasCBU());						

		return requestDTO;
	}	

	/**
	 * Valida campos obligatorios en el pago PEI
	 * 
	 * @param request
	 * @throws FinancieroValidationException
	 */
	private void validarDatoPEI(WSRealizarTransferenciaPEIRequest request) throws FinancieroValidationException {
		notEmpty(request.getRealizarTransferenciaPEIRequest().getSecuenciaOn(), FINA156);
		notEmpty(request.getRealizarTransferenciaPEIRequest().getPosentrymode(), FINA154);
		//notEmpty(request.getRealizarTransferenciaPEIRequest().getTrack1(), FINA152);
		notEmpty(request.getRealizarTransferenciaPEIRequest().getTrack2(), FINA153);
		notEmpty(request.getRealizarTransferenciaPEIRequest().getNumero(), FINA157);
		notEmpty(request.getRealizarTransferenciaPEIRequest().getTitulardocumento(), FINA158);		
		if (null==request.getRealizarTransferenciaPEIRequest().getImporte()) {
			throw new FinancieroValidationException(FINA159.getCode(), FINA159.getMsg());
		}		
		if(BigDecimal.ZERO.equals(request.getRealizarTransferenciaPEIRequest().getImporte())) {
			throw new FinancieroValidationException(FINA160.getCode(), FINA160.getMsg());
		}
		notEmpty(request.getRealizarTransferenciaPEIRequest().getMoneda(), FINA161);
		notEmpty(request.getRealizarTransferenciaPEIRequest().getCodigoseguridad(), FINA162);
	}
	
	/**
	 * Valida campos obligatorios en el pago PEI
	 * 
	 * @param request
	 * @throws FinancieroValidationException
	 */
	private void validarDatoPEI(WSRealizarTransferenciaPEIPagarRequest request) throws FinancieroValidationException {
		/*notEmpty(request.getRealizarTransferenciaPEIRequest().getSecuenciaOn(), FINA156);
		notEmpty(request.getRealizarTransferenciaPEIRequest().getPosentrymode(), FINA154);
		//notEmpty(request.getRealizarTransferenciaPEIRequest().getTrack1(), FINA152);
		notEmpty(request.getRealizarTransferenciaPEIRequest().getTrack2(), FINA153);
		notEmpty(request.getRealizarTransferenciaPEIRequest().getNumero(), FINA157);
		notEmpty(request.getRealizarTransferenciaPEIRequest().getTitulardocumento(), FINA158);		
		if (null==request.getRealizarTransferenciaPEIRequest().getImporte()) {
			throw new FinancieroValidationException(FINA159.getCode(), FINA159.getMsg());
		}		
		if(BigDecimal.ZERO.equals(request.getRealizarTransferenciaPEIRequest().getImporte())) {
			throw new FinancieroValidationException(FINA160.getCode(), FINA160.getMsg());
		}
		notEmpty(request.getRealizarTransferenciaPEIRequest().getMoneda(), FINA161);
		notEmpty(request.getRealizarTransferenciaPEIRequest().getCodigoseguridad(), FINA162);*/
		
		
		
		//notEmpty(request.getRealizarTransferenciaPEIRequest().getSecuenciaOn(), FINA156);
		notEmpty(request.getRealizarTransferenciaPEIPagarRequest().getPosEntryMode(), FINA154);
		//notEmpty(request.getRealizarTransferenciaPEIRequest().getTrack1(), FINA152);
		notEmpty(request.getRealizarTransferenciaPEIPagarRequest().getTarjeta().getTrack().getTrack2(), FINA153);
		notEmpty(request.getRealizarTransferenciaPEIPagarRequest().getTarjeta().getNumero(), FINA157);
		//notEmpty(request.getRealizarTransferenciaPEIRequest().getTitulardocumento(), FINA158);		
		if (null==request.getRealizarTransferenciaPEIPagarRequest().getImporte()) {
			throw new FinancieroValidationException(FINA159.getCode(), FINA159.getMsg());
		}		
		if(BigDecimal.ZERO.equals(request.getRealizarTransferenciaPEIPagarRequest().getImporte())) {
			throw new FinancieroValidationException(FINA160.getCode(), FINA160.getMsg());
		}
		notEmpty(request.getRealizarTransferenciaPEIPagarRequest().getMoneda(), FINA161);
		notEmpty(request.getRealizarTransferenciaPEIPagarRequest().getTarjeta().getCodSeguridad(), FINA162);
		
	}
	
	/**
	 * Valida campos obligatorios en la devolucion PEI desde PAGAR
	 * 
	 * @param request
	 * @throws FinancieroValidationException
	 * AQM
	 */	
	private void validarDatoDevolucionPagoPEIPagar(WSParametrosRealizarDevolucionPEIPagarRequest request)
			throws FinancieroValidationException {
		/*AQM
		 * Puede venir uno u otro o los dos 
		 * operacionOrigen/idReferenciaTrxOrigen 
		 * */
		if(null==request.getOperacionOrigen()) {
			if (null==request.getIdReferenciaTrxOrigen()) {
				notEmpty(request.getOperacionOrigen(), FINA151);
			}
		}
		else if (null==request.getIdReferenciaTrxOrigen()){
			if (null==request.getOperacionOrigen()) {
				notEmpty(request.getIdReferenciaTrxOrigen(),FINA177);
			}
		}										
		
		notEmpty(request.getIdRequerimientoOrig(), FINA165);
						
		notEmpty(request.getDocumento().getTipo(), FINA158);		
		notEmpty(request.getDocumento().getNumero(), FINA158);
		notEmpty(request.getTarjeta().getTrack().getTrack2(), FINA153);
		
		notEmpty(request.getPosEntryMode(), FINA154);
		
		if (null == request.getImporte()) {
			throw new FinancieroValidationException(FINA159.getCode(), FINA159.getMsg());
		}

		if (BigDecimal.ZERO.equals(request.getImporte())) {
			throw new FinancieroValidationException(FINA160.getCode(), FINA160.getMsg());
		}

		notEmpty(request.getIdReferenciaOperacion(), FINA155);
		notEmpty(request.getIdReferenciaTrx(), FINA155);
		notEmpty(request.getSecuenciaOn(), FINA156);
	}
	
	
	/**
	 * Valida campos obligatorios en la devolucion PEI
	 * 
	 * @param request
	 * @throws FinancieroValidationException
	 */
	private void validarDatoDevolucionPagoPEI(WSParametrosRealizarDevolucionPEIRequest request) throws FinancieroValidationException {
		notEmpty(request.getIdRequerimientoOrig(), FINA165);
		notEmpty(request.getIdpago(), FINA151);
		notEmpty(request.getTitulardocumento(), FINA158);	
		
		notEmpty(request.getTrack2(), FINA153);
		notEmpty(request.getPosentrymode(), FINA154);		
		if(null==request.getImporte()) {
			throw new FinancieroValidationException(FINA159.getCode(), FINA159.getMsg());
		}
		if(BigDecimal.ZERO.equals(request.getImporte())) {
			throw new FinancieroValidationException(FINA160.getCode(), FINA160.getMsg());
		}
		notEmpty(request.getIdreferenciaoperacioncomercio(), FINA155);
		notEmpty(request.getSecuenciaOn(), FINA156);	
	}	
	
	private static void notEmpty(String value, FinancieroServicesMsgEnum error) throws FinancieroValidationException {
		try {
			Validate.notEmpty(value);
		} catch (Exception e) {
			throw new FinancieroValidationException(error.getCode(), error.getMsg());
		}
	}

	@Override
	public WSRealizarDevolucionPEIResponse realizarDevolucionPagoPEI(WSRealizarDevolucionPagoPEIRequest request) {
		LOGGER.info("*[WS INI]* realizarDevolucionPagoPEI: " + this.requestToString(request.getHeader()));
		boolean success = true;
		ValidationDTO validation = createValidationDTO(request);

		WSRealizarDevolucionPEIResponse response = new WSRealizarDevolucionPEIResponse();

		try {
			financieroValidationChain.validate(validation);
			this.validarDatoDevolucionPagoPEI(request.getRealizarDevolucionferenciaPEIRequest());
		
		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}
		
		if (success) {		
			
			Intencion intenc= this.getFinancieroService().registrarInicioTransaccionPei(
					(Long)validation.getOutputValue(ValidationDTO.TERMINAL_SAM_ID),
					Integer.valueOf(OperationEnum.DEVOLUCION_PAGO_PEI.getCode()),
					(String)String.valueOf(validation.getInputValue(ValidationDTO.ID_REQ)) );
			
			try {
				RealizarDevolucionPagoPEIRequestDTO requestDTO = popularLoQueQuedaDevolucionPagoPEI(request);
					
				RealizarDevolucionResultadoPEIDTO responseDTO = this.getFinancieroService().doReturnPEI(
																			request.getHeader().getIdRequerimiento(), 
																			request.getHeader().getIpCliente(), 
																			validation, 
																			requestDTO,
																			request.getRealizarDevolucionferenciaPEIRequest().getSecuenciaOn(),
																			request.getRealizarDevolucionferenciaPEIRequest().getIdRequerimientoOrig(),
																			request.getRealizarDevolucionferenciaPEIRequest().getImporte(),
																			intenc);
				
				RealizarDevolucionPEIDTO realizarDevolucionPEIDTO = new RealizarDevolucionPEIDTO();
				realizarDevolucionPEIDTO.setResultado(responseDTO);
				realizarDevolucionPEIDTO.setSecuenciaOn(responseDTO.getSecuenciaOn());
				response = modelMapperService.modelMapperHelper(realizarDevolucionPEIDTO, WSRealizarDevolucionPEIResponse.class);				
							
			} catch (FinancieroException e) {
				LOGGER.error("Error de validacion: " + e.getMsj());
				success = false;
				this.llenarHeaderError(e, response);
				
				//intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj());
				intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj()+   (e.getMsjInterno()!=null?"->"+e.getMsjInterno():""));
				
				if (e instanceof FinancieroTandemException) {
					FinancieroTandemException tException = (FinancieroTandemException) e;
					
					if( tException.getMsjTandem()!=null)
						intenc.setObservacion(intenc.getObservacion()+"*"+ tException.getMsjTandem().trim());
				}
				this.getFinancieroService().registrarErrorTransaccion(intenc);
			}
			this.llenarDatosComunes(response, success);
		}
		
		LOGGER.info("*[WS FIN]* realizarDevolucionPagoPEI: " + request.getHeader().getIdRequerimiento());
		return response;
	}
	
	@Override
	public WSRealizarDevolucionPEIResponse realizarDevolucionPagoParcialPEI(WSRealizarDevolucionPagoParcialPEIRequest request) {
		LOGGER.info("[WS INI] realizarDevolucionPagoParcialPEI: " + this.requestToString(request.getHeader()));
		boolean success = true;
		ValidationDTO validation = createValidationDTO(request);

		WSRealizarDevolucionPEIResponse response = new WSRealizarDevolucionPEIResponse();

		try {
			financieroValidationChain.validate(validation);
			this.validarDatoDevolucionPagoPEI(request.getRealizarDevolucionParcialPEIRequest());
		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}
		
		
		if (success) {		
			
			Intencion intenc= this.getFinancieroService().registrarInicioTransaccionPei(
					(Long)validation.getOutputValue(ValidationDTO.TERMINAL_SAM_ID),
					Integer.valueOf(OperationEnum.DEVOLUCION_PARCIAL_PAGO_PEI.getCode()),
					(String)String.valueOf(validation.getInputValue(ValidationDTO.ID_REQ)) );
			
			try {		
				RealizarDevolucionPagoParcialPEIRequestDTO requestDTO = popularLoQueQuedaDevolucionParcialPagoPEI(request);
				
				RealizarDevolucionResultadoPEIDTO responseDTO = this.getFinancieroService().doReturnParcialPEI(
																		request.getHeader().getIdRequerimiento(), 
																		request.getHeader().getIpCliente(), 
																		validation, 
																		requestDTO,
																		request.getRealizarDevolucionParcialPEIRequest().getSecuenciaOn(),
																		request.getRealizarDevolucionParcialPEIRequest().getIdRequerimientoOrig(),
																		request.getRealizarDevolucionParcialPEIRequest().getImporte(),
																		intenc);
				
				RealizarDevolucionPEIDTO realizarDevolucionPEIDTO = new RealizarDevolucionPEIDTO();
				
				realizarDevolucionPEIDTO.setResultado(responseDTO);
				realizarDevolucionPEIDTO.setSecuenciaOn(responseDTO.getSecuenciaOn());
				
				response = modelMapperService.modelMapperHelper(realizarDevolucionPEIDTO, WSRealizarDevolucionPEIResponse.class);
				
			} catch (FinancieroException e) {
				LOGGER.error("Error de validacion: " + e.getMsj());
				success = false;
				this.llenarHeaderError(e, response);
				
				//intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj());
				intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj()+   (e.getMsjInterno()!=null?"->"+e.getMsjInterno():""));
				
				if (e instanceof FinancieroTandemException) {
					FinancieroTandemException tException = (FinancieroTandemException) e;
					
					if( tException.getMsjTandem()!=null)
						intenc.setObservacion(intenc.getObservacion()+"*"+ tException.getMsjTandem().trim());
				}
				this.getFinancieroService().registrarErrorTransaccion(intenc);
			}
			this.llenarDatosComunes(response, success);
		}

		LOGGER.info("[WS FIN] realizarDevolucionPagoParcialPEI: " + request.getHeader().getIdRequerimiento());
		return response;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.ws.service.WSFinancieroService#realizarExtraccionPrestamo(ar.com.redlink.sam.financieroservices.ws.request.WSRealizarExtraccionPrestamoRequest)
	 */
	@Override
	public WSRealizarExtraccionPrestamoResponse realizarExtraccionPrestamo(WSRealizarExtraccionPrestamoRequest request) {
		LOGGER.info("[WS INI] realizarExtraccionPrestamo: " + this.requestToString(request.getHeader()));
		//LOGGER.debug("Parametros recibidos: " + this.requestToString(request.getHeader()));

		boolean success = true;
		ValidationDTO validation = createValidationDTO(request);

		WSRealizarExtraccionPrestamoResponse response = new WSRealizarExtraccionPrestamoResponse();

		try {
			financieroValidationChain.validate(validation);
		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}

		if (success) {
			RealizarExtraccionPrestamoRequestDTO realizarExtraccionPrestamoRequestDTO = modelMapperService
					.modelMapperHelper(request.getRealizarExtraccionPrestamoRequest(), RealizarExtraccionPrestamoRequestDTO.class);

			this.llenarResultadosValidation(realizarExtraccionPrestamoRequestDTO, validation);

			try {
				ExtraccionPrestamoDTO realizarExtraccionPrestamoDTO = this.getFinancieroService().doLoanWithdraw(realizarExtraccionPrestamoRequestDTO);
				response = modelMapperService.modelMapperHelper(realizarExtraccionPrestamoDTO, WSRealizarExtraccionPrestamoResponse.class);

			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
			}
		}

		this.llenarDatosComunes(response, success);

		LOGGER.info("[WS FIN] realizarExtraccionPrestamo: " + request.getHeader().getIdRequerimiento());
		return response;
	}

	/**
	 * @see ar.com.redlink.sam.financieroservices.ws.service.WSFinancieroService#anularExtraccionPrestamo(ar.com.redlink.sam.financieroservices.ws.request.WSAnularExtraccionPrestamoRequest)
	 */
	@Override
	public WSAnularExtraccionPrestamoResponse anularExtraccionPrestamo(WSAnularExtraccionPrestamoRequest request) {
		LOGGER.info("[WS INI] anularExtraccionPrestamo: " + this.requestToString(request.getHeader()));

		boolean success = true;
		ValidationDTO validation = createValidationDTO(request);
		WSAnularExtraccionPrestamoResponse response = new WSAnularExtraccionPrestamoResponse();

		try {
			financieroValidationChain.validate(validation);
		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}

		if (success) {
			AnularExtraccionPrestamoRequestDTO anularExtraccionPrestamoRequestDTO = modelMapperService
					.modelMapperHelper(request.getAnularExtraccionPrestamoRequest(), AnularExtraccionPrestamoRequestDTO.class);

			this.llenarResultadosValidation(anularExtraccionPrestamoRequestDTO, validation);
			AnularExtraccionPrestamoDTO anularExtraccionPrestamoDTO;

			try {
				anularExtraccionPrestamoDTO = this.getFinancieroService().rollbackLoanWithdraw(anularExtraccionPrestamoRequestDTO);
				response = modelMapperService.modelMapperHelper(anularExtraccionPrestamoDTO,WSAnularExtraccionPrestamoResponse.class);
			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
			}
			this.llenarDatosComunes(response, success);
		}

		LOGGER.info("[WS FIN] anularExtraccionPrestamo: " + request.getHeader().getIdRequerimiento());
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.ws.service.WSFinancieroService#realizarExtraccionCuotas(ar.com.redlink.sam.financieroservices.ws.request.WSRealizarExtraccionCuotasRequest)
	 */
	@Override
	public WSRealizarExtraccionCuotasResponse realizarExtraccionCuotas(WSRealizarExtraccionCuotasRequest request) {
		LOGGER.info("[WS INI] realizarExtraccionCuotas: " + this.requestToString(request.getHeader()));

		boolean success = true;
		ValidationDTO validation = createValidationDTO(request);

		WSRealizarExtraccionCuotasResponse response = new WSRealizarExtraccionCuotasResponse();

		try {
			financieroValidationChain.validate(validation);
		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}

		if (success) {
			RealizarExtraccionCuotasRequestDTO realizarExtraccionCuotasRequestDTO = modelMapperService
					.modelMapperHelper(request.getRealizarExtraccionCuotasRequest(),RealizarExtraccionCuotasRequestDTO.class);

			this.llenarResultadosValidation(realizarExtraccionCuotasRequestDTO, validation);
			ExtraccionCuotasDTO realizarExtraccionCuotasDTO;

			try {
				realizarExtraccionCuotasDTO = this.getFinancieroService().doSharesWithdraw(realizarExtraccionCuotasRequestDTO);

				response = modelMapperService.modelMapperHelper(realizarExtraccionCuotasDTO,WSRealizarExtraccionCuotasResponse.class);
			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
			}

			this.llenarDatosComunes(response, success);
		}

		LOGGER.info("[WS FIN] realizarExtraccionCuotas: " + request.getHeader().getIdRequerimiento());
		return response;
	}

	/**
	 * 
	 * @see ar.com.redlink.sam.financieroservices.ws.service.WSFinancieroService#anularExtraccionCuotas(ar.com.redlink.sam.financieroservices.ws.request.WSAnularExtraccionCuotasRequest)
	 */
	@Override
	public WSAnularExtraccionCuotasResponse anularExtraccionCuotas(WSAnularExtraccionCuotasRequest request) {
		LOGGER.debug("[WS INI] anularExtraccionCuotas: " + this.requestToString(request.getHeader()));

		boolean success = true;
		ValidationDTO validation = createValidationDTO(request);

		WSAnularExtraccionCuotasResponse response = new WSAnularExtraccionCuotasResponse();

		try {
			financieroValidationChain.validate(validation);
		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}

		if (success) {
			AnularExtraccionCuotasRequestDTO anularExtraccionPrestamoRequestDTO = modelMapperService.modelMapperHelper(
					request.getAnularExtraccionCuotasRequest(), AnularExtraccionCuotasRequestDTO.class);

			this.llenarResultadosValidation(anularExtraccionPrestamoRequestDTO, validation);
			AnularExtraccionCuotasDTO anularExtraccionCuotasDTO;

			try {
				anularExtraccionCuotasDTO = this.getFinancieroService().rollbackSharesWithdraw(anularExtraccionPrestamoRequestDTO);
				response = modelMapperService.modelMapperHelper(anularExtraccionCuotasDTO, WSAnularExtraccionCuotasResponse.class);
			} catch (FinancieroException e) {
				success = false;
				this.llenarHeaderError(e, response);
			}

			this.llenarDatosComunes(response, success);
		}

		LOGGER.info("[WS FIN] anularExtraccionCuotas: " + request.getHeader().getIdRequerimiento());
		return response;
	}

	/* AQM */
	@Override
	public WSRealizarDevolucionPEIPagarResponse realizarDevolucionPagoPEIPagar(	WSRealizarDevolucionPagoPEIPagarRequest request) {
		LOGGER.info("*[WS INI]* realizarDevolucionPagoPEIPagar: " + this.requestToString(request.getHeader()));
		boolean success = true;
		ValidationDTO validation = createValidationDTO(request);

		WSRealizarDevolucionPEIPagarResponse response = new WSRealizarDevolucionPEIPagarResponse();

		try {
			financieroValidationChain.validate(validation);
			this.validarDatoDevolucionPagoPEIPagar(request.getRealizarDevolucionferenciaPEIPagarRequest());

		} catch (FinancieroException e) {
			LOGGER.error("Error de validacion: " + e.getMsj());
			success = false;
			this.llenarHeaderError(e, response);
		}

		if (success) {

			Intencion intenc = this.getFinancieroService().registrarInicioTransaccionPei(
					(Long) validation.getOutputValue(ValidationDTO.TERMINAL_SAM_ID),
					Integer.valueOf(OperationEnum.DEVOLUCION_PAGO_PEI_PAGAR.getCode()),
					(String) String.valueOf(validation.getInputValue(ValidationDTO.ID_REQ)));

			try {
				RealizarDevolucionPagoPEIPagarRequestDTO requestDTO = popularLoQueQuedaDevolucionPagoPEIPagar(request);

				RealizarDevolucionResultadoPEIPagarDTO responseDTO = this.getFinancieroService().doReturnPEIPagar(
						request.getHeader().getIdRequerimiento(), request.getHeader().getIpCliente(), validation,
						requestDTO, request.getRealizarDevolucionferenciaPEIPagarRequest().getSecuenciaOn(),
						request.getRealizarDevolucionferenciaPEIPagarRequest().getIdRequerimientoOrig(),												
						request.getRealizarDevolucionferenciaPEIPagarRequest().getImporte(), intenc);

				RealizarDevolucionPEIPagarDTO realizarDevolucionPEIPagarDTO = new RealizarDevolucionPEIPagarDTO();
				realizarDevolucionPEIPagarDTO.setResultado(responseDTO);
				realizarDevolucionPEIPagarDTO.setSecuenciaOn(responseDTO.getSecuenciaOn());
				response = modelMapperService.modelMapperHelper(realizarDevolucionPEIPagarDTO,	WSRealizarDevolucionPEIPagarResponse.class);

			} catch (FinancieroException e) {
				LOGGER.error("Error de validacion: " + e.getMsj());
				success = false;
				this.llenarHeaderError(e, response);

				// intenc.setObservacion(intenc.getObservacion()+"*"+e.getCod()+"*"+e.getMsj());
				intenc.setObservacion(intenc.getObservacion() + "*" + e.getCod() + "*" + e.getMsj()
						+ (e.getMsjInterno() != null ? "->" + e.getMsjInterno() : ""));

				if (e instanceof FinancieroTandemException) {
					FinancieroTandemException tException = (FinancieroTandemException) e;

					if (tException.getMsjTandem() != null)
						intenc.setObservacion(intenc.getObservacion() + "*" + tException.getMsjTandem().trim());
				}
				this.getFinancieroService().registrarErrorTransaccion(intenc);
			}
			this.llenarDatosComunes(response, success);
		}

		LOGGER.info("*[WS FIN]* realizarDevolucionPagoPEIPagar: " + request.getHeader().getIdRequerimiento());
		return response;
	}
	
	
	/**
	 * Retorna los datos del request, para loguear.
	 * 
	 * @param header
	 * @return
	 */
	private String requestToString(WSHeaderRequest header) {
		return header.getEntidad() + ", " + header.getIdentDispositivo() + ", " + header.getIdentTerminal() + ", "
				+ header.getIdRequerimiento() + ", " + header.getIpCliente() + ", " + header.getSucursal() + ", "
				+ header.getTerminal() + ", " + header.getTimeStamp();
	}

	/**
	 * @return the financieroService
	 */
	public FinancieroService getFinancieroService() {
		return financieroService;
	}

	/**
	 * @param financieroService
	 *            the financieroService to set
	 */
	public void setFinancieroService(FinancieroService financieroService) {
		this.financieroService = financieroService;
	}

	/**
	 * @return the modelMapperService
	 */
	public ModelMapperService getModelMapperService() {
		return modelMapperService;
	}

	/**
	 * @param modelMapperService
	 *            the modelMapperService to set
	 */
	public void setModelMapperService(ModelMapperService modelMapperService) {
		this.modelMapperService = modelMapperService;
	}

	/**
	 * @return the financieroValidationChain
	 */
	public ValidationChain getFinancieroValidationChain() {
		return financieroValidationChain;
	}

	/**
	 * @param financieroValidationChain
	 *            the financieroValidationChain to set
	 */
	public void setFinancieroValidationChain(ValidationChain financieroValidationChain) {
		this.financieroValidationChain = financieroValidationChain;
	}

	/**
	 * @return the idCanal
	 */
	public String getIdCanal() {
		return idCanal;
	}

	/**
	 * @param idCanal
	 *            the idCanal to set
	 */
	public void setIdCanal(String idCanal) {
		this.idCanal = idCanal;
	}

}
