package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.math.BigDecimal;

/**
 * Representacion de la relacion Grupo Prefijo - Operacion - Regla.
 * 
 * @author aguerrea
 */
public class GrupoPrefijoOpRegla extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -838711581090724112L;
	private long grupoPrefijoOpReglaId;
	private TipoPrefijoRegla tipoPrefijoRegla;
	private GrupoPrefijoOp grupoPrefijoOp;
	private EstadoRegistro estadoRegistro;
	private BigDecimal valor;

	/**
	 * @return the grupoPrefijoOpReglaId
	 */
	public long getGrupoPrefijoOpReglaId() {
		return grupoPrefijoOpReglaId;
	}

	/**
	 * @param grupoPrefijoOpReglaId
	 *            the grupoPrefijoOpReglaId to set
	 */
	public void setGrupoPrefijoOpReglaId(long grupoPrefijoOpReglaId) {
		this.grupoPrefijoOpReglaId = grupoPrefijoOpReglaId;
	}

	/**
	 * @return the tipoPrefijoRegla
	 */
	public TipoPrefijoRegla getTipoPrefijoRegla() {
		return tipoPrefijoRegla;
	}

	/**
	 * @param tipoPrefijoRegla
	 *            the tipoPrefijoRegla to set
	 */
	public void setTipoPrefijoRegla(TipoPrefijoRegla tipoPrefijoRegla) {
		this.tipoPrefijoRegla = tipoPrefijoRegla;
	}

	/**
	 * @return the grupoPrefijoOp
	 */
	public GrupoPrefijoOp getGrupoPrefijoOp() {
		return grupoPrefijoOp;
	}

	/**
	 * @param grupoPrefijoOp
	 *            the grupoPrefijoOp to set
	 */
	public void setGrupoPrefijoOp(GrupoPrefijoOp grupoPrefijoOp) {
		this.grupoPrefijoOp = grupoPrefijoOp;
	}

	/**
	 * @return the estadoRegistro
	 */
	public EstadoRegistro getEstadoRegistro() {
		return estadoRegistro;
	}

	/**
	 * @param estadoRegistro
	 *            the estadoRegistro to set
	 */
	public void setEstadoRegistro(EstadoRegistro estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	/**
	 * @return the valor
	 */
	public BigDecimal getValor() {
		return valor;
	}

	/**
	 * @param valor
	 *            the valor to set
	 */
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
}
