package ar.com.redlink.sam.financieroservices.entity;

public class AsociacionPerfilDetalle extends BaseEntity {

	private static final long serialVersionUID = 8887832998270238964L;
	private long asociacionPerfilDetalleId;
	private AsociacionPerfil asociacionperfil;
	private PerfilSam perfilSam;
	
	/**
	 * @return the asociacionPerfilDetalleId
	 */
	public long getAsociacionPerfilDetalleId() {
		return asociacionPerfilDetalleId;
	}
	/**
	 * @param asociacionPerfilDetalleId the asociacionPerfilDetalleId to set
	 */
	public void setAsociacionPerfilDetalleId(long asociacionPerfilDetalleId) {
		this.asociacionPerfilDetalleId = asociacionPerfilDetalleId;
	}
	/**
	 * @return the asociacionperfil
	 */
	public AsociacionPerfil getAsociacionperfil() {
		return asociacionperfil;
	}
	/**
	 * @param asociacionperfil the asociacionperfil to set
	 */
	public void setAsociacionperfil(AsociacionPerfil asociacionperfil) {
		this.asociacionperfil = asociacionperfil;
	}
	/**
	 * @return the perfilSam
	 */
	public PerfilSam getPerfilSam() {
		return perfilSam;
	}
	/**
	 * @param perfilSam the perfilSam to set
	 */
	public void setPerfilSam(PerfilSam perfilSam) {
		this.perfilSam = perfilSam;
	}
	


}
