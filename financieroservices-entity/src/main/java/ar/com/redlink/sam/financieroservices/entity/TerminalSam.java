package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Representacion de una terminal SAM.
 * 
 * @author aguerrea
 */
public class TerminalSam extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7572182461615579298L;
	private long terminalSamId;
	private Servidor servidor;
	private DispositivoPos dispositivoPos;
	private Entidad entidad;
	private EstadoRegistro estadoRegistro;
	private String codTerminal;
	private String terminalB24;
	private Date fechaUltimaRecepcion;
	private short tipoTerminal;
	private TipoTerminalSam tipoTerminalSam;
	private Set<EnrolamientoSam> enrolamientosSam = new HashSet<EnrolamientoSam>(0);
	private String tipoTerminalB24; 
	private Long productoId;
	private Long servidorId;
	
	public TerminalSam(long terminalSamId) {
		this.terminalSamId = terminalSamId;
	}

	public TerminalSam() {
	}

	/**
	 * @return the terminalSamId
	 */
	public long getTerminalSamId() {
		return terminalSamId;
	}

	/**
	 * @param terminalSamId
	 *            the terminalSamId to set
	 */
	public void setTerminalSamId(long terminalSamId) {
		this.terminalSamId = terminalSamId;
	}

	/**
	 * @return the servidor
	 */
	public Servidor getServidor() {
		return servidor;
	}

	/**
	 * @param servidor
	 *            the servidor to set
	 */
	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}

	/**
	 * @return the dispositivoPos
	 */
	public DispositivoPos getDispositivoPos() {
		return dispositivoPos;
	}

	/**
	 * @param dispositivoPos
	 *            the dispositivoPos to set
	 */
	public void setDispositivoPos(DispositivoPos dispositivoPos) {
		this.dispositivoPos = dispositivoPos;
	}

	/**
	 * @return the entidad
	 */
	public Entidad getEntidad() {
		return entidad;
	}

	/**
	 * @param entidad
	 *            the entidad to set
	 */
	public void setEntidad(Entidad entidad) {
		this.entidad = entidad;
	}

	/**
	 * @return the estadoRegistro
	 */
	public EstadoRegistro getEstadoRegistro() {
		return estadoRegistro;
	}

	/**
	 * @param estadoRegistro
	 *            the estadoRegistro to set
	 */
	public void setEstadoRegistro(EstadoRegistro estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	/**
	 * @return the codTerminal
	 */
	public String getCodTerminal() {
		return codTerminal;
	}

	/**
	 * @param codTerminal
	 *            the codTerminal to set
	 */
	public void setCodTerminal(String codTerminal) {
		this.codTerminal = codTerminal;
	}

	/**
	 * @return the terminalB24
	 */
	public String getTerminalB24() {
		return terminalB24;
	}

	/**
	 * @param terminalB24
	 *            the terminalB24 to set
	 */
	public void setTerminalB24(String terminalB24) {
		this.terminalB24 = terminalB24;
	}

	/**
	 * @return the fechaUltimaRecepcion
	 */
	public Date getFechaUltimaRecepcion() {
		return fechaUltimaRecepcion;
	}

	/**
	 * @param fechaUltimaRecepcion
	 *            the fechaUltimaRecepcion to set
	 */
	public void setFechaUltimaRecepcion(Date fechaUltimaRecepcion) {
		this.fechaUltimaRecepcion = fechaUltimaRecepcion;
	}

	/**
	 * @return the tipoTerminal
	 */
	public short getTipoTerminal() {
		return tipoTerminal;
	}

	/**
	 * @param tipoTerminal
	 *            the tipoTerminal to set
	 */
	public void setTipoTerminal(short tipoTerminal) {
		this.tipoTerminal = tipoTerminal;
	}

	/**
	 * @return the enrolamientoSams
	 */
	public Set<EnrolamientoSam> getEnrolamientosSam() {
		return enrolamientosSam;
	}

	/**
	 * @param enrolamientoSams
	 *            the enrolamientoSams to set
	 */
	public void setEnrolamientosSam(Set<EnrolamientoSam> enrolamientosSam) {
		this.enrolamientosSam = enrolamientosSam;
	}

	/**
	 * @return the tipoTerminalSam
	 */
	public TipoTerminalSam getTipoTerminalSam() {
		return tipoTerminalSam;
	}

	/**
	 * @param tipoTerminalSam
	 *            the tipoTerminalSam to set
	 */
	public void setTipoTerminalSam(TipoTerminalSam tipoTerminalSam) {
		this.tipoTerminalSam = tipoTerminalSam;
	}

	public String getTipoTerminalB24() {
		return tipoTerminalB24;
	}

	public void setTipoTerminalB24(String tipoTerminalB24) {
		this.tipoTerminalB24 = tipoTerminalB24;
	}

	public Long getProductoId() {
		return productoId;
	}

	public void setProductoId(Long productoId) {
		this.productoId = productoId;
	}

	/**
	 * @return the servidorId
	 */
	public Long getServidorId() {
		return servidorId;
	}

	/**
	 * @param servidorId the servidorId to set
	 */
	public void setServidorId(Long servidorId) {
		this.servidorId = servidorId;
	}


}
