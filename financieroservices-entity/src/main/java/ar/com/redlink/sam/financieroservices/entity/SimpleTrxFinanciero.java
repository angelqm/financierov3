/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  07/04/2015 - 21:38:09
 */

package ar.com.redlink.sam.financieroservices.entity;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * DTO para los campos requeridos para persistir un registro en TRX_Financiero.
 * Los datos que acarrea son principalmente IDs, pues el insert se hace
 * explicitamente, sin recurrir a Hibernate.
 * 
 * @author aguerrea
 * 
 */
public class SimpleTrxFinanciero implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1640834641888397982L;

	private Long trxFinancieroId;
	
	private String idEntidad;

	private String agenteId;

	private String sucursal;

	private String servidorId;

	private String redId;

	private String terminalSamId;

	private String prefijoOpId;

	private String tokenizado;

	private String estadoTrxId;

	private String importe;

	private String secuenciaOnWS;

	private String secuenciaOnB24;

	private String idRequerimiento;

	private String fechaOn;
	
	private String horaOn;
	
	private String idRequerimientoBaja;
	
	private Long operacionPei;
	

	/**
	 * Constructor default, inicializa algunos valores que pueden llegar a ser
	 * nulos.
	 * 
	 */
	public SimpleTrxFinanciero() {
		this.tokenizado = "?";
		this.importe = "0.00";
	}

	/**
	 * @return the agenteId
	 */
	public String getAgenteId() {
		return agenteId;
	}

	/**
	 * @param agenteId
	 *            the agenteId to set
	 */
	public void setAgenteId(String agenteId) {
		this.agenteId = agenteId;
	}

	/**
	 * @return the servidorId
	 */
	public String getServidorId() {
		return servidorId;
	}

	/**
	 * @param servidorId
	 *            the servidorId to set
	 */
	public void setServidorId(String servidorId) {
		this.servidorId = servidorId;
	}

	/**
	 * @return the terminalSamId
	 */
	public String getTerminalSamId() {
		return terminalSamId;
	}

	/**
	 * @param terminalSamId
	 *            the terminalSamId to set
	 */
	public void setTerminalSamId(String terminalSamId) {
		this.terminalSamId = terminalSamId;
	}

	/**
	 * @return the prefijoOpId
	 */
	public String getPrefijoOpId() {
		return prefijoOpId;
	}

	/**
	 * @param prefijoOpId
	 *            the prefijoOpId to set
	 */
	public void setPrefijoOpId(String prefijoOpId) {
		this.prefijoOpId = prefijoOpId;
	}

	/**
	 * @return the tokenizado
	 */
	public String getTokenizado() {
		return tokenizado;
	}

	/**
	 * @param tokenizado
	 *            the tokenizado to set
	 */
	public void setTokenizado(String tokenizado) {
		this.tokenizado = tokenizado;
	}

	/**
	 * @return the estadoTrxId
	 */
	public String getEstadoTrxId() {
		return estadoTrxId;
	}

	/**
	 * @param estadoTrxId
	 *            the estadoTrxId to set
	 */
	public void setEstadoTrxId(String estadoTrxId) {
		this.estadoTrxId = estadoTrxId;
	}

	/**
	 * @return the importe
	 */
	public String getImporte() {
		return importe;
	}

	/**
	 * @param importe
	 *            the importe to set
	 */
	public void setImporte(String importe) {
		this.importe = importe;
	}

	/**
	 * @return the redId
	 */
	public String getRedId() {
		return redId;
	}

	/**
	 * @param redId
	 *            the redId to set
	 */
	public void setRedId(String redId) {
		this.redId = redId;
	}

	/**
	 * @return the secuenciaOnWS
	 */
	public String getSecuenciaOnWS() {
		return secuenciaOnWS;
	}

	/**
	 * @param secuenciaOnWS
	 *            the secuenciaOnWS to set
	 */
	public void setSecuenciaOnWS(String secuenciaOnWS) {
		this.secuenciaOnWS = secuenciaOnWS;
	}

	/**
	 * @return the secuenciaOnB24
	 */
	public String getSecuenciaOnB24() {
		return secuenciaOnB24;
	}

	/**
	 * @param secuenciaOnB24
	 *            the secuenciaOnB24 to set
	 */
	public void setSecuenciaOnB24(String secuenciaOnB24) {
		this.secuenciaOnB24 = secuenciaOnB24;
	}

	/**
	 * @return the idRequerimiento
	 */
	public String getIdRequerimiento() {
		return idRequerimiento;
	}

	/**
	 * @param idRequerimiento
	 *            the idRequerimiento to set
	 */
	public void setIdRequerimiento(String idRequerimiento) {
		this.idRequerimiento = idRequerimiento;
	}

	/**
	 * @return the idEntidad
	 */
	public String getIdEntidad() {
		return idEntidad;
	}

	/**
	 * @param idEntidad
	 *            the idEntidad to set
	 */
	public void setIdEntidad(String idEntidad) {
		this.idEntidad = idEntidad;
	}

	/**
	 * @return the sucursal
	 */
	public String getSucursal() {
		return sucursal;
	}

	/**
	 * @param sucursal
	 *            the sucursal to set
	 */
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	/**
	 * @return the fechaOn
	 */
	public String getFechaOn() {
		return fechaOn;
	}

	/**
	 * @param fechaOn
	 *            the fechaOn to set
	 */
	public void setFechaOn(String fechaOn) {
		this.fechaOn = fechaOn;
	}

	/**
	 * @return the horaOn
	 */
	public String getHoraOn() {
		return horaOn;
	}

	/**
	 * @param horaOn the horaOn to set
	 */
	public void setHoraOn(String horaOn) {
		this.horaOn = horaOn;
	}

	/**
	 * @return the idRequerimientoBaja
	 */
	public String getIdRequerimientoBaja() {
		return idRequerimientoBaja;
	}

	/**
	 * @param idRequerimientoBaja the idRequerimientoBaja to set
	 */
	public void setIdRequerimientoBaja(String idRequerimientoBaja) {
		this.idRequerimientoBaja = idRequerimientoBaja;
	}

	/**
	 * @return the operacionId
	 */
	public Long getOperacionPei() {
		return operacionPei;
	}

	/**
	 * @param operacionId the operacionId to set
	 */
	public void setOperacionPei(Long operacionPei) {
		this.operacionPei = operacionPei;
	}

	public Long getTrxFinancieroId() {
		return trxFinancieroId;
	}

	public void setTrxFinancieroId(Long trxFinancieroId) {
		this.trxFinancieroId = trxFinancieroId;
	}
	
	

}
