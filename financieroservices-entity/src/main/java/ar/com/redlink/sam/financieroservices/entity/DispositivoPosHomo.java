package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.HashSet;
import java.util.Set;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representacion de un dispositivo POS homologado.
 * 
 * @author aguerrea
 */
public class DispositivoPosHomo implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2990775409324579841L;
	private long dispositivoPosHomoId;
	private EstadoRegistro estadoRegistro;
	private String marca;
	private String modelo;
	private Set<DispositivoPos> dispositivosPos = new HashSet<DispositivoPos>(0);

	/**
	 * @return the dispositivoPosHomoId
	 */
	public long getDispositivoPosHomoId() {
		return dispositivoPosHomoId;
	}

	/**
	 * @param dispositivoPosHomoId
	 *            the dispositivoPosHomoId to set
	 */
	public void setDispositivoPosHomoId(long dispositivoPosHomoId) {
		this.dispositivoPosHomoId = dispositivoPosHomoId;
	}

	/**
	 * @return the estadoRegistro
	 */
	public EstadoRegistro getEstadoRegistro() {
		return estadoRegistro;
	}

	/**
	 * @param estadoRegistro
	 *            the estadoRegistro to set
	 */
	public void setEstadoRegistro(EstadoRegistro estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	/**
	 * @return the marca
	 */
	public String getMarca() {
		return marca;
	}

	/**
	 * @param marca
	 *            the marca to set
	 */
	public void setMarca(String marca) {
		this.marca = marca;
	}

	/**
	 * @return the modelo
	 */
	public String getModelo() {
		return modelo;
	}

	/**
	 * @param modelo
	 *            the modelo to set
	 */
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	/**
	 * @return the dispositivosPos
	 */
	public Set<DispositivoPos> getDispositivosPos() {
		return dispositivosPos;
	}

	/**
	 * @param dispositivosPos
	 *            the dispositivosPos to set
	 */
	public void setDispositivosPos(Set<DispositivoPos> dispositivosPos) {
		this.dispositivosPos = dispositivosPos;
	}

}
