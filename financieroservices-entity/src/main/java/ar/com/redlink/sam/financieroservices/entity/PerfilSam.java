package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.HashSet;
import java.util.Set;

/**
 * Representacion de un perfil SAM.
 * 
 * @author aguerrea
 */
public class PerfilSam extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8994270797046842555L;
	private long perfilSamId;
	private Entidad entidad;
	private String nombre;
	private String descripcion;

	private Set<PerfilFinanciero> perfilesFinanciero = new HashSet<PerfilFinanciero>(
			0);
	private Set<SucursalPerfilSam> sucursalesPerfilSam = new HashSet<SucursalPerfilSam>(
			0);

	/**
	 * @return the perfilSamId
	 */
	public long getPerfilSamId() {
		return perfilSamId;
	}

	/**
	 * @param perfilSamId
	 *            the perfilSamId to set
	 */
	public void setPerfilSamId(long perfilSamId) {
		this.perfilSamId = perfilSamId;
	}

	/**
	 * @return the entidad
	 */
	public Entidad getEntidad() {
		return entidad;
	}

	/**
	 * @param entidad
	 *            the entidad to set
	 */
	public void setEntidad(Entidad entidad) {
		this.entidad = entidad;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the perfilesFinanciero
	 */
	public Set<PerfilFinanciero> getPerfilesFinanciero() {
		return perfilesFinanciero;
	}

	/**
	 * @param perfilesFinanciero
	 *            the perfilesFinanciero to set
	 */
	public void setPerfilesFinanciero(Set<PerfilFinanciero> perfilesFinanciero) {
		this.perfilesFinanciero = perfilesFinanciero;
	}

	/**
	 * @return the sucursalesPerfilSam
	 */
	public Set<SucursalPerfilSam> getSucursalesPerfilSam() {
		return sucursalesPerfilSam;
	}

	/**
	 * @param sucursalesPerfilSam
	 *            the sucursalesPerfilSam to set
	 */
	public void setSucursalesPerfilSam(
			Set<SucursalPerfilSam> sucursalesPerfilSam) {
		this.sucursalesPerfilSam = sucursalesPerfilSam;
	}

}
