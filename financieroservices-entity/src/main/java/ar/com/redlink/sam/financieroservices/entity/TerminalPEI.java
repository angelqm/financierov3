package ar.com.redlink.sam.financieroservices.entity;

import java.util.Date;

public class TerminalPEI extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6659217836791923145L;

	private long terminalPEIId;
	private long terminalSamId;
	private String codigoTerminalPei;
	private EstadoRegistro estadoRegistro;
	private Date fechaAlta;
	private Date fechaModificacion;
	private Date fechaBaja;
	private String codigoSucursalPei;
	
	
	/**
	 * @return the terminalPEIId
	 */
	public long getTerminalPEIId() {
		return terminalPEIId;
	}

	/**
	 * @param terminalPEIId the terminalPEIId to set
	 */
	public void setTerminalPEIId(long terminalPEIId) {
		this.terminalPEIId = terminalPEIId;
	}

	/**
	 * @return the terminalSamId
	 */
	public long getTerminalSamId() {
		return terminalSamId;
	}

	/**
	 * @param terminalSamId the terminalSamId to set
	 */
	public void setTerminalSamId(long terminalSamId) {
		this.terminalSamId = terminalSamId;
	}

	/**
	 * @return the codigoTerminalPei
	 */
	public String getCodigoTerminalPei() {
		return codigoTerminalPei;
	}

	/**
	 * @param codigoTerminalPei the codigoTerminalPei to set
	 */
	public void setCodigoTerminalPei(String codigoTerminalPei) {
		this.codigoTerminalPei = codigoTerminalPei;
	}

	/**
	 * @return the estadoRegistro
	 */
	public EstadoRegistro getEstadoRegistro() {
		return estadoRegistro;
	}

	/**
	 * @param estadoRegistro the estadoRegistro to set
	 */
	public void setEstadoRegistro(EstadoRegistro estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	/**
	 * @return the fechaAlta
	 */
	public Date getFechaAlta() {
		return fechaAlta;
	}

	/**
	 * @param fechaAlta the fechaAlta to set
	 */
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	/**
	 * @return the fechaModificacion
	 */
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaModificacion the fechaModificacion to set
	 */
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	/**
	 * @return the fechaBaja
	 */
	public Date getFechaBaja() {
		return fechaBaja;
	}

	/**
	 * @param fechaBaja the fechaBaja to set
	 */
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	/**
	 * @return the codigoSucursalPei
	 */
	public String getCodigoSucursalPei() {
		return codigoSucursalPei;
	}

	/**
	 * @param codigoSucursalPEI the codigoSucursalPei to set
	 */
	public void setCodigoSucursalPei(String codigoSucursalPei) {
		this.codigoSucursalPei = codigoSucursalPei;
	}

}
