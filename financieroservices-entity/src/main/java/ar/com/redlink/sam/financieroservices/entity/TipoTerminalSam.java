package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.HashSet;
import java.util.Set;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representacion de un tipo de terminal SAM.
 * 
 * @author aguerrea
 */
public class TipoTerminalSam implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8407816725569217948L;
	private int tipoTerminalSamId;
	private String nombre;
	private String descripcion;
	private Set<TerminalSam> terminalesSam = new HashSet<TerminalSam>(0);

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the tipoTerminalSamId
	 */
	public int getTipoTerminalSamId() {
		return tipoTerminalSamId;
	}

	/**
	 * @param tipoTerminalSamId
	 *            the tipoTerminalSamId to set
	 */
	public void setTipoTerminalSamId(int tipoTerminalSamId) {
		this.tipoTerminalSamId = tipoTerminalSamId;
	}

	/**
	 * @return the terminalesSam
	 */
	public Set<TerminalSam> getTerminalesSam() {
		return terminalesSam;
	}

	/**
	 * @param terminalesSam
	 *            the terminalesSam to set
	 */
	public void setTerminalesSam(Set<TerminalSam> terminalesSam) {
		this.terminalesSam = terminalesSam;
	}

}
