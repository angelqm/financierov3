package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.HashSet;
import java.util.Set;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representacion de un tipo de terminal POS.
 * 
 * @author aguerrea
 */
public class TipoTerminalPos implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8407816725569217948L;
	private int tipoTerminalPosId;
	private String nombre;
	private String descripcion;
	private Set<TerminalPos> terminalesPos = new HashSet<TerminalPos>(0);

	/**
	 * @return the tipoTerminalPosId
	 */
	public int getTipoTerminalPosId() {
		return tipoTerminalPosId;
	}

	/**
	 * @param tipoTerminalPosId
	 *            the tipoTerminalPosId to set
	 */
	public void setTipoTerminalPosId(int tipoTerminalPosId) {
		this.tipoTerminalPosId = tipoTerminalPosId;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the terminalesPos
	 */
	public Set<TerminalPos> getTerminalesPos() {
		return terminalesPos;
	}

	/**
	 * @param terminalesPos
	 *            the terminalesPos to set
	 */
	public void setTerminalesPos(Set<TerminalPos> terminalesPos) {
		this.terminalesPos = terminalesPos;
	}
}
