package ar.com.redlink.sam.financieroservices.entity;

import java.util.Set;

public class AsociacionPerfil extends BaseEntity {


	/**
	 * 
	 */
	private static final long serialVersionUID = 4457357855245467970L;
	private long asociacionPerfilId;
	private Sucursal sucursal;
	private Set<AsociacionPerfilDetalle> asociacionPerfilDetalle;
	
	
	/**
	 * @return the asociacionPerfilId
	 */	
	public long getAsociacionPerfilId() {
		return asociacionPerfilId;
	}
	
	/**
	 * @param asociacionPerfilId
	 *            the asociacionPerfilId to set
	 */	
	public void setAsociacionPerfilId(long asociacionPerfilId) {
		this.asociacionPerfilId = asociacionPerfilId;
	}

	
	/**
	 * @return the sucursal
	 */	
	public Sucursal getSucursal() {
		return sucursal;
	}
	/**
	 * @param sucursal
	 *            the sucursal to set
	 */
	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}
	
	public Set<AsociacionPerfilDetalle> getAsociacionPerfilDetalle() {
		return asociacionPerfilDetalle;
	}

	public void setAsociacionPerfilDetalle(Set<AsociacionPerfilDetalle> asociacionPerfilDetalle) {
		this.asociacionPerfilDetalle = asociacionPerfilDetalle;
	}


	
}
