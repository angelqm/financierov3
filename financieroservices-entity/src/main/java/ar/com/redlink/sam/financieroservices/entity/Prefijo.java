package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.HashSet;
import java.util.Set;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representacion de un Prefijo.
 * 
 * @author aguerrea
 */
public class Prefijo implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3723993993613210185L;
	private long prefijoId;
	private TipoTarjeta tipoTarjeta;
	private Entidad entidad;
	private long prefijo;
	private short longPrefijo;
	private short longTarjeta;
	private Set<PrefijoEntidad> prefijoEntidades = new HashSet<PrefijoEntidad>(
			0);

	/**
	 * @return the prefijoId
	 */
	public long getPrefijoId() {
		return prefijoId;
	}

	/**
	 * @param prefijoId
	 *            the prefijoId to set
	 */
	public void setPrefijoId(long prefijoId) {
		this.prefijoId = prefijoId;
	}

	/**
	 * @return the tipoTarjeta
	 */
	public TipoTarjeta getTipoTarjeta() {
		return tipoTarjeta;
	}

	/**
	 * @param tipoTarjeta
	 *            the tipoTarjeta to set
	 */
	public void setTipoTarjeta(TipoTarjeta tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	/**
	 * @return the entidad
	 */
	public Entidad getEntidad() {
		return entidad;
	}

	/**
	 * @param entidad
	 *            the entidad to set
	 */
	public void setEntidad(Entidad entidad) {
		this.entidad = entidad;
	}

	/**
	 * @return the prefijo
	 */
	public long getPrefijo() {
		return prefijo;
	}

	/**
	 * @param prefijo
	 *            the prefijo to set
	 */
	public void setPrefijo(long prefijo) {
		this.prefijo = prefijo;
	}

	/**
	 * @return the longPrefijo
	 */
	public short getLongPrefijo() {
		return longPrefijo;
	}

	/**
	 * @param longPrefijo
	 *            the longPrefijo to set
	 */
	public void setLongPrefijo(short longPrefijo) {
		this.longPrefijo = longPrefijo;
	}

	/**
	 * @return the longTarjeta
	 */
	public short getLongTarjeta() {
		return longTarjeta;
	}

	/**
	 * @param longTarjeta
	 *            the longTarjeta to set
	 */
	public void setLongTarjeta(short longTarjeta) {
		this.longTarjeta = longTarjeta;
	}

	/**
	 * @return the prefijoEntidades
	 */
	public Set<PrefijoEntidad> getPrefijoEntidades() {
		return prefijoEntidades;
	}

	/**
	 * @param prefijoEntidades
	 *            the prefijoEntidades to set
	 */
	public void setPrefijoEntidades(Set<PrefijoEntidad> prefijoEntidades) {
		this.prefijoEntidades = prefijoEntidades;
	}

}
