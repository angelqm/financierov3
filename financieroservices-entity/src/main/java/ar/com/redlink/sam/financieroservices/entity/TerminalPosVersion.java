package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.Date;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representacion de la version de la terminal POS.
 * 
 * @author aguerrea
 */
public class TerminalPosVersion implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2657109965333913137L;
	private long terminalPosVersionId;
	private TerminalPos terminalPos;
	private String versionSoft;
	private Date fechaAlta;

	/**
	 * @return the terminalPosVersionId
	 */
	public long getTerminalPosVersionId() {
		return terminalPosVersionId;
	}

	/**
	 * @param terminalPosVersionId
	 *            the terminalPosVersionId to set
	 */
	public void setTerminalPosVersionId(long terminalPosVersionId) {
		this.terminalPosVersionId = terminalPosVersionId;
	}

	/**
	 * @return the terminalPos
	 */
	public TerminalPos getTerminalPos() {
		return terminalPos;
	}

	/**
	 * @param terminalPos
	 *            the terminalPos to set
	 */
	public void setTerminalPos(TerminalPos terminalPos) {
		this.terminalPos = terminalPos;
	}

	/**
	 * @return the versionSoft
	 */
	public String getVersionSoft() {
		return versionSoft;
	}

	/**
	 * @param versionSoft
	 *            the versionSoft to set
	 */
	public void setVersionSoft(String versionSoft) {
		this.versionSoft = versionSoft;
	}

	/**
	 * @return the fechaAlta
	 */
	public Date getFechaAlta() {
		return fechaAlta;
	}

	/**
	 * @param fechaAlta
	 *            the fechaAlta to set
	 */
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

}
