package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.HashSet;
import java.util.Set;

/**
 * Representacion de un servidor.
 */
public class Servidor extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5439034839861143797L;
	private long servidorId;
	private TipoServidor tipoServidor;
	private Sucursal sucursal;
	private EstadoRegistro estadoRegistro;
	private String codServidor;
	private String sitioPropioEnv;
	private String dirPropioEnv;
	private boolean transmisionRas;
	private boolean transmisionIp;
	private boolean transmisionVpn;
	private Set<TerminalSam> terminalesSam = new HashSet<TerminalSam>(0);

	/**
	 * @return the servidorId
	 */
	public long getServidorId() {
		return servidorId;
	}

	/**
	 * @param servidorId
	 *            the servidorId to set
	 */
	public void setServidorId(long servidorId) {
		this.servidorId = servidorId;
	}

	/**
	 * @return the tipoServidor
	 */
	public TipoServidor getTipoServidor() {
		return tipoServidor;
	}

	/**
	 * @param tipoServidor
	 *            the tipoServidor to set
	 */
	public void setTipoServidor(TipoServidor tipoServidor) {
		this.tipoServidor = tipoServidor;
	}

	/**
	 * @return the sucursal
	 */
	public Sucursal getSucursal() {
		return sucursal;
	}

	/**
	 * @param sucursal
	 *            the sucursal to set
	 */
	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	/**
	 * @return the estadoRegistro
	 */
	public EstadoRegistro getEstadoRegistro() {
		return estadoRegistro;
	}

	/**
	 * @param estadoRegistro
	 *            the estadoRegistro to set
	 */
	public void setEstadoRegistro(EstadoRegistro estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	/**
	 * @return the codServidor
	 */
	public String getCodServidor() {
		return codServidor;
	}

	/**
	 * @param codServidor
	 *            the codServidor to set
	 */
	public void setCodServidor(String codServidor) {
		this.codServidor = codServidor;
	}

	/**
	 * @return the sitioPropioEnv
	 */
	public String getSitioPropioEnv() {
		return sitioPropioEnv;
	}

	/**
	 * @param sitioPropioEnv
	 *            the sitioPropioEnv to set
	 */
	public void setSitioPropioEnv(String sitioPropioEnv) {
		this.sitioPropioEnv = sitioPropioEnv;
	}

	/**
	 * @return the dirPropioEnv
	 */
	public String getDirPropioEnv() {
		return dirPropioEnv;
	}

	/**
	 * @param dirPropioEnv
	 *            the dirPropioEnv to set
	 */
	public void setDirPropioEnv(String dirPropioEnv) {
		this.dirPropioEnv = dirPropioEnv;
	}

	/**
	 * @return the transmisionRas
	 */
	public boolean isTransmisionRas() {
		return transmisionRas;
	}

	/**
	 * @param transmisionRas
	 *            the transmisionRas to set
	 */
	public void setTransmisionRas(boolean transmisionRas) {
		this.transmisionRas = transmisionRas;
	}

	/**
	 * @return the transmisionIp
	 */
	public boolean isTransmisionIp() {
		return transmisionIp;
	}

	/**
	 * @param transmisionIp
	 *            the transmisionIp to set
	 */
	public void setTransmisionIp(boolean transmisionIp) {
		this.transmisionIp = transmisionIp;
	}

	/**
	 * @return the transmisionVpn
	 */
	public boolean isTransmisionVpn() {
		return transmisionVpn;
	}

	/**
	 * @param transmisionVpn
	 *            the transmisionVpn to set
	 */
	public void setTransmisionVpn(boolean transmisionVpn) {
		this.transmisionVpn = transmisionVpn;
	}

	/**
	 * @return the terminalesSam
	 */
	public Set<TerminalSam> getTerminalesSam() {
		return terminalesSam;
	}

	/**
	 * @param terminalesSam
	 *            the terminalesSam to set
	 */
	public void setTerminalesSam(Set<TerminalSam> terminalesSam) {
		this.terminalesSam = terminalesSam;
	}

}
