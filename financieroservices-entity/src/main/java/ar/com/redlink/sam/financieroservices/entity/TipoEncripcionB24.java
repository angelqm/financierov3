package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

/**
 * Representacion de un tipo de encripcion de Base 24.
 * 
 * @author aguerrea
 */
public class TipoEncripcionB24 extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5392923483467757395L;
	private short tipoEncripcionB24Id;
	private String nombre;
	private String descripcion;

	// private Set<DispositivoPos> dispositivosPos = new
	// HashSet<DispositivoPos>(0);

	/**
	 * @return the tipoEncripcionB24Id
	 */
	public short getTipoEncripcionB24Id() {
		return tipoEncripcionB24Id;
	}

	/**
	 * @param tipoEncripcionB24Id
	 *            the tipoEncripcionB24Id to set
	 */
	public void setTipoEncripcionB24Id(short tipoEncripcionB24Id) {
		this.tipoEncripcionB24Id = tipoEncripcionB24Id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	// /**
	// * @return the dispositivosPos
	// */
	// public Set<DispositivoPos> getDispositivosPos() {
	// return dispositivosPos;
	// }
	//
	// /**
	// * @param dispositivosPos
	// * the dispositivosPos to set
	// */
	// public void setDispositivosPos(Set<DispositivoPos> dispositivosPos) {
	// this.dispositivosPos = dispositivosPos;
	// }

}
