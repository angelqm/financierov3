package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.HashSet;
import java.util.Set;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representacion de un tipo de persona.
 * 
 * @author aguerrea
 */
public class TipoPersona implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -913462461636089626L;
	private boolean tipoPersonaId;
	private String nombre;
	private String descripcion;
	private Set<Agente> agentes = new HashSet<Agente>(0);

	/**
	 * @return the tipoPersonaId
	 */
	public boolean isTipoPersonaId() {
		return tipoPersonaId;
	}

	/**
	 * @param tipoPersonaId
	 *            the tipoPersonaId to set
	 */
	public void setTipoPersonaId(boolean tipoPersonaId) {
		this.tipoPersonaId = tipoPersonaId;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the agentes
	 */
	public Set<Agente> getAgentes() {
		return agentes;
	}

	/**
	 * @param agentes
	 *            the agentes to set
	 */
	public void setAgentes(Set<Agente> agentes) {
		this.agentes = agentes;
	}

}
