package ar.com.redlink.sam.financieroservices.entity;

import java.util.Date;

import ar.com.redlink.framework.entities.RedLinkEntity;

public class Intencion implements RedLinkEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long intencionId;
	private TerminalSam terminalSamId;
	private PrefijoOp prefijoOpId;
	private EstadoTrx estadoTrxId;
	private String requerimientoId;
	private Date fechaInicioIntencion;
	private Date fechaFinIntencion;
	private String observacion;
	/**
	 * @return the intencionId
	 */
	public Long getIntencionId() {
		return intencionId;
	}
	/**
	 * @param intencionId the intencionId to set
	 */
	public void setIntencionId(Long intencionId) {
		this.intencionId = intencionId;
	}
	/**
	 * @return the terminalSamId
	 */
	public TerminalSam getTerminalSamId() {
		return terminalSamId;
	}
	/**
	 * @param terminalSamId the terminalSamId to set
	 */
	public void setTerminalSamId(TerminalSam terminalSamId) {
		this.terminalSamId = terminalSamId;
	}
	/**
	 * @return the prefijoOpId
	 */
	public PrefijoOp getPrefijoOpId() {
		return prefijoOpId;
	}
	/**
	 * @param prefijoOpId the prefijoOpId to set
	 */
	public void setPrefijoOpId(PrefijoOp prefijoOpId) {
		this.prefijoOpId = prefijoOpId;
	}
	/**
	 * @return the estadoTrxId
	 */
	public EstadoTrx getEstadoTrxId() {
		return estadoTrxId;
	}
	/**
	 * @param estadoTrxId the estadoTrxId to set
	 */
	public void setEstadoTrxId(EstadoTrx estadoTrxId) {
		this.estadoTrxId = estadoTrxId;
	}
	/**
	 * @return the requerimientoId
	 */
	public String getRequerimientoId() {
		return requerimientoId;
	}
	/**
	 * @param requerimientoId the requerimientoId to set
	 */
	public void setRequerimientoId(String requerimientoId) {
		this.requerimientoId = requerimientoId;
	}
	/**
	 * @return the fechaInicioIntencion
	 */
	public Date getFechaInicioIntencion() {
		return fechaInicioIntencion;
	}
	/**
	 * @param fechaInicioIntencion the fechaInicioIntencion to set
	 */
	public void setFechaInicioIntencion(Date fechaInicioIntencion) {
		this.fechaInicioIntencion = fechaInicioIntencion;
	}
	/**
	 * @return the fechaFinIntencion
	 */
	public Date getFechaFinIntencion() {
		return fechaFinIntencion;
	}
	/**
	 * @param fechaFinIntencion the fechaFinIntencion to set
	 */
	public void setFechaFinIntencion(Date fechaFinIntencion) {
		this.fechaFinIntencion = fechaFinIntencion;
	}
	/**
	 * @return the observacion
	 */
	public String getObservacion() {
		return observacion;
	}
	/**
	 * @param observacion the observacion to set
	 */
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	
}
