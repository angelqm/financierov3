package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representacion de una terminal POS.
 * 
 * @author aguerrea
 */
public class TerminalPos implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6944351583942477378L;
	private long terminalPosId;
	private DispositivoPos dispositivoPos;
	private TipoTerminalPos tipoTerminalPos;
	private Sucursal sucursal;
	private Entidad entidad;
	private EstadoRegistro estadoRegistro;
	private String codTerminal;
	private String terminalB24;
	private Date fechaActivacion;
	private Date fechaAlta;
	private Date fechaModificacion;
	private Date fechaBaja;
	private Set<TerminalPosVersion> terminalPosVersiones = new HashSet<TerminalPosVersion>(0);
	

	/**
	 * @return the terminalPosId
	 */
	public long getTerminalPosId() {
		return terminalPosId;
	}

	/**
	 * @param terminalPosId
	 *            the terminalPosId to set
	 */
	public void setTerminalPosId(long terminalPosId) {
		this.terminalPosId = terminalPosId;
	}

	/**
	 * @return the dispositivoPos
	 */
	public DispositivoPos getDispositivoPos() {
		return dispositivoPos;
	}

	/**
	 * @param dispositivoPos
	 *            the dispositivoPos to set
	 */
	public void setDispositivoPos(DispositivoPos dispositivoPos) {
		this.dispositivoPos = dispositivoPos;
	}

	/**
	 * @return the tipoTerminalPos
	 */
	public TipoTerminalPos getTipoTerminalPos() {
		return tipoTerminalPos;
	}

	/**
	 * @param tipoTerminalPos
	 *            the tipoTerminalPos to set
	 */
	public void setTipoTerminalPos(TipoTerminalPos tipoTerminalPos) {
		this.tipoTerminalPos = tipoTerminalPos;
	}

	/**
	 * @return the sucursal
	 */
	public Sucursal getSucursal() {
		return sucursal;
	}

	/**
	 * @param sucursal
	 *            the sucursal to set
	 */
	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	/**
	 * @return the entidad
	 */
	public Entidad getEntidad() {
		return entidad;
	}

	/**
	 * @param entidad
	 *            the entidad to set
	 */
	public void setEntidad(Entidad entidad) {
		this.entidad = entidad;
	}

	/**
	 * @return the estadoRegistro
	 */
	public EstadoRegistro getEstadoRegistro() {
		return estadoRegistro;
	}

	/**
	 * @param estadoRegistro
	 *            the estadoRegistro to set
	 */
	public void setEstadoRegistro(EstadoRegistro estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	/**
	 * @return the codTerminal
	 */
	public String getCodTerminal() {
		return codTerminal;
	}

	/**
	 * @param codTerminal
	 *            the codTerminal to set
	 */
	public void setCodTerminal(String codTerminal) {
		this.codTerminal = codTerminal;
	}

	/**
	 * @return the terminalB24
	 */
	public String getTerminalB24() {
		return terminalB24;
	}

	/**
	 * @param terminalB24
	 *            the terminalB24 to set
	 */
	public void setTerminalB24(String terminalB24) {
		this.terminalB24 = terminalB24;
	}

	/**
	 * @return the fechaActivacion
	 */
	public Date getFechaActivacion() {
		return fechaActivacion;
	}

	/**
	 * @param fechaActivacion
	 *            the fechaActivacion to set
	 */
	public void setFechaActivacion(Date fechaActivacion) {
		this.fechaActivacion = fechaActivacion;
	}

	/**
	 * @return the fechaAlta
	 */
	public Date getFechaAlta() {
		return fechaAlta;
	}

	/**
	 * @param fechaAlta
	 *            the fechaAlta to set
	 */
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	/**
	 * @return the fechaModificacion
	 */
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaModificacion
	 *            the fechaModificacion to set
	 */
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	/**
	 * @return the fechaBaja
	 */
	public Date getFechaBaja() {
		return fechaBaja;
	}

	/**
	 * @param fechaBaja
	 *            the fechaBaja to set
	 */
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	/**
	 * @return the terminalPosVersiones
	 */
	public Set<TerminalPosVersion> getTerminalPosVersiones() {
		return terminalPosVersiones;
	}

	/**
	 * @param terminalPosVersiones
	 *            the terminalPosVersiones to set
	 */
	public void setTerminalPosVersiones(
			Set<TerminalPosVersion> terminalPosVersiones) {
		this.terminalPosVersiones = terminalPosVersiones;
	}

}
