package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.HashSet;
import java.util.Set;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representacion de la relacion Tipo Prefijo y Regla.
 * 
 * @author aguerrea
 */
public class TipoPrefijoRegla implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -730092454662714079L;
	private long tipoPrefijoReglaId;
	private boolean habilitado;
	private String nombre;
	private String descripcion;
	private int precisionValor;
	private Set<GrupoPrefijoOpRegla> grupoPrefijoOpReglas = new HashSet<GrupoPrefijoOpRegla>(
			0);

	/**
	 * @return the tipoPrefijoReglaId
	 */
	public long getTipoPrefijoReglaId() {
		return tipoPrefijoReglaId;
	}

	/**
	 * @param tipoPrefijoReglaId
	 *            the tipoPrefijoReglaId to set
	 */
	public void setTipoPrefijoReglaId(long tipoPrefijoReglaId) {
		this.tipoPrefijoReglaId = tipoPrefijoReglaId;
	}

	/**
	 * @return the habilitado
	 */
	public boolean isHabilitado() {
		return habilitado;
	}

	/**
	 * @param habilitado
	 *            the habilitado to set
	 */
	public void setHabilitado(boolean habilitado) {
		this.habilitado = habilitado;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the precisionValor
	 */
	public int getPrecisionValor() {
		return precisionValor;
	}

	/**
	 * @param precisionValor
	 *            the precisionValor to set
	 */
	public void setPrecisionValor(int precisionValor) {
		this.precisionValor = precisionValor;
	}

	/**
	 * @return the grupoPrefijoOpReglas
	 */
	public Set<GrupoPrefijoOpRegla> getGrupoPrefijoOpReglas() {
		return grupoPrefijoOpReglas;
	}

	/**
	 * @param grupoPrefijoOpReglas
	 *            the grupoPrefijoOpReglas to set
	 */
	public void setGrupoPrefijoOpReglas(
			Set<GrupoPrefijoOpRegla> grupoPrefijoOpReglas) {
		this.grupoPrefijoOpReglas = grupoPrefijoOpReglas;
	}

}
