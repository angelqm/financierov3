package ar.com.redlink.sam.financieroservices.entity;

import java.util.HashSet;
import java.util.Set;

/**
 * Representacion de un Agente.
 * 
 * @author aguerrea
 */
public class Agente extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6593078795847170682L;
	private long agenteId;
	private TipoPersona tipoPersona;
	private Entidad entidad;
	private EstadoRegistro estadoRegistro;
	private TipoAgencia tipoAgencia;
	private String cuit;
	private String nombreFantasia;
	private String razonSocial;
	private String apellido;
	private String nombre;
	private Boolean operatoriaPosOnUs;
	private Set<Sucursal> sucursales = new HashSet<Sucursal>(0);
	private Set<TrxFinanciero> trxsFinanciero = new HashSet<TrxFinanciero>(0);

	/**
	 * @return the agenteId
	 */
	public long getAgenteId() {
		return agenteId;
	}

	/**
	 * @param agenteId
	 *            the agenteId to set
	 */
	public void setAgenteId(long agenteId) {
		this.agenteId = agenteId;
	}

	/**
	 * @return the tipoPersona
	 */
	public TipoPersona getTipoPersona() {
		return tipoPersona;
	}

	/**
	 * @param tipoPersona
	 *            the tipoPersona to set
	 */
	public void setTipoPersona(TipoPersona tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	/**
	 * @return the entidad
	 */
	public Entidad getEntidad() {
		return entidad;
	}

	/**
	 * @param entidad
	 *            the entidad to set
	 */
	public void setEntidad(Entidad entidad) {
		this.entidad = entidad;
	}

	/**
	 * @return the estadoRegistro
	 */
	public EstadoRegistro getEstadoRegistro() {
		return estadoRegistro;
	}

	/**
	 * @param estadoRegistro
	 *            the estadoRegistro to set
	 */
	public void setEstadoRegistro(EstadoRegistro estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	/**
	 * @return the tipoAgencia
	 */
	public TipoAgencia getTipoAgencia() {
		return tipoAgencia;
	}

	/**
	 * @param tipoAgencia
	 *            the tipoAgencia to set
	 */
	public void setTipoAgencia(TipoAgencia tipoAgencia) {
		this.tipoAgencia = tipoAgencia;
	}

	/**
	 * @return the cuit
	 */
	public String getCuit() {
		return cuit;
	}

	/**
	 * @param cuit
	 *            the cuit to set
	 */
	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	/**
	 * @return the nombreFantasia
	 */
	public String getNombreFantasia() {
		return nombreFantasia;
	}

	/**
	 * @param nombreFantasia
	 *            the nombreFantasia to set
	 */
	public void setNombreFantasia(String nombreFantasia) {
		this.nombreFantasia = nombreFantasia;
	}

	/**
	 * @return the razonSocial
	 */
	public String getRazonSocial() {
		return razonSocial;
	}

	/**
	 * @param razonSocial
	 *            the razonSocial to set
	 */
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	/**
	 * @return the apellido
	 */
	public String getApellido() {
		return apellido;
	}

	/**
	 * @param apellido
	 *            the apellido to set
	 */
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the operatoriaPosOnUs
	 */
	public Boolean getOperatoriaPosOnUs() {
		return operatoriaPosOnUs;
	}

	/**
	 * @param operatoriaPosOnUs
	 *            the operatoriaPosOnUs to set
	 */
	public void setOperatoriaPosOnUs(Boolean operatoriaPosOnUs) {
		this.operatoriaPosOnUs = operatoriaPosOnUs;
	}
	
	/**
	 * @return the sucursales
	 */
	public Set<Sucursal> getSucursales() {
		return sucursales;
	}

	/**
	 * @param sucursales
	 *            the sucursales to set
	 */
	public void setSucursales(Set<Sucursal> sucursales) {
		this.sucursales = sucursales;
	}

	/**
	 * @return the trxsFinanciero
	 */
	public Set<TrxFinanciero> getTrxsFinanciero() {
		return trxsFinanciero;
	}

	/**
	 * @param trxsFinanciero
	 *            the trxsFinanciero to set
	 */
	public void setTrxsFinanciero(Set<TrxFinanciero> trxsFinanciero) {
		this.trxsFinanciero = trxsFinanciero;
	}

}
