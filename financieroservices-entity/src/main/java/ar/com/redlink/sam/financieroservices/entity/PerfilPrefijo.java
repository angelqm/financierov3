package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representacion de la relacion Perfil - Prefijo.
 * 
 * 
 * @author aguerrea
 */
public class PerfilPrefijo implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3199357028335955607L;
	private long perfilPrefijoId;
	private PerfilPrefijo perfilPrefijo;
	private Entidad entidad;
	private EstadoRegistro estadoRegistro;
	private String nombre;
	private String descripcion;
	private Date fechaAlta;
	private Date fechaModificacion;
	private Date fechaBaja;
	private Set<PerfilPrefijo> perfilPrefijos = new HashSet<PerfilPrefijo>(0);
	private Set<PerfilPrefijoGrupo> perfilPrefijoGrupos = new HashSet<PerfilPrefijoGrupo>(
			0);
	private Set<PerfilFinanciero> perfilesFinanciero = new HashSet<PerfilFinanciero>(
			0);

	/**
	 * @return the perfilPrefijoId
	 */
	public long getPerfilPrefijoId() {
		return perfilPrefijoId;
	}

	/**
	 * @param perfilPrefijoId
	 *            the perfilPrefijoId to set
	 */
	public void setPerfilPrefijoId(long perfilPrefijoId) {
		this.perfilPrefijoId = perfilPrefijoId;
	}

	/**
	 * @return the perfilPrefijo
	 */
	public PerfilPrefijo getPerfilPrefijo() {
		return perfilPrefijo;
	}

	/**
	 * @param perfilPrefijo
	 *            the perfilPrefijo to set
	 */
	public void setPerfilPrefijo(PerfilPrefijo perfilPrefijo) {
		this.perfilPrefijo = perfilPrefijo;
	}

	/**
	 * @return the entidad
	 */
	public Entidad getEntidad() {
		return entidad;
	}

	/**
	 * @param entidad
	 *            the entidad to set
	 */
	public void setEntidad(Entidad entidad) {
		this.entidad = entidad;
	}

	/**
	 * @return the estadoRegistro
	 */
	public EstadoRegistro getEstadoRegistro() {
		return estadoRegistro;
	}

	/**
	 * @param estadoRegistro
	 *            the estadoRegistro to set
	 */
	public void setEstadoRegistro(EstadoRegistro estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the fechaAlta
	 */
	public Date getFechaAlta() {
		return fechaAlta;
	}

	/**
	 * @param fechaAlta
	 *            the fechaAlta to set
	 */
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	/**
	 * @return the fechaModificacion
	 */
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaModificacion
	 *            the fechaModificacion to set
	 */
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	/**
	 * @return the fechaBaja
	 */
	public Date getFechaBaja() {
		return fechaBaja;
	}

	/**
	 * @param fechaBaja
	 *            the fechaBaja to set
	 */
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	/**
	 * @return the perfilPrefijos
	 */
	public Set<PerfilPrefijo> getPerfilPrefijos() {
		return perfilPrefijos;
	}

	/**
	 * @param perfilPrefijos
	 *            the perfilPrefijos to set
	 */
	public void setPerfilPrefijos(Set<PerfilPrefijo> perfilPrefijos) {
		this.perfilPrefijos = perfilPrefijos;
	}

	/**
	 * @return the perfilPrefijoGrupos
	 */
	public Set<PerfilPrefijoGrupo> getPerfilPrefijoGrupos() {
		return perfilPrefijoGrupos;
	}

	/**
	 * @param perfilPrefijoGrupos
	 *            the perfilPrefijoGrupos to set
	 */
	public void setPerfilPrefijoGrupos(
			Set<PerfilPrefijoGrupo> perfilPrefijoGrupos) {
		this.perfilPrefijoGrupos = perfilPrefijoGrupos;
	}

	/**
	 * @return the perfilesFinanciero
	 */
	public Set<PerfilFinanciero> getPerfilesFinanciero() {
		return perfilesFinanciero;
	}

	/**
	 * @param perfilesFinanciero
	 *            the perfilesFinanciero to set
	 */
	public void setPerfilesFinanciero(Set<PerfilFinanciero> perfilesFinanciero) {
		this.perfilesFinanciero = perfilesFinanciero;
	}

}
