package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9


/**
 * Representacion de la relacion Sucursal - Perfil SAM.
 * 
 * @author aguerrea
 */
public class SucursalPerfilSam extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1921266950974968489L;
	private long sucursalPerfilSamId;
	private PerfilSam perfilSam;
	private Sucursal sucursal;

	/**
	 * @return the sucursalPerfilSamId
	 */
	public long getSucursalPerfilSamId() {
		return sucursalPerfilSamId;
	}

	/**
	 * @param sucursalPerfilSamId
	 *            the sucursalPerfilSamId to set
	 */
	public void setSucursalPerfilSamId(long sucursalPerfilSamId) {
		this.sucursalPerfilSamId = sucursalPerfilSamId;
	}

	/**
	 * @return the perfilSam
	 */
	public PerfilSam getPerfilSam() {
		return perfilSam;
	}

	/**
	 * @param perfilSam
	 *            the perfilSam to set
	 */
	public void setPerfilSam(PerfilSam perfilSam) {
		this.perfilSam = perfilSam;
	}

	/**
	 * @return the sucursal
	 */
	public Sucursal getSucursal() {
		return sucursal;
	}

	/**
	 * @param sucursal
	 *            the sucursal to set
	 */
	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}
}
