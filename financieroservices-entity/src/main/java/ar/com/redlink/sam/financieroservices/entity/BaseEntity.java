/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  27/03/2015 - 23:17:09
 */

package ar.com.redlink.sam.financieroservices.entity;

import java.util.Date;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Componente que tiene atributos comunes a todas las entidades para facilitar
 * los esquemas de validacion.
 * 
 * @author aguerrea
 * 
 */
public class BaseEntity implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4786030497979501971L;
	private EstadoRegistro estadoRegistro;
	private Date fechaActivacion;
	private Date fechaAlta;
	private Date fechaModificacion;
	private Date fechaBaja;
	private Date vigenciaDesde;
	private Date vigenciaHasta;


	/**
	 * @return the estadoRegistro
	 */
	public EstadoRegistro getEstadoRegistro() {
		return estadoRegistro;
	}

	/**
	 * @param estadoRegistro
	 *            the estadoRegistro to set
	 */
	public void setEstadoRegistro(EstadoRegistro estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	/**
	 * @return the fechaActivacion
	 */
	public Date getFechaActivacion() {
		return fechaActivacion;
	}

	/**
	 * @param fechaActivacion the fechaActivacion to set
	 */
	public void setFechaActivacion(Date fechaActivacion) {
		this.fechaActivacion = fechaActivacion;
	}

	/**
	 * @return the fechaAlta
	 */
	public Date getFechaAlta() {
		return fechaAlta;
	}

	/**
	 * @param fechaAlta the fechaAlta to set
	 */
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	/**
	 * @return the fechaModificacion
	 */
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaModificacion the fechaModificacion to set
	 */
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	/**
	 * @return the fechaBaja
	 */
	public Date getFechaBaja() {
		return fechaBaja;
	}

	/**
	 * @param fechaBaja the fechaBaja to set
	 */
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	/**
	 * @return the vigenciaDesde
	 */
	public Date getVigenciaDesde() {
		return vigenciaDesde;
	}

	/**
	 * @param vigenciaDesde the vigenciaDesde to set
	 */
	public void setVigenciaDesde(Date vigenciaDesde) {
		this.vigenciaDesde = vigenciaDesde;
	}

	/**
	 * @return the vigenciaHasta
	 */
	public Date getVigenciaHasta() {
		return vigenciaHasta;
	}

	/**
	 * @param vigenciaHasta the vigenciaHasta to set
	 */
	public void setVigenciaHasta(Date vigenciaHasta) {
		this.vigenciaHasta = vigenciaHasta;
	}
}
