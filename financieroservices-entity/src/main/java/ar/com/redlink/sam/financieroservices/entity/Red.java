package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representacion de una Red.
 * 
 * @author aguerrea
 */
public class Red implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2336010101015806925L;
	private int redId;
	private Entidad entidadByEntidadAsociadaId;
	private Entidad entidadByEntidadId;
	private EstadoRegistro estadoRegistro;
	private String descripcion;
	private Date fechaAlta;
	private Date fechaModificacion;
	private Date fechaBaja;
	private Set<Sucursal> sucursales = new HashSet<Sucursal>(0);
	private Set<Agente> agentes = new HashSet<Agente>(0);
	private Set<TrxFinanciero> trxsFinanciero = new HashSet<TrxFinanciero>();

	/**
	 * @return the redId
	 */
	public int getRedId() {
		return redId;
	}

	/**
	 * @param redId
	 *            the redId to set
	 */
	public void setRedId(int redId) {
		this.redId = redId;
	}

	/**
	 * @return the entidadByEntidadAsociadaId
	 */
	public Entidad getEntidadByEntidadAsociadaId() {
		return entidadByEntidadAsociadaId;
	}

	/**
	 * @param entidadByEntidadAsociadaId
	 *            the entidadByEntidadAsociadaId to set
	 */
	public void setEntidadByEntidadAsociadaId(Entidad entidadByEntidadAsociadaId) {
		this.entidadByEntidadAsociadaId = entidadByEntidadAsociadaId;
	}

	/**
	 * @return the entidadByEntidadId
	 */
	public Entidad getEntidadByEntidadId() {
		return entidadByEntidadId;
	}

	/**
	 * @param entidadByEntidadId
	 *            the entidadByEntidadId to set
	 */
	public void setEntidadByEntidadId(Entidad entidadByEntidadId) {
		this.entidadByEntidadId = entidadByEntidadId;
	}

	/**
	 * @return the estadoRegistro
	 */
	public EstadoRegistro getEstadoRegistro() {
		return estadoRegistro;
	}

	/**
	 * @param estadoRegistro
	 *            the estadoRegistro to set
	 */
	public void setEstadoRegistro(EstadoRegistro estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the fechaAlta
	 */
	public Date getFechaAlta() {
		return fechaAlta;
	}

	/**
	 * @param fechaAlta
	 *            the fechaAlta to set
	 */
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	/**
	 * @return the fechaModificacion
	 */
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaModificacion
	 *            the fechaModificacion to set
	 */
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	/**
	 * @return the fechaBaja
	 */
	public Date getFechaBaja() {
		return fechaBaja;
	}

	/**
	 * @param fechaBaja
	 *            the fechaBaja to set
	 */
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	/**
	 * @return the sucursales
	 */
	public Set<Sucursal> getSucursales() {
		return sucursales;
	}

	/**
	 * @param sucursales
	 *            the sucursales to set
	 */
	public void setSucursales(Set<Sucursal> sucursales) {
		this.sucursales = sucursales;
	}

	/**
	 * @return the agentes
	 */
	public Set<Agente> getAgentes() {
		return agentes;
	}

	/**
	 * @param agentes
	 *            the agentes to set
	 */
	public void setAgentes(Set<Agente> agentes) {
		this.agentes = agentes;
	}

	/**
	 * @return the trxsFinanciero
	 */
	public Set<TrxFinanciero> getTrxsFinanciero() {
		return trxsFinanciero;
	}

	/**
	 * @param trxsFinanciero the trxsFinanciero to set
	 */
	public void setTrxsFinanciero(Set<TrxFinanciero> trxsFinanciero) {
		this.trxsFinanciero = trxsFinanciero;
	}

}
