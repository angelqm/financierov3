package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.HashSet;
import java.util.Set;

/**
 * Representacion de la relacion Grupo Prefijo - Operacion.
 * 
 * @author aguerrea
 */
public class GrupoPrefijoOp extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1740564173069941907L;
	private long grupoPrefijoOpId;
	private PerfilPrefijoGrupo perfilPrefijoGrupo;
	private PrefijoOpEntidad prefijoOpEntidad;
	private EstadoRegistro estadoRegistro;
	private boolean requiereSfx;
	private boolean requiereSeg;
	private boolean requierePin;
	private boolean requiereDni;
	private boolean requiereAut;
	private boolean requiereTar;
	private boolean ticketFirma;
	private byte ticketCantidadCopias;
	private Set<GrupoPrefijoOpRegla> grupoPrefijoOpReglas = new HashSet<GrupoPrefijoOpRegla>(
			0);

	/**
	 * @return the grupoPrefijoOpId
	 */
	public long getGrupoPrefijoOpId() {
		return grupoPrefijoOpId;
	}

	/**
	 * @param grupoPrefijoOpId
	 *            the grupoPrefijoOpId to set
	 */
	public void setGrupoPrefijoOpId(long grupoPrefijoOpId) {
		this.grupoPrefijoOpId = grupoPrefijoOpId;
	}

	/**
	 * @return the perfilPrefijoGrupo
	 */
	public PerfilPrefijoGrupo getPerfilPrefijoGrupo() {
		return perfilPrefijoGrupo;
	}

	/**
	 * @param perfilPrefijoGrupo
	 *            the perfilPrefijoGrupo to set
	 */
	public void setPerfilPrefijoGrupo(PerfilPrefijoGrupo perfilPrefijoGrupo) {
		this.perfilPrefijoGrupo = perfilPrefijoGrupo;
	}

	/**
	 * @return the prefijoOpEntidad
	 */
	public PrefijoOpEntidad getPrefijoOpEntidad() {
		return prefijoOpEntidad;
	}

	/**
	 * @param prefijoOpEntidad
	 *            the prefijoOpEntidad to set
	 */
	public void setPrefijoOpEntidad(PrefijoOpEntidad prefijoOpEntidad) {
		this.prefijoOpEntidad = prefijoOpEntidad;
	}

	/**
	 * @return the estadoRegistro
	 */
	public EstadoRegistro getEstadoRegistro() {
		return estadoRegistro;
	}

	/**
	 * @param estadoRegistro
	 *            the estadoRegistro to set
	 */
	public void setEstadoRegistro(EstadoRegistro estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	/**
	 * @return the requiereSfx
	 */
	public boolean isRequiereSfx() {
		return requiereSfx;
	}

	/**
	 * @param requiereSfx
	 *            the requiereSfx to set
	 */
	public void setRequiereSfx(boolean requiereSfx) {
		this.requiereSfx = requiereSfx;
	}

	/**
	 * @return the requiereSeg
	 */
	public boolean isRequiereSeg() {
		return requiereSeg;
	}

	/**
	 * @param requiereSeg
	 *            the requiereSeg to set
	 */
	public void setRequiereSeg(boolean requiereSeg) {
		this.requiereSeg = requiereSeg;
	}

	/**
	 * @return the requierePin
	 */
	public boolean isRequierePin() {
		return requierePin;
	}

	/**
	 * @param requierePin
	 *            the requierePin to set
	 */
	public void setRequierePin(boolean requierePin) {
		this.requierePin = requierePin;
	}

	/**
	 * @return the requiereDni
	 */
	public boolean isRequiereDni() {
		return requiereDni;
	}

	/**
	 * @param requiereDni
	 *            the requiereDni to set
	 */
	public void setRequiereDni(boolean requiereDni) {
		this.requiereDni = requiereDni;
	}

	/**
	 * @return the requiereAut
	 */
	public boolean isRequiereAut() {
		return requiereAut;
	}

	/**
	 * @param requiereAut
	 *            the requiereAut to set
	 */
	public void setRequiereAut(boolean requiereAut) {
		this.requiereAut = requiereAut;
	}

	/**
	 * @return the ticketFirma
	 */
	public boolean isTicketFirma() {
		return ticketFirma;
	}

	/**
	 * @param ticketFirma
	 *            the ticketFirma to set
	 */
	public void setTicketFirma(boolean ticketFirma) {
		this.ticketFirma = ticketFirma;
	}

	/**
	 * @return the ticketCantidadCopias
	 */
	public byte getTicketCantidadCopias() {
		return ticketCantidadCopias;
	}

	/**
	 * @param ticketCantidadCopias
	 *            the ticketCantidadCopias to set
	 */
	public void setTicketCantidadCopias(byte ticketCantidadCopias) {
		this.ticketCantidadCopias = ticketCantidadCopias;
	}

	/**
	 * @return the grupoPrefijoOpReglas
	 */
	public Set<GrupoPrefijoOpRegla> getGrupoPrefijoOpReglas() {
		return grupoPrefijoOpReglas;
	}

	/**
	 * @param grupoPrefijoOpReglas
	 *            the grupoPrefijoOpReglas to set
	 */
	public void setGrupoPrefijoOpReglas(
			Set<GrupoPrefijoOpRegla> grupoPrefijoOpReglas) {
		this.grupoPrefijoOpReglas = grupoPrefijoOpReglas;
	}

	/**
	 * @return the requiereTar
	 */
	public boolean isRequiereTar() {
		return requiereTar;
	}

	/**
	 * @param requiereTar
	 *            the requiereTar to set
	 */
	public void setRequiereTar(boolean requiereTar) {
		this.requiereTar = requiereTar;
	}
}
