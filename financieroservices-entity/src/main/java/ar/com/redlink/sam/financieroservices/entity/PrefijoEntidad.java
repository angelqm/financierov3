package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.HashSet;
import java.util.Set;

/**
 * Representacion de la relacion Prefijo Entidad.
 * 
 * @author aguerrea
 */
public class PrefijoEntidad extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6941856284291483714L;
	private long prefijoEntidadId;
	private Prefijo prefijo;
	private Entidad entidad;
	private Set<GrupoPrefijoEntidad> grupoPrefijoEntidades = new HashSet<GrupoPrefijoEntidad>(
			0);

	/**
	 * @return the prefijoEntidadId
	 */
	public long getPrefijoEntidadId() {
		return prefijoEntidadId;
	}

	/**
	 * @param prefijoEntidadId
	 *            the prefijoEntidadId to set
	 */
	public void setPrefijoEntidadId(long prefijoEntidadId) {
		this.prefijoEntidadId = prefijoEntidadId;
	}

	/**
	 * @return the prefijo
	 */
	public Prefijo getPrefijo() {
		return prefijo;
	}

	/**
	 * @param prefijo
	 *            the prefijo to set
	 */
	public void setPrefijo(Prefijo prefijo) {
		this.prefijo = prefijo;
	}

	/**
	 * @return the entidad
	 */
	public Entidad getEntidad() {
		return entidad;
	}

	/**
	 * @param entidad
	 *            the entidad to set
	 */
	public void setEntidad(Entidad entidad) {
		this.entidad = entidad;
	}

	/**
	 * @return the grupoPrefijoEntidades
	 */
	public Set<GrupoPrefijoEntidad> getGrupoPrefijoEntidades() {
		return grupoPrefijoEntidades;
	}

	/**
	 * @param grupoPrefijoEntidades
	 *            the grupoPrefijoEntidades to set
	 */
	public void setGrupoPrefijoEntidades(
			Set<GrupoPrefijoEntidad> grupoPrefijoEntidades) {
		this.grupoPrefijoEntidades = grupoPrefijoEntidades;
	}

}
