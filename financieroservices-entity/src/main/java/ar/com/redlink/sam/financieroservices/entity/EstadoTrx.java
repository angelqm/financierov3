package ar.com.redlink.sam.financieroservices.entity;

import ar.com.redlink.framework.entities.RedLinkEntity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

/**
 * Representacion de un estado de Transaccion.
 * 
 * @author aguerrea
 */
public class EstadoTrx implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5062410500809137371L;
	private byte estadoTrxId;
	private String nombre;
	private String descripcion;

	public EstadoTrx() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EstadoTrx(byte estadoTrxId) {
		super();
		this.estadoTrxId = estadoTrxId;
	}

	/**
	 * @return the estadoTrxId
	 */
	public byte getEstadoTrxId() {
		return estadoTrxId;
	}

	/**
	 * @param estadoTrxId
	 *            the estadoTrxId to set
	 */
	public void setEstadoTrxId(byte estadoTrxId) {
		this.estadoTrxId = estadoTrxId;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
