package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9


/**
 * Representacion de un dispositivo POS.
 * 
 * @author aguerrea
 */
public class DispositivoPos extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6331470677457867372L;
	private long dispositivoPosId;
	private TipoEncripcionB24 tipoEncripcionB24;
	private DispositivoPosHomo dispositivoPosHomo;
	private Entidad entidad;
	private EstadoRegistro estadoRegistro;
	private String identificadorDisp;
	private String referencia;

	/**
	 * @return the dispositivoPosId
	 */
	public long getDispositivoPosId() {
		return dispositivoPosId;
	}

	/**
	 * @param dispositivoPosId
	 *            the dispositivoPosId to set
	 */
	public void setDispositivoPosId(long dispositivoPosId) {
		this.dispositivoPosId = dispositivoPosId;
	}

	/**
	 * @return the tipoEncripcionB24
	 */
	public TipoEncripcionB24 getTipoEncripcionB24() {
		return tipoEncripcionB24;
	}

	/**
	 * @param tipoEncripcionB24
	 *            the tipoEncripcionB24 to set
	 */
	public void setTipoEncripcionB24(TipoEncripcionB24 tipoEncripcionB24) {
		this.tipoEncripcionB24 = tipoEncripcionB24;
	}

	/**
	 * @return the dispositivoPosHomo
	 */
	public DispositivoPosHomo getDispositivoPosHomo() {
		return dispositivoPosHomo;
	}

	/**
	 * @param dispositivoPosHomo
	 *            the dispositivoPosHomo to set
	 */
	public void setDispositivoPosHomo(DispositivoPosHomo dispositivoPosHomo) {
		this.dispositivoPosHomo = dispositivoPosHomo;
	}

	/**
	 * @return the entidad
	 */
	public Entidad getEntidad() {
		return entidad;
	}

	/**
	 * @param entidad
	 *            the entidad to set
	 */
	public void setEntidad(Entidad entidad) {
		this.entidad = entidad;
	}

	/**
	 * @return the estadoRegistro
	 */
	public EstadoRegistro getEstadoRegistro() {
		return estadoRegistro;
	}

	/**
	 * @param estadoRegistro
	 *            the estadoRegistro to set
	 */
	public void setEstadoRegistro(EstadoRegistro estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	/**
	 * @return the identificadorDisp
	 */
	public String getIdentificadorDisp() {
		return identificadorDisp;
	}

	/**
	 * @param identificadorDisp
	 *            the identificadorDisp to set
	 */
	public void setIdentificadorDisp(String identificadorDisp) {
		this.identificadorDisp = identificadorDisp;
	}

	/**
	 * @return the referencia
	 */
	public String getReferencia() {
		return referencia;
	}

	/**
	 * @param referencia
	 *            the referencia to set
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

}
