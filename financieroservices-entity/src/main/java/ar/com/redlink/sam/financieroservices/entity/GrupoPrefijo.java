package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representacion de un grupo de prefijos.
 * 
 * @author aguerrea
 */
public class GrupoPrefijo implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6971730573208143712L;
	private long grupoPrefijoId;
	private Entidad entidad;
	private EstadoRegistro estadoRegistro;
	private String nombre;
	private String descripcion;
	private Date fechaAlta;
	private Date fechaModificacion;
	private Date fechaBaja;
	private Set<GrupoPrefijoEntidad> grupoPrefijoEntidades = new HashSet<GrupoPrefijoEntidad>(0);
	private Set<PerfilPrefijoGrupo> perfilPrefijoGrupos = new HashSet<PerfilPrefijoGrupo>(0);

	/**
	 * @return the grupoPrefijoId
	 */
	public long getGrupoPrefijoId() {
		return grupoPrefijoId;
	}

	/**
	 * @param grupoPrefijoId
	 *            the grupoPrefijoId to set
	 */
	public void setGrupoPrefijoId(long grupoPrefijoId) {
		this.grupoPrefijoId = grupoPrefijoId;
	}

	/**
	 * @return the entidad
	 */
	public Entidad getEntidad() {
		return entidad;
	}

	/**
	 * @param entidad
	 *            the entidad to set
	 */
	public void setEntidad(Entidad entidad) {
		this.entidad = entidad;
	}

	/**
	 * @return the estadoRegistro
	 */
	public EstadoRegistro getEstadoRegistro() {
		return estadoRegistro;
	}

	/**
	 * @param estadoRegistro
	 *            the estadoRegistro to set
	 */
	public void setEstadoRegistro(EstadoRegistro estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the fechaAlta
	 */
	public Date getFechaAlta() {
		return fechaAlta;
	}

	/**
	 * @param fechaAlta
	 *            the fechaAlta to set
	 */
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	/**
	 * @return the fechaModificacion
	 */
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaModificacion
	 *            the fechaModificacion to set
	 */
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	/**
	 * @return the fechaBaja
	 */
	public Date getFechaBaja() {
		return fechaBaja;
	}

	/**
	 * @param fechaBaja
	 *            the fechaBaja to set
	 */
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	/**
	 * @return the grupoPrefijoEntidades
	 */
	public Set<GrupoPrefijoEntidad> getGrupoPrefijoEntidades() {
		return grupoPrefijoEntidades;
	}

	/**
	 * @param grupoPrefijoEntidades
	 *            the grupoPrefijoEntidades to set
	 */
	public void setGrupoPrefijoEntidades(
			Set<GrupoPrefijoEntidad> grupoPrefijoEntidades) {
		this.grupoPrefijoEntidades = grupoPrefijoEntidades;
	}

	/**
	 * @return the perfilPrefijoGrupos
	 */
	public Set<PerfilPrefijoGrupo> getPerfilPrefijoGrupos() {
		return perfilPrefijoGrupos;
	}

	/**
	 * @param perfilPrefijoGrupos
	 *            the perfilPrefijoGrupos to set
	 */
	public void setPerfilPrefijoGrupos(
			Set<PerfilPrefijoGrupo> perfilPrefijoGrupos) {
		this.perfilPrefijoGrupos = perfilPrefijoGrupos;
	}
}
