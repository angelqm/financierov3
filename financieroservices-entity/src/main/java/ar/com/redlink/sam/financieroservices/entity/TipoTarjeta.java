package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.HashSet;
import java.util.Set;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representacion de un tipo de tarjeta.
 * 
 * @author aguerrea
 */
public class TipoTarjeta implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1231001204573343855L;
	private short tipoTarjetaId;
	private String nombre;
	private String descripcion;
	private Set<Prefijo> prefijos = new HashSet<Prefijo>(0);

	/**
	 * @return the tipoTarjetaId
	 */
	public short getTipoTarjetaId() {
		return tipoTarjetaId;
	}

	/**
	 * @param tipoTarjetaId
	 *            the tipoTarjetaId to set
	 */
	public void setTipoTarjetaId(short tipoTarjetaId) {
		this.tipoTarjetaId = tipoTarjetaId;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the prefijos
	 */
	public Set<Prefijo> getPrefijos() {
		return prefijos;
	}

	/**
	 * @param prefijos
	 *            the prefijos to set
	 */
	public void setPrefijos(Set<Prefijo> prefijos) {
		this.prefijos = prefijos;
	}
}
