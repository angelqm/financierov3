package ar.com.redlink.sam.financieroservices.entity;

// Generated 23-mar-2015 15:29:07 by Hibernate Tools 3.2.0.b9

import java.math.BigDecimal;
import java.util.Date;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representacion de una transaccion de financiero.
 * 
 * @author aguerrea
 */
public class TrxFinanciero implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1376813326282670306L;
	private long trxFinancieroId;
	private Red red;
	private Agente agente;
	// private int entidadId;
	private Entidad entidad;
	private Sucursal sucursal;
	private Servidor servidor;
	private TerminalSam terminalSam;
	// private long sucursalId;
	// private long servidorId;
	// private long terminalSamId;
	private PrefijoOp prefijoOp;
	// private long prefijoOpId;
	private String tokenizado;
	private BigDecimal importe;
	private Date fechaHoraAlta;
	private Date fechaHoraBaja;
	private Date fechaOn;
	private EstadoTrx estadoTrx;
	private Integer secuenciaOnWS;
	private Integer secuenciaOnB24;
	private String idRequerimientoAlta;
	private String idRequerimientoBaja;
	private Integer operacionId;

	/**
	 * @return the trxFinancieroId
	 */
	public long getTrxFinancieroId() {
		return trxFinancieroId;
	}

	/**
	 * @param trxFinancieroId
	 *            the trxFinancieroId to set
	 */
	public void setTrxFinancieroId(long trxFinancieroId) {
		this.trxFinancieroId = trxFinancieroId;
	}

	/**
	 * @return the red
	 */
	public Red getRed() {
		return red;
	}

	/**
	 * @param red
	 *            the red to set
	 */
	public void setRed(Red red) {
		this.red = red;
	}

	/**
	 * @return the agente
	 */
	public Agente getAgente() {
		return agente;
	}

	/**
	 * @param agente
	 *            the agente to set
	 */
	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	// /**
	// * @return the entidadId
	// */
	// public int getEntidadId() {
	// return entidadId;
	// }
	//
	// /**
	// * @param entidadId
	// * the entidadId to set
	// */
	// public void setEntidadId(int entidadId) {
	// this.entidadId = entidadId;
	// }
	//
	// /**
	// * @return the sucursalId
	// */
	// public long getSucursalId() {
	// return sucursalId;
	// }
	//
	// /**
	// * @param sucursalId
	// * the sucursalId to set
	// */
	// public void setSucursalId(long sucursalId) {
	// this.sucursalId = sucursalId;
	// }
	//
	// /**
	// * @return the servidorId
	// */
	// public long getServidorId() {
	// return servidorId;
	// }
	//
	// /**
	// * @param servidorId
	// * the servidorId to set
	// */
	// public void setServidorId(long servidorId) {
	// this.servidorId = servidorId;
	// }
	//
	// /**
	// * @return the terminalSamId
	// */
	// public long getTerminalSamId() {
	// return terminalSamId;
	// }
	//
	// /**
	// * @param terminalSamId
	// * the terminalSamId to set
	// */
	// public void setTerminalSamId(long terminalSamId) {
	// this.terminalSamId = terminalSamId;
	// }

	// /**
	// * @return the prefijoOpId
	// */
	// public long getPrefijoOpId() {
	// return prefijoOpId;
	// }
	//
	// /**
	// * @param prefijoOpId
	// * the prefijoOpId to set
	// */
	// public void setPrefijoOpId(long prefijoOpId) {
	// this.prefijoOpId = prefijoOpId;
	// }

	/**
	 * @return the tokenizado
	 */
	public String getTokenizado() {
		return tokenizado;
	}

	/**
	 * @param tokenizado
	 *            the tokenizado to set
	 */
	public void setTokenizado(String tokenizado) {
		this.tokenizado = tokenizado;
	}

	/**
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}

	/**
	 * @param importe
	 *            the importe to set
	 */
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}

	/**
	 * @return the fechaHoraAlta
	 */
	public Date getFechaHoraAlta() {
		return fechaHoraAlta;
	}

	/**
	 * @param fechaHoraAlta
	 *            the fechaHoraAlta to set
	 */
	public void setFechaHoraAlta(Date fechaHoraAlta) {
		this.fechaHoraAlta = fechaHoraAlta;
	}

	/**
	 * @return the fechaHoraBaja
	 */
	public Date getFechaHoraBaja() {
		return fechaHoraBaja;
	}

	/**
	 * @param fechaHoraBaja
	 *            the fechaHoraBaja to set
	 */
	public void setFechaHoraBaja(Date fechaHoraBaja) {
		this.fechaHoraBaja = fechaHoraBaja;
	}

	/**
	 * @return the idRequerimientoAlta
	 */
	public String getIdRequerimientoAlta() {
		return idRequerimientoAlta;
	}

	/**
	 * @param idRequerimientoAlta
	 *            the idRequerimientoAlta to set
	 */
	public void setIdRequerimientoAlta(String idRequerimientoAlta) {
		this.idRequerimientoAlta = idRequerimientoAlta;
	}

	/**
	 * @return the idRequerimientoBaja
	 */
	public String getIdRequerimientoBaja() {
		return idRequerimientoBaja;
	}

	/**
	 * @param idRequerimientoBaja
	 *            the idRequerimientoBaja to set
	 */
	public void setIdRequerimientoBaja(String idRequerimientoBaja) {
		this.idRequerimientoBaja = idRequerimientoBaja;
	}

	/**
	 * @return the secuenciaOn
	 */
	public Integer getSecuenciaOnWS() {
		return secuenciaOnWS;
	}

	/**
	 * @param secuenciaOn
	 *            the secuenciaOn to set
	 */
	public void setSecuenciaOnWS(Integer secuenciaOn) {
		this.secuenciaOnWS = secuenciaOn;
	}

	/**
	 * @return the estadoTrx
	 */
	public EstadoTrx getEstadoTrx() {
		return estadoTrx;
	}

	/**
	 * @param estadoTrx
	 *            the estadoTrx to set
	 */
	public void setEstadoTrx(EstadoTrx estadoTrx) {
		this.estadoTrx = estadoTrx;
	}

	/**
	 * @return the secuenciaOnB24
	 */
	public Integer getSecuenciaOnB24() {
		return secuenciaOnB24;
	}

	/**
	 * @param secuenciaOnB24
	 *            the secuenciaOnB24 to set
	 */
	public void setSecuenciaOnB24(Integer secuenciaOnB24) {
		this.secuenciaOnB24 = secuenciaOnB24;
	}

	/**
	 * @return the prefijoOp
	 */
	public PrefijoOp getPrefijoOp() {
		return prefijoOp;
	}

	/**
	 * @param prefijoOp
	 *            the prefijoOp to set
	 */
	public void setPrefijoOp(PrefijoOp prefijoOp) {
		this.prefijoOp = prefijoOp;
	}

	/**
	 * @return the entidad
	 */
	public Entidad getEntidad() {
		return entidad;
	}

	/**
	 * @param entidad
	 *            the entidad to set
	 */
	public void setEntidad(Entidad entidad) {
		this.entidad = entidad;
	}

	/**
	 * @return the sucursal
	 */
	public Sucursal getSucursal() {
		return sucursal;
	}

	/**
	 * @param sucursal
	 *            the sucursal to set
	 */
	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	/**
	 * @return the servidor
	 */
	public Servidor getServidor() {
		return servidor;
	}

	/**
	 * @param servidor
	 *            the servidor to set
	 */
	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}

	/**
	 * @return the terminalSam
	 */
	public TerminalSam getTerminalSam() {
		return terminalSam;
	}

	/**
	 * @param terminalSam
	 *            the terminalSam to set
	 */
	public void setTerminalSam(TerminalSam terminalSam) {
		this.terminalSam = terminalSam;
	}

	/**
	 * @return the fechaOn
	 */
	public Date getFechaOn() {
		return fechaOn;
	}

	/**
	 * @param fechaOn the fechaOn to set
	 */
	public void setFechaOn(Date fechaOn) {
		this.fechaOn = fechaOn;
	}

	/**
	 * @return the operacionId
	 */
	public Integer getOperacionId() {
		return operacionId;
	}

	/**
	 * @param operacionId the operacionId to set
	 */
	public void setOperacionId(Integer operacionId) {
		this.operacionId = operacionId;
	}

}
