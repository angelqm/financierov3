package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Representacion de la relacion Perfil Prefijo - Grupo.
 * 
 * @author aguerrea
 */
public class PerfilPrefijoGrupo extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1266429325655017677L;
	private long perfilPrefijoGrupoId;
	private PerfilPrefijo perfilPrefijo;
	private GrupoPrefijo grupoPrefijo;
	private Date fechaAlta;
	private Date fechaModificacion;
	private Date fechaBaja;
	private Set<GrupoPrefijoOp> grupoPrefijoOps = new HashSet<GrupoPrefijoOp>(0);

	/**
	 * @return the perfilPrefijoGrupoId
	 */
	public long getPerfilPrefijoGrupoId() {
		return perfilPrefijoGrupoId;
	}

	/**
	 * @param perfilPrefijoGrupoId
	 *            the perfilPrefijoGrupoId to set
	 */
	public void setPerfilPrefijoGrupoId(long perfilPrefijoGrupoId) {
		this.perfilPrefijoGrupoId = perfilPrefijoGrupoId;
	}

	/**
	 * @return the perfilPrefijo
	 */
	public PerfilPrefijo getPerfilPrefijo() {
		return perfilPrefijo;
	}

	/**
	 * @param perfilPrefijo
	 *            the perfilPrefijo to set
	 */
	public void setPerfilPrefijo(PerfilPrefijo perfilPrefijo) {
		this.perfilPrefijo = perfilPrefijo;
	}

	/**
	 * @return the grupoPrefijo
	 */
	public GrupoPrefijo getGrupoPrefijo() {
		return grupoPrefijo;
	}

	/**
	 * @param grupoPrefijo
	 *            the grupoPrefijo to set
	 */
	public void setGrupoPrefijo(GrupoPrefijo grupoPrefijo) {
		this.grupoPrefijo = grupoPrefijo;
	}

	/**
	 * @return the fechaAlta
	 */
	public Date getFechaAlta() {
		return fechaAlta;
	}

	/**
	 * @param fechaAlta
	 *            the fechaAlta to set
	 */
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	/**
	 * @return the fechaModificacion
	 */
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaModificacion
	 *            the fechaModificacion to set
	 */
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	/**
	 * @return the fechaBaja
	 */
	public Date getFechaBaja() {
		return fechaBaja;
	}

	/**
	 * @param fechaBaja
	 *            the fechaBaja to set
	 */
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	/**
	 * @return the grupoPrefijoOps
	 */
	public Set<GrupoPrefijoOp> getGrupoPrefijoOps() {
		return grupoPrefijoOps;
	}

	/**
	 * @param grupoPrefijoOps
	 *            the grupoPrefijoOps to set
	 */
	public void setGrupoPrefijoOps(Set<GrupoPrefijoOp> grupoPrefijoOps) {
		this.grupoPrefijoOps = grupoPrefijoOps;
	}

}
