package ar.com.redlink.sam.financieroservices.entity;

import ar.com.redlink.framework.entities.RedLinkEntity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

/**
 * Representacion del ID de la relacion Grupo Prefijo - Entidad.
 */
public class GrupoPrefijoEntidadId implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4897178374113427058L;
	private long grupoPrefijoId;
	private long prefijoEntidadId;

	/**
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof GrupoPrefijoEntidadId))
			return false;
		GrupoPrefijoEntidadId castOther = (GrupoPrefijoEntidadId) other;

		return (this.getGrupoPrefijoId() == castOther.getGrupoPrefijoId())
				&& (this.getPrefijoEntidadId() == castOther
						.getPrefijoEntidadId());
	}

	/**
	 * @return the grupoPrefijoId
	 */
	public long getGrupoPrefijoId() {
		return grupoPrefijoId;
	}

	/**
	 * @param grupoPrefijoId
	 *            the grupoPrefijoId to set
	 */
	public void setGrupoPrefijoId(long grupoPrefijoId) {
		this.grupoPrefijoId = grupoPrefijoId;
	}

	/**
	 * @return the prefijoEntidadId
	 */
	public long getPrefijoEntidadId() {
		return prefijoEntidadId;
	}

	/**
	 * @param prefijoEntidadId
	 *            the prefijoEntidadId to set
	 */
	public void setPrefijoEntidadId(long prefijoEntidadId) {
		this.prefijoEntidadId = prefijoEntidadId;
	}

}
