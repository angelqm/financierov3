package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.HashSet;
import java.util.Set;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representacion de un Tipo de Sucursal.
 * 
 * @author aguerrea
 */
public class TipoSucursal implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4983465840354276457L;
	private byte tipoSucursalId;
	private String codigo;
	private String descripcion;
	private Set<Sucursal> sucursales = new HashSet<Sucursal>(0);

	/**
	 * @return the tipoSucursalId
	 */
	public byte getTipoSucursalId() {
		return tipoSucursalId;
	}

	/**
	 * @param tipoSucursalId
	 *            the tipoSucursalId to set
	 */
	public void setTipoSucursalId(byte tipoSucursalId) {
		this.tipoSucursalId = tipoSucursalId;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the sucursales
	 */
	public Set<Sucursal> getSucursales() {
		return sucursales;
	}

	/**
	 * @param sucursales
	 *            the sucursales to set
	 */
	public void setSucursales(Set<Sucursal> sucursales) {
		this.sucursales = sucursales;
	}
}
