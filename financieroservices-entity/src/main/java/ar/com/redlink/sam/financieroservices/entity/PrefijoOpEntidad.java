package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representacion de la relacion Prefijo Op - Entidad.
 * 
 * @author aguerrea
 */
public class PrefijoOpEntidad implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6337302820852115956L;
	private long prefijoOpEntidadId;
	private PrefijoOp prefijoOp;
	private Entidad entidad;
	private EstadoRegistro estadoRegistro;
	private Date fechaAlta;
	private Date fechaModificacion;
	private Date fechaBaja;
	private Set<GrupoPrefijoOp> grupoPrefijoOps = new HashSet<GrupoPrefijoOp>(0);

	/**
	 * @return the prefijoOpEntidadId
	 */
	public long getPrefijoOpEntidadId() {
		return prefijoOpEntidadId;
	}

	/**
	 * @param prefijoOpEntidadId
	 *            the prefijoOpEntidadId to set
	 */
	public void setPrefijoOpEntidadId(long prefijoOpEntidadId) {
		this.prefijoOpEntidadId = prefijoOpEntidadId;
	}

	/**
	 * @return the prefijoOp
	 */
	public PrefijoOp getPrefijoOp() {
		return prefijoOp;
	}

	/**
	 * @param prefijoOp
	 *            the prefijoOp to set
	 */
	public void setPrefijoOp(PrefijoOp prefijoOp) {
		this.prefijoOp = prefijoOp;
	}

	/**
	 * @return the entidad
	 */
	public Entidad getEntidad() {
		return entidad;
	}

	/**
	 * @param entidad
	 *            the entidad to set
	 */
	public void setEntidad(Entidad entidad) {
		this.entidad = entidad;
	}

	/**
	 * @return the estadoRegistro
	 */
	public EstadoRegistro getEstadoRegistro() {
		return estadoRegistro;
	}

	/**
	 * @param estadoRegistro
	 *            the estadoRegistro to set
	 */
	public void setEstadoRegistro(EstadoRegistro estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	/**
	 * @return the fechaAlta
	 */
	public Date getFechaAlta() {
		return fechaAlta;
	}

	/**
	 * @param fechaAlta
	 *            the fechaAlta to set
	 */
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	/**
	 * @return the fechaModificacion
	 */
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaModificacion
	 *            the fechaModificacion to set
	 */
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	/**
	 * @return the fechaBaja
	 */
	public Date getFechaBaja() {
		return fechaBaja;
	}

	/**
	 * @param fechaBaja
	 *            the fechaBaja to set
	 */
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	/**
	 * @return the grupoPrefijoOps
	 */
	public Set<GrupoPrefijoOp> getGrupoPrefijoOps() {
		return grupoPrefijoOps;
	}

	/**
	 * @param grupoPrefijoOps
	 *            the grupoPrefijoOps to set
	 */
	public void setGrupoPrefijoOps(Set<GrupoPrefijoOp> grupoPrefijoOps) {
		this.grupoPrefijoOps = grupoPrefijoOps;
	}

}
