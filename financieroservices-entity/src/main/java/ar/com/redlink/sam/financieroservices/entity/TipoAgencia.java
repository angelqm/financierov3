package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.HashSet;
import java.util.Set;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representaicon del Tipo de Agencia.
 * 
 * @author aguerrea
 * 
 */
public class TipoAgencia implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4460075464263089310L;
	private int tipoAgenciaId;
	private short codigo;
	private String nombre;
	private String descripcion;
	private Set<Agente> agentes = new HashSet<Agente>(0);

	/**
	 * @return the tipoAgenciaId
	 */
	public int getTipoAgenciaId() {
		return tipoAgenciaId;
	}

	/**
	 * @param tipoAgenciaId
	 *            the tipoAgenciaId to set
	 */
	public void setTipoAgenciaId(int tipoAgenciaId) {
		this.tipoAgenciaId = tipoAgenciaId;
	}

	/**
	 * @return the codigo
	 */
	public short getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo(short codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the agentes
	 */
	public Set<Agente> getAgentes() {
		return agentes;
	}

	/**
	 * @param agentes
	 *            the agentes to set
	 */
	public void setAgentes(Set<Agente> agentes) {
		this.agentes = agentes;
	}

}
