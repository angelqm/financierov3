package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.Date;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representacion de un enrolamiento de tipo POS.
 * 
 * @author aguerrea
 */
public class EnrolamientoPos implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1775656812589096806L;
	private long enrolamientoPosId;
	private TerminalPos terminalPos;
	private DispositivoPos dispositivoPos;
	private EstadoRegistro estadoRegistro;
	private String identificadorTerm;
	private Date fechaAlta;
	private Date fechaModificacion;
	private Date fechaBaja;

	/**
	 * @return the enrolamientoPosId
	 */
	public long getEnrolamientoPosId() {
		return enrolamientoPosId;
	}

	/**
	 * @param enrolamientoPosId
	 *            the enrolamientoPosId to set
	 */
	public void setEnrolamientoPosId(long enrolamientoPosId) {
		this.enrolamientoPosId = enrolamientoPosId;
	}

	/**
	 * @return the terminalPos
	 */
	public TerminalPos getTerminalPos() {
		return terminalPos;
	}

	/**
	 * @param terminalPos
	 *            the terminalPos to set
	 */
	public void setTerminalPos(TerminalPos terminalPos) {
		this.terminalPos = terminalPos;
	}

	/**
	 * @return the dispositivoPos
	 */
	public DispositivoPos getDispositivoPos() {
		return dispositivoPos;
	}

	/**
	 * @param dispositivoPos
	 *            the dispositivoPos to set
	 */
	public void setDispositivoPos(DispositivoPos dispositivoPos) {
		this.dispositivoPos = dispositivoPos;
	}

	/**
	 * @return the estadoRegistro
	 */
	public EstadoRegistro getEstadoRegistro() {
		return estadoRegistro;
	}

	/**
	 * @param estadoRegistro
	 *            the estadoRegistro to set
	 */
	public void setEstadoRegistro(EstadoRegistro estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	/**
	 * @return the identificadorTerm
	 */
	public String getIdentificadorTerm() {
		return identificadorTerm;
	}

	/**
	 * @param identificadorTerm
	 *            the identificadorTerm to set
	 */
	public void setIdentificadorTerm(String identificadorTerm) {
		this.identificadorTerm = identificadorTerm;
	}

	/**
	 * @return the fechaAlta
	 */
	public Date getFechaAlta() {
		return fechaAlta;
	}

	/**
	 * @param fechaAlta
	 *            the fechaAlta to set
	 */
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	/**
	 * @return the fechaModificacion
	 */
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaModificacion
	 *            the fechaModificacion to set
	 */
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	/**
	 * @return the fechaBaja
	 */
	public Date getFechaBaja() {
		return fechaBaja;
	}

	/**
	 * @param fechaBaja
	 *            the fechaBaja to set
	 */
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

}
