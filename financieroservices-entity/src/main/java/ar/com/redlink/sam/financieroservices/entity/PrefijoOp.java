package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.HashSet;
import java.util.Set;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representacion de la relacion Prefijo Operacion.
 * 
 * @author aguerrea
 */
public class PrefijoOp implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4892429984772795475L;
	private long prefijoOpId;
	private String codigo;
	private String nombre;
	private String descripcion;
	private Set<PrefijoOpEntidad> prefijoOpEntidades = new HashSet<PrefijoOpEntidad>(0);

	
	
	public PrefijoOp(long prefijoOpId) {
		this.prefijoOpId = prefijoOpId;
	}

	public PrefijoOp() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the prefijoOpId
	 */
	public long getPrefijoOpId() {
		return prefijoOpId;
	}

	/**
	 * @param prefijoOpId
	 *            the prefijoOpId to set
	 */
	public void setPrefijoOpId(long prefijoOpId) {
		this.prefijoOpId = prefijoOpId;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the prefijoOpEntidades
	 */
	public Set<PrefijoOpEntidad> getPrefijoOpEntidades() {
		return prefijoOpEntidades;
	}

	/**
	 * @param prefijoOpEntidades
	 *            the prefijoOpEntidades to set
	 */
	public void setPrefijoOpEntidades(Set<PrefijoOpEntidad> prefijoOpEntidades) {
		this.prefijoOpEntidades = prefijoOpEntidades;
	}

}
