package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representacion de una Operacion.
 * 
 * @author aguerrea
 */
public class Operacion implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2637790043264937697L;
	private long operacionId;
	private String codigo;
	private String nombre;

	/**
	 * @return the operacionId
	 */
	public long getOperacionId() {
		return operacionId;
	}

	/**
	 * @param operacionId
	 *            the operacionId to set
	 */
	public void setOperacionId(long operacionId) {
		this.operacionId = operacionId;
	}

	/**
	 * @return the codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
