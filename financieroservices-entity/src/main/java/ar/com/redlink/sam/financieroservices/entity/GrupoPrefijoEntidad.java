package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.Date;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representacion de la relacion Grupo Prefijo - Entidad.
 * 
 */
public class GrupoPrefijoEntidad implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2980871821641029729L;
	private GrupoPrefijoEntidadId id;
	private GrupoPrefijo grupoPrefijo;
	private PrefijoEntidad prefijoEntidad;
	private EstadoRegistro estadoRegistro;
	private Date fechaAlta;
	private Date fechaModificacion;
	private Date fechaBaja;

	/**
	 * @return the id
	 */
	public GrupoPrefijoEntidadId getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(GrupoPrefijoEntidadId id) {
		this.id = id;
	}

	/**
	 * @return the grupoPrefijo
	 */
	public GrupoPrefijo getGrupoPrefijo() {
		return grupoPrefijo;
	}

	/**
	 * @param grupoPrefijo
	 *            the grupoPrefijo to set
	 */
	public void setGrupoPrefijo(GrupoPrefijo grupoPrefijo) {
		this.grupoPrefijo = grupoPrefijo;
	}

	/**
	 * @return the prefijoEntidad
	 */
	public PrefijoEntidad getPrefijoEntidad() {
		return prefijoEntidad;
	}

	/**
	 * @param prefijoEntidad
	 *            the prefijoEntidad to set
	 */
	public void setPrefijoEntidad(PrefijoEntidad prefijoEntidad) {
		this.prefijoEntidad = prefijoEntidad;
	}

	/**
	 * @return the estadoRegistro
	 */
	public EstadoRegistro getEstadoRegistro() {
		return estadoRegistro;
	}

	/**
	 * @param estadoRegistro
	 *            the estadoRegistro to set
	 */
	public void setEstadoRegistro(EstadoRegistro estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	/**
	 * @return the fechaAlta
	 */
	public Date getFechaAlta() {
		return fechaAlta;
	}

	/**
	 * @param fechaAlta
	 *            the fechaAlta to set
	 */
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	/**
	 * @return the fechaModificacion
	 */
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaModificacion
	 *            the fechaModificacion to set
	 */
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	/**
	 * @return the fechaBaja
	 */
	public Date getFechaBaja() {
		return fechaBaja;
	}

	/**
	 * @param fechaBaja
	 *            the fechaBaja to set
	 */
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

}
