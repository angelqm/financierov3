package ar.com.redlink.sam.financieroservices.entity;

import java.util.Date;

import ar.com.redlink.framework.entities.RedLinkEntity;

public class AcumuladoTotal implements RedLinkEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long acumuladoTotalId;
	private TerminalSam terminalSamId;
	private String tokenizado;
	private PrefijoOp prefijoOpId;
	private Date fechaTrx;
	private Double importe;
	private long cantidadTrx;
	
	
	/**
	 * @return the acumuladoTotalId
	 */
	public long getAcumuladoTotalId() {
		return acumuladoTotalId;
	}
	/**
	 * @param acumuladoTotalId the acumuladoTotalId to set
	 */
	public void setAcumuladoTotalId(long acumuladoTotalId) {
		this.acumuladoTotalId = acumuladoTotalId;
	}
	/**
	 * @return the terminalSamId
	 */
	public TerminalSam getTerminalSamId() {
		return terminalSamId;
	}
	/**
	 * @param terminalSamId the terminalSamId to set
	 */
	public void setTerminalSamId(TerminalSam terminalSamId) {
		this.terminalSamId = terminalSamId;
	}
	/**
	 * @return the tokenizado
	 */
	public String getTokenizado() {
		return tokenizado;
	}
	/**
	 * @param tokenizado the tokenizado to set
	 */
	public void setTokenizado(String tokenizado) {
		this.tokenizado = tokenizado;
	}
	/**
	 * @return the prefijoOpId
	 */
	public PrefijoOp getPrefijoOpId() {
		return prefijoOpId;
	}
	/**
	 * @param prefijoOpId the prefijoOpId to set
	 */
	public void setPrefijoOpId(PrefijoOp prefijoOpId) {
		this.prefijoOpId = prefijoOpId;
	}
	/**
	 * @return the fechaTrx
	 */
	public Date getFechaTrx() {
		return fechaTrx;
	}
	/**
	 * @param fechaTrx the fechaTrx to set
	 */
	public void setFechaTrx(Date fechaTrx) {
		this.fechaTrx = fechaTrx;
	}
	/**
	 * @return the importe
	 */
	public Double getImporte() {
		return importe;
	}
	/**
	 * @param importe the importe to set
	 */
	public void setImporte(Double importe) {
		this.importe = importe;
	}
	/**
	 * @return the cantidadTrx
	 */
	public long getCantidadTrx() {
		return cantidadTrx;
	}
	/**
	 * @param cantidadTrx the cantidadTrx to set
	 */
	public void setCantidadTrx(long cantidadTrx) {
		this.cantidadTrx = cantidadTrx;
	}
	
}
