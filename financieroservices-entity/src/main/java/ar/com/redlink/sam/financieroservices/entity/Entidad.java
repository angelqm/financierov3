package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representacion de una entidad.
 * 
 * @author aguerrea
 */
public class Entidad extends BaseEntity implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6379609562905995103L;
	private int entidadId;
	private String codEntidad;
	private String descripcion;
	private String cuit;
	private Date horaCorte;
	private long ultimoNroRetencion;
	private String repositorio;
	private Date fechaAlta;
	private Date fechaModificacion;
	private Date fechaBaja;
	private String codEntidadB24;
	private Set<PrefijoEntidad> prefijoEntidades = new HashSet<PrefijoEntidad>(0);
	private Set<TerminalSam> terminalesSam = new HashSet<TerminalSam>(0);
	private Set<Agente> agentes = new HashSet<Agente>(0);
	private Set<PerfilSam> perfilesSam = new HashSet<PerfilSam>(0);
	//private Set<DispositivoPos> dispositivosPos = new HashSet<DispositivoPos>(0);
	private Set<GrupoPrefijo> grupoPrefijos = new HashSet<GrupoPrefijo>(0);
	private Set<Prefijo> prefijos = new HashSet<Prefijo>(0);
	private Set<PrefijoOpEntidad> prefijoOpEntidades = new HashSet<PrefijoOpEntidad>(0);
	private Set<PerfilPrefijo> perfilPrefijos = new HashSet<PerfilPrefijo>(0);
	private Set<Sucursal> sucursales = new HashSet<Sucursal>(0);

	/**
	 * @return the entidadId
	 */
	public int getEntidadId() {
		return entidadId;
	}

	/**
	 * @param entidadId
	 *            the entidadId to set
	 */
	public void setEntidadId(int entidadId) {
		this.entidadId = entidadId;
	}

	/**
	 * @return the codEntidad
	 */
	public String getCodEntidad() {
		return codEntidad;
	}

	/**
	 * @param codEntidad
	 *            the codEntidad to set
	 */
	public void setCodEntidad(String codEntidad) {
		this.codEntidad = codEntidad;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the cuit
	 */
	public String getCuit() {
		return cuit;
	}

	/**
	 * @param cuit
	 *            the cuit to set
	 */
	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	/**
	 * @return the horaCorte
	 */
	public Date getHoraCorte() {
		return horaCorte;
	}

	/**
	 * @param horaCorte
	 *            the horaCorte to set
	 */
	public void setHoraCorte(Date horaCorte) {
		this.horaCorte = horaCorte;
	}

	/**
	 * @return the ultimoNroRetencion
	 */
	public long getUltimoNroRetencion() {
		return ultimoNroRetencion;
	}

	/**
	 * @param ultimoNroRetencion
	 *            the ultimoNroRetencion to set
	 */
	public void setUltimoNroRetencion(long ultimoNroRetencion) {
		this.ultimoNroRetencion = ultimoNroRetencion;
	}

	/**
	 * @return the repositorio
	 */
	public String getRepositorio() {
		return repositorio;
	}

	/**
	 * @param repositorio
	 *            the repositorio to set
	 */
	public void setRepositorio(String repositorio) {
		this.repositorio = repositorio;
	}

	/**
	 * @return the fechaAlta
	 */
	public Date getFechaAlta() {
		return fechaAlta;
	}

	/**
	 * @param fechaAlta
	 *            the fechaAlta to set
	 */
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	/**
	 * @return the fechaModificacion
	 */
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaModificacion
	 *            the fechaModificacion to set
	 */
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	/**
	 * @return the fechaBaja
	 */
	public Date getFechaBaja() {
		return fechaBaja;
	}

	/**
	 * @param fechaBaja
	 *            the fechaBaja to set
	 */
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	/**
	 * @return the codEntidadB24
	 */
	public String getCodEntidadB24() {
		return codEntidadB24;
	}

	/**
	 * @param codEntidadB24
	 *            the codEntidadB24 to set
	 */
	public void setCodEntidadB24(String codEntidadB24) {
		this.codEntidadB24 = codEntidadB24;
	}


	/**
	 * @return the prefijoEntidads
	 */
	public Set<PrefijoEntidad> getPrefijoEntidades() {
		return prefijoEntidades;
	}

	/**
	 * @param prefijoEntidads
	 *            the prefijoEntidads to set
	 */
	public void setPrefijoEntidades(Set<PrefijoEntidad> prefijoEntidades) {
		this.prefijoEntidades = prefijoEntidades;
	}
	

	/**
	 * @return the terminalSam
	 */
	public Set<TerminalSam> getTerminalesSam() {
		return terminalesSam;
	}

	/**
	 * @param terminalSam
	 *            the terminalSam to set
	 */
	public void setTerminalesSam(Set<TerminalSam> terminalesSam) {
		this.terminalesSam = terminalesSam;
	}

	/**
	 * @return the agentes
	 */
	public Set<Agente> getAgentes() {
		return agentes;
	}

	/**
	 * @param agentes
	 *            the agentes to set
	 */
	public void setAgentes(Set<Agente> agentes) {
		this.agentes = agentes;
	}

	/**
	 * @return the perfilesSam
	 */
	public Set<PerfilSam> getPerfilesSam() {
		return perfilesSam;
	}

	/**
	 * @param perfilesSam
	 *            the perfilesSam to set
	 */
	public void setPerfilesSam(Set<PerfilSam> perfilesSam) {
		this.perfilesSam = perfilesSam;
	}

	/**
	 * @return the dispositivosPos
	 */
	/*public Set<DispositivoPos> getDispositivosPos() {
		return dispositivosPos;
	}*/

	/**
	 * @param dispositivosPos
	 *            the dispositivosPos to set
	 */
	/*public void setDispositivosPos(Set<DispositivoPos> dispositivosPos) {
		this.dispositivosPos = dispositivosPos;
	}*/

	/**
	 * @return the grupoPrefijos
	 */
	public Set<GrupoPrefijo> getGrupoPrefijos() {
		return grupoPrefijos;
	}

	/**
	 * @param grupoPrefijos
	 *            the grupoPrefijos to set
	 */
	public void setGrupoPrefijos(Set<GrupoPrefijo> grupoPrefijos) {
		this.grupoPrefijos = grupoPrefijos;
	}
	

	/**
	 * @return the prefijos
	 */
	public Set<Prefijo> getPrefijos() {
		return prefijos;
	}

	/**
	 * @param prefijos
	 *            the prefijos to set
	 */
	public void setPrefijos(Set<Prefijo> prefijos) {
		this.prefijos = prefijos;
	}

	/**
	 * @return the prefijoOpEntidades
	 */
	public Set<PrefijoOpEntidad> getPrefijoOpEntidades() {
		return prefijoOpEntidades;
	}

	/**
	 * @param prefijoOpEntidades
	 *            the prefijoOpEntidades to set
	 */
	public void setPrefijoOpEntidades(Set<PrefijoOpEntidad> prefijoOpEntidades) {
		this.prefijoOpEntidades = prefijoOpEntidades;
	}

	
	/**
	 * @return the perfilPrefijos
	 */
	public Set<PerfilPrefijo> getPerfilPrefijos() {
		return perfilPrefijos;
	}

	/**
	 * @param perfilPrefijos
	 *            the perfilPrefijos to set
	 */
	public void setPerfilPrefijos(Set<PerfilPrefijo> perfilPrefijos) {
		this.perfilPrefijos = perfilPrefijos;
	}
	

	/**
	 * @return the sucursales
	 */
	public Set<Sucursal> getSucursales() {
		return sucursales;
	}

	/**
	 * @param sucursales
	 *            the sucursales to set
	 */
	public void setSucursales(Set<Sucursal> sucursales) {
		this.sucursales = sucursales;
	}

}
