package ar.com.redlink.sam.financieroservices.entity;

import java.util.Date;

import ar.com.redlink.framework.entities.RedLinkEntity;

public class AcumuladoParcial implements RedLinkEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long acumuladoParcialId;
	private String tokenizado;
	private PrefijoOp prefijoOpId;
	private Date fechaTrx;
	private Double importe;
	private long cantidadTrx;
	
	/**
	 * @return the acumuladoParcialId
	 */
	public long getAcumuladoParcialId() {
		return acumuladoParcialId;
	}
	/**
	 * @param acumuladoParcialId the acumuladoParcialId to set
	 */
	public void setAcumuladoParcialId(long acumuladoParcialId) {
		this.acumuladoParcialId = acumuladoParcialId;
	}
	/**
	 * @return the tokenizado
	 */
	public String getTokenizado() {
		return tokenizado;
	}
	/**
	 * @param tokenizado the tokenizado to set
	 */
	public void setTokenizado(String tokenizado) {
		this.tokenizado = tokenizado;
	}
	/**
	 * @return the prefijoOpId
	 */
	public PrefijoOp getPrefijoOpId() {
		return prefijoOpId;
	}
	/**
	 * @param prefijoOpId the prefijoOpId to set
	 */
	public void setPrefijoOpId(PrefijoOp prefijoOpId) {
		this.prefijoOpId = prefijoOpId;
	}
	/**
	 * @return the fechaTrx
	 */
	public Date getFechaTrx() {
		return fechaTrx;
	}
	/**
	 * @param fechaTrx the fechaTrx to set
	 */
	public void setFechaTrx(Date fechaTrx) {
		this.fechaTrx = fechaTrx;
	}
	/**
	 * @return the importe
	 */
	public Double getImporte() {
		return importe;
	}
	/**
	 * @param importe the importe to set
	 */
	public void setImporte(Double importe) {
		this.importe = importe;
	}
	/**
	 * @return the cantidadTrx
	 */
	public long getCantidadTrx() {
		return cantidadTrx;
	}
	/**
	 * @param cantidadTrx the cantidadTrx to set
	 */
	public void setCantidadTrx(long cantidadTrx) {
		this.cantidadTrx = cantidadTrx;
	}
	
}
