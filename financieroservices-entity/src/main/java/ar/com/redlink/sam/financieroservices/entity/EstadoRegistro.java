package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representacion de un estado de registro.
 * 
 * @author aguerrea
 */
public class EstadoRegistro implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1302189341483056620L;
	private short estadoRegistroId;
	private char codigo;
	private String descripcion;

	/**
	 * @return the estadoRegistroId
	 */
	public short getEstadoRegistroId() {
		return estadoRegistroId;
	}

	/**
	 * @param estadoRegistroId
	 *            the estadoRegistroId to set
	 */
	public void setEstadoRegistroId(short estadoRegistroId) {
		this.estadoRegistroId = estadoRegistroId;
	}

	/**
	 * @return the codigo
	 */
	public char getCodigo() {
		return codigo;
	}

	/**
	 * @param codigo
	 *            the codigo to set
	 */
	public void setCodigo(char codigo) {
		this.codigo = codigo;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
