/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  27/04/2015 - 13:26:31
 */

package ar.com.redlink.sam.financieroservices.entity.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Util para manejo de formato de fechas y timestamps, se lo ubica en este
 * modulo por estar vinculado a las entidades y ser el punto pivot de todas las
 * capas. Los formatos se acceden en forma local al thread de atencion.
 * 
 * @author aguerrea
 * 
 */
public final class DateUtil {

	private static final ThreadLocal<SimpleDateFormat> TIMESTAMP_FORMAT = new ThreadLocal<SimpleDateFormat>() {
		/**
		 * @see java.lang.ThreadLocal#initialValue()
		 */
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyyMMddHHmmss");
		}
	};

	private static final ThreadLocal<SimpleDateFormat> DATETIME_FORMAT = new ThreadLocal<SimpleDateFormat>() {
		/**
		 * @see java.lang.ThreadLocal#initialValue()
		 */
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		}
	};

	private static final ThreadLocal<SimpleDateFormat> FECHAONEXT_FORMAT = new ThreadLocal<SimpleDateFormat>() {
		/**
		 * @see java.lang.ThreadLocal#initialValue()
		 */
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyyMMdd");
		}
	};

	private static final ThreadLocal<SimpleDateFormat> DDMMYYYY_FORMAT = new ThreadLocal<SimpleDateFormat>() {
		/**
		 * @see java.lang.ThreadLocal#initialValue()
		 */
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("dd/MM/yyyy");
		}
	};

	private static final ThreadLocal<SimpleDateFormat> FECHAON_FORMAT = new ThreadLocal<SimpleDateFormat>() {
		/**
		 * @see java.lang.ThreadLocal#initialValue()
		 */
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyMMdd");
		}
	};

	/**
	 * Parsea un timestamp.
	 * 
	 * @param dateTimeString
	 * @return Date
	 * @throws ParseException
	 */
	public static final Date parseTimestamp(String dateTimeString)
			throws ParseException {
		return TIMESTAMP_FORMAT.get().parse(dateTimeString);
	}

	/**
	 * Parsea una fechaOn para que se interprete en formato yyyymmdd.
	 * 
	 * @param fechaOn
	 * @return Date
	 * @throws ParseException
	 */
	public static final Date parseFechaOnExt(String fechaOn)
			throws ParseException {
		return FECHAONEXT_FORMAT.get().parse(fechaOn);
	}

	/**
	 * Parsea y luego formatea una fechaOn de Tandem.
	 * 
	 * @param fechaOn
	 * @return la fecha formateada en yyyyMMdd.
	 * @throws ParseException
	 */
	public static final String fechaOnExtFormat(String fechaOn)
			throws ParseException {
		return FECHAONEXT_FORMAT.get().format(
				FECHAON_FORMAT.get().parse(fechaOn));
	}

	/**
	 * Formatea un Date correspondiente a una fechaOn.
	 * 
	 * @param fechaOn
	 * @return la fecha formateada en yyyyMMdd.
	 * @throws ParseException
	 */
	public static final String fechaOnExtFormat(Date fechaOn) {
		return FECHAONEXT_FORMAT.get().format(fechaOn);
	}

	/**
	 * Formatea un date acorde al formato de timestamp requerido por el servicio
	 * de financiero.
	 * 
	 * @param dateTimeDate
	 * @return la fecha formateada en yyyyMMddHHmmss.
	 */
	public static final String timestampFormat(Date dateTimeDate) {
		return TIMESTAMP_FORMAT.get().format(dateTimeDate);
	}

	/**
	 * Formatea un date acorde al formato dd/mm/yyyy.
	 * 
	 * @param date
	 * @return la fecha formateada en dd/mm/yyyy.
	 */
	public static final String dateFormat(Date date) {
		return DDMMYYYY_FORMAT.get().format(date);
	}

	/**
	 * Formatea un date acorde al formato dd/MM/yyyy HH:mm:ss.
	 * 
	 * @param date
	 * @return la fecha formateada en dd/MM/yyyy HH:mm:ss.
	 */
	public static final String dateTimeFormat(Date date) {
		return DATETIME_FORMAT.get().format(date);
	}
}
