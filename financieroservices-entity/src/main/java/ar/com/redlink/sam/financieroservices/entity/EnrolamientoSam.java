package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.Date;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representacion de un Enrolamiento de tipo SAM.
 * 
 * @author aguerrea
 */
public class EnrolamientoSam implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4043508532475899695L;
	private long enrolamientoSamId;
	private TerminalSam terminalSam;
	private EstadoRegistro estadoRegistro;
	private String identificadorTerm;
	private Date fechaAlta;
	private Date fechaModificacion;
	private Date fechaBaja;

	/**
	 * @return the enrolamientoSamId
	 */
	public long getEnrolamientoSamId() {
		return enrolamientoSamId;
	}

	/**
	 * @param enrolamientoSamId
	 *            the enrolamientoSamId to set
	 */
	public void setEnrolamientoSamId(long enrolamientoSamId) {
		this.enrolamientoSamId = enrolamientoSamId;
	}

	/**
	 * @return the terminalSam
	 */
	public TerminalSam getTerminalSam() {
		return terminalSam;
	}

	/**
	 * @param terminalSam
	 *            the terminalSam to set
	 */
	public void setTerminalSam(TerminalSam terminalSam) {
		this.terminalSam = terminalSam;
	}

	/**
	 * @return the estadoRegistro
	 */
	public EstadoRegistro getEstadoRegistro() {
		return estadoRegistro;
	}

	/**
	 * @param estadoRegistro
	 *            the estadoRegistro to set
	 */
	public void setEstadoRegistro(EstadoRegistro estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	/**
	 * @return the identificadorTerm
	 */
	public String getIdentificadorTerm() {
		return identificadorTerm;
	}

	/**
	 * @param identificadorTerm
	 *            the identificadorTerm to set
	 */
	public void setIdentificadorTerm(String identificadorTerm) {
		this.identificadorTerm = identificadorTerm;
	}

	/**
	 * @return the fechaAlta
	 */
	public Date getFechaAlta() {
		return fechaAlta;
	}

	/**
	 * @param fechaAlta
	 *            the fechaAlta to set
	 */
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	/**
	 * @return the fechaModificacion
	 */
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaModificacion
	 *            the fechaModificacion to set
	 */
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	/**
	 * @return the fechaBaja
	 */
	public Date getFechaBaja() {
		return fechaBaja;
	}

	/**
	 * @param fechaBaja
	 *            the fechaBaja to set
	 */
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

}
