package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.HashSet;
import java.util.Set;

import ar.com.redlink.framework.entities.RedLinkEntity;

/**
 * Representacion de un tipo de servidor.
 * 
 * @author aguerrea
 */
public class TipoServidor implements RedLinkEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4539256292983636112L;
	private byte tipoServidorId;
	private String nombre;
	private String descripcion;
	private Set<Servidor> servidores = new HashSet<Servidor>(0);

	/**
	 * @return the tipoServidorId
	 */
	public byte getTipoServidorId() {
		return tipoServidorId;
	}

	/**
	 * @param tipoServidorId
	 *            the tipoServidorId to set
	 */
	public void setTipoServidorId(byte tipoServidorId) {
		this.tipoServidorId = tipoServidorId;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre
	 *            the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the servidores
	 */
	public Set<Servidor> getServidores() {
		return servidores;
	}

	/**
	 * @param servidores
	 *            the servidores to set
	 */
	public void setServidores(Set<Servidor> servidores) {
		this.servidores = servidores;
	}

}
