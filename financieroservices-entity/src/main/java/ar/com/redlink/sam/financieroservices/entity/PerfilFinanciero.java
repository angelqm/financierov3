package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9


/**
 * Representacion de un perfil de Financiero.
 * 
 * @author aguerrea
 */
public class PerfilFinanciero extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5459374389405687868L;
	private long perfilFinancieroId;
	private PerfilPrefijo perfilPrefijo;
	private PerfilSam perfilSam;

	/**
	 * @return the perfilFinancieroId
	 */
	public long getPerfilFinancieroId() {
		return perfilFinancieroId;
	}

	/**
	 * @param perfilFinancieroId
	 *            the perfilFinancieroId to set
	 */
	public void setPerfilFinancieroId(long perfilFinancieroId) {
		this.perfilFinancieroId = perfilFinancieroId;
	}

	/**
	 * @return the perfilPrefijo
	 */
	public PerfilPrefijo getPerfilPrefijo() {
		return perfilPrefijo;
	}

	/**
	 * @param perfilPrefijo
	 *            the perfilPrefijo to set
	 */
	public void setPerfilPrefijo(PerfilPrefijo perfilPrefijo) {
		this.perfilPrefijo = perfilPrefijo;
	}

	/**
	 * @return the perfilSam
	 */
	public PerfilSam getPerfilSam() {
		return perfilSam;
	}

	/**
	 * @param perfilSam
	 *            the perfilSam to set
	 */
	public void setPerfilSam(PerfilSam perfilSam) {
		this.perfilSam = perfilSam;
	}
}
