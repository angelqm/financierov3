package ar.com.redlink.sam.financieroservices.entity;

import java.util.Date;

public class ComercioPEI extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4533801203456402432L;

	private long comercioPEIId;
	private long agenteId;
	private long codigoComercioPei;
	private EstadoRegistro estadoRegistro;
	private Date fechaAlta;
	private Date fechaModificacion;
	private Date fechaBaja;
	
	
	/**
	 * @return the comercioPEIId
	 */
	public long getComercioPEIId() {
		return comercioPEIId;
	}

	/**
	 * @param comercioPEIId the comercioPEIId to set
	 */
	public void setComercioPEIId(long comercioPEIId) {
		this.comercioPEIId = comercioPEIId;
	}

	/**
	 * @return the agenteId
	 */
	public long getAgenteId() {
		return agenteId;
	}

	/**
	 * @param agenteId the agenteId to set
	 */
	public void setAgenteId(long agenteId) {
		this.agenteId = agenteId;
	}

	/**
	 * @return the codigoComercioPei
	 */
	public long getCodigoComercioPei() {
		return codigoComercioPei;
	}

	/**
	 * @param codigoComercioPei the codigoComercioPei to set
	 */
	public void setCodigoComercioPei(long codigoComercioPei) {
		this.codigoComercioPei = codigoComercioPei;
	}

	/**
	 * @return the estadoRegistro
	 */
	public EstadoRegistro getEstadoRegistro() {
		return estadoRegistro;
	}

	/**
	 * @param estadoRegistro the estadoRegistro to set
	 */
	public void setEstadoRegistro(EstadoRegistro estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	/**
	 * @return the fechaAlta
	 */
	public Date getFechaAlta() {
		return fechaAlta;
	}

	/**
	 * @param fechaAlta the fechaAlta to set
	 */
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	/**
	 * @return the fechaModificacion
	 */
	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaModificacion the fechaModificacion to set
	 */
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	/**
	 * @return the fechaBaja
	 */
	public Date getFechaBaja() {
		return fechaBaja;
	}

	/**
	 * @param fechaBaja the fechaBaja to set
	 */
	public void setFechaBaja(Date fechaBaja) {
		this.fechaBaja = fechaBaja;
	}
	
}
