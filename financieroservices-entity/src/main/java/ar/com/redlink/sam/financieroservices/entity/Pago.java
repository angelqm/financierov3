package ar.com.redlink.sam.financieroservices.entity;

import java.math.BigDecimal;
import java.util.Date;

import ar.com.redlink.framework.entities.RedLinkEntity;

public class Pago implements RedLinkEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long pagoId;
	private Intencion intencionId;
	private String tokenizado;
	private BigDecimal importe;
	
	private Integer secuenciaOnwS;
	private Integer secuenciaOnB24;
	private Date fechaOn;
	private Date fechaHoraAlta;
	private Date fechaHoraBaja;
	private String idRequerimientoBaja;
	private long operacionPei;
	private String horaOn;
	
	/**
	 * @return the pagoId
	 */
	public Long getPagoId() {
		return pagoId;
	}
	/**
	 * @param pagoId the pagoId to set
	 */
	public void setPagoId(Long pagoId) {
		this.pagoId = pagoId;
	}
	/**
	 * @return the intencionId
	 */
	public Intencion getIntencionId() {
		return intencionId;
	}
	/**
	 * @param intencionId the intencionId to set
	 */
	public void setIntencionId(Intencion intencionId) {
		this.intencionId = intencionId;
	}
	/**
	 * @return the tokenizado
	 */
	public String getTokenizado() {
		return tokenizado;
	}
	/**
	 * @param tokenizado the tokenizado to set
	 */
	public void setTokenizado(String tokenizado) {
		this.tokenizado = tokenizado;
	}
	/**
	 * @return the importe
	 */
	public BigDecimal getImporte() {
		return importe;
	}
	/**
	 * @param importe the importe to set
	 */
	public void setImporte(BigDecimal importe) {
		this.importe = importe;
	}
	/**
	 * @return the secuenciaOnwS
	 */
	public Integer getSecuenciaOnwS() {
		return secuenciaOnwS;
	}
	/**
	 * @param secuenciaOnwS the secuenciaOnwS to set
	 */
	public void setSecuenciaOnwS(Integer secuenciaOnwS) {
		this.secuenciaOnwS = secuenciaOnwS;
	}
	/**
	 * @return the secuenciaOnB24
	 */
	public Integer getSecuenciaOnB24() {
		return secuenciaOnB24;
	}
	/**
	 * @param secuenciaOnB24 the secuenciaOnB24 to set
	 */
	public void setSecuenciaOnB24(Integer secuenciaOnB24) {
		this.secuenciaOnB24 = secuenciaOnB24;
	}
	/**
	 * @return the fechaOn
	 */
	public Date getFechaOn() {
		return fechaOn;
	}
	/**
	 * @param fechaOn the fechaOn to set
	 */
	public void setFechaOn(Date fechaOn) {
		this.fechaOn = fechaOn;
	}
	/**
	 * @return the fechaHoraAlta
	 */
	public Date getFechaHoraAlta() {
		return fechaHoraAlta;
	}
	/**
	 * @param fechaHoraAlta the fechaHoraAlta to set
	 */
	public void setFechaHoraAlta(Date fechaHoraAlta) {
		this.fechaHoraAlta = fechaHoraAlta;
	}
	/**
	 * @return the fechaHoraBaja
	 */
	public Date getFechaHoraBaja() {
		return fechaHoraBaja;
	}
	/**
	 * @param fechaHoraBaja the fechaHoraBaja to set
	 */
	public void setFechaHoraBaja(Date fechaHoraBaja) {
		this.fechaHoraBaja = fechaHoraBaja;
	}
	/**
	 * @return the idRequerimientoBaja
	 */
	public String getIdRequerimientoBaja() {
		return idRequerimientoBaja;
	}
	/**
	 * @param idRequerimientoBaja the idRequerimientoBaja to set
	 */
	public void setIdRequerimientoBaja(String idRequerimientoBaja) {
		this.idRequerimientoBaja = idRequerimientoBaja;
	}
	/**
	 * @return the operacionPei
	 */
	public long getOperacionPei() {
		return operacionPei;
	}
	/**
	 * @param operacionPei the operacionPei to set
	 */
	public void setOperacionPei(long operacionPei) {
		this.operacionPei = operacionPei;
	}
	/**
	 * @return the horaOn
	 */
	public String getHoraOn() {
		return horaOn;
	}
	/**
	 * @param horaOn the horaOn to set
	 */
	public void setHoraOn(String horaOn) {
		this.horaOn = horaOn;
	}
	
}
