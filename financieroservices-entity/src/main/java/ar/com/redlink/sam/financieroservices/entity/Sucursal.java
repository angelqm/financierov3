package ar.com.redlink.sam.financieroservices.entity;

// Generated 18-mar-2015 19:17:58 by Hibernate Tools 3.2.0.b9

import java.util.HashSet;
import java.util.Set;

/**
 * Representacion de una sucursal.
 * 
 * @author aguerrea
 */
public class Sucursal extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8644744273185175635L;
	private long sucursalId;
	private Red red;
	private Entidad entidad;
	private TipoSucursal tipoSucursal;
	private Agente agente;
	private EstadoRegistro estadoRegistro;
	private Sucursal sucursal;
	private String codSucursal;
	private String descripcion;
	private char generaPendientes;
	private String latitud;
	private String longitud;

	private Set<Sucursal> sucursales = new HashSet<Sucursal>(0);
	private Set<SucursalPerfilSam> sucursalPerfilesSam = new HashSet<SucursalPerfilSam>(
			0);

	private Set<Servidor> servidores = new HashSet<Servidor>(0);

	private Set<TerminalPos> terminalesPos = new HashSet<TerminalPos>(0);

	/**
	 * @return the sucursalId
	 */
	public long getSucursalId() {
		return sucursalId;
	}

	/**
	 * @param sucursalId
	 *            the sucursalId to set
	 */
	public void setSucursalId(long sucursalId) {
		this.sucursalId = sucursalId;
	}

	/**
	 * @return the red
	 */
	public Red getRed() {
		return red;
	}

	/**
	 * @param red
	 *            the red to set
	 */
	public void setRed(Red red) {
		this.red = red;
	}

	/**
	 * @return the entidad
	 */
	public Entidad getEntidad() {
		return entidad;
	}

	/**
	 * @param entidad
	 *            the entidad to set
	 */
	public void setEntidad(Entidad entidad) {
		this.entidad = entidad;
	}

	/**
	 * @return the tipoSucursal
	 */
	public TipoSucursal getTipoSucursal() {
		return tipoSucursal;
	}

	/**
	 * @param tipoSucursal
	 *            the tipoSucursal to set
	 */
	public void setTipoSucursal(TipoSucursal tipoSucursal) {
		this.tipoSucursal = tipoSucursal;
	}

	/**
	 * @return the agente
	 */
	public Agente getAgente() {
		return agente;
	}

	/**
	 * @param agente
	 *            the agente to set
	 */
	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	/**
	 * @return the estadoRegistro
	 */
	public EstadoRegistro getEstadoRegistro() {
		return estadoRegistro;
	}

	/**
	 * @param estadoRegistro
	 *            the estadoRegistro to set
	 */
	public void setEstadoRegistro(EstadoRegistro estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	/**
	 * @return the sucursal
	 */
	public Sucursal getSucursal() {
		return sucursal;
	}

	/**
	 * @param sucursal
	 *            the sucursal to set
	 */
	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	/**
	 * @return the codSucursal
	 */
	public String getCodSucursal() {
		return codSucursal;
	}

	/**
	 * @param codSucursal
	 *            the codSucursal to set
	 */
	public void setCodSucursal(String codSucursal) {
		this.codSucursal = codSucursal;
	}

	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion
	 *            the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the generaPendientes
	 */
	public char getGeneraPendientes() {
		return generaPendientes;
	}

	/**
	 * @param generaPendientes
	 *            the generaPendientes to set
	 */
	public void setGeneraPendientes(char generaPendientes) {
		this.generaPendientes = generaPendientes;
	}

	/**
	 * @return the latitud
	 */
	public String getLatitud() {
		return latitud;
	}

	/**
	 * @param latitud
	 *            the latitud to set
	 */
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	/**
	 * @return the longitud
	 */
	public String getLongitud() {
		return longitud;
	}

	/**
	 * @param longitud
	 *            the longitud to set
	 */
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	/**
	 * @return the sucursales
	 */
	public Set<Sucursal> getSucursales() {
		return sucursales;
	}

	/**
	 * @param sucursales
	 *            the sucursales to set
	 */
	public void setSucursales(Set<Sucursal> sucursales) {
		this.sucursales = sucursales;
	}

	/**
	 * @return the sucursalPerfilesSam
	 */
	public Set<SucursalPerfilSam> getSucursalPerfilesSam() {
		return sucursalPerfilesSam;
	}

	/**
	 * @param sucursalPerfilesSam
	 *            the sucursalPerfilesSam to set
	 */
	public void setSucursalPerfilesSam(
			Set<SucursalPerfilSam> sucursalPerfilesSam) {
		this.sucursalPerfilesSam = sucursalPerfilesSam;
	}

	/**
	 * @return the servidores
	 */
	public Set<Servidor> getServidores() {
		return servidores;
	}

	/**
	 * @param servidores
	 *            the servidores to set
	 */
	public void setServidores(Set<Servidor> servidores) {
		this.servidores = servidores;
	}

	/**
	 * @return the terminalesPos
	 */
	public Set<TerminalPos> getTerminalesPos() {
		return terminalesPos;
	}

	/**
	 * @param terminalesPos
	 *            the terminalesPos to set
	 */
	public void setTerminalesPos(Set<TerminalPos> terminalesPos) {
		this.terminalesPos = terminalesPos;
	}
}
