/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  20/03/2015 - 00:18:12
 */

package ar.com.redlink.sam.financieroservices.service.enums;

/**
 * Enums con los codigos de mensaje de las distintas capas de Financiero Services.
 * 
 * @author aguerrea
 * 
 */
public enum FinancieroServicesMsgEnum {

	FINA000("FINA-000", "OK"),
	FINA100("FINA-100", "Entidad no informada"),
	FINA101("FINA-101", "Entidad no operativa"),
	FINA102("FINA-102", "Entidad inexistente"),
	FINA103("FINA-103", "Sucursal no informada"),
	FINA104("FINA-104", "Sucursal no operativa"),
	FINA105("FINA-105", "Sucursal inexistente"),
	FINA106("FINA-106", "Terminal no informada"),
	FINA107("FINA-107", "Terminal no operativa"),
	FINA108("FINA-108", "Terminal inexistente"),
	FINA109("FINA-109", "Terminal no enrolada"),
	FINA110("FINA-110", "Terminal no coincide con enrolamiento"),
	FINA111("FINA-111", "Entidad no informada"),
	FINA112("FINA-112", "Entidad no operativa"),
	FINA113("FINA-113", "Entidad inexistente"),
	FINA114("FINA-114", "Sucursal no informada"),
	FINA115("FINA-115", "Sucursal no operativa"),
	FINA116("FINA-116", "Agente no informado"),
	FINA117("FINA-117", "Agente no operativo"),
	FINA118("FINA-118", "Agente inexistente"),
	FINA119("FINA-119", "Servidor no operativo"),
	FINA120("FINA-120", "Servidor inexistente"),
	FINA121("FINA-121", "Dispositivo POS no operativo"),
	FINA122("FINA-122", "Identificador de Dispositivo no coincidente"),
	FINA123("FINA-123", "Terminal sin dispositivo POS"),
	FINA124("FINA-124", "Tarjeta inexistente"),
	
	FINA125("FINA-125", "Prefijo no asociado a la entidad"),
	FINA126("FINA-126", "Prefijo no operativo"),
	FINA127("FINA-127", "Sucursal sin Perfil SAM operativo valido"),
	FINA128("FINA-128", "Sucursal con Perfil SAM inactivo"),
	FINA129("FINA-129", "Perfil Financiero no operativo"),
	FINA130("FINA-130", "Perfil SAM asociado a mas de un Perfil Financiero activo"),
	FINA131("FINA-131", "Perfil Prefijo Grupo no operativo"),
	FINA132("FINA-132", "Prefijo no habilitado en el perfil"),
	FINA133("FINA-133", "GrupoPrefijoOp no operativo"),
	FINA134("FINA-134", "GrupoPrefijoOpRegla no operativo"),
	FINA135("FINA-135", "Perfil SAM no operativo"),
	FINA136("FINA-136", "ID de requerimiento inexistente"),
	FINA137("FINA-137", "Terminal ya enrolada"),
	FINA138("FINA-138", "Los datos de registro de la transaccion no coinciden con los almacenados o la transaccion ya fue anulada"),
	FINA139("FINA-139", "La transaccion a anular no es la ultima cursada"),
	FINA140("FINA-140", "La anulacion no se realizo correctamente"),
	FINA141("FINA-141", "No se informo ID de Requerimiento"),
	FINA142("FINA-142", "No se informo IP Cliente"),
	FINA143("FINA-143", "No se informo timestamp"),
	FINA144("FINA-144", "Formato de Timestamp incorrecto"),
	FINA145("FINA-145", "Tokenizado no informado"),
	FINA146("FINA-146", "Sucursal con mas de un perfil SAM valido/activo"),
	FINA147("FINA-147", "Perfil SAM sin Perfil Financiero operativo valido"),
	FINA148("FINA-148", "Tipo Prefijo Regla no habilitado"),
	FINA149("FINA-149", "El ID de Requerimiento ya existe"),
	FINA150("FINA-150", "Terminal Base24 no informado"),		
	//validaciones de datos PEI
	FINA151("FINA-151", "Idpago no informado"),
	FINA152("FINA-152", "Track1 no informado"),
	FINA153("FINA-153", "Track2 no informado"),
	FINA154("FINA-154", "Postentrymode no informada"),
	FINA155("FINA-155", "Idreferenciaoperacioncomercio no informada"),
	FINA156("FINA-156", "SecuenciaOn no informada"),
	FINA157("FINA-157", "Numero no informado"),
	FINA158("FINA-158", "Titular documento no informado"),
	FINA159("FINA-159", "Importe no informado"),
	FINA160("FINA-160", "El importe no puede ser cero"),
	FINA161("FINA-161", "Moneda no informada o invalida"),
	FINA162("FINA-162", "Codigo seguridad no informado"),
	FINA163("FINA-163", "Error al parsear fechas. Formato valido: dd/MM/yyyy"),
	FINA164("FINA-164", "Error al parsear la fechaOn"),
	FINA165("FINA-165", "Requerimiento Origen no informado"),
	FINA166("FINA-166", "Comercio PEI no asociado."),
	FINA167("FINA-167", "Comercio PEI no operativo."),
	FINA168("FINA-168", "Terminal PEI no asociada."),
	FINA169("FINA-169", "Terminal PEI no operativa."),
	//Validaciones de TERMINALES PAGAR. PRODUCTO_ID=4
	FINA170("FINA-170", "Sucursal NO dada de alta en Asociacion Perfil."),
	FINA171("FINA-171", "Sucursal con Asociacion Perfil inactivo."),
	FINA172("FINA-172", "Sucursal SIN Asociacion Perfil SAM activa."),	
	FINA173("FINA-173", "Sucursal SIN Asociacion Perfil SAM."),
	FINA175("FINA-175", "Producto ID en terminal no informado."),
	FINA176("FINA-176", "Terminal ya enrolada previamente."),
	FINA177("FINA-177", "idReferenciaTrx no informado"),
	
	
	FINA174("FINA-174", "La transaccion ha sido realizada, por favor consulte sus movimientos."),
	
	FINA200("FINA-200", "Error Interno"),
	FINA201("FINA-201", "Error Interno al Enrolar el Dispositivo"),
	FINA202("FINA-202", "Error Interno al cargar el archivo de mappings de Prefijos"),
	//Validaciones al desencriptar el pan de la tarjeta.
	FINA203("FINA-203", "Error Interno al desencriptar el pan de la tarjeta, por favor volver a consultar sus movimientos"),
	
	//Validaciones de limites y campos requeridos.
	FINA300("FINA-300", "Operacion no Autorizada. Se supera el limite de importe de operaciones por dia por %1s  [Limite tarjeta: %2$.2f - Remanente: %3$.2f]."),
	FINA306("FINA-306", "Operacion no Autorizada. Se supera el limite de cantidad de operaciones por dia por %1s  [Limite tarjeta: %2$.0f - Remanente: %3$.0f]."),
	FINA301("FINA-301", "La transaccion requiere PIN"),
	FINA302("FINA-302", "La transaccion requiere autorizacion"),
	FINA303("FINA-303", "La transaccion requiere codigo de seguridad"),
	FINA304("FINA-304", "La transaccion requiere sufijo"),
	FINA305("FINA-305", "La transaccion requiere DNI"),
	//Tandem.
	FINA500("FINA-500", "Operacion cancelada por timeout - [A0-%1] "),
	FINA501("FINA-501", "Datos insuficientes [A2-%1]"),	
	FINA502("FINA-502", "Codigo de operacion no soportado [A3-%1]"),
	FINA503("FINA-503", "Tipo de terminal no soportado [A4-%1]"),
	FINA504("FINA-504", "No se pudo realizar la operacion  [A5-%1]"),
	FINA505("FINA-505", "Numero de terminal no encontrado [A6-%1]"),
	FINA506("FINA-506", "No se pudo realizar la operacion  [A8-%1]"),
	FINA507("FINA-507", "No se pudo realizar la operacion  [{0}]"),
	FINA508("FINA-508", "Operacion Rechazada"), //70 - Operacion Rechazada / No se pudo desencriptar el track 2.
	FINA509("FINA-509", "PIN invalido"),
	//respuestas del servicio PEI
	FINA600("FINA-600", "ERROR_GENERICO"),
	FINA601("FINA-601", "ERROR_GENERICO"),
	FINA602("FINA-602", "CODIGO_SEGURIDAD_INCORRECTO"),
	FINA603("FINA-603", "COMERCIO_INVALIDO"),
	FINA604("FINA-604", "DNI_INCORRECTO"),
	FINA605("FINA-605", "ESTADO_INVALIDO"),
	FINA606("FINA-606", "ID_PAGO_INVALIDO"),
	FINA607("FINA-607", "ID_CANAL_INVALIDO"),
	FINA608("FINA-608", "IMPORTE_INCORRECTO"),
	FINA609("FINA-609", "CUENTA_DE_COMERCIO_INVALIDA"),
	FINA610("FINA-610", "FECHA_INVALIDA"),
	FINA611("FINA-611", "REFERENCIA_TRX_COMERCIO_REPETIDA"),
	FINA612("FINA-612", "TARJETA_INVALIDA"),
	FINA613("FINA-613", "ULTIMOS_4_DIGITOS_INCORRECTO"),
	FINA614("FINA-614", "LIMITE_DIARIO_EXCEDIDO"),
	FINA615("FINA-615", "ID_TERMINAL_INVALIDO"),
	FINA616("FINA-616", "NO_AUTORIZADO"),
	FINA617("FINA-617", "CONCEPTO_NO_DISPONIBLE"),
	FINA618("FINA-618", "CONCEPTO_INVALIDO"),
	FINA619("FINA-619", "SALDO_EXCEDIDO");
	
				
	private String code;
	private String msg;

	/**
	 * Constructor para el Enum.
	 * @param code
	 * @param msg
	 */
	FinancieroServicesMsgEnum(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @param msg the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}
}
