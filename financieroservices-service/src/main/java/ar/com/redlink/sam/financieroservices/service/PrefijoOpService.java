/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  12/04/2015 - 12:38:34
 */

package ar.com.redlink.sam.financieroservices.service;

import ar.com.redlink.framework.services.RedLinkService;
import ar.com.redlink.sam.financieroservices.entity.PrefijoOp;

/**
 * Interface que extiende sobre el servicio generico de {@link PrefijoOp}.
 * 
 * @author aguerrea
 * 
 */
public interface PrefijoOpService extends RedLinkService {

	/**
	 * Provee el codigo de prefijo asociado al metodo provisto por parametro.
	 * 
	 * @param methodName
	 * @return prefijoOpId.
	 */
	public String getPrefijoOpCod(String methodName);

	/**
	 * Valida si la trx es reversible, sin ir a la base.
	 * 
	 * @param prefijoOp
	 * @return true si es reversible.
	 */
	public boolean isReversable(String prefijoOp);

	/**
	 * Devuelve un listado de los prefijos no reversables.
	 * 
	 * @return Listado de prefijos no reversables.
	 */
	public String getNotReversablePrefijoOps();
}
