/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  29/03/2015 - 14:25:58
 */

package ar.com.redlink.sam.financieroservices.service.validation;

import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;

/**
 * Definicion de las operaciones de una validacion de FinancieroService.
 * 
 * @author aguerrea
 * 
 */
public interface FSValidation {

	/**
	 * 
	 * @param validation
	 * @throws FinancieroValidationException
	 * @return
	 * @throws FinancieroException 
	 */
	public ValidationDTO validate(ValidationDTO validation)
			throws FinancieroValidationException, FinancieroException;
}
