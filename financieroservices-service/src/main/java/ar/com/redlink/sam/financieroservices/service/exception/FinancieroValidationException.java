/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  22/03/2015 - 13:26:06
 */

package ar.com.redlink.sam.financieroservices.service.exception;

/**
 * Excepcion para las validaciones del servicio.
 * 
 * @author aguerrea
 * 
 */
public class FinancieroValidationException extends FinancieroException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2934852143122835769L;

	/**
	 * Constructor default para las excepciones de validacion.
	 * 
	 * @param cod
	 * @param msj
	 */
	public FinancieroValidationException(String cod, String msj) {
		super(cod, msj);
	}

}
