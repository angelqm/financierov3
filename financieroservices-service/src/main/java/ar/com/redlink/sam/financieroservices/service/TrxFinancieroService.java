/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  31/03/2015 - 08:11:21
 */

package ar.com.redlink.sam.financieroservices.service;

import java.util.Date;

import ar.com.redlink.framework.services.crud.RedLinkGenericService;
import ar.com.redlink.sam.financieroservices.dto.TrxFinancieroDTO;
import ar.com.redlink.sam.financieroservices.dto.TrxFinancieroValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.Intencion;
import ar.com.redlink.sam.financieroservices.entity.Pago;
import ar.com.redlink.sam.financieroservices.entity.TrxFinanciero;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;

/**
 * Servicio para {@link TrxFinanciero}.
 * 
 * @author aguerrea
 * 
 */
public interface TrxFinancieroService extends RedLinkGenericService<TrxFinanciero> {

	/**
	 * Hace un insert en TRX_FINANCIERO utilizando la entidad provista.
	 * 
	 * @param trxFinancieroDTO
	 * @throws FinancieroException
	 */
	@Deprecated
	public void insert(TrxFinancieroDTO trxFinancieroDTO) throws FinancieroException;

	/**
	 * Actualiza en TRX_FINANCIERO
	 * 
	 * @param trxFinancieroDTO
	 * @throws FinancieroException
	 */
	public void updatePei(Pago pago) throws FinancieroException;
	
	public void updateDevolucionPei(Pago pago,  String idRequerimientoOrig, boolean isParcial) throws FinancieroException;
	
	/**
	 * Anula una transaccion en el registro de TRX_FINANCIERO.
	 * 
	 * @param idRequerimientoOrig
	 * @param idRequerimientoBaja
	 * @throws FinancieroException
	 */
	@Deprecated
	public void annulTrx(Long trxFinancieroId, String idRequerimientoBaja) throws FinancieroException;

	/**
	 * 
	 * Rollbackea una transaccion en el registro de TRX_FINANCIERO.
	 * 
	 * @param idRequerimientoOrig
	 * @param idRequerimientoBaja
	 * @throws FinancieroException
	 */
	public void rollbackTrx(Long trxFinancieroId, String idRequerimientoBaja) throws FinancieroException;

	/**
	 * Devuelve la suma del campo importe por el criterio definido.
	 * 
	 * @param filterField
	 * @param filterId
	 * @param dateFrom
	 * @param dateTo
	 * @param tokenizado
	 * @param prefijoOpId
	 * 
	 * @return importe actual en TRX_FINANCIERO.
	 * 
	 */
	public Double getSum(String filterField, String filterId, Date dateFrom, Date dateTo, String tokenizado, Long prefijoOpId)
			throws FinancieroException;

	/**
	 * Devuelve el count dado por el criterio definido.
	 * 
	 * @param filterField
	 * @param filterId
	 * @param dateFrom
	 * @param dateTo
	 * @param tokenizado
	 * @param prefijoOpId
	 * 
	 * @return count de operaciones validas.
	 */
	public Integer getCount(String filterField, String filterId, Date dateFrom,	Date dateTo, String tokenizado, Long prefijoOpId)
			throws FinancieroException;

	/**
	 * Devuelve el count de SeqNums encontrados filtrando por terminalSamId.
	 * 
	 * @param seqNum
	 * @param terminalSamId
	 * @param listado
	 *            de operationIds reversibles.
	 * 
	 * @return SEQNum.
	 * @throws FinancieroException
	 */
	public Long getReversableOperationIds(String seqNum, String terminalSamId, String reversableOperationIds)
			throws FinancieroException;

	/**
	 * Valida los datos de TRX provistos y devuelve el numero de secuencia
	 * asociado si existe alguno.
	 * 
	 * @param trxFinancieroValidationDTO
	 */
	@Deprecated
	public TrxFinanciero validateTrx( TrxFinancieroValidationDTO trxFinancieroValidationDTO) throws FinancieroException;
	
	
	public Pago validateCancel( TrxFinancieroValidationDTO trxFinancieroValidationDTO) throws FinancieroException;

	/**
	 * Obtiene el ultimo valor de SEQNUM de Tandem.
	 * 
	 * @param terminalSamId
	 * 
	 * @return El numero de secuencia.
	 * @throws FinancieroException
	 */
	public String getMaxSeqForTerm(String terminalSamId) throws FinancieroException;

	/**
	 * Checkea el ID de requerimiento indicado y devuelve true o false si el
	 * mismo ya existe.
	 * 
	 * @param idRequerimiento
	 * @return true si existe o false.
	 * @throws FinancieroException
	 */
	public boolean checkIdRequerimiento(String idRequerimiento)	throws FinancieroException;

	/**
	 * 
	 * @param idRequerimientoOrig
	 * @param idRequerimientoBaja
	 * @param prefijoOpId
	 * @throws FinancieroException
	 */
	@Deprecated
	void annulTrxPei(String idRequerimientoOrig, String idRequerimientoBaja, String prefijoOpId) throws FinancieroException;
	
	void annulTrxPeiIntencion(String idRequerimientoOrig, String idRequerimientoBaja, String prefijoOpId) throws FinancieroException;
	
	/**
	 * 
	 * @param idRequerimiento
	 * @return
	 * @throws FinancieroException
	 */
	boolean checkIdRequerimientoOperacionPeiNulo(String idRequerimiento) throws FinancieroException;
	
	/**
	 * 
	 * @param trxFinancieroDTO
	 * @throws FinancieroException
	 */
	void updatePeiBaja(TrxFinancieroDTO trxFinancieroDTO) 	throws FinancieroException;

	/**
	 * Hace registra la generacion del workingkey.
	 * 
	 * @param trxFinancieroDTO
	 * @throws FinancieroException
	 */
	public void registerWorkingKey(TrxFinancieroDTO trxFinancieroDTO, Intencion in) throws FinancieroException;
	
	/**
	 * Hace registra la validacion de la tarjeta.
	 * 
	 * @param trxFinancieroDTO
	 * @throws FinancieroException
	 */
	public void validarTarjeta(TrxFinancieroDTO trxFinancieroDTO, Intencion in) throws FinancieroException;	

	public void getBalance(TrxFinancieroDTO trxFinancieroDTO, Intencion in) throws FinancieroException;
	
	public void doDeposit(TrxFinancieroDTO trxFinancieroDTO, Intencion in) throws FinancieroException;
	
	public void doDebit(TrxFinancieroDTO trxFinancieroDTO, Intencion in) throws FinancieroException;
	
	public void doTransfer(TrxFinancieroDTO trxFinancieroDTO, Intencion in) throws FinancieroException;
		
	public Pago pagoPei(TrxFinancieroDTO trxFinancieroDTO, Intencion in) throws FinancieroException;
	
	public Pago devolucionPagoPei(TrxFinancieroDTO trxFinancieroDTO, Intencion in, boolean isParcial) throws FinancieroException;
	
	public void doDebitPayment(TrxFinancieroDTO trxFinancieroDTO, Intencion in) throws FinancieroException;
	
	public void doWithdraw(TrxFinancieroDTO trxFinancieroDTO, Intencion in) throws FinancieroException;
	
	public void rollbackWithdraw(Long trxFinancieroId, String idRequerimientoBaja) throws FinancieroException;
	
	public void rollbackDebit(Long trxFinancieroId, String idRequerimientoBaja, Intencion in) throws FinancieroException;
	
	public void anullDebitPay(Long trxFinancieroId, String idRequerimientoBaja, Intencion in) throws FinancieroException;
	
	public void cancelExtraction(Long trxFinancieroId, String idRequerimientoBaja, Intencion in) throws FinancieroException;
	
	public void rollbackDeposit(Long trxFinancieroId, String idRequerimientoBaja, Intencion in) throws FinancieroException;
	
	public void registerConsultaPEITrx(TrxFinancieroDTO trxFinancieroDTO, Intencion in) throws FinancieroException;
	
	public Intencion registrarInicioTransaccion(TrxFinancieroDTO trxFinancieroDTO) throws FinancieroException;
	
	public Intencion registrarPruebaServicio(TrxFinancieroDTO trxFinancieroDTO) throws FinancieroException;
	
	public void updateIntencion(Intencion in);
	
}
