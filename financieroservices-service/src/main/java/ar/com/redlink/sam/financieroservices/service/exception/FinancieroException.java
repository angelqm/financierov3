/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  24/03/2015 - 13:58:57
 */

package ar.com.redlink.sam.financieroservices.service.exception;

/**
 * Excepcion base para todo lo referido a FinancieroServices.
 * 
 * @author aguerrea
 * 
 */
public class FinancieroException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3219216488653996296L;
	private final String cod;
	private final String msj;
	private String msjInterno;

	/**
	 * Constructor default para las excepciones de validacion.
	 * 
	 * @param cod
	 * @param msj
	 */
	public FinancieroException(String cod, String msj) {
		this.cod = cod;
		this.msj = msj;
	}

	public FinancieroException(String cod, String msj, String msjInterno) {
		super();
		this.cod = cod;
		this.msj = msj;
		this.msjInterno = msjInterno;
	}

	/**
	 * @return the cod
	 */
	public String getCod() {
		return cod;
	}

	/**
	 * @return the msj
	 */
	public String getMsj() {
		return msj;
	}

	/**
	 * @return the msjInterno
	 */
	public String getMsjInterno() {
		return msjInterno;
	}
	
}
