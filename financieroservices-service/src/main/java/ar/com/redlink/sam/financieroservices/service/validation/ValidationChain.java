/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  29/03/2015 - 14:22:47
 */

package ar.com.redlink.sam.financieroservices.service.validation;

import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroValidationException;

/**
 * Interface que define las operaciones del orquestador de la cadena de
 * validacion.
 * 
 * @author aguerrea
 * 
 */
public interface ValidationChain {

	/**
	 * Valida los parametros recibidos en el ValidationDTO recibido y nutre de
	 * adicionales al mismo.
	 * 
	 * @return validationDTO.
	 * @throws FinancieroValidationException
	 * @throws FinancieroException
	 */
	public ValidationDTO validate(ValidationDTO validation) throws FinancieroValidationException, FinancieroException;
}
