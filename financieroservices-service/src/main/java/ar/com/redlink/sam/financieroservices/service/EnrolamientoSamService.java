/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  12/04/2015 - 00:07:26
 */

package ar.com.redlink.sam.financieroservices.service;

import ar.com.redlink.framework.services.RedLinkService;
import ar.com.redlink.sam.financieroservices.dto.request.EnrolamientoRequestDTO;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;

/**
 * Extension del servicio CRUD para {@link EnrolamientoSam}.
 * 
 * @author aguerrea
 * 
 */
public interface EnrolamientoSamService extends RedLinkService {

	/**
	 * Enrola al dispositivo con la informacion provista en el DTO.
	 * 
	 * @param enrolamientoSamDTO
	 * @throws FinancieroException
	 */
	public void enrollDevice(EnrolamientoRequestDTO enrolamientoSamDTO) throws FinancieroException;
}
