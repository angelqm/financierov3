package ar.com.redlink.sam.financieroservices.service.enums;

import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA600;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA601;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA602;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA603;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA604;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA605;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA606;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA607;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA608;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA609;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA610;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA611;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA612;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA613;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA614;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA615;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA616;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA617;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA618;
import static ar.com.redlink.sam.financieroservices.service.enums.FinancieroServicesMsgEnum.FINA619;

public enum PeiFinancieroMsgsEnums {		
	
	ERRORGENERICO1(FINA600, "%s - [%s]"), //error generico con descripcion enviada desde enlacepagos
	ERRORGENERICO2(FINA601, "Ha ocurrido un error inesperado - [%s]"), //error generico sin descripcion enviada desde enlacepagos
	ERRORGENERICO3(FINA601, "Ha ocurrido un error inesperado"), 
	CODIGOSEGUIRDADINCORRECTO(FINA602, "El codigo de seguridad no es correcto - [%s]"),
	COMERCIOINVALIDO(FINA603, "El comercio es invalido - [%s]"),
	DNIINCORRECTO(FINA604, "El DNI no coincide con el de la tarjeta - [%s]"),
	ESTADOINVALIDO(FINA605, "El estado del pago es invalido - [%s]"),
	IDPAGOINVALIDO(FINA606, "El id de pago es invalido - [%s]"),
	IDCANALINVALIDO(FINA607, "El id del canal es invalido - [%s]"),
	IMPORTEINCORRECTO(FINA608, "El importe no es un numero mayor a cero - [%s]"),
	CUENTADECOMERCIOINVALIDA(FINA609, "La cuenta destino del comercio es invalida - [%s]"),
	FECHAINVALIDA(FINA610, "La fecha es invalida - [%s]"),
	REFERENCIATRXCOMERCIOREPETIDA(FINA611, "La referencia de transaccion del comercio ya fue utilizada previamente - [%s]"),
	TARJETAINVALIDA(FINA612, "La tarjeta es invalida - [%s]"),
	ULTIMOS4DIGINTOSINCORRECTO(FINA613, "Los ultimos 4 digitos no coinciden con los de la tarjeta - [%s]"),
	LIMITEDIARIOEXCEDIDO(FINA614, "Se ha excedido el limite de compras diario de la tarjeta - [%s]"),
	IDTERMINALINVALIDO(FINA615, "El Terminal ID es invalido - [%s]"),
	NOAUTORIZADO(FINA616, "No autorizado - [%s]"),
	CONCEPTO_NO_DISPONIBLE(FINA617, "Concepto no disponible - [%s]"),
	CONCEPTO_INVALIDO(FINA618, "Concepto invalido - [%s]"),
	SALDO_EXCEDIDO(FINA619, "Saldo excedido - [%s]");
	

	
	private FinancieroServicesMsgEnum finaEnum;
	private String msg;
	
	PeiFinancieroMsgsEnums(FinancieroServicesMsgEnum finaEnum, String msg) {
		this.setFinaEnum(finaEnum);
		this.setMsg(msg);
	}

	/**
	 * @return the finaEnum
	 */
	public FinancieroServicesMsgEnum getFinaEnum() {
		return finaEnum;
	}

	/**
	 * @param finaEnum the finaEnum to set
	 */
	public void setFinaEnum(FinancieroServicesMsgEnum finaEnum) {
		this.finaEnum = finaEnum;
	}

	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @param msg the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
