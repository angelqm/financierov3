package ar.com.redlink.sam.financieroservices.service.exception;

public class FinancieroPeiException extends FinancieroTandemException {

	private static final long serialVersionUID = -7911299451721862507L;

	public FinancieroPeiException(String cod, String msj, String codTandem, String msjTandem) {
		super(cod, msj, codTandem, msjTandem);

	}

}
