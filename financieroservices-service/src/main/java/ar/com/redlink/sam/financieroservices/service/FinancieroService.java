/*
 * Red Link S.A.
 * http://www.Redlink.com.ar
 */

package ar.com.redlink.sam.financieroservices.service;

import java.math.BigDecimal;

import ar.com.redlink.framework.services.RedLinkService;
import ar.com.redlink.sam.financieroservices.dto.request.AnularDebitoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularDepositoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionCuotasRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionPrestamoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.AnularExtraccionRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.BalanceRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ConsultaOperacionPEIPagarRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ConsultaOperacionPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.EnrolamientoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ExtraccionRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDebitoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDepositoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoPEIPagarRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarDevolucionPagoParcialPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarExtraccionCuotasRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarExtraccionPrestamoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarPagoDebitoRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaPEIPagarRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaPEIRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.RealizarTransferenciaRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ResultadoOperacionRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.TerminalRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.ValidarTarjetaRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.WorkingKeyRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.request.common.FinancieroServiceBaseRequestDTO;
import ar.com.redlink.sam.financieroservices.dto.response.AnularDebitoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.AnularDepositoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.AnularExtraccionCuotasDTO;
import ar.com.redlink.sam.financieroservices.dto.response.AnularExtraccionDTO;
import ar.com.redlink.sam.financieroservices.dto.response.AnularExtraccionPrestamoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ConsultaOperacionPEIDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ConsultaOperacionPEIPagarDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ConsultaTipoDocumentoPEIDTO;
import ar.com.redlink.sam.financieroservices.dto.response.EnrolamientoStatusDTO;
import ar.com.redlink.sam.financieroservices.dto.response.EstadoCuentaDTO;
import ar.com.redlink.sam.financieroservices.dto.response.EstadoOperacionDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ExtraccionCuotasDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ExtraccionPrestamoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ExtraccionResultadoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarDebitoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarDepositoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarDevolucionResultadoPEIDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarDevolucionResultadoPEIPagarDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarPagoDebitoDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarTransferenciaDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarTransferenciaPEIDTO;
import ar.com.redlink.sam.financieroservices.dto.response.RealizarTransferenciaPEIPagarDTO;
import ar.com.redlink.sam.financieroservices.dto.response.UltimasOperacionesDTO;
import ar.com.redlink.sam.financieroservices.dto.response.ValidacionTarjetaDTO;
import ar.com.redlink.sam.financieroservices.dto.response.WorkingKeyDTO;
import ar.com.redlink.sam.financieroservices.dto.validation.ValidationDTO;
import ar.com.redlink.sam.financieroservices.entity.Intencion;

import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;

/**
 * Servicio que provee operaciones del modulo financiero.
 * 
 * @author aguerrea
 * 
 */
public interface FinancieroService extends RedLinkService {

	/**
	 * Metodo para enrolamiento de una terminal.
	 * 
	 * @param enrolamiento
	 * @return Status del enrolamiento.
	 * @throws FinancieroException
	 */
	public EnrolamientoStatusDTO enroll(EnrolamientoRequestDTO enrolamiento) throws FinancieroException;

	/**
	 * Metodo para obtencion de una working key.
	 * 
	 * @param workingKeyRequest
	 * @return WorkingKey negociada.
	 * @throws FinancieroException
	 */
	public WorkingKeyDTO getWorkingKey(WorkingKeyRequestDTO workingKey, Intencion in) throws FinancieroException;

	/**
	 * Metodo para validacion de una tarjeta.
	 * 
	 * @param validateCardRequest
	 * @return devuelve las operaciones que una tarjeta puede realizar.
	 * @throws FinancieroException
	 */
	public ValidacionTarjetaDTO validateCard(ValidarTarjetaRequestDTO validarTarjeta, Intencion in)
			throws FinancieroException;

	/**
	 * Permite realizar una extraccion.
	 * 
	 * @param extraccionRequest
	 * @return devuelve el resultado de la extraccion e informacion de la misma.
	 * @throws FinancieroException
	 */
	public ExtraccionResultadoDTO doWithdraw(ExtraccionRequestDTO extraccion, Intencion in) throws FinancieroException;

	/**
	 * Anula una extraccion.
	 * 
	 * @param anularExtraccion
	 * @throws FinancieroException
	 */
	public AnularExtraccionDTO rollbackWithdraw(AnularExtraccionRequestDTO anularExtraccion, Intencion in)
			throws FinancieroException;

	/**
	 * Devuelve el estado de cuenta.
	 * 
	 * @param balance
	 * @return
	 * @throws FinancieroException
	 */
	public EstadoCuentaDTO getBalance(BalanceRequestDTO balance, Intencion in) throws FinancieroException;

	/**
	 * Devuelve el estado de una operacion.
	 * 
	 * @param resultadoOperacion
	 * @return
	 * @throws FinancieroException
	 */
	public EstadoOperacionDTO getOperationResult(ResultadoOperacionRequestDTO resultadoOperacion)
			throws FinancieroException;

	/**
	 * Devuelve el estado de las ultimas operaciones.
	 * 
	 * @param terminal
	 * @return resultado de la transaccion.
	 * @throws FinancieroException
	 */
	public UltimasOperacionesDTO getLatestOperations(TerminalRequestDTO terminal) throws FinancieroException;

	/**
	 * Realiza una operacion de extraccion para debito.
	 * 
	 * @param realizarDebitoRequestDTO
	 * @return resultado de la transaccion.
	 * @throws FinancieroException
	 */
	public RealizarDebitoDTO doDebit(RealizarDebitoRequestDTO realizarDebitoRequestDTO, Intencion in)
			throws FinancieroException;

	/**
	 * 
	 * @param realizarPagoDebitoRequestDTO
	 * @return
	 * @throws FinancieroException
	 */
	public RealizarPagoDebitoDTO doDebitPay(RealizarPagoDebitoRequestDTO realizarPagoDebitoRequestDTO, Intencion in)
			throws FinancieroException;

	/**
	 * Anula una transaccion de extraccion para debito.
	 * 
	 * @param anularDebitoRequestDTO
	 * @return resultado de la transaccion.
	 * @throws FinancieroException
	 */
	public AnularDebitoDTO rollbackDebit(AnularDebitoRequestDTO anularDebitoRequestDTO, Intencion in)
			throws FinancieroException;

	/**
	 * Realiza una operacion de deposito.
	 * 
	 * @param realizarDepositoRequestDTO
	 * @return resultado de la transaccion.
	 * @throws FinancieroException
	 */
	public RealizarDepositoDTO doDeposit(RealizarDepositoRequestDTO realizarDepositoRequestDTO, Intencion in)
			throws FinancieroException;

	/**
	 * Anula una operacion de deposito.
	 * 
	 * @param anularDepositoRequestDTO
	 * @return resultado de la operacion.
	 * @throws FinancieroException
	 */
	public AnularDepositoDTO rollbackDeposit(AnularDepositoRequestDTO anularDepositoRequestDTO, Intencion in)
			throws FinancieroException;

	/**
	 * Realiza una transferencia.
	 * 
	 * @param realizarTransferenciaRequestDTO
	 * @return resultado de la operacion.
	 * @throws FinancieroException
	 */
	public RealizarTransferenciaDTO doTransfer(RealizarTransferenciaRequestDTO realizarTransferenciaRequestDTO,
			Intencion in) throws FinancieroException;

	/**
	 * Realiza una transferencia a PEI.
	 * 
	 * @param RealizarTransferenciaPEIRequestDTO
	 * @return resultado de la operacion.
	 * @throws FinancieroException
	 */
	public RealizarTransferenciaPEIDTO doTransferPEI(String idRequerimiento, String ipCliente, ValidationDTO validation,
			RealizarTransferenciaPEIRequestDTO realizarTransferenciaRequesPEIDTO, String secuenciaOn, Intencion in)
			throws FinancieroException;

	/**
	 * Realiza una devolucion total PEI
	 * 
	 * @param idRequerimiento
	 * @param ipCliente
	 * @param validation
	 * @param realizarDevolucionPagoPEIRequestDTO
	 * @param secuenciaOn
	 * @param idRequerimientoOrig
	 * @param importe
	 * @return
	 * @throws FinancieroException
	 */
	public RealizarDevolucionResultadoPEIDTO doReturnPEI(String idRequerimiento, String ipCliente,
			ValidationDTO validation, RealizarDevolucionPagoPEIRequestDTO realizarDevolucionPagoPEIRequestDTO,
			String secuenciaOn, String idRequerimientoOrig, BigDecimal importe, Intencion in)
			throws FinancieroException;

	/**
	 * Realiza una transferencia a PEI desde PAGAR.
	 * 
	 * @param RealizarTransferenciaPEIPagarRequestDTO
	 * @return resultado de la operacion.
	 * @throws FinancieroException
	 */
	public RealizarTransferenciaPEIPagarDTO doTransferPEIPagar(String idRequerimiento, String ipCliente,
			ValidationDTO validation, RealizarTransferenciaPEIPagarRequestDTO realizarTransferenciaRequesPEIPagarDTO,
			String secuenciaOn, Intencion in) throws FinancieroException;

	
	/* AQM */
	/**
	 * Realiza una devolucion total PEI desde PAGAR
	 * 
	 * @param idRequerimiento
	 * @param ipCliente
	 * @param validation
	 * @param realizarDevolucionPagoPEIPagarRequestDTO
	 * @param secuenciaOn
	 * @param idRequerimientoOrig
	 * @param importe
	 * @return
	 * @throws FinancieroException
	 */
	public RealizarDevolucionResultadoPEIPagarDTO doReturnPEIPagar(String idRequerimiento, String ipCliente,
			ValidationDTO validation, RealizarDevolucionPagoPEIPagarRequestDTO realizarDevolucionPagoPEIPagarRequestDTO,
			String secuenciaOn, String idRequerimientoOrig, BigDecimal importe, Intencion in)
			throws FinancieroException;

	/**
	 * Realiza la extraccion de prestamo.
	 * 
	 * @param realizarExtraccionPrestamoRequestDTO
	 * @return resultado de la operacion.
	 * @throws FinancieroException
	 */
	public ExtraccionPrestamoDTO doLoanWithdraw(
			RealizarExtraccionPrestamoRequestDTO realizarExtraccionPrestamoRequestDTO) throws FinancieroException;

	/**
	 * Anula la extraccion de prestamo.
	 * 
	 * @param anularExtraccionPrestamoRequestDTO
	 * @return resultado de la operacion.
	 * @throws FinancieroException
	 */
	public AnularExtraccionPrestamoDTO rollbackLoanWithdraw(
			AnularExtraccionPrestamoRequestDTO anularExtraccionPrestamoRequestDTO) throws FinancieroException;

	/**
	 * Realizar una extraccion en cuotas.
	 * 
	 * @param realizarExtraccionCuotasRequestDTO
	 * @return Resultado de la operacion.
	 * @throws FinancieroException
	 */
	public ExtraccionCuotasDTO doSharesWithdraw(RealizarExtraccionCuotasRequestDTO realizarExtraccionCuotasRequestDTO)
			throws FinancieroException;

	/**
	 * Anula la extraccion en cuotas.
	 * 
	 * @param anularExtraccionCuotasDTO
	 * @return Resultado de la operacion.
	 * @throws FinancieroException
	 */
	public AnularExtraccionCuotasDTO rollbackSharesWithdraw(AnularExtraccionCuotasRequestDTO anularExtraccionCuotasDTO)
			throws FinancieroException;

	/**
	 * Consulta operaciones PEI
	 * 
	 * @param requestDTO
	 * @return
	 */
	public ConsultaOperacionPEIDTO getOperationsPEI(String requerimiento, String cliente, ValidationDTO validation,
			ConsultaOperacionPEIRequestDTO requestDTO, String secuenciaOn, Intencion in) throws FinancieroException;

	/**
	 * Consulta operaciones PEI desde PAGAR
	 * 
	 * @param requestDTO
	 * @return
	 */
	public ConsultaOperacionPEIPagarDTO getOperationsPEIPagar(String requerimiento, String cliente,
			ValidationDTO validation, ConsultaOperacionPEIPagarRequestDTO requestDTO, String secuenciaOn, Intencion in)
			throws FinancieroException;

	/**
	 * Realiza una devolucion parcial PEI
	 * 
	 * @param idRequerimiento
	 * @param ipCliente
	 * @param validation
	 * @param requestDTO
	 * @param secuenciaOn
	 * @param idRequerimientoOrig
	 * @return
	 * @throws FinancieroException
	 */
	public RealizarDevolucionResultadoPEIDTO doReturnParcialPEI(String idRequerimiento, String ipCliente,
			ValidationDTO validation, RealizarDevolucionPagoParcialPEIRequestDTO requestDTO, String secuenciaOn,
			String idRequerimientoOrig, BigDecimal importe, Intencion in) throws FinancieroException;

	

	/**
	 * Realiza una transferencia a PEI.
	 * 
	 * @param RealizarTransferenciaPEIRequestDTO
	 * @return resultado de la operacion.
	 * @throws FinancieroException
	 */
	public ConsultaTipoDocumentoPEIDTO consultaTipoDocumentoPEI(String idRequerimiento, String ipCliente)
			throws FinancieroException;


	/**
	 * 
	 * @param anularDebitoRequestDTO
	 * @return
	 * @throws FinancieroException
	 */
	AnularDebitoDTO rollbackDebitPay(AnularDebitoRequestDTO anularDebitoRequestDTO, Intencion in)
			throws FinancieroException;

	public Intencion registrarInicioTransaccion(FinancieroServiceBaseRequestDTO request);

	public Intencion registrarInicioTransaccionPei(Long terminalSamId, int prefijo_op, String idRequerimiento);

	public Intencion registrarPruebaServicio(Long terminalSamId, int prefijo_op, String idRequerimiento)
			throws FinancieroException;

	public void registrarErrorTransaccion(Intencion inten);

}
