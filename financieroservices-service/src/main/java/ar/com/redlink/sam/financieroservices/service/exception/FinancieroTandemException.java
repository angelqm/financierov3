/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  10/04/2015 - 18:36:58
 */

package ar.com.redlink.sam.financieroservices.service.exception;


/**
 * Excepcion que arroja lo referido a Tandem en Financiero.
 * 
 * @author aguerrea
 * 
 */
public class FinancieroTandemException extends FinancieroException {

	private String codTandem;
	private String msjTandem;

	/**
	 * @param cod
	 * @param msj
	 */
	public FinancieroTandemException(String cod, String msj, String codTandem,
			String msjTandem) {
		super(cod, msj);
		this.codTandem = codTandem;
		this.msjTandem = msjTandem;
	}

	/**
	 * @return the codTandem
	 */
	public String getCodTandem() {
		return codTandem;
	}

	/**
	 * @param codTandem
	 *            the codTandem to set
	 */
	public void setCodTandem(String codTandem) {
		this.codTandem = codTandem;
	}

	/**
	 * @return the msjTandem
	 */
	public String getMsjTandem() {
		return msjTandem;
	}

	/**
	 * @param msjTandem
	 *            the msjTandem to set
	 */
	public void setMsjTandem(String msjTandem) {
		this.msjTandem = msjTandem;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 965598151974804121L;

}
