package ar.com.redlink.sam.financieroservices.service.enums;

public enum OperationEnum {

	WORKINGKEY(1, "WORKINGKEY"),
	CUENTAS_RELACIONADAS(2, "CUENTAS RELACIONADAS"),
	CONSULTA_SALDO(3, "CONSULTA SALDO"),
	TRANSFERENCIA(4, "TRANSFERENCIA"),
	EXTRACCION(5, "EXTRACCION"),
	ANULAR_EXTRACCION(6, "ANULAR EXTRACCION"),
	DEPOSITO(7, "DEPOSITO"),
	ANULACION_DEPOSITO(8, "ANULACION DE DEPOSITO"),
	DEBITO(11, "DEBITO BU"),
	ANULACION_DEBITO(12, "ANULACION DEBITO BU"),
	CONSULTA_OPERACIONES_PEI(24, "CONSULTA OPERACIONES PEI"),
	PAGO_CON_DEBITO(27, "PAGO CON DEBITO"),
	PAGO_PEI(23, "PAGO PEI"),
	DEVOLUCION_PAGO_PEI(25, "DEVOLUCION PAGO PEI"),
	DEVOLUCION_PARCIAL_PAGO_PEI(26, "DEVOLUCION PARCIAL PAGO PEI"),
	ANULACION_CON_DEBITO(28, "ANULACION PAGO CON DEBITO"),
	PAGO_PEI_PAGAR(29, "PAGO PEI PAGAR"),
	DEVOLUCION_PAGO_PEI_PAGAR(30, "DEVOLUCION PAGO PEI PAGAR"),
	PRUEBA_SERVICIO_FINANCIERO(99, "PRUEBA DEL SERVICIO FINANCIERO");
	
	private int code;
	private String msg;
	/**
	 * @return the code
	 */
	public int getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(int code) {
		this.code = code;
	}
	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}
	/**
	 * @param msg the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}
	private OperationEnum(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}
	
}
