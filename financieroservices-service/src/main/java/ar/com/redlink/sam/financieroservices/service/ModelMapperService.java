/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  29/01/2015 - 20:07:48
 */

package ar.com.redlink.sam.financieroservices.service;

import org.modelmapper.PropertyMap;

/**
 * Interface para el servicio de Mappeo de entidades.
 * 
 * @author aguerrea
 * 
 */
public interface ModelMapperService {
	/**
	 * Realiza el mapeo entre dos entidades a partir de sus atributos.
	 * 
	 * @param entidad
	 *            entidad origen.
	 * @param entityClass
	 *            entidad destino.
	 * @return la entidad destino con los valores de los atributos de la entidad
	 *         origen.
	 */
	public <T1, T2> T2 modelMapperHelper(T1 entidad, Class<T2> entityClass);

	/**
	 * Realiza el mapeo entre dos entidades a partir de sus atributos. En caso
	 * de mapear algun atributo de manera excepcional se pasa un mapa con la
	 * transformacion especifica.
	 * 
	 * @param entidad
	 *            entidad origen.
	 * @param entityClass
	 *            entidad destino.
	 * @return la entidad destino con los valores de los atributos de la entidad
	 *         origen.
	 */
	public <T1, T2> T2 modelMapperHelper(PropertyMap<T1, T2> orderMap,
			T1 requestDTO, Class<? extends T2> entityClass);
}
