/*
 * Red Link Argentina
 * Ciudad Autonoma de Buenos Aires - Argentina
 * http://www.redlink.com.ar
 * Author: aguerrea
 * Date:  26/03/2015 - 15:03:33
 */

package ar.com.redlink.sam.financieroservices.service;

import ar.com.redlink.framework.services.crud.RedLinkGenericService;
import ar.com.redlink.sam.financieroservices.entity.PrefijoEntidad;
import ar.com.redlink.sam.financieroservices.service.exception.FinancieroException;

/**
 * Extension de RedLinkGenericService para proveer operaciones adicionales
 * relacionadas a los prefijos de tarjeta.
 * 
 * @author aguerrea
 * 
 */
public interface PrefijoEntidadService extends
		RedLinkGenericService<PrefijoEntidad> {

	/**
	 * Busca un prefijo por valor de prefijo.
	 * 
	 * @param tarjeta
	 * @param entidad
	 * @return El prefijoEntidad hallado.
	 * @throws FinancieroException
	 */
	public PrefijoEntidad getPrefijoEntidadByTarjeta(String tarjeta,
			String entidad) throws FinancieroException;
}
